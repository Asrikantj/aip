/*
 * AIPManager.java
 *
 * Created on March 23, 2004, 4:15 PM
 */

package com.computhink.aip;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import com.computhink.aip.resource.ResourceManager;
import com.computhink.aip.ui.ArchiveHistory;
import com.computhink.aip.util.AIPUtil;
import com.computhink.aip.util.VWIdleLogger;
import com.computhink.aip.util.VWLogger;
import com.computhink.aip.util.LocationStatus;
import com.computhink.aip.client.*;

import com.computhink.common.Constants;
import com.computhink.common.Node;
import com.computhink.vws.license.Base64Coder;
/**
 *
 * @author  Ayman
 */
public class AIPManager {
	private  static final ResourceManager connectorManager = ResourceManager.getDefaultManager();
    private static VWAIPClient vwc =null;    
    private static Vector sessions = new Vector();
    private static Hashtable conSessions = new Hashtable();
    private static Hashtable processLocationsTable = new Hashtable();
    private static Hashtable processlocations = new Hashtable();
    private static Hashtable archiveThreads = new Hashtable();
    private static String workingFolder =null;
    private static int currSessionId =0;
    private static boolean autoStart = false;
    private static String autoStartRoom ;
    public static int NOERROR =0;
    public static int ERROR =-1;
    public static Node currentNode = null;
    private static boolean commandPrompt = false;
    static 
    {
       init(); 
    }    
    public AIPManager() {
     
    }
    private static void init()
    {
        createWorkingFolder();
       try{
        vwc =new VWAIPClient(workingFolder);
        
       }
       catch(Exception e){e.printStackTrace();}
       finally
       {
           if(vwc ==null) 
           {
            javax.swing.JOptionPane.showMessageDialog(new javax.swing.JPanel(),connectorManager.getString("AIPManager.CannotInitialize")+" "+ Constants.PRODUCT_NAME +" "+connectorManager.getString("AIPManager.AIP.Client"),"",javax.swing.JOptionPane.ERROR_MESSAGE);
            
            System.exit(0);
           }    
       }    
       
    }
    
    
   public static int login(Session s, String user, String pass) 
   {
	   /**CV2019 merges from CV10.2 line. User name and password encoding has added. Added by Vanitha S**/
	    String userName = Base64Coder.encode(user);
	    String password = Base64Coder.encode(pass);
	    int sid = vwc.login(s.server,s.room,userName, password) ;
        //System.out.println("server:"+server+":"+room+":"+user+":"+pass);
        if(sid > 0)
        {    
            s.user =user;
            s.sessionId =sid;
            File cashFile =new File(s.cacheFolder);
            if(!cashFile.exists())
                cashFile.mkdirs();
            else if(cashFile.isDirectory())
                    AIPUtil.emptyDirectory(s.cacheFolder);
            conSessions.put(new Integer(sid),s);
            processlocations.put(new Integer(sid),new Vector());
        }    
        return sid;
   } 
   public static int logout(int sid)
   {
        int status = vwc.logout(sid);
        if(status == NOERROR)
        {   
           Session s =  getSession(sid);
           File cashFile =new File(s.cacheFolder);
           if(cashFile.exists()&&cashFile.isDirectory())
                AIPUtil.deleteDirectory(s.cacheFolder);                   
           s.sessionId =0;
           conSessions.remove(new Integer(sid));
           processlocations.remove(new Integer(sid));
        }
        else
        {
           //VWLogger.error(getErrorDescription(status)+"<"+status+">",null); 
            //javax.swing.JOptionPane.showMessageDialog(new javax.swing.JPanel(),getErrorDescription(status)+"<"+status+">",AIPUtil.getString("Init.Msg.Title"),javax.swing.JOptionPane.ERROR_MESSAGE);
        }   
        return status;
   }
   public static void logoutAll()
   {
        for(Enumeration e =conSessions.keys();e.hasMoreElements();)
        {
            int sid =((Integer)e.nextElement()).intValue();
            logout(sid);
        }    
   }    
   public static int shutdown()
   {
        AIPUtil.deleteDirectory(workingFolder);
        return vwc.shutdown();
   }    
   public static String getErrorDescription(int err)
   {
        return vwc.getErrorDescription(err);
   }    
    //fill Vector with all servers rooms, element structure (server.room)
   
   /* Issue # 754: If VWS server is down, client should be notified
   *  Methods added of issue 754, C.Shanmugavalli, 10 Nov 2006
   */
   public static int getServersStatus(String server){
   
        int status = -1;
   		Vector rooms =new Vector();
   		status = vwc.getRooms(server,rooms);
   		if(rooms.size() ==0)
   		{
   			status = -1;
   		}else{
   			status = 0;
   		}
   		return status;
   			
   }
    public static int getServersRooms(Vector sRooms)
    {
        int status =0;
        Vector servers =new Vector();
        status = vwc.getServers(servers);
        if(status != NOERROR )
        {
           // javax.swing.JOptionPane.showMessageDialog(new JPanel(),"Can not initialize ViewWise AIP client","",javax.swing.JOptionPane.ERROR_MESSAGE);
            return status;
        }    
        
        for(int i=0;i <servers.size();i++)
        {
            String server = (String)servers.get(i);
            Vector rooms =new Vector();
            vwc.getRooms(server,rooms);
            if(rooms.size() ==0)
            {
                String invalidRoom =AIPUtil.getString("Init.Lab.NoServer");
                sRooms.add(server+invalidRoom); 
                sessions.add(new Session(0,server,invalidRoom,workingFolder,"")); 
            }
            else
            {    
                for(int j=0;j<rooms.size();j++)
                {    
                    sRooms.add(server+"."+(String)rooms.get(j));          
                    sessions.add(new Session(0,server,(String)rooms.get(j),workingFolder,"")); 

                } 
            }    
                
        }    
        return status;        
    }    
    
    private static void createWorkingFolder()
    {
         String home =AIPUtil.findHome();
        if(home ==null || home.equals(""))
        {    
          String CurrentPath =System.getProperty("user.dir");
          String Separetar= System.getProperty("file.separator");
          workingFolder =CurrentPath.substring(0,CurrentPath.lastIndexOf(Separetar))+Separetar+"AIPTemp";
        }  
        else
          workingFolder =home +AIPUtil.pathSep+"AIPTemp";  
          File f_workingFolder =new File(workingFolder);

          if(!f_workingFolder.exists() && !f_workingFolder.isDirectory()){
              f_workingFolder.mkdirs();              
          }
   }
   public static String getWorkingFolder()
   {
        return workingFolder;
   }    
   public static VWAIPClient getVWC()
   {
        return vwc;
   }
   public static Vector getSessions()
   {
     return sessions;   
   }    
   public static Session getSession(int sid)
   {
        for(int i=0;i<sessions.size();i++)
        {
            Session s =(Session)sessions.get(i);
            if(s.sessionId == sid)
                return s;
        }    
        return null;
   } 
   public static Session getSession(String server,String room)
   {
        for(int i=0;i<sessions.size();i++)
        {
            Session s =(Session)sessions.get(i);
            if(server.trim().equalsIgnoreCase(s.server.trim()) && room.trim().equalsIgnoreCase(s.room.trim()))
                return s;
        }    
        return null;
   }    
   public static int getSessionId(String server,String room)
   {
       Session s =getSession(server,room);
       if(s != null)
          return s.sessionId;
       return 0;
   }
   public static Vector getConnectSessionIds()
   {
        Vector sids =new Vector();
        /**CV2019 merges from CV10.2 line. Try catch block added. Added by Vanitha S**/
        try {
	        for(Enumeration e =conSessions.keys();e.hasMoreElements();)
	        {
	            sids.add(e.nextElement());
	            
	        }   
        } catch (Exception e) {
        	VWLogger.info("Exception while getConnectSessionIds ::"+e.getMessage());
        }
        return sids;
   }    
   public static void setCurrSid(int sid)
   {
       currSessionId = sid;
   }
   public static int getCurrSid()
   {
        return currSessionId;
   }    
   public static String getCurrSRoom()
   {
       Session s =getSession(currSessionId);
       
        return s.server+"."+s.room;
   }
   //ayman added temp to discuss
   public static Session getRoomSession(String room)
   {
        
       for(int i=0;i<sessions.size();i++)
        {
            Session s =(Session)sessions.get(i);
            if(s.room.equalsIgnoreCase(room))
                return s;
        }    
        return null;
        
   }
   public static void setLastAuthentication(String userPass)
   {
       if(userPass !=null || !userPass.trim().equals(""))
       { 
        
         String authPref =getVWC().encryptStr(userPass) ;
         
         AIPPreferences.setLoggin(authPref);         
       }  
       else AIPPreferences.setLoggin("");
   } 
   public static String getLastAuthentication()
   {
       String authPref =AIPPreferences.getLoggin();
       
       if(authPref != null && !authPref.trim().equals("")){             
             return getVWC().decryptStr(authPref);
       }      
       else return null;
   }
   // Code added for EAS Service - Start
   public static String getDefaultStartupRoom()
   {
       return AIPPreferences.getStartupRoom();
   }
   public static void setDefaultStartupRoom(String room)
   {
       if(room !=null)
         AIPPreferences.setStartupRoom(room);         
       else 
       	AIPPreferences.setStartupRoom("");
   } 
 
   // Code added for AIP Service - Start
   public static void setAIPApplicationStarted(boolean enable)
   {
        AIPPreferences.setAIPApplicationStarted(enable);
   }    
   public static boolean isAIPApplicationStarted()
   {
        return AIPPreferences.isAIPApplicationStarted();
   } 
   public static void setAIPServiceStarted(boolean enable)
   {
        AIPPreferences.setAIPServiceStarted(enable);
   }    
   public static boolean isAIPServiceStarted()
   {
        return AIPPreferences.isAIPServiceStarted();
   } 
   // Code added for AIP Service - End
   public static boolean isAutoStart()
   {
        return autoStart;
   } 
   public static void setAutoStart(boolean auto)
   {
        autoStart =auto;
   }    
   public static void setAutoStartRoom(String room)
   {
        autoStartRoom =room;
   }    
   public static String getAutoStartRoom()
   {
       return autoStartRoom;
   } 
   public static void setEnableLog(boolean enable)
   {
        AIPPreferences.setEnableLog(enable);
   }    
   public static boolean isEnableLog()
   {
        return AIPPreferences.isEnableLog();
   }
   public static void setConvertToDJVU(int ret)
   {
         AIPPreferences.setConvertToDJVU(ret);
   }
   public static int isConvertToDJVU()
   {
        return AIPPreferences.isConvertToDJVU(); 
   }
   /*  Issue - 742
  *   Developer - Nebu Alex 
  *   Code Description - The following piece of code implements
  *                      Generate primitive document functionalities 
  */
   public static void setPrimitiveDocs(int ret)
   {
         AIPPreferences.setPrimitiveDocs(ret);
   }
   public static int isPrimitiveDocs()
   {
        return AIPPreferences.isPrimitiveDocs(); 
   }
    //End of Issue - 742
   public static void setLogLevel(int level)
   {
        AIPPreferences.setLogLevel(level);
        VWLogger.setLevel(level);
   } 
   public static int getLogLevel()
   {
       return  AIPPreferences.getLogLevel();
   }  
   /*
    * Enhancement 	: Enhancements of AIP  
    * 				  Requirment sent by Rajesh on 09-June-2006 through eMail  	 
    * Created By	: M.Premananth
    * Date			: 14-June-2006
    */
   public static int getMaximumRow()
   {
       return AIPPreferences.getMaximumRow();
   }
   public static void setMaximumRow(int count)
   {
         AIPPreferences.setMaximumRow(count);         
   } 
   
   public static int getDocumentDelay()
   {
	   return AIPPreferences.getDocumentDelay();
   }
   public static void setDocumentDelay(int count)
   {
	   AIPPreferences.setDocumentDelay(count);
   }
   // Archive history - Start
   public static void setArchivalFolder(String path)
   {
        AIPPreferences.setArchivalFolder(path);
   }    
   public static String getArchivalFolder()
   {
        return AIPPreferences.getArchivalFolder();
   } 
   public static void setAgeForArchive(int age)
   {
        AIPPreferences.setAgeForArchive(age);
   }    
   public static int getAgeForArchive()
   {
        return AIPPreferences.getAgeForArchive();
   } 
   public static void setArchiveCount(int count)
   {
        AIPPreferences.setArchiveCount(count);
   }    
   public static int getArchiveCount()
   {
        return AIPPreferences.getArchiveCount();
   } 
   public static boolean isArchiveHistoryEnabled(int sid)
   {
       Session s =getSession(sid);
       if(s ==null) return false;
       return AIPPreferences.isArchiveHistoryEnabled(s);           
   }
   public static boolean setArchiveHistoryEnabled(int sid,boolean enable)
   {
       Session s =getSession(sid);
       if(s == null) return false;
       return AIPPreferences.setArchiveHistoryEnabled(s,enable);           
   }
   public static void addArchiveThread(int sid,ArchiveHistory archiveHistory)
   {
        Integer Isid=new Integer(sid);
        if(!archiveThreads.containsKey(Isid))
        	archiveThreads.put(Isid,archiveHistory);          
   }
   public static ArchiveHistory removeArchiveThread(int sid)
   {
        Integer Isid=new Integer(sid);
        if(!archiveThreads.containsKey(Isid)) return null;
        return (ArchiveHistory)archiveThreads.remove(Isid);
   }
   // Archive history - End   
   public static void initLogger()
   {
     if(!AIPPreferences.isEnableLog())return ;  
      String logPath ;
      String logPath1="";
      logPath =AIPPreferences.getLogFile();
      if(logPath == null || logPath.equals(""))
      {    
            String home =AIPUtil.findHome();      
            /**CV2019 merges from CV10.2 line**/
            if(home ==null || home.equals("")){
                    logPath =".AIPLog.log";
                    logPath1=".AIPIdleLog.log";
            }
            else {
                    logPath =home +AIPUtil.pathSep+"Log"+AIPUtil.pathSep+"AIPLog.log";  
                    logPath1 =home +AIPUtil.pathSep+"Log"+AIPUtil.pathSep+"AIPIdleLog.log"; 
            }
      }
      int filesCount =AIPPreferences.getLogFileNo();
      int fileLength =AIPPreferences.getLogFileLength();
      int level =AIPPreferences.getLogLevel();
      boolean bMethodPath =AIPPreferences.isLogAddMethodPath();
      new VWLogger(logPath,level,filesCount,fileLength,bMethodPath);
      if(AIPPreferences.isEnableTimerLog()){
      new VWIdleLogger(logPath1,level,filesCount,fileLength,bMethodPath);
      
      }
       //setAddClassMethodPath(
   }
   public static void stopLogger()
   {
       VWLogger.stopLogger();
       
        //new VWLogger();
   }
   //update 24-07-2004
   public static void addLocationStatus(int sid,LocationStatus loc)
   {
	    VWLogger.info("inside addLocationStatus :"+processlocations);
	    Integer Isid=new Integer(sid);
        if(!processlocations.containsKey(Isid))
            processlocations.put(Isid,new Vector());          
        Vector v=(Vector)processlocations.get(Isid);
        v.add(loc);
        VWLogger.info("Before adding to processLocationsTable sid:"+sid+", loc.getName() :"+loc.getName());
        processLocationsTable.put(loc.getName(), sid);
   }
   
   public static LocationStatus getLocationStatus(String locName)
   {
	   if (!processLocationsTable.containsKey(locName)) return null;
       int Isid  = Integer.parseInt(processLocationsTable.get(locName).toString());
       if(locName == null || !processlocations.containsKey(Isid)) {
	   return null;
       }
       Vector v =(Vector)processlocations.get(Isid);
       for(int i=0;i<v.size();i++)
       {    
	   if(locName.equalsIgnoreCase(v.get(i).toString()))
	       return (LocationStatus)v.get(i);
       }
       return null;
   }
   public static ArrayList getAllLocationStatus()
   {        
        ArrayList result  = new ArrayList();
        Enumeration processList = processlocations.elements();
        while(processList.hasMoreElements()){
            Vector data = (Vector) processList.nextElement();
            result.addAll(data);
        }        
        return result;
   }
   public static LocationStatus getLocationStatus(int sid,String locName)
   {    try{
	   Integer Isid=new Integer(sid);
	   if(locName ==null || !processlocations.containsKey(Isid)) return null;
	   Vector v =(Vector)processlocations.get(Isid);
	   for(int i=0;i<v.size();i++)
	   {    
		   if(locName.equalsIgnoreCase(v.get(i).toString()))
			   return (LocationStatus)v.get(i);
	   }    
	   
   }catch(Exception e){
	   VWLogger.info("Exception while getLocationStatus ::"+e.getMessage());
   }
   return null;
   }

	public static int getLocationStatusOrder(int sid, String locName) {
		try {
			VWLogger.info("processlocations :: " + processlocations);
			Integer Isid = new Integer(sid);
			if (locName == null || !processlocations.containsKey(Isid))
				return -1;
			Vector v = (Vector) processlocations.get(Isid);
			for (int i = 0; i < v.size(); i++) {
				VWLogger.info("processlocations locName :: " + v.get(i).toString());
				if (locName.equalsIgnoreCase(v.get(i).toString()))
					return i;
			}

		} catch (Exception e) {
			VWLogger.info("Exceptiion while getLocationStatusOrder ::" + e.getMessage());
		}
		return -1;
	}

   public static void removeLocationStatus(int sid,String locName)
   {
	   try{
        Integer Isid=new Integer(sid);
        if(locName ==null || !processlocations.containsKey(Isid)) return;
        Vector v =(Vector)processlocations.get(Isid);
        for(int i=0;i<v.size();i++)
        {    
           if(locName.equalsIgnoreCase(v.get(i).toString()))
           {    
                v.remove(i);
                return ;
           }     
        }  
	   }catch(Exception e){
		   VWLogger.info("exception while remove location ::"+e.getMessage());
	   }
   }    
   public static void removeAllLocationsStatus(int sid)
   {
        Integer Isid=new Integer(sid);
        processlocations.put(Isid,new Vector());
   }
   public static Vector getAllLocationsStatus(int sid)
   {
	   /**CV2019 merges from CV10.2 line. If condition added**/
	   if (processlocations != null) {
		   Integer Isid=new Integer(sid);
	       if(!processlocations.containsKey(Isid)) return null;
	       return (Vector)processlocations.get(Isid);
	   } else 
		   return null;
   } 
   public static Vector getEnabledFolders(int sid)
   {
        Session s =getSession(sid);
        if(s ==null) return null;
        return AIPPreferences.getEnabledFolders(s);           
   }    
   public static boolean putEnabledFolders(int sid,Vector folders)
   {
        Session s =getSession(sid);
        if(s ==null) return false;
        return AIPPreferences.putEnabledFolders(s,folders);           
   }    
   public static void setProcessSubdirs(boolean ret)
   {
        AIPPreferences.setProcessSubdirs(ret);
   }
   public static boolean isProcessSubdirs()
   {
       return AIPPreferences.isProcessSubdirs(); 
   }
   public static boolean isCommandPrompt(){
	   return commandPrompt;
   }
   public static void setCommandPrompt(boolean visible){
	   commandPrompt = visible;
   }
   public static void setScheduleTime(String schedule)
   {
        AIPPreferences.setScheduleTime(schedule);
   }    
   public static String getScheduleTime()
   {
        return AIPPreferences.getScheduleTime();
   }
   public static String getScheduleTimeForProcessingPendingDocs()
   {
        return AIPPreferences.getScheduleTimeForProcessingPendingDocs();
   } 

   public static void setPollingMode(int pollingMode)
   {
	   AIPPreferences.setPollingMode(pollingMode);
   }
   public static int getPollinMode()
   {
	   return AIPPreferences.getPollingMode();
   }
   public static void setLastAIPProcessSuccess(boolean success)
   {
        AIPPreferences.setLastAIPProcessSuccess(success);
   }
   public static boolean isLastAIPProcessSuccess()
   {
        return AIPPreferences.isLastAIPProcessSuccess();
   } 
   public static boolean isLoginReTry()
   {
        return AIPPreferences.isLoginReTry();
   } 
   public static int getLocationStatusOrder(String locName)
   {
       if(locName ==null || !processLocationsTable.containsKey(locName)) return -1;
       Enumeration locationList = processLocationsTable.keys();
       int position = 0;
       while (locationList.hasMoreElements()){
	   
	   if (locName.equalsIgnoreCase(locationList.nextElement().toString())) {	       
	       return position;
	   }
	   position++;	   
       }
       return -1;       
   }
   
   public static int getMaxDocSize(){
	   return AIPPreferences.getMaxDocSize();
   }
   /**CV2019 merges from CV10.2 line. Minimum zip size registry reading added. Added by Vanitha S**/
   public static int getMinZipSize(){
	   return AIPPreferences.getMinZipSize();
   }
   public static int getMaxLoginCount(){
	   return AIPPreferences.getMaxLoginCount();
   }
	/**
	 * Registry created to set the time delay while auto start during network delay
	 * @return
	 */
   public static int getAipAutoStartDelayTime(){
	   return AIPPreferences.getAipAutoStartDelayTime();
   }
   public static int setAIPConnectionStatus(int currSid) {
		// TODO Auto-generated method stub
		VWLogger.info("in aipmanager setAIPConnectionStatus"+currSid);
		int retval=0;
		retval=vwc.setAIPConnectionStatus(currSid);
		return retval;
	}
 //End of Issue - 742
   public static void setCompression(int compressionVal)
   {
        AIPPreferences.setCompression(compressionVal);;
        
   } 
   public static int getCompression()
   {
       return  AIPPreferences.getComptession();
    		   
   }  
   /**CV2019 merges from CV10.2 line**/
   public static int getPageSize()
   {
       return  AIPPreferences.getpageSize();
    		   
   }  
   
   
  /* public static void setConversionMode(int conversion)
   {
        AIPPreferences.setConversionMode(conversion);;
        
   } 
   public static int getConversionMode()
   {
       return  AIPPreferences.getConversionMode();
    		   
   }  */
   /**CV2019 merges from CV10.2 line. Added to check whether AIP service is running**/
   public static boolean isApplicationProcessRunning(){
   	boolean status=false;

   	BufferedReader input=null;
   	try{
   		String process;
   		String pInfo="";
   		Process p=Runtime.getRuntime().exec("tasklist");
   		 input=new BufferedReader(new InputStreamReader(p.getInputStream()));
   		while((process=input.readLine())!=null){
   			pInfo+=process;
   			System.out.println(process);
   		}
   		
   		if(pInfo.contains("VWInputProcessor.exe")){
   			VWLogger.info("process is running");
   			status=true;
   		}else{
   			VWLogger.info("process is not running");
   			status=false;
   		}
   	}catch(Exception err){
   		
   	}
   	finally{
   		try {
   			input.close();
   		} catch (Exception e) {
   			// TODO Auto-generated catch block
   			e.printStackTrace();
   		}
   	}
		return status;
   	
   	
   	
   }
   
    public static boolean checkConvertToDJVUFlag(int sessionId) {
		boolean isDJVUEnabled = false; 
		int flag = vwc.checkCustomizationFlagValues(sessionId, "AIPdjvu");
		if (flag == 1)
			isDJVUEnabled = true;
		return isDJVUEnabled;
	}

	public static void setSinglePageHcpdf(int enable) {
		AIPPreferences.setSinglePageHcpdf(enable);
	}
	public static int isSinglePageHcpdf() {
		return AIPPreferences.isSinglePageHcpdf();
	}
	public static int getImageBackupDays() {
		return AIPPreferences.getImageBackupDays();
	}
}
