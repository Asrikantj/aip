package com.computhink.aip;

import java.util.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import com.computhink.aip.util.VWLogger;
import com.computhink.aip.util.LocationStatus;
import com.computhink.aip.util.MutableLocationThread;

import com.computhink.common.Constants;
import com.computhink.common.Document;
import com.computhink.common.DocComment;
import com.computhink.common.DocType;
import com.computhink.common.Index;
import com.computhink.common.Node;
import com.computhink.common.ViewWiseErrors;
import com.computhink.aip.client.*;
/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public class DocumentsManager {

  public static final int COMMENT_CHAR_LIMIT = 3900;  
  private static Hashtable navNodes = new Hashtable(); 
  private static Hashtable lockedNodes =new Hashtable();
  public DocumentsManager() {
  }
  public static void releaseSession(int sid)
  {
     navNodes.remove(new Integer(sid));  
  }   
//---------------VW Nodes  -------------------------------------------
	public static Vector getNavigatorNode(int sid, boolean refresh) {
		Vector NodsData = new Vector();
		/**CV2019 merges from CV10.2 line. Try catch block added. Added by Vanitha S**/
		try {
			if (!refresh && navNodes.containsKey(new Integer(sid))) {
				return (Vector) navNodes.get(new Integer(sid));
			}
			Session s = AIPManager.getSession(sid);
			if (s == null)
				return null;

			Vector Ndata = new Vector();
			Ndata.add(0, new Integer(0));
			VWNodeInfo MNinfo = new VWNodeInfo();
			MNinfo.setNodeId(0);
			MNinfo.setParentId(-1);
			MNinfo.setNodeName(s.server + "." + s.room);
			DefaultMutableTreeNode MainNode = new DefaultMutableTreeNode(MNinfo);
			Ndata.add(1, MainNode);
			NodsData.add(Ndata);
			int ret = getVWNodesTree(sid, MainNode, NodsData);
			if (ret != AIPManager.NOERROR) {

				return null;
			}

			navNodes.put(new Integer(sid), NodsData);
		} catch (Exception e) {
			VWLogger.info("Exception in getNavigatorNode :" + e.getMessage());
		}
		return NodsData;
	}

	private static int getVWNodesTree(int sid, DefaultMutableTreeNode parentNode, Vector allNodes) {
		Vector nodes = new Vector();
		try {
			int nodeId = ((VWNodeInfo) parentNode.getUserObject()).getNodeId();
			Node pNode = new Node(nodeId);
			int ret = AIPManager.getVWC().getNodeContents(sid, pNode, 0, nodes);
			if (ret != AIPManager.NOERROR) {
				if (ret == ViewWiseErrors.ServerNotFound || ret == ViewWiseErrors.ServerNotAvailable
						|| ret == ViewWiseErrors.invalidSessionId) {
					AIPManager.logoutAll();
					AIPManager.shutdown();
					VWLogger.audit(Constants.PRODUCT_NAME + " Server connection lost! Shutting down...");
					System.exit(0);
				}
				return ret;
			}
			for (int i = 0; i < nodes.size(); i++) {
				Node n = (Node) nodes.get(i);
				DefaultMutableTreeNode tNode = new DefaultMutableTreeNode(getNodeInfo(n));
				parentNode.add(tNode);
				Vector Ndata = new Vector();
				Ndata.add(0, new Integer(n.getId()));
				Ndata.add(1, tNode);
				allNodes.addElement(Ndata);
				ret = getVWNodesTree(sid, tNode, allNodes);

			}
		} catch (Exception e) {
			VWLogger.info("Exception in getVWNodesTree :" + e.getMessage());
		}
		return AIPManager.NOERROR;
	}

  private static VWNodeInfo getNodeInfo(Node node)
  {
     VWNodeInfo nodeInfo =new VWNodeInfo();
     nodeInfo.setNodeId(node.getId());
     nodeInfo.setNodeName(node.getName());
     nodeInfo.setParentId(node.getParentId());
     return nodeInfo;    
  }    

	public static DefaultMutableTreeNode getNode(int ID, Vector NodsData) {
		/**CV2019 merges from CV10.2 line. Try catch block added**/
		try {
			for (int i = 0; i < NodsData.size(); i++) {
				Vector Ndata = (Vector) NodsData.get(i);
				if (ID == ((Integer) Ndata.get(0)).intValue())
					return (DefaultMutableTreeNode) Ndata.get(1);
			}
		} catch (Exception e) {
			VWLogger.info("Exception in getNode :" + e.getMessage());
		}
		return new DefaultMutableTreeNode("");
	}
   public static String getNodePath(int sid,int nodeId)
   {
      final String fileSep = System.getProperty("FILE.SEPARATOR");
      Vector vNodes =getNavigatorNode(sid,false);
      DefaultMutableTreeNode node =getNode(nodeId,vNodes);
      if(node ==null)  return null;      
      javax.swing.tree.TreePath treePath = new javax.swing.tree.TreePath(node.getPath());
      
      StringBuffer buffer = new StringBuffer();      
      for (int i = 0; i < treePath.getPathCount(); i++)
          buffer.append(treePath.getPath()[i].toString() + fileSep);
      return buffer.toString();
   }
   public static Session getSessionOfNodePath(String nodePath)
   {
		Session s = null;
		try {
			StringTokenizer sToken = new StringTokenizer(nodePath, "/");
			if (!sToken.hasMoreTokens())
				return s;
			String roomName = sToken.nextToken();
			int ix = roomName.indexOf(".");
			if (ix > -1) {
				String server = roomName.substring(0, ix).trim();
				String room = roomName.substring(ix + 1).trim();
				s = AIPManager.getSession(server, room);
			}
			if (s == null) {
				s = AIPManager.getRoomSession(roomName);

			}
		} catch (Exception e) {
			VWLogger.info("Exception in getSessionOfNodePath ::" + e.getMessage());
		}
		return s;
	}    
   /*
   *  Issue  - AIP Hangs when processing large rooms
   *  Developer - Nebu Alex
   *  Description - This function works same as the function getIdOfNodePath. 
   *				The diffrence is in the implementation logic. Instead of loading
   *				all nodes in to memory and obtain node information from a vector, this 
   *				function makes a data base call when node information is required.
   */ 
   public static int getparentNodeIdofNodePath(LocationStatus locStatus,String nodePath,boolean create)
 {
		VWLogger.debug("Handling path <" + nodePath + "> create <" + create + ">");
		Node currentParentNode;
		String parentid = "0";
		boolean found = false;
		/**CV2019 merges from CV10.2 line. Try catch block added. Added by Vanitha S**/
		try {
			StringTokenizer sToken = new StringTokenizer(nodePath, "/");
			if (!sToken.hasMoreTokens())
				return -1;
			sToken.nextToken();
			String cabinetName = sToken.nextToken().trim();
			Session s = getSessionOfNodePath(nodePath);
			if (s == null)
				return -2;
			int sid = s.sessionId;
			Vector cabinet = new Vector();
			// Patentid, nodename
			String strid = "0";
			int ret = AIPManager.getVWC().getNodeDetails(sid, strid, cabinetName, cabinet);
			if (ret < 0) {
				VWLogger.ver_debug("Cannot Fetch Cabinet Nodes From DataBase");
				return -6;
			}
			if (cabinet.size() <= 0) {
				return -6;
			}
			Node cabinetnode = null;
			// find out cabinet
			int c = 0;

			boolean nodeFound = false;
			for (c = 0; c < cabinet.size(); c++) {
				cabinetnode = (Node) cabinet.get(c);
				if (cabinetnode.getName().equalsIgnoreCase(cabinetName)) {
					nodeFound = true;
					break;
				}
			}
			if (!nodeFound) { // change
				return -6; // Cabinet not found
			}
			parentid = String.valueOf(cabinetnode.getId());
			currentParentNode = cabinetnode;
			cabinetnode = null;
			while (sToken.hasMoreTokens()) {
				found = false;
				String currentNodeName = sToken.nextToken().trim();

				if (locStatus != null && lockedNodes.containsKey(currentParentNode)) {
					Vector waitThreads = (Vector) lockedNodes.get(currentParentNode);

					waitThreads.add(locStatus.getWorkThread());
					((MutableLocationThread) locStatus.getWorkThread()).waitUntilAsk();
				}

				VWLogger.ver_debug("Search for node <" + currentNodeName + ">");
				Vector nodes = new Vector();
				int ret1 = AIPManager.getVWC().getNodeDetails(sid, parentid, currentNodeName, nodes);
				if (nodes.size() <= 0) {
					VWLogger.ver_debug("Create node <" + currentNodeName + ">");
					// create node
					if (create) {
						try {
							lockedNodes.put(currentParentNode, new Vector());
							Node n = new Node(0);
							n.setName(currentNodeName);
							n.setParentId(currentParentNode.getId()); // parentid
							int ret2 = AIPManager.getVWC().createNode(sid, n); // create
																				// node
							if (ret2 > 0 && (AIPManager.currentNode == null))
								AIPManager.currentNode = new Node(ret2, n.getName());
							if (ret2 >= 0) {
								Vector newNode = new Vector();
								AIPManager.getVWC().getNodeDetails(sid, parentid, currentNodeName, newNode);
								currentParentNode = (Node) newNode.get(0);
								newNode = null;
								parentid = String.valueOf(currentParentNode.getId());
								VWLogger.ver_debug("Created Node <" + currentNodeName + "> ");
								found = true;
								continue;
							} else {
								VWLogger.ver_debug("Falied to create Node <" + currentNodeName + "> ");
								found = false;
							}
							// Locked Nodes Logic
							if (lockedNodes.containsKey(currentParentNode)) {

								Vector waitThreads = (Vector) lockedNodes.get(currentParentNode);
								for (int i = 0; i < waitThreads.size(); i++) {
									((MutableLocationThread) waitThreads.get(i)).commonNotify();
								}
								lockedNodes.remove(currentParentNode);
							}
							// Locked Nodes Logic
						} catch (Exception e) {
							VWLogger.info("Exception while creating node in getparentNodeIdofNodePath 1:" + e.getMessage());
						}
					} else {
						return -4;
					}
				} else { // Nodes are present
					int i = 0;
					for (i = 0; i < nodes.size(); i++) {

						if (((Node) nodes.get(i)).getName().equalsIgnoreCase(currentNodeName)) {
							parentid = String.valueOf(((Node) nodes.get(i)).getId());
							currentParentNode = (Node) nodes.get(i);
							VWLogger.ver_debug("Found node <" + currentNodeName + "> with ID: " + parentid);
							found = true;
							break;
						}
					} // end of for loop
					if (i == nodes.size()) {// Node not found
						// Create Node
						if (create) {
							try {
								Node n = new Node(0);
								n.setName(currentNodeName);
								n.setParentId(currentParentNode.getId());
								int ret2 = AIPManager.getVWC().createNode(sid, n);
								if (ret2 >= 0) {
									Vector newNode = new Vector();
									AIPManager.getVWC().getNodeDetails(sid, parentid, currentNodeName, newNode);
									currentParentNode = (Node) newNode.get(0);
									parentid = String.valueOf(currentParentNode.getId());
									VWLogger.ver_debug("Created Node <" + currentNodeName + "> ");
									newNode = null;
									found = true;
									continue;
								} else {
									found = false;
									VWLogger.ver_debug("Falied to create Node <" + currentNodeName + "> ");
								}
								if (lockedNodes.containsKey(currentParentNode)) {
									Vector waitThreads = (Vector) lockedNodes.get(currentParentNode);
									for (int j = 0; j < waitThreads.size(); j++) {
										((MutableLocationThread) waitThreads.get(j)).commonNotify();
									}
									lockedNodes.remove(currentParentNode);
								}
							} catch (Exception e) {
								VWLogger.info("Exception while creating node in getparentNodeIdofNodePath :" + e.getMessage());
							}
						} else {
							return -4;
						}
					}
				} // end of else loop

			} // end of while loop
		} catch (Exception e) {
			VWLogger.info("Exception in getparentNodeIdofNodePath :" + e.getMessage());
		}

		if (found) {
			VWLogger.debug("Found the id of node path :" + parentid);
			return Integer.parseInt(parentid);
		} else
			return -1;
	}
   // End of code change
	public static int getIdOfNodePath(LocationStatus locStatus, String nodePath, boolean create) {
		VWLogger.debug("Handling path <" + nodePath + "> create <" + create + ">");
		boolean found = false;
		DefaultMutableTreeNode currentParentNode = null;
		/**CV2019 merges from CV10.2 line. Try catch block added. Added by Vanitha S**/
		try {
			int treeLevel = 1;
			StringTokenizer sToken = new StringTokenizer(nodePath, "/");
			if (!sToken.hasMoreTokens())
				return -1;
			String currentNodeName = sToken.nextToken().trim();
			Session s = getSessionOfNodePath(nodePath);
			if (s == null)
				return -2;
			int sid = s.sessionId;
			Vector vNodes = getNavigatorNode(sid, false);
			currentParentNode = getNode(0, vNodes);
			int level = 1;
			while (sToken.hasMoreTokens()) {
				try {
					currentNodeName = sToken.nextToken().trim();
					VWLogger.ver_debug("search for node <" + currentNodeName + ">");
					if (locStatus != null && lockedNodes.containsKey(currentParentNode)) {
						Vector waitThreads = (Vector) lockedNodes.get(currentParentNode);

						waitThreads.add(locStatus.getWorkThread());
						((MutableLocationThread) locStatus.getWorkThread()).waitUntilAsk();
					}
					level++;
					int cont = currentParentNode.getChildCount();
					found = false;
					for (int i = 0; i < cont; i++) {
						String nodeName = currentParentNode.getChildAt(i).toString();
						if (nodeName.equals(currentNodeName)) {
							currentParentNode = (DefaultMutableTreeNode) currentParentNode.getChildAt(i);
							found = true;
							int nodeId = ((VWNodeInfo) currentParentNode.getUserObject()).getNodeId();
							VWLogger.ver_debug("found node <" + currentNodeName + "> with ID: " + nodeId);
							break;
						}
					}
					if (!found) {
						if (level >= 3) {
							if (create) {
								DefaultMutableTreeNode dNode = createNode(sid, currentParentNode, currentNodeName,
										vNodes);
								if (dNode != null) {
									currentParentNode = dNode;
									found = true;
									continue;
								}
							}
							return -4;
						} else
							return -6;
					}
				} catch (Exception e) {
					VWLogger.info("Exception while creating node in getIdOfNodePath :" + e.getMessage());
				}

			}
		} catch (Exception e) {
			VWLogger.info("Exception in getIdOfNodePath :" + e.getMessage());
		}
		if (found) {
			VWLogger.debug(
					"found the id of node path :" + ((VWNodeInfo) currentParentNode.getUserObject()).getNodeId());
			return ((VWNodeInfo) currentParentNode.getUserObject()).getNodeId();
		} else
			return -1;

	}

	public static DefaultMutableTreeNode createNode(int sid, DefaultMutableTreeNode parentNode, String nodeName,
			Vector cachNodes) {
		DefaultMutableTreeNode tNode = null;
		try {
			lockedNodes.put(parentNode, new Vector());
			Node n = new Node(0);
			n.setName(nodeName);
			n.setParentId(((VWNodeInfo) parentNode.getUserObject()).getNodeId());
			int ret = AIPManager.getVWC().createNode(sid, n);
			if (ret > 0) {
				VWLogger.ver_debug("create new Node node <" + nodeName + "> with ID: " + ret);
				tNode = new DefaultMutableTreeNode(getNodeInfo(n));
				parentNode.add(tNode);
				Vector Ndata = new Vector();
				int id = ((VWNodeInfo) tNode.getUserObject()).getNodeId();
				Ndata.add(0, new Integer(id));
				Ndata.add(1, tNode);
				cachNodes.add(Ndata);

			} else {
				VWLogger.error("Failed to create node (" + nodeName + ")->" + AIPManager.getErrorDescription(ret) + "("
						+ ret + ")", null);
				if (ret == ViewWiseErrors.ServerNotFound || ret == ViewWiseErrors.ServerNotAvailable
						|| ret == ViewWiseErrors.invalidSessionId) {
					AIPManager.logoutAll();
					AIPManager.shutdown();
					VWLogger.audit(Constants.PRODUCT_NAME + " Server connection lost! Shutting down...");
					System.exit(0);
				}

			}
			if (lockedNodes.containsKey(parentNode)) {
				Vector waitThreads = (Vector) lockedNodes.get(parentNode);
				for (int i = 0; i < waitThreads.size(); i++) {
					((MutableLocationThread) waitThreads.get(i)).commonNotify();

				}

				lockedNodes.remove(parentNode);
			}
		} catch (Exception e) {
			VWLogger.info("Exception in createNode : " + e.getMessage());
		}
		return tNode;
	}
   public static boolean isNodeidExists(int sid,int Nodeid)
   {
        Vector nodes =getNavigatorNode(sid,false);
        if(getNode(Nodeid,nodes) !=null) return true;
        return false;
   }
   public static int calcDepth(String VWNodePath)
   {
      StringTokenizer sTokens = new StringTokenizer(VWNodePath, "/");
      int i = 0;
      while (sTokens.hasMoreTokens())
      {
          i++;
          sTokens.nextToken();
      }
      //if(Log.LOG_DEBUG) Log.debug("Tree Depth for VW Path: " + VWNodePath);
      return i;
   }
  
  public static String loadComment(String commentFile)
  {
      String comment =null;
      try
       {

           if (commentFile.trim().equals("") || commentFile.equals("<!NF!>"))               
               return comment;
           java.io.File f =new java.io.File(commentFile);
           if(!f.exists() || f.isDirectory())
           {
                VWLogger.error("invalid comment file path <"+commentFile+">",null);
                return comment;
           }    
           /**/
           BufferedReader commentFR = new BufferedReader(new FileReader(commentFile));
           StringBuffer commentBuffer = new StringBuffer();
           int dummy;
           while ((dummy = commentFR.read()) != -1)
           {
               commentBuffer.append((char)dummy);
           }
           comment = new String(commentBuffer);
           if(comment.length() > COMMENT_CHAR_LIMIT )
           {  
              
              comment =comment.substring(0, COMMENT_CHAR_LIMIT);
              
           }
           commentFR.close();
       }
       catch (IOException ioe)
       {
           VWLogger.error(" couldn't read comment file! ", ioe);
         
       }
       return comment; 
  }    
  public static int setDocumentComment(int sid,Document doc,String comment)
  {
      DocComment docComment =new DocComment();
      docComment.setId(0);
      docComment.setDocId(doc.getId());
      docComment.setPublic(true);
      docComment.setComment(comment);
      return AIPManager.getVWC().setDocumentComment(sid,docComment);      
  }
  
  
  
  
  /* added by amaleh */
  /**
   * Takes a room ID and document indices and returns the document ID
   */
  // update Nishad 
  public static int getIDofDoc(int sid, Document baseDoc)
  {
	  int docTypeId = baseDoc.getDocType().getId();
	  VWLogger.info("Document Type Id ----->: "+docTypeId);
	  String keyField = "";
	  Vector vecKeyIndexField = new Vector(); 
	  AIPManager.getVWC().getKeyIndexName(sid, docTypeId, vecKeyIndexField);
	  if(vecKeyIndexField!=null && vecKeyIndexField.size()>0){
		  keyField = vecKeyIndexField.get(0).toString();
	  }
	  VWLogger.info("keyField ----->: "+keyField);
	  Vector docs =new Vector();
	  try{
		  String curIndexVal = "";
		  if (baseDoc != null && baseDoc.getDocType().getIndices()!= null && baseDoc.getDocType().getIndices().size()>0) {
			  for (int x=0;x<baseDoc.getDocType().getIndices().size();x++) {
				  Index idx1 = (Index)baseDoc.getDocType().getIndices().get(x);
				  /**
				   * Issue, Append = true, not appending page for the inactive room. 
				   * Code modified from passing the AIPManager.getCurrSid() to sid for the getIndex method.  
				   */
				  Index idx2 = DoctypesManager.getIndex(sid,baseDoc.getDocType().getId(),idx1.getId());
				  if (idx2.getName().trim().equalsIgnoreCase(keyField.trim())) {
					  curIndexVal = idx1.getValue();
					  break;
				  }
			  }
		  }
		  VWLogger.info("Before calling getAIPNodeDocuments :: ParentID:"+baseDoc.getParentId()+", Document Type Id:"+docTypeId+", Current index value:"+curIndexVal);
		  AIPManager.getVWC().getAIPNodeDocuments(sid,baseDoc.getParentId(),0, docTypeId, curIndexVal, docs);
		  VWLogger.info("getAIPNodeDocuments return value ------>:"+ docs);
		  if (docs!=null && docs.size()>0) {
			  VWLogger.info("Size of the document ------>"+ docs.size());
			  for (int i = 0; i < docs.size(); i++) {
				Document doc = (Document) docs.get(i);
				AIPManager.getVWC().getDocumentIndices(sid, doc);
				VWLogger.info("Document Id --------->" + (i + 1) + "  :" + (doc.getId()));
				VWLogger.info("Indices Id --------->" + (i + 1) + " :" + (doc.getDocType().getIndices()));
				if (!indicesEqual(doc.getDocType().getIndices(), baseDoc.getDocType().getIndices(),
						baseDoc.getDocType().getId()))
					continue;
				return doc.getId();
			  }
		  }
	  }catch(Exception e){
		  VWLogger.info("Exception in getIDofDoc :"+ e.getMessage());
	  }
      return -1;
  }
  /**/  
  
  /* added by amaleh on 2004-02-04 */
  /** tests equality of indices between index vectors */
  // update Ayman to be see 
  // comment the compraring not always correct 
  //   so sort of indices the value added automatically
  // like sequence ,current date
  public static boolean indicesEqual(Vector v1, Vector bv2,int dtId)
  {
	  VWLogger.info("Document indices size in DB :"+v1.size());
	  VWLogger.info("Document indices size in processing location :"+bv2.size());
      if (v1.size() != bv2.size()) {
    	  VWLogger.info("Document indices size mismatch found......");
          return false;
      }
      try {
	      for (int i = 0; i < v1.size(); i++)
	      {
	          boolean found =false;
	          Index v1Index =(Index)v1.get(i);
	          VWLogger.info("Document indexid :: index value from DB ("+i+"):"+v1Index.getId()+" :: "+v1Index.getValue());
	          Index bv2Index =(Index)bv2.get(i);
	          VWLogger.info("Document indexid :: index value from Bif ("+i+"):"+bv2Index.getId()+" :: "+bv2Index.getValue());
	          if(bv2Index.getId() != v1Index.getId()) {
	        	  VWLogger.info("Document index id mismatch found ......");
	              return false;
	          }
	          if(!bv2Index.getValue().trim().equals(v1Index.getValue().trim()))
	          {
	        	  VWLogger.info("inside document index value mismatch condition.......");
	              if(bv2Index.getValue() == null || bv2Index.getValue().trim().equals(""))
	              {  
	            	   VWLogger.info("Before getting the index value for index id "+bv2Index.getId());
	                   Index indx = DoctypesManager.getIndex(AIPManager.getCurrSid(),dtId,bv2Index.getId());
	                   VWLogger.info("indx :"+indx);
					   if (indx != null) {
						   if (indx.getDef() != null && indx.getDef().size() > 0)
	                       {    	                    	   
	                            String defVal =(String)indx.getDef().get(0);
	                            VWLogger.info("default Value  :"+defVal);
	                            if(defVal !=null && v1Index.getValue().trim().equals(defVal.trim())) {
	                                continue;
	                            } else {
	                            	VWLogger.info("Mismatch values :: v1Index.getValue().trim():: "+v1Index.getValue().trim() + "defVal.trim()::"+ defVal.trim());
	                            }
	                       }  
	                   }
	                   return false;
	              } else {
	            	  VWLogger.info("Document index value mismatch found ......");
	            	  return false;
	              }
	          }  
	          // ignore actValue its my be changed in DB
	      }
	  } catch (Exception e) {
		  VWLogger.info("Exception in indicesEqual method :"+e);
	  }
      return true;
  }
  
  

}