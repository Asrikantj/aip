package com.computhink.aip;

import java.io.*;
import java.io.Reader;
import java.awt.*;
import java.awt.event.*;
import java.util.Vector;
import java.util.*;

import com.computhink.aip.*;
import com.computhink.aip.util.AIPUtil;
import com.computhink.aip.util.VWLogger;

import com.computhink.common.DocType;
import com.computhink.common.Index;
import com.computhink.aip.client.*;
/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author amaleh@computhink.com
 * @version 1.0
 */

public class CustomFileManager {
    String filePath;
    File EFile;
    int ScanLine =0;
    int PosStart =-1;
    final String SepChar= new Character((char)9).toString();
    private String FormsFileName="EHForms.EFF";
    // added by amaleh on 2004-01-22
    private String FormsDocsFileExt="EHD";
    // added by amalehDocsFileExt
    private String DocsFileExt="BIF";
    //added by amaleh on 2003-11-25
    private String TempDocsFileExt="BIF.TMP";
    
    /* added by amaleh on 2003-08-05 */
    private Reader reader = null;
    /* added by amaleh on 2003-09-19 */
    /** a string containing the complete BIF file data */
    private String fileString;
    /**
     * a byte array that is used to hold a document block from the BIF file.
     * It is usable through an input stream.
     */
    private byte[] stringByteArray;
    /** the current position in the fileString */
    private int bytePos = 0;
    /** the current block index (document number) */
    private int docNumber = 0;
    /**/
    private BIFShadowFileManager shadowFileManager = new BIFShadowFileManager();
        
    
    public CustomFileManager() {
        
    }
    public void setFormsFileName(String Name){
        if(Name !=null && !Name.equals(" "))
            FormsFileName=Name;
    }
    //added on 2004-01-22
    public void setFormsDocsFileExt(String EXT){
        if(EXT !=null && !EXT.equals(" "))
            FormsDocsFileExt =EXT;
    }
    public void setDocsFileExt(String EXT){
        if(EXT !=null && !EXT.equals(" "))
            DocsFileExt =EXT;
    }
    //added on 2004-01-22
    public void setTempDocsFileExt(String EXT){
        if(EXT !=null && !EXT.equals(" "))
            TempDocsFileExt =EXT;
    }
    public  File[] getEHDocumentsFiles(String FolderPath){
        Vector VFiles =new Vector();
        
        File Folder = new File(FolderPath);
        
        if(Folder.exists() && Folder.isDirectory()){
            return  Folder.listFiles(new EHFilesFilter());
        }
        return null;
    }
    /* added by amaleh on 2003-12-11 */
    /**
     * returns shadow files
     */
    public  File[] getEHDocumentsShadowFiles(String FolderPath){
        File Folder = new File(FolderPath);
        
        if(Folder.exists() && Folder.isDirectory()){
            return  Folder.listFiles(new EHShadowFilesFilter());
        }
        return null;
    }
    /**/
    public File getEHFormsFile(String FolderPath){
        //if(Log.LOG_VERBOSE_DEBUG) Log.debug("get EHForms File in the Folder -----:"+FolderPath);
        String FilePath = FolderPath+System.getProperty("file.separator")+FormsFileName;
        
        File FFile = new File(FilePath);
        if(FFile.exists() && FFile.isFile()){
            return  FFile;
        }
       // if(Log.LOG_DEBUG) Log.debug("Error :EHForms File not found in the Folder -----:"+FolderPath);
        return null;
    }
    public File GetUsedFile()
    {
        return EFile;
    }
    public void SetUsedFile(File EHFile){
        EFile =EHFile;
        /* added by amaleh on 2003-08-05 
        try {
            BufferedReader br = new BufferedReader(new FileReader(EFile));
            StringBuffer sb = new StringBuffer();
            int dummy = 0;
            while ((dummy = br.read()) != -1)
                sb.append((char)dummy);
            fileString = new String(sb);
            stringByteArray = null;
            refreshReader(0);
        }
        catch (Exception e) {
            if(Log.LOG_DEBUG) Log.debug("Exception in reading file: " + e);
        }
        /**/
        
    }
    /* added by amaleh on 2003-08-06 */
    /* modified by amaleh on 2003-09-19 */
    /**
     * Refreshes the InputStreamReader to be used in LineNumbeif(Log.LOG_DEBUG) Log.debugReader within
     * getValue and findKey
     */
    public boolean refreshReader(int docNum) {
        try {
            if (reader != null)
                reader.close();
            // if supplied docNum is smaller than current docNumber, reset.
            if (docNum < docNumber) {
                bytePos = 0;
                docNumber = 0;
            }
            // if document is different and stringByteArray is empty, then
            // repopulate the stringByteArray with a new block of document data.
            if (docNum != docNumber || stringByteArray == null) {
                //every block begins with "document#=". Search for them at the
                //beginning of every line. If you find them, end the block.
                String det = "document" + Integer.toString(docNum+1) + "=";
                StringBuffer sb = new StringBuffer();
                int sbcurlength = 0;
                for (int i = bytePos; i < fileString.length(); i++) {
                    sb.append(fileString.charAt(i));
                    if (fileString.charAt(i) == '\n') {
                        sbcurlength = sb.length();
                    }
                    if ((sb.length() - sbcurlength) == det.length()) {
                        String comp = sb.substring(sb.length() - det.length(), sb.length());
                        if (comp.equalsIgnoreCase(det)) {
                            bytePos = i - det.length();
                            break;
                        }
                    }
                }
                String s = sb.toString();
                if (docNum > 0)
                    s = s.substring(1);
                stringByteArray = s.getBytes();
                docNumber = docNum;
            }
            InputStream is = new ByteArrayInputStream(stringByteArray);
            reader = new InputStreamReader(is);
            return true;
        }
        catch(IOException ioe) {
            // Log.error("Refreshing of Reader failed!",ioe);
            return false;
        }
    }
    /**/
    
    public boolean isEHFile(File F,String Type){
        if(F != null){
            SetUsedFile(F);
            //disabled by amaleh on 2003-12-03
            //if(getMainkeysPos(Type))
                return true;
        }
        return false;
    }
  /*
  public String getValue(String key,int beginLine,int EndLine){
     //  Log.debug("get value of key <"+key+"> in file <"+EFile.getName()+"> between ("+beginLine+":"+EndLine+")" );
      try{
          if(Log.LOG_DEBUG) Log.debug("beginLine: " + beginLine);
          if(Log.LOG_DEBUG) Log.debug("EndLine: " + EndLine);
          /* modified by amaleh on 2003-08-05
         //LineNumberReader Lread =new LineNumberReader(new FileReader(EFile));
          refreshReader();
          LineNumberReader Lread =new LineNumberReader(reader);
          /*
         String line;
         while ((line =Lread.readLine()) !=null){
             if(Log.LOG_DEBUG) Log.debug("Line: " + line);
              line = line.trim();
              if(Lread.getLineNumber() < beginLine  ) continue;
              if (line.startsWith("#") || line.length() == 0) continue;
              if(line.indexOf(key) > -1 ){
                  ScanLine = Lread.getLineNumber();
                  int EqualPos = line.indexOf("=");
                  if( EqualPos != -1){
                     //  Log.debug("find at line ("+ScanLine+")"+" value :"+ line.substring(EqualPos+1).trim());
                     String Value = line.substring(EqualPos+1).trim();
                     if(!Value.equals("")) return Value;
                     else return " ";
                  }
                   Log.debug("Error :Can not find value of key <"+key+"> in file <"+EFile.getName()+"> between ("+beginLine+":"+EndLine+")" );
                  return " ";
              }
              if(EndLine !=0 && Lread.getLineNumber() == EndLine) break;
        }
     }
     catch (IOException e) {
            Log.error("Error :Can not find value",e);
            return "<!NF!>";
     }
     catch (SecurityException se) {
           Log.error("Error :Can not find value",se);
           return "<!NF!>";
     }
     return "<!NF!>";
  }
   */
    public String getValue(String key,int docNum,int beginLine,int EndLine){
        //  Log.debug("get value of key <"+key+"> in file <"+EFile.getName()+"> between ("+beginLine+":"+EndLine+")" );
        try{
            /* modified by amaleh on 2003-08-05 */
            //LineNumberReader Lread =new LineNumberReader(new FileReader(EFile));
            //bring up the proper block to extract the value from.
            refreshReader(docNum);
            LineNumberReader Lread =new LineNumberReader(reader);
            /**/
            String line;
            while ((line =Lread.readLine()) !=null){
                line = line.trim();
                //ignore line number checks
                //if(Lread.getLineNumber() < beginLine  ) continue;
                if (line.startsWith("#") || line.length() == 0) continue;
                if(line.indexOf(key) > -1 ){
                    ScanLine = Lread.getLineNumber();
                    int EqualPos = line.indexOf("=");
                    if( EqualPos != -1){
                        //  Log.debug("find at line ("+ScanLine+")"+" value :"+ line.substring(EqualPos+1).trim());
                        String Value = line.substring(EqualPos+1).trim();
                        if(!Value.equals("")) return Value;
                        else return " ";
                    }
                    // Log.debug("Error :Can not find value of key <"+key+"> in file <"+EFile.getName()+"> between ("+beginLine+":"+EndLine+")" );
                    return " ";
                }
                //if(EndLine !=0 && Lread.getLineNumber() == EndLine) break;
            }
        }
        catch (IOException e) {
           // Log.error("Error :Can not find value",e);
            return "<!NF!>";
        }
        catch (SecurityException se) {
           //  Log.error("Error :Can not find value",se);
            return "<!NF!>";
        }
        return "<!NF!>";
    }
  /*
  public boolean Findkey(String key,int beginLine,int EndLine){
      //uncommented by amaleh on 2003-08-06
      if(Log.LOG_VERBOSE_DEBUG) Log.debug("Find key <"+key+"> in file <"+EFile.getName()+"> between ("+beginLine+":"+EndLine+")" );
      try{
          /* modified by amaleh on 2003-08-05
         //LineNumberReader Lread =new LineNumberReader(new FileReader(EFile));
          refreshReader(0);
          LineNumberReader Lread =new LineNumberReader(reader);
          /*
         String line;
         while ((line =Lread.readLine()) !=null){
              line = line.trim();
              if(Lread.getLineNumber() < beginLine  ) continue;
              if (line.startsWith("#") || line.length() == 0) continue;
              if(line.indexOf(key) > -1 ){
                  ScanLine = Lread.getLineNumber();
                 //  if(Log.LOG_VERBOSE_DEBUG) Log.debug("find at line ("+ScanLine+")"+" Key :"+ key);
                     return true;
              }
              if(EndLine !=0 && Lread.getLineNumber() == EndLine){
                 if(Log.LOG_DEBUG) Log.debug("Error :Can not find key <"+key+"> in file <"+EFile.getName()+"> between ("+beginLine+":"+EndLine+")" );
                return false;
   
              }
        }
     }
     catch (IOException e) {
           if(Log.LOG_ERROR) Log.error("Error :Can not find Key",e);
            return false;
     }
     catch (SecurityException se) {
          if(Log.LOG_ERROR) Log.error("Error :Can not find Key",se);
           return false;
     }
     catch (Exception e)
     {
         if(Log.LOG_ERROR) Log.error("Error :",e);
     }
     return false;
  }
   */
    public boolean Findkey(String key,int docNum, int beginLine,int EndLine){
        //uncommented by amaleh on 2003-08-06
        // Log.debug("Find key <"+key+"> in file <"+EFile.getName()+"> between ("+beginLine+":"+EndLine+")" );
        try{
            /* modified by amaleh on 2003-08-05 */
            //LineNumberReader Lread =new LineNumberReader(new FileReader(EFile));
            //bring up the proper block to extract the value from.
            refreshReader(docNum);
            LineNumberReader Lread =new LineNumberReader(reader);
            /**/
            String line;
            while ((line =Lread.readLine()) !=null){
                line = line.trim();
                //ignore line number checks.
                //if(Lread.getLineNumber() < beginLine  ) continue;
                if (line.startsWith("#") || line.length() == 0) continue;
                if(line.indexOf(key) > -1 ){
                    ScanLine = Lread.getLineNumber();
                    //  if(Log.LOG_VERBOSE_DEBUG) Log.debug("find at line ("+ScanLine+")"+" Key :"+ key);
                    return true;
                }
              /*
              if(EndLine !=0 && Lread.getLineNumber() == EndLine){
                 if(Log.LOG_DEBUG) Log.debug("Error :Can not find key <"+key+"> in file <"+EFile.getName()+"> between ("+beginLine+":"+EndLine+")" );
                return false;
               
              }
               */
            }
        }
        catch (IOException e) {
           // if(Log.LOG_ERROR) Log.error("Error :Can not find Key",e);
            return false;
        }
        catch (SecurityException se) {
            // Log.error("Error :Can not find Key",se);
            return false;
        }
        catch (Exception e) {
            //Log.error("Error :",e);
        }
        return false;
    }
    
    public boolean getMainkeysPos(String Type){
        if(Type == "forms"){
            if(!Findkey("[E&H Forms File]",0,1,0)){
                return false;
            }else
                if(Findkey("[forms]",0,1,0))  PosStart = ScanLine;
                else return false;
            
        }else if(Type =="docs"){
            if(!Findkey("[Documents File]",0,1,0)){
                return false;
            }else
                if(Findkey("[documents]",0,1,0)) PosStart = ScanLine;
                else return false;
            
        }
        return true;
    }
    public Vector getNamesOf(String Type,int StartLine,int EndLine){
        Vector Names = new Vector();
        if(PosStart > 0) {
            String SNamesCount = getValue("count",0,StartLine,EndLine);
            if(!SNamesCount.equals("<!NF!>")){
                for(int i=0;i<Integer.parseInt(SNamesCount);i++){
                    String NameKey  =Type+Integer.toString(i+1);
                    String value =getValue(NameKey,i+1,StartLine,EndLine);
                    if(! value.equalsIgnoreCase("<!NF!>") ){
                        StartLine = ScanLine;
                        Vector Nm =new Vector() ;
                        Nm.add(0,value);
                        Nm.add(1,new Integer(StartLine));
                        Names.addElement(Nm);
                    }
                }
                
            }
        }
        return Names;
    }
    
    /* redone by amaleh on 2003-06-24 */
  /*
  public Vector getIndicesOf(String Type,int NUM,int StartLine,int EndLine){
      int i=0;
      Vector NameInds =new Vector();
      while(EndLine ==0 || StartLine < EndLine){
          String NmFieldKey  =Type+Integer.toString(NUM)+".field"+Integer.toString(i+1);
          String NmFieldData =getValue(NmFieldKey,StartLine,EndLine);
   
          if(!NmFieldData.equalsIgnoreCase("<!NF!>")){
              if(Type == "f"){
                    EHFormFieldInfo EHFInfo =new EHFormFieldInfo();
                     StringTokenizer Stoken =new StringTokenizer(NmFieldData,SepChar);
                   if(Stoken.hasMoreTokens()) EHFInfo.setEHFieldName(Stoken.nextToken().trim());
                   if(Stoken.hasMoreTokens()) EHFInfo.setType(Integer.parseInt(Stoken.nextToken().trim()));
                     EHFInfo.setFieldOrder(i);
                     NameInds.addElement(EHFInfo);
              }else if(Type == "doc"){
                    EHDocFieldInfo FieldInfo =new EHDocFieldInfo();
                    FieldInfo.setFieldid(0);
                    FieldInfo.setFieldOrder(i);
                    FieldInfo.setValue(NmFieldData);
                    FieldInfo.setStatus(FieldInfo.ACCEPTED);
                    NameInds.addElement(FieldInfo);
              }else  NameInds.addElement(NmFieldData);
              i++;
              StartLine = ScanLine;
          }
          else break;
      }
      return NameInds;
  }
   */
    
    /**/
    public Vector getIndicesOf(String Type,int NUM,int StartLine,int EndLine, Vector params){
        int i=0;
        Vector NameInds =new Vector();
        /* added by amaleh on 2003-06-24 */
        Vector indexNamesInOrder = new Vector();
        Vector indexNames = new Vector();
        Vector indexOrder = new Vector();
        if (Type == DocsFileExt) {
            // first get index names in the proper order from the doctype
            String formName = (String)(params.get(0));
            DoctypesManager DTManager = new DoctypesManager();
            int formID = DTManager.getIDofDoctype(AIPManager.getCurrSid(), formName);
            if (formID < 0)
                return new Vector();
            DocType dt =DTManager.getDoctype(AIPManager.getCurrSid(),formID);
            indexNamesInOrder = dt.getIndices();
            // second get index names specified in the BIF file
            String fieldKey = "doc"+Integer.toString(NUM)+"." + "fields";
            String fieldData =getValue(fieldKey,NUM,StartLine,EndLine);
            StringTokenizer indicesTokens = new StringTokenizer(fieldData, "|");
            while (indicesTokens.hasMoreTokens())
                indexNames.addElement(indicesTokens.nextToken());
            indexOrder = getOrderTransition(indexNamesInOrder, indexNames);
        }
        /**/
        while(EndLine ==0 || StartLine < EndLine){
            String NmFieldKey = new String();
            /* added by amaleh on 2003-06-24 */
          /*
          if (Type == DocsFileExt)
          {
              String indexName = ((DoctypeIndicesInfo)(indexNamesInOrder.get(i))).getIndexName();
              NmFieldKey="doc"+Integer.toString(NUM)+"." + indexName;
          }
          else
           */
            /**/
            if (Type == DocsFileExt)
                NmFieldKey="doc"+Integer.toString(NUM)+".field"+Integer.toString(i+1);
            else
                NmFieldKey=Type+Integer.toString(NUM)+".field"+Integer.toString(i+1);
            String NmFieldData =getValue(NmFieldKey,NUM,StartLine,EndLine);
            if(!NmFieldData.equalsIgnoreCase("<!NF!>")){
                if(Type == "f"){
                    AIPField fField =new AIPField();
                    StringTokenizer Stoken =new StringTokenizer(NmFieldData,SepChar);
                    if(Stoken.hasMoreTokens()) fField.setName(Stoken.nextToken().trim());
                    if(Stoken.hasMoreTokens()) fField.setType(Integer.parseInt(Stoken.nextToken().trim()));
                    fField.setOrder(i);
                    NameInds.addElement(fField);
                }else if(Type == "doc" || Type == DocsFileExt){
                    AIPField docField =new AIPField();
                    docField.setId(0);
                    docField.setOrder(i);
                    docField.setValue(NmFieldData);
                    docField.setStatus(docField.ACCEPTED);
                    NameInds.addElement(docField);
                }else  NameInds.addElement(NmFieldData);
                i++;
                /* added by amaleh on 2003-06-24 */
              /* In case "bif" format was used, if i gets bigger than the
               * max number of indices, break the loop before index gets
               * out of bounds.
               */
                if (Type == DocsFileExt) {
                    if (i >= indexNames.size())
                        break;
                }
                else
                    /**/
                    StartLine = ScanLine;
            }
            else {
                // added for fault tolerance
                AIPField docField =new AIPField();
                docField.setId(0);
                docField.setOrder(i);
                docField.setValue("");
                docField.setStatus(docField.ACCEPTED);
                NameInds.addElement(docField);
            }
        }
        if (Type == DocsFileExt)
            NameInds = reorder(NameInds, indexOrder);
        return NameInds;
    }
    
    /* added by amaleh on 2003-07-30 */
    /* loads indices for a specific document */
    public void loadIndicesFor(AIPDocument aipDocInfo, String Type,int NUM,int StartLine,int EndLine, Vector params) {
        int i=0;
        Vector NameInds =new Vector();
        /* added by amaleh on 2003-06-24 */
        Vector indexNamesInOrder = new Vector();
        Vector indexNames = new Vector();
        Vector indexOrder = new Vector();
        if (Type == DocsFileExt) {
          /* 2003-08-15
          // first get index names in the proper order from the doctype
          String formName = (String)(params.get(0));
          DoctypesManager DTManager = new DoctypesManager();
          int formID = DTManager.getIDofDoctype(AIPManager.getCurrSid(), formName);
          if (formID < 0)
              return; // do nothing if form is unrecognized.
          indexNamesInOrder = DTManager.getDoctypeIndices(formID, AIPManager.getCurrSid());
           */
            // second get index names specified in the BIF file
            String fieldKey = "doc"+Integer.toString(NUM)+"." + "fields";
            String fieldData =getValue(fieldKey,NUM,StartLine,EndLine);
            //set fieldNames variable value
            aipDocInfo.setFieldNames(fieldData);
            StringTokenizer indicesTokens = new StringTokenizer(fieldData, "|");
            while (indicesTokens.hasMoreTokens())
                indexNames.addElement(indicesTokens.nextToken());
          /* 2003-08-15
          indexOrder = getOrderTransition(indexNamesInOrder, indexNames);
           */
        }
        /**/
        // commented by amaleh on 2003-09-19
        //while(EndLine ==0 || StartLine < EndLine){
        while(true){
            String NmFieldKey = new String();
            /* added by amaleh on 2003-06-24 */
          /*
          if (Type == DocsFileExt)
          {
              String indexName = ((DoctypeIndicesInfo)(indexNamesInOrder.get(i))).getIndexName();
              NmFieldKey="doc"+Integer.toString(NUM)+"." + indexName;
          }
          else
           */
            /**/
            if (Type == DocsFileExt)
                NmFieldKey="doc"+Integer.toString(NUM)+".field"+Integer.toString(i+1);
            else
                NmFieldKey=Type+Integer.toString(NUM)+".field"+Integer.toString(i+1);
            String NmFieldData =getValue(NmFieldKey,NUM,StartLine,EndLine);
            if(!NmFieldData.equalsIgnoreCase("<!NF!>")){
                if(Type == "f"){
                    AIPField fField =new AIPField();
                    StringTokenizer Stoken =new StringTokenizer(NmFieldData,SepChar);
                    if(Stoken.hasMoreTokens()) fField.setName(Stoken.nextToken().trim());
                    if(Stoken.hasMoreTokens()) fField.setType(Integer.parseInt(Stoken.nextToken().trim()));
                    fField.setOrder(i);
                    NameInds.addElement(fField);
                }else if(Type == "doc" || Type == DocsFileExt){
                    AIPField docField =new AIPField();
                    docField.setId(0);
                    //FieldInfo.setFieldOrder(((Integer)(indexOrder.elementAt(i))).intValue());
                    docField.setOrder(i);
                    docField.setValue(NmFieldData);
                    docField.setStatus(AIPField.ACCEPTED);
                    NameInds.addElement(docField);
                }else  NameInds.addElement(NmFieldData);
                i++;
                /* added by amaleh on 2003-06-24 */
              /* In case "bif" format was used, if i gets bigger than the
               * max number of indices, break the loop before index gets
               * out of bounds.
               */
                if (Type == DocsFileExt) {
                    if (i >= indexNames.size())
                        break;
                }
                else
                    /**/
                    StartLine = ScanLine;
            }
            else {
                // added for fault tolerance
                AIPField docField =new AIPField();
                docField.setId(0);
                docField.setOrder(i);
                docField.setValue("");
                docField.setStatus(AIPField.ACCEPTED);
                NameInds.addElement(docField);
            }
        }
        // commented by amaleh on 2003-08-01
      /*
      if (Type == "bif")
          NameInds = reorder(NameInds, indexOrder);
       */
        aipDocInfo.getAIPForm().setFields(NameInds);
    }
    /**/
    
    /* created by amaleh on 2003-12-02 */
    public void loadIndicesFor(AIPDocument aipDocInfo, String Type,TreeMap parsedMap,Vector params) {
        String segNum = (String)(parsedMap.get("segment#"));
        String num = (String)(parsedMap.get("segment" + segNum + ".doc_number"));
        int fieldCount = Integer.parseInt((String)(parsedMap.get("doc" + num + ".field_count")));
        Vector NameInds =new Vector();
        /* added by amaleh on 2003-06-24 */
        //Vector indexNamesInOrder = new Vector();
        //Vector indexNames = new Vector();
        Vector indexStatuses = new Vector();
        Vector indexOrder = new Vector();
        if (Type == DocsFileExt) {
          /* 2003-08-15
          // first get index names in the proper order from the doctype
          String formName = (String)(params.get(0));
          DoctypesManager DTManager = new DoctypesManager();
          int formID = DTManager.getIDofDoctype(AIPManager.getCurrSid(), formName);
          if (formID < 0)
              return; // do nothing if form is unrecognized.
          indexNamesInOrder = DTManager.getDoctypeIndices(formID, AIPManager.getCurrSid());
           */
            String fieldsKey = "doc"+num+"." + "fields";
            String fieldsData =(String)(parsedMap.get(fieldsKey));
            StringTokenizer indicesTokens = new StringTokenizer(fieldsData, "|");
            StringBuffer fieldsBuffer = new StringBuffer();
            while (indicesTokens.hasMoreTokens())
            {
                String indexName = indicesTokens.nextToken();
                String indexStatus = Integer.toString(AIPField.ACCEPTED);
                StringTokenizer indexNameTokens = new StringTokenizer(indexName, "~");
                if (indexNameTokens.hasMoreTokens())
                    indexName = indexNameTokens.nextToken();
                if (indexNameTokens.hasMoreTokens())
                    indexStatus = indexNameTokens.nextToken();
                //indexNames.addElement(indexName);
                if (fieldsBuffer.length() == 0)
                    fieldsBuffer.append(indexName);
                else
                    fieldsBuffer.append("|" + indexName);
                indexStatuses.addElement(indexStatus);
            }
            
            // second get index names specified in the BIF file
            //field data is optional if a field was to be left empty.
            if (fieldsData == null)
                fieldsData = "";
            //set fieldNames variable value
            aipDocInfo.setFieldNames(fieldsBuffer.toString());
          /* 2003-08-15
          indexOrder = getOrderTransition(indexNamesInOrder, indexNames);
           */
        }
        /**/
        // commented by amaleh on 2003-09-19
        //while(EndLine ==0 || StartLine < EndLine){
        for (int i = 0; i < fieldCount; i++){
            String NmFieldKey = new String();
            /* added by amaleh on 2003-06-24 */
          /*
          if (Type == "bif")
          {
              String indexName = ((DoctypeIndicesInfo)(indexNamesInOrder.get(i))).getIndexName();
              NmFieldKey="doc"+Integer.toString(num)+"." + indexName;
          }
          else
           */
            /**/
            String NmFieldData;
            if (Type == DocsFileExt)
                NmFieldKey="doc"+num+".field"+Integer.toString(i+1);
            else
                NmFieldKey=Type+num+".field"+Integer.toString(i+1);
            NmFieldData =(String)(parsedMap.get(NmFieldKey));
            //if index value is null, fill it with empty string.
            if (NmFieldData == null)
            {
                NmFieldData = "";
            }
            if(!NmFieldData.equalsIgnoreCase("<!NF!>")){
                if(Type == "f"){
                    AIPField formField =new AIPField();
                    StringTokenizer Stoken =new StringTokenizer(NmFieldData,SepChar);
                    if(Stoken.hasMoreTokens()) formField.setName(Stoken.nextToken().trim());
                    if(Stoken.hasMoreTokens()) formField.setType(Integer.parseInt(Stoken.nextToken().trim()));
                    formField.setOrder(i);
                    NameInds.addElement(formField);
                }else if(Type == "doc" || Type == DocsFileExt){
                    AIPField docField =new AIPField();
                    docField.setId(0);
                    
                    docField.setOrder(i);
                    docField.setValue(NmFieldData);
                    docField.setStatus(Integer.parseInt((String)(indexStatuses.get(i))));
                    NameInds.addElement(docField);
                }else  NameInds.addElement(NmFieldData);
            }
            else {
                // added for fault tolerance
                AIPField docField =new AIPField();
                docField.setId(0);
                docField.setOrder(i);
                docField.setValue("");
                docField.setStatus(docField.ACCEPTED);
                NameInds.addElement(docField);
            }
        }
        // commented by amaleh on 2003-08-01
      /*
      if (Type == "bif")
          NameInds = reorder(NameInds, indexOrder);
       */
        aipDocInfo.getAIPForm().setFields(NameInds);
    }
    /**/
    
    public TreeMap getForms(){
       // if(Log.LOG_DEBUG) Log.debug("Get E&H File Forms");
        TreeMap TM = new TreeMap();
        Vector FNs = getNamesOf("form",PosStart,0);
        if(FNs.size() >0){
            for(int i=0;i< FNs.size();i++){
                Vector FN =(Vector)FNs.get(i);
                if(FN.elementAt(0).toString().trim().equals("")) continue;
                int SLine =((Integer)FN.elementAt(1)).intValue();
                int EndLine = 0;
                if(i+1 < FNs.size())
                    EndLine =((Integer)((Vector)FNs.get(i+1)).elementAt(1)).intValue();
                AIPForm FInfo =new AIPForm();
                FInfo.setName(FN.elementAt(0).toString());
                Vector FIndxs = getIndicesOf("f",i+1,SLine,EndLine, null);
                FInfo.setFields(FIndxs);
                TM.put(FN.elementAt(0),FInfo);
            }
        }
        //if(Log.LOG_VERBOSE_DEBUG) Log.debug("<"+TM.size()+"> E&H Form have been extracted");
        return TM;
    }
    
    /* renamed from getDocuments to getDocuments2 by amaleh on 2003-12-01 */
    public TreeMap getDocuments2(){
        //if(Log.LOG_DEBUG) Log.debug("Get EHFile Documents");
        TreeMap TM = new TreeMap();
        Vector DocNs = getNamesOf("document",PosStart,0);
        if(DocNs.size() >0){
            for(int i=0;i< DocNs.size();i++){
                //commented by amaleh on 2003-09-29               
                Vector DocN =(Vector)DocNs.get(i);
                if(DocN.elementAt(0).toString().trim().equals("")) continue;
                int SLine =((Integer)DocN.elementAt(1)).intValue();
                int EndLine = 0;
                
                if(i+1 < DocNs.size())
                    EndLine =((Integer)((Vector)DocNs.get(i+1)).elementAt(1)).intValue();
                AIPDocument aipDocInfo =new AIPDocument();
                aipDocInfo.setName("Document"+Integer.toString(i+1));
                aipDocInfo.setId(0);
                aipDocInfo.setFileId(0);
                aipDocInfo.setStatus(0);
                String FormLoc=DocN.elementAt(0).toString();
                /* modified by amaleh on 2003-06-24 */
                String FormName = FormLoc.substring(0,FormLoc.indexOf(SepChar)).trim();
                aipDocInfo.getAIPForm().setName(FormName);
                /**/
                String ImagePath = AIPUtil.cleanTabInString(FormLoc.substring(FormLoc.indexOf(SepChar)).trim());
                aipDocInfo.setImagesPath(ImagePath);
                /* modified by amaleh on 2003-06-24 */
                Vector params = new Vector();
                params.addElement(FormName);
                /* added by amaleh on 2003-07-30 */
                
                loadIndicesFor(aipDocInfo, DocsFileExt, i+1, SLine, EndLine, params);
                /**/
                /**/
                /* added by amaleh on 2003-07-03 - reads comment field entry */
                String commentFieldKey  ="doc"+Integer.toString(i+1)+".comment_file";
                String commentFieldData =getValue(commentFieldKey,i+1,SLine,EndLine);
                
                /* added on 2003-07-07 */
                /* modified on 2003-07-30 - quotations are now optional */
                //eliminate the quotations ("[comment_path]")
                if (commentFieldData.charAt(0) == '"' & commentFieldData.charAt(commentFieldData.length()-1) == '"')
                    commentFieldData = commentFieldData.substring(1, commentFieldData.length() - 1);
                /**/
                aipDocInfo.setCommentFile(commentFieldData);
                /**/
                /* added by amaleh - reads VW folder location entry */
                String VWNodeFieldKey  ="doc"+Integer.toString(i+1)+".location";
                String VWNodeFieldData =getValue(VWNodeFieldKey,i+1,SLine,EndLine);
                aipDocInfo.setVWNodePath(VWNodeFieldData);
                /**/
                /* added by amaleh on 2003-07-02 - reads create_location option value */
                String createLocationKey  ="doc"+Integer.toString(i+1)+".create_location";
                String createLocationData =getValue(createLocationKey,i+1,SLine,EndLine);
                if (createLocationData.equalsIgnoreCase("true") || createLocationData.equalsIgnoreCase("t") || createLocationData.equals("1"))
                    aipDocInfo.setCreateVWNode(true);
                else
                    aipDocInfo.setCreateVWNode(false);
                /**/
                /* added by amaleh on 2003-06-09 - reads append option value */
                String AppendKey  ="doc"+Integer.toString(i+1)+".append";
                String AppendData =getValue(AppendKey,i+1,SLine,EndLine);
                if (AppendData.equalsIgnoreCase("true") || AppendData.equalsIgnoreCase("t") || AppendData.equals("1"))
                    aipDocInfo.setAppend(true);
                else
                    aipDocInfo.setAppend(false);
                /**/
                /* added by amaleh on 2003-07-02 - reads delete_images option value */
                String deleteImagesKey ="doc"+Integer.toString(i+1)+".delete_images";
                String deleteImagesData =getValue(deleteImagesKey,i+1,SLine,EndLine);
                if (deleteImagesData.equalsIgnoreCase("true") || deleteImagesData.equalsIgnoreCase("t") || deleteImagesData.equals("1"))
                    aipDocInfo.setDeleteImages(true);
                else
                    aipDocInfo.setDeleteImages(false);
                /**/
                
                
                TM.put("document"+Integer.toString(i),aipDocInfo);
            }
                //if(Log.LOG_VERBOSE_DEBUG) Log.debug("<"+TM.size()+"> Documents have been extracted");
        }
        return TM;
    }
    
    /** created on 2003-12-18
     * creates a shadow file from a regular BIF file
     */
    public File createShadowFile(File EFile)
    {
        try
        {
            //read bif file into memory
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(EFile)));
            StringBuffer buffer = new StringBuffer();
            int dummy;
            while ((dummy = reader.read()) != -1)
                buffer.append((char)dummy);
            reader.close();
            //modify bif file contents in memory
            char c;
            String line;
            //beginning of line positions
            Vector linesi = new Vector();
            //position
            int linei = 0;
            //current line number
            int ln = 0;
            int i = 0;
            linesi.add(new Integer(i));
            for (; i < buffer.length(); i++)
            {
                c = buffer.charAt(i);
                if (c == '\n')
                {
                    linei = ((Integer)(linesi.get(ln))).intValue();
                    line = buffer.substring(linei, i);
                    if (line.toLowerCase().startsWith("document1="))
                    {
                        //insert file id
                        buffer.insert(linei, "0000000000" + System.getProperty("line.separator"));
                    }else
                    if (line.toLowerCase().indexOf(".fields=") != -1)
                    {
                        //offset created by inserting characters into the buffer
                        int offset = 0;
                        char d;
                        for (int j = 0; j < line.length(); j++)
                        {
                            d = line.charAt(j);
                            if (d == '|')
                            {
                                buffer.insert(linei + j + offset, "~0");
                                offset+=2;
                            }
                        }
                        d = line.charAt(line.length() - 1);
                        if (d == '\r')
                            offset--;
                        //if end of line reached, insert ~0 after the last field name
                        //make sure at least one field name exist however
                        if (line.indexOf("=\r") == -1)
                            buffer.insert(linei + line.length() + offset, "~0");
                    }else
                    if (line.toLowerCase().startsWith("document"))
                    {
                        //insert doc status string
                        buffer.insert(linei, "2000" + System.getProperty("line.separator"));
                    }
                    //record beginning of next line
                    linesi.add(new Integer(i+1));
                    ln++;
                }
            }
            //insert extra newline in case there was no hard return in the end
            buffer.insert(buffer.length(), System.getProperty("line.separator"));
            //insert last doc status string
            buffer.insert(buffer.length(), "2000" + System.getProperty("line.separator"));
            //create shadow file and write to it the modified bif contents in memory.
            File file = new File(EFile.getPath() + ".SHD");
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
            for (int k = 0; k < buffer.length(); k++)
                writer.write(buffer.charAt(k));
            writer.close();
            EFile.delete();
            return file;
        }
        catch(IOException ioe)
        {
        	VWLogger.info("Exception while creating the shadowFile :::"+ioe.getMessage());
            return null;
        }
    }
    /*
     * "doc_number"
     * "file_id"
     * "field#_status"
     * "status"
     * "pend_cause"
     * "hist_input_status"    
     */
    public boolean updateNext(java.util.TreeMap map) 
    {
        return shadowFileManager.updateNext(map);
    }
    
    /* modified by amaleh on 2003-12-01 */
    /**
     * Returns null if file is nonexistant, invalid, or contains an invalid
     * segment.
     */
    public TreeMap getDocuments(){
        //VWLogger.debug("Get AIPFile Documents");
        //added by amaleh on 2004-02-16
        //check for validity of BIF file before converting to shadow file
        //i.e. check for [Documents File] at the beginning.
        BIFFileParser parser = new BIFFileParser();
        try
        {
            parser.setFile(this.EFile);
        }
        catch(java.io.FileNotFoundException fnfe)
        {
           VWLogger.error("File not found, exception: ", fnfe);
            return null;
        }
        TreeMap parsedTestMap = null;
        VWLogger.info("Before parsing the bif file.....");
		if ((parsedTestMap = parser.parseNext()) != null) {
            String status = (String)(parsedTestMap.get("status"));
            VWLogger.info("Bif file status :"+status);
            //if it's an invalid file, break
            if (status.equalsIgnoreCase("invalid_file"))
            {
                VWLogger.error("Invalid file format: " + EFile.getPath(),null);
                return null;
            }
        }
        /* if segement is invalid, discontinue and close shadowFileManager */
        try
        {
            parser.close();
        }
        catch(java.io.IOException ioe)
        {
            VWLogger.error("Error closing BIFFileParser", ioe);
        }
        //2004-02-12
        int EFileLength = EFile.getPath().length();
        if (EFile != null)
        if (EFile.getPath().substring(EFileLength - 4, EFileLength).equalsIgnoreCase(".BIF"))
            SetUsedFile(createShadowFile(EFile));
        EFileLength = EFile.getPath().length();
        if (EFile != null)
        if (!EFile.getPath().substring(EFileLength - 4, EFileLength).equalsIgnoreCase(".SHD"))
            return null;
        TreeMap TM = new TreeMap(new Comparator()
        {
            public int compare(Object o1, Object o2)
            {
                String name1 = (String)o1;
                String name2 = (String)o2;
                String number1 = name1.substring(8);
                String number2 = name2.substring(8);
                int i1 = Integer.parseInt(number1);
                int i2 = Integer.parseInt(number2);
                return i1 - i2;
            }
            public boolean equals(Object obj)
            {
                return false;
            }
        });
        TreeMap parsedMap;
        shadowFileManager = new BIFShadowFileManager();
        try
        {
            shadowFileManager.setFile(this.EFile);
        }
        catch(java.io.FileNotFoundException fnfe)
        {
            VWLogger.error("File not found, exception: ", fnfe);
            TM = null;
            return TM;
        }
        while ((parsedMap = shadowFileManager.parseNext()) != null) {
            String status = (String)(parsedMap.get("status"));
            //if end of file reached, break. This is the case of SUCCESS.
            if (status.equalsIgnoreCase("eof"))
                break;
            //if it's an invalid file, break
            if (status.equalsIgnoreCase("invalid_file"))
            {
                TM = null;
                break;
            }
            //if it's an invalid segment, break. (TODO: continue to the next one).
            if (status.equalsIgnoreCase("invalid_segment"))
            {
                /* if segement is invalid, discontinue and close shadowFileManager */
                try
                {
                    shadowFileManager.close();
                }
                catch(java.io.IOException ioe)
                {
                   VWLogger.error("Error closing shadowFileManager", ioe);
                }
                TM = null;
                break;
            }
            //commented by amaleh on 2003-09-29
           // VWLogger.debug("Reading document: " + (i+1));
            AIPDocument aipDocInfo =new AIPDocument();
            String segNum = (String)(parsedMap.get("segment#"));
            String i = (String)(parsedMap.get("segment" + segNum + ".doc_number"));
            //prefix for name, doctype, and image paths keys.
            String prefix = "doc" + i;
            aipDocInfo.setName((String)(parsedMap.get(prefix + ".name")));
            aipDocInfo.setId(0);
            aipDocInfo.setFileId(Integer.parseInt((String)(parsedMap.get("file_id"))));
            aipDocInfo.setStatus(Integer.parseInt((String)(parsedMap.get(prefix + ".status"))));
            aipDocInfo.setPendCause(Integer.parseInt((String)(parsedMap.get(prefix + ".pend_cause"))));
            /* modified by amaleh on 2003-06-24 */
            /* assign FormName the doctype value */
            String FormName = (String)(parsedMap.get(prefix + ".doctype"));
            aipDocInfo.getAIPForm().setName(FormName);
            /**/
            String ImagePath = (String)(parsedMap.get(prefix + ".image_paths"));
            aipDocInfo.setImagesPath(ImagePath);
            /* modified by amaleh on 2003-06-24 */
            Vector params = new Vector();
            params.addElement(FormName);            
            loadIndicesFor(aipDocInfo, DocsFileExt, parsedMap, params);
            /**/
            /**/
            /* added by amaleh on 2003-07-03 - reads comment field entry */
            String commentFieldKey  =prefix + ".comment_file";
            String commentFieldData =(String)(parsedMap.get(commentFieldKey));

            /* added on 2003-07-07 */
            /* modified on 2003-07-30 - quotations are now optional */
            // Comments are optional. Replace null value with empty string.
            if (commentFieldData == null)
                commentFieldData = "";
            aipDocInfo.setCommentFile(commentFieldData);
            /**/
            /* added by amaleh - reads VW folder location entry */
            String VWNodeFieldKey  =prefix + ".location";
            String VWNodeFieldData =(String)(parsedMap.get(VWNodeFieldKey));
            aipDocInfo.setVWNodePath(VWNodeFieldData);
            /**/
            /* added by amaleh on 2003-07-02 - reads create_location option value */
            String createLocationKey  =prefix + ".create_location";
            String createLocationData =(String)(parsedMap.get(createLocationKey));
            if (createLocationData == null)
                createLocationData = "";
            if (createLocationData.equalsIgnoreCase("true") || createLocationData.equalsIgnoreCase("t") || createLocationData.equals("1"))
                aipDocInfo.setCreateVWNode(true);
            else
                aipDocInfo.setCreateVWNode(false);
            /**/
            /* added by amaleh on 2003-06-09 - reads append option value */
            String AppendKey  =prefix + ".append";
            String AppendData =(String)(parsedMap.get(AppendKey));
            if (AppendData == null)
                AppendData = "";
            if (AppendData.equalsIgnoreCase("true") || AppendData.equalsIgnoreCase("t") || AppendData.equals("1"))
                aipDocInfo.setAppend(true);
            else
                aipDocInfo.setAppend(false);
            /**/
            /* added by amaleh on 2003-07-02 - reads delete_images option value */
            String deleteImagesKey =prefix + ".delete_images";
            String deleteImagesData =(String)(parsedMap.get(deleteImagesKey));
            if (deleteImagesData == null)
                deleteImagesData = "";
            if (deleteImagesData.equalsIgnoreCase("true") || deleteImagesData.equalsIgnoreCase("t") || deleteImagesData.equals("1"))
                aipDocInfo.setDeleteImages(true);
            else
                aipDocInfo.setDeleteImages(false);
            /**/
			//adding the field security for apply the permission through the AIP
            String securityInfoKey = prefix + ".security";
            String securityInfoData = (String)(parsedMap.get(securityInfoKey));
            if (securityInfoData == null)
            	aipDocInfo.setSecurityInfo("");
            else            	
            	aipDocInfo.setSecurityInfo(securityInfoData);           
            	
            TM.put("document"+i,aipDocInfo);
        }
        //if file was not assigned properly and parsedMap was null, then TM is null too.
        if (parsedMap == null)
            TM = null;
        //if (TM != null)
            
        return TM;
    }
    
    public void close()
    {
        try{
        shadowFileManager.close();
        }catch(IOException ioe)
        {
           VWLogger.error("Error closing file: ", ioe);
        }
    }

    /* added by amaleh on 2003-07-02 */
    /**
     * returns an order transition array that indicates how to reorder the second
     * vector so it can have the order of elements in the first.
     */
    private Vector getOrderTransition(Vector vectorInOrder, Vector vector) {
        if (vectorInOrder == null || vector == null)
            return null;
        Vector order = new Vector();
        for (int i=0; i<vectorInOrder.size(); i++) {
            String currentElement1 = vectorInOrder.elementAt(i).toString();
            int j;
            for (j=0; j<vector.size(); j++) {
                String currentElement2 = (String)(vector.elementAt(j));
                if (currentElement1.equals(currentElement2)) {
                    order.addElement(new Integer(j));
                    break;
                }
            }
            if (j == vector.size())
                order.addElement(new Integer(-1));
        }
        return order;
    }
    
    /* added by amaleh on 2003-07-02 */
    /**
     * reorders a vector according to an order transition vector
     */
    private Vector reorder(Vector v1, Vector order) {
        if (v1 == null)
            return null;
        else
            if (order == null)
                return v1;
        Vector reordered = new Vector();
        for (int i = 0; i < order.size(); i++) {
            int orderValue = ((Integer)(order.elementAt(i))).intValue();
            if (orderValue == -1) {
                AIPField docField =new AIPField();
                docField.setId(0);
                docField.setOrder(i);
                docField.setValue("");
                docField.setStatus(docField.ACCEPTED);
                reordered.addElement(docField);
            }
            else
                reordered.addElement(v1.elementAt(orderValue));
        }
        return reordered;
    }
//--------------------------------------------------------------------
    class EHFilesFilter implements FileFilter{
        public EHFilesFilter(){}
        public boolean accept(File pathname){
            if(pathname.isDirectory()) return false;
            String FName = pathname.getName();
            int i = FName.lastIndexOf('.');
            if (i > 0 &&  i < FName.length() - 1) {
                String extension = FName.substring(i+1).toLowerCase();
                if(extension.equalsIgnoreCase(DocsFileExt))
                    return true;
            }
            return false;
        }
        
    }
    /* added by amaleh on 2003-12-22 */
    /**
     * Filters in shadow files
     */
    class EHShadowFilesFilter implements FileFilter{
        public EHShadowFilesFilter(){}
        public boolean accept(File pathname){
        	try{
	            if(pathname.isDirectory()) return false;
	            String FName = pathname.getName();
	            int i = FName.lastIndexOf('.');
	            if (i > 0 &&  i < FName.length() - 1) {
	                String extension = FName.substring(i+1).toLowerCase();
	                if(extension.equalsIgnoreCase("SHD"))
	                    return true;
	            }
        	}catch(Exception e){
        		VWLogger.info("Exception EHShadowFilesFilter :::"+e.getMessage());
        	}
            return false;
        }
        
    }
    /**/
    
}