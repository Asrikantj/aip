/*
 * BIFFileParser.java
 *
 * Created on November 25, 2003, 4:47 PM
 */

package com.computhink.aip;

import java.io.*;
import java.util.*;
import java.util.regex. *;

import com.computhink.aip.util.VWLogger;

/**
 * A derivative of GradualFileParser that parses BIF files.
 *
 * BIF File Format:
 *
 * [Documents File]
 * [documents]
 * count=(count)
 * document1=(doctype)	"(image1)";"(image2)";"(image3)"
 * doc1.fields=(index1)|(index2)|(index3)
 * doc1.field1=(index1 value)
 * doc1.field2=(index2 value)
 * doc1.field3=(index3 value)
 * doc1.comment_file="(comment_file)"
 * doc1.location=(Room/Cabinet/Drawer/Folder[/Subfolder/...])
 * doc1.create_location=([true|FALSE])
 * doc1.append=([true|FALSE])
 * doc1.delete_images=([true|FALSE])
 * document2=(doctype)	"(image1)";"(image2)";"(image3)"
 * doc2.fields=(index1)|(index2)|(index3)
 * doc2.field1=(index1 value)
 * doc2.field2=(index2 value)
 * doc2.field3=(index3 value)
 * doc2.comment_file="(comment_file)"
 * doc2.location=(Room/Cabinet/Drawer/Folder[/Subfolder/...])
 * doc2.create_location=([true|FALSE])
 * doc2.append=([true|FALSE])
 * doc2.delete_images=([true|FALSE])
 * document3=(doctype)	"(image1)";"(image2)";"(image3)"
 * doc3.fields=(index1)|(index2)|(index3)
 * doc3.field1=(index1 value)
 * doc3.field2=(index2 value)
 * doc3.field3=(index3 value)
 * doc3.comment_file="(comment_file)"
 * doc3.location=(Room/Cabinet/Drawer/Folder[/Subfolder/...])
 * doc3.create_location=([true|FALSE])
 * doc3.append=([true|FALSE])
 * doc3.delete_images=([true|FALSE])
 *
 * The "count", "docx.comment_file", "docx.create_location", "docx.append",
 * and "docx.delete_images" lines are optional.
 *
 * The "count" line was in the older version of the BIF format. Now, it is
 * simply ignored when encountered.
 *
 * The parsing function checks for validity of the file format in respect with
 * the BIF format.
 *
 * Conditions for validity:
 * 1) all the required lines are present
 * 2) all lines satisfy their expected format
 *
 * @author  amaleh
 */
public class BIFFileParser implements com.computhink.aip.util.GradualFileParser 
{
    /**
     * Regex for the document number, doctype, and image line
     * Modified on 2003-12-15:
     * Now accepts an empty string for image paths, or empty image paths.
     */
    public static final String REGEX_DOCUMENT = "[Dd][Oo][Cc][Uu][Mm][Ee][Nn][Tt]\\d+=[^\t]*\t.*";
    
    /**
     * Regex for fields line
     */
    public static final String REGEX_FIELDS = "[Dd][Oo][Cc]\\d+[.][Ff][Ii][Ee][Ll][Dd][Ss]=.*";
    
    /**
     * Regex for field value line
     */
    public static final String REGEX_FIELD = "[Dd][Oo][Cc]\\d+[.][Ff][Ii][Ee][Ll][Dd]\\d+=.*";
    
    /**
     * Regex for comment file line
     */
    public static final String REGEX_LOCATION = "[Dd][Oo][Cc]\\d+[.][Ll][Oo][Cc][Aa][Tt][Ii][Oo][Nn]=.*";
    
    /**
     * Regex for comment_file line
     */
    public static final String REGEX_COMMENT = "[Dd][Oo][Cc]\\d+[.][Cc][Oo][Mm][Mm][Ee][Nn][Tt]_[Ff][Ii][Ll][Ee]=.*";

    /**
     * Regex for option lines, also generalizes for all others except document.
     */
    public static final String REGEX_OPTION = "[Dd][Oo][Cc]\\d+[.]\\w+=.*";
    
    /**
     * Pattern based on the regex for document
     */
    protected static final Pattern pDocument = Pattern.compile(REGEX_DOCUMENT);

    /**
     * Pattern based on the regex for fields
     */
    protected static final Pattern pFields = Pattern.compile(REGEX_FIELDS);

    /**
     * Pattern based on the regex for field
     */
    protected static final Pattern pField = Pattern.compile(REGEX_FIELD);

    /**
     * Pattern based on the regex for location
     */
    protected static final Pattern pLocation = Pattern.compile(REGEX_LOCATION);

    /**
     * Pattern based on the regex for comment
     */
    protected static final Pattern pComment = Pattern.compile(REGEX_COMMENT);
    
    /**
     * Pattern based on the regex for option, also generalizes for all others
     * except document.
     */
    protected static final Pattern pOption = Pattern.compile(REGEX_OPTION);
    
    /**
     * StringTokenizer delimiters for document line
     */
    public static final String delDocument = "=";
    
    /**
     * StringTokenizer delimiters for document line
     */
    public static final String delDocument1 = "\t";
    /**
     * StringTokenizer delimiters for fields line
     */
    public static final String delFields1 = "=";
    public static final String delFields2 = "|";
    /**
     * StringTokenizer delimiters for comment line
     */
    public static final String delComment = "=\"";
    
    /**
     * StringTokenizer delimiters for all lines
     */
    public static final String delGeneric = "=";

    /**
     * file to be used for parsing
     */
    protected File file;
    
    /**
     * Reader to be used in parsing the file
     */
    protected LineNumberReader reader;
    
    /**
     * Line reached by the reader
     */
    protected int lineNumber;
    
    /**
     * Last line read from file. Value null means eof or file not found.
     */
    protected String line = null;
    
    /**
     * Holds the order number of the segment to parse next
     */
    protected int segmentNumber;
    
    /** Creates a new instance of BIFFileParser */
    public BIFFileParser() 
    {
    }
    
    public java.util.TreeMap parse() 
    {
        return parseNext();
    }
    
    public java.util.TreeMap parseNext()
    {
        //if reader is null, initialize it.
        if (reader == null)
        {
            try
            {
                //create a line number reader out of the file supplied
                reader = new LineNumberReader(new FileReader(file));
                VWLogger.info("reader inside bifffile parser::::::::"+reader);
                lineNumber = reader.getLineNumber();
                VWLogger.info(" inside biffile parse lineNumber ::::::::"+lineNumber);
            }
            catch(FileNotFoundException fnfe)
            {
            	VWLogger.info("Exception while parsing the bif File "+fnfe);
                return null;
            } catch (Exception e) {
            	VWLogger.info("General Exception while parsing the bif File "+e);
            	return null;
            }
        }
        //create an initial invalid TreeMap object
        TreeMap map = new TreeMap();
        try
        {
            /* If line number reader did not begin yet, then check for the validity
             * of the file by looking for the "[Documents File]" and "[documents]"
             * lines.
             * If a "Count=x" line is encountered, ignore it for backwards
             * compatibility.
             */
            if (lineNumber == 0)
            {
                String line0 = reader.readLine();
                String line1 = reader.readLine();
                if (!line0.trim().equalsIgnoreCase("[Documents File]") || !line1.trim().equalsIgnoreCase("[documents]"))
                {
                    //set map's status as invalid
                    map.put("status", "invalid_file");
                    //close reader for it is of no further use.
                    reader.close();
                    //return invalid TreeMap object
                    return map;
                }
                //read a line
                line = reader.readLine();
            }
            /* If line value is null, then end of file is reached */
            if (line == null)
            {
                //set map's status to end of file
                map.put("status", "eof");
                return map;
            }
            /* The following booleans record the result of parsing lines.
             * Also, the following integer records the count of field lines
             * parsed successfully.
             * In order for the segment parsing to be considered "valid", 
             * isDocumentParsed, isFieldsParsed, and isLocationParsed must all 
             * be true.
             */
            boolean isDocumentParsed = false;
            boolean isFieldsParsed = false;
            boolean isLocationParsed = false;
            boolean isCommentParsed = false;
            /* give lineNumber its true value so an initial validity check is not
             * repeated again
             */
            lineNumber = reader.getLineNumber();
            /* Lines of a document will be now read one by one. If one of them did
             * not match its proper format as supplied in the description of this
             * class, an invalid TreeMap object is returned.
             */
            /* if the 3rd line contained the count of the docs, acknowledge for 
             * backwards compatibility, and skip to the next line.
             */
            if (lineNumber == 3)
            {
                if (line.trim().toLowerCase().startsWith("count="))
                    line = reader.readLine();
            }
            /*if line is empty, skip it.*/
            if (line.trim().equals(""))
                line = reader.readLine();
            /* if the document line was not parsed successfully, return an
             * invalid TreeMap object
             */
            isDocumentParsed = parseDocumentLine(map, line.trim());
            /* loop through the following lines until you hit the next document,
             * which begins with "document"
             */
            for (line = reader.readLine(); line != null; line = reader.readLine())
            {
                //added ayman 
                line =line.trim();
                /*if line is empty, skip it.*/
                if (line.trim().equals(""))
                    line = reader.readLine();
                /* if next document is reached, break */
                if (line.toUpperCase().startsWith("DOCUMENT"))
                    break;
                /* if document line was not parsed successfully, keep looping
                 * until you reach the next document
                 */
                if (!isDocumentParsed)
                    continue;
                /* test line for fields, field, location, comment, or one of the
                 * options.
                 */
                if (!isFieldsParsed)
                {
                    isFieldsParsed = parseFieldsLine(map, line);
                    if (isFieldsParsed)
                        continue;
                }
                if (!isLocationParsed)
                {
                    isLocationParsed = parseLocationLine(map, line);
                    if (isLocationParsed)
                        continue;
                }
                if (!isCommentParsed)
                {
                    isCommentParsed = parseCommentLine(map, line);
                    if (isCommentParsed)
                        continue;
                }
                if (parseFieldLine(map, line))
                    continue;
                /* option lines may include all other lines tested, so they are
                 * tested last.
                 */
                if (parseOptionLine(map, line))
                    continue;
            }
            /* If line value is null, then end of file is reached */
            if (line == null)
            {
                reader.close();
            }
            /* if required lines were parsed successfully, set status to "valid".
             * Otherwise, "invalid_segment"
             */
            if (isDocumentParsed && isFieldsParsed && isLocationParsed)
                map.put("status", "valid");
            else
                map.put("status", "invalid_segment");
        }
        catch (IOException ioe)
        {
            //in case of file io, return null.
        	VWLogger.info("Exception while parsing the bif File XXX :"+ioe);
            map = null;
            return map;
        } catch (Exception e) {
        	VWLogger.info("General Exception while parsing the bif File XXX :"+e);
        	map = null;
            return map;
        }
        map.put("segment#", Integer.toString(segmentNumber++));
        VWLogger.info("before returning map from biffile parse....."+map);
        return map;
    }
    
    /**
     * Parses data from document line and puts it in the TreeMap object to be
     * returned in the parse() or parseNext() function.
     * Returns true if it succeeds in parsing and false otherwise.
     */
    protected boolean parseDocumentLine(TreeMap map, String line)
    {
        //match line with document line pattern
        Matcher mDocument = pDocument.matcher(line);
        //if it matches, parse it. Otherwise, return invalid map.
        if (mDocument.matches())
        {
            //StringTokenizer for document line
            StringTokenizer lineTokens = new StringTokenizer(line, delDocument);
            //document name is the first token
            String docName = lineTokens.nextToken();
            //document type is the second
            StringTokenizer lineTokens1 = new StringTokenizer(line.substring(docName.length()+1, line.length()), delDocument1);
            String docType = lineTokens1.nextToken();
            //document image paths string is the third
            String docImagePaths;
          
            //String docType1 = lineTokens1.nextToken();
            if (lineTokens1.hasMoreTokens()) {
            	
                docImagePaths = lineTokens1.nextToken();
            }
            else
                docImagePaths = "";
            //document number extracted from name
            String docNumber = docName.substring(8);
            //prefix for document number.
            String docNumberKey = "segment" + Integer.toString(segmentNumber) + ".doc_number";
            //prefix for document name, doctype, and image paths.
            String prefix = "doc" + docNumber;
            map.put(docNumberKey, docNumber);
            map.put(prefix + ".name", docName);
            map.put(prefix + ".doctype", docType);
            map.put(prefix + ".image_paths", docImagePaths);
            return true;
        }
        return false;
    }
    
    /**
     * Parses data from fields line and puts it in the TreeMap object to be
     * returned in the parse() or parseNext() function.
     * Also puts the field count within the TreeMap object to be returned.
     * Returns true if it succeeds in parsing and false otherwise.
     */
    protected boolean parseFieldsLine(TreeMap map, String line)
    {
        //match line with fields line pattern
        Matcher mFields = pFields.matcher(line);
        //if it matches, parse it. Otherwise, return invalid map.
        if (mFields.matches())
        {
            //StringTokenizer for document line
            StringTokenizer lineTokens = new StringTokenizer(line, delFields1);
            //skip key token
           String key1= lineTokens.nextToken();
            //count fields
            lineTokens = new StringTokenizer(line.substring(key1.length()+1,line.length()), delFields2);
            int fieldCount = 0;
            while (lineTokens.hasMoreTokens())
            {
            	lineTokens.nextToken();
                fieldCount++;
            }
            //prefix for document number.
            String docNumberKey = "segment" + Integer.toString(segmentNumber) + ".doc_number";
            //document number extracted from name
            String docNumber = (String)(map.get(docNumberKey));
            map.put("doc" + docNumber + ".field_count", Integer.toString(fieldCount));
            lineTokens = new StringTokenizer(line, delGeneric);
            //the first token is the key
            String key = lineTokens.nextToken();
            //the second is the value
            //modified on 2004-02-18
            //check first if value is empty or not
            String fieldvalue = line.substring(key.length()+1, line.length());
            String value = fieldvalue.length()>0 ? fieldvalue : "";
            		//lineTokens.hasMoreTokens() ? lineTokens.nextToken() : "";
            map.put(key, value);
            return true;
        }
        return false;
    }

    /**
     * Parses data from location line and puts it in the TreeMap object to be
     * returned in the parse() or parseNext() function.
     * Returns true if it succeeds in parsing and false otherwise.
     */
    protected boolean parseLocationLine(TreeMap map, String line)
    {
        //match line with location line pattern
        Matcher mLocation = pLocation.matcher(line);
        //if it matches, parse it. Otherwise, return invalid map.
        if (mLocation.matches())
        {
            //StringTokenizer for location line
            StringTokenizer lineTokens = new StringTokenizer(line, delGeneric);
            //the first token is the key
            String key = lineTokens.nextToken();
            //the second is the value
            //modified on 2004-02-18
            //check first if value is empty or not
            String fieldvalue = line.substring(key.length()+1, line.length());
            String value = fieldvalue.length()>0 ? fieldvalue : "";
            map.put(key, value);
            return true;
        }
        return false;
    }

    /**
     * Parses data from comment line and puts it in the TreeMap object to be
     * returned in the parse() or parseNext() function.
     * Returns true if it succeeds in parsing and false otherwise.
     */
    protected boolean parseCommentLine(TreeMap map, String line)
    {
        //match line with comment line pattern
        Matcher mComment = pComment.matcher(line);
        //if it matches, parse it. Otherwise, return invalid map.
        if (mComment.matches())
        {
            //StringTokenizer for comment line
            StringTokenizer lineTokens = new StringTokenizer(line, delComment);
            //the first token is the key
            String key = lineTokens.nextToken();
            //modified on 2004-02-06 to account for the case of empty comment_file value
            //since there might not be any value, a check is made for next token
            String value = "";
            if (lineTokens.hasMoreTokens())
            {
                //the second is the value
                value = lineTokens.nextToken();
                //if comment was contained in quotes, then trim them.
                if (value.charAt(0) == '"')
                    value = value.substring(1, value.length() - 1);
            }
            map.put(key, value);
            return true;
        }
        return false;
    }

    /**
     * Parses data from field line and puts it in the TreeMap object to be
     * returned in the parse() or parseNext() function.
     * Returns true if it succeeds in parsing and false otherwise.
     */
    protected boolean parseFieldLine(TreeMap map, String line)
    {
        //match line with field line pattern
        Matcher mField = pField.matcher(line);
        //if it matches, parse it. Otherwise, return invalid map.
        if (mField.matches())
        {
            //StringTokenizer for field line
            StringTokenizer lineTokens = new StringTokenizer(line, delGeneric);
            //the first token is the key
            String key = lineTokens.nextToken();
            //the second is the value
            //modified on 2004-02-18
            //check first if value is empty or not
            String fieldvalue = line.substring(key.length()+1, line.length());
            String value = fieldvalue.length()>0 ? fieldvalue : "";
            //String value = lineTokens.hasMoreTokens() ? lineTokens.nextToken() : "";
            map.put(key, value);
            return true;
        }
        return false;
    }

    /**
     * Parses data from option line and puts it in the TreeMap object to be
     * returned in the parse() or parseNext() function.
     * Returns true if it succeeds in parsing and false otherwise.
     */
    protected boolean parseOptionLine(TreeMap map, String line)
    {
        //match line with option line pattern
        Matcher mOption = pOption.matcher(line);
        //if it matches, parse it. Otherwise, return invalid map.
        if (mOption.matches())
        {
            //StringTokenizer for option line
            StringTokenizer lineTokens = new StringTokenizer(line, delGeneric);
            //the first token is the key
            String key = lineTokens.nextToken();
            //the second is the value
            //modified on 2004-02-18
            //check first if value is empty or not
            String value = lineTokens.hasMoreTokens() ? lineTokens.nextToken() : "";
            map.put(key, value);
            return true;
        }
        return false;
    }

    public void setFile(java.io.File file) throws FileNotFoundException
    {
        //set the parsing file to the file supplied
        this.file = file;
    }
    
    public void close() throws IOException
    {
        reader.close();
    }

    public java.io.File getFile() 
    {
        //return parsing file
        return this.file;
    }
    
    public static void main(String[] args) throws Exception
    {
        BIFFileParser p = new BIFFileParser();
        p.setFile(new File("f:\\output\\1.bif"));
        p.parseNext();
        p.parseNext();
        System.out.println("finished");
    }
    
}
