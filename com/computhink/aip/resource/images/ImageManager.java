package com.computhink.aip.resource.images;

import java.awt.*;
import javax.swing.*;
/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class ImageManager {

  public ImageManager() {

  }
  public ImageIcon getImageIcon(String Name){
      if(getClass().getResource(Name) !=null){
          return new ImageIcon(getClass().getResource(Name));
      }
      return null;
  }

}