package com.computhink.aip;

import java.io.*;
import java.io.Reader;
import java.awt.*;
import java.awt.event.*;
import java.util.Vector;
import java.util.*;
import com.computhink.aip.util.AIPUtil;

import com.computhink.aip.util.AIPUtil;
import com.computhink.aip.util.VWLogger;
import com.computhink.aip.client.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public class EyesFileManager {
  String filePath;
  File EFile;
  int ScanLine =0;
  int PosStart =-1;
  final String SepChar= new Character((char)9).toString();
  private String FormsFileName="EHForms.EFF";
  private String DocsFileExt="EHD";

  public EyesFileManager() {

  }
  public void setFormsFileName(String Name){
      if(Name !=null && !Name.equals(" "))
      FormsFileName=Name;
  }
  public void setDocsFileExt(String EXT){
      if(EXT !=null && !EXT.equals(" "))
      DocsFileExt =EXT;
  }
  public File[] getEHDocumentsFiles(String FolderPath){
	  try{
       Vector VFiles =new Vector();

       File Folder = new File(FolderPath);

       if(Folder.exists() && Folder.isDirectory()){
          return  Folder.listFiles(new EHFilesFilter());
       }
	  }catch(Exception e){
		  VWLogger.info("Exception in getEHDocumentsFiles :::"+e.getMessage());
	  }
       return null;
  }
  public File getEHFormsFile(String FolderPath){
       //if(Log.LOG_VERBOSE_DEBUG) Log.debug("get EHForms File in the Folder -----:"+FolderPath);
	  try{
       String FilePath = FolderPath+System.getProperty("file.separator")+FormsFileName;

       File FFile = new File(FilePath);
       if(FFile.exists() && FFile.isFile()){
          return  FFile;
       }
	  }catch(Exception e){
		  VWLogger.info("Exception while getEhFormsFile ::"+e.getMessage());
	  }
       //if(Log.LOG_DEBUG) Log.debug("Error :EHForms File not found in the Folder -----:"+FolderPath);
       return null;
  }
  public void SetUsedFile(File EHFile){
        EFile =EHFile;
  }
  //added by amaleh on 2004-01-22
  /** returns used file */
  public File GetUsedFile()
  {
        return EFile;
  }
  //
  public boolean isEHFile(File F,String Type){
     if(F != null){
        SetUsedFile(F);
        if(getMainkeysPos(Type))
              return true;
     }
     return false;
  }
  public String getValue(String key,int beginLine,int EndLine){
     // if(Log.LOG_VERBOSE_DEBUG) Log.debug("get value of key <"+key+"> in file <"+EFile.getName()+"> between ("+beginLine+":"+EndLine+")" );
      LineNumberReader Lread = null;
      try{
         Lread =new LineNumberReader(new FileReader(EFile));
         String line;
         while ((line =Lread.readLine()) !=null){
              line = line.trim();
              if(Lread.getLineNumber() < beginLine  ) continue;
              if (line.startsWith("#") || line.length() == 0) continue;
              if(line.indexOf(key) > -1 ){
                  ScanLine = Lread.getLineNumber();
                  int EqualPos = line.indexOf("=");
                  if( EqualPos != -1){
                     // if(Log.LOG_VERBOSE_DEBUG) Log.debug("find at line ("+ScanLine+")"+" value :"+ line.substring(EqualPos+1).trim());
                     String Value = line.substring(EqualPos+1).trim();
                     if(!Value.equals("")) 
                     {
                         return Value;
                     }
                     else 
                     {
                         return " ";
                     }
                  }
                  //if(Log.LOG_DEBUG) Log.debug("Error :Can not find value of key <"+key+"> in file <"+EFile.getName()+"> between ("+beginLine+":"+EndLine+")" );
                  return " ";
              }
              if(EndLine !=0 && Lread.getLineNumber() == EndLine) break;
        }
     }
     catch (IOException e) {
           //if(Log.LOG_ERROR) Log.error("Error :Can not find value",e);
            return "<!NF!>";
     }
     catch (SecurityException se) {
          //if(Log.LOG_ERROR) Log.error("Error :Can not find value",se);
           return "<!NF!>";
     }
     //added by amaleh on 2004-02-05 - close file
     finally
     {
         try
         {
            //added by amaleh on 2004-02-05 - close file
            if (Lread != null)
            {
             Lread.close();
            }
         }
         catch(IOException ioe)
         {
             //if(Log.LOG_ERROR) Log.error("Error :Cannot close EH file",ioe);
         }
     }
     return "<!NF!>";
  }
  public boolean Findkey(String key,int beginLine,int EndLine){
     // if(Log.LOG_VERBOSE_DEBUG) Log.debug("Find key <"+key+"> in file <"+EFile.getName()+"> between ("+beginLine+":"+EndLine+")" );
      LineNumberReader Lread = null;
      try{
         Lread =new LineNumberReader(new FileReader(EFile));
         String line;
         while ((line =Lread.readLine()) !=null){
              line = line.trim();
              if(Lread.getLineNumber() < beginLine  ) continue;
              if (line.startsWith("#") || line.length() == 0) continue;
              if(line.indexOf(key) > -1 ){
                  ScanLine = Lread.getLineNumber();
                 //  if(Log.LOG_VERBOSE_DEBUG) Log.debug("find at line ("+ScanLine+")"+" Key :"+ key);
                     return true;
              }
              if(EndLine !=0 && Lread.getLineNumber() == EndLine){
                 //if(Log.LOG_DEBUG) Log.debug("Error :Can not find key <"+key+"> in file <"+EFile.getName()+"> between ("+beginLine+":"+EndLine+")" );
                return false;

              }
        }
     }
     catch (IOException e) {
           //if(Log.LOG_ERROR) Log.error("Error :Can not find Key",e);
            return false;
     }
     catch (SecurityException se) {
          //if(Log.LOG_ERROR) Log.error("Error :Can not find Key",se);
           return false;
     }
    //added by amaleh on 2004-02-05 - close file
     finally
     {
         try
         {
            if (Lread != null)
            {
             Lread.close();
            }
         }
         catch(IOException ioe)
         {
             //if(Log.LOG_ERROR) Log.error("Error :Cannot close EH file",ioe);
         }
     }
     return false;
  }
  public boolean getMainkeysPos(String Type){
    if(Type == "forms"){
        if(!Findkey("[E&H Forms File]",1,0)){
           return false;
       }else
            if(Findkey("[forms]",1,0))  PosStart = ScanLine;
            else return false;

    }else if(Type =="docs"){
         if(!Findkey("[E&H Documents File]",1,0)){
             return false;
         }else
             if(Findkey("[documents]",1,0)) PosStart = ScanLine;
             else return false;

    }
    return true;
  }
  public Vector getNamesOf(String Type,int StartLine,int EndLine){
     Vector Names = new Vector();
    if(PosStart > 0) {
        String SNamesCount = getValue("count",StartLine,EndLine);
        if(!SNamesCount.equals("<!NF!>")){
            for(int i=0;i<Integer.parseInt(SNamesCount);i++){
                  String NameKey  =Type+Integer.toString(i+1);
                  String value =getValue(NameKey,StartLine,EndLine);
                if(! value.equalsIgnoreCase("<!NF!>") ){
                     StartLine = ScanLine;
                     Vector Nm =new Vector() ;
                     Nm.add(0,value);
                     Nm.add(1,new Integer(StartLine));
                     Names.addElement(Nm);
                }
            }

        }
    }
     return Names;
  }

  public Vector getIndicesOf(String Type,int NUM,int StartLine,int EndLine){
      int i=0;
      Vector NameInds =new Vector();
      while(EndLine ==0 || StartLine < EndLine){
          String NmFieldKey  =Type+Integer.toString(NUM)+".field"+Integer.toString(i+1);
          String NmFieldData =getValue(NmFieldKey,StartLine,EndLine);

          if(!NmFieldData.equalsIgnoreCase("<!NF!>")){
              if(Type == "f"){
                    AIPField fField =new AIPField();
                     StringTokenizer Stoken =new StringTokenizer(NmFieldData,SepChar);
                   if(Stoken.hasMoreTokens()) fField.setName(Stoken.nextToken().trim());
                   if(Stoken.hasMoreTokens()) fField.setType(Integer.parseInt(Stoken.nextToken().trim()));
                     fField.setOrder(i);
                     NameInds.addElement(fField);
              }else if(Type == "doc"){
                    AIPField docField =new AIPField();
                    docField.setId(0);
                    docField.setOrder(i);
                    docField.setValue(NmFieldData);
                    docField.setStatus(docField.ACCEPTED);
                    NameInds.addElement(docField);
              }else  NameInds.addElement(NmFieldData);
              i++;
              StartLine = ScanLine;
          }
          else break;
      }
      return NameInds;
  }

  public TreeMap getForms(){
    //if(Log.LOG_DEBUG) Log.debug("Get E&H File Forms");
    TreeMap TM = new TreeMap();
    Vector FNs = getNamesOf("form",PosStart,0);
    if(FNs.size() >0){
      for(int i=0;i< FNs.size();i++){
          Vector FN =(Vector)FNs.get(i);
          if(FN.elementAt(0).toString().trim().equals("")) continue;
          int SLine =((Integer)FN.elementAt(1)).intValue();
          int EndLine = 0;
          if(i+1 < FNs.size())
           EndLine =((Integer)((Vector)FNs.get(i+1)).elementAt(1)).intValue();
           AIPForm FInfo =new AIPForm();
            FInfo.setName(FN.elementAt(0).toString());
           Vector FIndxs = getIndicesOf("f",i+1,SLine,EndLine);
           FInfo.setFields(FIndxs);
          TM.put(FN.elementAt(0),FInfo);
      }
    }
    //if(Log.LOG_VERBOSE_DEBUG) Log.debug("<"+TM.size()+"> E&H Form have been extracted");
    return TM;
  }

  public TreeMap getDocuments(){
    //if(Log.LOG_DEBUG) Log.debug("Get EHFile Documents");
    TreeMap TM = new TreeMap();
    Vector DocNs = getNamesOf("document",PosStart,0);
    if(DocNs.size() >0){
      for(int i=0;i< DocNs.size();i++){
          Vector DocN =(Vector)DocNs.get(i);
          if(DocN.elementAt(0).toString().trim().equals("")) continue;
          int SLine =((Integer)DocN.elementAt(1)).intValue();
          int EndLine = 0;

          if(i+1 < DocNs.size())
           EndLine =((Integer)((Vector)DocNs.get(i+1)).elementAt(1)).intValue();
           AIPDocument aipDoc =new AIPDocument();
           aipDoc.setName("Document"+Integer.toString(i+1));
           aipDoc.setId(0);
           aipDoc.setFileId(0);
           aipDoc.setStatus(0);
           String FormLoc=DocN.elementAt(0).toString();
           aipDoc.getAIPForm().setName(FormLoc.substring(0,FormLoc.indexOf(SepChar)).trim());
           String ImagePath = AIPUtil.cleanTabInString(FormLoc.substring(FormLoc.indexOf(SepChar)).trim());
           aipDoc.setImagesPath(ImagePath);
           aipDoc.getAIPForm().setFields(getIndicesOf("doc",i+1,SLine,EndLine));///////////////////
           //added by amaleh on 2004-01-23
           //adds missing fields from non-templates mode as empty strings
           aipDoc.setFieldNames("");
           aipDoc.setVWNodePath("");
           //
           //added by amaleh on 2004-02-09
           //read optional values
           
           //parse path to comment file of which contents will be added to document
           String commentFileKey = "doc" + Integer.toString(i+1) + ".comment_file";
           String commentFileValue = getValue(commentFileKey, SLine, EndLine);
           aipDoc.setCommentFile(commentFileValue);

           //parse option that specifies whether to append processed doc to a similar existing one
           String appendKey = "doc" + Integer.toString(i+1) + ".append";
           String appendValue = getValue(appendKey, SLine, EndLine);
           boolean appendBoolean = Boolean.valueOf(appendValue).booleanValue();
           aipDoc.setAppend(appendBoolean);
           
           //parse option that specifies whether to delete images after processing
           String deleteImagesKey = "doc" + Integer.toString(i+1) + ".delete_images";
           String deleteImagesValue = getValue(deleteImagesKey, SLine, EndLine);
           boolean deleteImagesBoolean = Boolean.valueOf(deleteImagesValue).booleanValue();
           aipDoc.setDeleteImages(deleteImagesBoolean);

           TM.put("document"+Integer.toString(i),aipDoc);

      }
      //if(Log.LOG_VERBOSE_DEBUG) Log.debug("<"+TM.size()+"> Documents have been extracted");
    }
    return TM;
  }
//--------------------------------------------------------------------
  class EHFilesFilter implements FileFilter{
      public EHFilesFilter(){}
      public boolean accept(File pathname){
         if(pathname.isDirectory()) return false;
         String FName = pathname.getName();
         int i = FName.lastIndexOf('.');
         if (i > 0 &&  i < FName.length() - 1) {
            String extension = FName.substring(i+1).toLowerCase();
            if(extension.equalsIgnoreCase(DocsFileExt))
                 return true;
         }
         return false;
      }

  }
}