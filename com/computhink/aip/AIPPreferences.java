/*
 * AIPPreferences.java
 *
 * 
 */
package com.computhink.aip;

import java.util.prefs.*;
import java.util.Vector;

import com.computhink.aip.util.AIPUtil;
import com.computhink.aip.util.VWLogger;
//import java.util.*;
import com.computhink.common.Constants;

public final class AIPPreferences 
{
    private static Preferences AIPPref;
    public static final String DEFAULT_DAILYFREQUENCY = "6:01:01";
    public static final String DEFAULT_PROCESSING_PENDING_DOCS= "00:45:00";
    public static final int DEFAULT_POLLING_MODE = 0;
    public static final int DEFAULT_Conversion_MODE=0;
    public static final int DELAY_TIME=120000;
    
    static
    {
        AIPPref= Preferences.systemRoot().node("/computhink/"+ Constants.PRODUCT_NAME.toLowerCase() +"/aip");
        //AIPPref= Preferences.userRoot().node("/computhink/"+ Constants.PRODUCT_NAME.toLowerCase() +"/aip");
    }
    private static Preferences getNode(String node)
    {
        return AIPPref.node(node);
    }
    protected static String getLoggin()
    {
        return getNode("general").get("authenhist","");        
    }    
    protected static void setLoggin(String s)
    {
         Preferences genPref = getNode("general"); 
         genPref.put("authenhist",s);
    }
    protected static void setConvertToDJVU(int ret)
    {
         Preferences genPref = getNode("general"); 
         genPref.putInt("convert_to_djvu",ret);        
    }
    protected static int isConvertToDJVU()
    {
        return getNode("general").getInt("convert_to_djvu",0); 
    }
      /*  Issue - 742
  *   Developer - Nebu Alex 
  *   Code Description - The following piece of code implements
  *                      Generate primitive document functionalities 
  */
    protected static void setPrimitiveDocs(int ret)
    {
         Preferences genPref = getNode("general"); 
         genPref.putInt("primitivedocs",ret);        
    }
    protected static int isPrimitiveDocs()
    {
        return getNode("general").getInt("primitivedocs",0); 
    }
    // End of Issue - 742 
    protected static void setEnableLog(boolean enable)
    {
        Preferences genPref = getNode("log"); 
         genPref.putBoolean("enable",enable);
    }  
    protected static boolean isEnableLog()
    {
         return getNode("log").getBoolean("enable",false); 
    }
    protected static boolean isEnableTimerLog()
    {
         return getNode("log").getBoolean("enable",false); 
    }
    protected static void setLogFile(String logPath)
    {
        Preferences genPref = getNode("log"); 
         genPref.put("filepath",logPath);
    }  
    protected static String getLogFile()
    {
         return getNode("log").get("filepath",""); 
    }    
    protected static int getLogLevel()
    {
        return getNode("log").getInt("level",2); 
    } 
    protected static void setLogLevel(int level)
    {
         Preferences genPref = getNode("log"); 
         genPref.putInt("level",level);        
    }
    /*
     * Enhancement 	: Enhancements of AIP  
     * 				  Requirment sent by Rajesh on 09-June-2006 through eMail  	 
     * Created By	: M.Premananth
     * Date			: 14-June-2006
     */
    protected static void setMaximumRow(int count)
    {
         Preferences genPref = getNode("general"); 
         genPref.putInt("maximumrow",count);
    }
    protected static int getMaximumRow()
    {
        return getNode("general").getInt("maximumrow",0);        
    }
    
    protected static void setDocumentDelay(int count){
    	Preferences genPref = getNode("general");
    	genPref.putInt("documentdelay", count);
    }
    protected static int getDocumentDelay(){
    	return getNode("general").getInt("documentdelay", 0);
    }
    protected static void setArchivalFolder(String path)
    {
         Preferences genPref = getNode("general"); 
         genPref.put("archivalfolder",path);
    }
    protected static String getArchivalFolder()
    {
        return getNode("general").get("archivalfolder","");        
    }
    protected static void setAgeForArchive(int age)
    {
         Preferences genPref = getNode("general"); 
         genPref.putInt("archiveage",age);
    }
    protected static int getAgeForArchive()
    {
        return getNode("general").getInt("archiveage",24);        
    }
    protected static void setArchiveCount(int count)
    {
         Preferences genPref = getNode("general"); 
         genPref.putInt("archivecount",count);
    }
    protected static boolean setArchiveHistoryEnabled(Session s,boolean enable)
    {
        String serverRoom =s.server+"."+s.room;
        Preferences EnableFolderPref = getNode("enabledarchive");
        try{
        	Preferences ServerRoomPref =EnableFolderPref.node(serverRoom);
        	ServerRoomPref.putBoolean("archivehistory",enable);
        }catch(Exception ex){
        	return false;
        }
        return true;
    }
    protected static boolean isArchiveHistoryEnabled(Session s)
    {
        String serverRoom =s.server+"."+s.room;
        Preferences EnableFolderPref = getNode("enabledarchive");
        Preferences ServerRoomPref =EnableFolderPref.node(serverRoom);
        return ServerRoomPref.getBoolean("archivehistory",false);
    }
    protected static int getArchiveCount()
    {
        return getNode("general").getInt("archivecount",500);        
    }
    // Archive history - End    
    protected static int getLogFileNo()
    {
        return getNode("log").getInt("fnum",5); 
    }
    protected static void setLogFileNo(int fNo)
    {
         Preferences genPref = getNode("log"); 
         genPref.putInt("fnum",fNo);        
    }    
    // Issue 562
    public static int getLogFileCount()
    {
        return getNode("log").getInt("maxlogfilecount",100); 
    }
    public static void setLogFileCount(int count)
    {
         Preferences genPref = getNode("log"); 
         genPref.putInt("maxlogfilecount",count);        
    } 
    // End of fix Issue 562
    protected static void setLogFileLength(int fLength)
    {
         Preferences genPref = getNode("log"); 
         genPref.putInt("flength",fLength);        
    }
    public static int getLogFileLength()
    {
        return getNode("log").getInt("flength",500000); 
    } 
    protected static boolean isLogAddMethodPath()
    {
        int Add =getNode("log").getInt("addmethodpath",0);
        return (Add ==1) ? true:false;
    }    
    protected static Vector getEnabledFolders(Session s)
    {
        Vector folders =new Vector();
        String serverRoom =s.server+"."+s.room;
        Preferences EnableFolderPref = getNode("enabledfolders");
        Preferences ServerRoomPref =EnableFolderPref.node(serverRoom);
        for(int i=1;i<12;i++)
        {
            String folder = ServerRoomPref.get("folder"+(i),"");
            if(folder !=null || !folder.trim().equals("")){
            	int poll =  ServerRoomPref.getInt("poll",0);
            	folders.add(folder+"\t"+poll);
            }
        }    
        return folders;        
    }    
    protected static boolean putEnabledFolders(Session s,Vector folders)
    {        
        if(folders ==null) return true;
        String serverRoom =s.server+"."+s.room;
        Preferences EnableFolderPref = getNode("enabledfolders");
        Preferences ServerRoomPref =EnableFolderPref.node(serverRoom);
        try{
            ServerRoomPref.removeNode();
            ServerRoomPref =EnableFolderPref.node(serverRoom);
            for(int i=0;i<folders.size();i++)
            {
            	 String folder =(String)folders.get(i);                 
                 if(folder !=null || !folder.trim().equals(""))
                 {  
                    int poll =0;
                    String fPath =folder;
                    int tabLoc = folder.indexOf("\t");
                    if( tabLoc>0)
                    {
                        fPath =folder.substring(0,tabLoc);
                        poll =AIPUtil.to_Number(folder.substring(tabLoc+1,folder.length()));
                    }    
                    ServerRoomPref.put("folder"+(i+1),fPath);                                        
                    //ServerRoomPref.putInt("poll"+(i+1),poll);
                    ServerRoomPref.putInt("poll",poll);
                 }                    
            }               
        }
        catch(Exception e)
        {
           //e.printStackTrace();
            VWLogger.error("in put EnabledFolders for room <"+serverRoom+">",e);
            return false;
        }    
        return true;    
    }   
    protected static void setProcessSubdirs(boolean ret)
    {
         Preferences genPref = getNode("general"); 
         genPref.putBoolean("process_subdirs",ret);        
    }
    protected static boolean isProcessSubdirs()
    {
        return getNode("general").getBoolean("process_subdirs",false); 
    }
    public static void setStartupRoom(String defaultRoom)
    {
         Preferences genPref = getNode("general"); 
         genPref.put("startuproom",defaultRoom);        
    }
    public static String getStartupRoom()
    {         
         return getNode("general").get("startuproom",""); 
    }

    protected static boolean isAIPApplicationStarted()
    {
        return getNode("AIPApplication").getBoolean("enable",false); 
    }    
    protected static void setAIPApplicationStarted(boolean flag)
    {
    	try{
    		Preferences genPref = getNode("AIPApplication"); 
    		genPref.putBoolean("enable",flag);
    	}catch(Exception e){
    		System.out.println("Exception in setAIPApplicationStarted : "+e.getMessage());
    		e.printStackTrace();
    	}
    }    
    protected static boolean isAIPServiceStarted()
    {
        return getNode("AIPService").getBoolean("enable",false); 
    }    
    protected static void setAIPServiceStarted(boolean flag)
    {
        Preferences genPref = getNode("AIPService"); 
        genPref.putBoolean("enable",flag);
    }
    public static boolean isSecurityForDoc()
    {
        return getNode("general").getBoolean("securityfordoc",false); 
    }    
    public static void setSecurityForDoc(boolean flag)
    {
        Preferences genPref = getNode("general"); 
        genPref.putBoolean("securityfordoc",flag);
    }        
    protected static void setScheduleTime(String frequency)
    {
        Preferences genPref = getNode("general"); 
        genPref.put("frequency", frequency);
    }  
    protected static String getScheduleTime()
    {
         return getNode("general").get("frequency", DEFAULT_DAILYFREQUENCY); 
    }
    protected static String getScheduleTimeForProcessingPendingDocs()
    {
         return getNode("general").get("pendingfrequency", DEFAULT_PROCESSING_PENDING_DOCS); 
    }
    protected static void setPollingMode(int pollingMode)
    {
        Preferences genPref = getNode("general"); 
        genPref.putInt("pollingmode", pollingMode);
    }  
    protected static int getPollingMode()
    {
         return getNode("general").getInt("pollingmode", DEFAULT_POLLING_MODE); 
    }
    protected static void setLastAIPProcessSuccess(boolean success)
    {
        Preferences genPref = getNode("general"); 
         genPref.putBoolean("lastAIPProcessSuccess",success);
    } 
    protected static boolean isLastAIPProcessSuccess()
    {
         return getNode("general").getBoolean("lastAIPProcessSuccess",false); 
    }
    protected static int getMaxLoginCount()
    {
         return getNode("general").getInt("maxlogincount",9999999); 
    }
    
    protected static int getMaxDocSize()
    {
         return getNode("general").getInt("maxdocsize", 100); 
    }
    /**
	 * Registry created to set the time delay while auto start during network delay
	 * @return
	 */
    public static int getAipAutoStartDelayTime() {
    	// TODO Auto-generated method stub
    	return getNode("general").getInt("autostartdelay", 100);
    }

    protected static boolean isLoginReTry()
    {
    	return getNode("general").getBoolean("aipretry",false); 
    }
    protected static int getComptession()
    {
    	return getNode("general").getInt("compression",2); 
    }

    protected static void setCompression(int ret)
    {
    	Preferences genPref = getNode("general"); 
    	genPref.putInt("compression",ret);        
    }
    
    protected static int getpageSize()
    {
    	return getNode("general").getInt("pagesize",10);        
    }
    protected static int getMinZipSize()
    {
         return getNode("general").getInt("minzipsize", 10); 
    }
    /*  protected static int getConversionMode()
    {
    	return getNode("general").getInt("conversionMode",DEFAULT_Conversion_MODE); 
    }

    protected static void setConversionMode(int ret)
    {
    	Preferences genPref = getNode("general"); 
    	genPref.putInt("conversionMode",ret);        
    }*/
	protected static void setSinglePageHcpdf(int enable) {
		Preferences genPref = getNode("general");
		genPref.putInt("SinglePageHcpdf", enable);
	}
	protected static int isSinglePageHcpdf() {
		return getNode("general").getInt("SinglePageHcpdf", 0);
	}
	protected static int getImageBackupDays() {
		try {
			return getNode("general").getInt("imagebackup", 5);
		} catch (Exception e) {
			return 5;
		}
	}
}

