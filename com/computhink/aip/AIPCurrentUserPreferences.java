package com.computhink.aip;

import java.util.prefs.Preferences;

import com.computhink.common.Constants;

public final class AIPCurrentUserPreferences {
	private static Preferences AIPPref;

	static {
		//AIPPref = Preferences.userRoot().node("/computhink/" + Constants.PRODUCT_NAME.toLowerCase() + "/aip");
		AIPPref = Preferences.systemRoot().node("/computhink/" + Constants.PRODUCT_NAME.toLowerCase() + "/aip");
	}

	private static Preferences getNode(String node) {
		return AIPPref.node(node);
	}

	public static void setDocumentName(String docName) {
		Preferences genPref = getNode("general");
		genPref.put("documentname", docName);
	}
	
	public static void setProcessDate(String date) {
		Preferences genPref = getNode("general");
		genPref.put("processdate", date);
	}
}
