/*
 * Session.java
 *
 * Created on December 21, 2003, 12:34 PM
 */

package com.computhink.aip;

import com.computhink.aip.util.AIPUtil;
/**
 *
 * @author  Administrator
 */
public class Session 
{
    public int sessionId = 0;
    public String server = "";
    public String room = "";
    public String cacheFolder = "";
    public String user = "";
    
    public Session() 
    {
    }
  
    public Session(int sessionId, String server, String room, String path, 
                                                                     String usr) 
    {
        this.sessionId = sessionId;
        this.server = server;
        this.room = room;
        this.cacheFolder = path + AIPUtil.pathSep + room; 
        this.user = usr;
    }
    
}
