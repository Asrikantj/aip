package com.computhink.aip;

import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.io.*;

import com.computhink.common.DocType;
import com.computhink.common.Document;
import com.computhink.common.Util;
import com.computhink.common.ViewWiseErrors;
import com.computhink.aip.util.VWPC1;

import com.computhink.aip.util.AIPUtil;
import com.computhink.aip.util.VWLogger;
import com.computhink.aip.util.VWZipUtilities;
import com.computhink.aip.util.vUtil;
import com.computhink.aip.util.database_rEntry;
import com.computhink.aip.util.pack_rEntry;
import com.computhink.aip.util.rampage_rHeader;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public class PagesManager {

 // static File EHTempFolder =null;
  public static int nConvertToDJVU =0; // 0 false, 1 true  
  public static int pendingErrorMessage =0; // 1-Image path is incorrect , 2-Image file not found, 3-Page file size mismatch(all.zip file is less than 10 bytes),
                                            // 4-Not enough memory available, 5-Page file size exceeded 200 MB, 6-Failed to process document, 7-FileName does not have extension, 8-Failed to process 0kb file..
  public PagesManager() {

  }
  static {
        String home =AIPUtil.findHome();
        if(home ==null || home.equals(""))
            home ="VWView.dll";
        else 
            home =home +AIPUtil.pathSep+"system"+AIPUtil.pathSep+"VWView.dll";        
        
          System.load(home);        
        

  }
  private static String getPagesTempFolder(int sid,int docId)
  {
      String folderPath =null;
      Session s =AIPManager.getSession(sid);
        if(s != null)                      
          folderPath = s.cacheFolder+AIPUtil.pathSep+docId; 
      return folderPath;
  }    
  public static String createPagesTempFolder(int sid,int docId){
        String folderPath =getPagesTempFolder(sid,docId);        
        if(folderPath == null || folderPath.equals(""))
            return null;           
        File f =new File(folderPath);
        if(!f.exists()) f.mkdirs();
        else AIPUtil.emptyDirectory(folderPath);
        return folderPath;        
  }
  private static boolean emptyPagesTempFolder(int sid,int docId){
       String folderPath =getPagesTempFolder(sid,docId);        
        if(folderPath == null || folderPath.equals(""))
            return false; 
        File f =new File(folderPath);
        if(f.exists()&& f.isDirectory()) 
         AIPUtil.emptyDirectory(folderPath);
        return true;  
  }
  public boolean deletePagesTempFolder(int sid,int docId){
       String folderPath =getPagesTempFolder(sid,docId);       
       VWLogger.info("folderPath in deletePagesTempFolder :" + folderPath);
        if(folderPath == null || folderPath.equals(""))
            return false; 
        File f =new File(folderPath);
        if(f.exists()&& f.isDirectory()) 
         AIPUtil.deleteDirectory(folderPath);
        return true; 
  }
  /**
   * Method created to validate all.zip from AIP temp location
   * Implemented date :-17-Jan-2019
   * @param sid
   * @param docId
   * @return
   */
	public List checkAllZipFilesAIPTemp(int sid, int docId) {
		List zipContentList = new ArrayList();
		/**CV2019 merges from CV10.2 line. Try catch block added**/
		try {
			String folderPath = getPagesTempFolder(sid, docId);
			VWLogger.info("sid ::" + sid + "docId :" + docId);
			VWLogger.info("Inside delete  aip TempFolder" + folderPath);
			if (folderPath != null && folderPath.length() > 0) {
				File f = new File(folderPath + "\\" + "all.zip");
				if (f.exists()) {
					VWLogger.info("before list files..");
					zipContentList = getAllZipFilesList(f.getAbsolutePath());
				}
			}
		} catch (Exception e) {
			zipContentList = null;
		}
		return zipContentList;
	}
  
  
  public ArrayList getAllZipFilesList(String srcPath) throws RemoteException{
  	ArrayList fileList = new ArrayList();
  	ZipFile zfile=null;
  	/**CV2019 merges from CV10.2 line. Try catch block added**/
  	try{
  		File allZipFile=new File(srcPath);
  	
  		if(allZipFile.exists()){
  			long zipSize=allZipFile.length();
  			//fileList.add(String.valueOf(zipSize));
  			zfile = new ZipFile(allZipFile);
  			if(zfile!=null){
  				java.util.Enumeration e = zfile.entries();
  				while (e.hasMoreElements())
  				{
  					ZipEntry zipEntry = (ZipEntry) e.nextElement();
  					//VWLogger.info("Zip file contents are ::"+zipEntry.getName());
  					fileList.add(zipEntry.getName()+Util.SepChar+zipEntry.getSize());
  				}
  			}

  		}
  	}catch(Exception e){
  		VWLogger.info("Exception while getting Zip file contents are ::"+e.getMessage());
  		return null;
  	}
  	finally{
  		try {
  			zfile.close();
  		} catch (IOException e) {
  			// TODO Auto-generated catch block
  			VWLogger.info("Exception while getting files list in fianlly"+e.getMessage());
  		}
  	}
  	return fileList;
  }
  
  
  
  
  
  
  

  /**
   * Added for sending the document to pending if the size is more than 100MB(Default)
   *  or based on the maxdocsize registry set value.
   * @param ImageFile
   * @return
   */
	public boolean getAllowedPageSize(String ImageFile, File[] PageFiles) {
		/**
		 * Following code commented bz we can pass PageFiles as a parameter from
		 * EHGeneralPanel calling method
		 **/
		// File PageFiles[]=parseImageFiles(ImageFile);
		VWLogger.info("inside getAllowedPageSize.......");
		pendingErrorMessage = 0;
		if (PageFiles != null && PageFiles.length > 0) {
			try {
				String PagesPath = "";
				int maxdocSize = AIPManager.getMaxDocSize();
				VWLogger.debug("maxdocsize allowed in MB::" + maxdocSize);
				for (int i = 0; i < PageFiles.length; i++) {
					File PageFile = PageFiles[i];
					// PageFile.length() return value will be in bytes. so have convert this from Bytes to MB(1024*1024)
					VWLogger.info("Image :<" + PageFile.getPath() + "> Size:<"	+ PageFile.length() / (1024 * 1024) + " MB>\t ");
					/**CV2019 merges from CV10.2 line. else condition added for pendcause error 5. Added by Vanitha S**/
					if ((PageFile.length() / (1024 * 1024)) <= 200) {
						if ((PageFile.length() / (1024 * 1024)) > (maxdocSize)) {							
							return false;
						}
					} else {
						pendingErrorMessage = 5; //Page file size exceeded 200 MB.
						return false;
					}
				}
			} catch (Exception e) {
				VWLogger.info("Exception while getAllowedPageSize " + e.getMessage());
			}
		}
		return true;
	}
  

  public boolean addPagesToTempFolder(int sid,int docId,File[] PageFiles) {
	  	pendingErrorMessage = 0;
		if (PageFiles == null || PageFiles.length == 0)
			return false;
		String folderPath = getPagesTempFolder(sid, docId);
		if (folderPath == null || folderPath.equals(""))
			return false;
		try {
			VWLogger.debug(System.lineSeparator() + "Location(" + (docId + 1) + "):" + "Add Pages To TempFolder <" + folderPath + ">");
			String PagesPath = "";
			for (int i = 0; i < PageFiles.length; i++) {
				File PageFile = PageFiles[i];
				VWLogger.debug("Page " + (i + 1) + " : " + PageFile.getPath() + "\t" + (PageFile.length() / 1024) + "."	+ (PageFile.length() % 1024) + "KB");
				PagesPath += PageFile.getPath();
				if (i != (PageFiles.length - 1))
					PagesPath += ";";

			}
			File f =new File(folderPath);
			VWLogger.info("folderPath exist :" + folderPath); 
	        if(!f.exists()) f.mkdirs();
	        VWLogger.info("Folder Permissions before calling Add_Page canWrite :" + f.canWrite()+", canRead :"+f.canRead()+", canExecute :"+f.canExecute());
			// Added the new overloading method with primitive as argument. To
			// improve the performance this argument is added.
			VWLogger.info("PagesPath before sending to native addpage method " + PagesPath);
			VWLogger.info("folderPath before sending to native addpage method " + folderPath);
			VWLogger.info("nConvertToDJVU......" + nConvertToDJVU);
			int primitiveDocs = AIPManager.isPrimitiveDocs();
			VWLogger.info("AIPManager.isPrimitiveDocs()......" + primitiveDocs);
			int singlePage = 0;
			if (nConvertToDJVU == 3)
				singlePage = AIPManager.isSinglePageHcpdf();
			VWLogger.info("Single page hcpdf :" + singlePage);
			int status = Add_Page(PagesPath, folderPath, nConvertToDJVU, primitiveDocs);
			//int status = Add_Page(PagesPath, folderPath, nConvertToDJVU, primitiveDocs, singlePage);
			VWLogger.info("Add_Page return value ....." + status);
			// status = 1:Successfully created all.zip file, 2:Not enough memory available, 0:Failed to create all.zip
			/**CV2019 merges from CV10.2 line. Added for pendcause error messages 3,4 and 6. Added by Vanitha S**/
			if (status != 1) {
				if (status == 2) {
					pendingErrorMessage = 4;
				} else {
					pendingErrorMessage = 6;
				}
			}
			VWLogger.info("Folder Permissions after calling Add_Page canWrite :" + f.canWrite()+", canRead :"+f.canRead()+", canExecute :"+f.canExecute());
			File folderPathFile = new File(folderPath + Util.pathSep + "all.zip");
			VWLogger.debug("is all.zip File Exist : " + folderPathFile.exists());
			if (folderPathFile.exists()) {
				boolean isAllowedsize = getAllowedZipSize(folderPathFile);
				VWLogger.info("Zip File size ...." + isAllowedsize);
				if (!isAllowedsize) {
					pendingErrorMessage = 3;// this is to send document to pending if all.zip is less than 10 bytes
				}
			} else {
				VWLogger.info("folderPathFile not exist ....");
				pendingErrorMessage = 6;
			}
		} catch (Exception e) {
			VWLogger.error("Location(" + (docId + 1) + "):" + " when adding pages ", e);
			return false;
		}
		if (pendingErrorMessage > 0) {
			return false;
		} else {
			return true;
		}
	}
  /*  Issue - 742
  *   Developer - Nebu Alex 
  *   Code Description - The following piece of code implements
  *                      Generate primitive document functionalities 
  */
  
	public boolean addPagesToTempFolderPrimitiveGeneration(int sid, int docId, File[] PageFiles) {
		VWLogger.info("addPagesToTempFolderPrimitiveGeneration.........."); 
		if (PageFiles == null || PageFiles.length == 0)
			return false;
		String folderPath = getPagesTempFolder(sid, docId);
		if (folderPath == null || folderPath.equals(""))
			return false;
		try {
			VWLogger.debug("Location(" + (docId + 1) + "):" + "add Pages To TempFolder PrimitiveGeneration <" + folderPath + ">");
			String PagesPath = "";
			for (int i = 0; i < PageFiles.length; i++) {
				File PageFile = PageFiles[i];
				VWLogger.debug("Page " + (i + 1) + " : " + PageFile.getPath() + "\t" + (PageFile.length() / 1024) + " KB");
				PagesPath += PageFile.getPath();
				if (i != (PageFiles.length - 1))
					PagesPath += ";";

			}
			VWLogger.debug("Before calling encriptPages.........");
			Vector vEncFiles = encriptPages(PagesPath, folderPath);
			VWLogger.debug("encriptPages return value :"+vEncFiles);
			if (vEncFiles == null) {
				VWLogger.error("Location(" + (docId + 1) + "):" + "Fail to encript pages", null);
				return false;
			}

			File zipFile = new File(folderPath + File.separator + "all.Zip");
			if (vEncFiles != null) {
				File[] aEncFiles = new File[vEncFiles.size()];
				for (int j = 0; j < vEncFiles.size(); j++) {
					aEncFiles[j] = (File) vEncFiles.get(j);
				}
				try {

					VWZipUtilities zip = new VWZipUtilities();//
					zip.compress(zipFile, aEncFiles);
					zip = null;
				} catch (Exception e) {
					// VWLogger.error("Location("+(docId+1)+"):"+ "Fail to
					// create pages zip file",e);
					return false;

				}
			}
			VWLogger.debug("is all.zip exist :"+zipFile.exists());
			if (zipFile.exists()) {
				Date ModifiedDate = new Date(zipFile.lastModified());
				String Modified = AIPUtil.SetDateMaskedValue(AIPUtil.MASKDATE, ModifiedDate);
				VWLogger.audit("Location(" + (docId + 1) + "):" + "Pages Zip File Created:<" + Modified + ">\t"
						+ " with size:<" + zipFile.length() + " bytes >");

				boolean isAllowedsize = getAllowedZipSize(zipFile);
				VWLogger.info("Zip File size ...." + isAllowedsize);
				if (!isAllowedsize) {
					pendingErrorMessage = 3;
					return false; // this is to send document to pending if
									// all.zip is less than 100 bytes
				}
			} else {
				VWLogger.error("Location(" + (docId + 1) + "):" + "Fail to create pages zip file", null);
				return false;
			}
		} catch (Exception e) {
			VWLogger.error("Location(" + (docId + 1) + "):" + " when adding pages ", e);
			e.printStackTrace();
			return false;
		}
		// VWLogger.debug("Location("+(docId+1)+"):"+"Pages added successfully
		// ");
		return true;
	}

	public Vector encriptPages(String pagesPath, String distFolder) {
		if (pagesPath == null || distFolder == null)
			return null;
		Vector encFiles = new Vector();
		try {
			int i = 0;
			Vector srcFilesName = new Vector();
			String info = "";
			String filesInfo = "";
			VWLogger.debug("pagesPath........."+pagesPath);
			StringTokenizer tok = new StringTokenizer(pagesPath, ";");
			while (tok.hasMoreTokens()) {
				String pagePath = tok.nextToken().trim();
				VWLogger.debug("Creating vw for the page :"+pagePath);
				int ind = pagePath.lastIndexOf(AIPUtil.pathSep);
				String pageName = pagePath.substring(ind + 1);
				i++;
				String encPath = distFolder + AIPUtil.pathSep + (i) + ".vw";
				VWLogger.debug("encPath before encrypting :"+encPath);
				try {
					VWLogger.info("pagePath before encrypting :" + pagePath);
					VWLogger.info("encPath before encrypting :" + encPath);
					AIPManager.getVWC().encryptVWFile(pagePath, encPath);
					/*long size = new File(pagePath).length();
					byte[] bytes = new byte[(int) size];
					FileInputStream in = new FileInputStream(pagePath);
					in.read(bytes);
					VWPC1 pc = new VWPC1();
					pc.encrypt(bytes);
					pc = null;
					FileOutputStream out = new FileOutputStream(encPath);
					String encHeader = "18f39b10-b4c7-11d9-9669-0800200c9a66";
					byte[] bencHeader = encHeader.getBytes("UTF-16");
	
					byte[] bhead = new byte[bencHeader.length - 2];
					System.arraycopy(bencHeader, 3, bhead, 0, bencHeader.length - 3);
					out.write(bhead);
					out.write(bytes);
					out.close();
					in.close();*/
				} catch (Exception e) { 
					e.printStackTrace();
					return null;
				}
				File encFile = new File(encPath);
				if (encFile.exists()) {
					encFiles.add(encFile);
					info += "," + i + ",0,W";
					srcFilesName.add(pageName);
				}
			}
			
			info = encFiles.size() + info + ",";
			StringBuffer infobuffer = new StringBuffer();
			infobuffer.append(info);
			try {
				String dbInfoPath = distFolder + AIPUtil.pathSep + "database.info";
				BufferedOutputStream abos = new BufferedOutputStream(new FileOutputStream(dbInfoPath));
				for (int k = 0; k < infobuffer.length(); k++)
					abos.write(infobuffer.charAt(k));
	
				abos.close();
	
			} catch (Exception e) {
				VWLogger.info("Exception in pageManager encriptPages 2 " + e.getMessage());
				e.printStackTrace();
			}
			File pagesInfoFile = generatePagesInfoFile(srcFilesName, distFolder);
			if (pagesInfoFile != null)
				encFiles.add(pagesInfoFile);
		} catch(Exception e) {
			VWLogger.info("Exception while encrypting pages :"+e);
		}
		return encFiles;
	}
  
  private static File generatePagesInfoFile(Vector srcFNames,String parentFolder)
  {     
      String infoFilePath =parentFolder+File.separator+"folder_database.info";
      try
      {
         int pagesNo =srcFNames.size(); 
         byte[] b_header = vUtil.getUnicodeData(
                                      "7C0F931B-2BE1-4693-B6C8-7A38AE7BF969");
         byte[] b_pagesNo =new byte[4];
         vUtil.putInt(b_pagesNo,0,pagesNo+1);                   
         FileOutputStream out = new FileOutputStream(infoFilePath);
         out.write(b_header);
         out.write(b_pagesNo);
         out.write(getDatabaseEntriesData(pagesNo,b_header.length+4));
         out.write(getRheaderData(pagesNo));
         out.write(getPackEntriesData(srcFNames));
         out.close();         
          
          
      }
      catch(Exception e)
      {
    	 VWLogger.info("generatePagesInfoFile : "+e.getMessage());
         e.printStackTrace(); 
         return null;
      } 
      File dbFile =new File(infoFilePath);
      return dbFile;
  } 
  private static byte[] getRheaderData(int pagesNo)
  {
      rampage_rHeader rh =new rampage_rHeader(pagesNo,pagesNo+2);
      byte[] data =rh.getObjectData();
      VWPC1 pc = new VWPC1();
      pc.encrypt(data);
      pc =null;
      return data;
  } 
  private static byte[] getPackEntriesData(Vector filesName)
  {
     int pagesNo = filesName.size();
     int dataLen = pagesNo*pack_rEntry.DATA_LEN;
     byte[] data =new byte[dataLen]; 
     int dataPos =0;
     for(int i=0;i<pagesNo;i++)
     {
       String pageName = (String)filesName.get(i);  
       pack_rEntry p_entry =new pack_rEntry(i, pageName);
       byte[] b_p_en_data =p_entry.getObjectData(); 
       VWPC1 pc = new VWPC1();
       pc.encrypt(b_p_en_data);
       pc = null;
       System.arraycopy(b_p_en_data,0,data,dataPos,b_p_en_data.length);
       dataPos +=b_p_en_data.length; 
     } 
    
     return data;
  }
  private static byte[] getDatabaseEntriesData(int pagesNo,int headerLen)
  {
      int dataLen =(pagesNo+1)*database_rEntry.DATA_LEN;
      byte[] data =new byte[dataLen];
      int offset = dataLen+headerLen;
      int dataPos =0;
      database_rEntry h_db_en =new database_rEntry(-5,offset,rampage_rHeader.DATA_LEN);
      h_db_en.nIndex1=-5;
      h_db_en.nIndex2=11;
      byte[] b_h_db_data =h_db_en.getObjectData();
      System.arraycopy(b_h_db_data,0,data,dataPos,b_h_db_data.length);
      dataPos += b_h_db_data.length;
      offset += rampage_rHeader.DATA_LEN;
      for(int i=0;i<pagesNo;i++)
      {
          database_rEntry p_db_en =new database_rEntry(i+1,offset,pack_rEntry.DATA_LEN);
          byte[] b_p_db_data =p_db_en.getObjectData(); 
          System.arraycopy(b_p_db_data,0,data,dataPos,b_p_db_data.length);
          dataPos += b_p_db_data.length;
          offset += pack_rEntry.DATA_LEN;
      }    
      return data;
  }
// End of fix
	public synchronized int addPagesToDocument(int sid, int docId, File[] PageFiles, Document vwDoc) {
		File ZipFile = null;
		boolean pageMismath = false;
		int status = 0;
		pendingErrorMessage = 0;
		try {
			// Adding the pages to the existing documents.
			if (PageFiles == null || PageFiles.length == 0)
				return ViewWiseErrors.Error;
			String folderPath = "";
			Session s = AIPManager.getSession(sid);
			if (s != null)
				folderPath = s.cacheFolder + AIPUtil.pathSep + docId;
			if (folderPath == null || folderPath.equals(""))
				return ViewWiseErrors.Error;
			String ZipFilePath = folderPath + File.separator + "all.Zip";
			ZipFile = new File(ZipFilePath);
			if (!ZipFile.exists())
				ZipFilePath = folderPath;

			Document doc = new Document(docId);
			doc.setDocType(new DocType(vwDoc.getDocType().getId(), vwDoc.getDocType().getName()));

			if (isDocumentLocked(sid, docId)) {
				VWLogger.info("Location(" + (docId + 1) + "):" + " Document is locked " + docId);
				return ViewWiseErrors.documentAlreadyLocked;
			}

			try {
				String PagesPath = "";
				for (int i = 0; i < PageFiles.length; i++) {
					File PageFile = PageFiles[i];
					PagesPath += PageFile.getPath();
					if (i != (PageFiles.length - 1))
						PagesPath += ";";

				}
				// Pass the Primitive argument as '0'.
				VWLogger.info("pagesPath before calling add_page native1 " + PagesPath);
				VWLogger.info("ZipFilePath before calling add_page native1 " + ZipFilePath);
				VWLogger.info("nConvertToDJVU....." + nConvertToDJVU);
				VWLogger.info("Folder Permissions before calling Add_Page canWrite :" + ZipFile.canWrite()+", canRead :"+ZipFile.canRead()+", canExecute :"+ZipFile.canExecute());
				int singlePage = 0;
				if (nConvertToDJVU == 3)
					singlePage = AIPManager.isSinglePageHcpdf();
				VWLogger.info("Single page hcpdf :" + singlePage);
				status = Add_Page(PagesPath, ZipFilePath, nConvertToDJVU, 0);
				//status = Add_Page(PagesPath, ZipFilePath, nConvertToDJVU, 0, singlePage);
				VWLogger.info("Add_Page return value ....." + status);
				VWLogger.info("Folder Permissions after calling Add_Page canWrite :" + ZipFile.canWrite()+", canRead :"+ZipFile.canRead()+", canExecute :"+ZipFile.canExecute());
		        //status =  1:Successfully created all.zip file, 2:Not enough memory available, 0:Failed to create all.zip
				/**CV2019 merges from CV10.2 line. Pendcause error messages added**/
				if (status != 1) {
					if (status == 2) {
						pendingErrorMessage = 4;				  
					} else {
						pendingErrorMessage = 6;
					}					
				}								
				if (ZipFile.exists()) {
					VWLogger.info("all zip exist");
				} else {
					VWLogger.info("all zip not exist");
					pendingErrorMessage = 6;
				}
			} catch (Exception e) {
				VWLogger.error("Location(" + (docId + 1) + "):" + " when adding pages ", e);
				e.printStackTrace();
				return ViewWiseErrors.Error;
			}
			VWLogger.info("pendingErrorMessage......"+pendingErrorMessage);
			if (pendingErrorMessage > 0) {
				return ViewWiseErrors.Error;
			}
			status = 0;

			// Pass the last argument as false for old document
			VWLogger.info("Before calling setDocFile in PagesManager ." + doc.getId());
			// VWLogger.info("s.cacheFolder in PagesManager
			// ."+s.cacheFolder+Util.pathSep+doc.getId()+Util.pathSep+"all.zip");
			List aipTempZipFilesList = checkAllZipFilesAIPTemp(sid, doc.getId());
			//VWLogger.info("aipTempZipFilesList :::" + aipTempZipFilesList);
			long vwPageSize = Long.parseLong(String.valueOf(AIPPreferences.getpageSize()));
			if (aipTempZipFilesList.size() > 0 && aipTempZipFilesList != null) {
				String vwFile[] = aipTempZipFilesList.get(0).toString().split(Util.SepChar);
				if (vwFile != null && vwFile.length > 0) {
					if (vwFile[0].contains(".vw")) {
						long vwFileSize = Long.parseLong(vwFile[1]);
						VWLogger.info("vwFileSize :::" + vwFileSize);
						if (vwFileSize >= vwPageSize) {
							pageMismath = false;
						} else {
							pageMismath = true;
						}

					}
				}
			}
			if (pageMismath == false) {
				VWLogger.info("page mismatch is false");
				status = AIPManager.getVWC().setDocFile(sid, doc, s.cacheFolder, false);
				VWLogger.info("setdocfile status :" + status);
				VWLogger.info("docId commented on " + docId);
			} else {
				return ViewWiseErrors.pageCountMismatch;
			}

			VWLogger.info("Before deleting aip temp in add pages of pagesManager");
			if (status == AIPManager.NOERROR) {
				deletePagesTempFolder(sid, docId);
			}

			if (status != AIPManager.NOERROR) {
				VWLogger.error("when adding pages to document " + docId + ",Error No:" + status, null);
				return status;
			}
		} catch (Exception e) {
			VWLogger.error("Exception while adding pages to document ", e);
		}
		return ViewWiseErrors.NoError;
	}
  
  public boolean isDocumentLocked(int sid, int docId)
  {	  
  	Vector owner = new Vector();
  	int ret = AIPManager.getVWC().getLockOwner(sid, new Document(docId), owner, true);
  	VWLogger.info("Document Id : ("+(docId)+"):"+" Document is locked " + !owner.isEmpty()+ " : " + owner.size());  	
  	return !owner.isEmpty();
  }
  
	public synchronized boolean pullDocumentPages(int sid, int docId) {
		/**CV2019 merges from CV10.2 line. Try catch block added**/
		boolean flag = true;
		try {
			Session s = AIPManager.getSession(sid);
			if (s == null)
				flag = false;
			String folderPath = s.cacheFolder;
			String docFolderPath = folderPath + AIPUtil.pathSep + docId;
			VWLogger.info("Document Id (" + (docId) + "):" + " downloading Document to location  " + docFolderPath);
			File f = new File(docFolderPath);
			if (!f.exists())
				f.mkdirs();
			else
				AIPUtil.emptyDirectory(docFolderPath);
			// pull old pages
			VWLogger.info("Is all.zip placed :" + f.exists());
			Document doc = new Document(docId);
			int status = AIPManager.getVWC().getDocFile(sid, doc, docFolderPath);
			VWLogger.info("getDocFile method return value :" + status);
			if (status != AIPManager.NOERROR) {
				flag = false;
				/**Added to fix - AIP creating document with 0 pages when the DSS is inactive issue. Added by Vanitha.S on 17/01/2020**/
				pendingErrorMessage = status;
			}
		} catch (Exception e) {
			VWLogger.info("Exception in pullDocumentPages :" + e.getMessage());
			flag = false;
		}
		return flag;

	}
 
 /* public boolean addPagesToDocument(int sid,int docId)
  {
        Session s =AIPManager.getSession(sid);
        if(s == null)             
            return false; 
        String folderPath =s.cacheFolder;
        Document doc = new Document(docId);
        
        int status = AIPManager.getVWC().setDocFile(sid, doc, folderPath);      
        deletePagesTempFolder(sid,docId);
        if(status !=AIPManager.NOERROR)
        {    
            return false;
        }    
      return true;
  }  
  public boolean pullOldPages(int sid,int docId)
  {         
            String folderPath =getPagesTempFolder(sid,docId);          
            if(folderPath == null || folderPath.equals(""))
              return false;             
            Document doc =new Document(docId);
            int status =AIPManager.getVWC().getDocFile(sid,doc,folderPath);
            if(status !=AIPManager.NOERROR)
            {    
           //     javax.swing.JOptionPane.showMessageDialog(new javax.swing.JPanel(),AIPManager.getErrorDescription(status)+"<"+status+">",AIPUtil.getString("Init.Msg.Title"),javax.swing.JOptionPane.ERROR_MESSAGE);
                return false;
            }    
        
            Unzip_Document(folderPath + File.separator + "all.Zip");
            String ZipFilePath = folderPath+File.separator+"all.Zip";
            File ZipFile = new File(ZipFilePath);
            if(ZipFile.exists())
                ZipFile.delete();
            return true;
  }
  */ 
	public static File[] parseImageFiles(String PagesPaths) { 
		VWLogger.info("parseImageFiles method pagesPaths " + PagesPaths);
		/**CV2019 merges from CV10.2 line**/
		File[] imageFilesArray = null;		
		/** a file filter that filters documents according to an extension */
		/*
		 * class FileExtFilter implements FileFilter { private String ext;
		 * FileExtFilter(String ext) { this.ext = ext; } public boolean
		 * accept(File file) { String path = file.getPath(); if
		 * (path.toUpperCase().endsWith(ext.toUpperCase())) return true; else
		 * return false; } }
		 */
		pendingErrorMessage = 0;		
		class WildcardFilter implements FilenameFilter {
			private String filter;

			public WildcardFilter(String filter) {
				this.filter = filter.toLowerCase();
			}

			public boolean accept(File dir, String name) {
				int idx = 0;
				try {
					name = name.toLowerCase();
					boolean wild = false;
					StringTokenizer tokens = new StringTokenizer(filter, "*", true);
					while (tokens.hasMoreTokens()) {
						String token = tokens.nextToken();
						if (wild == true) {
							wild = false;
							if (name.indexOf(token, idx) > idx)
								idx = name.indexOf(token, idx);
						}
						if (token.equals("*"))
							wild = true;
						else if (name.indexOf(token, idx) == idx)
							idx += token.length();
						else
							break;
						if (!tokens.hasMoreTokens()) {
							if (token.equals("*") || name.endsWith(token))
								idx = name.length();
						}
					}
				} catch (Exception e) {
					VWLogger.info("Exception in WildcardFilter : "+e.getMessage());
				}
				if (name != null)
					return (idx == name.length());
				else
					return false;
			}
		}
		
		final String SepChars = "\";\"";
		// if(Log.LOG_DEBUG) Log.debug("Parsing Page Files <"+PagesPaths+">");
		StringTokenizer Stoken = new StringTokenizer(PagesPaths, SepChars);
		Vector imageFiles = new Vector();
		// extract individual image paths and create files out of them
		boolean fileExist = false;
		while (Stoken.hasMoreTokens()) {
			try {
				String PagePath = Stoken.nextToken();
				VWLogger.info("PagePath  " + PagePath);
				if (PagePath.trim().length() > 1 && PagePath.indexOf("*") == -1) {
					File checkFileExist = new File(PagePath);
					if (checkFileExist.exists()) {
						VWLogger.info("Inside checkFile exist is true  ");
						fileExist = true;
					}
				}

				if (!PagePath.trim().equals("")) {
					// check for any files with a wild card
					int xi = 0;
					if ((xi = PagePath.lastIndexOf("\\")) != -1) {
						String ext = PagePath.substring(xi + 1);
						int li = PagePath.lastIndexOf('\\');
						String parentPath = PagePath.substring(0, li);
						File parentFile = new File(parentPath);
						File[] files;
						// check if parent file exists and is a directory
						if (parentFile.exists() && parentFile.isDirectory())
							files = parentFile.listFiles(new WildcardFilter(ext));
						else {
							// if(Log.LOG_ERROR)Log.error("Page Files
							// <"+PagePath+">
							// not exists");
							pendingErrorMessage = 1;
							return null;
						}
						for (int i = 0; i < files.length; i++) {
							// if(Log.LOG_DEBUG) Log.debug("Page File
							// <"+files[i].toString() +"> is valid" );
							imageFiles.addElement(files[i]);
						}
						continue;
					}
					File PageFile = new File(PagePath);
					if (PageFile.exists()) {
						VWLogger.info("Inside page file exist...");
						// check for any files within a directory
						if (PageFile.isDirectory()) {
							File[] files = PageFile.listFiles();
							for (int i = 0; i < files.length; i++) {
								if (!files[i].isFile())
									continue;
								// if(Log.LOG_DEBUG) Log.debug("Page File
								// <"+files[i].toString() +"> is valid" );
								VWLogger.info("File names if pages is directory" + files[i].getName());
								imageFiles.addElement(files[i]);
							}
							continue;
						}
						// check for normal files
						else if (PageFile.isFile()) {
							// if(Log.LOG_DEBUG) Log.debug("Page File
							// <"+PageFile.toString() +"> is valid" );
							VWLogger.info("File names if pages is file" + PageFile);
							imageFiles.addElement(PageFile);
						}
					} else {
						pendingErrorMessage = 1;
						return null;
					}
				}
			}catch (Exception e) {
				VWLogger.info("Exception in parseImageFiles :"+e.getMessage());		
			}
		}

		VWLogger.info("imageFiles size is " + imageFiles.size());
		boolean extensionAvailable = true;
		if (imageFiles != null && imageFiles.size() > 0) {
			imageFilesArray = new File[imageFiles.size()];
			File imageFile = null;
			int j = 0;
			for (int i = 0; i < imageFiles.size(); i++) {
				if (imageFiles.elementAt(i).toString().indexOf(".") > 0) {
					imageFile = (File) (imageFiles.elementAt(i));
					if (imageFile.length() > 0) {
						if (!imageFile.getName().contains("thumbs.db")) {
							imageFilesArray[j] = imageFile;
							VWLogger.info("imageFilesArray value of " + j + "is :: " + imageFilesArray[j]);
							j++;
						}
					} else {
						pendingErrorMessage = 8;
						return null;
					}
				} else {
					extensionAvailable = false;
					break;					
				}
				
			}
		} else {
			pendingErrorMessage = 2;
			return null;
		}
		VWLogger.info("File extension available :" + extensionAvailable);
		if (!extensionAvailable) {
			pendingErrorMessage = 7;
			return null;
		}
		/*
		 * This code added for fixed the issue reported in the Census 1052
		 * imageFilesArray != null changed to equal to null.
		 */
		/*
		 * VWLogger.info("imageFilesArray.length  is "+imageFilesArray.length );
		 * if (!fileExist && ( imageFilesArray == null && imageFilesArray.length
		 * ==0 )) { VWLogger.info(
		 * "Image path is not exists; Length of the image list is " +
		 * imageFilesArray.length); return null; }
		 */
		VWLogger.info("Final image files list :"+imageFilesArray);
		return imageFilesArray;
	}
  
 /* CV10.2 - Added for sending the document to pending if the zip size is more than 100MB(Default)
  *  or based on the maxzipsize registry set value.
  * @param ImageFile
  * @return
 */
 public boolean getAllowedZipSize(File zipFile){
	VWLogger.info("inside getAllowedZipSize method.........");
	boolean isAllowedSize = true; 
	try {
		long zipSize = zipFile.length();
		VWLogger.info("zipSize size  :: "+zipSize);
		int minZipSize = AIPManager.getMinZipSize();
		if (zipSize < minZipSize) { // zipSize & minZipSize will be in bytes
			isAllowedSize = false;
		}
	} catch (Exception e) {
		VWLogger.info("Exception in getAllowedZipSize method : "+e.getMessage());
		isAllowedSize = false;
	}
	 return isAllowedSize;
 }
 
 public File moveBifFileToImageLocation(File bifFile) {
	 String home = AIPUtil.findHome();
	 String imageTempFolder = home + AIPUtil.pathSep + "AIPImageTemp" + AIPUtil.pathSep + bifFile.getName();
	 File tempBifFile = new File(imageTempFolder);
	 if (tempBifFile.exists()) {
		 tempBifFile.delete();
	 }
	 tempBifFile.getParentFile().mkdirs();
	 boolean f = AIPUtil.CopyFile(bifFile, tempBifFile);
	 VWLogger.info("is bif file "+bifFile+" moved to AIPImageTemp folder :"+f);
	 return tempBifFile;
 }
 
 public String createImageTempLocation(File bifFile, String bifFileName) {
		String imageTempPath = null; 
		String imageTempFolder = null;
		File imageTemp =  null;
		try {
			String home = AIPUtil.findHome();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HHmmss");			
			Date date1 = new Date();
			String currentDate = dateFormat.format(date1);
			String dateFolder = currentDate.split(" ")[0];
			String timeFolder = currentDate.split(" ")[1];
			imageTempFolder = home + AIPUtil.pathSep + "AIPImageTemp" + AIPUtil.pathSep + dateFolder;
			if (bifFile != null) {
				bifFileName = bifFile.getName().substring(0, bifFile.getName().indexOf('.'));
				imageTempFolder = imageTempFolder + AIPUtil.pathSep + bifFileName+"_"+timeFolder;
				imageTempPath = imageTempFolder + AIPUtil.pathSep + bifFile.getName();	
				VWLogger.info("AIP Image temp location :" + imageTempPath);
				imageTemp = new File(imageTempPath);
				imageTemp.getParentFile().mkdirs();
				boolean f = AIPUtil.CopyFile(bifFile, imageTemp);
				VWLogger.info("is Bif file copied :" + f);
			} else {
				imageTempFolder = imageTempFolder + AIPUtil.pathSep + "Pending"+ AIPUtil.pathSep + bifFileName+"_"+timeFolder;
				VWLogger.info("AIP Image temp folder location :" + imageTempFolder);
				imageTemp = new File(imageTempFolder);
				imageTemp.getParentFile().mkdirs();
				VWLogger.info("Image temp folder exist 1:" + imageTemp.exists());
				imageTemp.mkdir();
				VWLogger.info("Image temp folder exist 2:" + imageTemp.exists());
			}
		} catch (Exception e) {
			VWLogger.info("Exception while creating image temp location :" + e);
		}
		return imageTempFolder;
	}
	
	public void moveImagesToTempLocation(File[] imageArray, String imageTempLocation, String documentName) {
		VWLogger.info("Moving images to temp location.........");
		try {
			String tempLocation = null;
			File tempImgFile = null;
			for (File imageFile : imageArray) {
				tempLocation = imageTempLocation;
				VWLogger.info("Moving image file " + imageFile.getName() + " to temp location "+tempLocation);
				tempImgFile = new File(tempLocation + AIPUtil.pathSep + imageFile.getName());
				VWLogger.info("Is file exist : " + tempImgFile.exists());				
				if (tempImgFile.exists()) {
					tempImgFile = new File(tempLocation + AIPUtil.pathSep + documentName+"_"+imageFile.getName());
				}
				boolean isCopied = AIPUtil.CopyFile(imageFile, tempImgFile);
				VWLogger.info("Is file copied : " + isCopied);
			}
		} catch (Exception e) {
			VWLogger.info("Exception while moving images to temp location :" + e);
		}
	}
  
  //public native void Add_Begin(String folder);
  public native void Add_Page(String SourceFile,String DestFolder,int nConvDJVU);
  // Added the new overloading method with primitive as argument. To improve the performance this argument is added.
  public native int Add_Page(String SourceFile,String DestFolder,int nConvDJVU, int nPrimitive);
  //Added the new overloading method with create single page hcpdf as argument.


  //public native void Add_End(String SourceFolder);  
  //public native void Unzip_Document(String ZipFile);

}
