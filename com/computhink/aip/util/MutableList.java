/*
 * MutableList.java
 *
 * Created on September 26, 2003, 3:55 PM
 */

package com.computhink.aip.util;

import javax.swing.JList;
import javax.swing.DefaultListModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import java.awt.Component;
import com.computhink.aip.resource.images.ImageManager;
import com.computhink.aip.AIPManager;
/**
 *
 * @author  amaleh
 */
public class MutableList extends JList {
    public MutableList() {
	super(new DefaultListModel());
        setCellRenderer(new MutableListRenderer());
       // setBorder(new javax.swing.border.EmptyBorder(1,2,1,1));
        setFixedCellHeight(18);
    }
    public DefaultListModel getContents() {
	return (DefaultListModel)getModel();
    }
    class MutableListRenderer extends DefaultListCellRenderer {
        private ImageManager IM =new ImageManager();
        private ImageIcon chekedIcon =IM.getImageIcon("checked.gif");    
        private ImageIcon unCheckedIcon = IM.getImageIcon("notChecked.gif");    
        
         public MutableListRenderer() {
             setBorder(new javax.swing.border.EmptyBorder(0,2,0,0));
            // setOpaque(true);
         }
         public Component getListCellRendererComponent(
             JList list,
             Object value,
             int index,
             boolean isSelected,
             boolean cellHasFocus)
         {
            // setText(value.toString());
             
             super.getListCellRendererComponent(list,value,index,isSelected,cellHasFocus);
              LocationStatus locStatus =AIPManager.getLocationStatus(AIPManager.getCurrSid(),(String)value);              
              if(locStatus == null)
                setIcon(unCheckedIcon);
              else
              setIcon((locStatus.isEnabledProcess()) ? chekedIcon : unCheckedIcon);
                
              
             return this;
         }
    }

}   