/*
 * vUtil.java
 *
 * Created on August 21, 2005, 5:54 PM
 */

package com.computhink.aip.util;

/**
 *
 * @author  Administrator
 */
public class vUtil {
    
    /** Creates a new instance of vUtil */
    public vUtil() {
    }
    public static boolean getBoolean(byte[] b, int off) {
	return b[off] != 0;
    }   
    
    public static short getShort(byte[] b, int off) {
	return (short) (((b[off + 0] & 0xFF) << 0) + 
			((b[off + 1] & 0xFF) << 8));
    }
    
    public static int getInt(byte[] b, int off) {
	return ((b[off + 0] & 0xFF) << 0) +
	       ((b[off + 1] & 0xFF) << 8) +
	       ((b[off + 2] & 0xFF) << 16) +
	       ((b[off + 3] & 0xFF) << 24);
    }
    
    public static void putBoolean(byte[] b, int off, boolean val) {
	b[off] = (byte) (val ? 1 : 0);
    }
    
    public static void putShort(byte[] b, int off, short val) {
	b[off + 0] = (byte) (val >>> 0);
	b[off + 1] = (byte) (val >>> 8);
    }

    public static void putInt(byte[] b, int off, int val) {
	b[off + 0] = (byte) (val >>> 0);
	b[off + 1] = (byte) (val >>> 8);
	b[off + 2] = (byte) (val >>> 16);
	b[off + 3] = (byte) (val >>> 24);
    }
   
    public static byte[] getUnicodeData(String s) throws Exception
    {
       if(s !=null )
       {    
           byte[] uni_h =s.getBytes("UTF-16");                     
           byte[] uni =new byte[uni_h.length-2];
           System.arraycopy(uni_h,3,uni,0,uni_h.length-3);
           return uni;
       }              
       return null;
    }    
}
