/*
 * FileOperator.java
 *
 * Created on December 8, 2003, 3:43 PM
 */

package com.computhink.aip.util;

import java.io.*;

/**
 * An interface defined for classes that will operate on a particular file.
 * @author  amaleh
 */
public interface FileOperator 
{
    /**
     * Sets the file to be used.
     */
    public void setFile(java.io.File file) throws java.io.FileNotFoundException;
    
    /**
     * Returns the file to be used.
     */
    public java.io.File getFile();
}
