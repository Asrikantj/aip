package com.computhink.aip.util;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.swing.filechooser.*;
import javax.swing.JFileChooser;

import com.computhink.aip.resource.ResourceManager;

import java.io.*;



/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public class SystemExplorer extends JPanel  {
  private final ResourceManager connectorManager = ResourceManager.getDefaultManager();
  private JTree tree ;
  public int SNodeId;
  String DefaultFolder;
  private Vector NodsData;
  private DefaultMutableTreeNode MainNode;
  private JFileChooser fc;
  private FileSystemView FSysView;
  private DefaultTreeModel treeModel ;
  private TreeSelectionModel TreeSModel;
  private JLabel Title;
  public SystemExplorer (){
        setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
        Title =new JLabel(connectorManager.getString("SystemExplorer.SystemFolder"));
        Title.setAlignmentX(Component.LEFT_ALIGNMENT);
        add(Title);
        add(Box.createVerticalStrut(5));
        add(CreateTreeNode(),BorderLayout.CENTER);

  }
  public void setTitle(String T){
      Title.setText(T);
  }
  public void getDirectoryTree(DefaultMutableTreeNode Parent,boolean Expand){
        if(Parent.getChildCount() == 1 && ((DFile)((DefaultMutableTreeNode)Parent.getChildAt(0)).getUserObject()).istemp() )
            Parent.remove(0);
        DFile ParentFile =(DFile)Parent.getUserObject();

       // if(Log.LOG_VERBOSE_DEBUG) Log.debug("get Directory CHILD Tree of----:"+ParentFile.getfile().getPath());

        File[] dirs =FSysView.getFiles(ParentFile.getfile(),true);
        if(dirs.length > 0){
           for(int j=0;j<dirs.length;j++){
              if(dirs[j].isDirectory()){

                //if(Log.LOG_VERBOSE_DEBUG) Log.debug("Find Directory CHILD :"+dirs[j].getName());

                DefaultMutableTreeNode Child =new DefaultMutableTreeNode(new DFile(dirs[j],false,false));
                Parent.add(Child);
                if(Expand) tree.scrollPathToVisible(new TreePath(Child.getPath()));
                Child.add(new DefaultMutableTreeNode(new DFile(null,true,true)));
              }
           }
        }
  }

  private JScrollPane CreateTreeNode(){
            MainNode = new DefaultMutableTreeNode(new DFile(null,true,true));
            
            getFolderRoots();

            treeModel = new DefaultTreeModel(MainNode);
            tree = new JTree(treeModel) {
	          public Insets getInsets() {
		    return new Insets(5,5,5,5);
	           }
	    };
           tree.getSelectionModel().setSelectionMode
                (TreeSelectionModel.SINGLE_TREE_SELECTION);
            TreeSModel = tree.getSelectionModel();
            tree.setScrollsOnExpand(true);
            tree.addTreeSelectionListener(new TreeSelectionListener() {
                public void valueChanged(TreeSelectionEvent e) {
                    //DefaultMutableTreeNode node = (DefaultMutableTreeNode)
                      //       tree.getLastSelectedPathComponent();
                    //if(node != null && node != MainNode){

                      //EyesHandsManager.LinkedFolderPath =((DFile)node.getUserObject()).getfile().getPath();
                    //}
                }
            });
            tree.addTreeExpansionListener(new TreeExpand());
            tree.addTreeWillExpandListener(new TreeWillExpand());
            tree.setScrollsOnExpand(true);
            tree.setRootVisible(false);
            tree.setCellRenderer(new IconRenderer());
            tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
            tree.setEditable(false);
            tree.addMouseListener(new MouseAdapter(){
                public void mouseClicked(MouseEvent e){
                    /*int selRow =tree.getRowForLocation(e.getX(),e.getY());
                    if(selRow > -1){
                        Rectangle bounds= tree.getRowBounds(selRow);
                        System.out.println(selRow+":"+tree.getRowBounds(selRow)+":"+tree.getBounds().getWidth()+":"+(bounds.getX()+bounds.getWidth()));
                        //if(bounds.getX()+bounds.getWidth() > 250 ) ShowTip =true;
                        //else ShowTip =false;
                        tree.setToolTipText(tree.getSelectionPath().getLastPathComponent().toString());
                     // setToolTipText(tree.getSelectionPath().getLastPathComponent().toString());
                        //TreePath NodePath = tree.getPathForLocation(e.getX(),e.getY());
                    }*/
                }
            });
            //tree.putClientProperty("JTree.lineStyle", "Angled");
            tree.setRowHeight(17);
            JScrollPane SPane =new JScrollPane(tree);
            SPane.setAlignmentX(Component.LEFT_ALIGNMENT);
	    return SPane;
  }
  public void getFolderRoots(){
             fc =new JFileChooser();
             FSysView =fc.getFileSystemView();
             File[] Roots=File.listRoots();
             for(int i=0;i< Roots.length;i++){
                 DefaultMutableTreeNode root=new DefaultMutableTreeNode(new DFile(Roots[i],true,false)) ;
                 MainNode.add(root);
                 root.add(new DefaultMutableTreeNode(new DFile(null,true,true)));
             }
  }
  public void RefreshSystemTree(String FilePath){
        //System.out.println("before remove:"+MainNode.getChildCount());
        MainNode.removeAllChildren();
        treeModel.reload();
        MainNode = new DefaultMutableTreeNode();
        //System.out.println("after remove:"+MainNode.getChildCount());
        getFolderRoots();
        //System.out.println("after Load:"+MainNode.getChildCount());
        treeModel.reload(MainNode);
        //treeModel.reload(MainNode);
      /*  treeModel =null;
        MainNode = new DefaultMutableTreeNode();

        treeModel = new DefaultTreeModel(MainNode);
        tree.revalidate();
        tree.repaint();*/
        setSelectionFile(FilePath);
  }
  public int AddNodeTree(DefaultMutableTreeNode ParentNode,Object ObjectItem,boolean Expand,boolean gotonew){
                       DefaultMutableTreeNode node =new DefaultMutableTreeNode(ObjectItem);
	               treeModel.insertNodeInto(node,ParentNode,ParentNode.getChildCount());
                       if(Expand) tree.scrollPathToVisible(new TreePath(node.getPath()));
                       if(gotonew){

                            tree.setSelectionPath(new TreePath(node.getPath()));
                       }
      return -1;
  }
  public void setAllEnabled(boolean enable){
      //tree.setEnabled(enable);
      if(enable){
        tree.setSelectionModel(TreeSModel);
        tree.getSelectionModel().setSelectionMode
          (TreeSelectionModel.SINGLE_TREE_SELECTION);
      }else{
       //TreeSModel =tree.getSelectionModel();
       tree.setSelectionModel(null);
      }
  }
  public void UpdateNodeTree(DefaultMutableTreeNode node,Object UserObject){
      node.setUserObject(UserObject);
      treeModel.nodeChanged(node);
  }
  public boolean setSelectionFile(String FilePath){
        //if(FilePath.equals("")) return false;
       // if(Log.LOG_VERBOSE_DEBUG) Log.debug("set Selection File :"+FilePath);
        String Separetar= System.getProperty("file.separator");
        if(FilePath.indexOf(Separetar) <0 ) return false;
        StringTokenizer Stoken =new StringTokenizer(FilePath,Separetar);
        DefaultMutableTreeNode Node =MainNode;
        while (Stoken.hasMoreTokens()){
           String FolderName=Stoken.nextToken();
           boolean found =false;
           if(Node.getChildCount() == 1 && ((DFile)((DefaultMutableTreeNode)Node.getChildAt(0)).getUserObject()).istemp() ) getDirectoryTree(Node,false);
           for(int i=0;i<Node.getChildCount();i++){
              if(Node.getChildAt(i).toString().equals(FolderName)){
                  Node=(DefaultMutableTreeNode)Node.getChildAt(i);
                  found =true;
                  break;
              }
           }
           if(!found) {
           //   if(Log.LOG_ERROR) Log.debug("Error:Missing Folder <"+FolderName +"> in the Folder :"+ ((DFile)Node.getUserObject()).getfile().getPath());
              tree.setSelectionPath(new TreePath(Node.getPath()));
              tree.scrollPathToVisible(new TreePath(Node.getPath()));
              //tree.expandPath(new TreePath(Node.getPath()));
              return false;
           }
        }
        tree.setSelectionPath(new TreePath(Node.getPath()));
        tree.scrollPathToVisible(new TreePath(Node.getPath()));
        //if(Log.LOG_VERBOSE_DEBUG) Log.debug("Selection Folder Done:");
        return true;


  }
  public File getSelectionFile(){
     if(tree.isSelectionEmpty()) return null;
     if(tree.getSelectionRows() !=null && tree.getSelectionRows().length > 0){
         DefaultMutableTreeNode node= (DefaultMutableTreeNode)tree.getSelectionPath().getLastPathComponent();
         return ((DFile)node.getUserObject()).getfile();
     }
     return null;
  }
 /* public String getSelectionFilePath(){
        if(getSelectionFile() == null) return " ";
        else return getSelectionFile().getPath();
  }*/
  public String getSelectionFilePath(){
	  if(getSelectionFile() == null) return " ";
	  else {
		  // Code Added for AIP Service - Start
		  VWLogger.info("****** getSelectionFilePath() ******");
		  File f = getSelectionFile();
		  if(f.getParent() == null) return " ";
		  String sDrive = f.getParent().substring(0,2);
		  String sPath = f.getPath();
		  String sUNCPath = getUNCPath(f);
		  VWLogger.info("sUNCPath path"+sUNCPath);
		  if(!(("").equals(sUNCPath)))
		  {
			  VWLogger.info("Path before : "+sPath);
			  sPath = sUNCPath+"\\"+sPath.substring(3);
			  VWLogger.info("Path after : "+sPath);
			  return sPath;
		  }
		  else 
			  return sPath;
		  // Code Added for AIP Service - End
	  }
  }
  //------------------------Methodes Renderer ------------------------------
  class TreeExpand implements TreeExpansionListener{
     public void treeExpanded(TreeExpansionEvent e) {

     }
     public void treeCollapsed(TreeExpansionEvent e) {
     }
  }
  class TreeWillExpand implements TreeWillExpandListener{
     public void treeWillExpand(TreeExpansionEvent e) {
        DefaultMutableTreeNode parent = (DefaultMutableTreeNode)e.getPath().getLastPathComponent();
         if(parent.getChildCount() == 1 && ((DFile)((DefaultMutableTreeNode)parent.getChildAt(0)).getUserObject()).istemp() ){
          ((DefaultMutableTreeNode)parent.getChildAt(0)).removeFromParent();
           getDirectoryTree(parent,false);
          tree.expandPath(new TreePath(parent.getPath()));
         }
          /*for(int i=0;i< parent.getChildCount();i++){
              if(parent.getChildAt(i).isLeaf()){
                getDirectoryTree((DefaultMutableTreeNode)parent.getChildAt(i),false);
            }*/

     }
     public void treeWillCollapse(TreeExpansionEvent e) {

     }
  }
  class IconRenderer extends DefaultTreeCellRenderer {

        public IconRenderer() {

        }
        public Component getTreeCellRendererComponent(
                            JTree tree,Object value,boolean sel,boolean expanded,boolean leaf,
                            int row, boolean hasFocus) {

            super.getTreeCellRendererComponent(
                        tree, value, sel,expanded, leaf, row,hasFocus);
            int level =((DefaultMutableTreeNode)value).getLevel();
           // if(level >1){
            File f =((DFile)((DefaultMutableTreeNode)value).getUserObject()).getfile();
            if(f !=null)
                setIcon(fc.getIcon(f));
            else setIcon(null);
            return this;
       }

  }
  class DFile {
    private File file;
    private boolean Root;
    private boolean temp;
   // private String Name;
    public DFile(File file1,boolean Root1,boolean temp1){
      file =file1;
      Root =Root1;
      temp =temp1;
    }
    public File getfile(){
      return file;
    }
    public boolean istemp(){
        return temp;
    }
    public String toString(){
      if(temp) return "temp";         
       int lastseparetor = file.toString().lastIndexOf(file.separatorChar);              
       if(lastseparetor > 0) 
           if(lastseparetor == file.toString().length() -1)
             return file.toString().substring(0,lastseparetor);   
           else   return file.getName();  
       else  return file.toString();              
    }
  }
  public static String getUNCPath(File file){
	  // Code Added for AIP Service - Start
	  try {
		  //VWLogger.info("Inside the getUNCPath method");
		  String sDrive = file.getParent().substring(0,2);
		  Runtime runTime = Runtime.getRuntime() ; 
		  Process process = runTime.exec( "net use "+sDrive ) ; 
		  InputStream inStream = process.getInputStream() ; 
		  InputStreamReader inputStreamReader = new InputStreamReader( inStream ) ; 
		  BufferedReader bufferedReader = new BufferedReader( inputStreamReader ) ; 
		  String line = null ; 
		  String [] components = null ; 
		  while( null != (line = bufferedReader.readLine() ) ) 
		  { 
			  line = line.trim();
			  //VWLogger.info("net use output :"+line);
			  if(line.indexOf("Remote name") != -1)
				  return line.substring(line.indexOf("Remote name")+12,line.length()).trim();
		  } 
	  }catch(Exception ex){
		  VWLogger.error("Exception while getting UNC path for :"+file.getName(),ex);
	  }
	  return "";
	  // Code Added for AIP Service - End
  }
}