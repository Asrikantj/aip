/*
 * pack_rEntry.java
 *
 * Created on August 21, 2005, 6:02 PM
 */

package com.computhink.aip.util;

/**
 *
 * @author  Administrator
 */
public class pack_rEntry {
    public static final int DATA_LEN = 2608; //2598;   
    public int pDataProcess;
    public String tModify =new String(new byte[12]); //replace 8byte with 10
    public String acDescApplication =new String(new byte[512]);
    public String acPathSave =new String(new byte[512]);
    public String acPathFile =new String(new byte[512]); // must unicode;
    public String acPathOutput =new String(new byte[512]);
    public String acPathExecutable =new String(new byte[512]);
    public int nIndexPage;  // start from 0;
    public int nIndexFormat;
    public int nIndexVersion;
    public int nSizeFrame;
    public int nWidthThumb;
    public int nHeightThumb;
    public short bModify;
   // public String csim
    
    /** Creates a new instance of pack_rEntry */
    public pack_rEntry() {
    }
    public pack_rEntry(int pageIndex,String pageName) {
        nIndexPage =pageIndex;
        if(pageName !=null)
        {    
            try
            {
                byte[] b_unidata = vUtil.getUnicodeData(pageName);
                int len = b_unidata.length;
                if(len >512) len =512;
                byte[] b_uniPageName =new byte[512];
                System.arraycopy(b_unidata,0,b_uniPageName,0,len);
                acPathFile = new String(b_uniPageName);
               
            }
            catch (Exception e)
            {
                e.printStackTrace();
            } 
        }    
        
    }
    public byte[] getObjectData()
    {        
        byte[] data =new byte[DATA_LEN];
        
        int count =0;        
        vUtil.putInt(data,0,pDataProcess);
        count +=4;
        byte[] btM= tModify.getBytes();
        System.arraycopy(btM,0,data,count,12);        
        count +=12;
        byte[] bac= acDescApplication.getBytes();
        System.arraycopy(bac,0,data,count,512);
        count +=512;
        byte[] bac1= acPathSave.getBytes();
        System.arraycopy(bac1,0,data,count,512);
        count +=512;
        byte[] bac2= acPathFile.getBytes();
        System.arraycopy(bac2,0,data,count,512);
        count +=512;
        byte[] bac3= acPathOutput.getBytes();
        System.arraycopy(bac3,0,data,count,512);
        count +=512;
        byte[] bac4= acPathExecutable.getBytes();
        System.arraycopy(bac4,0,data,count,512);
        count +=512;
        vUtil.putInt(data,count,nIndexPage);
        count +=4;
        vUtil.putInt(data,count,nIndexFormat);
        count +=4;
        vUtil.putInt(data,count,nIndexVersion);
        count +=4;
        vUtil.putInt(data,count,nSizeFrame);
        count +=4;
        vUtil.putInt(data,count,nWidthThumb);
        count +=4;
        vUtil.putInt(data,count,nHeightThumb);
        count +=4;
        vUtil.putShort(data,count,bModify);
        // there count 2598,add 10 byte add to simulat C structure
        return data;
    }    
}
