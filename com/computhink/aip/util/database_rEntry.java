/*
 * database_rEntry.java
 *
 * Created on August 21, 2005, 12:49 PM
 */

package com.computhink.aip.util;
import java.io.*;
/**
 *
 * @author  Administrator
 */
public class database_rEntry implements java.io.Serializable{
    
    public static final int DATA_LEN = 24;
    public int nIndex0;
    public int nIndex1=-5;
    public int nIndex2=1;
    public int nOffset;
    public int nSizeLoad;
    public int pyDataLoad =0;
    
    public database_rEntry(int indx,int offset,int sizeLoad ) {
        nIndex0 =indx;
        nOffset =offset;
        nSizeLoad =sizeLoad;
    }
   
    
    public boolean writeObjectData(OutputStream os,int offset)
    {
       try
       {       
            
         
            DataOutputStream oos = new DataOutputStream(os);
            oos.writeInt(nIndex0);
            oos.writeInt(nIndex1);
            oos.writeInt(nIndex2);
            oos.writeInt(nOffset);
            oos.writeInt(nSizeLoad);
            oos.writeInt(pyDataLoad);
            oos.close();
       }
       catch (Exception e)
       {
            e.printStackTrace();
       }  
       return true;
        
    }
    public byte[] getObjectData()
    {   
        byte[] data =new byte[DATA_LEN];
        vUtil.putInt(data,0,nIndex0);
        vUtil.putInt(data,4,nIndex1);
        vUtil.putInt(data,8,nIndex2);
        vUtil.putInt(data,12,nOffset);
        vUtil.putInt(data,16,nSizeLoad);
        vUtil.putInt(data,20,pyDataLoad);
        return data;
    }    
    public static void main(String[] args)
    {
          database_rEntry rE =new database_rEntry(1,10,15);
          
       /*   try{
             FileOutputStream fos = new FileOutputStream("D:\\temp\\testrEntry2.txt");  
             OutputStreamWriter osw =new OutputStreamWriter(fos);
             osw.write(rE.getObjectData());
             osw.close();
             
          }
          catch(Exception e)
          {
            e.printStackTrace();
          }*/ 
          try{
             FileOutputStream fos = new FileOutputStream("D:\\temp\\testrEntry3.txt");  
             rE.writeObjectData(fos,0);
          }
          catch(Exception e)
          {
            e.printStackTrace();
          }   
    }   
    
}
