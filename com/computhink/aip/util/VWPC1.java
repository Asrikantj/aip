/*
 * VWPC1.java
 *
 * Created on August 28, 2004, 12:44 PM
 */
package com.computhink.aip.util;
/**
 *
 * @author  Administrator
 */
import java.io.*;

public class VWPC1 
{
	public VWPC1(){
		
	}
     char ax, bx, cx, dx, si, res, inter, i;
     char key[] = new char [16];               
     char x1a0[] = new char [8];
     final String VWKey = "D36083DE-284C-4d62-8B4A-0F1537A654B0";
   
    private  void reset()
    {
        System.arraycopy(VWKey.toCharArray(), 0, key, 0, 16);
        inter = 0; res = 0; i = 0;
    }
    private  void assemble()
    {
        x1a0[0]= (char) (( key[0]*256 ) + key[1]); code(); 
        inter = res;
        x1a0[1]= (char) (x1a0[0] ^ ( (key[2]*256) + key[3])); code();
        inter =  (char) (inter^res);
        x1a0[2]= (char) (x1a0[1] ^ ( (key[4]*256) + key[5])); code();
        inter =  (char) (inter^res);
        x1a0[3]= (char) (x1a0[2] ^ ( (key[6]*256) + key[7] )); code();
        inter =  (char) (inter^res);
        x1a0[4]= (char) (x1a0[3] ^ ( (key[8]*256) + key[9] )); code();
        inter =  (char) (inter^res);
        x1a0[5]= (char) (x1a0[4] ^ ( (key[10]*256) + key[11] )); code();
        inter =  (char) (inter^res);
        x1a0[6]= (char) (x1a0[5] ^ ( (key[12]*256) + key[13] )); code();
        inter =  (char) (inter^res);
        x1a0[7]= (char) (x1a0[6] ^ ( (key[14]*256) + key[15] )); code();
        inter =  (char) (inter^res);
        i=0;
    }
    private  void code()
    {
        char tmp = 0;
        char si = 0;
        char xla2 = 0;
        
        dx = (char) (xla2 + i);
        ax = x1a0[i];
        cx = 0x015a;
        bx = 0x4e35;
        tmp = ax;
        ax = si;
        si = tmp;
        tmp = ax;
        ax = dx;
        dx = tmp;
        if (ax != 0) ax = (char) (ax * bx);
        tmp = ax;
        ax = cx;
        cx = tmp;
        if (ax != 0)  
        {
            ax = (char) (ax * si);
            cx = (char) (ax + cx);
        }
        tmp = ax;
        ax = si;
        si = tmp;
        ax = (char) (ax * bx);
        dx = (char) (cx + dx);
        ax = (char) (ax + 1);
        xla2 = dx;
        x1a0[i] = ax;
        res = (char) (ax ^ dx);
        i = (char) (i + 1);
    }
    public  void encrypt (byte[] bytes) 
    {
        if (bytes.length == 0) return;
        int c = 0;
        reset();
        /* Issue No - EAS Hangs when processing archives with bigger size 
         * 			  and PDF's which are processed by EAS are not opening in DTClient.  
         * Developer - Nebu Alex
         * 
         * Fix Description - DTClient decrypt pages up to maximum of 1048576 characters.
         *                  -But EAS encrypted the whole page contents.
         *                  - In the fix a condtion is put so as to limit encryption to a
         *                  - maximum of 1 MB.
         *  Date - August 23, 2006
         * */
        int nSize =  (bytes.length > 1048576)?1048576:bytes.length; 
        for (int x = 0; x < nSize; x++)
        {
        	// End of fix
            assemble();
            char cfc = (char) (inter >> 8); 
            char cfd = (char) (inter & 255);
            c = bytes[x] & 0xff; 
            if (c != 0) 
               for(int y = 0; y < 16; y++) key[y] = (char) (key[y] ^ c);
            bytes[x] = (byte) (bytes[x] ^ (cfc^cfd));
        }
    }
    public  void decrypt (byte[] bytes) 
    {
        if (bytes.length == 0) return;
        int c = 0;
        reset();
        for (int x = 0; x < bytes.length; x++)
        {
            assemble();
            char cfc = (char) (inter >> 8); 
            char cfd = (char) (inter & 255);
            bytes[x] = (byte) (bytes[x] ^ (cfc^cfd));
            //...unsign the resulting byte
            //...java does not support unsigned byte
            c = bytes[x] & 0xff; 
            if (c != 0) 
               for(int y = 0; y < 16; y++) key[y] = (char) (key[y] ^ c);

        }
      }

      public static void main(String args[])
      {
          //sample code remove later
          String org = "c:\\tmp\\original.tif";
          String enc = "c:\\tmp\\encrypt.tif";
          String dec = "c:\\tmp\\decrypt.tif";
          
     /*     try
          {
              long size = new File(org).length();
              byte[] bytes = new byte[(int) size];
              FileInputStream in = new FileInputStream(org);
              in.read(bytes);

              VWPC1.encrypt(bytes);

              FileOutputStream out = new FileOutputStream(enc);
              out.write(bytes);
              out.close();
              in.close();
          }
          catch(Exception e){}
          
          try
          {
              long size = new File(enc).length();
              byte[] bytes = new byte[(int) size];
              FileInputStream in = new FileInputStream(enc);
              in.read(bytes);

              VWPC1.decrypt(bytes);

              FileOutputStream out = new FileOutputStream(dec);
              out.write(bytes);
              out.close();
              in.close();
          }
          catch(Exception e){}*/
      }
}



