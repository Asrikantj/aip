/*
 * ObservableComboBox.java
 *
 * Created on May 31, 2004, 4:57 PM
 */

package com.computhink.aip.util;

import java.util.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * A JComboBox subclass that notifies observers of changes.
 * @author  amaleh
 */
public class ObservableComboBox extends javax.swing.JComboBox 
{
    Observable observable;
    
    /** Creates a new instance of ObservableComboBox */
    public ObservableComboBox(Observable observable) 
    {
        super();
        this.observable = observable;
        init();
    }

    /** Creates a new instance of ObservableComboBox */
    public ObservableComboBox(Observable observable, ComboBoxModel aModel) 
    {
        super(aModel);
        this.observable = observable;
        init();
    }

    /** Creates a new instance of ObservableComboBox */
    public ObservableComboBox(Observable observable, Object[] items) 
    {
        super(items);
        this.observable = observable;
        init();
    }

    /** Creates a new instance of ObservableComboBox */
    public ObservableComboBox(Observable observable, Vector items) 
    {
        super(items);
        this.observable = observable;
        init();
    }
    
    /** initiates by adding a listener to notify observers of changes */
    private void init()
    {
        addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                if (observable != null)
                    observable.notifyObservers();
            }
        });
    }
    
    /** Sets current observable */
    public void setObservable(Observable observable)
    {
        this.observable = observable;
    }
}
