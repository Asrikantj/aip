/*
 * folderStatus.java
 *
 * Created on July 24, 2004, 12:09 PM
 */

package com.computhink.aip.util;

/**
 *
 * @author  Ayman
 */
public class LocationStatus {
        
    private String name;    
        
    private int status =STOP;
        
    private String statusText=AIPUtil.getString("General.Lab.ProcessStopped");
    
    private int progressVal =0;
    
    private String progressStr="" ;
    
    private Thread workThread;
    
    private boolean enableProcess =false;
    
    public final static int STOP =0,WAIT =1,PROCESS =2;         
   
    private int iPolling =0;
    
    public LocationStatus(String name) {
        this.name =name;
        
    }    
   
    public String getName() {
        return this.name;
    }    
    public void setName(String name) {
        this.name =name;
    }    
    public int getStatus() {
        return this.status;
    }    
    public void setStatus(int status) {
        this.status = status;
    }    
    public String getStatusText() {
        return this.statusText;
    }    
    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }    
    public int getProgressVal() {
        return this.progressVal;
    }    
    public void setProgressVal(int progressVal) {
        this.progressVal = progressVal;
    }    
    public String getProgressStr() {
        return this.progressStr;
    }    
    public void setProgressStr(String progressStr) {
        this.progressStr = progressStr;
    }
    public void setWorkThread(Thread thread)
    {
         this.workThread =thread;   
    }
    public Thread getWorkThread()
    {
        return this.workThread;   
    }
    public boolean isEnabledProcess(){
        return this.enableProcess;
    }    
    public void setEnabledProcess(boolean enabled)
    {
        this.enableProcess =enabled;
    }    
    public String toString()
    {
        return this.name;
    }       
    public void setPolling(int poll)
    {
        iPolling =poll;
    }    
    public int getPolling()
    {
        return iPolling;
    }
    
}
