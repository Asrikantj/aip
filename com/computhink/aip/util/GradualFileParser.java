/*
 * GradualParser.java
 *
 * Created on November 25, 2003, 4:05 PM
 */

package com.computhink.aip.util;

/**
 * Extends standard file parsers by having a function that extract a text file
 * information gradually upon every call.
 * It can only move between segments sequentially. It cannot do random access.
 * @author  amaleh
 */
public interface GradualFileParser extends FileParser 
{
    /**
     * Parses a block of the text file and returns a TreeMap object containing
     * the extracted data.
     * An additional "segment#" key is included within the TreeMap object, which
     * refers to the order number of the segement being parsed, and begins at
     * zero.
     * A "status" key is always included within the TreeMap object. It holds the
     * value "valid" if parsing went on successfully, "invalid_file" if the 
     * format of the text file did not match the one expected, "invalid_segment" 
     * if the format of the segment did not match the one expected, and "eof" if 
     * end of file reached.
     * Returns null if a file was not supplied or a file IO error occurred.
     */
    public abstract java.util.TreeMap parseNext();
}
