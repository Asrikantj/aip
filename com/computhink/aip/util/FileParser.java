/*
 * Parser.java
 *
 * Created on November 25, 2003, 4:00 PM
 */

package com.computhink.aip.util;

/**
 * A foundation for text file parsers.
 * @author  amaleh
 */
public interface FileParser extends FileOperator
{
    /**
     * Parses a text file and returns a TreeMap object containing all the
     * extracted data.
     * A "status" key is always included within the TreeMap object. It holds the
     * value "valid" if parsing went on successfully and "invalid" if the format
     * of the text file did not match the one expected.
     * Returns null if a file was not supplied or a file IO error occurred.
     */
    public java.util.TreeMap parse();
}
