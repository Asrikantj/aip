package com.computhink.aip.util;

import javax.swing.*;
import javax.swing.text.*;


import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;
/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class NumericField extends JTextField {

  private NumberFormat NumericFormatter;
  private int NumLength =0 ;

  public NumericField(int value, int columns) {
        //super(columns);
        NumLength = columns;
        NumericFormatter = NumberFormat.getNumberInstance(Locale.US);
        NumericFormatter.setParseIntegerOnly(true);
        setValue(value);
  }
  public int getValue() {
        int retVal = 0;
        try {
            retVal = NumericFormatter.parse(getText()).intValue();
        } catch (ParseException e) {
            // This should never happen because insertString allows
            // only properly formatted data to get in the field.

        }
        return retVal;
  }

  public void setValue(int value) {
        setText(NumericFormatter.format(value));
  }
  protected Document createDefaultModel() {
        return new TestNumberDocument();
  }
  protected class TestNumberDocument extends PlainDocument {
      public void insertString(int offs, String str, AttributeSet a)
            throws BadLocationException {
            char[] source = str.toCharArray();
            char[] result = new char[source.length];
            int j = 0;

            for (int i = 0; i < result.length; i++) {
                if ((Character.isDigit(source[i])|| source[i] == '.'))
                    result[j++] = source[i];
                else {

                    //System.err.println("insertString: " + source[i]);
                }
            }
            super.insertString(offs, new String(result, 0, j), a);
      }
  }
}