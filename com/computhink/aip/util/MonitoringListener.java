/*
 * MonitoringInterface.java
 *
 * Created on July 5, 2004, 11:32 AM
 */

package com.computhink.aip.util;
import java.util.Vector;
/**
 *
 * @author  Administrator
 */
public interface MonitoringListener {
   public boolean processLocation(String loc,boolean init);  
}
