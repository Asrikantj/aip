/*
 * XMLSerializer.java
 *
 * 
 * 
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.computhink.aip.util;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * A high-level interface for all XML serializers
 */
public interface XMLSerializer {

    /**
     * Loads a serialized object from an XML file
     * @param myClass object class type
     * @param file XML file path
     * @return loaded object
     */
    public Object loadFromXML(Class myClass, File file) throws FileNotFoundException, XMLSerializationException;

    /**
     * Saves (serializes) an object to an XML file
     * @param object object to serialize
     * @param file XML file path
     */
    public void saveToXML(Object object, File file) throws FileNotFoundException, XMLSerializationException;
}
