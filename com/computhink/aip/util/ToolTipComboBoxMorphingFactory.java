/*
 * ToolTipComboBoxMorphingFactory.java
 *
 * Created on May 31, 2004, 4:44 PM
 */

package com.computhink.aip.util;

import java.awt.*;
import javax.swing.*;
import javax.swing.plaf.basic.BasicComboBoxRenderer;

/**
 * A factory of JComboBox objects that display tooltip when hovered over with 
 * the mouse cursor
 * @author  amaleh
 */
public class ToolTipComboBoxMorphingFactory implements JComboBoxMorphingFactory 
{
    private static ToolTipComboBoxMorphingFactory factory;
    
    /** Creates a new instance of ToolTipComboBoxMorphingFactory */
    private ToolTipComboBoxMorphingFactory() 
    {
    }
    
    public static ToolTipComboBoxMorphingFactory getInstance()
    {
        if (factory == null)
            factory = new ToolTipComboBoxMorphingFactory();
        return factory;
    }
    
    public javax.swing.JComboBox morph(javax.swing.JComboBox box) 
    {
        final JComboBox newBox = box;
        newBox.addItemListener(new java.awt.event.ItemListener(){
            public void itemStateChanged(java.awt.event.ItemEvent e)
            {
                newBox.setToolTipText((String)newBox.getSelectedItem());
                newBox.setRenderer(new BasicComboBoxRenderer()
                {
                    public Component getListCellRendererComponent(JList list,
                                              Object value,
                                              int index,
                                              boolean isSelected,
                                              boolean cellHasFocus)
                    {
                        try
                        {
                            setToolTipText((String)value);
                        }
                        catch (ClassCastException cce)
                        {
                        }
                        return super.getListCellRendererComponent(list,
                                              value,
                                              index,
                                              isSelected,
                                              cellHasFocus);
                    }
                });
            }
        });
        return newBox;
    }
    
}
