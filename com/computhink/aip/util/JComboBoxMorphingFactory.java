/*
 * JComboBoxMorphingFactory.java
 *
 * Created on May 31, 2004, 4:36 PM
 */

package com.computhink.aip.util;
import javax.swing.*;

/**
 * Provides a framework for factories that morph an existing JComboBox object
 * to another.
 * @author  amaleh
 */
public interface JComboBoxMorphingFactory 
{
    /** Morphes supplied JComboBox object by adding properties or listeners to it */
    public JComboBox morph(JComboBox box);
        
}
