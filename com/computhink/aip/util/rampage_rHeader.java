/*
 * rampage_rHeader.java
 *
 * Created on August 22, 2005, 9:44 AM
 */

package com.computhink.aip.util;

/**
 *
 * @author  Administrator
 */
public class rampage_rHeader {
    public static final int DATA_LEN = 12;   
    public int nSizePage;
    public int nIndexMagic;
    public short bBypass; 
    public short bLegacy;
    public rampage_rHeader() {
    }
    public rampage_rHeader(int pagesNo,int indexMagic) {
        nSizePage =pagesNo;
        nIndexMagic =indexMagic;
    }
    public byte[] getObjectData()
    {
        byte[] data =new byte[DATA_LEN];
        vUtil.putInt(data,0,nSizePage);
        vUtil.putInt(data,4,nIndexMagic);
        vUtil.putShort(data,8,bBypass);
        vUtil.putShort(data,10,bLegacy);
        return data;
    }    
    
}
