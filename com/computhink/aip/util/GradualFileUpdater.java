/*
 * GradualFileUpdater.java
 *
 * Created on December 8, 2003, 3:52 PM
 */

package com.computhink.aip.util;

/**
 * An interface that adds to FileUpdater the ability to update a file gradually
 * by working on one segment of it at a time.
 * It can only move from one segment to the next sequentially. It cannot do 
 * random access.
 * @author  amaleh
 */
public interface GradualFileUpdater extends FileUpdater 
{
    /**
     * Updates a segment of a text file using a TreeMap of data. The keys in the 
     * TreeMap point the locations in the file that will be updated with the 
     * values in the TreeMap.
     */
    public boolean updateNext(java.util.TreeMap map);
}
