/*
 * ChangeObservable.java
 *
 * Created on May 31, 2004, 12:26 PM
 */

package com.computhink.aip.util;

/**
 * An observable subclass that notifies observers of changes
 * @author  amaleh
 */
public class ChangeObservable extends java.util.Observable {
    
    /** Creates a new instance of ChangeObservable */
    public ChangeObservable() 
    {
    }
    
    /** Notifies observers of changes */
    public void notifyObservers()
    {
        setChanged();
        super.notifyObservers();
        clearChanged();
    }
    
    /** Notifies observers of changes while passing arguments */
    public void notifyObservers(Object arg)
    {
        setChanged();
        super.notifyObservers(arg);
        clearChanged();
    }
}
