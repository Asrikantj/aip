/*
 * VWZipUtilities.java
 *
 * Created on July 30, 2002, 4:17 PM
 */

package com.computhink.aip.util;

import java.io.File;
import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.zip.ZipFile;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import java.util.zip.ZipInputStream;
import java.util.zip.CheckedOutputStream;
import java.util.zip.Adler32;
import java.util.zip.ZipException;
import java.util.Vector;
import java.util.Enumeration;

/**
 *
 * @author  Annas Maleh
 * @version 
 */

/**
 * This class provides convenience methods for compression and extraction of zip
 * files.
 */
public class VWZipUtilities 
{
	static final int BUFFER = 2048;
    /* 
     * This method is there for testing purposes only
     */
    /*
    public static void main(String[] args) throws Exception
    {
        File zipFile = new File("D:/VWIV.zip");
        File directory = new File("D:/z1/z2/z3");
        String fileName = "VWIV/images/Follow.jpg";
        String[] fileNames = new String[2];
        fileNames[0] = fileName;
        fileNames[1] = "VWIV/images/Azul.jpg";
        
        File zipArchive = new File("D:/zipArchive.zip");
        File baseDir = new File("D:/");
        File[] files = new File[3];
        files[0] = new File("D:/Images/untitled.jpg");
        files[1] = new File("D:/Follow.jpg");
        files[2] = new File("D:/Purple Flower.jpg");
        File folder = new File("D:/ScrewedImages");
        //compress(zipArchive, baseDir, files);
        //compress(zipArchive, null, files);
        //compress(zipArchive, baseDir, folder);
        compress(zipArchive, null, folder);
        //extract(zipFile, directory);
    }
     */
    
    /**************************************************************************/
    /** 
     * Extracts a file from a zip file into a path.
     */
    public static void extract(File zipFile, File directory, String fileName) 
                            throws IOException
    {
        ZipFile zF = new ZipFile(zipFile, ZipFile.OPEN_READ);
        ZipEntry zE = zF.getEntry(fileName);
        InputStream inputStream = zF.getInputStream(zE);
        BufferedInputStream bufferedInputStream = 
            new BufferedInputStream(inputStream);
        directory.mkdirs(); // create the directory
        String extractedFilePath = directory.getPath() + "/" + fileName;
        ///System.out.println(extractedFilePath);
        File extractedFile = new File(extractedFilePath);
        if (extractedFilePath.endsWith("/")) // if path indicates a directory
            extractedFile.mkdirs();
        else
        {
            ///System.out.println("parent: " + extractedFile.getParentFile());
            extractedFile.getParentFile().mkdirs();
            extractedFile.createNewFile();
            try
            {
                FileOutputStream fos = new FileOutputStream(extractedFile);
                 BufferedOutputStream dest = new BufferedOutputStream(fos, BUFFER);
                 int count;
                 byte data[] = new byte[BUFFER];                
                while((count = bufferedInputStream.read(data, 0, BUFFER)) != -1)
                    fos.write(data, 0, count);
                bufferedInputStream.close();
                fos.close();
            }
            catch(FileNotFoundException fnfe)
            {
                System.err.println("ZipUtilities Internal Error: ");
                System.err.println(fnfe);
            }
        }
        zF.close();
    }
    
    /**************************************************************************/
    /** 
     * Extracts files from a zip file into a path.
     */
    public static void extract(File zipFile, File directory, String[] fileNames) 
                            throws IOException
    {
        //ZipFile zF = new ZipFile(zipFile, ZipFile.OPEN_READ);
        for(int i = 0; i < fileNames.length; i++)
            extract(zipFile, directory, fileNames[i]);
    }

    /**************************************************************************/
    /** 
     * Extracts a zip file into a path.
     */
    public static void extract(File zipFile, File directory) throws IOException
    {
        ZipFile zF = new ZipFile(zipFile);
        Enumeration zEntries = zF.entries();
        while(zEntries.hasMoreElements())
            extract(zipFile, 
                    directory, 
                    ((ZipEntry)zEntries.nextElement()).getName());
        zF.close();
    }
    
    /**************************************************************************/
    /** 
     * Adds files that share a base directory into a zip file, which would be 
     * created if it did not exist.
     */
    public  void compress(File zipArchive, File baseDir, File[] files) 
                            throws IOException
    {
        FileOutputStream fileOutputStream = 
            new FileOutputStream(zipArchive);
        CheckedOutputStream checkedOutputStream = 
            new CheckedOutputStream(fileOutputStream, new Adler32());
        BufferedOutputStream bufferedOutputStream = 
            new BufferedOutputStream(checkedOutputStream);
        ZipOutputStream zipOutputStream = 
            new ZipOutputStream(bufferedOutputStream);

        for (int i = 0; i < files.length; i++)
        {
            try
            {
                File file = files[i];
                if (file.isDirectory())
                    continue;
                FileInputStream fileInputStream = 
                    new FileInputStream(file);
                BufferedInputStream bufferedInputStream = 
                    new BufferedInputStream(fileInputStream);

                String zipEntryName;
                if (baseDir != null)
                {
                    String filePath = file.getPath();
                    String basePath = baseDir.getPath();
                    if (filePath.startsWith(basePath))
                        zipEntryName = filePath.substring(basePath.length());
                    else
                        throw new IllegalArgumentException(
                            "baseDir does not contain files.");
                }
                else
                    zipEntryName = file.getName();
                if (zipEntryName.startsWith("/") || 
                    zipEntryName.startsWith("\\"))
                    zipEntryName = zipEntryName.substring(1);
                ZipEntry zipEntry = new ZipEntry(zipEntryName);
                zipOutputStream.putNextEntry(zipEntry);
				zipOutputStream.setLevel(java.util.zip.Deflater.BEST_SPEED);
                int count;
                byte data[] = new byte[BUFFER];
                while((count = bufferedInputStream.read(data, 0, BUFFER)) != -1)
                    zipOutputStream.write(data, 0, count);                

                
          /*      while ((dummy = bufferedInputStream.read()) != -1)
                    zipOutputStream.write(dummy);*/
                bufferedInputStream.close();
            }
            catch(ZipException ze)
            {
                System.err.println(files[i] + " *** DUPLICATE, IGNORED! ***");
                continue;
            }
        }
        zipOutputStream.close();
    }

    /**************************************************************************/
    /** 
     * Adds files that do not necessarily share the same base directory into a 
     * zip file, which would be created if it did not exist.
     */
    public  void compress(File zipArchive, File[] files) 
                            throws IOException
    {
        
        compress(zipArchive, null, files);
    }

    /**************************************************************************/
    /** 
     * Adds all files in a directory into a zip file, which would be created if 
     * it did not exist.
     */
    public  void compress(File zipArchive, 
                                File baseDirectory, 
                                File directory) 
                            throws IOException
    {
        compress(zipArchive, baseDirectory, listFilesRecursively(directory));
    }
    
    /**************************************************************************/
    /** 
     * Lists all files in a directory and its subdirectories.
     */
    public  File[] listFilesRecursively(File directory)
                                        throws IllegalArgumentException
    {
        if (directory == null)
            return null;
        if (!directory.isDirectory())
            throw new IllegalArgumentException("File must be a directory.");
        Vector fileVector = new Vector();
        File[] files;
        files = directory.listFiles();
        for (int i = 0; i < files.length; i++)
        {
            if (files[i].isDirectory())
                addElementsToVector(listFilesRecursively(files[i]), fileVector);
            else
                fileVector.add(files[i]);
        }
        File[] listFiles = new File[fileVector.size()];
        for (int i = 0; i < fileVector.size(); i++)
            listFiles[i] = (File)fileVector.get(i);
        return listFiles;
    }
    
    /**************************************************************************/
    /**
     * Adds elements to a Vector. 
     */
    public  void addElementsToVector(Object[] elements, Vector vector)
    {
        if (vector == null)
            return;
        for (int i = 0; i < elements.length; i++)
            vector.add(elements[i]);
    }
}