/*
 * INIManager.java
 *
 * Created on February 27, 2004, 4:21 PM
 */

package com.computhink.aip.util;
import java.io.*;
import java.nio.*;
import java.nio.channels.*;
import java.nio.charset.*;

/**
 * A class that manages data in files that follow the INI format.
 * @author  amaleh (amaleh@computhink.com)
 */
public class INIManager 
{
    /**
     * Reference to INI file
     */
    private File INIFile;
    
    /**
     * Reference to INI Random Access File
     */
    private RandomAccessFile INIRAF;
    
    /**
     * Reference to INI mapped buffer
     */
    private MappedByteBuffer INIMappedBuffer;
    
    /**
     * Reference to INI file mapped decoded char buffer
     */
    private CharBuffer INIFileBuffer;
    
    /** Creates a new instance of INIManager */
    public INIManager(File INIFile, String mode) throws Exception
    {
        //set class INI file
        this.INIFile = INIFile;
        
        /* map file's contents into a char buffer */
        INIRAF = new RandomAccessFile(INIFile, mode);
        FileChannel INIFC = INIRAF.getChannel();
        int INIFCLength = (int)INIFC.size();
        if (mode.equals("r"))
            INIMappedBuffer = INIFC.map(FileChannel.MapMode.READ_ONLY, 0, INIFCLength);
        else if (mode.equals("rw") || mode.equals("rws") || mode.equals("rwd"))
            INIMappedBuffer = INIFC.map(FileChannel.MapMode.READ_WRITE, 0, INIFCLength);

        // ISO-8859-1  is ISO Latin Alphabet #1
        Charset charset = Charset.forName("ISO-8859-1");
        CharsetDecoder decoder = charset.newDecoder();
        INIFileBuffer = decoder.decode(INIMappedBuffer);
    }
    
    /** Creates a new instance of INIManager */
    public INIManager(String INIFileName, String mode) throws Exception
    {
        this(new File(INIFileName), mode);
    }
    
    /** closes resources */
    public void close() throws Exception
    {
        INIRAF.close();
    }
    
    /** Extracts key value from section */
    public String getKeyValue(String section, String key)
    {
        //construct section string
        String sectionStr = "[" + section + "]";
        int sectionStrLen = sectionStr.length();
        //limitation to key=value, no key = value
        String keyStr = key + "=";
        int keyStrLen = keyStr.length();
        StringBuffer compStr = new StringBuffer();
        boolean sectionFound = false;
        boolean keyFound = false;
        int i = 0;
        for(; i < INIFileBuffer.length(); i++)
        {
            if (INIFileBuffer.get(i) == '\r' || INIFileBuffer.get(i) == '\n')
            {
                //if key found then break because value is recorded and end of line reached.
                if (keyFound)
                    break;
                compStr.setLength(0);
            }
            else
            //if key found, record value
            if (keyFound)
            {
                compStr.append(INIFileBuffer.get(i));
            }
            //if section found, look for key
            else if (sectionFound)
            {
                //if section is already found and a new section is encountered, return null because key not found.
                if (INIFileBuffer.get(i) == '[')
                    return null;
                if (compStr.length() < keyStrLen)
                    compStr.append(INIFileBuffer.get(i));
                if (compStr.toString().equals(keyStr))
                {
                    keyFound = true;
                    compStr.setLength(0);
                }
            }
            //look for section
            else
            {
                if (compStr.length() < sectionStrLen)
                    compStr.append(INIFileBuffer.get(i));
                if (compStr.toString().equals(sectionStr))
                {
                    sectionFound = true;
                }
            }
        }
        //if end of file reached, return null because key or section not found
        if (i == INIFileBuffer.length())
            return null;
        return compStr.toString();
    }
    
    /** Stores key value in section */
    public void putKeyValue(String section, String key, String value)
    {
    }
    
    public static void main(String[] args) throws Exception
    {
        INIManager manager = new INIManager("F:\\wptouni.ini", "r");
        System.out.println(manager.getKeyValue("WPCS1", "17"));
        System.out.println(manager.getKeyValue("WPCS1", "18"));
    }
}
