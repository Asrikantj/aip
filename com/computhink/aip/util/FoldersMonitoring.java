/*
 * FoldersMonitoring.java
 *
 * Created on July 3, 2004, 5:50 PM
 */

package com.computhink.aip.util;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Vector;
import java.util.Hashtable;
import java.util.Enumeration;

import com.computhink.aip.AIPManager;
import com.computhink.aip.ui.AIPConnector;
import com.computhink.aip.util.AIPUtil;
import com.computhink.common.Constants;
import com.computhink.manager.ManagerUtil;
/**
 *
 * @author  Administrator
 */
public class FoldersMonitoring {
   
    private static boolean catchChenge =false;   
    private static Hashtable foldersStatus =new Hashtable();
    private static MonitoringListener listener =null;
    public FoldersMonitoring() {
    }
    
    static
    {
        String home =AIPUtil.findHome();
        if(home ==null || home.equals(""))
            home = System.getProperty("user.dir");
        else 
            home =home +AIPUtil.pathSep+"system";        
        String nMonitorPath =home+AIPUtil.pathSep+AIPUtil.getString("Native.EXE.Name");         
        //String nMonitorPath =home+AIPUtil.pathSep+"VWAIPDLL.dll";
        	//System.load(nMonitorPath);        	
        if(AIPManager.isAutoStart() && !AIPManager.isCommandPrompt())
        {
        	//VWLogger.info("Loading...AIPService.exe");
        	nMonitorPath =home+AIPUtil.pathSep+"AIPService.exe";
        	
        }
        try{        	 
        	System.load(nMonitorPath);

        }catch(Error er){
        	VWLogger.info("Error while loading the exe " + er.getCause().getMessage() + " : " + nMonitorPath);
        }catch(Exception ex){
        	VWLogger.info("Exception while loading the exe " + ex.getCause().getMessage() + " : " + nMonitorPath);
        }
    }
   
    public static boolean startMonitorFolders(Vector folders)
    {   
        if(folders ==null ) return false;
        
        foldersStatus.clear();
        for(int i=0;i<folders.size();i++)
        {
            foldersStatus.put(folders.get(i),new Boolean(false));
        }
        return startMonitoring(folders);
        
        
    }    
    public static void folderUpdate(String folderPath)
    {
        //System.out.println("folderUpdate:"+folderPath);
        catchChenge =true;
        if(foldersStatus.containsKey(folderPath))
        {
            foldersStatus.put(folderPath,new Boolean(true));
        }
        if(listener !=null)
            listener.processLocation(folderPath,false);
    }    
    public static void resetFoldersStatus()
    {
        
        for(Enumeration e =foldersStatus.keys();e.hasMoreElements();)
        {
            String key =(String)e.nextElement();
            foldersStatus.put(key,new Boolean(false));            
        }    
    }
    public static void resetFolderStatus(String folder)
    {
        if( foldersStatus.containsKey(folder))        
            foldersStatus.put(folder,new Boolean(false));            
        
            
    }
    public static void stopAIPService()
    {
        if(AIPManager.isAIPServiceStarted())
        {
	        String home =AIPUtil.findHome();
	        if(home ==null || home.equals(""))
	            home = System.getProperty("user.dir");
	        else 
	            home =home +AIPUtil.pathSep+"system";
	        String nMonitorPath = "";
	       	nMonitorPath = home+AIPUtil.pathSep+"VWjUtil.dll";
	        System.load(nMonitorPath);	        
        }
        String sHost = "";
        try{
        	sHost = InetAddress.getLocalHost().getHostAddress();
        }catch(UnknownHostException unhostex){
        	VWLogger.error("Could not find Host Address :",unhostex);
        }
        int serviceFlag = ManagerUtil.switchWindowsService(0,sHost,Constants.PRODUCT_NAME +" AIP Service");
    }
    public static boolean isFolderUpdated(String loc)
    {
        if(foldersStatus.containsKey(loc))
        {
            return  ((Boolean)foldersStatus.get(loc)).booleanValue();
        }
        return false;
    }    
    public static Vector getUpdatedLocations()
    {
        Vector locs =new Vector();
        for(Enumeration e =foldersStatus.keys();e.hasMoreElements();)
        {
            String key =(String)e.nextElement();
            Boolean val = (Boolean)foldersStatus.get(key);
            if(val.booleanValue())
                locs.add(key);            
        }   
        return locs;
    }    
    
    public static void addMonitoringListener(MonitoringListener mlistener)
    {
        listener =mlistener;
    }    
    public static void main(String[] args) {
    	String sHost = "";
        try{
        	sHost = InetAddress.getLocalHost().getHostAddress();
        }catch(UnknownHostException unhostex){
        	VWLogger.error("Could not find Host Address :",unhostex);
        	System.out.println("Error in loading... " + unhostex.getMessage());
        }
        //System.load("C:\\ViewWise\\system\\VWjUtil.dll");
        System.load("c:\\Program Files\\ViewWise 5.6 Client\\System\\VWjUtil.dll");
    	int serviceFlag = ManagerUtil.switchWindowsService(0,sHost,Constants.PRODUCT_NAME +" AIP Service");
	}
    public static native boolean startMonitoring(Vector folders);
    public static native boolean stopMonitoring();
}
