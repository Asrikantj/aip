package com.computhink.aip.util;

import java.awt.event.KeyEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusAdapter;
import java.awt.datatransfer.*;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;
import javax.swing.event.DocumentListener;
import javax.swing.event.DocumentEvent;

/**
A text field that controls and formats user input.
A "mask" to define the format and constraints.
<p>
This is an JFC-compatible component.
See
<a href="com.symantec.itools.awt.MaskedTextField"><code>MaskedTextField</code></a>
 for the AWT-compatible version of this component.
 @see #setMask
*/
public class JMaskedNumberField extends javax.swing.JTextField {

    /* These constants are used to specify the data type information needed
        to specialize input time behavior based on data type.
        For now, the only such difference is between text and numeric:
        In numeric fields, an empty field is initialzed to zero, and input of
        a decimal point when the mask contains one and the cartet is to its left
        causes the caret to move to the right of the decimal point position.
    */
  /** A constant that indicates the component string value of is data type "text".
  <p>
  The <a href="#texttype"><code>texttype</code></a>,
  <a href="#numbtype"><code>numbtype</code></a>,
  <a href="#datetype"><code>datetype</code></a>, and
  <a href="#timetype"><code>timetype</code></a>
  constants are used to specify the data type information needed
  to specialize input-time behavior based on data type.
  <p>
  For now, the only such difference is between text and numeric:
  in numeric fields, an empty field is initialzed to "0", and input of
  a decimal point when the mask contains one and the caret is to its left
  causes the caret to move to the right of the decimal point position.
  <br>
  All other types currently behave like the text type.
  @see #numbtype
  @see #datetype
  @see #timetype
  @see #getDatatype
  @see #setDatatype
  */
    public static final int texttype = MaskNumberEngine.texttype;
  /** A constant that indicates the component string value of is data type "number".
  <p>
  The <a href="#texttype"><code>texttype</code></a>,
  <a href="#numbtype"><code>numbtype</code></a>,
  <a href="#datetype"><code>datetype</code></a>, and
  <a href="#timetype"><code>timetype</code></a>
  constants are used to specify the data type information needed
  to specialize input-time behavior based on data type.
  <p>
  For now, the only such difference is between text and numeric:
  in numeric fields, an empty field is initialzed to "0", and input of
  a decimal point when the mask contains one and the caret is to its left
  causes the caret to move to the right of the decimal point position.
  <br>
  All other types currently behave like the text type.
  @see #texttype
  @see #datetype
  @see #timetype
  @see #getDatatype
  @see #setDatatype
  */
    public static final int numbtype = MaskNumberEngine.numbtype;
  /** A constant that indicates the component string value of is data type "date".
  <p>
  The <a href="#texttype"><code>texttype</code></a>,
  <a href="#numbtype"><code>numbtype</code></a>,
  <a href="#datetype"><code>datetype</code></a>, and
  <a href="#timetype"><code>timetype</code></a>
  constants are used to specify the data type information needed
  to specialize input-time behavior based on data type.
  <p>
  For now, the only such difference is between text and numeric:
  in numeric fields, an empty field is initialzed to "0", and input of
  a decimal point when the mask contains one and the caret is to its left
  causes the caret to move to the right of the decimal point position.
  <br>
  All other types currently behave like the text type.
  @see #texttype
  @see #numbtype
  @see #timetype
  @see #getDatatype
  @see #setDatatype
  */
    public static final int datetype = MaskNumberEngine.datetype;
  /** A constant that indicates the component string value of is data type "time".
  <p>
  The <a href="#texttype"><code>texttype</code></a>,
  <a href="#numbtype"><code>numbtype</code></a>,
  <a href="#datetype"><code>datetype</code></a>, and
  <a href="#timetype"><code>timetype</code></a>
  constants are used to specify the data type information needed
  to specialize input-time behavior based on data type.
  <p>
  For now, the only such difference is between text and numeric:
  in numeric fields, an empty field is initialzed to "0", and input of
  a decimal point when the mask contains one and the caret is to its left
  causes the caret to move to the right of the decimal point position.
  <br>
  All other types currently behave like the text type.
  @see #texttype
  @see #numbtype
  @see #datetype
  @see #getDatatype
  @see #setDatatype
  */
    public static final int timetype = MaskNumberEngine.timetype;

    // ctors
    /** Constructs a default <code>JMaskedNumberField</code> object.
    It initially will have no text and zero columns of length.
    The number of columns is used to calculate the preferred size of this component.
    */
	public JMaskedNumberField (                   ) { this(""         , 0              ); }

    /** Constructs a <code>JMaskedNumberField</code> object with the specified
    number of columns.
    The number of columns is used to calculate the preferred size of this component.
    @param numberOfColumns the number of columns this component has. Used to calculate this
    component's preferred size
    */
	public JMaskedNumberField (int numberOfColumns) { this(""         , numberOfColumns); }

    /** Constructs a <code>JMaskedNumberField</code> object with the specified
    text and 256 columns.
    The number of columns is used to calculate the preferred size of this component.
    @param initialText the initial text of this component (before formatting)
    */
	public JMaskedNumberField (String initialText ) { this(initialText, 256            ); }

    /** Constructs a <code>JMaskedNumberField</code> object with the specified
    text and number of columns.
    The number of columns is used to calculate the preferred size of this component.
    @param initialText the initial text of this component (before formatting)
    @param numberOfColumns the number of columns this component has. Used to calculate this
    component's preferred size
    */
	public JMaskedNumberField (String initialText, int numberOfColumns)	{
	  this(null, initialText, numberOfColumns);
	}

	// All other constructors call this one
    /** Constructs a <code>JMaskedNumberField</code> object that uses the given text container,
    which initially displays the specified text, and which has the indicated number of columns.
    The text container supports storing and editing text, and serves as the "model" in an "MVC"
    relationship.
    The number of columns is used to calculate the preferred size of this component.
    @param doc the text container to use. If <code>null</code> a default model will be created
    by calling the <a href="com.sun.java.swing.JTextField#createDefaultModel"><code>createDefaultModel</code></a> method.
    @param initialText the initial text of this component (before formatting)
    @param numberOfColumns the number of columns this component has. Used to calculate this
    component's preferred size
    */
    public JMaskedNumberField(Document doc, String initialText, int numberOfColumns) {
        super(doc, initialText, numberOfColumns);
    }

    //over-ride
    /**
     * Initialize the field. String <CODE>t</CODE> should only contain
     * character corresponding to a position in the mask. For instance,
     * if the mask is "AA/00/AA" the parameter could be "ab12cd".
     *
     * @param t The text to put in the field
     */
    public synchronized void setText ( String t ) {
        if ( t != null && t.length() > 0 ) {
            super.setText ( getMatchedText ( t ) ) ;
        }
        else {
            super.setText( t );
        }
    }

    /**
     * Initialize the field. String <CODE>t</CODE> should only contain
     * character corresponding to a position in the mask. For instance,
     * if the mask is "AA/00/AA" the parameter could be "ab12cd".
     *
     * @param t The text to put in the field
     */
    public synchronized void setMaskedText(String t) {
      //  System.out.println("setMaskedText");
        if(getMask().equals("")){
            super.setText(t);
        }else{
            StringBuffer newData = new StringBuffer();
            _firstInputPos = _MaskNumberEngine.initDisplay(t, newData);
           // System.out.println("fgtr:"+t+":"+newData);
            super.setText(newData.toString());
            if (_MaskNumberEngine._filterCount == 0 ) this.setEditable(false);
            if (_haveFocus && isEditable() && _MaskNumberEngine._filterCount != 0 ) {
                setCaretPosition (_firstInputPos + 1);
                moveCaretPosition (_firstInputPos);
            }
        }
    }

    /**
     * Returns current text with mask characters removed.
     */
  /** Gets the current text with the mask characters removed.
  @return the unmasked text
  @see #setMaskedText
  @see #getMask
  @see #setMask
  */
    public synchronized String getUnmaskedText() {
        if (getMask().equals("")) {
            return getText();
        } else {
            StringBuffer newData = new StringBuffer();
            _dataComplete = _MaskNumberEngine.stripMask(getText(), newData);
            return newData.toString();
        }
    }

  /**
   * This is a standard AWT method which gets called when
   * this component is added to a container.
   <br>
   * It has been overridden to add listener(s).
   *
   * @see #removeNotify
   */
    public synchronized void addNotify() {
        if (_focusListener == null) {
            _focusListener = new FocusAdapter() {
                               public void focusGained(FocusEvent e) { gotFocus (e); }
                        //        public void focusLost  (FocusEvent e) { lostFocus(e); }
                            };
            addFocusListener(_focusListener);
        }
        super.addNotify();
    }

  /**
   * This method gets called when this component is removed from a
   * container.
   <br>
   * It has been overridden here to remove listener(s).
   *
   * @see #addNotify
   */
    public synchronized void removeNotify() {
        if (_focusListener != null) {
    	    removeFocusListener(_focusListener);
    	    _focusListener = null;
        }
        super.removeNotify();
    }

  /**
  Cuts the selected text and saves it in the clipboard.
  @param clipboard the destination of the cut text
  */
    public synchronized void cut(Clipboard clipboard) {
        if (!isEditable()) {
            return;
        }
        _activity = true;
        StringBuffer newData = new StringBuffer();
        int selStart = getSelectionStart();
        String clipboardData = _MaskNumberEngine.cut(getText(), selStart,
                                            getSelectionEnd(),
                                            newData);
        StringSelection ss = new StringSelection(clipboardData);
        clipboard.setContents(ss, ss);
        super.setText(newData.toString());
        setCaretPosition(selStart);
    }

  /**
  Pastes the string on the clipboard into the current text selection.
  @param clipboard the source of the string to paste
  */
    public synchronized void paste(Clipboard clipboard) {

        if (!isEditable()) {
            return;
        }

        _activity = true;
        StringBuffer newData = new StringBuffer();
        String data = "";

        try {
            data = (String)clipboard.getContents(this).getTransferData(DataFlavor.stringFlavor);
        } catch (Exception e) {}

        int pos = getCaretPosition();
        int selStart = getSelectionStart();
        int selEnd = getSelectionEnd();
        int newpos = _MaskNumberEngine.paste(getText(), data, pos, newData,
                                    selStart, selEnd);
        if (newpos < 0)   { // beep if paste failed
            getToolkit().beep();
            return;
        }
        if (newpos == -2)  { // quit if filter mismatch
            return;
        }

        super.setText(newData.toString());

        if (newpos >= 0) {              // if good new caret position
            setCaretPosition(newpos + 1);  // select this position
            moveCaretPosition (newpos);
        }
        else {
            setCaretPosition (0);  // turn off selection
            moveCaretPosition (0);
            newpos = newpos - getNumberOfCaseConvertors();
            if (newpos != -1) { // cursor just moved out of range
                setCaretPosition(newpos + 1000);  // move one past last filter
            }
        }
    }

    // Override of JTextComponent
  /**
  Cuts the selected text and saves it in the system clipboard.
  */
    public void cut() {
        //vasu start
        if(getMask().equals("")){
            super.cut();
        }else{
        //vasu end
            cut(java.awt.Toolkit.getDefaultToolkit().getSystemClipboard());
        }
    }

    // Override of JTextComponent
  /**
  Pastes the string on the system clipboard into the current text selection.
  */
    public void paste() {
        //vasu start
        if(getMask().equals("")) {
            super.paste();
        }else{
        //vasu end
            paste(java.awt.Toolkit.getDefaultToolkit().getSystemClipboard());
        }
    }

  /** Sets the formatting mask for this component to use.
  <p>
  The following table defines the special mask characters.
  <p>
  If you want to be always displayed, instead of being interpreted as a
  special mask character, preface it with an escape ('\').
  This includes spaces. For instance, the mask for a long distance
  telephone number could be: \(999\)\ 999\-9999.

     <table>
     <tr><th>Use</th>               <th>To restrict the digit position to</th></tr>
     <tr><td><code>\</code></td>    <td>Escape (characters are displayed as literals)</td></tr>
     <tr><td><code>0</code></td>    <td>Digit</td></tr>
     <tr><td><code>9</code></td>    <td>Digit or Space</td></tr>
     <tr><td><code>#</code></td>    <td>Digit, Sign (+ or -), or Space</td></tr>
     <tr><td><code>L</code></td>    <td>Letter (A through Z) entry required</td></tr>
     <tr><td><code>?</code></td>    <td>Letter (A through Z) entry optional</td></tr>
     <tr><td><code>A</code></td>    <td>Letter or Digit - entry required</td></tr>
     <tr><td><code>a</code></td>    <td>Letter or Digit - entry optional</td></tr>
     <tr><td><code>&amp;</code></td>    <td>Any character or space - entry required</td></tr>
     <tr><td><code>C</code></td>    <td>Any character or space - entry optional</td></tr>
     <tr><td><code>. ,</code></td>  <td>Decimal placeholder and Thousands separator</td></tr>
     <tr><td><code>:;-/</code></td> <td>Date and Time separators</td></tr>
     <tr><td><code>&lt;</code></td> <td>Forces all characters that follow to convert to lowercase</td></tr>
     <tr><td><code>&gt;</code></td> <td>Forces all characters that follow to convert to uppercase</td></tr>
	 </table>

  @param mask the new mask
  @see #getMask
  */
    public void   setMask    (String mask) {
        _MaskNumberEngine.setMask( mask );
        setText ( getText() );
    }

  /** Gets the formatting mask this component uses.
  <p>
  See <a href="#setMask"><code>setMask</code></a>
  method for mask format details.
  @return the mask
  @see #setMask
  */
    public String getMask    (           ) { return _MaskNumberEngine.getMask    (    ); }
  /** Sets the data type the component's string value represents.
  Knowledge of the data type is used to specialize input-time behavior.
  <p>
  Possible values are:<ul>
  <li><a href="#texttype"><code>texttype</code></a> - text
  <li><a href="#numbtype"><code>numbtype</code></a> - number
  <li><a href="#datetype"><code>datetype</code></a> - date
  <li><a href="#timetype"><code>timetype</code></a> - time
  </ul>
  <p>
  For now, the only difference is between text and numeric:
  in numeric fields, an empty field is initialzed to "0", and input of
  a decimal point when the mask contains one and the caret is to its left
  causes the caret to move to the right of the decimal point position.
  <br>
  All other types currently behave like the text type.
  @see #getDatatype
  */
    public void   setDatatype(int    type) {        _MaskNumberEngine.setDatatype(type); }
  /** Gets the data type the component's string value represents.
  Knowledge of the data type is used to specialize input-time behavior.
  <p>
  Possible values are:<ul>
  <li><a href="#texttype"><code>texttype</code></a> - text
  <li><a href="#numbtype"><code>numbtype</code></a> - number
  <li><a href="#datetype"><code>datetype</code></a> - date
  <li><a href="#timetype"><code>timetype</code></a> - time
  </ul>
  <p>
  For now, the only difference is between text and numeric:
  in numeric fields, an empty field is initialzed to "0", and input of
  a decimal point when the mask contains one and the caret is to its left
  causes the caret to move to the right of the decimal point position.
  <br>
  All other types currently behave like the text type.
  @return the current data type
  @see #setDatatype
  */
    public int    getDatatype(           ) { return _MaskNumberEngine.getDatatype(    ); }

  /** Determines whether the appropriate characters have been entered for all
  the mandatory characters in the current mask.
  @return <code>true</code> if all mandatory characters entered, <code>false</code> otherwise
  */
    public boolean isDataComplete() {

        //vasu start
        if(getMask().equals(""))  {
            return true;
        }
        //vasu end
        if (!_dataComplete) {
            if (!_activity) {
                return true;
            }
            _dataComplete = _MaskNumberEngine.stripMask(getText(), new StringBuffer());
        }
        return _dataComplete;
    }

    protected String  getMatchedText ( String t ) {

        return  _MaskNumberEngine.getMatchedText ( t );

    }

  /** Internal use.
  Called internally by the focus listener.
  @param e the notifying event
  */
    protected void gotFocus(FocusEvent e) {
        _activity = false;
        _haveFocus = true;
        if (getText().length() == 0) {
            setMaskedText("");  // show the mask literals if field is empty
        } else if (isEditable() && !getMask().equals("") &&  _MaskNumberEngine._filterCount != 0  //--Ayman
                &&  getCaretPosition () <= _MaskNumberEngine.lastFilterPosition ()) {
            int oldCaretPosition = getCaretPosition ();
            setCaretPosition (oldCaretPosition + 1);
            moveCaretPosition (oldCaretPosition);
        }
    }

    /**
     * This method is called when the focus masked text field looses the
     * focus. It will set the new value of the mask in the model seen
     * outside of this class. Since someone outside of this class could
     * also listen on the focus lost event, and we can't be sure the
     * this method is called before anyone'n listener, we will generate
     * an other focus lost event (in fact it will be the same one), once
     * the value of the "public model" is changed. Methods inherinting
     * from this one have to do the same thing.
     */
    protected void lostFocus(FocusEvent e) {
        if (_haveFocus) { // Nothing to do if we don't have the focus.
            _haveFocus = false;
            if (isEditable() && _activity) {
                try {
                    _docListenerDisabled = true;
                    _document.remove(0, _document.getLength());
                    _document.insertString(0, getUnmaskedText(), null);
                    processFocusEvent (e); // Because public model changed.
                } catch (javax.swing.text.BadLocationException ex) {
                    badLocException();
                } finally {
                    _docListenerDisabled = false;
                }
            }
        }
    }

    // Override JTextComponent
  /** Internal use.
  Handles key events this component is interested in.
  @param e the key event to check
  */
    protected void processComponentKeyEvent(KeyEvent e) {
        //vasu start - because we check whether or not something
        //             was done before we update the model
        _activity = true;
        //vasu end

        switch(e.getID()) {

        case KeyEvent.KEY_TYPED:
           if (_MaskNumberEngine.isHandledKey(e) && isEditable()) {
                 processKey(e);
            }
            else {
                super.processComponentKeyEvent(e);
            }
            break;

        case KeyEvent.KEY_PRESSED:
           if (_MaskNumberEngine.isHandledKey(e) && isEditable()) {
                e.consume();
               if (e.getKeyCode()==KeyEvent.VK_LEFT ||e.getKeyCode()==KeyEvent.VK_RIGHT)
                 processKey(e);
               else if (_keyPressed) { // key must be auto-repeating
                  if (Character.isISOControl(e.getKeyChar())) {
                        processKey(e);
                  }

                }
                else {
                  _keyPressed = true;
                }
            }
            else {
                super.processComponentKeyEvent(e);
            }
          break;

        case KeyEvent.KEY_RELEASED:
            _keyPressed = false;
            if (_MaskNumberEngine.isHandledKey(e) && isEditable()) {
            if (Character.isISOControl(e.getKeyChar())) {
                    processKey(e);
                }
            } else {
                super.processComponentKeyEvent(e);
            }
            break;

        }


    }

  /** Internal use.
  Called internally by <code>processComponentKeyEvent</code>.
  @param e the notifying event
  */
    protected void processKey(KeyEvent e)
    {
        _activity = true;
        e.consume();
        StringBuffer newData = new StringBuffer("");
        String data = getText();
        int selStart = getSelectionStart();
        int selEnd = getSelectionEnd();
        // Fixed bug 70799
        // If there is a selected area ensure that the current position is
        // the begining of the selection
        if (selStart >= 0 && selEnd >= 0 && (selEnd - selStart) > 1) {
            setCaretPosition(selStart);
        }
        int caretPosition = getCaretPosition();
        int newpos = _MaskNumberEngine.processKey (e, caretPosition, data, newData,
                                             selStart, selEnd);
        if (newpos == -1) { // Inconsistent input
            getToolkit().beep();
        } else if (newpos != -2) { // Nothing done if filter mismatch
            super.setText(newData.toString());
            if (newpos >= 0) { // If good new caret position
                setCaretPosition(newpos + 1);
                moveCaretPosition (newpos);  // Select this position
            } else { // Cursor just moved out of range
                setCaretPosition (0);  // turn off selection
                moveCaretPosition (0);
                setCaretPosition((newpos + 3)*(-1)); // Move past last filter
            }
        }
    }


    // Override
    // This gets called during superclass construction!
  /** Sets the document to associate with this component.
  @param d the document
  */
    public void setDocument(Document d) {

        if (_docListener == null) {
            _docListener = new DocumentListener() {
                public void changedUpdate(DocumentEvent e) { docChange(e); }
                public void removeUpdate (DocumentEvent e) { docRemove(e); }
                public void insertUpdate (DocumentEvent e) { docInsert(e); }
            };
        }

        if (_document != null) {
            _document.removeDocumentListener(_docListener);
        }

        _document = d;
        _document.addDocumentListener(_docListener);

        if (_myDoc == null) {
            _myDoc = new PlainDocument();
        }
        super.setDocument(_myDoc);
    }

    // These shouldn't get called
  /** Internal use.
  Called internally by the document listener when notified of changes to a document.
  @param e the notifying document event
  */
    protected void docChange(DocumentEvent e) {
        if (_docListenerDisabled) {
            return;
        }

    }

  /** Internal use.
  Called internally by the document listener when notified of removal of part of a document.
  @param e the notifying document event
  */
    protected void docRemove(DocumentEvent e) {
        if (_docListenerDisabled) {
            return;
        }

        super.setText("");
    }

    // This gets called on data insertion into the field
  /** Internal use.
  Called internally by the document listener when notified of insertion into a document.
  @param e the notifying document event
  */
    protected void docInsert(DocumentEvent e) {
        if (_docListenerDisabled) {
            return;
        }
        int len = e.getLength();
        String data = "";

        try {
            data = _document.getText(0, len);
        } catch(javax.swing.text.BadLocationException ex) {
            badLocException();
        }

        setMaskedText(data);
    }

  /** <code>BadLocationException</code> handler.
  Emits a beep.
  */
    protected void badLocException() {
        getToolkit().beep();
       // System.out.println("Bad Location Exception in JMaskedNumberField");
    }

    private int getNumberOfCaseConvertors() {
        int retInt = 0;

        java.text.StringCharacterIterator iter =  new java.text.StringCharacterIterator ( getMask() );
        while ( iter.next() != iter.DONE ) {
            if ( iter.current() == '<' || iter.current() == '>' ) {
                retInt++;
            }
        }

        return retInt;
    }

    public boolean isSpaceIgnored ()
    {
        return _MaskNumberEngine.isSpaceIgnored ();
    }

    public void setSpaceIgnored (boolean spaceIgnored)
    {
        _MaskNumberEngine.setSpaceIgnored (spaceIgnored);
    }

    /**
     * If set to true, the method getUnmaskedText would return all the
     * separators and literals. If false, it will return only the characters
     * corresponding to a mask character.
     *
     * @return true if getUnmaskedText returns literals
     * @see    setIncludeLiterals
     */
    public boolean isIncludeLiterals ()
    {
        return _MaskNumberEngine.isIncludeLiterals ();
    }

    /**
     * Sets if getUnmaskedText must return literals.
     *
     * @param includeLiterals true if getUnmaskedText returns literals
     * @see   isIncludeLiterals
     */
    public void setIncludeLiterals (boolean includeLiterals)
    {
        _MaskNumberEngine.setIncludeLiterals (includeLiterals);
    }

    private PlainDocument    _myDoc         = null;
    private DocumentListener _docListener   = null;
    private Document         _document      = null;
    private boolean          _docListenerDisabled = false;
    private boolean          _dataComplete  = true;    // true iff no mandatory filters are empty
    private boolean          _haveFocus     = false;   // true if this component has input focus
    private boolean          _keyPressed    = false;   // true if a key is down
    private boolean          _activity      = false;
    private int              _firstInputPos = 0;       // position of first filter
    private MaskNumberEngine       _MaskNumberEngine    = new MaskNumberEngine();
    private FocusAdapter     _focusListener = null;

        //debug flag
    private boolean          debug          = true;
}
