/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/

package com.computhink.aip.util;

import java.awt.*;
import java.awt.event.*;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;

import javax.swing.*;
import javax.swing.event.PopupMenuListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.plaf.ComboBoxUI;
import javax.swing.plaf.basic.ComboPopup;
import javax.swing.plaf.metal.MetalComboBoxUI;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.EmptyBorder;

import com.computhink.vwc.VWUtil;
import com.sun.java.swing.plaf.motif.MotifComboBoxUI;
import com.sun.java.swing.plaf.windows.WindowsComboBoxUI;

public class VWDateComboBox extends JComboBox {

    String dateMask="";        
    private int firstOfWeek = 1;
    public VWDateComboBox() {
        SymKey aSymKey=new SymKey();
        getEditor().getEditorComponent().addKeyListener(aSymKey);
        setEditable(false);
        //Block the following key in the calendar control to avoid the exception
    	InputMap im = getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    	KeyStroke tab = KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0);        	
    	KeyStroke shiftTab = KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 1);
    	KeyStroke shift = KeyStroke.getKeyStroke(KeyEvent.VK_SHIFT, 1);
    	im.put(tab, "none");
    	im.put(shiftTab, "none");
    	im.put(shift, "none");
        
    }
    //--------------------------------------------------------------------------
    class SymKey extends java.awt.event.KeyAdapter {
        public void keyTyped(java.awt.event.KeyEvent event) {
            //Object object = event.getSource();
            if(event.getKeyText(event.getKeyChar()).equals("Escape")||
            event.getKeyText(event.getKeyChar()).equals("Delete")||
            event.getKeyText(event.getKeyChar()).equals("Backspace"))
                Clear_actionPerformed();
        }
        public void keyReleased(java.awt.event.KeyEvent event) {
            //Object object = event.getSource();
            if(event.getKeyText(event.getKeyChar()).equals("Escape") ||
            event.getKeyText(event.getKeyChar()).equals("Delete")||
            event.getKeyText(event.getKeyChar()).equals("Backspace"))
                Clear_actionPerformed();
        }
    }
    //--------------------------------------------------------------------------
    void Clear_actionPerformed() {
        this.setSelectedIndex(-1);
    }
    //--------------------------------------------------------------------------
    public void setDateFormat(String mask){
        dateMask=mask;
        dateMask=VWUtil.replaceWord(dateMask, "dddd", "EEEE");
        dateMask=VWUtil.replaceWord(dateMask, "ddd", "EEE");
    }
    //--------------------------------------------------------------------------
    public void clear() {
        this.setSelectedIndex(-1);
        this.setText("");
    }
    //--------------------------------------------------------------------------
    public String getText() {
        if(getSelectedIndex()<0 || getSelectedItem()==null || getSelectedItem().equals(""))
            return "";
        return getText(getSelectedItem());
    }
    //--------------------------------------------------------------------------
    public String getText(Object dateObj) {
        if(dateMask==null || dateMask.equals(""))
            return (String)getSelectedItem();
        Date date=new Date();
        try
        {
            date = DateFormat.getDateInstance().parse((String)dateObj);
        }
        catch (java.text.ParseException e)
        {
        }
        Format formatter;
        formatter = new SimpleDateFormat(dateMask,Locale.ENGLISH);
        String s = formatter.format(date);
        return s;
    }
    //--------------------------------------------------------------------------
    public void setText(String date) {
        if(date.equals("") || date.equals("."))
            setSelectedIndex(-1);
        setSelectedItem(date);
    }
    //--------------------------------------------------------------------------
    /*
    public void setDateFormat(String mask,Locale localize) {
        if(mask!=null && !mask.equals("")){
            mask =MatchVWMask(mask);
            if(localize == null ) localize=Locale.getDefault();
            try{
                dateFormat = new SimpleDateFormat(mask,localize);
            }catch (Exception e){
                System.out.println(e.getLocalizedMessage());
                dateFormat = new SimpleDateFormat("yyyyMMdd",localize);
            }
        }
    }
    //--------------------------------------------------------------------------
    public static String MatchVWMask(String mask){
        int BDayint =mask.indexOf("ddd");
        int EDayint = BDayint+2;
        if (BDayint >= 0){
            boolean  MoreThree = false;
            while(mask.charAt(EDayint+1)=='d'){
                EDayint++;
                if(!MoreThree) MoreThree =true;
            }
            String Change ="EEE";
            if(MoreThree) Change ="EEEE";
            if (BDayint == 0)
                mask = Change+mask.substring(EDayint+1,mask.length());
            else
                mask =mask.substring(0,BDayint)+Change+mask.substring(EDayint+1,mask.length());
        }
        return mask;
    }
     */
    //--------------------------------------------------------------------------
    public void setSelectedItem(Object item) {
        // Could put extra logic here or in renderer when item is instanceof Date, Calendar, or String
        // Dont keep a list ... just the currently selected item
        ///removeAllItems(); // hides the popup if visible
        addItem(item);
        super.setSelectedItem(item);
    }

    public void updateUI(){
        ComboBoxUI cui = (ComboBoxUI) UIManager.getUI(this);
        if (cui instanceof MetalComboBoxUI){
            cui = new MetalDateComboBoxUI();
        } else if (cui instanceof MotifComboBoxUI){
            cui = new MotifDateComboBoxUI();
        } else if (cui instanceof WindowsComboBoxUI){
            cui = new WindowsDateComboBoxUI();
        }
        setUI(cui);
    }
    // Inner classes are used purely to keep DateComboBox component in one file
    //////////////////////////////////////////////////////////////
    // UI Inner classes -- one for each supported Look and Feel
    //////////////////////////////////////////////////////////////
    
    class MetalDateComboBoxUI extends MetalComboBoxUI {
        protected ComboPopup createPopup() {
            return new DatePopup( comboBox );
        }
    }
    
    class WindowsDateComboBoxUI extends WindowsComboBoxUI {
        protected ComboPopup createPopup() {
            return new DatePopup(comboBox);
        }
    }
    
    class MotifDateComboBoxUI extends MotifComboBoxUI {
        protected ComboPopup createPopup() {
            return new DatePopup( comboBox );
        }
    }
    
    //////////////////////////////////////////////////////////////
    // DatePopup inner class
    //////////////////////////////////////////////////////////////
    
    class DatePopup implements ComboPopup, MouseMotionListener,
    MouseListener, KeyListener, PopupMenuListener {
        
        protected JComboBox comboBox;
        protected Calendar calendar;
        protected JPopupMenu popup;
        protected JLabel monthLabel;
        protected JPanel days = null;
        
        protected Color selectedBackground;
        protected Color selectedForeground;
        protected Color background;
        protected Color foreground;
        
        public DatePopup(JComboBox comboBox) {
            this.comboBox = comboBox;
            calendar = Calendar.getInstance();
            background = UIManager.getColor("ComboBox.background");
            foreground = UIManager.getColor("ComboBox.foreground");
            selectedBackground = UIManager.getColor("ComboBox.selectionBackground");
            selectedForeground = UIManager.getColor("ComboBox.selectionForeground");
            initializePopup();
        }
        //========================================
        // begin ComboPopup method implementations
        //
        public void show() {
            try {
                // if setSelectedItem() was called with a valid date, adjust the calendar
                String strDate=comboBox.getSelectedItem().toString();
                if(strDate==null || strDate.equals(""))
                {
                    strDate = DateFormat.getDateInstance().format(new Date());
                }
            	SimpleDateFormat formatter = new SimpleDateFormat(dateMask);
            	Date dateObj = formatter.parse(strDate);
            	//String s = formatter.format(dateObj);
            	calendar.setTime(dateObj);
            } catch (Exception e){
            	//System.out.println("Show date Ex in " + e.getMessage());
            }
            Rectangle rect = comboBox.getBounds();
            updatePopup();            
            //popup.show(comboBox, 0, comboBox.getHeight());
            //System.out.println("rect " + rect.x +  " : " + rect.y);
            //System.out.println("comboBox.getParent() " + comboBox.getParent().getX() +  " : " + comboBox.getParent().getY());
            //System.out.println("comboBox.getParent() HW " + comboBox.getParent().getHeight() +  " : " + comboBox.getParent().getWidth());
            int x = rect.x;
            int y = rect.y;
            if (rect.y < 80)
            	popup.show(comboBox, 0, comboBox.getHeight());
            else{
		        if (firstOfWeek == 6 || firstOfWeek == 7)
		        	popup.show(comboBox.getParent(), rect.x, (rect.y - 120));
		        else
		        	popup.show(comboBox.getParent(), rect.x, (rect.y - 106));
            }
        }

        public void hide() {
            popup.setVisible(false);
        }

        protected JList list = new JList();
        public javax.swing.JList getList() {
            return list;
        }
        
        public java.awt.event.MouseListener getMouseListener() {
            return this;
        }
        
        public java.awt.event.MouseMotionListener getMouseMotionListener() {
            return this;
        }
        
        public java.awt.event.KeyListener getKeyListener() {
            return this;
        }
        
        public boolean isVisible() {
            return popup.isVisible();
        }
        
        public void uninstallingUI() {
            popup.removePopupMenuListener(this);
        }
        
        //
        // end ComboPopup method implementations
        //======================================
        
        //===================================================================
        // begin Event Listeners
        //
        
        // MouseListener
        
        public void mousePressed( MouseEvent e ) {}
        public void mouseReleased( MouseEvent e ) {}
        // something else registered for MousePressed
        public void mouseClicked(MouseEvent e) {
            if ( !SwingUtilities.isLeftMouseButton(e) )
                return;
            if ( !comboBox.isEnabled() )
                return;
            /*if ( comboBox.isEditable() ) {
                comboBox.getEditor().getEditorComponent().requestFocus();
            } else {
                comboBox.requestFocus();
            }*/
            comboBox.requestFocusInWindow();//.requestFocus();
            togglePopup();
        }
        
        protected boolean mouseInside = false;
        public void mouseEntered(MouseEvent e) {
            mouseInside = true;
        }
        public void mouseExited(MouseEvent e) {
            mouseInside = false;
        }
        
        // MouseMotionListener
        public void mouseDragged(MouseEvent e) {}
        public void mouseMoved(MouseEvent e) {}
        
        // KeyListener
        public void keyPressed(KeyEvent e) {}
        public void keyTyped(KeyEvent e) {}
        public void keyReleased( KeyEvent e ) {
            if ( e.getKeyCode() == KeyEvent.VK_SPACE ||
            e.getKeyCode() == KeyEvent.VK_ENTER ) {
                togglePopup();
            }
            else if (e.getKeyCode() == KeyEvent.VK_CANCEL ||
            e.getKeyCode() == KeyEvent.VK_DELETE ||
            e.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
                comboBox.setSelectedItem("");
                comboBox.setSelectedIndex(-1);
            }
        }
        /**
         * Variables hideNext and mouseInside are used to
         * hide the popupMenu by clicking the mouse in the JComboBox
         */
        public void popupMenuCanceled(PopupMenuEvent e) {}
        protected boolean hideNext = false;
        public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
            hideNext = mouseInside;
        }
        public void popupMenuWillBecomeVisible(PopupMenuEvent e) {}
        
        //
        // end Event Listeners
        //=================================================================
        
        //===================================================================
        // begin Utility methods
        //
        
        protected void togglePopup() {
            if ( isVisible() || hideNext ) {
                hide();
            } else {
                show();
            }
            hideNext = false;
        }
        
        //
        // end Utility methods
        //=================================================================
        
        // Note *** did not use JButton because Popup closes when pressed
        protected JLabel createUpdateButton(final int field, final int amount) {
            final JLabel label = new JLabel();
            final Border selectedBorder = new EtchedBorder();
            final Border unselectedBorder = new EmptyBorder(selectedBorder.getBorderInsets(new JLabel()));
            label.setBorder(unselectedBorder);
            label.setForeground(foreground);
            label.addMouseListener(new MouseAdapter() {
                public void mouseReleased(MouseEvent e) {
                    calendar.add(field, amount);
                    updatePopup();
                }
                public void mouseEntered(MouseEvent e) {
                    label.setBorder(selectedBorder);
                }
                public void mouseExited(MouseEvent e) {
                    label.setBorder(unselectedBorder);
                }
            });
            return label;
        }
        // Added to have empty date value. Valli 24 Oct 2007
        protected JLabel createNoneButton() {
            final JLabel label = new JLabel();
            final Border selectedBorder = new EtchedBorder();
            final Border unselectedBorder = new EmptyBorder(selectedBorder.getBorderInsets(new JLabel()));
            label.setBorder(unselectedBorder);
            label.setForeground(foreground);
            label.addMouseListener(new MouseAdapter() {
                public void mouseReleased(MouseEvent e) {
                	clear();
                	label.setOpaque(false);
                    label.setBackground(background);
                    label.setForeground(foreground);
                    popup.setVisible(false);
                    //comboBox.setSelectedItem("");
                    //comboBox.requestFocus();
                }
                public void mouseEntered(MouseEvent e) {
                    label.setBorder(selectedBorder);
                }
                public void mouseExited(MouseEvent e) {
                    label.setBorder(unselectedBorder);
                }
            });
            return label;
        }
        
        
        protected void initializePopup() {
            JPanel header = new JPanel(); // used Box, but it wasn't Opaque
            header.setLayout(new BoxLayout(header, BoxLayout.X_AXIS));
            header.setBackground(background);
            header.setOpaque(true);
            
            JLabel label;
            JLabel labelNone;// = new JLabel();
            label = createUpdateButton(Calendar.YEAR, -1);
            label.setText("<<");
            label.setToolTipText("Previous Year");
            
            header.add(Box.createHorizontalStrut(12));
            header.add(label);
            header.add(Box.createHorizontalStrut(12));
            
            label = createUpdateButton(Calendar.MONTH, -1);
            label.setText("<");
            label.setToolTipText("Previous Month");
            header.add(label);
            
            monthLabel = new JLabel("", JLabel.CENTER);
            monthLabel.setForeground(foreground);
            header.add(Box.createHorizontalGlue());
            header.add(monthLabel);
            header.add(Box.createHorizontalGlue());
            
            label = createUpdateButton(Calendar.MONTH, 1);
            label.setText(">");
            label.setToolTipText("Next Month");
            header.add(label);
            
            label = createUpdateButton(Calendar.YEAR, 1);
            label.setText(">>");
            label.setToolTipText("Next Year");
            
            header.add(Box.createHorizontalStrut(12));
            header.add(label);
            header.add(Box.createHorizontalStrut(12));
            
            labelNone = createNoneButton();
            labelNone.setText("None");
            labelNone.setToolTipText("Clear");
            
            //header.add(Box.createHorizontalStrut(12));
            header.add(labelNone);
            //header.add(Box.createHorizontalStrut(12));
            
            popup = new JPopupMenu();
            popup.setBorder(BorderFactory.createLineBorder(Color.black));
            popup.setLayout(new BorderLayout());
            popup.setBackground(background);
            popup.addPopupMenuListener(this);
            popup.add(BorderLayout.NORTH, header);            
        }
        
        // update the Popup when either the month or the year of the calendar has been changed
        protected void updatePopup() {
            ///monthLabel.setText( monthFormat.format(calendar.getTime()) );
            monthLabel.setText(DateFormat.getDateInstance().
                format(calendar.getTime()));
            //monthLabel.setText(getText(calendar.getTime()));
            monthLabel.setText(getText(monthLabel.getText()));
            if (days != null) {
                popup.remove(days);
            }
            days = new JPanel(new GridLayout(0, 7));
            days.setBackground(background);
            days.setOpaque(true);
            
            Calendar setupCalendar = (Calendar) calendar.clone();
            setupCalendar.set(Calendar.DAY_OF_WEEK, setupCalendar.getFirstDayOfWeek());
            for (int i = 0; i < 7; i++) {
                int dayInt = setupCalendar.get(Calendar.DAY_OF_WEEK);
                JLabel label = new JLabel();
                label.setHorizontalAlignment(JLabel.CENTER);
                label.setForeground(foreground);
                if (dayInt == Calendar.SUNDAY) {
                    label.setText("Sun");
                } else if (dayInt == Calendar.MONDAY) {
                    label.setText("Mon");
                } else if (dayInt == Calendar.TUESDAY) {
                    label.setText("Tue");
                } else if (dayInt == Calendar.WEDNESDAY) {
                    label.setText("Wed");
                } else if (dayInt == Calendar.THURSDAY) {
                    label.setText("Thu");
                } else if (dayInt == Calendar.FRIDAY) {
                    label.setText("Fri");
                } else if (dayInt == Calendar.SATURDAY){
                    label.setText("Sat");
                }
                days.add(label);
                setupCalendar.roll(Calendar.DAY_OF_WEEK, true);
            }
            
            setupCalendar = (Calendar) calendar.clone();
            setupCalendar.set(Calendar.DAY_OF_MONTH, 1);
            int first = setupCalendar.get(Calendar.DAY_OF_WEEK);
            firstOfWeek  = first;
            for (int i = 0; i < (first - 1); i++) {
                days.add(new JLabel(""));
            }
            for (int i = 1; i <= setupCalendar.getActualMaximum(Calendar.DAY_OF_MONTH); i++) {
                final int day = i;
                final JLabel label = new JLabel(String.valueOf(day));
                label.setHorizontalAlignment(JLabel.CENTER);
                label.setForeground(foreground);
                label.addMouseListener(new MouseListener() {
                    public void mousePressed(MouseEvent e) {}
                    public void mouseClicked(MouseEvent e) {}
                    public void mouseReleased(MouseEvent e) {
                        label.setOpaque(false);
                        label.setBackground(background);
                        label.setForeground(foreground);
                        calendar.set(Calendar.DAY_OF_MONTH, day);
                        comboBox.setSelectedItem(getText(DateFormat.getDateInstance().format(calendar.getTime())));
                        ///comboBox.setSelectedItem(DateFormat.getDateInstance(DateFormat.SHORT).format(calendar.getTime()));
                        
                        ///comboBox.setSelectedItem(dateFormat.format(calendar.getTime()));
                        //hide();
                        // hide is called with setSelectedItem() ... removeAll()
						//Hiding the Calender after selecting Date...
                        hide();
                        comboBox.requestFocusInWindow();//.requestFocus();
                    }
                    public void mouseEntered(MouseEvent e) {
                        label.setOpaque(true);
                        label.setBackground(selectedBackground);
                        label.setForeground(selectedForeground);
                    }
                    public void mouseExited(MouseEvent e) {
                        label.setOpaque(false);
                        label.setBackground(background);
                        label.setForeground(foreground);
                    }
                });
                days.add(label);
            }
            popup.add(BorderLayout.CENTER, days);                
            popup.pack();
        }        
    }
    public static void main(String[] args) {
    	System.out.println("show Date : ");
    	String dateMask = "dd/MM/yyyy";
        //String strDate= "27/09/1972";
    	String strDate= "20/03/2006";
    	 
        System.out.println("Date : " + strDate);
        try{
        Date date = DateFormat.getDateInstance().parse(strDate);
        System.out.println("date  " + date );
        	//Date dateObj = new Date(strDate);
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date dateObj = formatter.parse(strDate); 
        String s = formatter.format(dateObj);
        System.out.println("s " + s);
        }catch(Exception ex){
        	System.out.println("Exception " + ex.getMessage());
        }
		
	}
}