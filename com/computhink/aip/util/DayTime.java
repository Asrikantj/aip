// DAYTIME Daemon - Gives the Time Of Day as a string.
// Sits on Port 13 and waits for requests.
//
// Copyright 1998 Michael Lecuyer.n (mjl@theorem.com)
// Permission is granted to use this code as you like.  I'm not responsible
// any problems encountered using this.  Not suitable for life support systems
// unless you need a good laugh.
//
// Version 1.0 June 23, 1998 Initial coding
// Version 1.1 June 27, 1998 Fixed DST zone reporting somewhat.
//                           Fixed undocumented Day Of Week offset.

// There are only two industries that refer to their customers as "users".
//        -- Edward Tufte

package com.computhink.aip.util;

import java.io.*;
import java.util.*;

// This merely imports the Fmt Class which you can find at:
// http://www.acme.com/java/
// The class source is available so you can put it in any packager you want.
// A pretty handy class to have around.
// Fmt is Copyright (C) 1996 by Jef Poskanzer <jef@acme.com>.  All rights reserved.
//import com.theorem.misc.*;


// Create a standard date.
// There's a somewhat non-standard method of generating a daylight savings
// time zone - by replacing the middle character with a 'D'.  If this
// doesn't work for you change the code.  "ids[0]" is the time zone
// string.
//
public class DayTime {

   static final String monthName[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
   static final String dayName[] = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };

   /* added by amaleh on 2003-06-27 */
   public static String formatNumber(int number, int maxDigits)
   {
       String numberString = Integer.toString(number);
       while (numberString.length() < maxDigits)
           numberString = "0" + numberString;
       return numberString;
   }

   public static String date(Date date)
   {
      // Time format: 27 Oct 1998 15:27:39 PST
      // Get Greg's time of day       
      Calendar dt = new GregorianCalendar();
      StringBuffer s = new StringBuffer();

      String[] ids = TimeZone.getAvailableIDs(dt.get(dt.ZONE_OFFSET));
      if (ids.length == 0)
         return "Unknown Date - no time zone names available";

      // If in daylight savings time change the middle letter of the
      // time zone to 'D'.
      if (dt.get(dt.DST_OFFSET) > 0)
      {
         StringBuffer b = new StringBuffer(ids[0]);
         b.setCharAt(1, 'D');
         ids[0] = b.toString();
      }
      //s.append(dayName[dt.get(dt.DAY_OF_WEEK)-1]).append(' ');
      s.append(monthName[dt.get(dt.MONTH)]).append(' ').append(formatNumber(dt.get(dt.DATE), 2)).append(" ").append(dt.get(dt.YEAR));
      s.append(' ').append(formatNumber(dt.get(dt.HOUR_OF_DAY),2)).append(":").append(formatNumber(dt.get(dt.MINUTE),2)).append(":").append(formatNumber(dt.get(dt.SECOND),2));
      //s.append(' ').append(ids[0]);

      return s.toString();
   }
  
   public static String rawDate()
   {
      // Time format: 27 Oct 1998 15:27:39 PST
      // Get Greg's time of day
      Calendar dt = new GregorianCalendar();
      StringBuffer s = new StringBuffer();

      String[] ids = TimeZone.getAvailableIDs(dt.get(dt.ZONE_OFFSET));
      if (ids.length == 0)
         return "Unknown Date - no time zone names available";

      // If in daylight savings time change the middle letter of the
      // time zone to 'D'.
      if (dt.get(dt.DST_OFFSET) > 0)
      {
         StringBuffer b = new StringBuffer(ids[0]);
         b.setCharAt(1, 'D');
         ids[0] = b.toString();
      }
      //s.append(dayName[dt.get(dt.DAY_OF_WEEK)-1]).append(' ');
      s.append(dt.get(dt.YEAR)).append(formatNumber(dt.get(dt.MONTH) + 1, 2)).append(formatNumber(dt.get(dt.DATE), 2));
      s.append(formatNumber(dt.get(dt.HOUR_OF_DAY),2)).append(formatNumber(dt.get(dt.MINUTE),2)).append(formatNumber(dt.get(dt.SECOND),2));
      //s.append(' ').append(ids[0]);

      return s.toString();
   }
   
}

