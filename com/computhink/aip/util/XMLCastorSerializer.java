/*
 * XMLCastorSerializer.java
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.computhink.aip.util;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;

/**
 * Serializes any object into and from XML using Castor library.
 * Follows Singleton Design Pattern.
 *
 * All exceptions are handled.
 * 
 */
public class XMLCastorSerializer implements XMLSerializer {
    private static XMLCastorSerializer instance = new XMLCastorSerializer(); //singleton instance

    private XMLCastorSerializer()
    {
        
    }
    
    public static XMLCastorSerializer getInstance()
    {
        return instance;
    }

    /**
     * Loads an object from an XML file that was saved by this class
     * @param myClass object class
     * @param file XML file location
     * @return loaded object
     * @throws XMLSerializationException if serialization exception encountered
     * @throws FileNotFoundException if file location is invalid
     */
    public Object loadFromXML(Class myClass, File file)
                    throws FileNotFoundException, XMLSerializationException {
        Object object = null;
        try {
            Reader reader = new FileReader(file);

            // Marshal the person object
            Unmarshaller unmarshaller = new Unmarshaller(myClass);
            // Unmarshal the person object
            object = unmarshaller.unmarshal(reader);
            // close reader
            reader.close();

        } catch (MarshalException e) {
            XMLSerializationException newException = new XMLSerializationException(e);
            //logger.log(Level.SEVERE, "Exception", newException);
            //newException.printStackTrace();
            throw newException;
        }
        catch (ValidationException e) {
            XMLSerializationException newException = new XMLSerializationException(e);
            //logger.log(Level.SEVERE, "Exception", newException);
            //newException.printStackTrace();
            throw newException;
        }
        catch (FileNotFoundException e) {
            throw e;
        }
        catch (IOException e) {
            //logger.log(Level.SEVERE, "Encountered IOException", e);
            //e.printStackTrace();
            //no need to throw this exception as it is only generated if
            //reader was not closed successfully.
            //throw new XMLSerializationException(e);
        }
        return object;
    }

    /**
     * Saves an object into an empty XML file
     * @param object object to save
     * @param file XML file location
     * @throws XMLSerializationException if serialization exception encountered
     * @throws FileNotFoundException if file location is invalid
     */
    public void saveToXML(Object object, File file)
                throws FileNotFoundException, XMLSerializationException {
        try {
        Writer writer = new FileWriter(file);
        // Marshal the person object
        Marshaller.marshal(object, writer);
        writer.close();
        } catch (MarshalException e) {
            //logger.log(Level.SEVERE, "Encountered MarshalException", e);
            //e.printStackTrace();
            throw new XMLSerializationException(e);
        }
        catch (ValidationException e) {
            //logger.log(Level.SEVERE, "Encountered ValidationException", e);
            //e.printStackTrace();
            throw new XMLSerializationException(e);
        }
        catch (FileNotFoundException e) {
            throw e;
        }
        catch (IOException e) {
            //logger.log(Level.SEVERE, "Encountered IOException", e);
            //e.printStackTrace();
            //no need to throw this exception as it is only generated if
            //writer was not closed successfully.
            //throw new XMLSerializationException(e);
        }
    }

}
