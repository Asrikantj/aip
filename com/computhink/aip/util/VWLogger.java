/*
 * IDWLogger.java
 *
 * Created on October 22, 2002, 6:48 AM
 */

/**
 *
 * @author Ayman
 */

package com.computhink.aip.util;

import java.util.logging.*;
import java.util.logging.Formatter;
import java.io.RandomAccessFile;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.File;
import java.util.*;
import java.text.SimpleDateFormat;
import com.computhink.aip.AIPPreferences;


public class VWLogger {
    
   // private static  Logger logger;
   // private static  FileHandler fileHandler;
    private static  int nThreshould = -1;
    private static int nFileCount =1;
    private static int nFileSize;
    private static String sLogPath;
    private static boolean enableLog =true;
    private static RandomAccessFile logFile;
    private static boolean bAddClassMethod =false;
    private static  VWFormatter Formatter;
    private static HashMap levelMap;
    private static int Level_ERROR =0;
    private static int Level_WARNING =1;
    private static int Level_AUDIT =2;
    private static int Level_INFO =3;
    private static int Level_DEBUG =4;
    private static int Level_VERBUS_DEBUG =5;
    
   
    public VWLogger(String sFileName, int nLevel, int nFileCount, int nFileSize) {
                
        nThreshould = nLevel;
        this.nFileCount =nFileCount;
        this.nFileSize = nFileSize;
        this.sLogPath =sFileName;
        enableLog =true;
       // logger = Logger.getLogger("VWLogger");     
        Formatter = new VWFormatter();
        vwLevelMaps();
        int lastIndx =sFileName.lastIndexOf(File.separatorChar);
        if(lastIndx > -1) 
        {    
            String path=sFileName.substring(0,lastIndx);
            File file=new File(path);
            try
            {
            file.mkdirs();
            }
            catch(SecurityException se)
            {
            System.err.println(se.getMessage());
            }
        }
      /*  try
        {
          logFile = new RandomAccessFile(sLogPath, "rw");
        }
        catch (Exception e)
        {
            enableLog =false;
            logFile =null;
            System.err.println(e.getMessage());
        }*/
        
        /*try
        {
            if (nFileCount==1)            
               fileHandler=new FileHandler(sFileName+".log",nFileSize,nFileCount,true);            
            else
               fileHandler=new FileHandler(sFileName+"%g"+".log",nFileSize,nFileCount,true);
        }
        catch (IOException ioe)
        {
            
            System.err.println(ioe.getMessage());
        }*/
    
        /*fileHandler.setFormatter(Formatter);
        
        logger.addHandler(fileHandler);
        logger.setUseParentHandlers(false);
        logger.setLevel(Level.FINEST);*/
    } 
    public VWLogger(String sFileName, int nLevel, int nFileCount, int nFileSize
                                                  ,boolean addClassMethodPath) {
        this(sFileName,nLevel,nFileCount,nFileSize);
        bAddClassMethod =addClassMethodPath;
    }
    
    public static void stopLogger()
    {
        enableLog =false;
        try{
         if(logFile !=null)
         {    
            logFile.close();
            logFile =null;
         }   
        }
        catch(Exception e){}
    }
    public static void setLevel(int level)
    {
        if(nThreshould == 10 && level != 10)
            logFile =null;
        nThreshould =level;
        if(level ==10)
        {
          try{
            if(logFile !=null)                
                logFile.close();
            
               
            }
            catch(Exception e){}            
        }    
    }    
    public static void setAddClassMethodPath(boolean add)
    {
        bAddClassMethod =add;
    }    
    public static void error(String sMsg,Exception e)
    {   
        if(!enableLog || nThreshould < Level_ERROR|| nThreshould == 10) return;
        LogRecord IDWRecord=new LogRecord(Level.SEVERE,sMsg);
        IDWRecord.setThrown(e);
        findWhoWhere(IDWRecord);        
        log(IDWRecord); 
         
    } 
    public static void warning(String sMsg)
    {
        if(!enableLog ||nThreshould < Level_WARNING || nThreshould == 10) return;
        LogRecord IDWRecord=new LogRecord(Level.WARNING,sMsg);        
        findWhoWhere(IDWRecord);        
        log(IDWRecord); 
         
    } 
    public static void audit(String sMsg)
    {
        if(!enableLog || nThreshould < Level_AUDIT) return;
        LogRecord IDWRecord=new LogRecord(Level.CONFIG,sMsg);        
        findWhoWhere(IDWRecord);        
        log(IDWRecord); 
         
    }  
    public static void info(String sMsg)
    {
        if(!enableLog || nThreshould < Level_INFO || nThreshould == 10) return;
        LogRecord IDWRecord=new LogRecord(Level.INFO,sMsg);        
        findWhoWhere(IDWRecord);        
        log(IDWRecord); 
         
    }  
    public static void debug(String sMsg)
    {
        if(!enableLog || (nThreshould < Level_DEBUG || nThreshould == 10)) return;
        LogRecord IDWRecord=new LogRecord(Level.FINE,sMsg);        
        findWhoWhere(IDWRecord);        
        log(IDWRecord); 
         
    }    
    public static void ver_debug(String sMsg)
    {
        if(!enableLog || (nThreshould < Level_VERBUS_DEBUG || nThreshould == 10)) return;
        LogRecord IDWRecord=new LogRecord(Level.FINER,sMsg);        
        findWhoWhere(IDWRecord);         
        log(IDWRecord);         
         
    }    
    private static boolean log(java.util.logging.LogRecord logRecord)
    {
        if(!enableLog )return false;
        try
        {
            if (logFile == null || nThreshould == 10)
            
                logFile = new RandomAccessFile(sLogPath, "rw");
            	nFileSize = AIPPreferences.getLogFileLength();
            	nFileSize = nFileSize - 1024;
            if (logFile.length() > nFileSize)
            {
                logFile.close();
                File oldFile = new File(sLogPath);
                File newFile = new File(sLogPath + "." + DayTime.rawDate());
                oldFile.renameTo(newFile);
                logFile = new RandomAccessFile(sLogPath, "rw");
                // Issue 562
                int filecount = AIPPreferences.getLogFileCount();
                AIPUtil.maintainLogFiles(sLogPath,filecount);
                // Issue 562
            }

            logFile.seek(logFile.length());
            String line =Formatter.format(logRecord);
            logFile.write(line.getBytes());
/*            for (int i = 0; i < line.length(); i++)
                logFile.write(line.charAt(i));
*/            /* added on 2003-11-11 */
            if (nThreshould == 10)
                logFile.close();
            /**/
            return true;
        }
        catch (FileNotFoundException fnfe)
        {
            System.err.println("Error: File Not Found ->"+fnfe.getMessage());
            fnfe.printStackTrace();
            return false;
        }
        catch(IOException ioe)
        {
            
            System.err.println("Error: Reading Byte  ->"+ioe.getMessage());
            ioe.printStackTrace();
            return false;
        }
        catch(Exception e)
        {
            System.err.println("Error: Exception ->"+e.getMessage());
            e.printStackTrace();
            return false;
        }
    
        
    }    
    private static void findWhoWhere(LogRecord record)
    {    
       if(!bAddClassMethod) return ; 
          if (record.getSourceClassName() == null ||
                        record.getSourceMethodName() == null)
          {
                  StackTraceElement stElements[] = new Throwable().getStackTrace();        
                  if(stElements.length > 0)
                  {
                     String line = stElements[2].toString();   
                     int  startIndex = line.indexOf(" ")+1;
                     int  endIndex = line.indexOf("(", startIndex);
                     if (startIndex == -1 || endIndex == -1)
                     {
                                // we choked
                                System.out.println(line);
                                return;
                     }
                     String classAndMethod = line.substring(startIndex, endIndex);
                     int index = classAndMethod.lastIndexOf('.');
                    if (index == -1) return;
                        record.setSourceClassName(classAndMethod.substring(0, index));
                        record.setSourceMethodName(classAndMethod.substring(index+1));
                 }    
            
         }
        
    }
    private static void vwLevelMaps()
    {
        levelMap =new HashMap() ;
        levelMap.put(Level.SEVERE,"Exception:");
        levelMap.put(Level.WARNING,"Warning:"); 
        levelMap.put(Level.CONFIG,""); 
        levelMap.put(Level.INFO,"Info:");
        levelMap.put(Level.FINE,"Debug:");
        levelMap.put(Level.FINER,"Ver_debug:");
    }   
    
    public class VWFormatter extends Formatter
    {
        SimpleDateFormat dateformatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS",new Locale("en","US"));
    
        public VWFormatter() {
            super();
        }    
        public String format(java.util.logging.LogRecord logRecord) {
            
            String sDate=dateformatter.format(new Date());
            /*GregorianCalendar date=new GregorianCalendar();
            String sDate=date.get(Calendar.YEAR)+"-"+date.get(Calendar.MONTH)+"-"+date.get(Calendar.DAY_OF_MONTH)+"  "+
                  date.get(Calendar.HOUR_OF_DAY)+":"+date.get(Calendar.MINUTE)+":"+date.get(Calendar.SECOND)+":"+date.get(Calendar.MILLISECOND);*/
            String level =(String)levelMap.get(logRecord.getLevel());
            /*if (logRecord.getLevel()==Level.SEVERE)
            {
                level="EXCEPTION";
            }
            else
            {   
                level="";
            }
            */
            String log = "\r\n"+sDate+" ";
            if(bAddClassMethod)
                log +=logRecord.getSourceClassName()+"<"+logRecord.getSourceMethodName()+"> ";
            log +=level+" "+logRecord.getMessage();
            if(logRecord.getThrown() !=null)
            {                   
                log +="\r\n"+logRecord.getThrown().toString();
                StackTraceElement[] stElements =logRecord.getThrown().getStackTrace();              
                for(int i=0;i<stElements.length;i++)
                {
                    log +="\r\n"+stElements[i];
                }    
            } 
            //System.out.println("Log:"+log);
            return log; 
        }
    }    
}//end class IDWLogger


