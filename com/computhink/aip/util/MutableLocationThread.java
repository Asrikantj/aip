/*
 * MutableLocationThread.java
 *
 * Created on August 2, 2004, 11:44 AM
 */

package com.computhink.aip.util;

/**
 *
 * @author  Administrator
 */
public abstract class MutableLocationThread extends Thread
{
        protected LocationStatus locStatus;
        public MutableLocationThread(LocationStatus locStatus)
        {
            this.locStatus =locStatus;            
        }    
        public abstract void run();
        public synchronized void waitUntilAsk()
        {
            
                try{                      
                    this.wait();
                }
                catch (InterruptedException e) {
                    e.printStackTrace();  
                }
                
        }
        public synchronized void resumeProcess()
        {
           if(locStatus.getStatus() ==locStatus.WAIT)
           {
                this.notify();
           }     
        }    
        public synchronized void commonNotify()
        {
           
                this.notify();
                
        }
        public synchronized void waitForMin(int min)
        {   
                try{          
                    if(min >0)
                        this.wait(min*60*1000);
                }
                catch (InterruptedException e) {
                    e.printStackTrace();  
                }
                
        }
    
}
