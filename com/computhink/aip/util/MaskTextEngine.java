package com.computhink.aip.util;

import java.awt.event.KeyEvent;
import java.awt.*;
import java.text.StringCharacterIterator;

/**
 * Engine class for support of masked input in IRAD.
 *
 * <P>To use a reserved characters as a literal character, prefix the
 * character with the Escape character ('\'), including  spaces.  For
 * instance, to specify the mask for an 800 telephone number as:
 * 1 8\0\0-AAA-AAAA.  This example shows that the "(" ")" and "-"
 * characters will be displayed literally and not as a special mask
 * character. The following are valid characters that can be used for
 * a mask:
 *
 * <PRE>
 * To restrict the digit position to:                          Use:
 * --------------------------------------------------------    ----
 * Escape (characters are displayed as literals)               \
 * Digit                                                       0
 * Digit or Space                                              9
 * Digit, Sign (+ or -), or Space                              #
 * Letter (A through Z) entry required                         L
 * Letter (A through Z) entry optional                         ?
 * Letter or Digit - entry required                            A
 * Letter or Digit - entry optional                            a
 * Any character or space - entry required                     &
 * Any character or space - entry optional                     C
 * Decimal placeholder and Thousands separator                 . ,
 * Date and Time separators                                    :;-/
 * Forces all characters that follow to convert to lowercase   &lt;
 * Forces all characters that follow to convert to uppercase   &gt;
 * </PRE>
 *
 * @author Paul Lancaster
 */
public class MaskTextEngine
{

    // These constants are used to specify the data type information needed by
    // the engine to specialize mask behaviour.
  /** A Constant that indicates the component string value of is data type "text".
  <p>
  The <a href="#texttype"><code>texttype</code></a>,
  <a href="#numbtype"><code>numbtype</code></a>,
  <a href="#datetype"><code>datetype</code></a>, and
  <a href="#timetype"><code>timetype</code></a>
  constants are used to specify the data type information needed
  to specialize input-time behavior based on data type.
  <p>
  For now, the only such difference is between text and numeric:
  in numeric fields, an empty field is initialzed to "0", and input of
  a decimal point when the mask contains one and the caret is to its left
  causes the caret to move to the right of the decimal point position.
  <br>
  All other types currently behave like the text type.
  @see #numbtype
  @see #datetype
  @see #timetype
  @see #getDatatype
  @see #setDatatype
  */
    public static final int texttype = 0;
  /** A Constant that indicates the component string value of is data type "number".
  <p>
  The <a href="#texttype"><code>texttype</code></a>,
  <a href="#numbtype"><code>numbtype</code></a>,
  <a href="#datetype"><code>datetype</code></a>, and
  <a href="#timetype"><code>timetype</code></a>
  constants are used to specify the data type information needed
  to specialize input-time behavior based on data type.
  <p>
  For now, the only such difference is between text and numeric:
  in numeric fields, an empty field is initialzed to "0", and input of
  a decimal point when the mask contains one and the caret is to its left
  causes the caret to move to the right of the decimal point position.
  <br>
  All other types currently behave like the text type.
  @see #texttype
  @see #datetype
  @see #timetype
  @see #getDatatype
  @see #setDatatype
  */
    public static final int numbtype = 1;
  /** A Constant that indicates the component string value of is data type "date".
  <p>
  The <a href="#texttype"><code>texttype</code></a>,
  <a href="#numbtype"><code>numbtype</code></a>,
  <a href="#datetype"><code>datetype</code></a>, and
  <a href="#timetype"><code>timetype</code></a>
  constants are used to specify the data type information needed
  to specialize input-time behavior based on data type.
  <p>
  For now, the only such difference is between text and numeric:
  in numeric fields, an empty field is initialzed to "0", and input of
  a decimal point when the mask contains one and the caret is to its left
  causes the caret to move to the right of the decimal point position.
  <br>
  All other types currently behave like the text type.
  @see #texttype
  @see #numbtype
  @see #timetype
  @see #getDatatype
  @see #setDatatype
  */
    public static final int datetype = 2;
  /** A Constant that indicates the component string value of is data type "time".
  <p>
  The <a href="#texttype"><code>texttype</code></a>,
  <a href="#numbtype"><code>numbtype</code></a>,
  <a href="#datetype"><code>datetype</code></a>, and
  <a href="#timetype"><code>timetype</code></a>
  constants are used to specify the data type information needed
  to specialize input-time behavior based on data type.
  <p>
  For now, the only such difference is between text and numeric:
  in numeric fields, an empty field is initialzed to "0", and input of
  a decimal point when the mask contains one and the caret is to its left
  causes the caret to move to the right of the decimal point position.
  <br>
  All other types currently behave like the text type.
  @see #texttype
  @see #numbtype
  @see #datetype
  @see #getDatatype
  @see #setDatatype
  */
    public static final int timetype = 3;

  /** Constructs a <code>MaskTextEngine</code> object with the given
  mask and of the indicated data type.
  @param mask the mask
  @param datatype the data type
  @see #setMask
  @see #setDatatype
  */
    public MaskTextEngine(String mask, int datatype) {  // ctor
        init();
        setMask(mask);
        setDatatype(datatype);
    }

  /** Constructs a default <code>MaskTextEngine</code> object.
  It has no mask, and is of data type <a href="#texttype"><code>texttype</code></a>.
  */
    public MaskTextEngine() {  // default constructor
        init();
    }

  /** Sets the formatting mask for this component to use.
  <p>
  The following table defines the special mask characters.
  <p>
  If you want to be always displayed, instead of being interpreted as a
  special mask character, preface it with an escape ('\').
  This includes spaces. For instance, the mask for a long distance
  telephone number could be: \(999\)\ 999\-9999.

     <table>
     <tr><th>Use</th>               <th>To restrict the digit position to</th></tr>
     <tr><td><code>\</code></td>    <td>Escape (characters are displayed as literals)</td></tr>
     <tr><td><code>0</code></td>    <td>Digit</td></tr>
     <tr><td><code>9</code></td>    <td>Digit or Space</td></tr>
     <tr><td><code>#</code></td>    <td>Digit, Sign (+ or -), or Space</td></tr>
     <tr><td><code>L</code></td>    <td>Letter (A through Z) entry required</td></tr>
     <tr><td><code>?</code></td>    <td>Letter (A through Z) entry optional</td></tr>
     <tr><td><code>A</code></td>    <td>Letter or Digit - entry required</td></tr>
     <tr><td><code>a</code></td>    <td>Letter or Digit - entry optional</td></tr>
     <tr><td><code>&amp;</code></td>    <td>Any character or space - entry required</td></tr>
     <tr><td><code>C</code></td>    <td>Any character or space - entry optional</td></tr>
     <tr><td><code>. ,</code></td>  <td>Decimal placeholder and Thousands separator</td></tr>
     <tr><td><code>:;-/</code></td> <td>Date and Time separators</td></tr>
     <tr><td><code>&lt;</code></td> <td>Forces all characters that follow to convert to lowercase</td></tr>
     <tr><td><code>&gt;</code></td> <td>Forces all characters that follow to convert to uppercase</td></tr>
	 </table>

  @param mask the new mask
  @see #getMask
  */
    public void setMask(String mask) {
        //vasu start
        if(mask.trim().equals("")) return;
        //vasu end

        _mask = mask;
        _maskLength = mask.length();

        // Add Literial slash --Ayman
        _literalSlashs =new boolean[_maskLength];
        for (int i=0;i < _maskLength;i++){

        if((i+2) <=_maskLength ){
          if (_mask.charAt(i)=='\\' && _mask.charAt(i+1)=='\\'){
            _literalSlashs[i]=false;
            _literalSlashs[i+1]=true;
             //if((i+2) ==_maskLength ) break;
             i++;
          }else _literalSlashs[i]=false;
        } else _literalSlashs[i]=false;

        }
        // Analyze filter and initialize supporting data structures
        _filterCount = 0;
        resetMaskScan();

        // First pass: count filters and total displayable mask characters.
        // Also mark first and last filter positions.
        for (_maskCount = 0; 0 != getNextMaskChar(); _maskCount++) {
            if (_curMaskType == filtertype) {
                _filterCount++;                // count filters
                _lastFilterPos = _curMaskPos;  // new "last" filter pos
            }
            if (1 == _filterCount)
                _firstFilterPos = _curMaskPos; // note first filter pos
        }
        if (_maskCount * _filterCount == 0)
            return;  // error: no displayable characters or no filters in mask

        // Second pass: fill filter position array.
        _filterPositions = new int[_filterCount];
        _commandCorrections = new int[_filterCount];
        resetMaskScan();
        for (int i = 0, j = 0; i < _maskCount; i++) {
            getNextMaskChar();
            if (_curMaskType == filtertype) {
                _filterPositions[j] = _curMaskPos;
                _commandCorrections[j++] = _commandCorrection;
            }
        }
    }

  /** Sets the data type the component's string value represents.
  Knowledge of the data type is used to specialize input-time behavior.
  <p>
  Possible values are:<ul>
  <li><a href="#texttype"><code>texttype</code></a> - text
  <li><a href="#numbtype"><code>numbtype</code></a> - number
  <li><a href="#datetype"><code>datetype</code></a> - date
  <li><a href="#timetype"><code>timetype</code></a> - time
  </ul>
  <p>
  For now, the only difference is between text and numeric:
  in numeric fields, an empty field is initialzed to "0", and input of
  a decimal point when the mask contains one and the caret is to its left
  causes the caret to move to the right of the decimal point position.
  <br>
  All other types currently behave like the text type.
  @see #getDatatype
  */
    public void   setDatatype(int datatype) { _datatype = datatype; }

  /** Gets the formatting mask this component uses.
  <p>
  See <a href="#setMask"><code>setMask</code></a>
  method for mask format details.
  @return the mask
  @see #setMask
  */
    public String getMask()                 { return _mask        ; }
  /** Gets the data type the component's string value represents.
  Knowledge of the data type is used to specialize input-time behavior.
  <p>
  Possible values are:<ul>
  <li><a href="#texttype"><code>texttype</code></a> - text
  <li><a href="#numbtype"><code>numbtype</code></a> - number
  <li><a href="#datetype"><code>datetype</code></a> - date
  <li><a href="#timetype"><code>timetype</code></a> - time
  </ul>
  <p>
  For now, the only difference is between text and numeric:
  in numeric fields, an empty field is initialzed to "0", and input of
  a decimal point when the mask contains one and the caret is to its left
  causes the caret to move to the right of the decimal point position.
  <br>
  All other types currently behave like the text type.
  @return the current data type
  @see #setDatatype
  */
    public int    getDatatype()             { return _datatype    ; }

    public boolean isSpaceIgnored ()
    {
        return _spaceIgnored;
    }

    public void setSpaceIgnored (boolean spaceIgnored)
    {
        _spaceIgnored = spaceIgnored;
    }

    public boolean isIncludeLiterals ()
    {
        return _includeLiterals;
    }

    public void setIncludeLiterals (boolean includeLiterals)
    {
        _includeLiterals = includeLiterals;
    }

    /*  Called to initialize the display of the masked data.
        The first parameter is the current data in the field.
        The second parameter holds the string that should be displayed.
        The return value is the initial caret position.
    */
    public int initDisplay(String data, StringBuffer newData) {
        //vasu start
      //update --Ayman
       /* if (_filterCount == 0){       // no filters?
            newData.append(data);
            return 0;                  // return first position
        }*/
        //vasu end
        int datalen = data.length();
        int inpos = 0;   // track input position
        boolean zerofill           = false;
        boolean dataRightOfDecimal = false;
        boolean maskRightOfDecimal = false;
        resetMaskScan();
        char c;
        for (int i = 0; (c = getNextMaskChar()) != 0; i++) { // mask drives output
            if (_curMaskType == filtertype) {         // we are on a filter?
             //   char z = zerofill ? '0' : '_';
                  char z = zerofill ? '0' : ' '; //--Ayman
                if (inpos < datalen) {       // consumed all data yet?
                    c = data.charAt(inpos++);  // if not, get next data char
                    if (numbtype == _datatype) { // special handling for numerics
                        if (_decimalPoint == c) {
                            c = z;
                            dataRightOfDecimal = true;
                            if (maskRightOfDecimal && inpos < datalen)
                                c = data.charAt(inpos++);
                        } else if (dataRightOfDecimal && !maskRightOfDecimal) {
                            c = z;
                            inpos--;  // can not use this fractional digit yet
                        } else if (!dataRightOfDecimal && maskRightOfDecimal) {
                            c = z;
                        while (inpos < datalen)    // scan data for dec. pt.
                            if (_decimalPoint == data.charAt(inpos++)) {
                                if (inpos < datalen)
                                    c = data.charAt(inpos++);
                                dataRightOfDecimal = true;
                                break;
                            }
                        }
                    }  // if numeric field
                } else            // we have consumed all input data
                c = z;
            } else if (_curMaskChar == _decimalPoint && numbtype == _datatype) {
                maskRightOfDecimal = true;
                if (0 == datalen) {  // for empty numeric fields, start zero filling
                    zerofill = true;
                    newData.setCharAt(newData.length() - 1, '0'); // zero units digit
                }
                else if (inpos < datalen && data.charAt(inpos) == _decimalPoint) {
                    inpos++;   // skip decimal point in input
                    dataRightOfDecimal = true;
                }
            }
            newData.append(c);
        }

        // Zero units digit of empty numeric fields with no decimal.
        if (numbtype == _datatype && 0 == datalen && !zerofill && _maskLength > 0)
            newData.setCharAt(newData.length() - 1, '0');
         if (_filterCount == 0){       // no filters?
           // newData.append(data);

            return 0;                  // return first position
        }
        return _filterPositions[0] - _commandCorrections[0];
    }

    /**
     * Called for every key stroke corresponding to displayable characters
     * once editing begins. This is the main workhorse method.
     *
     * @param e        User keystroke event object
     * @param pos      Current cursor position (zero based)
     * @param data     Current text from the component
     * @param newData  What should be displayed in the component
     *                 (modified by this method)
     * @param selStart Selection start
     * @param selEnd   Selection end
     * @return         <UL>
     *                 <LI>If &gt;= 0: new cursor position within
     *                 <CODE>newData</CODE>
     *                 <LI>If == -1: the input is inconsistent (position not in
     *                 range or not on a filter)
     *                 <LI>If == -2: the input keystroke is not accepted by the
     *                 relevant filter
     *                 <LI>If &lt;= -3: cursor out of range, carret should be
     *                 set at position (x + 3)*(-1)
     *                 </UL>
     */
  /** This is the main workhorse method.
  It's called for every key stroke corresponding to displayable
  characters once editing begins.
  @param e the user keystroke event object
  @param pos the current cursor position (0-based)
  @param data the current text from the component
  @param newData output and is what should be displayed in the component
  @param selStart the selection start (0-based, inclusive)
  @param selEnd the selection end (0-based, non-inclusive)
  @return the new cursor position within the "newData"
        parameter (0-based), unless it is negative, in which case
        -1 means the input is inconsistent
  */
    public int processKey(java.awt.event.KeyEvent e, int pos, String data,
                          StringBuffer newData, int selStart, int selEnd)
    {

        newData.append(data);    // init output to input
        int keyCode = e.getKeyCode();
        if (!setMaskPos(pos) && !isNavKey(keyCode))
            return -1;   // cursor pos not in bounds
        char key = e.getKeyChar();
        if (key == _decimalPoint && _datatype == numbtype) {   // decimal?

        // Move caret to first filter to the right of first decimal point
        // that is to the right of current caret position.
            while (getNextMaskChar() != 0)
                if (_curMaskChar == _decimalPoint)
                    return nextFilterPos();
            return pos;   // could not find rightward decimal point
        } else if (filtertype != _curMaskType) {   // not on filter position?
            if (0 == pos && 0 == selStart && selEnd > _firstFilterPos)
                setMaskPos(pos = _firstFilterPos);
            else if (!isNavKey(keyCode)) {
                if (pos > _lastFilterPos)
                    return -1;      // beyond last filter
                nextFilterPos();  // advance to next filter
                pos = _curMaskPos - _commandCorrection;
            }
        //  System.out.println(pos+":"+_lastFilterPos+":"+keyCode);
        }
        switch (keyCode) {   // handle control keys
        case KeyEvent.VK_BACK_SPACE:
            if (selStart < selEnd) {  // if selected text exists
                if (pos != selStart || selEnd - selStart > 1) {
                clearSelectedText(selStart, selEnd, newData);
                return Math.max(selStart, _firstFilterPos);
                }
            }
            int newPos = prevFilterPos();
            if (pos > 0 && newPos != -1)
                newData.setCharAt(newPos, ' ');
            return newPos != -1 ? newPos : pos;
        case KeyEvent.VK_DELETE:
            if (selStart < selEnd) {   // if selected text exists
                clearSelectedText(selStart, selEnd, newData);
                return Math.max(selStart, _firstFilterPos);
            }
        newData.setCharAt(pos, ' ');
            return pos;
        case KeyEvent.VK_LEFT:

            return prevFilterPos();
        case KeyEvent.VK_RIGHT:

            return nextFilterPos();
        case KeyEvent.VK_HOME:
            return _filterPositions[0] - _commandCorrections[0];
        case KeyEvent.VK_END:
            return _filterPositions[_filterCount-1] - _commandCorrections[_filterCount-1];

        }
        if (matchFilter(key)) {
            clearSelectedText(selStart, selEnd, newData);
            if      (eShiftLower == _shiftState)
                key = Character.toLowerCase(key);
            else if (eShiftUpper == _shiftState)
                key = Character.toUpperCase(key);
            newData.setCharAt(pos, key);
            int newpos = nextFilterPos();;  // match: advance to next filter
            return newpos == -1 ? (lastFilterPosition () + 1)*(-1) - 3 : newpos;
        } else              // no match: return with no changes
            return -2;
    }

    /**
     * Strips the mask from a data string and checks if the data is
     * complete according to the mask.
     *
     * @param data    The analyzed string
     * @param newData Content of data without the mask characters,
     *                this is an out parameter
     * @return        Returns true if the data is complete according to
     *                the mask, false otherwise (data not complete or does
     *                not match the mask)
     */
  /** Strips the mask from the given text and returns the result in <code>newData</code>.
  @param data the text with mask characters in it
  @param newData the resulting text without any mask characters in it
  */
    public boolean stripMask(String data, StringBuffer newData) {
        if (_maskLength == 0){
            newData.append(data);
            return true;
        }
        resetMaskScan();
        boolean retval = true;
        for (int i = 0; getNextMaskChar() != 0; i++) {
            if (i > data.length () - 1) { // Oups, data is not long enough.
                retval = false;
                break;
            }
            char c = data.charAt(i);
            if (_curMaskType == filtertype) { // We are on a filter.
                if (c != '_') { // We have a character.
                    if (!_spaceIgnored || !(c == ' ')) {
                        // Don't add space if we ignore spaces.
                        newData.append(c);
                    }
                } else if (isMandatory()) { // Empty filter position.
                    retval = false; // Missing mandatory data.
                }
            } else if (_curMaskChar == _decimalPoint &&
                       _datatype == numbtype) {
                // Always include decimal point if data type is decimal.
                newData.append(_decimalPoint);
            } else if (_curMaskType == literaltype && _includeLiterals) {
                // Include literals when asked to do so.
                newData.append (c);
            }
        }
        return retval;
    }

    // Return true iff the engine handles the given key stroke.
  /** Handles a key event.
  @param e the key event to handle
  @return <code>true</code> if and only if the key event was handled
  */
    public boolean isHandledKey(java.awt.event.KeyEvent e) {
        if (0 == _maskLength)  // if no mask, let component handle all keys
            return false;
        char c = e.getKeyChar();
        if (Character.isISOControl(c)) {
            int k = e.getKeyCode();
            switch (k) {
                case KeyEvent.VK_LEFT:
                case KeyEvent.VK_RIGHT:
                case KeyEvent.VK_HOME:
                case KeyEvent.VK_END:
                case KeyEvent.VK_KP_LEFT:
                case KeyEvent.VK_KP_RIGHT:
                return !e.isShiftDown();
            }
            switch (c) {
                case KeyEvent.VK_BACK_SPACE:
                case KeyEvent.VK_DELETE:
                return true;
                default:
                return false;
            }
        }
        return true;
    }

  /**
  Cuts the indicated text range from the text in the given string buffer.
  After the operation the <code>newData</code> string buffer contains the text it
  originally had, minus the text within the selected range.
  @param newData the text to operate on
  @param selStart the selection start (0-based, inclusive)
  @param selEnd the selection end (0-based, non-inclusive)
  */
    public String cut(String data, int selStart, int selEnd, StringBuffer newData) {
        newData.append(data);
        setMaskPos(selStart);
        clearSelectedText(selStart, selEnd, newData);
        return data.substring(selStart, selEnd);
    }

  /**
  Removes the selected text in the given <code>data</code> <code>String</code>,
  then inserts the <code>pasteData</code> text at the specified position.
  The result in returnd in <code>newData</code>.
  @param data the text to operate on
  @param pasteData the new text to add
  @param pos the position to insert the new text (0-based)
  @param newData the resultant text
  @param selStart the selection start (0-based, inclusive)
  @param selEnd the selection end (0-based, non-inclusive)
  @return the <code>pos</code> location parameter, adjusted to compensate for the operation
  */
    public int paste(String data, String pasteData, int pos, StringBuffer newData, int selStart, int selEnd) {

        boolean hasSelection = selStart < selEnd;
        if (hasSelection)
            pos = selStart;
        if (!setMaskPos(pos))
            return -1;
        StringBuffer sb = new StringBuffer(data);
        int len = pasteData.length();
        KeyEvent e = new KeyEvent(new TextField(), 0, 0, 0, 0,' ');
        String s = new String(sb.toString());
        int newpos = pos;
        for (int i = 0; i < len; i++) {
            char c = pasteData.charAt(i);
            e.setKeyChar(c);
            e.setKeyCode(c);
            sb = new StringBuffer();
            int selstart = 0, selend = 0;
            if (hasSelection && newpos >= selStart && newpos < selEnd) {
                if (i == 0) {
                    selstart = selStart;
                    selend = selEnd;
                }
                else {
                    selstart = newpos;
                    selend = newpos + 1;
                }
            }
            newpos = processKey(e, newpos, s, sb, selstart, selend);
            if (newpos == -1 || newpos == -2) {
                break;  // next paste character was rejected by mask
            }
            s = sb.toString();
        }
        newData.append(s);
        return newpos;
    }

    //fix for 68889
    public String getMatchedText ( String t ) {
        if ( debug ) {

        }
        //Initailize the return stringbuffer
        StringBuffer retString = new StringBuffer ( );

        if ( t != null && t.length() > 0 ) {//there is some text

            //if mask is "", then do not do anything
            String mask = getMask() ;
            if ( mask.length() > 0 ) {//there is some mask
                int textCharCounter = 0;
                java.text.StringCharacterIterator maskIterator = new java.text.StringCharacterIterator( mask );

                //iterate for each mask character - also means that if the text provided
                //is more the extra char will be neglected
                //used do while iteration so that the substatement is executed
                //atleast once
                do   {

                    char textChar = ' ';
                    char maskChar = maskIterator.current();
                    int  MCharPos = maskIterator.getIndex();
                    if ( textCharCounter < t.length () ) {//sufficient text
                        //we are still within the text
                        textChar = t.charAt ( textCharCounter ) ;
                        if (     isFilter  ( maskChar ) ) { //if the mask char is a filter

                            //    if it matches with the filter, type accept it
                            if ( matchFilter ( textChar , maskChar ) ) {
                                retString.append ( textChar );
                            }
                            else {
                                //    otherwise use '_'
                                retString.append ( ' ' );

                            }

                        }
                        else if (isCommand ( maskChar ) ) {//if the mask char is a command
                            //handle case conversion and escape command
                            //in all these cases, the current mask is just a command
                            //that is applied to the next mask filter. so get it.
                            maskIterator.next();
                            if ( maskChar == '>' ) {//command : convert to upper case
                                //    if it matches with the filter, type accept it
                                if ( matchFilter ( textChar , maskIterator.current()) ) {
                                    retString.append ( Character.toUpperCase( textChar ));
                                }
                                else {
                                    //    otherwise use '_'
                                    retString.append ( ' ' );
                                }
                            }
                            else if ( maskChar == '<' ){//command : convert to lower case
                                //    if it matches with the filter, type accept it
                                if ( matchFilter ( textChar , maskIterator.current()) ) {
                                    retString.append ( Character.toLowerCase( textChar ));
                                }
                                else {
                                    //    otherwise use '_'
                                    retString.append ( ' ' );
                                }

                            }
                            else {//escape character - so append mask char as a literal
                               if (_literalSlashs[MCharPos]) retString.append ( maskChar );//update --Ayman
                                else retString.append ( maskIterator.current() );
                            }

                        }
                        else {//if the mask char is a literal
                            //otherwise neglect the text char. append the masks char
                            //because it is a mask literal
                            retString.append ( maskChar );
                        }
                    }
                    else {//insufficient text - text length < mask length
                        //    otherwise use '_'
                        retString.append ( ' ' );

                    }

                    textCharCounter ++;//increment text counter

                } while ( maskIterator.next() != maskIterator.DONE ) ;

            }
            else {//there is no mask - append the input string as it is.
                retString.append ( t ) ;
            }
        }
        else {//there is no text or it is null
            //do nothing
        }

        if ( debug ) {
            //System.out.println ( "Output text : " + retString ) ;
        }

        return retString.toString() ;

    }


    boolean clearSelectedText(int selStart, int selEnd, StringBuffer newData) {
        if (selEnd > selStart) {  // only if selected text exists
            // save mask scan state
            int curMaskPos = _curMaskPos;
            int nextMaskPos = _nextMaskPos;
            int curMaskType = _curMaskType;
            char curMaskChar = _curMaskChar;
            char lastMaskChar = _lastMaskChar;
            char decimalPoint = _decimalPoint;
            int commandCorrection = _commandCorrection;
            int shiftState = _shiftState;

            setMaskPos(selStart);
            int outpos = selStart;   // track input position
            char c = _curMaskChar;
            while (outpos < selEnd && c != 0) {
                if (_curMaskType == filtertype)
                c = ' ';
                newData.setCharAt(outpos++, c);
                c = getNextMaskChar();
            }

            // restore mask scan state
            _curMaskPos = curMaskPos;
            _nextMaskPos = nextMaskPos;
            _curMaskType = curMaskType;
            _curMaskChar = curMaskChar;
            _lastMaskChar = lastMaskChar;
            _decimalPoint = decimalPoint;
            _commandCorrection = commandCorrection;
            _shiftState = shiftState;
            return true;
        }
        return false;
    }

    // Set new position in mask.  Return false iff it is out of bounds.
    boolean setMaskPos(int pos) {
        if (pos < 0)
            return false;
        resetMaskScan();
        _inRange = true;
        while (pos-- >= 0)
            if (getNextMaskChar() == 0)
                return _inRange = false;
        return true;
    }

    // Returns the position in the mask of the first filter that follows the
    // current position.
    int nextFilterPos() {
        while (getNextMaskChar() != 0)
           if (_curMaskType == filtertype)
                return _curMaskPos - _commandCorrection;
        return -1;
    }

  // Returns the position in the mask of the filter that precedes the
  // current position.  Return of -1 means no previous filter was found.
    int prevFilterPos() {
        int oldPos = _curMaskPos;
        resetMaskScan();
        int prevFilterPos = -1;
        int prevCommandCorrection = 0;
        while (nextFilterPos() != -1) {
            if (_curMaskPos >= oldPos) { // scanned past previous filter
                if (!_inRange)
                return _curMaskPos - _commandCorrection;
                break;
            }
            prevFilterPos = _curMaskPos;  // save pos info on this filter
            prevCommandCorrection = _commandCorrection;
        }

        return prevFilterPos - prevCommandCorrection;
    }

    // Return true iff key is accepted by current filter
    boolean matchFilter(char key) {
        return matchFilter ( key , _curMaskChar ) ;
    }

    // Return true iff key is accepted by current filter
    boolean matchFilter(char key , char maskChar ) {
        switch ( maskChar ) {
            case '0':   // any digit
                return Character.isDigit(key);
            case '9':   // any digit or space or sign
             return Character.isDigit(key) || Character.isSpaceChar(key) ||
                    key == '+' || key == '-';
            case '~':   // any digit or space or sign
             return Character.isDigit(key) || Character.isSpaceChar(key) ||
                    key == '+' || key == '-';
            case '#':   // digit, space
                return Character.isDigit(key) || Character.isSpaceChar(key);
            case 'L':   // letter
            case '?':
                return Character.isLetter(key);
            case 'A':   // letter or digit
            case 'a':
                return Character.isLetterOrDigit(key);
            case '&':   // any character
            case 'C':
                //some ignorable characters cause problems while
                //auto-repeating back-space and other keys. so filter them out.
                return ( !Character.isIdentifierIgnorable ( key ));
            default:
                return false;
        }
    }

    // Prepare mask to be scanned from the beginning.
    void resetMaskScan() {
        _curMaskPos = 0;
        _nextMaskPos = 0;   // set first char to get
        _lastMaskChar = 0;
        _commandCorrection = 0;
        _curMaskType = 0;
        _shiftState = eShiftNul;
    }

    char getNextMaskChar() {
        while (true) {        // loop through command chars
            if (_nextMaskPos >= _maskLength)
                return 0;
            _lastMaskChar = _curMaskChar;

            _curMaskChar = _mask.charAt(_nextMaskPos++);
            _curMaskPos = _nextMaskPos - 1;
            if (isCommand()) {
                _commandCorrection++;
                if ('<' == _curMaskChar)
                    _shiftState = eShiftLower;
                else if ('>' == _curMaskChar)
                    _shiftState = eShiftUpper;
                else if (_literalSlashs[_nextMaskPos-1] ){
                   _commandCorrection--;
                   break;
                }
            } else
                break;
        }
        _curMaskType = literaltype;
        if (_lastMaskChar != '\\'){       //update --AYMAN
            if (isFilter()) _curMaskType = filtertype;
        }else if (_nextMaskPos <=1){
           if ( ( _literalSlashs[_nextMaskPos-1]) && isFilter()) _curMaskType = filtertype;
        }

        _curMaskPos = _nextMaskPos - 1;

        return _curMaskChar;
    }

    // Return true iff key is a cursor navigation key.
    boolean isNavKey(int key) {
        switch (key) {
            default:
                return false;
            case java.awt.event.KeyEvent.VK_LEFT:
            case java.awt.event.KeyEvent.VK_RIGHT:
            case java.awt.event.KeyEvent.VK_BACK_SPACE:
            case java.awt.event.KeyEvent.VK_HOME:
            case java.awt.event.KeyEvent.VK_END:

        }
        return true;
    }

    /**
     * Returns the position of the last character in the mask which is
     * a filter. The cursor should not move past this character.
     *
     * @return Last character index
     */
    public int lastFilterPosition ()
    {
        final int rawLastPosition = // Last position with commands
          _filterPositions [_filterPositions.length - 1];
        int lastPosition = rawLastPosition;
        for (int characterIndex = 0; characterIndex <= rawLastPosition;
             characterIndex++) {
            if (isCommand (getMask ().charAt (characterIndex))) {
                if (!_literalSlashs[characterIndex]) //update --Ayman
                lastPosition--;
            }
        }
        return lastPosition;
    }

    boolean isFilter()    { return isFilter(_curMaskChar); }
    boolean isCommand()   { //update --Ayman
    if ( _literalSlashs[_curMaskPos]) return false;
    else return isCommand(_curMaskChar); }
    boolean isMandatory() { return isMandatory(_curMaskChar); }

    //add other utility methods
     boolean isFilter( char c )    { return -1 != "9~#aA?&C".indexOf( c ); } //update --Ayman
  //  boolean isFilter( char c )    { return -1 != "09#aAL?&C".indexOf( c ); }
    boolean isCommand( char c )   { return -1 != "\\<>"     .indexOf( c ); }
//  boolean isMandatory( char c ) { return -1 != "0LA&"     .indexOf( c ); }
      boolean isMandatory( char c ) { return false ;}



    // Called by all ctors
    void init() {}

    // Variables
    String  _mask             = ""      ; // current mask
    int     _maskLength       = 0       ; // its length
    int     _datatype         = texttype; // data type of masked field
    int     _filterCount      = 0       ; // count of filters in mask
    int     _maskCount        = 0       ; // count of displayable characters
    int     _firstFilterPos   = 0       ;
    int     _lastFilterPos    = 0       ;
    int[]   _filterPositions            ; // track all filter positions
    int[]   _commandCorrections         ; // track all command corrections
    boolean[]   _literalSlashs          ; // TRACE LITERAL SLASH   --Ayman
    boolean _inRange          = true    ; // true if last mask position request is in range
    boolean _spaceIgnored     = false   ; // true if space considered as empty character
    boolean _includeLiterals  = false   ; // true if literals included when stripping mask

    // State variables for scanning the mask
    int  _curMaskPos        = 0;          // position of current mask character
    int  _nextMaskPos       = 0;          // position of next mask character
    int  _curMaskType       = nulltype;   // type of current mask character
    char _curMaskChar       = 0;
    char _lastMaskChar      = 0;
    char _nextMaskChar      = 0;
    int  _commandCorrection = 0;
    int  _shiftState        = eShiftNul;
    char _decimalPoint      = '.';

    // Internal mask command type codes
    static final int nulltype    = 0;
    static final int filtertype  = 1;
    static final int literaltype = 2;

    // Internal shift states
    static final int eShiftNul   = 0;
    static final int eShiftUpper = 1;
    static final int eShiftLower = 2;

    // Debug flag
    private boolean          debug          = true;

}
