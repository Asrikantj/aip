/*
 * FileUpdater.java
 *
 * Created on December 8, 2003, 3:43 PM
 */

package com.computhink.aip.util;

/**
 * An interface that defines file updaters. These are programs that work on a
 * particular file by updating certain parts of it.
 * @author  amaleh
 */
public interface FileUpdater extends FileOperator
{
    /**
     * Updates a text file using a TreeMap of data. The keys in the TreeMap
     * point the locations in the file that will be updated with the values
     * in the TreeMap.
     */
    public boolean update(java.util.TreeMap map);
}
