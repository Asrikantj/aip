/*
 * util.java
 *
 * Created on March 25, 2004, 1:08 PM
 */

package com.computhink.aip.util;

/**
 *
 * @author  Ayman
 */

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.text.ParsePosition;
import java.util.*;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import com.computhink.common.Comparitor;

public class AIPUtil {
    
    private static ResourceBundle bundle = null;
    
    public static final String pathSep = String.valueOf(File.separatorChar);
    public final static String MASKDATE = "yyyyMMdd HH:mm:ss";

    public AIPUtil() {
    }
    public static String getString(String key) {
	String value = null;
	try {
	    value = getResourceBundle().getString(key);
	} catch (MissingResourceException e) {
	    //System.out.println("java.util.MissingResourceException: Couldn't find value for: " + key);

	}
	if(value == null) {
	    value = "  ";
	}
	return value;
  }
  public static ResourceBundle getResourceBundle() {
	if(bundle == null) {
	    bundle = ResourceBundle.getBundle("com.computhink.aip.resource.Connector");
	}
	return bundle;
  }
  public  static String SetDateMaskedValue(String Mask,Date Value){
      try{
      SimpleDateFormat formatter = new SimpleDateFormat(Mask,new Locale("en","US"));
      return formatter.format(Value);
      }catch (Exception e){
                  //System.out.println("Exception:"+e);
                   SimpleDateFormat Format= new SimpleDateFormat("yyyyMMdd");
                   return Format.format(Value);
     }

  }
  public static Date getDate(String Mask,String Value){
      SimpleDateFormat formatter = new SimpleDateFormat(Mask,new Locale("en","US"));
      //ParsePosition pos =new ParsePosition(0);
      /*
       * Issue #627 	: Importing a document with a date field, 
       * 				  the date data that gets imported into viewwise 
       *                  with the format yyyymmdd, irrespective of 
       *                  how it was formatted in the BIF file. 	 
       * Created By		: M.Premananth
       * Date			: 30-June-2006
       */
      formatter.setLenient(false);
      Date HDate = null;
      try{
    	  HDate = formatter.parse(Value.trim());
      }catch(ParseException parseException){
    	  VWLogger.info("Date format dose not match with mask");
      }
      return HDate ;
  }
  public static String MatchVWMask(String Mask){
      int BDayint =Mask.indexOf("ddd");
      int EDayint = BDayint+2;
      if (BDayint >= 0){
         boolean  MoreThree = false;
         while(Mask.charAt(EDayint+1)=='d'){
              EDayint++;
              if(!MoreThree) MoreThree =true;
         }
         String Change ="EEE";
         if(MoreThree) Change ="EEEE";
         if (BDayint == 0)
         Mask = Change+Mask.substring(EDayint+1,Mask.length());
         else
         Mask =Mask.substring(0,BDayint)+Change+Mask.substring(EDayint+1,Mask.length());
      }
      return Mask;
  }
  public static String findHome()
  {
        String  urlPath = "";
        String  indexerHome = "";
        String res = "com/computhink/aip/resource/images/FCSplash.png";
        String jarres = "/lib/"+getString("Init.Jar.Name")+"!";
        
        
        int startIndex = 0;
        int endIndex = 0;
        try
        {
            java.net.URL url = ClassLoader.getSystemClassLoader().getSystemResource(res);
            urlPath = url.getPath();
        }
        catch(Exception e){urlPath="";}
        if (!urlPath.equalsIgnoreCase(""))
        {
            if (urlPath.startsWith("file"))
            {
                startIndex = 6;
                endIndex = urlPath.length() - res.length() - jarres.length();
            }
            else
            {
                ///c:/program%20files/viewwise%20indexer/Classes/indexer.gif
                startIndex = 1;
                endIndex = urlPath.length() - res.length();
            }
            indexerHome = urlPath.substring(startIndex, endIndex-1);  
            indexerHome = indexerHome.replace('/', '\\');
        }
        return cleanUP(indexerHome);
  }
  
  /**
   * Desc   :This method returns the archival path as DTC installed path\Archival.
   * 		 If the Archival folder doesn't exist in the Home path it will create the same
   * 		 and returns the path.
   * Author :Nishad Nambiar
   * Date   :13-May-2008
   * 
   * @return archivalPath
   */
  public static String findArchivalPath(){
	  String resHome = "";
	  String home = findHome();
	  File file = new File(home+"\\Archival");
	  try{
		  if(!file.exists()){
			  file.mkdirs();
		  }
		  if(file.exists())
			  resHome = file.getAbsolutePath();
		  else
			  resHome = home;
	  }catch(Exception ex){
		  resHome = home;
	  }
	  return resHome;
  }
  
  private static String cleanUP(String str) // removes %20    
  {
       String cleanStr = ""; 
       StringTokenizer st = new StringTokenizer(str, "%20");
       while (st.hasMoreElements())
       {
           cleanStr = cleanStr + st.nextToken() + " ";
       }
       return (cleanStr.equalsIgnoreCase("")? str : cleanStr.trim());
  }
  public static String cleanTabInString(String srs){
      final String SepChar= new Character((char)9).toString();
      int i=0;
      while(srs.indexOf(SepChar,i) >= 0){
          i= srs.indexOf(SepChar,i);
          if(i == 0)  srs=srs.substring(i+1);
          else srs =srs.substring(0,i)+srs.substring(i+1);
      }
      return srs;
  }
  public static void deleteDirectory(String directoryName)
    {
        if(directoryName.length()<3) return;
        File directoryFile = new File(directoryName);
        File[] files = directoryFile.listFiles();
        int fileCount=0;
        if(files!=null) fileCount=files.length;
        for (int i=0;i<fileCount;i++)
        {
            if (files[i].isDirectory())
            {
                deleteDirectory(files[i].getAbsolutePath());
            }
            else
            {
                //Log.verboseDebug("Deleting "+files[i].getAbsolutePath());
                files[i].delete();
            }
        }
        // delete the directory
        directoryFile.delete();
    }
    public static void emptyDirectory(String directoryName)
    {
        if(directoryName.length()<3) return;
        File directoryFile = new File(directoryName);
        File[] files = directoryFile.listFiles();
        int fileCount=0;
        if(files!=null) fileCount=files.length;
        for (int i=0;i<fileCount;i++)
        {
            if (files[i].isDirectory())
            {
                deleteDirectory(files[i].getAbsolutePath());
            }
            else
            {
                //Log.verboseDebug("Deleting "+files[i].getAbsolutePath());
                files[i].delete();
            }
        }
       
    }
    public static int to_Number(String str)
    {
        try 
        {
            return Integer.parseInt(str);
        }
        catch(Exception e){return 0;}
    }
 // Issue 562
    public static void maintainLogFiles(String path,int maxCount){
    	
    	String receviedDir = path;
    	String correctDir =findHome()+AIPUtil.pathSep+"Log"+AIPUtil.pathSep+"AIPLog.log";
    	if(receviedDir.equalsIgnoreCase(correctDir)){
    		String dir =findHome()+AIPUtil.pathSep+"Log"+AIPUtil.pathSep;		
    		File logDir = new File(dir);
    		String[] filenames = logDir.list();       
    		int count=0;
    		HashMap hData = new HashMap();
    		
    		for(int i=0;i<filenames.length;i++){
    			
    			if(filenames[i].startsWith("AIPLog.log")){
    				if(filenames[i].equalsIgnoreCase("AIPLog.log"))continue;
    				File f = new File(dir+filenames[i]);
    				count++;
    				try{				         	
    					hData.put(filenames[i],String.valueOf(f.lastModified()));				         	
    				}
    				catch(Exception e){ 			         	
    					
    				}
    			} 
    		}  
    		if(!hData.isEmpty()){
    			ArrayList listManager = new ArrayList(hData.entrySet());
    			Collections.sort(listManager,new Comparitor());
    			if (maxCount!=0){     	
    				
    				if (maxCount<listManager.size()){
    					
    					int i=listManager.size()-1;
    					
    					while(count>maxCount){
    						
    						String fileName = (String)((Map.Entry)listManager.get(i)).getKey();
    						File managerFile = new File(dir+fileName);
    						managerFile.delete();
    						i--;
    						count--;
    						
    					}
    				} 
    			}
    		}
    	}
    }
    // Issue 562
    
    //  Desc   :This method returns the date object from the parameter string date(yearmonthday-flat format) to proper date object
    //Author :Nishad Nambiar
    //Date   :19-Nov-2008
    public static Date getDate(String date){
		Date dateObj = new Date();
		try{					
			String year = date.substring(0,4);
			String month = date.substring(4,6);
			String day = date.substring(6,8);
			String hour = date.substring(9,11);
			String minutes = date.substring(12,14);
			String seconds = date.substring(15,17);
			Calendar calendar = Calendar.getInstance();
			calendar.set(Integer.parseInt(year.trim()),Integer.parseInt(month.trim()) -1,Integer.parseInt(day.trim()),Integer.parseInt(hour.trim()),Integer.parseInt(minutes.trim()),Integer.parseInt(seconds.trim()));
			dateObj = calendar.getTime();
		}catch(Exception e){
			
		}
		return dateObj;
	}
    
	public static boolean CopyFile(File fromFile, File toFile) {
		BufferedInputStream in = null;
		BufferedOutputStream out = null;
		try {
			in = new BufferedInputStream(new FileInputStream(fromFile));
			out = new BufferedOutputStream(new FileOutputStream(toFile));
			int bufferSize = Math.min((int) fromFile.length(), 1024000);
			byte[] buffer = new byte[bufferSize];
			int numRead;
			while ((numRead = in.read(buffer, 0, buffer.length)) != -1) {
				out.write(buffer, 0, numRead);
			}
			out.flush();
			in.close();
		} catch (IOException e) {
			return false;
		} finally {
			try {
				if (in != null)
					in.close();
				if (out != null)
					out.close();
			} catch (IOException e) {
			}
		}
		return true;
	}
}
