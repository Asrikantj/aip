package com.computhink.aip.client;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

 public class VWNodeInfo {
     String NodeName;
     int NodeId;
     int ParentId;
     public VWNodeInfo(){
     }
     public String getNodeName(){
        return NodeName;
     }
     public int getNodeId(){
        return NodeId;
     }
     public int getParentId(){
        return ParentId;
     }
     public void setNodeName(String name){
        NodeName =name;
     }
     public void setNodeId(int NId){
        NodeId =NId;
     }
     public void setParentId(int PId){
        ParentId =PId;
     }
     public String toString(){
        return NodeName;
     }
  }