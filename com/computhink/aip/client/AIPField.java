/*
 * AIPDocField.java
 *
 * Created on April 10, 2004, 2:22 PM
 */

package com.computhink.aip.client;
import java.util.StringTokenizer;
import com.computhink.common.Util;

/**
 *
 * @author  Administrator
 */
public class AIPField implements java.io.Serializable{
    
   
    private int id;    
    private String name;    
    private int formFieldId; 
    private String value;    
    private int order;       
    private int status;    
    private int type; 
    private int mapIndexId;
    private String fDefault;
    
    public final static int ACCEPTED =0;
    public final static int REJECTED =1;
    
    public AIPField() {
    }
    
    public AIPField(String dbString) {
        StringTokenizer Stoken =new StringTokenizer(dbString,Util.SepChar);
        if(Stoken.hasMoreTokens()) id=Util.to_Number(Stoken.nextToken().trim());
        if(Stoken.hasMoreTokens()) name =Stoken.nextToken().trim();
        if(Stoken.hasMoreTokens()) type =Util.to_Number(Stoken.nextToken().trim());
        if(Stoken.hasMoreTokens()) order =Util.to_Number(Stoken.nextToken().trim());
    }    
    public int getId() {
        return this.id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public void setName(String name)
    {
        this.name =name;        
    }    
    public String getName()
    {
        return name;
    }
    public int getType()
    {
        return type;
    }
    public void setType(int type)
    {
        this.type =type;
    }    
    public int getFormFieldId() {
        return this.formFieldId;
    }   
    
    public void setFormFieldId(int formFieldId) {
        this.formFieldId = formFieldId;
    }
   
    public String getValue() {
        return this.value;
    }
    
   
    public void setValue(String value) {
        this.value = value;
    }
  
    public int getOrder() {
        return this.order;
    }
    
   
    public void setOrder(int order) {
        this.order = order;
    }
       
    public int getStatus() {
        return this.status;
    }
    
   
    public void setStatus(int status) {
        this.status = status;
    }
    
    public String toString()
    {
      return name;   
    }    
       
    public String getFDefault() {
        return this.fDefault;
    }
    
    public void setFDefault(String FDefault) {
        this.fDefault =FDefault;
    }
    
    public int getMapIndexId() {
        return mapIndexId;
    }
    
    public void setMapIndexId(int mapIndexId) {
        this.mapIndexId =mapIndexId;        
    }
    
}
