/*
 * AIPForm.java
 *
 * Created on April 10, 2004, 4:23 PM
 */

package com.computhink.aip.client;

import java.util.Vector;
import java.util.StringTokenizer;
import com.computhink.common.Util;

/**
 *
 * @author  Ayman
 */
public class AIPForm implements java.io.Serializable{
    
   
    private int id;    
   
    private String name;
        
    private int mapDTId;
    
    private int desNodeId;
    
    private String desNodePath;
    
    private Vector fields =new Vector();
    
    private boolean exsits;
    
    private int status;
    
     public static final int NORMAL  = 0     ;  public static final int UPDATE  = 1 ;
     public static final int FIELDSNEEDMAP =2; public static final int FIELDSUPDATE =4;
     public static final int NEW     = 5     ; public static final int FOUND         =6;
    
    public AIPForm() {
    }
    public AIPForm(String dbString) {
         StringTokenizer Stoken =new StringTokenizer(dbString,Util.SepChar);
         if(Stoken.hasMoreTokens()) id = Util.to_Number(Stoken.nextToken().trim());
         if(Stoken.hasMoreTokens()) name = Stoken.nextToken().trim();
         if(Stoken.hasMoreTokens()) mapDTId =Util.to_Number(Stoken.nextToken().trim());
         if(Stoken.hasMoreTokens()) desNodeId=Util.to_Number(Stoken.nextToken().trim());
         if(Stoken.hasMoreTokens()) status =Util.to_Number(Stoken.nextToken().trim());
         if(Stoken.hasMoreTokens()) exsits=Util.to_Number(Stoken.nextToken().trim())>0 ? true : false ;
         if(Stoken.hasMoreTokens()) desNodePath =Stoken.nextToken().trim();                        
    }
   
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
   
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public int getMapDTId() {
        return this.mapDTId;
    }
    
    public void setMapDTId(int mapDTId) {
        this.mapDTId = mapDTId;
    }
    
    public int getDesNodeId() {
        return this.desNodeId;
    }
    
    public void setDesNodeId(int desNodeId) {
        this.desNodeId = desNodeId;
    }
    
    public String getDesNodePath() {
        return this.desNodePath;
    }
    
    public void setDesNodePath(String desNodePath) {
        this.desNodePath = desNodePath;
    }
    
    public Vector getFields() {
        return this.fields;
    }
    
    public void setFields(Vector fields) {
        this.fields.removeAllElements();
        this.fields.addAll(fields);
    }
       
    public boolean isExsits() {
        return this.exsits;
    }
        
    public void setExsits(boolean exsits) {
        this.exsits = exsits;
    }
    
    public int getStatus() {
        return this.status;
    }
        
    public void setStatus(int status) {
        this.status = status;
    }
    public String toString()
    {
      return name;   
    }
    public void set(AIPForm f)
    {
        this.id =f.getId();
        this.name =f.getName();
        this.mapDTId =f.getMapDTId();
        this.desNodeId =f.getDesNodeId();
        this.desNodePath =f.getDesNodePath();
        this.fields =f.getFields();
        this.exsits =f.isExsits();
        this.status =f.getStatus();
    }   
    public String getDBString()
    {       
        return  id+Util.SepChar+
                name+Util.SepChar+
                mapDTId+Util.SepChar+
                desNodeId+Util.SepChar+
                desNodePath+Util.SepChar+
                status+Util.SepChar+
                (isExsits()? "1":"0") ;                
    }    
}
