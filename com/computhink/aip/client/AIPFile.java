/*
 * AIPFile.java
 *
 * Created on April 10, 2004, 4:10 PM
 */

package com.computhink.aip.client;
import java.util.Vector;
import java.util.StringTokenizer;
import com.computhink.common.Util;

/**
 *
 * @author  Administrator
 */
public class AIPFile implements java.io.Serializable{
    
     private String FilePath;
     private String CreationDate;
     private String ProcessDate;
     private int Imported;
     private int Pending;
     private int id;
   //  private Vector aipDocs =new Vector();

  public AIPFile() {
  }
  public AIPFile(String dbString)
  {
        StringTokenizer Stoken =new StringTokenizer(dbString,Util.SepChar);
        if(Stoken.hasMoreTokens()) setId(Integer.parseInt(Stoken.nextToken().trim()));
        if(Stoken.hasMoreTokens()) setFilePath(Stoken.nextToken().trim());
        if(Stoken.hasMoreTokens()) setCreationDate(Stoken.nextToken().trim());
        if(Stoken.hasMoreTokens()) setProcessDate(Stoken.nextToken().trim());
        if(Stoken.hasMoreTokens()) setImported(Integer.parseInt(Stoken.nextToken().trim()));
        if(Stoken.hasMoreTokens()) setPending(Integer.parseInt(Stoken.nextToken().trim()));
                        
  }    
  /*
   * Enhancement 	: Enhancements of AIP  
   * 				  Requirment sent by Rajesh on 09-June-2006 through eMail  	 
   * Created By	: M.Premananth
   * Date			: 14-June-2006
   */
  public AIPFile(String dbString,String temp,int ImportedOrPending)
  {
        StringTokenizer Stoken =new StringTokenizer(dbString,",");
        if(Stoken.hasMoreTokens()) setFilePath(Stoken.nextToken().trim());
        if(Stoken.hasMoreTokens()) setCreationDate(Stoken.nextToken().trim());
        if(Stoken.hasMoreTokens()) setProcessDate(Stoken.nextToken().trim());
        if(ImportedOrPending == 1){
            if(Stoken.hasMoreTokens()) setImported(Integer.parseInt(Stoken.nextToken().trim()));
            if(Stoken.hasMoreTokens()) setPending(0);
        }else{
            if(Stoken.hasMoreTokens()) setPending(Integer.parseInt(Stoken.nextToken().trim()));
            if(Stoken.hasMoreTokens()) setImported(0);
        }
        if(Stoken.hasMoreTokens()) setId(Integer.parseInt(Stoken.nextToken().trim()));
                        
  }    
  public void setFilePath(String F){
      FilePath =F;
  }
  public String getFilePath(){
      return FilePath;
  }
  public void setId(int id){
      this.id =id;
  }
  public int getId(){
      return id;
  }
  public void setCreationDate(String F){
      CreationDate =F;
  }
  public String getCreationDate(){
      return CreationDate;
  }
  public void setProcessDate(String F){
      ProcessDate =F;
  }
  public String getProcessDate(){
      return ProcessDate;
  }
  public void setImported(int I){
      Imported =I;
  }
  public int getImported(){
      return Imported;
  }
  public void setPending(int P){
      Pending =P;
  }
  public int getPending(){
      return Pending;
  }
 /* public Vector getAIPDocs()
  {
    return aipDocs;
  }
  public void setAIPDocs(Vector aipdocs)
  {
      this.aipDocs.addAll(aipDocs);
  }*/    
  public String toString(){
      return FilePath;
  }
    
    
}
