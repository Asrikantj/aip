/*
 * AIPInit.java
 *
 * Created on April 10, 2004, 12:01 PM
 */

package com.computhink.aip.client;
import java.util.Hashtable;
import com.computhink.common.Util;

/**
 *
 * @author  Administrator
 */
public class AIPInit implements java.io.Serializable{
    
    private int defaultRoomNodeId ;
    private String pollingTime ;
    private String defaultLinkedFolder;
     private String defaultRoomNodePath;
    private String[] inputFolders=new String[10];    
    
    public AIPInit() {
    }
    public int getDefaultRoomNodeId() 
    {
      return defaultRoomNodeId;
    }
  
    public void setDefaultRoomNodeId(int defaultRoomNodeId) 
    {
      this.defaultRoomNodeId =defaultRoomNodeId;
    }
    
    public String getPollingTime() {
      return pollingTime;
    }
  
  
    public void setPollingTime(String pollingTime) {
      this.pollingTime =pollingTime;
    }
  
  
    public String[] getInputFolders() {
      return inputFolders;
    }
  
 
    public void setInputFolders(String[] inputFolders) {
      this.inputFolders =inputFolders;
    }
  
  
    public String getDefaultLinkedFolder() {
      return defaultLinkedFolder;
    }
  
    public void setDefaultLinkedFolder(String defaultLinkedFolder) {
      this.defaultLinkedFolder =defaultLinkedFolder;
    }
    public void set(AIPInit aipInit)
    {
        this.defaultRoomNodeId =aipInit.defaultRoomNodeId;
        this.defaultRoomNodePath =aipInit.defaultRoomNodePath;
        this.defaultLinkedFolder =aipInit.defaultLinkedFolder;
        this.pollingTime =aipInit.pollingTime;        
        this.inputFolders =aipInit.inputFolders;
    }       
    public String getDefaultRoomNodePath() {
        return defaultRoomNodePath;
    }
      
    public void setDefaultRoomNodePath(String defaultRoomNodePath) {
        this.defaultRoomNodePath =defaultRoomNodePath;
    }
    public Hashtable getAIPInit()
    {        
        Hashtable initMap =new Hashtable();
        initMap.put("DefaultRoomNodeId",Integer.toString(defaultRoomNodeId));
        initMap.put("LinkedFolderPath",this.defaultLinkedFolder);
        initMap.put("PollingTime",this.pollingTime);
        initMap.put("DefaultRoomNodePath",this.defaultRoomNodePath);
        initMap.put("InputFolders",this.inputFolders);
        return initMap;
    }    
    public void setAIPInit(Hashtable initMap)
    {
        if(initMap.containsKey("DefaultRoomNodeId"))
          defaultRoomNodeId =Util.to_Number((String)initMap.get("DefaultRoomNodeId"));
        if(initMap.containsKey("LinkedFolderPath"))
            this.defaultLinkedFolder=(String)initMap.get("LinkedFolderPath");
        if(initMap.containsKey("PollingTime"))
             this.pollingTime=(String)initMap.get("PollingTime");
        if(initMap.containsKey("DefaultRoomNodePath"))
             this.defaultRoomNodePath=(String)initMap.get("DefaultRoomNodePath");
         if(initMap.containsKey("InputFolders"))
             this.inputFolders=(String[])initMap.get("InputFolders");
    }    
}
