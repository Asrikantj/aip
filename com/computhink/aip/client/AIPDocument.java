/*
 * AIPDocument.java
 *
 * Created on April 10, 2004, 12:37 PM
 */

package com.computhink.aip.client;

import java.util.Vector;
import java.util.StringTokenizer;
import com.computhink.common.Util;

/**
 *
 * @author  Administrator
 */
public class AIPDocument implements java.io.Serializable{
    
  private int id;
  private String name;
  private int fileId; 
 
  private String imagesPath;
  private String vwNodePath;
  private boolean createVWNode =false;  
  private boolean append =false;  
  private boolean deleteImages =false;
  private int pendCause ;
  private int status;  
  private String fieldNames;   
  private AIPForm aipForm;
  private String commentFile="";
  private String securityInfo = "";
  
  public final static int IMPORTED =1;
  public final static int PENDING =0;

   
  public AIPDocument() {
  }
  public AIPDocument(String dbString)    
  {
       StringTokenizer Stoken =new StringTokenizer(dbString,Util.SepChar);
       if(Stoken.hasMoreTokens()) setId(Integer.parseInt(Stoken.nextToken().trim()));
       if(Stoken.hasMoreTokens()) setName(Stoken.nextToken().trim());
       if(Stoken.hasMoreTokens()) getAIPForm().setName(Stoken.nextToken().trim());
       if(Stoken.hasMoreTokens()) setFileId(Integer.parseInt(Stoken.nextToken().trim()));
       if(Stoken.hasMoreTokens()) setImagesPath(Stoken.nextToken().trim());
       if(Stoken.hasMoreTokens()) setStatus(Integer.parseInt(Stoken.nextToken().trim()));
       if(Stoken.hasMoreTokens()) setPendCause(Integer.parseInt(Stoken.nextToken().trim()));
       if(Stoken.hasMoreTokens()) setVWNodePath(Stoken.nextToken().trim());
       if(Stoken.hasMoreTokens()) setCommentFile(Stoken.nextToken().trim());
       if(Stoken.hasMoreTokens()) setFieldNames(Stoken.nextToken().trim());
       if(Stoken.hasMoreTokens()) setOptions(Integer.parseInt(Stoken.nextToken().trim()));
       if(Stoken.hasMoreTokens()) setSecurityInfo(Stoken.nextToken().trim());
 
  } 
  public Object clone() throws CloneNotSupportedException {
      AIPDocument clone =new AIPDocument();
      clone.setId(this.id);
      clone.setName(this.name);
      clone.setFileId(this.fileId);
      clone.setAppend(this.append);
      clone.setCommentFile(this.commentFile);
      clone.setCreateVWNode(this.createVWNode);
      clone.setDeleteImages(this.deleteImages);
      clone.setFieldNames(this.fieldNames);
      clone.setImagesPath(this.imagesPath);
      clone.setOptions(this.getOptions());
      clone.setPendCause(this.getPendCause());
      clone.setStatus(this.status);
      clone.setVWNodePath(this.getVWNodePath());
      clone.setAIPForm(this.getAIPForm());
      return clone;
      
   // return super.clone();
      
  }
  public int getId() {
      return id;
  }
  
  public void setId(int id) {
      this.id =id;
  }
 
  public String getName() {
      return name;
  }  

  public void setName(String name) {
      this.name =name;
  }
 
  public int getFileId() {
      return fileId;
  }
  
 
  public void setFileId(int fileId) {
      this.fileId =fileId;
  }
  public String getImagesPath() {
      return imagesPath;
  }
  
  public void setImagesPath(String imagesPath) {
      this.imagesPath =imagesPath;
  }
 
  public String getVWNodePath() {
      return vwNodePath ;
  }
  

  public void setVWNodePath(String vwNodePath) {
      this.vwNodePath =vwNodePath;
  }
  
  public boolean isCreateVWNode() 
  { 
      return createVWNode;
  }
  
  
  public void setCreateVWNode(boolean createVWNode) {
      this.createVWNode =createVWNode;
  }
  
  
  public boolean isAppend() {
      return append;
  }
  
  public void setAppend(boolean append) {
      this.append =append;
  }
  
  public boolean isDeleteImages() {
      return deleteImages;
  }
  
 
  public void setDeleteImages(boolean deleteImages) {
      this.deleteImages =deleteImages;
  }
 
  public int getPendCause() {
      return pendCause;
  }
  
  
  public void setPendCause(int pendCause) {
      this.pendCause =pendCause;
  }
 
  public int getStatus() {
      return status;
  }
  
  
  public void setStatus(int status) {
      this.status =status;
  }
  
  public String getFieldNames() {
      return fieldNames;
  }
  
  
  public void setFieldNames(String fieldNames) {
      this.fieldNames =fieldNames;
  }
  
  public AIPForm getAIPForm() {
      if(aipForm == null)
          aipForm =new AIPForm();
      return aipForm;
  }
   
  public void setAIPForm(AIPForm aipForm) {
      this.aipForm =aipForm;
  }
  
 
  public String getCommentFile() {
      return commentFile;
  }
  
 
  public void setCommentFile(String commentFile) {
      this.commentFile=commentFile;
  }
  public int getOptions()
  {
      int createLocationOption = 0;
      int deleteImagesOption = 0;
      int appendOption = 0;
      if (createVWNode)
          createLocationOption = 1;
      if (deleteImages)
          deleteImagesOption = (1 << 1);
      if (append)
          appendOption = (1 << 2);
      return createLocationOption + deleteImagesOption + appendOption;  
      
  } 
  public void setOptions(int options)
  {
      int option1 = 1;
      int option2 = 1 << 1;
      int option3 = 1 << 2;
      if ((options & option1) == option1)
          setCreateVWNode(true);
      if ((options & option2) == option2)
          setDeleteImages(true);
      if ((options & option3) == option3)
          setAppend(true);
  }
/**
 * @return the securityInfo
 */
public String getSecurityInfo() {
	return securityInfo;
}
/**
 * @param securityInfo the securityInfo to set
 */
public void setSecurityInfo(String securityInfo) {
	this.securityInfo = securityInfo;
}    
}
