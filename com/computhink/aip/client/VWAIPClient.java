/*
 * VWAIPClient.java
 *
 * Created on June 14, 2004, 10:48 AM
 */

package com.computhink.aip.client;

import com.computhink.vwc.VWClient;
import com.computhink.vwc.Session;
import com.computhink.aip.resource.ResourceManager;
import com.computhink.aip.ui.AIPConnector;
import com.computhink.aip.util.VWLogger;
import com.computhink.common.Document;
import com.computhink.common.ServerSchema;
import com.computhink.common.VWEvent;
import com.computhink.common.ViewWiseErrors;
import com.computhink.common.Util;
import com.computhink.common.VWDoc;
import com.computhink.vws.server.Client;
import com.computhink.vws.server.VWS;
import com.computhink.vws.server.VWSLog;
import com.computhink.aip.AIPManager;





import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.util.StringTokenizer;
import java.util.Hashtable;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 *
 * @author  Ayman
 */
public class VWAIPClient extends VWClient 
{
	
	//resource manager used to get static strings used in interface or other places
	private final ResourceManager connectorManager = ResourceManager.getDefaultManager();
		
    public VWAIPClient(String workingFolder) 
    {
        super("0B9B5177-1011-4BBF-B617-455288043D4A",workingFolder, Client.AIP_Client_Type);        
    }
    public int createDocument(int sid, Document doc)
    {
    	
    	Vector storageVect=new Vector();
    	int retdssspace=0;
    	Session session = getSession(sid);
    	getAzureStorageCredentials(sid,doc.getId(),storageVect);
    	VWLogger.info("storageVect::::"+storageVect.get(0).toString());
    	String storageArray[]=storageVect.get(0).toString().split(Util.SepChar);
    	VWLogger.info("Stroage type from storage array"+storageArray[7]);
    	if(storageArray[7].equals("On Premises")){
    		retdssspace =checkDssSpace(sid);
    	}    
    	VWLogger.info("Check DSS Space method output  >>> "+retdssspace);
		VWLogger.info("ViewWiseErrors.dssCriticalError >>> "+ViewWiseErrors.dssCriticalError);
		VWLogger.info("AIPManager.isAIPServiceStarted() >>> "+AIPManager.isAIPServiceStarted());
    	if(AIPManager.isAIPServiceStarted()&&retdssspace==ViewWiseErrors.dssCriticalError)
    	{	
    	   	AIPManager.logoutAll();
    		Runtime rt = Runtime.getRuntime();
    		try {
    			Process proc = rt.exec("cmd /c Net stop \"Contentverse AIP Service\"");
    			VWLogger.info("Critical Alert:  Low Disk space: AIP Service Stopped. Contact Administrator immediately.");
    		} catch (Exception e) {
    			// TODO Auto-generated catch block
    			VWLogger.info("Exception inside stop service of AIP: "+e.getMessage());
    		}
    		return dssCriticalError;
    	} else if(retdssspace==ViewWiseErrors.dssCriticalError){
    		return dssCriticalError;
    	} 
    	/**Following conditions are added to fix issue - AIP creating document with 0 pages when the DSS is inactive. Added by Vanitha On 17/01/2020 **/
    	else if(retdssspace==ViewWiseErrors.dssInactive){
    		return dssInactive;
    	} else if (retdssspace < 0) {
    		return retdssspace;
    	}
    	
       // Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
		try {
			// Modified for Lock document type,if ValidateNodeDocType method
			// returns 2 means the particular Document is not having permission
			// for cabinet,process will continue by placing the document in
			// pending panel,Pending cause will be set.
			Vector validatNode = new Vector();
			ValidateNodeDocType(sid, 2, doc.getDocType().getId(), doc.getParentId(), validatNode);
			VWLogger.info("validatNode :::: " + validatNode);			
			VWLogger.info("Available Page Count :"+doc.getPageCount());
			boolean isDocPurgeRequired = false;
			if (!validatNode.get(0).toString().equalsIgnoreCase("1"))
				return DocumentTypeNotPermitted;
			else {
				if (retdssspace >= 0) {
					VWLogger.info("Before calling createDocument dss space check value:"+retdssspace);
					Document retDoc = vws.createDocument(session.room, sid, doc);
					VWLogger.info("After calling createDocument......" + retDoc);
					if (retDoc.getId() == ViewWiseErrors.InvalidDateMask) {
						return ViewWiseErrors.InvalidDateMask;
					}
	
					if (retDoc.getId() > 0 && retDoc.getVersionDocFile() != null
							&& !retDoc.getVersionDocFile().equals("")) {
						// false = do not assume id folder
						VWLogger.info("Before calling setDocFile docID......"+retDoc.getId());
						int ret = setDocFile(sid, retDoc, retDoc.getVersionDocFile(), false, true);
						VWLogger.info("ret value from setDocFile :::: " + ret);
						if (ret >= 0) {
							VWLogger.info("Storage location for the document "+doc.getName()+" :::: " + retDoc.getVWDoc().getDstFile());
							String documentLocation = retDoc.getVWDoc().getDstFile();
							VWLogger.info("All.zip storage location :"+documentLocation);
							int pageCount = vws.getDocumentStorageDetails(sid, documentLocation);
							VWLogger.info("Document storage page count :"+pageCount);
							int sourcePageCount = getSourceLocationPageCount(retDoc.getVWDoc().getDstFile());
							VWLogger.info("Actual page count for the processing document :"+sourcePageCount);
							if (pageCount != sourcePageCount) {
								VWLogger.info("Document storage page count does not match with the processed document page count ....");
								isDocPurgeRequired = true;
								ret = ViewWiseErrors.allDotZipDoesNotExist;
							}
						} else {
							isDocPurgeRequired = true;
						}
						VWLogger.info("Document purge required :"+isDocPurgeRequired);
						if (isDocPurgeRequired) {
							purgeDocument(sid, retDoc);
							return ret;
						}
					}
					doc.set(retDoc);
					return doc.getId();
				} else 
					return retdssspace;
			} 
		} catch (Exception e) {
			VWLogger.info("Exception while createDocument ::" + e.getMessage());
			return Error;

		}
    }
    /**
     * CV10.1 issue fix for AIP processing location  issue fix for multiple system.
     * @param sid
     * @param aipInit
     * @param ipaddress
     * @return
     */
    public int setAIPInit(int sid,AIPInit aipInit,String ipaddress)
    {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            
            return vws.setAIPInit
                              (session.room, sid, aipInit.getAIPInit(),ipaddress);         
            
        }
        catch(Exception e)
        {
        	VWLogger.info("Exception while setAIPInit : "+e.getMessage());
            return Error;
        }
       
    }   
    
    public int setAIPInit(int sid,AIPInit aipInit)
    {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            
            return vws.setAIPInit
                              (session.room, sid, aipInit.getAIPInit());         
            
        }
        catch(Exception e)
        {
        	VWLogger.info("Exception while setAIPInit : "+e.getMessage());
            return Error;
        }
       
    }    
    public int getAIPInit(int sid,AIPInit aipInit)
    {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
        	InetAddress inet=null;

        	try {
        		inet = InetAddress.getLocalHost();
        	} catch (UnknownHostException e) {
        		// TODO Auto-generated catch block
        		VWLogger.info("Exception while getting inet address :::"+e.getMessage());
        	}
    		
            Hashtable aInit = vws.getAIPInit
                              (session.room, sid,inet.getHostAddress());
            if(aInit != null && !aInit.isEmpty())
                aipInit.setAIPInit(aInit);
            else return invalidAIPComponents;
            return NoError;
        }
        catch(Exception e)
        {
        	VWLogger.info("Exception while getAIPInit ::"+e.getMessage());
            return Error;
        }   
    } 
    public int getAIPForms(int sid,Vector forms)
    {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector rets =vws.getAIPForms(session.room, sid);
            forms.addAll(vectorToForms(rets));
            for(int i=0;i<forms.size();i++)
            {
                 getAIPFormFields(sid,(AIPForm)forms.get(i));  
                 getAIPFormMap(sid,(AIPForm)forms.get(i));
            }    
            return NoError;
        }
        catch(Exception e)
        {
        	VWLogger.info("Exception while getAIPForms ::"+e.getMessage());
            return Error;
        }  
   }
   public int getAIPFormFields(int sid,AIPForm form)
   {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector rets =vws.getAIPFormFields(session.room, sid,form.getId());
            form.setFields(vectorToFormFields(rets));
            
            return NoError;
        }
        catch(Exception e)
        {
        	VWLogger.info("Exception while getAIPFormFields :"+e.getMessage());
            return Error;
        }  
        
   }    
   public int setAIPForm(int sid,AIPForm form)
   {
       Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            int id =vws.setAIPForm(session.room, sid,form.getDBString());
            if(id >0)
                form.setId(id);
            return id;
            
        }
        catch(Exception e)
        {
        	VWLogger.info("Exception while setAIPForm ::"+e.getMessage());
            return Error;
        }
   } 
   public int setAIPFormFields(int sid,AIPForm form)
   {     
         if(form.getId() <=0 || form.getFields() == null || 
                          form.getFields().size() ==0 ) return invalidParameter;
        for(int i=0;i<form.getFields().size();i++)
        {
           int ret =setAIPFormField(sid,form.getId(),(AIPField)form.getFields().get(i)); 
           if(ret < NoError) return ret;
        }    
        return NoError; 
   }   
   public int setAIPFormField(int sid,int formId,AIPField field)
   {
       Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        if(formId <=0) return invalidParameter;
        try
        {
            String fieldParam =field.getId()+Util.SepChar+
                               field.getName()+Util.SepChar+
                               field.getType()+Util.SepChar+
                               field.getOrder();
                                
            
            int id = vws.setAIPFormField(session.room, sid,formId,fieldParam);
            if(id>0)
                field.setId(id);            
            return id;            
            
        }
        catch(Exception e)
        {
        	VWLogger.info("Exception while set Form Field1 :"+e.getMessage());
            return Error;
        }
   } 
   public int deleteAIPForm(int sid,AIPForm form)
   {
       Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            return vws.deleteAIPForm(session.room,sid,form.getId());
            
        }
        catch(Exception e)
        {
        	VWLogger.info("Exception while deletAIPForm :"+e.getMessage());
            return Error;
        }
   }
   public int deleteAIPFormFields(int sid,AIPForm form)
   {
       Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
           return  vws.deleteAIPFormFields(session.room,sid,form.getId());
            
        }
        catch(Exception e)
        {
        	VWLogger.info("Exception while deletAIPFormFields : "+e.getMessage());
            return Error;
        }
   }
   public int deleteAIPFormField(int sid,AIPField field)
   {
       Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
           return  vws.deleteAIPFormField(session.room,sid,field.getId());
            
        }
        catch(Exception e)
        {
        	VWLogger.info("Exception while deletAipFomField"+e.getMessage());
            return Error;
        }
   }
   public int getAIPFormMap(int sid,AIPForm form) 
   {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
              Vector maps = vws.getAIPFormMap(session.room,sid,form.getId(),form.getMapDTId());
              applyMapFieldsToForm(form,maps);              
              return NoError;
        }
        catch(Exception e)
        {
        	VWLogger.info("Exception while getAIPFormMap :: "+e.getMessage());
            return Error;
        }
   } 
   public int setAIPFormMap(int sid,AIPForm form) 
   {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        Vector fields =form.getFields();
        if(form.getId() <=0 || form.getMapDTId() <= 0 || fields.size() == 0)
           return invalidParameter;
        deleteAIPFormMap(sid,form);
        for(int i=0;i<fields.size();i++)
        {    
          int ret= setAIPFormFieldMap(sid,form.getId(),form.getMapDTId(),(AIPField)fields.get(i));
          if(ret <=0)
            return ret;  
        }    
        return NoError;   
   }
   public int setAIPFormFieldMap(int sid,int formId,int mapDTId,AIPField field)
   {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            String mapFieldParams = formId+Util.SepChar+
                                    mapDTId+Util.SepChar+
                                    "0"+Util.SepChar+
                                    field.getMapIndexId()+Util.SepChar+
                                    field.getId()+Util.SepChar+
                                    field.getFDefault();
            return vws.setAIPFormFieldMap(session.room,sid,mapFieldParams);            
            
        }
        catch(Exception e)
        {
        	VWLogger.info("Exception while setAIPFormFieldMap : "+e.getMessage());
            return Error;
        }    
   }    
   public int deleteAIPFormMap(int sid,AIPForm form) 
   {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
             return vws.deleteAIPFormMap(session.room,sid,form.getId(),form.getMapDTId());              
            
        }
        catch(Exception e)
        {
        	VWLogger.info("Exception while deletAIPFormMap :"+e.getMessage());
            return Error;
        }   
   } 
   //------------
   public int getAIPHistoryFiles(int sid,Vector aipFiles)
   {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
             Vector ret= vws.getAIPHistoryFiles(session.room,sid); 
             aipFiles.addAll(vectorToAIPFiles(ret));
             return NoError;
            
        }
        catch(Exception e)
        {
        	VWLogger.info("Exception while getAIPHistoryFiles :"+e.getMessage());
            return Error;
        } 
   }    
   /*
    * Enhancement 	: Enhancements of AIP  
    * 				  Requirment sent by Rajesh on 09-June-2006 through eMail  	 
    * Created By	: M.Premananth
    * Date			: 14-June-2006
    */
   public int getAIPHistoryCount(int sid,Vector aipCount,int ImportOrPending)
   {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            String Params = ""+ImportOrPending;
            Vector ret= vws.getAIPHistoryCount(session.room,sid,Params); 
            aipCount.addAll(ret);
            return NoError;
            
        }
        catch(Exception e)
        {
            VWLogger.info("Exception while getAIPHistoryCount :"+e.getMessage());
            return Error;
        } 
   } 
   /**
    * 
    * @param sid - session id
    * @param aipFiles - Vector for getting aip files
    * @param startingPos - starting position of record
    * @param endingPos - end position of record
    * @param ImportOrPending - 0 for pending and 1 for import.
    * @return
    */
   public int getAIPHistoryFiles(int sid,Vector aipFiles,int startingPos,int endingPos,int ImportOrPending)
   {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
             String Params = startingPos+Util.SepChar+endingPos+Util.SepChar+ImportOrPending;
             Vector ret= vws.getAIPHistoryFiles(session.room,sid,Params); 
             aipFiles.addAll(vectorToAIPFiles(ret));
             return NoError;
            
        }
        catch(Exception e)
        {
        	VWLogger.info("Exception while getAIPHistoryFiles : "+e.getMessage());
            return Error;
        } 
   }    
   
   public int getAIPHistoryFilesForArchive(int sid,Vector aipFiles,int Age,int RowCount)
   {
       Session session = getSession(sid);
       if(session==null) return invalidSessionId;
       VWS vws = getServer(session.server);
       if (vws == null) return ServerNotFound;
       try
       {
   	    	ServerSchema mySchema = Util.getMySchema();
   	    	String Params = Age+Util.SepChar+RowCount+Util.SepChar+mySchema.address;
            Vector ret= vws.getAIPHistoryFilesForArchive(session.room,sid,Params); 
            aipFiles.addAll(vectorToAIPFiles(ret));
            return NoError;
           
       }
       catch(Exception e)
       {
    	   VWLogger.info("Exception while getAIPHistoryFilesForArchive : "+e.getMessage());
           return Error;
       } 
       
   }
   
   
   public int deleteAIPHistoryArchivedFiles(int sid)
   {
       Session session = getSession(sid);
       if(session==null) return invalidSessionId;
       VWS vws = getServer(session.server);
       if (vws == null) return ServerNotFound;
       try
       {    	                
            int ret= vws.deleteAIPHistoryArchivedFiles(session.room,sid);             
            return ret;
           
       }
       catch(Exception e)
       {
    	   VWLogger.info("Exception deleteAIPHistoryArchivedFiles : "+e.getMessage());
           return Error;
       } 
       
   }
   
   public int setAIPHistoryFile(int sid,AIPFile aipFile)
   {
       Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            String fileParams =aipFile.getId()+Util.SepChar+
                               aipFile.getFilePath()+Util.SepChar+
                               aipFile.getCreationDate()+Util.SepChar+
                               aipFile.getProcessDate()+Util.SepChar+
                               aipFile.getImported()+Util.SepChar+
                               aipFile.getPending();
            int ret = vws.setAIPHistoryFile(session.room,sid,fileParams);              
            if(ret > 0) aipFile.setId(ret);
            return ret;
        }
        catch(Exception e)
        {
        	VWLogger.info("Exception while setAIPHistoryFile : "+e.getMessage());
            return Error;
        } 
   }    
   public int deleteAIPHistoryFile(int sid,AIPFile aipFile) 
   {
       Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
             return vws.deleteAIPHistoryFile(session.room,sid,aipFile.getId());              
            
        }
        catch(Exception e)
        {
            return Error;
        }
   }    
   public int setAIPDocument(int sid,AIPDocument doc) 
   {
       Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            
            String aipDocParams =doc.getId()+Util.SepChar+
                                doc.getName()+Util.SepChar+
                                doc.getAIPForm().getName()+Util.SepChar+
                                doc.getFileId()+Util.SepChar+
                                doc.getStatus()+Util.SepChar+
                                doc.getPendCause()+Util.SepChar+
                                doc.getImagesPath()+Util.SepChar+                                
                                doc.getVWNodePath()+Util.SepChar+
                                doc.getCommentFile()+Util.SepChar+
                                doc.getFieldNames()+Util.SepChar+
                                doc.getOptions()+Util.SepChar+doc.getSecurityInfo();
           
             int ret = vws.setAIPDocument(session.room,sid,aipDocParams);              
             if(ret > 0){
                 doc.setId(ret);                 
                 setAIPDocFields(sid,doc);
             }
             return ret;
        }
        catch(Exception e)
        {
        	VWLogger.info("Exception while ");
            return Error;
        }        
   }
   public int setAIPDocFields(int sid,AIPDocument doc)
   {
       Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector fieldsParams =vectorToAIPDocFieldsParams(doc);
            return  vws.setAIPDocFields(session.room,sid,fieldsParams);
        }
        catch(Exception e)
        {
        	VWLogger.info("Exception while  setAIPDocFields"+e.getMessage());
            return Error;
        } 
   }
   public int getAIPDocuments(int sid,AIPFile file,Vector aipDocs)
   {
	   return getAIPDocuments(sid, file, aipDocs, 0);
   }

	   
   public int getAIPDocuments(int sid,AIPFile file,Vector aipDocs, int pendingStatus)
   {
       Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
             Vector ret= vws.getAIPDocuments(session.room,sid,file.getId(), pendingStatus); 
             aipDocs.addAll(vectorToAIPDocuments(ret));
             return NoError;
            
        }
        catch(Exception e)
        {
        	VWLogger.info("Exception while getAIPDocuments :"+e.getMessage());
            return Error;
        }  
   }    
   public int getAIPDocFields(int sid,AIPDocument aipDoc)
   {
       Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
             Vector ret= vws.getAIPDocFields(session.room,sid,aipDoc.getId()); 
             aipDoc.getAIPForm().setFields(vectorToAIPDocFields(ret));
             return NoError;
            
        }
        catch(Exception e)
        {
            VWLogger.info("Exception while getAIPDocFields :"+e.getMessage());
            return Error;
        }  
       
   }    
   public int deleteAIPDocument(int sid,AIPDocument aipDoc)
   {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
             return vws.deleteAIPDocument(session.room,sid,aipDoc.getId());              
            
        }
        catch(Exception e)
        {
        	VWLogger.info("Exception while deleteAIPDocument :"+e.getMessage());
            return Error;
        }
   }
   public int getAIPEmailSuffixes(int sid,Vector suffixes)
   {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
           Vector ret = vws.getAIPEmailSuffixes(session.room,sid);
           suffixes.addAll(ret);
           return NoError;
        }
        catch(Exception e)
        {
        	VWLogger.info("Exception while getAIPEmailSuffixes :"+e.getMessage());
            return Error;
        }    
   }    
   
   public int setAIPEmailSuffixes(int sid,Vector suffixes)
   {
       
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
          return vws.setAIPEmailSuffixes(session.room,sid,suffixes);
           
        }
        catch(Exception e)
        {
        	VWLogger.info("Exception while setAIPEmailSuffixes :"+e.getMessage());
            return Error;
        }     
   }    
 //---------------AIP private-----------------------
   private Vector vectorToForms(Vector f)
   {
       Vector forms =new Vector();
       for(int i=0;i<f.size();i++)
       {
            AIPForm form =new AIPForm((String)f.get(i));
            forms.add(form);
       }    
       return forms;
   }    
   private Vector vectorToFormFields(Vector f)
   {
        Vector fields =new Vector();
       for(int i=0;i<f.size();i++)
       {
            AIPField field =new AIPField((String)f.get(i));
            fields.add(field);
       }    
       return fields;       
   }
   private Vector vectorToAIPFiles(Vector f)
   {
        Vector files =new Vector();
        for(int i=0;i<f.size();i++)
        {
            AIPFile file =new AIPFile((String)f.get(i));
            files.add(file);
        }   
        return files;
       
   } 
   private void applyMapFieldsToForm(AIPForm form,Vector maps)
   {
      if(maps != null && maps.size() != 0)
      {
          for(int i=0;i<maps.size();i++)
          {    
                String srs = (String)maps.get(i);             
                StringTokenizer Stoken =new StringTokenizer(srs,Util.SepChar);
                int Indexid =0;int FieldId =0;                
                if(Stoken.hasMoreTokens()) Stoken.nextToken(); // map Id
                if(Stoken.hasMoreTokens()) Indexid =Integer.parseInt(Stoken.nextToken().trim());
                if(Stoken.hasMoreTokens()) Stoken.nextToken().trim();// dt Id
                if(Stoken.hasMoreTokens()) Stoken.nextToken().trim();// formId
                if(Stoken.hasMoreTokens()) FieldId =Integer.parseInt(Stoken.nextToken().trim());
                AIPField field = getAIPField(FieldId,form.getFields());
                field.setMapIndexId(Indexid);                
                if(Stoken.hasMoreTokens()) field.setFDefault(Stoken.nextToken().trim());                                
          }
       }    
       
   }
   private AIPField getAIPField(int fieldId,Vector fields)
   {
        AIPField field;
        for (int i=0;i<fields.size();i++)
        {
            field =(AIPField)fields.get(i);
            if(field.getId() == fieldId) return field;           
        }    
        field =new AIPField();
        field.setId(fieldId);
        fields.add(field);
        return field;
   }    
   private Vector vectorToAIPDocuments(Vector v)
   {
        Vector docs =new Vector();
        for(int i=0;i<v.size();i++)
        {
            AIPDocument doc =new AIPDocument((String)v.get(i));
            docs.add(doc);
        }   
        return docs;
       
   } 
   private Vector vectorToAIPDocFields(Vector v)
   {
       Vector docFields =new Vector();
        for(int i=0;i<v.size();i++)
        {       
            StringTokenizer Stoken =new StringTokenizer((String)v.get(i),Util.SepChar);
            AIPField field =new AIPField();
            if(Stoken.hasMoreTokens()) field.setId(Util.to_Number(Stoken.nextToken().trim()));
            if(Stoken.hasMoreTokens()) Stoken.nextToken().trim();
            if(Stoken.hasMoreTokens()) Stoken.nextToken().trim();
            if(Stoken.hasMoreTokens()) field.setOrder(Util.to_Number(Stoken.nextToken().trim()));
            if(Stoken.hasMoreTokens()) field.setValue(Stoken.nextToken().trim());
            if(Stoken.hasMoreTokens()) field.setStatus(Util.to_Number(Stoken.nextToken().trim()));            
            docFields.add(field);
        }   
        return docFields;
        
   } 
   private Vector vectorToAIPDocFieldsParams(AIPDocument doc)
   {      
      Vector fields = doc.getAIPForm().getFields();
      Vector fieldsParams =new Vector();
      for(int i=0;i<fields.size();i++)
      {     
            AIPField field =(AIPField)fields.get(i);            
            String fieldPrams =field.getId()+Util.SepChar+
                               doc.getId()+Util.SepChar+
                               "0"      +Util.SepChar+
                               field.getOrder()+Util.SepChar+
                               field.getValue()+Util.SepChar+
                               field.getStatus();
            fieldsParams.add(fieldPrams);
      }    
      return fieldsParams;
   }    
 
   /* Issue # 754: If VWS server is down, client should be notified
   *  Methods added of issue 754, C.Shanmugavalli, 10 Nov 2006
   */
   public void eventHandler(VWEvent vwevent)
   {
   		if(vwevent.getId() == VWEvent.ADMIN_DISCONNECT)
   		{
   			AIPConnector.getInstance().getEHMainPanel().stopProcessAndExit();
   			SwingUtilities.invokeLater(new Thread(){
   	    		public void run(){
   	    			Util.Msg(null, connectorManager.getString("General.Msg.ServerDisconnected"),connectorManager.getString("VWAIPClient.AIP"));
   	    		}
   	    	});	    	       			   		
   		}
   }
   
   public  int checkDssSpace(int sid){
	   int retval=0;
	   try{
		   String str="";
		   Vector retvect=new Vector();
		   Vector size=new Vector();
		   VWLogger.info("Before calling getLatestDocId method......");
		   getLatestDocId(sid,retvect);
		   VWLogger.info("Last maximum doc id:"+retvect);
		   str=(String) retvect.get(0);
		   int maxDocId=Integer.parseInt(str);
		   Document latestDoc=new Document(maxDocId);
		   retval=checkAvailableSpaceInDSS(sid,latestDoc,"",size);
		   VWLogger.info("checkAvailableSpaceInDSS return value >>>>>:"+retvect);
		   long availableSpace=(Long) size.elementAt(0);
		   long registrySpace=(Long)size.get(1);
		   if(registrySpace>availableSpace){
			   retval= -507;    		
		   }
	   }

	   catch(Exception e){ 
		   VWLogger.info("checkDssSpace:"+e.getMessage());
	   }
	   return retval;
   }
   /**
    * Issue Fix :- CV10.1 Method created to send the current session id 
    * to server to remove the orphan session.
    * @param currSid
    * @return
    */
   public int setAIPConnectionStatus(int currSid) {
	   // TODO Auto-generated method stub
	   Session session = getSession(currSid);
	   if(session==null) return invalidSessionId;
	   VWS vws = getServer(session.server);
	   if (vws == null) return ServerNotFound;
	   try
	   {
		  // VWLogger.info("in vwaipclient setAIPConnectionStatus"+currSid); 
		   Vector result=new Vector();
		   result= vws.setAIPConnectionStatus(session.room,currSid);
		   if(result.size()>0&&result!=null){
			   return   (int) result.get(0);
		   }

	   }catch(Exception e){
		   VWLogger.info("Exception in setAIPConnectionStatus"+e.getMessage());
	   }
	   return NoError;	
   }
   
   /**
    * This method is used to get the folder location from the DB
    * @param currSid
    * @param folderName
    * @author vanitha.s
    * @return
    */
   public Vector getAIPDocumentLocation(int currSid, String folderName) {
	   Vector result = null;
	   String location = null;
	   Session session = getSession(currSid);
	   if(session !=null) {
		   VWS vws = getServer(session.server);
		   if (vws != null) {
				try {
					folderName +=  Util.SepChar  + 0 + Util.SepChar  + 5;
					result = vws.getFolders(session.room, currSid, folderName);	
					if (result != null && result.size() == 1) {
						location = session.room+"\\"+result.get(0).toString();
						result.set(0, location);
					}
				} catch (Exception e) {
					VWLogger.info("Exception in setAIPConnectionStatus" + e.getMessage());
				}
		   }
	   }	   
	   return result;	
   }
   
   public int getSourceLocationPageCount(String location) {
		int pageCount = 0;
		try {			
			if (new File(location).exists()) {
				ZipFile zfile = new ZipFile(location);
				java.util.Enumeration e = zfile.entries();
				while (e.hasMoreElements())
				{
					if (((ZipEntry) e.nextElement()).getName().endsWith(".vw"))
						pageCount++;
				}
				zfile.close();
			}
		} catch (Exception e) {
			VWSLog.dbg("Exception while getting document storage information :"+e);
		}
		return pageCount;
	}
}
