/*
 * BIFShadowFileManager.java
 *
 * Created on December 5, 2003, 9:44 AM
 */

package com.computhink.aip;

import java.io.*;
import java.nio.*;
import java.nio.channels.*;
import java.nio.charset.*;
import java.util.*;
import java.util.regex.*;
import com.computhink.aip.util.*;
//import com.computhink.aip.resource.*;


/**
 * A class that extends BIFFileParser to work with BIF shadow files.
 * It keeps track of what documents got processed and the processing results of
 * each document.
 * It is useful for the AIP to resume from a certain point in processing a BIF
 * file.
 *
 * BIF Shadow Format:
 *
 * [Documents File]
 * [documents]
 * count=(count)
 * (file id)
 * document1=(doctype)	"(image1)";"(image2)";"(image3)"
 * doc1.fields=(index1)~(field status)|(index2)~(field status)|(index3)~(field status)
 * doc1.field1=(index1 value)~(field status)
 * doc1.field2=(index2 value)~(field status)
 * doc1.field3=(index3 value)~(field status)
 * doc1.comment_file="(comment_file)"
 * doc1.location=(Room/Cabinet/Drawer/Folder[/Subfolder/...])
 * doc1.create_location=([true|FALSE])
 * doc1.append=([true|FALSE])
 * doc1.delete_images=([true|FALSE])
 * (document status)(document pending cause)(document history input status)
 *
 * Description of changes from BIF format:
 * -(file id) consists of 10 digits and represents the file ID assigned during
 *  the import process.
 * -(field status) is one digit that holds either EHDocFieldInfo.ACCEPTED or 
 *  EHDocFieldInfo.REJECTED, indicating whether the field was accepted or
 *  rejected.
 * -(document status) is one digit that holds either EHDocInfo.IMPORTED or 
 *  EHDocInfo.PENDING, indicating whether the document was imported successfully
 *  or otherwise pending.
 * -(document pending cause) consists of 2 digits and represents the pending
 *  cause integer in case the document was pending. Otherwise, it would just
 *  hold the value zero.
 * -(document history input status) is one digit that holds either 1 or 0
 *  depending on whether the document was input into the history database tables
 *  or not.
 *
 * @author  amaleh
 */
public class BIFShadowFileManager extends BIFFileParser implements GradualFileUpdater 
{

    /**
     * Regex for fields line has an extra digit after every field to indicate
     * whether it has been accepted or rejected.
     */
    public static final String REGEX_FIELDS = "[Dd][Oo][Cc]\\d+[.][Ff][Ii][Ee][Ll][Dd][Ss]=(([^|~]+[~][\\d])([|]([^|~]+[~][\\d]))*)?";
    
    /**
     * Regex for (file id) line
     */
    public static final String REGEX_FILE_ID = "\\d{10}";
    
    /**
     * Regex for (document status)(document pending cause)(document history input status) line
     */
    public static final String REGEX_DOC_STATUS = "\\d{4}";

    /**
     * Pattern based on the regex for fields
     */
    protected static final Pattern pFields = Pattern.compile(REGEX_FIELDS);

    /**
     * Pattern based on the regex for file id line
     */
    protected static final Pattern pFileID = Pattern.compile(REGEX_FILE_ID);
    
    /**
     * Pattern bsaed on the regex for doc status line
     */
    protected static final Pattern pDocStatus = Pattern.compile(REGEX_DOC_STATUS);
    
    /**
     * File id value string to be dumped into the parsed map of every document
     */
    protected String fileID = null;
    
    /**
     * FileChannel to be used for updating the shadow bif file.
     */
    protected CharBuffer fileBuffer = null;
    
    protected static Charset charset = Charset.forName("ISO-8859-15");
    
    protected static CharsetDecoder decoder = charset.newDecoder();
    
    protected RandomAccessFile raf = null;
    
    /**
     * update segment number
     */
    protected int updateDocNumber = 0;

    protected String rafLine = null;
    protected long rafPrevPos = 0;
    
    /** Creates a new instance of BIFShadowFileManager */
    public BIFShadowFileManager() {
    }

    public java.util.TreeMap parseNext()
    {
        //if reader is null, initialize it.
        if (reader == null)
        {
            try
            {
                //create a line number reader out of the file supplied
                reader = new LineNumberReader(new FileReader(file));
                lineNumber = reader.getLineNumber();
            }
            catch(FileNotFoundException fnfe)
            {
                //if (Log.LOG_ERROR) Log.error("File to parse not found! ",fnfe);
            	VWLogger.info(" File not found while parseNext in BIFShadowFileManager  ::"+fnfe.getMessage());
                return null;
            }
        }
        //create an initial invalid TreeMap object
        TreeMap map = new TreeMap();
        try
        {
            /* If line number reader did not begin yet, then check for the validity
             * of the file by looking for the "[Documents File]" and "[documents]"
             * lines.
             * If a "Count=x" line is encountered, ignore it for backwards
             * compatibility.
             */
            if (lineNumber == 0)
            {
                String line0 = reader.readLine();
                String line1 = reader.readLine();
                if (!line0.trim().equalsIgnoreCase("[Documents File]") || !line1.trim().equalsIgnoreCase("[documents]"))
                {
                    //set map's status as invalid
                    map.put("status", "invalid_file");
                    //close reader for it is of no further use.
                    reader.close();
                    //return invalid TreeMap object
                    return map;
                }
                //read a line
                line = reader.readLine();
            }
            /* If line value is null, then end of file is reached */
            if (line == null)
            {
                //set map's status to end of file
                map.put("status", "eof");
                return map;
            }
            /* The following booleans record the result of parsing lines.
             * Also, the following integer records the count of field lines
             * parsed successfully.
             * In order for the segment parsing to be considered "valid", 
             * isDocumentParsed, isFieldsParsed, isLocationParsed, isFileIDParsed, 
             * and isDocStatusParsed must all be true.
             */
            boolean isDocumentParsed = false;
            boolean isFieldsParsed = false;
            boolean isLocationParsed = false;
            boolean isCommentParsed = false;
            boolean isFileIDParsed = false;
            boolean isDocStatusParsed = false;
            /* give lineNumber its true value so an initial validity check is not
             * repeated again
             */
            lineNumber = reader.getLineNumber();
            /* Lines of a document will be now read one by one. If one of them did
             * not match its proper format as supplied in the description of this
             * class, an invalid TreeMap object is returned.
             */
            /* if the 3rd line contained the count of the docs, acknowledge for 
             * backwards compatibility, and skip to the next line.
             */
            if (lineNumber == 3)
            {
                if (line.toLowerCase().startsWith("count="))
                    line = reader.readLine();
                isFileIDParsed = parseFileIDLine(line.trim());
                if (!isFileIDParsed)
                {
                    //set map's status as invalid
                    map.put("status", "invalid_file");
                    //close reader for it is of no further use.
                    reader.close();
                    //return invalid TreeMap object
                    return map;
                }
                else
                    line = reader.readLine();
            }
            /*if line is empty, skip it.*/
            if (line.trim().equals(""))
                line = reader.readLine();
            /* insert the file id into the segment */
            map.put("file_id", this.fileID);
            /* if the document line was not parsed successfully, return an
             * invalid TreeMap object
             */
            isDocumentParsed = parseDocumentLine(map, line.trim());
            /* loop through the following lines until you hit the next document,
             * which begins with "document"
             */
            for (line = reader.readLine(); line != null; line = reader.readLine())
            {
                line =line.trim();
                /*if line is empty, skip it.*/
                if (line.trim().equals(""))
                    line = reader.readLine();
                /* if next document is reached, break */
                if (line.toUpperCase().startsWith("DOCUMENT"))
                    break;
                /* if document line was not parsed successfully, keep looping
                 * until you reach the next document
                 */
                if (!isDocumentParsed)
                    continue;
                /* test line for fields, field, location, comment, or one of the
                 * options.
                 */
                if (!isFieldsParsed)
                {
                    isFieldsParsed = parseFieldsLine(map, line);
                    if (isFieldsParsed)
                        continue;
                }
                if (!isLocationParsed)
                {
                    isLocationParsed = parseLocationLine(map, line);
                    if (isLocationParsed)
                        continue;
                }
                if (!isCommentParsed)
                {
                    isCommentParsed = parseCommentLine(map, line);
                    if (isCommentParsed)
                        continue;
                }
                if (!isDocStatusParsed)
                {
                    isDocStatusParsed = parseDocStatusLine(map, line);
                    if (isDocStatusParsed)
                        continue;
                }
                if (parseFieldLine(map, line))
                    continue;
                /* option lines may include all other lines tested, so they are
                 * tested last.
                 */
                if (parseOptionLine(map, line))
                    continue;
            }
            /* If line value is null, then end of file is reached */
            if (line == null)
            {
                reader.close();
            }
            /* if required lines were parsed successfully, set status to "valid".
             * Otherwise, "invalid_segment"
             */
            if (isDocumentParsed && isFieldsParsed && isLocationParsed && isDocStatusParsed)
                map.put("status", "valid");
            else
                map.put("status", "invalid_segment");
        }
        catch (IOException ioe)
        {
            //in case of file io, return null.
            //if (Log.LOG_ERROR) Log.error("File IO error during parsing! ",ioe);
            map = null;
            return map;
        }
        map.put("segment#", Integer.toString(segmentNumber++));
        return map;
    }
    
    /**
     * Parses data from fields line and puts it in the TreeMap object to be
     * returned in the parse() or parseNext() function.
     * Also puts the field count within the TreeMap object to be returned.
     * Returns true if it succeeds in parsing and false otherwise.
     */
    protected boolean parseFieldsLine(TreeMap map, String line)
    {
        //match line with fields line pattern
    	try{
        Matcher mFields = pFields.matcher(line);
        //if it matches, parse it. Otherwise, return invalid map.
        if (mFields.matches())
        {
            //StringTokenizer for document line
            StringTokenizer lineTokens = new StringTokenizer(line, delFields1);
            //skip key token
           String key1= lineTokens.nextToken();
            //count fields
            lineTokens = new StringTokenizer(line.substring(key1.length()+1,line.length()), delFields2);
            int fieldCount = 0;
            while (lineTokens.hasMoreTokens())
            {
                lineTokens.nextToken();
                fieldCount++;
            }
            //prefix for document number.
            String docNumberKey = "segment" + Integer.toString(segmentNumber) + ".doc_number";
            //document number extracted from name
            String docNumber = (String)(map.get(docNumberKey));
            map.put("doc" + docNumber + ".field_count", Integer.toString(fieldCount));
            lineTokens = new StringTokenizer(line, delGeneric);
            //the first token is the key
            String key = lineTokens.nextToken();
            //the second is the value
            //modified on 2004-02-18
            //check first if value is empty or not
            String fieldvalue = line.substring(key.length()+1, line.length());
            String value = fieldvalue.length()>0 ? fieldvalue : "";
            map.put(key, value);
            return true;
        }
    	}catch(Exception  e){
    		VWLogger.info("Exception while parseFieldsLine ind BifshadowFilemanager :"+e.getMessage());
    	}
        return false;
    }

    /**
     * Parses data from file id line.
     * Returns true if it succeeds in parsing and false otherwise.
     */
    protected boolean parseFileIDLine(String line)
    {
        //match line with file id line pattern
        Matcher mFileID = pFileID.matcher(line);
        //if it matches, parse it. Otherwise, return invalid map.
        if (mFileID.matches())
        {
            fileID = line;
            return true;
        }
        return false;
    }
    
    /**
     * Parses data from doc status line and puts it in the TreeMap object to be
     * returned in the parse() or parseNext() function.
     * Returns true if it succeeds in parsing and false otherwise.
     */
    protected boolean parseDocStatusLine(TreeMap map, String line)
    {
        //match line with DocStatus line pattern
        Matcher mDocStatus = pDocStatus.matcher(line);
        //if it matches, parse it. Otherwise, return invalid map.
        if (mDocStatus.matches())
        {
            //get doc number from map
            String docNumber = (String)(map.get("segment" + Integer.toString(segmentNumber) + ".doc_number"));
            //construct key prefix
            String keyPrefix = "doc" + docNumber;
            //create document status key
            String docStatusKey = keyPrefix + ".status";
            //extract document status from line
            String docStatus = line.substring(0,1);
            //create document pending cause key
            String docPendCauseKey = keyPrefix + ".pend_cause";
            //extract document pending cause from line
            String docPendCause = line.substring(1,3);
            //create document history input status key
            String docHistInputStatusKey = keyPrefix + ".hist_input_status";
            //extract document history input status
            String docHistInputStatus = line.substring(3,4);
            map.put(docStatusKey, docStatus);
            map.put(docPendCauseKey, docPendCause);
            map.put(docHistInputStatusKey, docHistInputStatus);
            return true;
        }
        return false;
    }
    

    public boolean update(java.util.TreeMap map) {
        return updateNext(map);
    }
    
    /**
     * Map entries are:
     * "file_id"
     * "field#_status"
     * "status"
     * "pend_cause"
     * "hist_input_status"
     */
    public boolean updateNext(java.util.TreeMap map) 
    {
        //Initialize file buffer if it is null.
        if (raf == null)
        {
            try
            {
                raf = new RandomAccessFile(file, "rw");
                /*
                FileChannel fc = (raf).getChannel();
                long sz = fc.size();
                fileBuffer = decoder.decode(fc.map(FileChannel.MapMode.READ_WRITE, 0, sz));
                 */
            }
            catch(FileNotFoundException fnfe)
            {
                //if (Log.LOG_ERROR) Log.error("File to update not found! ",fnfe);
                return false;
            }
        }
        try
        {
            String docNumber = (String)(map.get("doc_number"));
            seekDoc(Integer.parseInt(docNumber));
            /* If we are still in the beginning of file, check for the validity
             * of the file by looking for the "[Documents File]" and "[documents]"
             * lines.
             * If a "Count=x" line is encountered, ignore it for backwards
             * compatibility.
             */
            if (updateDocNumber == 0)
            {
                String line0 = raf.readLine();
                String line1 = raf.readLine();
                if (!line0.equalsIgnoreCase("[Documents File]") || !line1.equalsIgnoreCase("[documents]"))
                {
                    //close raf for it is of no further use.
                    raf.close();
                    //return invalid TreeMap object
                    return false;
                }
                //capture of the position of the beginning of the line to be read
                rafPrevPos = raf.getFilePointer();
                //read a line
                rafLine = raf.readLine();
                /* if rafLine begins with count, skip to the next line */
                if (rafLine.toLowerCase().startsWith("count="))
                {
                    //capture of the position of the beginning of the line to be read
                    rafPrevPos = raf.getFilePointer();
                    //read a line
                    rafLine = raf.readLine();
                    
                }
                //match line with file id line pattern
                Matcher mFileID = pFileID.matcher(rafLine);
                //if it matches, parse it. Otherwise, return invalid map.
                if (mFileID.matches())
                {
                    String fileID = (String)(map.get("file_id"));
                    long rafCurrPos = raf.getFilePointer();
                    raf.seek(rafPrevPos);
                    for (int i = 0; i < fileID.length(); i++)
                        raf.write(fileID.charAt(i));
                    raf.seek(rafCurrPos);
                    //capture of the position of the beginning of the line to be read
                    rafPrevPos = raf.getFilePointer();
                    //read a line
                    rafLine = raf.readLine();                    
                    updateDocNumber++;
                    return true;
                }
                else
                    return false;
            }
                /* Lines of a document will be now read one by one. If one of them did
                 * not match its proper format as supplied in the description of this
                 * class, false returned.
                 */
            /* The following booleans record the result of parsing lines.
             * Also, the following integer records the count of field lines
             * parsed successfully.
             * In order for the segment parsing to be considered "valid", 
             * isDocumentParsed, isFieldsParsed, isLocationParsed, isFileIDParsed, 
             * and isDocStatusParsed must all be true.
             */
            boolean isFieldsUpdated = false;
            boolean isDocStatusUpdated = false;

            /* loop through the following lines until you hit the next document,
             * which begins with "document"
             */
            for (rafPrevPos = raf.getFilePointer(), rafLine = raf.readLine(); rafLine != null; rafPrevPos = raf.getFilePointer(), rafLine = raf.readLine())
            {
                /* if next document is reached, break */
                if (rafLine.toUpperCase().startsWith("DOCUMENT"))
                    break;
                /* test line for fields, field, location, comment, or one of the
                 * options.
                 */
                if (!isFieldsUpdated)
                {
                    isFieldsUpdated = updateFieldsLine(map);
                    if (isFieldsUpdated)
                        continue;
                }
                if (!isDocStatusUpdated)
                {
                    isDocStatusUpdated = updateDocStatusLine(map);
                    if (isDocStatusUpdated)
                        continue;
                }
            }
            /* If line value is null, then end of file is reached 
            if (rafLine == null)
            {
                raf.close();
            }
            /**/
            /* if required lines were parsed successfully, set status to "valid".
             * Otherwise, "invalid_segment"
             */
            if (isFieldsUpdated && isDocStatusUpdated)
            {
                updateDocNumber++;
                return true;
            }
            else
                return false;
        }
        catch (IOException ioe)
        {
            //in case of file io, return null.
            //if (Log.LOG_ERROR) Log.error("File IO error during update! ",ioe);
            return false;
        }
    }
    
    /**
     * Parses data from fields line and puts it in the TreeMap object to be
     * returned in the parse() or parseNext() function.
     * Also puts the field count within the TreeMap object to be returned.
     * Returns true if it succeeds in parsing and false otherwise.
     */
    protected boolean updateFieldsLine(TreeMap map)
    {
        try
        {
            //if map did not contain any field values, 
            if (!map.containsKey("field1_status"))
                return true;
            //match rafLine with fields line pattern
            Matcher mFields = pFields.matcher(rafLine);
            //if it matches, parse it. Otherwise, return invalid map.
            if (mFields.matches())
            {
                long rafCurrPos = raf.getFilePointer();
                raf.seek(rafPrevPos);
                char c;
                while((c=(char)(raf.read())) != '=');
                int i = 1;
                while((c=(char)(raf.read())) != '\n')
                {
                    if (c == '~')
                        raf.write(((String)(map.get("field" + (i++) + "_status"))).charAt(0));
                }
                raf.seek(rafCurrPos);
                return true;
            }
            return false;
        }
        catch(IOException ioe)
        {
            //if (Log.LOG_ERROR) Log.error("File IO error during update of fields line! ",ioe);
            return false;
        }
    }

    /**
     * Parses data from file id line.
     * Returns true if it succeeds in parsing and false otherwise.
     *
    protected boolean updateFileIDLine(String line)
    {
        //match line with file id line pattern
        Matcher mFileID = pFileID.matcher(line);
        //if it matches, parse it. Otherwise, return invalid map.
        if (mFileID.matches())
        {
            fileID = line;
            return true;
        }
        return false;
    }
    /**/

    /**
     * Parses data from doc status line and puts it in the TreeMap object to be
     * returned in the parse() or parseNext() function.
     * Returns true if it succeeds in parsing and false otherwise.
     */
    protected boolean updateDocStatusLine(TreeMap map)
    {
        try
        {
            //match line with DocStatus line pattern
            Matcher mDocStatus = pDocStatus.matcher(rafLine);
            //if it matches, parse it. Otherwise, return invalid map.
            if (mDocStatus.matches())
            {
                long rafCurrPos = raf.getFilePointer();
                raf.seek(rafPrevPos);
                //create document status key
                String docStatusKey = "status";
                //extract document status from line
                String docStatus = (String)(map.get(docStatusKey));
                if (docStatus == null)
                    return false;
                //create document pending cause key
                String docPendCauseKey = "pend_cause";
                //extract document pending cause from line
                String docPendCause = (String)(map.get(docPendCauseKey));
                if (docPendCause == null)
                    docPendCause = "00";
                //create document history input status key
                String docHistInputStatusKey = "hist_input_status";
                //extract document history input status
                String docHistInputStatus = (String)(map.get(docHistInputStatusKey));
                if (docHistInputStatus == null)
                    docHistInputStatus = "0";
                String docStatusString = docStatus + docPendCause + docHistInputStatus;
                for (int i = 0; i < docStatusString.length(); i++)
                    raf.write(docStatusString.charAt(i));
                raf.seek(rafCurrPos);
                return true;
            }
            return false;
        }
        catch(IOException ioe)
        {
            //if (Log.LOG_ERROR) Log.error("File IO error during update of document status line! ",ioe);
            return false;
        }
    }
    
    public boolean seekDoc(int docNumber)
    {
        try
        {
            //if doc number is 0 then go back to beginning of file
            if (docNumber == 0)
            {
                raf.seek(0);
                return true;
            }
            if (rafLine == null)
            {
                raf.seek(0);
                rafPrevPos = raf.getFilePointer();
                rafLine = raf.readLine();
            }
            while(rafLine != null)
            {
                if (rafLine.toLowerCase().startsWith("document" + docNumber))
                    break;
                rafPrevPos = raf.getFilePointer();
                rafLine = raf.readLine();
            }
            if (rafLine == null)
                return false;
            updateDocNumber = docNumber;
            return true;
        }
        catch(IOException ioe)
        {
        	VWLogger.info("Exception while seekDoc : "+ioe.getMessage());
            //if (Log.LOG_ERROR) Log.error("File IO error during seeking of documents! ",ioe);
            return false;
        }
    }
    
    public void setFile(java.io.File file) throws FileNotFoundException
    {
        //set the parsing file to the file supplied
        this.file = file;
        try
        {
            close();
        }
        catch(IOException ioe)
        {
            //if (Log.LOG_ERROR) Log.error("File IO error during closing of files to parse or update! ",ioe);
        }
    }
    
    public void close() throws IOException
    {
        if (reader != null)
            reader.close();
        if (raf != null)
            raf.close();
        reader = null;
        raf = null;
    }

    public static void main(String[] args) throws Exception
    {
        /*testing parsing capabilities
        BIFFileParser p = new BIFShadowFileManager();
        p.setFile(new java.io.File("f:\\output\\1.bif"));
        TreeMap tm = p.parseNext();
        Set s = tm.keySet();
        Object[] so = s.toArray();
        Collection c = tm.values();
        Object[] co = c.toArray();
        System.out.println(tm.get("status"));
        p.parseNext();
        System.out.println("finished");
         */
        /*testing updating capabilities*/
        /*
        BIFFileParser p = new BIFShadowFileManager();
        p.setFile(new java.io.File("f:\\output\\1.bif"));
        TreeMap tm = p.parseNext();
        Set s = tm.keySet();
        Object[] so = s.toArray();
        Collection c = tm.values();
        Object[] co = c.toArray();
        System.out.println(tm.get("status"));
        p.parseNext();
        System.out.println("finished");
         */
    }    
        
}