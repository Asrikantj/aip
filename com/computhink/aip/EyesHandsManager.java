package com.computhink.aip;

import java.util.*;
import java.text.*;
import java.io.File;
import com.computhink.aip.util.AIPUtil;
import com.computhink.aip.util.VWLogger;
import com.computhink.aip.util.LocationStatus;



import javax.swing.JComboBox;
import java.awt.event.ActionEvent;

import com.computhink.common.Document;
import com.computhink.common.DocType;
import com.computhink.common.Index;
import com.computhink.aip.client.*;
/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class EyesHandsManager {
  
  private static Hashtable initsMap =new Hashtable();
  private static Hashtable formLocationsLastModified =new Hashtable();
  public static Date FormsFileLastModified  ;
  public static boolean processSubdirs =false;
  public static boolean isProcessing;
  public static boolean tempDisabled = true;
  public static boolean usejHandlePages =false;
  
    
  public EyesHandsManager() {

  }
  public static void releaseSession(int sid)
  {
     initsMap.remove(new Integer(sid));  
  }  
//----------------------------------------AIPInit Methods-------------------------------
  public static void setLastModifiedFormLocation(String location,Date modified)
  {
        formLocationsLastModified.put(location,modified);
  }    
  public static Date getLastModifiedFormLocation(String location)
  {
      if(formLocationsLastModified.containsKey(location))
          return (Date)formLocationsLastModified.get(location);
      return null;
  }    
  public static void saveInitParams(int sid,String ipaddress)
  {
        AIPInit iInfo =(AIPInit)initsMap.get(new Integer(sid));
        if(iInfo != null)
        {
            AIPManager.getVWC().setAIPInit(sid,iInfo,ipaddress);
        }    
        
   } 
  public static void saveInitParams(int sid)
  {
        AIPInit iInfo =(AIPInit)initsMap.get(new Integer(sid));
        if(iInfo != null)
        {
            AIPManager.getVWC().setAIPInit(sid,iInfo);
        }    
        
   }    
   public static AIPInit getInitParams(int sid)
   {
	   AIPInit iInfo =(AIPInit)initsMap.get(new Integer(sid));
	   try{
		   if(iInfo == null)
		   {
			   iInfo =new AIPInit();
			   int status = AIPManager.getVWC().getAIPInit(sid,iInfo);
			   if(status !=AIPManager.NOERROR)
				   return null;
			   else initsMap.put(new Integer(sid),iInfo);
		   } 

	   }catch(Exception e){
		   VWLogger.info("Exception while getInitParams ::"+e.getMessage());
	   }
	   return iInfo;
    }  
    public static AIPInit getCurrent()
    {
        return getInitParams(AIPManager.getCurrSid());
    }    
 //------------------------------------------EHForms Actions & Methode-----------------------------------

  public boolean RefreshDBFormsWithFileForms(Vector DBForms,TreeMap FileForms,int sid)
  {      
      Vector foundDBForms =new Vector();      
      boolean Updated =false;
     if(FileForms.size() >0)
     {
        Iterator iterator = FileForms.keySet().iterator();
        while (iterator.hasNext())
        {
            String FormFName =(String)iterator.next();
            AIPForm FInfo= (AIPForm)FileForms.get(FormFName);
            boolean found = false;
            VWLogger.debug("CHeck Update File Form <"+FInfo.getName()+">");
            for(int i=0;i<DBForms.size();i++)
            {
                AIPForm DBInfo=(AIPForm)DBForms.get(i);
                if(FormFName.equalsIgnoreCase(DBInfo.getName()))
                {
                    int LastDBStatus =DBInfo.getStatus();
                    boolean LastDBExsits =DBInfo.isExsits();
                    VWLogger.debug("Compare With DB Form <"+DBInfo.getName()+">"+":LastStatus :"+LastDBStatus+":missing:"+LastDBExsits);
                    DBInfo.setStatus(MapFileFormFieldsWithDBFormFields(DBInfo,FInfo,sid));
                    DBInfo.setExsits(true);

                   if( LastDBStatus == DBInfo.FIELDSNEEDMAP) DBInfo.setStatus(DBInfo.FIELDSNEEDMAP);
                   else if( DBInfo.getStatus() == DBInfo.FIELDSNEEDMAP)
                   {
                         if(!Updated) Updated =true;
                   }
                   if( LastDBStatus != DBInfo.getStatus() || !LastDBExsits )
                   {                   
                      AIPManager.getVWC().setAIPForm(sid,DBInfo);
                   }
                   found =true;
                   foundDBForms.add(DBInfo);                   
                   break;
                }
            }
            if(!found){

              VWLogger.debug("File Form <"+FInfo.getName()+"> is new,And will be added to DB ");
               if(AIPManager.getVWC().setAIPForm(sid,FInfo) >0){
                  AIPManager.getVWC().setAIPFormFields(sid,FInfo);
                  foundDBForms.add(FInfo);
                  FInfo.setExsits(true);                  
               }
               DBForms.add(FInfo);
               Updated =true;
            }
        }   
     }
     //-search for deleted Forms
       VWLogger.debug("Search for missing Form in File ");
     for(int j=0;j<DBForms.size();j++)
     {
               AIPForm dbFInfo =(AIPForm)DBForms.get(j);
               if(foundDBForms.contains(dbFInfo))
               {
                   foundDBForms.remove(dbFInfo);
               }
               else
               {                
                    if(dbFInfo.isExsits())
                    {
                      dbFInfo.setExsits(false);
                      AIPManager.getVWC().setAIPForm(sid,dbFInfo);
                     
                    }
               }
      }
      return Updated ;
  }
  public int MapFileFormFieldsWithDBFormFields(AIPForm DBFInfo,AIPForm FFInfo,int sid)
  {
          int Status =AIPForm.NORMAL;
          Vector foundDBFields =new Vector();
          Vector DBFields =DBFInfo.getFields();
          Vector FFields =FFInfo.getFields();
          for(int i=0;i<FFields.size();i++)
          {
                AIPField FFldInfo =(AIPField)FFields.get(i);
                boolean find =false;
                //if(Log.LOG_VERBOSE_DEBUG) Log.debug("Check Map File Form Field  <"+FFldInfo.getEHFieldName()+">");
                for(int j=0;j<DBFields.size();j++)
                {
                    AIPField DBFldInfo =(AIPField)DBFields.get(j);

                    if(DBFldInfo.getName().equalsIgnoreCase(FFldInfo.getName()))
                    {
                        find =true;
                        boolean fieldUpdate =false;
                        //if(Log.LOG_VERBOSE_DEBUG) Log.debug("Compare with DB Form Field  <"+DBFldInfo.getEHFieldName()+">");
                        if(DBFldInfo.getType() != FFldInfo.getType())
                        {
                             //if(Log.LOG_DEBUG) Log.debug(" Type change in file Form Field  <"+DBFldInfo.getEHFieldName()+">");
                             int LastType = DBFldInfo.getType();
                             DBFldInfo.setType(FFldInfo.getType());
                             fieldUpdate =true;
                             
                             
                             if(DBFInfo.getMapDTId() > 0)
                               Status =AIPForm.UPDATE;
                               
                             
                             
                        }
                        if(DBFldInfo.getOrder() != FFldInfo.getOrder())
                        {
                            DBFldInfo.setOrder(FFldInfo.getOrder());
                            fieldUpdate =true;
                        }
                        foundDBFields.add(DBFldInfo);
                        if(fieldUpdate)
                            AIPManager.getVWC().setAIPFormField(sid,DBFInfo.getId(),DBFldInfo);
                        
                        break;
                    }    
                }              
                if(!find) //if not found add to the table Form fields
                {
                       AIPManager.getVWC().setAIPFormField(sid,DBFInfo.getId(),FFldInfo);
                       foundDBFields.add(FFldInfo);
                       DBFields.addElement(FFldInfo);
                       if(DBFInfo.getMapDTId() > 0 && Status !=AIPForm.FIELDSNEEDMAP) Status = AIPForm.UPDATE;                 
                }     
          }
          //-Check Deleted Fields in file and save in DB
          //if(Log.LOG_VERBOSE_DEBUG) Log.debug("Check Missing Fields in the File Form Fields");
          for(int j= DBFields.size()-1;j>=0;j--)
          {
              AIPField DBFldInfo =(AIPField)DBFields.get(j);
              if(!foundDBFields.contains(DBFldInfo))
              {
                  if(DBFldInfo.getMapIndexId() > 0)
                      Status =AIPForm.FIELDSNEEDMAP;                    
                  else  if(DBFInfo.getMapDTId() > 0 && Status != AIPForm.FIELDSNEEDMAP) Status = AIPForm.UPDATE;
                  AIPManager.getVWC().deleteAIPFormField(sid,DBFldInfo);
                  DBFields.remove(j);
              }
          }
          //if(Log.LOG_DEBUG) Log.debug(" Form Fields Status is <"+Status+"> ");
          return Status;
  }
 
//-------------------------------------------EHDocument Actions & Methode------------------------------------
   public Document TransferAIPDocToVWDoc(LocationStatus locStatus,AIPDocument aipDoc,AIPForm FormInfo,DocType dt )
 {
		Document DocInfo = new Document();
		/**CV2019 merges from CV10.2 line. try catch block added**/
		try {
			Vector DTIndices = dt.getIndices();
			boolean Accepted = true;
			// if(Log.LOG_DEBUG) Log.debug("Transfer AIPDocument To VWDocument
			// ");
			int ParentNodeId = FormInfo.getDesNodeId();

			DocInfo.setDocType(new DocType(FormInfo.getMapDTId()));
			Vector FormFields = FormInfo.getFields();

			Vector aipDocFields = aipDoc.getAIPForm().getFields();
			if (aipDocFields != null) {
				for (int i = 0; i < aipDocFields.size(); i++) {
					((AIPField) aipDocFields.get(i)).setStatus(AIPField.ACCEPTED);
				}
			}
			Vector newDocIndices = new Vector();
			try {
				for (int i = 0; i < DTIndices.size(); i++) {
					Index dtIndex = (Index) DTIndices.get(i);
					int Indexid = dtIndex.getId();
					String value = " ";
					Index docIndex = new Index(Indexid);
					AIPField MapFInfo = getMapFieldInfo(FormFields, Indexid);
					if (MapFInfo == null) {
						value = (String) dtIndex.getDef().get(0);
					} else if (MapFInfo.getId() <= 0) {
						value = MapFInfo.getFDefault();
					} else if (MapFInfo.getId() > 0) {
						int FieldOrder = MapFInfo.getOrder();
						if (FieldOrder > -1) {
							AIPField docField = getAIPDocField(FieldOrder, aipDocFields);
							if (docField != null) {
								docField.setStatus(AIPField.ACCEPTED);
								value = docField.getValue();
							}
							// ayman to be see
							// if(dtIndex.getSvid() == 1 &&
							// dtIndex.getIndexType()
							// == 2){
							// value
							// =AIPUtil.SetDateMaskedValue(AIPUtil.MatchVWMask(dtIndex.getIndexMask()),new
							// Date());
							// }else
							if (value.equals(" ") || value.equals("") || value == null)
								value = MapFInfo.getFDefault();
							else {
								value = CheckAcceptableOfDocIndexValue(value, dtIndex);
								if (value == null) {
									if (docField != null)
										docField.setStatus(AIPField.REJECTED);

									if (Accepted)
										Accepted = false;
								}

								if (Accepted && dtIndex.isUnique() && !value.equals(" ")) {
									int ret = AIPManager.getVWC().violatesUnique(AIPManager.getCurrSid(), dt,
											new Index(Indexid), value);
									if (ret != 1)
										if (docField != null)
											docField.setStatus(AIPField.REJECTED);

									if (Accepted)
										Accepted = false;
								}
							}
							// System.out.println("After
							// Check:"+DTIndicesInfo.getIndexName()+":"+FieldOrder+":"+value);
						} else {
							value = MapFInfo.getFDefault();
							if (value.equals(" ") || value == null)
								value = (String) dtIndex.getDef().get(0);

						}
					}
					docIndex.setValue(value);
					docIndex.setActValue(value);
					newDocIndices.addElement(docIndex);
				}
				DocInfo.getDocType().setIndices(newDocIndices);
				DocInfo.setName(aipDoc.getName());
			} catch (Exception e) {
				VWLogger.info("Exception while setting setIndices and setName inside TransferAIPDocToVWDoc method .... " + e.getMessage());
			}
			if (!Accepted) {
				aipDoc.setStatus(aipDoc.PENDING);
				aipDoc.setPendCause(6);
				return null;
			}
			
			// added by amaleh on 2004-01-30
			// if tree node id is 0, then parse tree node path from form and
			// assign it dynamically to both EHDocInfo and DocInfo.
			if (ParentNodeId == 0) {
				try {
					String ParentNodePath;
					if (aipDoc.getVWNodePath() != null && !aipDoc.getVWNodePath().trim().equals(""))
						ParentNodePath = aipDoc.getVWNodePath();
					else
						ParentNodePath = FormInfo.getDesNodePath();
					if (DocumentsManager.calcDepth(aipDoc.getVWNodePath()) >= 4) {
						String parsedPath = parseDynamicVWNodePath(AIPManager.getCurrSid(), ParentNodePath, newDocIndices);
						if (parsedPath == null || parsedPath.equals("")) {
							ParentNodeId = 0;
							aipDoc.setVWNodePath("");
						} else {
							aipDoc.setVWNodePath(parsedPath);
							ParentNodeId = DocumentsManager.getIdOfNodePath(locStatus, parsedPath, true);												
						}
					} else {
						VWLogger.info("Before calling getAIPDocumentLocation method .....");
						String nodeID = getAIPDocumentLocation(AIPManager.getCurrSid(), aipDoc, locStatus, false);
						if (nodeID == null)
							return null;
						ParentNodeId = Integer.parseInt(nodeID);
					}
					if (ParentNodeId <= 0) {
						VWLogger.error("Invalied Parent node id ", null);
						aipDoc.setStatus(aipDoc.PENDING);
						aipDoc.setPendCause(7);
						// aipDoc.setVWNodePath(null);
						return null;
					}
				} catch (Exception e) {
					VWLogger.info("Exception while setting ParentNodeId in TransferAIPDocToVWDoc " + e.getMessage());
				}
			}
			//

			// moved by amaleh on 2004-01-30
			DocInfo.setParentId(ParentNodeId);
			// if(Log.LOG_DEBUG) Log.debug("The Transfer Operation Succeed");
		} catch (Exception e) {
			VWLogger.info("Exception in TransferAIPDocToVWDoc method.... " + e.getMessage());
		}
		return DocInfo;
	}
  /**/
   /* added by amaleh on 2003-05-30 */
   // transfer documents without using templates
  //update ayman
   public Document TransferAIPDocToVWDoc(int sid,LocationStatus locStatus,AIPDocument aipDoc)
   {            
            boolean Accepted =true;
            
            int ParentNodeId =0;
          
            
            Document DocInfo =new Document();  
            /**CV2019 merges from CV10.2 line. try catch block added**/
            try {
            
            
				DoctypesManager dtManager = new DoctypesManager();
				DocType dt = dtManager.getDoctype(sid, aipDoc.getAIPForm().getName());
				if (dt == null) {
					aipDoc.setStatus(aipDoc.PENDING);
					aipDoc.setPendCause(8);
					return null;
				}
				DocInfo.setDocType(new DocType(dt.getId(), dt.getName()));
				Vector newDocIndices = new Vector();
				Vector DocFieldsVector = aipDoc.getAIPForm().getFields();
				AIPField currentAIPDocField;
				Vector dtIndices = dt.getIndices();
	
				// first get index names in the proper order from the doctype
				Vector indexNames = new Vector();
				Vector indexOrder = new Vector();
				String indexNamesString = aipDoc.getFieldNames();
				// VWLogger.info("indexNamesString......"+indexNamesString);
				try {
					StringTokenizer indexNamesTokens = new StringTokenizer(indexNamesString, "|");
					while (indexNamesTokens.hasMoreTokens())
						indexNames.addElement(indexNamesTokens.nextToken());
					indexOrder = getOrderTransition(dtIndices, indexNames);
					/* added by amaleh on 2003-08-18 */
					boolean indexNameRejected = false;
					// VWLogger.info("indexOrder.lastElement()......"+indexOrder.lastElement());
					boolean[] acceptedArray = (boolean[]) (indexOrder.lastElement());
					for (int i = 0; i < DocFieldsVector.size(); i++) {
						currentAIPDocField = (AIPField) DocFieldsVector.get(i);
						if (acceptedArray[i] == false) {
							currentAIPDocField.setStatus(AIPField.REJECTED);
							indexNameRejected = true;
						} else
							currentAIPDocField.setStatus(AIPField.ACCEPTED);
					}
					// VWLogger.info("indexNameRejected......"+indexNameRejected);
					if (indexNameRejected) {
						aipDoc.setStatus(aipDoc.PENDING);
						aipDoc.setPendCause(9);
						return null;
					}
	            } catch (Exception e) {
					VWLogger.info("Exception in TransferAIPDocToVWDoc getOrderTransition " + e.getMessage());
				}
				/*
				 * Enhancemnt:- Unique to room Code added to avoid to avoid
				 * duplication of Sequence value for unique to room set. Pendcause
				 * 17 Unique sequence violation.
				 */
				try {
					Vector returnVect = new Vector();
					// VWLogger.info("EyesHandsManager dt.getId()::: "+dt.getId());
					AIPManager.getVWC().checkValidateUniqueSeq(sid, dt.getId(), returnVect);
					if (returnVect != null && returnVect.size() > 0) {
						// VWLogger.info("EyesHands Manager
						// returnVect.get(0).toString() ::: "+
						// returnVect.get(0).toString());
						if (!returnVect.get(0).toString().equalsIgnoreCase("0")) {
							aipDoc.setStatus(aipDoc.PENDING);
							aipDoc.setPendCause(17);
							return null;
						}
	
					}
				} catch (Exception e) {
					VWLogger.info("Exception inside checkValidateUniqueSeq" + e.getMessage());
				}
				try {
					for (int i = 0; i < dtIndices.size(); i++) {
						Index dtIndex = (Index) (dtIndices.get(i));
						int Indexid = dtIndex.getId();
						Index currentIndexInfo = new Index(dtIndex.getId());
						String value = "";
						int order = ((Integer) (indexOrder.elementAt(i))).intValue();
						if (order == -1) {
							if (dtIndex.getDef().size() > 0)
								value = (String) dtIndex.getDef().get(0);
							if (dtIndex.isRequired() && value.trim().equals("")) {
								Accepted = false;
							} else {
								currentIndexInfo.setValue(value);
								currentIndexInfo.setActValue(dtIndex.getActDef());
								newDocIndices.add(currentIndexInfo);
							}
						} else {
							currentAIPDocField = (AIPField) DocFieldsVector.get(order);
							if (currentAIPDocField != null) {
								currentAIPDocField.setStatus(AIPField.ACCEPTED);
								value = currentAIPDocField.getValue();
							}
							/* check if value is required or key */
							String newValue = null;
							String actValue = value;
							if (value.trim().equals("")) {
								newValue = "";
								if (dtIndex.isRequired()) {
									// VWLogger.info("it is required field and checking
									// for the default value");
									if (dtIndex.getDefaultValue().trim().length() > 0) {
										newValue = dtIndex.getDefaultValue();
									} else {
										currentAIPDocField.setStatus(AIPField.REJECTED);
										Accepted = false;
									}
								}
							} else {
		
								newValue = CheckAcceptableOfDocIndexValue(value, dtIndex);
							}
		
							if (newValue == null) {
								currentAIPDocField.setStatus(AIPField.REJECTED);
								Accepted = false;
							} else {
								value = newValue;
								currentAIPDocField.setStatus(AIPField.ACCEPTED);
								actValue = CreateActIndexValue(newValue, dtIndex);
							}
							// int indexUnique =
							// Integer.parseInt(String.valueOf(DTIndicesInfo.getInfo().charAt(1)))
							/*
							 * Enhancemnt:- Unique to room Code added to avoid to avoid
							 * duplication of Sequence value for unique to room set.
							 * Pendcause 17 Unique sequence violation. below code
							 * commented by srikanth as we should pass only document
							 * type id for checkValidateUniqueSeq procedure
							 */
		
							/*
							 * try{ Vector returnVect=new Vector();
							 * AIPManager.getVWC().checkValidateUniqueSeq(sid,dtIndex.
							 * getId(),returnVect); if (returnVect != null &&
							 * returnVect.size() > 0){
							 * if(!returnVect.get(0).toString().equalsIgnoreCase("0")){
							 * aipDoc.setStatus(aipDoc.PENDING);
							 * aipDoc.setPendCause(17); return null; }
							 * 
							 * } } catch(Exception e){ VWLogger.info(
							 * "Exception in checkValidateUniqueSeq"+e.getMessage()); }
							 */
		
							if (dtIndex.getType() == 5) {
								if (Accepted && dtIndex.isUnique() && !value.equals(" ")) {
									int ret = AIPManager.getVWC().violatesUnique(sid, dt, dtIndex, value);
									/*
									 * ViolatesUnique method will return False if the
									 * IndexValue not exists in DB (that means you can
									 * create document with that value)
									 */
									if (ret != 0) {
										currentAIPDocField.setStatus(AIPField.REJECTED);
										if (Accepted)
											Accepted = false;
									}
								}
							} else if (Accepted && dtIndex.isUnique() && !value.equals(" ")) {
								int ret = AIPManager.getVWC().violatesUnique(sid, dt, dtIndex, value);
								// VWLogger.info("EyesHandsManager ret::: "+ret);
								// if(ret !=1) //commented by srikanth as it should
								// check whether
								if (ret != 0) // First check for violatesUnique property
												// if true then CHECKING FOR ISAPPEND
												// FLAG AND SETTING AIPFIELD TO ACCEPT /
												// REJECT CODE MODIFIED BY SRIKANTH
								{
									// VWLogger.info("EyesHandsManager
									// aipDoc.isAppend()::: "+aipDoc.isAppend());
									if (aipDoc.isAppend() == false) {
										currentAIPDocField.setStatus(AIPField.REJECTED);
										if (Accepted)
											Accepted = false;
										aipDoc.setStatus(aipDoc.PENDING);
										aipDoc.setPendCause(18);// newly added pendcause
																// unique violation
										return null;
									} else { // When Unique violation happens and if
												// append is set to true then it should
												// process the document added by
												// Srikanth on 20 Nov 2015
										currentAIPDocField.setStatus(AIPField.ACCEPTED);
										// VWLogger.info("EyesHandsManager Accepte true
										// srikanth ::: ");
										Accepted = true;
									}
								}
		
							}
							/**/
							// add value seen
							currentIndexInfo.setValue(value);
							// add actual value used in db
							if (currentAIPDocField.getStatus() == AIPField.ACCEPTED)
								currentIndexInfo.setActValue(actValue);
							else
								currentIndexInfo.setValue(value);
							newDocIndices.add(currentIndexInfo);
						}
		
					}
					DocInfo.getDocType().setIndices(newDocIndices);
					DocInfo.setName(aipDoc.getName());
					// VWLogger.info("EyesHandsManager aipDoc.isAppend():::
					// "+aipDoc.isAppend());
	            } catch (Exception e) {
		        	VWLogger.info("Exception while setting setIndices and setName inside TransferAIPDocToVWDoc method .... "+e.getMessage());
	            }
	
				if (Accepted == false) {
					aipDoc.setStatus(aipDoc.PENDING);
					aipDoc.setPendCause(10);
					return null;
				}
				boolean folderSearch = false;
				if (DocumentsManager.calcDepth(aipDoc.getVWNodePath()) >= 4) {
					String parsedPath = parseDynamicVWNodePath(sid, aipDoc.getVWNodePath(), newDocIndices);
					VWLogger.info("parsedPath >>>>>>>>>> "+parsedPath);
					aipDoc.setVWNodePath(parsedPath);
					/*
					 * Issue - AIP Hangs when processing large rooms Developer -
					 * Nebu Alex Description - This function works same as the
					 * function getIdOfNodePath. The diffrence is in the
					 * implementation logic. Instead of loading all nodes in to
					 * memory and obtain node information from a vector, this
					 * function makes a data base call when node information is
					 * required.
					 */
					// ParentNodeId
					// =DocumentsManager.getIdOfNodePath(locStatus,aipDoc.getVWNodePath(),
					// aipDoc.isCreateVWNode());
					ParentNodeId = DocumentsManager.getparentNodeIdofNodePath(locStatus, aipDoc.getVWNodePath(),
							aipDoc.isCreateVWNode());
					// End of fix
				} else {
					ParentNodeId = 0;
					folderSearch = true;
				}
				VWLogger.info("folderSearch >>>>>>>>>> "+folderSearch);
				if (folderSearch) {
					VWLogger.info("Before calling getAIPDocumentLocation method .....");
					String nodeID = getAIPDocumentLocation(sid, aipDoc, locStatus, true);
					if (nodeID == null)
						return null;
					ParentNodeId = Integer.parseInt(nodeID);
				}
				// VWLogger.info("Parent node id "+ParentNodeId);
				// if(ParentNodeId <= 0) ParentNodeId =
				// getInitParams(sid).getDefaultRoomNodeId() ;
				// DocInfo.setParentId(ParentNodeId);
				if (ParentNodeId <= 0) {
					// VWLogger.error("Invalied Parent node id ",null);
					aipDoc.setStatus(aipDoc.PENDING);
					aipDoc.setPendCause(7);
					return null;
				}
				DocInfo.setParentId(ParentNodeId);     
            } catch (Exception e) {
     		   VWLogger.info("Exception in TransferAIPDocToVWDoc method.... "+e.getMessage());
     	    }
            return DocInfo;
  }        
  
  public static void deleteImageFiles(AIPDocument aipDoc)
  {
	  try{
		  /**
		   * Issue Fixed: "delete_images = True" is not deleting images when page path contains wild characters.
		   */
		  VWLogger.debug("Deleting Images from <"+aipDoc.getImagesPath()+">");
		  File[] imageArray = PagesManager.parseImageFiles(aipDoc.getImagesPath());
		  if(imageArray != null && imageArray.length > 0){
			  for(int i=0; i<imageArray.length; i++){
				  imageArray[i].delete();
				  VWLogger.debug(imageArray[i]+" is deleted");
			  }
		  }
	  }catch(Exception e){
		  VWLogger.error("Exception while deleting the Images ", e);
	  }
  }
  
  /**
   * This Method used to delete the image files and folders when delete_image flag is true. 
   * @param aipDoc
   * @param processLoction
   */
  public static void deleteImageFiles(AIPDocument aipDoc, String processLoction)
  {
	  try{
		  String imageFolderPath = new File(aipDoc.getImagesPath()).getParent();
		  if (imageFolderPath.startsWith("\"")) {
			  imageFolderPath = imageFolderPath.substring(1, imageFolderPath.length());
		  }
		  VWLogger.debug("Deleting Images from <"+aipDoc.getImagesPath()+">");
		  File[] imageArray = PagesManager.parseImageFiles(aipDoc.getImagesPath());
		  if(imageArray != null && imageArray.length > 0){
			  for(int i=0; i<imageArray.length; i++){
				  imageArray[i].delete();
				  VWLogger.debug(imageArray[i]+" is deleted");
			  }
		  }
		  VWLogger.debug("image folder :"+imageFolderPath);
		  VWLogger.debug("processLoction :"+processLoction);
		  /**processLoction will be not null from General tab document process. It will be null from Pending tab document process **/
		  if ((processLoction == null) || (!imageFolderPath.equalsIgnoreCase(processLoction))) {
			  File parentFolder = new File(imageFolderPath);
			  VWLogger.debug("parentFolder.isDirectory() :"+parentFolder.isDirectory());
			  VWLogger.debug("parentFolder.list() :"+parentFolder.listFiles());
			  if (parentFolder.listFiles() == null || (parentFolder.listFiles() != null && parentFolder.listFiles().length == 0)) {
				  boolean flag = parentFolder.delete();
				  VWLogger.debug("is folder deleted :"+flag);
			  }
		  }
	  }catch(Exception e){
		  VWLogger.error("Exception while deleting the Images :", e);
	  }
  }
   
  
  public static void deleteCommentFile(AIPDocument aipDoc)
  {
      StringTokenizer imageTokens = new StringTokenizer(aipDoc.getCommentFile(), "\"");
      while(imageTokens.hasMoreTokens())
      {
          String currentPath = imageTokens.nextToken();
          File commentFile = new File(currentPath);
          commentFile.delete();
      }
  } 

  /* added by amaleh on 2003-08-15 */
  /**
   * returns an order transition array that indicates how to reorder the second
   * vector so it can have the order of elements in the first.
   */
  private Vector getOrderTransition(Vector vectorInOrder, Vector vector)
 {
		Vector order = new Vector();
		/**CV2019 merges from CV10.2 line. Try catch block added**/
		try {
			if (vectorInOrder == null || vector == null)
				return null;

			int elements = 0;
			// added on 2003-09-24
			boolean[] acceptedArray = new boolean[vector.size()];
			for (int i = 0; i < vectorInOrder.size(); i++) {
				String currentElement1 = vectorInOrder.elementAt(i).toString();
				int j;
				for (j = 0; j < vector.size(); j++) {
					String currentElement2 = (String) (vector.elementAt(j));
					// VWLogger.info("currentElement2 :::"+currentElement2);
					// VWLogger.info("currentElement1 :::"+currentElement1);
					currentElement1 = doTrimFieldValuesSpaces(currentElement1);
					currentElement2 = doTrimFieldValuesSpaces(currentElement2);
					if (currentElement1.equals(currentElement2)) {
						order.addElement(new Integer(j));
						acceptedArray[j] = true;
						elements++;
						break;
					}
				}
				if (j == vector.size())
					order.addElement(new Integer(-1));
			}
			order.addElement(acceptedArray);
		} catch (Exception e) {
			VWLogger.info("Exception while getOrderTransition :" + e.getMessage());
		}
		return order;
	}   
  /**
   * This method is used to trim the left and right spaces in fieldValues
   * @param fieldValues
   * @return
   */
  private String doTrimFieldValuesSpaces(String fieldValue) {
	  fieldValue = fieldValue.replaceAll("^\\s+","");
	  fieldValue = fieldValue.replaceAll("\\s+$","");
	  fieldValue = fieldValue.replaceAll("\\t","");
  	return fieldValue;
  }
  //added by amaleh on 2004-02-12
  /** parses dynamic VW Tree Node Paths */
  public static String parseDynamicVWNodePath(int sid,String dynamicVWNodePath, Vector indices)
 {
		StringTokenizer tokens = new StringTokenizer(dynamicVWNodePath, "/");
		StringBuffer pathBuffer = new StringBuffer();
		DoctypesManager DTManager = new DoctypesManager();
		while (tokens.hasMoreTokens()) {
			/**CV2019 merges from CV10.2 line. Try catch block added**/
			try {
				String token = tokens.nextToken();
				if (token.startsWith("<") && token.endsWith(">")) {
					boolean valueSet = false;
					// try to parse value from indices
					String trimmedToken = token.substring(1, token.length() - 1);
					for (int i = 0; i < indices.size(); i++) {
						Index idx = (Index) indices.get(i);
						int currentID = idx.getId();
						int trimmedTokenID = DTManager.getIDofIndex(sid, trimmedToken);
						if (trimmedTokenID == currentID) {
							String currentValue = idx.getValue();
							pathBuffer.append(currentValue.concat("/"));
							valueSet = true;
						}
					}
					// try to parse value from date
					if (!valueSet) {
						try {
							SimpleDateFormat format = new SimpleDateFormat(trimmedToken);
							String dateString = format.format(Calendar.getInstance().getTime());
							if (dateString != null) {
								pathBuffer.append(dateString.concat("/"));
								valueSet = true;
							}
						} catch (IllegalArgumentException iae) {
							// if (Log.LOG_DEBUG) Log.debug("Date format in
							// index value not accepted. Index value will pass
							// without parsing.", iae);
						}
					}
					if (!valueSet)
						pathBuffer.append(token.concat("/"));
				} else {
					pathBuffer.append(token.concat("/"));
				}
			} catch (Exception e) {
				VWLogger.info("Exception in parseDynamicVWNodePath ......" + e.getMessage());
			}
		}
		if (pathBuffer.length() == 0)
			return null;
		String path = pathBuffer.substring(0, pathBuffer.length() - 1);
		return path;
	}
  
    /**CV2019 merges from CV10.2 line**/
	public String CheckAcceptableOfDocIndexValue(String value, Index index) {
		try {
			if (index.getType() == 0 || index.getType() == 5)
				return value;
			if (index.getType() == 1) {
				return getAcceptNumericValue(value, index);
			} else if (index.getType() == 2) {
				String Mask = AIPUtil.MatchVWMask(index.getMask());
				Date DateValue = AIPUtil.getDate(Mask, value);
				if (DateValue == null)
					return null;
				else
					return value;
			} else if (index.getType() == 3) {
				if (value.equalsIgnoreCase("Yes")) {
					value = "Yes";
				} else if (value.equalsIgnoreCase("No")) {
					value = "No";
				} else {
					try {
						int intvalue = Integer.parseInt(value.trim());
						if (intvalue <= 0)
							value = "No";
						else if (intvalue > 0)
							value = "Yes";
					} catch (Exception e) {
						return null;
					}
					return value;
				}
			} else if (index.getType() == 4) {

				Vector SelectionVal = index.getDef();
				for (int i = 0; i < SelectionVal.size(); i++) {

					String select = ((String) SelectionVal.get(i)).trim();
					if (select.equalsIgnoreCase(value)) {
						value = select;
						return value;
					}
				}
				return null;
			}
		} catch (Exception e) {
			VWLogger.info("Exception in CheckAcceptableOfDocIndexValue ......" + e.getMessage());
		}
		return value;

	}
  
	/**CV2019 merges from CV10.2 line**/
	public String CreateActIndexValue(String value, Index indx) {
		String actValue = value;
		try {
			if (indx.getType() == 1) {
				if (value.startsWith("$"))
					actValue = value.substring(1);
				else if (value.endsWith("%"))
					actValue = value.substring(0, value.length() - 1);
				else if (value.indexOf("E") != -1) {
					actValue = new String(new StringBuffer(value).insert(value.indexOf("E") + 1, '+'));
				}
			} else if (indx.getType() == 2) {
				if (indx.getMask().equals("MM/dd/yyyy")) {
					SimpleDateFormat format = new SimpleDateFormat(indx.getMask());
					if (value != null && !value.equals("")) {
						Date date = format.parse(value, new ParsePosition(0));
						format.applyPattern("yyyyMMdd");
						actValue = format.format(date);
					}
				}
			}
		} catch (Exception e) {
			VWLogger.info("Exception in CreateActIndexValue ......" + e.getMessage());
		}
		return actValue;

	}
  /**/
 // update ayman  to be see  
  private String getAcceptNumericValue(String Value,Index index){ 
      String mask =index.getMask();
      VWLogger.info("mask in getacceptNumericvalue ......"+mask);
      int subtype =0;
     String sNumericVal = getActNumValue(Value); 
     String pattern =mask;
     String outValue =sNumericVal;
     if(sNumericVal.length() == 0 ){
        if(Value.length() > 0){
                return null;
        }    
        return sNumericVal;
     }   
     double dValue =0;
     try{
    	 //if condition added for cv10.1 issue fix for genral numeric mask with more digits.
    	 if(!(mask.trim().equals("0"))) {
       		 dValue = Double.parseDouble(sNumericVal);
       		 VWLogger.info("after double conversion  dValue......"+dValue);
       	 }
       if(mask.indexOf('%') >-1)dValue = dValue / 100;
     }
     catch(Exception e){
    	 VWLogger.info("Exception while getAcceptNumericValue 1 :"+e.getMessage());
           return sNumericVal; 
     }   
	 //if condition commented for cv10.1 issue fix for genral numeric mask with more digits.
   /*  if(mask.trim().equals("0")){
           return Long.toString(java.lang.Math.round(dValue));            
     }   */  
     
     int Epos =mask.indexOf('E');
      if(Epos > -1){
            int plusPos =mask.indexOf('+',Epos);
            if(plusPos >0){
                 pattern =mask.substring(0,plusPos )+mask.substring(plusPos+1,mask.length());
            }     
      }    
        
     boolean bEncloseNegative =false;
        
     if(mask.indexOf("(") > -1){
        bEncloseNegative =true;
        pattern = mask.substring(1,mask.length()-1);
     }  
     try{
    	 VWLogger.info("pattern :::::::::::::::"+pattern);
       	 if(!(mask.trim().equals("0"))) {
       		 java.text.DecimalFormat myFormatter = new java.text.DecimalFormat(pattern);
       		 outValue = myFormatter.format(dValue);
       	 }
     }
     catch(Exception e){
    	 VWLogger.info("Exception while getAcceptNumericValue 2 :"+e.getMessage());
        return null;
     }         
     if(bEncloseNegative){
        if(outValue.charAt(0) =='-'){ 
           int i=1; 
           while(!Character.isDigit(outValue.charAt(i))  ){i++;}
           if(i >1)
           outValue = outValue.substring(1,i)+"("+outValue.substring(i)+")";   
           else
            outValue = "("+outValue.substring(1)+")";               
        }   
     }
     VWLogger.info("outValue :::::::::::::"+outValue);
     return outValue;             
  }
 // public String getNumericSubType(String mask
public String getActNumValue(String Value){
      String NumValue ="";
      
      boolean addDecimal =false;
     for(int i=0;i<Value.length();i++){
       char Vchar = Value.charAt(i);  
       if(Character.isDigit(Vchar))        
          NumValue = NumValue+Vchar; 
       else if( Vchar =='.'){
           if(addDecimal) break; 
           NumValue = NumValue+Vchar; 
           addDecimal =true;
       }
       else if(Vchar =='-'){
          if(NumValue.length() >0 ) break;
          else NumValue = NumValue+Vchar; 
       }    
     } 
     
     return  NumValue;
     
  }    
 /* public int getFieldOrder(int Fieldid,EHFormInfo EHInfo){
      for(int i=0;i< EHInfo.getFormFields().size();i++){
          EHFormFieldInfo FFInfo =(EHFormFieldInfo)EHInfo.getFormFields().get(i);
          if(FFInfo.getEHFieldid() == Fieldid ) return FFInfo.getFieldOrder();
      }
      return -1;
  }*/
	public AIPField getMapFieldInfo(Vector MapFields, int Indexid) {
		if (MapFields != null) {
			for (int i = 0; i < MapFields.size(); i++) {
				AIPField MapFInfo = (AIPField) MapFields.get(i);
				if (MapFInfo.getMapIndexId() == Indexid)
					return MapFInfo;
			}
		}
		return null;
	}

	public AIPField getAIPDocField(int order, Vector DocFields) {
		if (DocFields != null) {
			for (int i = 0; i < DocFields.size(); i++) {
				AIPField field = (AIPField) DocFields.get(i);
				if (field.getOrder() == order)
					return field;
			}
		}
		return null;
	}
	
	public String getAIPDocumentLocation(int sid, AIPDocument aipDoc, LocationStatus locStatus, boolean nodePathFlag) {
		VWLogger.info("Before calling getAIPDocumentLocation method .....");
		Vector location = AIPManager.getVWC().getAIPDocumentLocation(sid, aipDoc.getVWNodePath());
		VWLogger.info("=getAIPDocumentLocation method return value :"+location);
		if (location == null || (location != null && location.size() == 0)) {
			aipDoc.setStatus(aipDoc.PENDING);
			aipDoc.setPendCause(28);
			return null;
		} else if (location != null && location.size() > 1) {
			aipDoc.setStatus(aipDoc.PENDING);
			aipDoc.setPendCause(29);
			return null;
		}
		String locationStr = location.get(0).toString();
		locationStr = locationStr.replace("\t", "/");
		locationStr = locationStr.replace("\\", "/");
		VWLogger.info("locationStr >>>>>>>>>> "+locationStr);
		aipDoc.setVWNodePath(locationStr);
		int parentNodeId = 0;
		if (nodePathFlag) {
			parentNodeId = DocumentsManager.getparentNodeIdofNodePath(locStatus, aipDoc.getVWNodePath(),
				aipDoc.isCreateVWNode());
		} else {
			parentNodeId = DocumentsManager.getIdOfNodePath(locStatus, locationStr, true);
		}
		return String.valueOf(parentNodeId);
		
	}
}