package com.computhink.aip.ui;

import com.computhink.aip.*;
import com.computhink.aip.resource.ResourceManager;
import com.computhink.aip.resource.images.ImageManager;
import com.computhink.aip.util.AIPUtil;
import com.computhink.aip.util.VWLogger;

import com.computhink.common.Constants;
import com.computhink.common.DocType;
import com.computhink.common.Document;
import com.computhink.aip.client.*;
import com.computhink.common.ViewWiseErrors;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;

import java.util.*;

import javax.swing.border.*;


import java.io.File;
/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public class ProcessFileLayout extends JPanel {

  private Vector Tabrows;
  private String[] columnNames;
  public JTable table;
  private ResourceBundle bundle = null;
  private boolean isTableEdit =false;
  SpecialTableModel TableModel;
  EHHistoryPanel HistPane;
  EyesHandsManager EHManager =new EyesHandsManager();
  Vector EHForms;
  Vector VWDoctypes;
  Vector refreashNodesRooms;

  /*
   * Issue / Enhancement 	: Enhancements of AIP  
   * 						  Requirment sent by Rajesh on 09-June-2006 through eMail  	 
   * Created By				: M.Premananth
   * Date					: 14-June-2006
   */
  JTextField 	Pagecounttxt;
  JButton 		Firstbtn;
  JButton		Previousbtn;
  JButton		Nextbtn;
  JButton		Lastbtn;
  private ImageManager IM = new ImageManager();

  
  private final int MARGINCOL    =0 ;  private final int EHFILECOL    =1 ;
  private final int FCREATECOL   =2 ;  private final int FPROCESSCOL  =3 ;
  private final int IMPORTCOL    =4 ;  //public final int PENDINGCOL    =5 ;
  private final int EHFILEIDCOL  =5 ;  private final int STATE        =6 ;
  private final int COLCOUNT     =7 ;
  
  private final ResourceManager connectorManager = ResourceManager.getDefaultManager();

  // modified by amaleh on 2003-07-17   
  public ProcessFileLayout(EHHistoryPanel HistPane1) {
    HistPane = HistPane1;
    setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
    setAlignmentX(Component.LEFT_ALIGNMENT);
    
    /*
     * Issue / Enhancement 	: Enhancements of AIP  
     * 						  Requirment sent by Rajesh on 09-June-2006 through eMail  	 
     * Created By				: M.Premananth
     * Date					: 14-June-2006
     */
    JPanel informationPanel = new JPanel();
    informationPanel.setLayout(new BoxLayout(informationPanel,BoxLayout.X_AXIS));
    informationPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
    JLabel MapLab= new JLabel(AIPUtil.getString("History.Lab.ProcessedFiles"));
    MapLab.setFont(new Font(MapLab.getFont().getName(),Font.BOLD,MapLab.getFont().getSize()));
    MapLab.setAlignmentX(Component.LEFT_ALIGNMENT);
    informationPanel.add(MapLab);
    informationPanel.add(Box.createHorizontalStrut(231));
    ButtonListener btnListener =new ButtonListener();
    JLabel maxrow	= new JLabel(connectorManager.getString("History.Lab.MaximumRows"));
    int iMaximumRow = AIPManager.getMaximumRow();
    if(iMaximumRow <= 0)
       	AIPManager.setMaximumRow(iMaximumRow = 15);
    VWLogger.info("Maximum Rows to be displayed on history page table :"+iMaximumRow);
    EHHistoryPanel.iMaximumRows = iMaximumRow;
    Pagecounttxt 	= new JTextField(String.valueOf(EHHistoryPanel.iMaximumRows),15); 
    Pagecounttxt.setMaximumSize(new Dimension(45,21));
    Pagecounttxt.setPreferredSize(new Dimension(45,21));
    Firstbtn  		= new JButton();
    Firstbtn.setIcon(IM.getImageIcon("easfirst.gif"));
    Firstbtn.setToolTipText(AIPUtil.getString("History.Btn.First"));

    Previousbtn  		= new JButton();
    Previousbtn.setIcon(IM.getImageIcon("easprev.gif"));
    Previousbtn.setToolTipText(AIPUtil.getString("History.Btn.Previous"));

    Nextbtn  		= new JButton();
    Nextbtn.setIcon(IM.getImageIcon("easnext.gif"));
    Nextbtn.setToolTipText(AIPUtil.getString("History.Btn.Next"));

    Lastbtn  		= new JButton();
    Lastbtn.setIcon(IM.getImageIcon("easlast.gif"));
    Lastbtn.setToolTipText(AIPUtil.getString("History.Btn.Last"));


    TextFieldListener txtListener = new TextFieldListener();
    Pagecounttxt.addFocusListener(txtListener);
    Firstbtn.addActionListener(btnListener);
    Previousbtn.addActionListener(btnListener);
    Nextbtn.addActionListener(btnListener);
    Lastbtn.addActionListener(btnListener);

    Firstbtn.setPreferredSize(new Dimension(20,14));
    Firstbtn.setMinimumSize(new Dimension(20,14));
    Previousbtn.setPreferredSize(new Dimension(20,14));
    Previousbtn.setMinimumSize(new Dimension(20,14));
    Nextbtn.setPreferredSize(new Dimension(20,14));
    Nextbtn.setMinimumSize(new Dimension(20,14));
    Lastbtn.setPreferredSize(new Dimension(20,14));
    Lastbtn.setMinimumSize(new Dimension(20,14));
	// Issue 544
    Firstbtn.setEnabled(false);
    Previousbtn.setEnabled(false);
    Lastbtn.setEnabled(true);
    Nextbtn.setEnabled(true);
    
    informationPanel.add(maxrow);
    informationPanel.add(Pagecounttxt);
    informationPanel.add(Box.createHorizontalStrut(30));
    informationPanel.add(Firstbtn);
    informationPanel.add(Box.createHorizontalStrut(2));
    informationPanel.add(Previousbtn);
    informationPanel.add(Box.createHorizontalStrut(5));
    informationPanel.add(Nextbtn);
    informationPanel.add(Box.createHorizontalStrut(2));
    informationPanel.add(Lastbtn);
    add(informationPanel);
    add(Box.createVerticalStrut(5));
    add(createProcessTablePanel());
    
    for(int i=0; i<table.getColumnCount(); i++){
    	TableColumn column = table.getColumnModel().getColumn(i);
    	if (i==MARGINCOL){
            column.setPreferredWidth(18);
            column.setMaxWidth(18);
            column.setCellRenderer(new ButtonRenderer());
            column.setCellEditor(new ButtonEditor());
        }else if (i == EHFILECOL){
        	column.setMaxWidth(450);
			column.setMinWidth(345);
			column.setPreferredWidth(345);
        }else if (i == FCREATECOL){
        	column.setMaxWidth(450);
			column.setMinWidth(115);
			column.setPreferredWidth(115);
        }else if (i == FPROCESSCOL){
        	column.setMaxWidth(450);
			column.setMinWidth(115);
			column.setPreferredWidth(115);
        }else if (i == IMPORTCOL){
        	column.setMaxWidth(450);
     	    column.setMinWidth(70);
     	    column.setPreferredWidth(70);
        }

    }
  }
  public JScrollPane createProcessTablePanel(){
        Tabrows =new Vector();

        TableModel  = new SpecialTableModel();
        table = new JTable(TableModel);
        table.setRowHeight(20);
        table.getTableHeader().setReorderingAllowed(false);
        table.getTableHeader().setMinimumSize(new Dimension(30,21));
        JLabel lblHeader=(JLabel)table.getTableHeader().getDefaultRenderer();
		lblHeader.setHorizontalAlignment(SwingConstants.CENTER);
		//Below code is commented, because of problem with the header resize(header was breaking up).
        //table.getTableHeader().setPreferredSize(new Dimension(100,21));
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        table.getTableHeader().setAlignmentX(LEFT_ALIGNMENT) ;
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        TableMouseListener MListener =new TableMouseListener();
        this.addMouseListener(MListener);
        table.addMouseListener(MListener);
        table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        ListSelectionModel SelectionModel= table.getSelectionModel();
        SelectionModel.addListSelectionListener(new ListSelectionListener() {
              public void valueChanged(ListSelectionEvent e) {
                    tableSelectionChanged(e);
              }
        });
        JScrollPane tableScroller = new JScrollPane(table);

        /*for(int i=0;i<table.getColumnCount();i++){
           TableColumn column = table.getColumnModel().getColumn(i);
           if (i==MARGINCOL){
               column.setPreferredWidth(18);
               column.setMaxWidth(18);
               column.setCellRenderer(new ButtonRenderer());
               column.setCellEditor(new ButtonEditor());
           }else if (i == EHFILECOL){
               //column.setMaxWidth(470);
               column.setPreferredWidth(155);
           }else if (i == FCREATECOL){
               column.setMaxWidth(450);
               column.setPreferredWidth(90);
           }else if (i == FPROCESSCOL){
               column.setMaxWidth(450);
               column.setPreferredWidth(90);
           }else if (i == IMPORTCOL){
               column.setMaxWidth(450);
               column.setPreferredWidth(30);
           }

        }*/
        //tableScroller.setAlignmentX(Component.LEFT_ALIGNMENT);
          JTableHeader header = table.getTableHeader();
          header.addMouseListener(TableModel.new ColumnListener(this));
          header.setDefaultRenderer(
          		TableModel.new SortableHeaderRenderer(header.getDefaultRenderer()));
          tableScroller.setAlignmentX(Component.LEFT_ALIGNMENT);
          
        return tableScroller;
  }

  public void LoadTableData(Vector TData){
    if(TData !=null){
      if(TData.size() >0){
         for(int i=0;i<TData.size();i++){
             Tabrows.add(getTableRowOfInfo((AIPFile)TData.get(i)));
             TableModel.fireTableRowsInserted(Tabrows.size()-1,Tabrows.size()-1);
         }
      }
    }
  }
  public void addTableRow(AIPFile Info ){
        Tabrows.add(getTableRowOfInfo(Info));
        TableModel.fireTableRowsInserted(Tabrows.size()-1,Tabrows.size()-1);
  }
  public void DeleteSelectedRows(){
    int[]  Rows = table.getSelectedRows();
    for(int i=Rows.length-1; i >= 0;i--){
         // int fileid =((Integer)TableModel.getValueAt(Rows[i],EHFILEIDCOL)).intValue();
    	   int fileid =((Integer)TableModel.getValueOf(Rows[i],EHFILEIDCOL)).intValue();                
               AIPFile aipFile =new AIPFile();
               aipFile.setId(fileid);
               int ret =AIPManager.getVWC().deleteAIPHistoryFile(AIPManager.getCurrSid(),aipFile);
               if(ret !=AIPManager.NOERROR)
               {    
                   JOptionPane.showMessageDialog(this,AIPManager.getErrorDescription(ret)+"("+ret+")",AIPUtil.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);
                   return ;              
               }               
               TableModel.RemoveRow(Rows[i]);
    }
  }
  public void ProcessSelectedRows(){
      VWLogger.audit("Reprocessing of Pending Documents Started ");
    try{
     HistPane.EHD.PManager =new PagesManager();
     }
     catch (java.lang.UnsatisfiedLinkError e){
             JOptionPane.showMessageDialog(this,AIPUtil.getString("Init.Msg.LostLibrary"),AIPUtil.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);
            // if(Log.LOG_ERROR) Log.error("Native library VWPage.dll not found. Cannot proceed.",e);
             //System.exit(0);
             return;
    }
    //HistPane.EHD.PManager.CreatePageTempFolder();
    ReLoadEHForms();
     int[]  Rows = table.getSelectedRows();
     refreashNodesRooms =new Vector();
     for(int i=Rows.length-1; i >= 0;i--){
          AIPFile aipFile=(AIPFile)getAIPFileInfoOfRow(Rows[i]);
          ProcessAIPFile(aipFile);
          TableModel.setValueAt(aipFile.getProcessDate(),Rows[i],FPROCESSCOL );
          TableModel.setValueAt(new Integer(aipFile.getImported()),Rows[i],IMPORTCOL );
          //TableModel.setValueAt(new Integer(aipFile.getPending()),Rows[i],PENDINGCOL );
          TableModel.fireTableRowsUpdated(Rows[i],Rows[i]);
     }
    //HistPane.EHD.PManager.deletePageTempFolder();
    HistPane.EHD.PManager =null;
    VWLogger.info("Reprocessing of Pending Documents Ended ");
  }
  public void ProcessAIPFile(AIPFile aipFile){
         VWLogger.info("Reprocessing file with ID: " + aipFile.getId());
         try{
            Vector pendingDocs =new Vector();  
            int ret =AIPManager.getVWC().getAIPDocuments(AIPManager.getCurrSid(),aipFile,pendingDocs);              
            if(ret!=AIPManager.NOERROR)
            {    
                 JOptionPane.showMessageDialog(this,AIPManager.getErrorDescription(ret)+"("+ret+")",AIPUtil.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);
                   return ; 
            }    
            for(int i=0;i<pendingDocs.size();i++)
            {
                ret = AIPManager.getVWC().getAIPDocFields(AIPManager.getCurrSid(),(AIPDocument)pendingDocs.get(i));
                               
            } 
            int currSid =AIPManager.getCurrSid();
            int DocImport= ReprocessAIPFileDocuments(pendingDocs);
            AIPManager.setCurrSid(currSid);
            aipFile.setImported(aipFile.getImported()+DocImport);
            aipFile.setPending(aipFile.getPending()-DocImport);
            aipFile.setProcessDate(AIPUtil.SetDateMaskedValue(AIPUtil.MASKDATE,new Date()));
            AIPManager.getVWC().setAIPHistoryFile(AIPManager.getCurrSid(),aipFile);  
                    
            VWLogger.audit("File:<" + aipFile.getFilePath() + ">\t" + "Processed:<" + aipFile.getProcessDate() + ">\t" + "Docs Imported:<" + aipFile.getImported() + ">\t" + "Docs Pending:<" + aipFile.getPending() + ">");
         }
         catch (Exception e){
             VWLogger.error(" in Processing history Saved File <"+aipFile.getFilePath()+"> :",e );
                return ;
         }
         
  }

  public int ReprocessAIPFileDocuments(Vector aipDocs){
        int DocSaveNum = 0;
        int isPrimitiveDocs = 0;
		int isConvertToDJVU = 0;
        for(int i=0;i<aipDocs.size();i++){
            AIPDocument aipDoc = (AIPDocument)aipDocs.get(i);
            VWLogger.debug("Add Pending AIPDocument <"+aipDoc.getName()+"> To "+ Constants.PRODUCT_NAME);
            /* added by amaleh on 2003-06-20 */
            Document vwDoc;
            AIPForm aipForm;
            
            int sid = AIPManager.getCurrSid();
            isPrimitiveDocs = AIPManager.isPrimitiveDocs();
			isConvertToDJVU = AIPManager.isConvertToDJVU();
            //now checks for whether aipDoc has field names or not. That way, it works with or without templates.
            if ((aipDoc.getFieldNames() != null) && (!aipDoc.getFieldNames().trim().equals("")))
            {   
            	Session s = null;
				if (DocumentsManager.calcDepth(aipDoc.getVWNodePath()) >= 4) {
	                s =DocumentsManager.getSessionOfNodePath(aipDoc.getVWNodePath());
	                if(s ==null){
	                     VWLogger.error(" Room was not found in combo box!",null);
	                    aipDoc.setStatus(aipDoc.PENDING);
	                    aipDoc.setPendCause(12);
	                    String docImpMsg = aipDoc.getName()+" is pending because of error " + aipDoc.getPendCause() + ": "+AIPUtil.getString("Pending.Status." + aipDoc.getPendCause());
	                    VWLogger.audit(EHGeneralPanel.formatDocMsg(aipDoc, docImpMsg));
	                    AIPManager.getVWC().setAIPDocument(AIPManager.getCurrSid(),aipDoc);
	                     continue;                    
	                 }
				} else {
					s = AIPManager.getSession(AIPManager.getCurrSid());
				}
                sid =s.sessionId;
                if(sid <=0 )
                {    
                    sid= HistPane.EHD.GenPane.internalConnect(s);
                    if(sid <=0)
                    {
                          VWLogger.error(" Cannot connect to the destination Room!",null);
                          aipDoc.setStatus(aipDoc.PENDING);
                          aipDoc.setPendCause(12);
                          String docImpMsg = aipDoc.getName()+" is pending because of error " + aipDoc.getPendCause() + ": "+AIPUtil.getString("Pending.Status." + aipDoc.getPendCause());
                          VWLogger.audit(EHGeneralPanel.formatDocMsg(aipDoc, docImpMsg));
                          AIPManager.getVWC().setAIPDocument(AIPManager.getCurrSid(),aipDoc);
                        continue;
                    }
                    refreashNodesRooms.add(Integer.toString(sid));
                    
                }        
                else
                {
                   if(!refreashNodesRooms.contains(Integer.toString(sid)))
                   {
                      DocumentsManager.getNavigatorNode(sid,true);
                       refreashNodesRooms.add(Integer.toString(sid));
                       
                   }    
                }    
                vwDoc = EHManager.TransferAIPDocToVWDoc(sid,null,aipDoc);
                if(vwDoc == null){
                   // aipDoc.setStatus(aipDoc.PENDING);
                    //aipDoc.setPendCause(9);                     
                      String docImpMsg = aipDoc.getName()+" is pending because of error " + aipDoc.getPendCause() + ": "+AIPUtil.getString("Pending.Status." + aipDoc.getPendCause());
                      VWLogger.audit(EHGeneralPanel.formatDocMsg(aipDoc, docImpMsg));
                     AIPManager.getVWC().setAIPDocument(AIPManager.getCurrSid(),aipDoc);
                     continue;
                }
                     
            }
            else // Eyes Hands Process
            {
                if(!refreashNodesRooms.contains(Integer.toString(sid)))
                {
                       DocumentsManager.getNavigatorNode(sid,true);
                       refreashNodesRooms.add(Integer.toString(sid));                       
                }  
                aipForm =getAIPForm(aipDoc.getAIPForm().getName());
                if(aipForm == null || aipDoc.getAIPForm().getName().equals("")){                
                    aipDoc.setPendCause(1);
                    AIPManager.getVWC().setAIPDocument(AIPManager.getCurrSid(),aipDoc);
                    String docImpMsg = aipDoc.getName()+" is pending because of error " + aipDoc.getPendCause() + ": "+AIPUtil.getString("Pending.Status." + aipDoc.getPendCause());
                    VWLogger.audit(EHGeneralPanel.formatDocMsg(aipDoc, docImpMsg));
                    continue;
                }
                String FormName=aipForm.getName();
                if( aipForm.getStatus()== aipForm.FIELDSNEEDMAP){                
                    aipDoc.setPendCause(3);
                    AIPManager.getVWC().setAIPDocument(AIPManager.getCurrSid(),aipDoc);
                    String docImpMsg = aipDoc.getName()+" is pending because of error " + aipDoc.getPendCause() + ": "+AIPUtil.getString("Pending.Status." + aipDoc.getPendCause());
                    VWLogger.audit(EHGeneralPanel.formatDocMsg(aipDoc, docImpMsg));
                    continue;
                }
                if (aipForm.getMapDTId() <=0){                
                    aipDoc.setPendCause(2);
                    AIPManager.getVWC().setAIPDocument(AIPManager.getCurrSid(),aipDoc);
                    String docImpMsg = aipDoc.getName()+" is pending because of error " + aipDoc.getPendCause() + ": "+AIPUtil.getString("Pending.Status." + aipDoc.getPendCause());
                    VWLogger.audit(EHGeneralPanel.formatDocMsg(aipDoc, docImpMsg));
                    continue;            
                }

                DocType dt= DoctypesManager.getDoctype(AIPManager.getCurrSid(),aipForm.getMapDTId());
                vwDoc=EHManager.TransferAIPDocToVWDoc(null,aipDoc,aipForm,dt);    

                if(vwDoc == null)
                {                       
                        String docImpMsg = aipDoc.getName()+" is pending because of error " + aipDoc.getPendCause() + ": "+AIPUtil.getString("Pending.Status." + aipDoc.getPendCause());
                        VWLogger.audit(EHGeneralPanel.formatDocMsg(aipDoc, docImpMsg));    
                        
                        AIPManager.getVWC().setAIPDocument(AIPManager.getCurrSid(),aipDoc);
                         continue;
                }
            } 
            
            File[] imageArray = HistPane.EHD.PManager.parseImageFiles(aipDoc.getImagesPath());
            
            if(imageArray == null)
            {                
            	aipDoc.setPendCause(4);
				VWLogger.info("pendingErrorMessage :: "+PagesManager.pendingErrorMessage);
				/**CV10.2 - Added for pendcause 22. Added by Vanitha S**/	
				if (PagesManager.pendingErrorMessage == 2) {
					aipDoc.setPendCause(22);					
				} else if (PagesManager.pendingErrorMessage == 7) {
					aipDoc.setPendCause(27);					
				}
				PagesManager.pendingErrorMessage = 0;
                String docImpMsg = aipDoc.getName()+" is pending because of error " + aipDoc.getPendCause() + ": "+AIPUtil.getString("Pending.Status." + aipDoc.getPendCause());
                VWLogger.audit(EHGeneralPanel.formatDocMsg(aipDoc, docImpMsg));
                 AIPManager.getVWC().setAIPDocument(AIPManager.getCurrSid(),aipDoc);
                 continue;
            }
            DocumentsManager DocsManager=new  DocumentsManager();
            int VWDocId =0;
            if (aipDoc.isAppend())
            {
                VWDocId = DocsManager.getIDofDoc(sid, vwDoc); 
                if(VWDocId > 0 && imageArray.length >0)
                {                       
                     VWLogger.debug("pull Pages from document <"+VWDocId+">");                         
                     boolean bGetPages =HistPane.EHD.PManager.pullDocumentPages(sid,VWDocId);                  
                     if(!bGetPages)
                     {
                       aipDoc.setStatus(aipDoc.PENDING);         
                       aipDoc.setPendCause(4);  
                       AIPManager.getVWC().setAIPDocument(sid,aipDoc);
                       continue;
                     }    
                     else
                     {  
                    	 VWLogger.debug("add new Pages to document <"+VWDocId+">");    
                    	 int ret = HistPane.EHD.PManager.addPagesToDocument(sid,VWDocId,imageArray, vwDoc);
                    	 VWLogger.info("ProcessFileLayout : return value from addPagesToDocument() : "+ret+" : "+AIPManager.getErrorDescription(ret));
                    	 if(ret == ViewWiseErrors.NoError) 
                    		 VWLogger.debug("Pages added successfully ");
                    	 else{
                    		 aipDoc.setStatus(aipDoc.PENDING);
                    		 /**CV10.2 - Added for pendcause 21, 23 and 24. Added by Vanitha S**/
                    		 if (PagesManager.pendingErrorMessage > 0) {
									if (PagesManager.pendingErrorMessage == 4) {// Not enough memory
										aipDoc.setPendCause(23);
									} else {
										aipDoc.setPendCause(24); // Failed to create all.zip
									}
									PagesManager.pendingErrorMessage = 0;
							 }  else {
								if (ret == ViewWiseErrors.documentAlreadyLocked) {
									aipDoc.setPendCause(13);
								} else if (ret == ViewWiseErrors.pageCountMismatch) {
									aipDoc.setPendCause(21);
								} else
									aipDoc.setPendCause(14);
							 }
                    		 
                    		 String docImpMsg = "ProcessFileLayout ::: " + aipDoc.getName()+" is pending because of error " + aipDoc.getPendCause() + ": "+AIPUtil.getString("Pending.Status." + aipDoc.getPendCause());
                    		 VWLogger.audit(EHGeneralPanel.formatDocMsg(aipDoc, docImpMsg));
                    		 continue;
                    	 }
                    	 vwDoc.setId(VWDocId);
                     }   
                      
                } 
            }
            if(VWDocId <= 0)
            {
                   if(imageArray.length >0)
                   {    
                         String docFilePath =PagesManager.createPagesTempFolder(sid,VWDocId);                                
                           /*  Issue - 742
  							*   Developer - Nebu Alex 
  							*   Code Description - The following piece of code implements
 							*                      Generate primitive document functionalities 
  							*/
                         boolean findPages = false;
                         /**CV2019 merges from CV10.2 line**/
                         if(isPrimitiveDocs ==1 && isConvertToDJVU == 0){
                        	 findPages=HistPane.EHD.PManager.addPagesToTempFolderPrimitiveGeneration(sid,VWDocId,imageArray);
                         }else{
                        	 findPages=HistPane.EHD.PManager.addPagesToTempFolder(sid,VWDocId,imageArray);
                         } 
                         //boolean findPages=HistPane.EHD.PManager.addPagesToTempFolder(sid,VWDocId,imageArray);  
                         // End of issue 742
                         if(!findPages)
                         {   
                             aipDoc.setStatus(aipDoc.PENDING);         
                             aipDoc.setPendCause(4); 
							 VWLogger.info("pendingErrorMessage :: " + PagesManager.pendingErrorMessage);
							 /**CV10.2 - Added for pendcause 21, 23 and 24. Added by Vanitha S**/
							 if (PagesManager.pendingErrorMessage == 3) {
								 aipDoc.setPendCause(21);					
							 } else if (PagesManager.pendingErrorMessage == 4) {
								 aipDoc.setPendCause(23);
							 } else if (PagesManager.pendingErrorMessage == 5) {
								 aipDoc.setPendCause(24);
							 }
							 PagesManager.pendingErrorMessage = 0;
                             AIPManager.getVWC().setAIPDocument(AIPManager.getCurrSid(),aipDoc);
                             String docImpMsg = aipDoc.getName()+" is pending because of error " + aipDoc.getPendCause() + ": "+AIPUtil.getString("Pending.Status." + aipDoc.getPendCause());
                             VWLogger.audit(EHGeneralPanel.formatDocMsg(aipDoc, docImpMsg));
                             continue;
                         } 
                         else vwDoc.setVersionDocFile(docFilePath);
                   }
                   int ret =AIPManager.getVWC().createDocument(sid,vwDoc);
                   HistPane.EHD.PManager.deletePagesTempFolder(sid,VWDocId);
                   if(ret <=0)
				   {
						aipDoc.setStatus(aipDoc.PENDING);
						if (ret == ViewWiseErrors.dssCriticalError) {
							aipDoc.setPendCause(15);
						} // Added for Lock document type
						else if (ret == ViewWiseErrors.DocumentTypeNotPermitted) {
							aipDoc.setPendCause(16);
						} else if (ret == ViewWiseErrors.dssInactive) {
							aipDoc.setPendCause(11);
						} // CV10.1 Enhancement-Dynamic Sequence validation. Modified by rkant.
						else if (ret == ViewWiseErrors.CheckUniquenessIndexErr) {
							aipDoc.setPendCause(17);
						} else if (ret == ViewWiseErrors.InvalidDateMask) {
							aipDoc.setPendCause(20);
						} else if (ret == ViewWiseErrors.allDotZipDoesNotExist) {
							aipDoc.setPendCause(24);
						} else {
							aipDoc.setPendCause(5);
						}
					
						String docImpMsg = aipDoc.getName() + " is pending because of error " + aipDoc.getPendCause() + ": "
								+ AIPUtil.getString("Pending.Status." + aipDoc.getPendCause());
						VWLogger.audit(EHGeneralPanel.formatDocMsg(aipDoc, docImpMsg));
						AIPManager.getVWC().setAIPDocument(AIPManager.getCurrSid(), aipDoc);
					
						continue;
				    }    
            }    
            DocSaveNum++;
            if(aipDoc.getCommentFile() !=null && !aipDoc.getCommentFile().trim().equals(""))
            {    
                VWLogger.info("load comment from file <"+aipDoc.getCommentFile()+">");                
                String comment = DocsManager.loadComment(aipDoc.getCommentFile());
                if(comment!=null)
                {    
                      int retc =DocsManager.setDocumentComment(sid,vwDoc,comment);                
                      if(retc <= 0)
                         VWLogger.error("can not"+
                          " add comment to document <"+vwDoc.getName()+">"
                          ,null); 
                }
                else
                {
                    VWLogger.info("Comment file path was invalid! Comment field will remain empty.");
                }    
                    
           }     
                          
            aipDoc.setStatus(aipDoc.IMPORTED);
            if (aipDoc.isDeleteImages())
                        EyesHandsManager.deleteImageFiles(aipDoc);
             EyesHandsManager.deleteCommentFile(aipDoc);
              
            AIPManager.getVWC().deleteAIPDocument(AIPManager.getCurrSid(),aipDoc);
            String docImpMsg = aipDoc.getName()+" is imported.";
            VWLogger.info(EHGeneralPanel.formatDocMsg(aipDoc, docImpMsg));       
            //VWLogger.debug(" AIPDocument <"+aipDoc.getName()+"> is imported");
            
        }
       
        return DocSaveNum;
  }
  public void ReLoadEHForms(){
      EHForms =new Vector();
      AIPManager.getVWC().getAIPForms(AIPManager.getCurrSid(),EHForms);      
  }
  public AIPForm getAIPForm(String FormName){
        if(FormName ==" ") return null;
        for(int i=0;i<EHForms.size();i++){
            AIPForm FInfo =(AIPForm)EHForms.get(i);
            if(FormName.equals(FInfo.getName())){
                return FInfo;
            }
        }
        return null;
  }

  public void setEditable(boolean enable){
      isTableEdit = enable;
  }
  public void setAllEnabled(boolean enable){
      table.setEnabled(enable);

  }


  public AIPFile getAIPFileInfoOfRow(int rownum){
      AIPFile aipFile =new AIPFile();
      String aipFilePath = TableModel.getValueAt(rownum,EHFILECOL).toString();
      if(!aipFilePath.equalsIgnoreCase("")){
          aipFile.setFilePath(aipFilePath);
          aipFile.setId(((Integer)TableModel.getValueAt(rownum,EHFILEIDCOL)).intValue());
          aipFile.setCreationDate(TableModel.getValueAt(rownum,FCREATECOL).toString().trim());
          aipFile.setProcessDate(TableModel.getValueAt(rownum,FPROCESSCOL).toString().trim());
          aipFile.setImported(((Integer)TableModel.getValueAt(rownum,IMPORTCOL)).intValue());
          aipFile.setPending(0);
      }
      return aipFile;
  }

  public Vector getTableRowOfInfo(AIPFile FInfo){
     Vector Trow  =new Vector();
     Trow.add(MARGINCOL,"");
     Trow.add(EHFILECOL,FInfo.getFilePath());
     Trow.add(FCREATECOL,FInfo.getCreationDate());
     Trow.add(FPROCESSCOL,FInfo.getProcessDate());
     Trow.add(IMPORTCOL,new Integer(FInfo.getImported()));
     //Trow.add(PENDINGCOL,new Integer(FInfo.getPending()));
     Trow.add(EHFILEIDCOL,new Integer(FInfo.getId()));
     Trow.add(STATE,new Integer(0));
     return Trow;
  }
  public void CheckEnableButton(){
      int[] Rows= table.getSelectedRows();
      if(Rows.length > 0 ){
        HistPane.Delbtn.setEnabled(true);
        for(int i=0;i<Rows.length;i++){
          HistPane.Procssbtn.setEnabled(false);
          if(false){//((Integer)TableModel.getValueAt(Rows[i],PENDINGCOL)).intValue() >0){
               HistPane.Procssbtn.setEnabled(true);
               break;
          }
       }
     }else{
        HistPane.Delbtn.setEnabled(false);
        HistPane.Procssbtn.setEnabled(false);
     }

  }
  /*
  public void ShowPendingDocumentDialog(int Pointrow,int X,int Y){
          this.setCursor(new Cursor(Cursor.WAIT_CURSOR));

          Frame f = JOptionPane.getFrameForComponent(table);
          int fileId =((Integer)TableModel.getValueAt(Pointrow,EHFILEIDCOL)).intValue();
          ReLoadEHForms();
          Vector pendingDocs =new Vector();
          AIPFile aipFile =new AIPFile();
          aipFile.setId(fileId);
          int ret =AIPManager.getVWC().getAIPDocuments(AIPManager.getCurrSid(),aipFile,pendingDocs);
          if(ret !=AIPManager.NOERROR)
          {    
              JOptionPane.showMessageDialog(this,AIPManager.getErrorDescription(ret)+"("+ret+")",AIPUtil.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);
              return ; 
          }    
          PendingDocumentDialog PDD =new PendingDocumentDialog(f,AIPUtil.getString("Pending.Lab.Title"),true,this,pendingDocs);
          PDD.setLocationRelativeTo(table);
          PDD.setLocation(HistPane.getLocationOnScreen().x+213,HistPane.getLocationOnScreen().y+45);
          PDD.setSize(450,390);
          PDD.setResizable(false);
          PDD.setVisible(true);
          this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
          f.pack();

  }
  */
  //--------------------table Inner class & Methode------------------------

  //Methode responce to table Selection change
  public void  tableSelectionChanged(ListSelectionEvent e){
        ListSelectionModel lsm = (ListSelectionModel)e.getSource();
        if (!lsm.isSelectionEmpty()){
          if(table.isEnabled()) {
            CheckEnableButton();
          }else {
            HistPane.Procssbtn.setEnabled(false);
            HistPane.Delbtn.setEnabled(false);
          }
        }else {
            HistPane.Procssbtn.setEnabled(false);
            HistPane.Delbtn.setEnabled(false);
        }
  }
  private class TableMouseListener extends MouseAdapter  {
      public void mousePressed(MouseEvent e) {
           int Pointrow =table.rowAtPoint(e.getPoint());
           if (e.getClickCount() == 1){


              if(Pointrow > -1)
                  table.setRowSelectionInterval(Pointrow,Pointrow);
              if (e.getModifiers() == e.BUTTON3_MASK) {

              }else if (e.getModifiers() == e.BUTTON1_MASK){

              }
            } else if(e.getClickCount() == 2 && table.isEnabled()){
                if(Pointrow > -1){
                  if(false){//{((Integer)TableModel.getValueAt(Pointrow,PENDINGCOL)).intValue() > 0){
                    ;//ShowPendingDocumentDialog(Pointrow,e.getX(),e.getY());
                  }
                }
           }

     }
     public void mouseReleased(MouseEvent e) {
     }
  }
 
  //--------------------  SpecialTableModel-----------------------
  public  class SpecialTableModel extends AbstractTableModel {
	  String[] columnNames={"",AIPUtil.getString("History.TCol.FileName"),AIPUtil.getString("History.TCol.DateCreated"),AIPUtil.getString("History.TCol.DateProcessed"),AIPUtil.getString("History.TCol.Imported")};
	  Vector ARow;
	  int Srow ;
	  int m_sortCol = 0;
	  boolean m_sortAsc = true;
	  private   boolean[] visibleColumns = new boolean[5];{ 
		  visibleColumns[0] = true; 
		  visibleColumns[1] = true; 
		  visibleColumns[2] = true; 
		  visibleColumns[3] = true; 
		  visibleColumns[4] = true;       
	  }        
	  public SpecialTableModel(){
	  }
	  public int getRowCount() {
		  return Tabrows.size();
	  }
	  public String getColumnName(int col) {    	 	   
		  int j =getNumber(col);
		  return columnNames[j]; 
	  }
	  public Object getValueOf(int aRow, int aColumn) {
		  if(Tabrows.size() <= aRow) return ""; 
		  Vector row = (Vector) Tabrows.elementAt(aRow);
		  return row.elementAt(aColumn);
	  }
	  public Object getValueAt(int aRow, int aColumn) {
		  if(Tabrows.size() <= aRow) return ""; 
		  Vector row = (Vector) Tabrows.elementAt(aRow);
		  int j =getNumber(aColumn);         
		  return row.elementAt(j);       
	  }
	  public Class getColumnClass(int c) {
		  return getValueOf(0, c).getClass();
	  }
	  public boolean isCellEditable(int row, int col) {
		  if(col==0)
			  return true;
		  else
			  return false;
	  }
	  public void setValueAt(Object value, int row, int col) {
		  Vector row1 = (Vector) Tabrows.elementAt(row);
		  row1 = (Vector) Tabrows.elementAt(row);
		  if(value ==null)return;
		  if(value.equals(row1.elementAt(col))) return;
		  row1.remove(col);
		  row1.add(col,value);
	  }
	  
	  public void RemoveRow(int Rnum){
		  Tabrows.remove(Rnum);
		  fireTableRowsDeleted(Rnum,Rnum);
	  }
	  public void RemoveAllRows(){
		  int lastrow =Tabrows.size();
		  if(lastrow >0){
			  Tabrows.removeAllElements();
			  this.fireTableRowsDeleted(0,lastrow-1);
		  }
	  }
	  protected void setVisibleColumns(int col, boolean selection){  
		  int count=0;
		  for(int i=1;i<visibleColumns.length;i++){
			  if(visibleColumns[i]==true){count++;}
			  
		  }  	
		  if(count==1 && selection==false){
			  
		  }else{
			  
			  visibleColumns[col] = selection; 
			  fireTableStructureChanged();           
		  }
		  
	  } 
	  /** 
	   * This function converts a column number of the table into
	   * the right number of the data. 
	   */ 
	  protected int getNumber(int column) { 
		  int n = column;    // right number
		  
		  int i = 0; 
		  do { 
			  if (!(visibleColumns[i])) n++; 
			  i++; 
		  } while (i < n); 
		  // When we are on an invisible column, 
		  // we must go on one step
		  while (!(visibleColumns[n])) n++; 
		  
		  return n; 
	  } 
	  // *** METHODS OF THE TABLE MODEL *** 
	  public int getColumnCount() {    	  
		  int n = 0; 
		  for (int i = 0; i < visibleColumns.length; i++) 
			  if (visibleColumns[i]) n++;    		
		  return n; 
	  } 
	  class ColumnListener extends MouseAdapter
	  {
		  protected JTable m_table;
		  protected ProcessFileLayout pfl;
		  
		  public ColumnListener(ProcessFileLayout PFL){
			  pfl = PFL;
			  m_table = PFL.table;
		  }
		  
		  public void mouseClicked(MouseEvent e){
			  if(e.getModifiers()==e.BUTTON1_MASK){
				  m_table.getTableHeader().setCursor(new Cursor(Cursor.WAIT_CURSOR));	        	
				  sortCol(e);	         	
				  m_table.getTableHeader().setCursor(new Cursor(Cursor.DEFAULT_CURSOR));	         	
			  }
			  else if(e.getModifiers()==e.BUTTON3_MASK){
				  javax.swing.JPopupMenu menu=new javax.swing.JPopupMenu("View");
				  for (int i=1; i < columnNames.length; i++){
					  final javax.swing.JCheckBoxMenuItem subMenu=new javax.swing.JCheckBoxMenuItem(
							  columnNames[i],visibleColumns[i]);
					  final int j =i;
					  subMenu.addActionListener(new ActionListener() { 
						  public void actionPerformed(ActionEvent evt) { 
							  TableModel.setVisibleColumns(j, subMenu.getState()); 
							  for(int i=0;i<table.getColumnCount();i++){
								  TableColumn column = table.getColumnModel().getColumn(i);
								  if (i==MARGINCOL){
									  column.setPreferredWidth(22);
									  column.setMaxWidth(22);
									  column.setCellRenderer(new ButtonRenderer());
									  column.setCellEditor(new ButtonEditor());
								  }
							  }
						  } 
					  }); 
					  menu.add(subMenu);
				  }
				  menu.show(m_table,e.getX(),e.getY());             
			  }
		  }
		  private void sortCol(MouseEvent e)
		  {
			  TableColumnModel colModel = m_table.getColumnModel();
			  int columnModelIndex = colModel.getColumnIndexAtX(e.getX());
			  int modelIndex = colModel.getColumn(columnModelIndex).getModelIndex();
			  if (modelIndex < 0)
				  return;
			  if ( m_sortCol==modelIndex)
				  m_sortAsc = !m_sortAsc;
			  else
				  m_sortCol = modelIndex;
			  
			  for (int i=0; i < colModel.getColumnCount();i++) {
				  TableColumn column = colModel.getColumn(i);
				  column.setHeaderValue(m_table.getColumnName(column.getModelIndex()));    
			  }
			  m_table.getTableHeader().repaint();  
			  
			  m_table.getTableHeader().setCursor(new Cursor(Cursor.WAIT_CURSOR));  
			  Collections.sort(Tabrows, new AIPFileComparator(modelIndex, m_sortAsc));
			  pfl.TableModel.fireTableChanged(new TableModelEvent(SpecialTableModel.this));
			  
		  }
	  }
	  class AIPFileComparator implements Comparator
	  {
		  protected int     m_sortCol;
		  protected boolean m_sortAsc;
		  
		  public AIPFileComparator(int sortCol, boolean sortAsc) {
			  m_sortCol = sortCol;
			  m_sortAsc = sortAsc;
		  }

		  public int compare(Object o1, Object o2) {
			  String sAip1 = o1.toString();
			  String sAip2 = o2.toString();
			  sAip1 = sAip1.substring(3,sAip1.length());
			  sAip2 = sAip2.substring(3,sAip2.length());
			  o1 = new AIPFile(sAip1,"",1);
			  o2 = new AIPFile(sAip2,"",1);
			  if(!(o1 instanceof AIPFile) || !(o2 instanceof AIPFile))
				  return 0;
			  AIPFile s1 = (AIPFile)o1;
			  AIPFile s2 = (AIPFile)o2;
			  int result = 0;
			  double d1, d2;
			  switch (m_sortCol) {
			  case 1:
				  result = s1.getFilePath().compareTo(s2.getFilePath());
				  break;
			  case 2:
				  try{
					  Date date_1 = AIPUtil.getDate(s1.getCreationDate());
					  Date date_2 = AIPUtil.getDate(s2.getCreationDate());
	  		      	  result = date_1.compareTo(date_2);
				  }catch(Exception e){
					  
				  }
				  break;
			  case 3:
				  try{
					  Date date_1 = AIPUtil.getDate(s1.getProcessDate());
					  Date date_2 = AIPUtil.getDate(s2.getProcessDate());
	  		      	  result = date_1.compareTo(date_2);
				  }catch(Exception e){
					  
				  }
				  break;
			  case 4:				  
				  try{
					  Integer intImported1 = Integer.valueOf(s1.getImported());
					  Integer intImported2 = Integer.valueOf(s2.getImported());
					  result = intImported1.compareTo(intImported2);
				  }catch(Exception e){
					  
				  }
				  break;
			  case 5:				 
				  try{
					  Integer intImported1 = Integer.valueOf(s1.getImported());
					  Integer intImported2 = Integer.valueOf(s2.getImported());
					  result = intImported1.compareTo(intImported2);
				  }catch(Exception e){
					  
				  }
				  break;
			  }
			  if (!m_sortAsc)
				  result = -result;
			  return result;
		  }
		  
		  public boolean equals(Object obj) {
			  if (obj instanceof AIPFileComparator) {
				  AIPFileComparator compObj = (AIPFileComparator)obj;
				  return (compObj.m_sortCol==m_sortCol) && 
				  (compObj.m_sortAsc==m_sortAsc);
			  }
			  return false;
		  }
	  }
	  
	  private class SortableHeaderRenderer implements TableCellRenderer {
		  private TableCellRenderer tableCellRenderer;
		  
		  public SortableHeaderRenderer(TableCellRenderer tableCellRenderer) {
			  this.tableCellRenderer = tableCellRenderer;
		  }
		  
		  public Component getTableCellRendererComponent(JTable table, 
				  Object value,
				  boolean isSelected, 
				  boolean hasFocus,
				  int row, 
				  int column) {
			  Component c = tableCellRenderer.getTableCellRendererComponent(table, 
					  value, isSelected, hasFocus, row, column);
			  if (c instanceof JLabel) {
				  JLabel l = (JLabel) c;
				  l.setHorizontalTextPosition(JLabel.LEFT);
				  int modelColumn = table.convertColumnIndexToModel(column);
				  l.setIcon(getHeaderRendererIcon(modelColumn, l.getFont().getSize()));
			  }
			  return c;
		  }
	  }
	  protected Icon getHeaderRendererIcon(int column, int size) {
		  if(m_sortCol == 0 || column != m_sortCol)
			  return null;
		  return new Arrow(!m_sortAsc, size,column);
	  }


   }
  private static class Arrow implements Icon {
	  private boolean descending;
	  private int size;
	  private int priority;
	  
	  public Arrow(boolean descending, int size, int priority) {
		  this.descending = descending;
		  this.size = 10;
		  this.priority = 1;
	  }
	  
	  public void paintIcon(Component c, Graphics g, int x, int y) {
		  Color color = c == null ? Color.GRAY : c.getBackground();             
		  // In a compound sort, make each succesive triangle 20% 
		  // smaller than the previous one. 
		  int dx = (int)(size/2*Math.pow(0.8, priority));
		  int dy = descending ? dx : -dx;
		  // Align icon (roughly) with font baseline. 
		  y = y + 5*size/6 + (descending ? -dy : 0);
		  int shift = descending ? 1 : -1;
		  g.translate(x, y);
		  
		  // Right diagonal. 
		  g.setColor(color.darker());
		  g.drawLine(dx / 2, dy, 0, 0);
		  g.drawLine(dx / 2, dy + shift, 0, shift);
		  
		  // Left diagonal. 
		  g.setColor(color.brighter());
		  g.drawLine(dx / 2, dy, dx, 0);
		  g.drawLine(dx / 2, dy + shift, dx, shift);
		  
		  // Horizontal line. 
		  if (descending) {
			  g.setColor(color.darker().darker());
		  } else {
			  g.setColor(color.brighter().brighter());
		  }
		  g.drawLine(dx, 0, 0, 0);
		  
		  g.setColor(color);
		  g.translate(-x, -y);
	  }
	  
	  public int getIconWidth() {
		  return 10;
	  }
	  
	  public int getIconHeight() {
		  return 10;
	  }
  }

   public boolean IsUnSaved(){

      return false;
   }
   public boolean checkUnSaved(){

      return true;
   }

   private class ButtonRenderer extends JButton
                        implements TableCellRenderer {
        public ButtonRenderer() {
            super();

        }
        public Component getTableCellRendererComponent(
                                JTable table, Object color,
                                boolean isSelected, boolean hasFocus,
                               int row, int column) {
          return this;
        }
   }
   private class ButtonEditor extends DefaultCellEditor {
       public ButtonEditor() {
                super(new JCheckBox());
            JButton b =new JButton();
            editorComponent = b;
            setClickCountToStart(1);
            b.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    fireEditingStopped();
                }
            });
       }
       protected void fireEditingStopped() {
            super.fireEditingStopped();
       }

       public Object getCellEditorValue() {
            return null;
       }
       public Component getTableCellEditorComponent(JTable table,
                                                     Object value,
                                                     boolean isSelected,
                                                     int row,
                                                    int column) {
          return editorComponent;
       }
   }
   /*
    * Issue / Enhancement 	: Enhancements of AIP  
    * 						  Requirment sent by Rajesh on 09-June-2006 through eMail  	 
    * Created By			: M.Premananth
    * Date					: 14-June-2006
    */
   class ButtonListener implements ActionListener {  	
	   public void actionPerformed(ActionEvent e) {
		   Nextbtn.setEnabled(true);            	
		   Lastbtn.setEnabled(true); 
		   Firstbtn.setEnabled(true);
		   Previousbtn.setEnabled(true);
		   JButton Source = (JButton)e.getSource();
		   
		   if(Source.equals(Firstbtn)){
			   HistPane.RemoveAllPFLRows();
			   int iStart 	= 0;
			   int iEnd 	= EHHistoryPanel.iMaximumRows;	
			   Vector hFiles =new Vector();
			   HistPane.EHD.setCursor(new Cursor(Cursor.WAIT_CURSOR));
			   AIPManager.getVWC().getAIPHistoryFiles(AIPManager.getCurrSid(),hFiles,iStart,iEnd,1);
			   HistPane.PFL.LoadTableData(hFiles); 
			   HistPane.EHD.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			   Firstbtn.setEnabled(false);
			   Previousbtn.setEnabled(false);  
			   EHHistoryPanel.iStartIndex 	= iStart;
			   EHHistoryPanel.iEndIndex	= iEnd;            
		   }
		   else if(Source.equals(Previousbtn)){
			   HistPane.RemoveAllPFLRows();
			   int iEnd 	= EHHistoryPanel.iStartIndex;
			   int iStart 	= iEnd - EHHistoryPanel.iMaximumRows;
			   if(iStart < 0)
			   {
				   iStart 	= 0;
				   iEnd 	= EHHistoryPanel.iMaximumRows;
			   }
			   Vector hFiles =new Vector();
			   HistPane.EHD.setCursor(new Cursor(Cursor.WAIT_CURSOR));
			   AIPManager.getVWC().getAIPHistoryFiles(AIPManager.getCurrSid(),hFiles,iStart,iEnd,1);
			   if(iStart== 0){
				   Firstbtn.setEnabled(false);
				   Previousbtn.setEnabled(false);
			   }             
			   HistPane.PFL.LoadTableData(hFiles);
			   HistPane.EHD.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			   EHHistoryPanel.iStartIndex 	= iStart;
			   EHHistoryPanel.iEndIndex		= iEnd;
			   
			   
		   }
		   else if(Source.equals(Nextbtn)){
			   HistPane.RemoveAllPFLRows();
			   int iStart 	= EHHistoryPanel.iEndIndex;
			   int iEnd 	= iStart + EHHistoryPanel.iMaximumRows;
				// Dynamic updation of total number of histroy files
			   Vector hCount = new Vector();
			   AIPManager.getVWC().getAIPHistoryCount(AIPManager.getCurrSid(),hCount,1);
			   EHHistoryPanel.iTotalCount = Integer.parseInt(hCount.get(0).toString());
			   hCount.clear();
			   hCount = null;
			   // End of Fix
			   if(iEnd > EHHistoryPanel.iTotalCount)
			   {
				   iStart 	= EHHistoryPanel.iTotalCount - EHHistoryPanel.iMaximumRows;
				   iEnd	= EHHistoryPanel.iTotalCount;
			   }
			   Vector hFiles =new Vector();
			   HistPane.EHD.setCursor(new Cursor(Cursor.WAIT_CURSOR));
			   AIPManager.getVWC().getAIPHistoryFiles(AIPManager.getCurrSid(),hFiles,iStart,iEnd+1,1);
			   int count =iEnd-iStart;
			   if(hFiles.size()==count){
				   Nextbtn.setEnabled(false);
				   Lastbtn.setEnabled(false);
			   }
			   else if(hFiles.size()>count){
				   hFiles.remove(count);
			   }
			   else if(hFiles.size()<count){
				   Nextbtn.setEnabled(false);
				   Lastbtn.setEnabled(false);            	
			   }           
			   HistPane.PFL.LoadTableData(hFiles);
			   HistPane.EHD.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			   EHHistoryPanel.iStartIndex 	= iStart;
			   EHHistoryPanel.iEndIndex		= iEnd;       	  	
		   }
		   else if(Source.equals(Lastbtn)){           	
			   HistPane.RemoveAllPFLRows();
 				// Dynamic updation of total number of histroy files
			   Vector hCount = new Vector();
			   AIPManager.getVWC().getAIPHistoryCount(AIPManager.getCurrSid(),hCount,1);
			   EHHistoryPanel.iTotalCount = Integer.parseInt(hCount.get(0).toString());
			   hCount.clear();
			   hCount = null;
			   // End of fix
			   int iStart 	= EHHistoryPanel.iTotalCount - EHHistoryPanel.iMaximumRows;
			   int iEnd 	= EHHistoryPanel.iTotalCount;
			   Vector hFiles =new Vector();
			   HistPane.EHD.setCursor(new Cursor(Cursor.WAIT_CURSOR));
			   AIPManager.getVWC().getAIPHistoryFiles(AIPManager.getCurrSid(),hFiles,iStart,iEnd,1);
			   HistPane.PFL.LoadTableData(hFiles);
			   HistPane.EHD.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			   Lastbtn.setEnabled(false);
			   Nextbtn.setEnabled(false);
			   EHHistoryPanel.iStartIndex 	= iStart;
			   EHHistoryPanel.iEndIndex	= iEnd;            
		   }
		   
	   }
   }
   
   class TextFieldListener implements FocusListener{
	   public void focusGained(FocusEvent e){
		   
	   }
	   public void focusLost(FocusEvent e){
		   JTextField source = (JTextField)e.getSource();
		   String sMaxRow = source.getText(); 
		   if(sMaxRow.trim().length() > 0)
		   {
			   int maxRow =Integer.parseInt(sMaxRow);
			   if (maxRow==0){
				   JOptionPane.showMessageDialog(null,connectorManager.getString("ProcessFileLayout.Rows"),connectorManager.getString("ProcessFileLayout.AIP"),JOptionPane.OK_OPTION);
				   source.setText("15");	  			   
			   }
			   EHHistoryPanel.iMaximumRows = Integer.parseInt(source.getText());
			   AIPManager.setMaximumRow(EHHistoryPanel.iMaximumRows);
		   }
	   }
   }
   
}

