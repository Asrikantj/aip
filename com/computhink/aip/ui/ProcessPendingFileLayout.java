package com.computhink.aip.ui;

import com.computhink.aip.*;
import com.computhink.aip.resource.ResourceManager;
import com.computhink.aip.resource.images.ImageManager;
import com.computhink.aip.util.AIPUtil;
import com.computhink.aip.util.VWLogger;

import com.computhink.common.Constants;
import com.computhink.common.DocType;
import com.computhink.common.Document;
import com.computhink.aip.client.*;
import com.computhink.common.ViewWiseErrors;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;

import java.util.*;


import java.io.File;
import java.text.SimpleDateFormat;
/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public class ProcessPendingFileLayout extends JPanel {

	private Vector Tabrows;
	public JTable table;
	private boolean isTableEdit =false;
	SpecialTableModel TableModel;
	EHPendingPanel PendingPane;
	EyesHandsManager EHManager =new EyesHandsManager();
	Vector EHForms;
	Vector VWDoctypes;
	Vector refreashNodesRooms = new Vector();
	/**CV10.2 - Added for restarting the Pending Process if it is hanging. Added by Vanitha S **/
	java.util.Timer pendProcessMonitoringTimer =null;
	PendProcessMonitoringThread pendProcessMonitoringThread = null;
	private DocumentProcessStatus pendingProcessDocDetails = null;
	private DocumentProcessStatus pendingProcessMonitoringDetails = null;
	public static boolean isPendingProcessStopped = false;
	/*
	 * Issue / Enhancement 	: Enhancements of AIP  
	 * 						  Requirment sent by Rajesh on 09-June-2006 through eMail  	 
	 * Created By				: M.Premananth
	 * Date					: 14-June-2006
	 */
	JTextField 	Pagecounttxt;
	JButton 		Firstbtn;
	JButton		Previousbtn;
	JButton		Nextbtn;
	JButton		Lastbtn;
	private ImageManager IM = new ImageManager();


	private final int MARGINCOL    =0 ;  private final int EHFILECOL    =1 ;
	private final int FCREATECOL   =2 ;  private final int FPROCESSCOL  =3 ;
	//private final int IMPORTCOL    =4 ;  
	public final int PENDINGCOL    =4 ;
	private final int EHFILEIDCOL  =5 ;  private final int STATE        =6 ;
	private final int COLCOUNT     =7 ;
	
	//resource manager used to get static strings used in interface or other places
	private final ResourceManager connectorManager = ResourceManager.getDefaultManager();

	// modified by amaleh on 2003-07-17   
	public ProcessPendingFileLayout(EHPendingPanel pendingPane1) {
		PendingPane = pendingPane1;
		setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		setAlignmentX(Component.LEFT_ALIGNMENT);

		/*
		 * Issue / Enhancement 	: Enhancements of AIP  
		 * 						  Requirment sent by Rajesh on 09-June-2006 through eMail  	 
		 * Created By				: M.Premananth
		 * Date					: 14-June-2006
		 */
		JPanel informationPanel = new JPanel();
		informationPanel.setLayout(new BoxLayout(informationPanel,BoxLayout.X_AXIS));
		informationPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		JLabel MapLab= new JLabel(AIPUtil.getString("Pending.Lab.PendingFiles"));
		MapLab.setFont(new Font(MapLab.getFont().getName(),Font.BOLD,MapLab.getFont().getSize()));
		MapLab.setAlignmentX(Component.LEFT_ALIGNMENT);
		informationPanel.add(MapLab);
		informationPanel.add(Box.createHorizontalStrut(254));
		ButtonListener btnListener =new ButtonListener();
		JLabel maxrow	= new JLabel(connectorManager.getString("Pending.Lab.MaximumRows"));
		int iMaximumRow = AIPManager.getMaximumRow();
		if(iMaximumRow <= 0)
			AIPManager.setMaximumRow(iMaximumRow = 15);
		EHPendingPanel.iMaximumRows = iMaximumRow;
		VWLogger.info("Maximum Rows to be displayed on pending page table :"+iMaximumRow);
		Pagecounttxt 	= new JTextField(String.valueOf(EHPendingPanel.iMaximumRows),15); 
		Pagecounttxt.setMaximumSize(new Dimension(45,21));
		Pagecounttxt.setPreferredSize(new Dimension(45,21));
		Firstbtn  		= new JButton();
		Firstbtn.setIcon(IM.getImageIcon("easfirst.gif"));
		Firstbtn.setToolTipText(AIPUtil.getString("History.Btn.First"));

		Previousbtn  		= new JButton();
		Previousbtn.setIcon(IM.getImageIcon("easprev.gif"));
		Previousbtn.setToolTipText(AIPUtil.getString("History.Btn.Previous"));

		Nextbtn  		= new JButton();
		Nextbtn.setIcon(IM.getImageIcon("easnext.gif"));
		Nextbtn.setToolTipText(AIPUtil.getString("History.Btn.Next"));

		Lastbtn  		= new JButton();
		Lastbtn.setIcon(IM.getImageIcon("easlast.gif"));
		Lastbtn.setToolTipText(AIPUtil.getString("History.Btn.Last"));


		TextFieldListener txtListener = new TextFieldListener();
		Pagecounttxt.addFocusListener(txtListener);
		Firstbtn.addActionListener(btnListener);
		Previousbtn.addActionListener(btnListener);
		Nextbtn.addActionListener(btnListener);
		Lastbtn.addActionListener(btnListener);

		Firstbtn.setPreferredSize(new Dimension(20,14));
		Firstbtn.setMinimumSize(new Dimension(20,14));
		Previousbtn.setPreferredSize(new Dimension(20,14));
		Previousbtn.setMinimumSize(new Dimension(20,14));
		Nextbtn.setPreferredSize(new Dimension(20,14));
		Nextbtn.setMinimumSize(new Dimension(20,14));
		Lastbtn.setPreferredSize(new Dimension(20,14));
		Lastbtn.setMinimumSize(new Dimension(20,14));
		// Issue 544
		Firstbtn.setEnabled(false);
		Previousbtn.setEnabled(false);
		// Issue 544   
		Lastbtn.setEnabled(true);
		Nextbtn.setEnabled(true);

		informationPanel.add(maxrow);
		informationPanel.add(Pagecounttxt);
		informationPanel.add(Box.createHorizontalStrut(30));
		informationPanel.add(Firstbtn);
		informationPanel.add(Box.createHorizontalStrut(2));
		informationPanel.add(Previousbtn);
		informationPanel.add(Box.createHorizontalStrut(5));
		informationPanel.add(Nextbtn);
		informationPanel.add(Box.createHorizontalStrut(2));
		informationPanel.add(Lastbtn);
		add(informationPanel);
		add(Box.createVerticalStrut(5));
		add(createProcessTablePanel());
		
		for(int i=0; i<table.getColumnCount(); i++){
	    	TableColumn column = table.getColumnModel().getColumn(i);
	    	if (i==MARGINCOL){
				column.setPreferredWidth(18);
				column.setMaxWidth(18);
				column.setCellRenderer(new ButtonRenderer());
				column.setCellEditor(new ButtonEditor());
			}else if (i == EHFILECOL){
				column.setMaxWidth(450);
				column.setMinWidth(345);
				column.setPreferredWidth(345);
			}else if (i == FCREATECOL){
				column.setMaxWidth(450);
				column.setMinWidth(115);
				column.setPreferredWidth(115);
			}else if (i == FPROCESSCOL){
				column.setMaxWidth(450);
				column.setMinWidth(115);
				column.setPreferredWidth(115);
			}/*else if (i == IMPORTCOL){
               column.setMaxWidth(450);
               column.setPreferredWidth(30);
           }*/else if (i == PENDINGCOL){
        	   column.setMaxWidth(450);
        	   column.setMinWidth(70);
        	   column.setPreferredWidth(70);
           }
	    }
	}
	public JScrollPane createProcessTablePanel(){
		Tabrows =new Vector();

		TableModel  = new SpecialTableModel();
		table = new JTable(TableModel);
		table.setRowHeight(20);
		table.getTableHeader().setReorderingAllowed(false);
		table.getTableHeader().setMinimumSize(new Dimension(30,21));
		JLabel lblHeader=(JLabel)table.getTableHeader().getDefaultRenderer();
		lblHeader.setHorizontalAlignment(SwingConstants.CENTER);
		//Below code is commented, because of problem with the header resize(header was breaking up).
		//table.getTableHeader().setPreferredSize(new Dimension(100,21));
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		table.getTableHeader().setAlignmentX(LEFT_ALIGNMENT) ;
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		TableMouseListener MListener =new TableMouseListener();
		this.addMouseListener(MListener);
		table.addMouseListener(MListener);
		table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		ListSelectionModel SelectionModel= table.getSelectionModel();
		SelectionModel.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				/**CV10.2 - Added for restarting the Pending Process if it is hanging. Added by Vanitha S **/
				VWLogger.debug("ListSelectionModel PendingPane.EHD.Getcursor :"+PendingPane.EHD.getCursor());
				if (PendingPane.EHD.getCursor().toString().contains("Wait Cursor")) {
					//PendingPane.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
					return;
				}
				tableSelectionChanged(e);
			}
		});
		JScrollPane tableScroller = new JScrollPane(table);

		/*for(int i=0;i<table.getColumnCount();i++){
			TableColumn column = table.getColumnModel().getColumn(i);
			if (i==MARGINCOL){
				column.setPreferredWidth(18);
				column.setMaxWidth(18);
				column.setCellRenderer(new ButtonRenderer());
				column.setCellEditor(new ButtonEditor());
			}else if (i == EHFILECOL){
				column.setMaxWidth(450);
				column.setPreferredWidth(155);
			}else if (i == FCREATECOL){
				column.setMaxWidth(450);
				column.setPreferredWidth(70);
			}else if (i == FPROCESSCOL){
				column.setMaxWidth(450);
				column.setPreferredWidth(70);
			}else if (i == PENDINGCOL){
        	   column.setMaxWidth(450);
        	   column.setPreferredWidth(30);
           }
		}
		table.repaint();*/

		//tableScroller.setAlignmentX(Component.LEFT_ALIGNMENT);

		JTableHeader header = table.getTableHeader();
		header.addMouseListener(TableModel.new ColumnListener(this));
		header.setDefaultRenderer(
				TableModel.new SortableHeaderRenderer(header.getDefaultRenderer()));
		tableScroller.setAlignmentX(Component.LEFT_ALIGNMENT);

		return tableScroller;
	}

	public void LoadTableData(Vector TData){
		if(TData !=null){
			if(TData.size() >0){
				for(int i=0;i<TData.size();i++){
					Tabrows.add(getTableRowOfInfo((AIPFile)TData.get(i)));
					TableModel.fireTableRowsInserted(Tabrows.size()-1,Tabrows.size()-1);
				}
			}
		}
	}
	public void addTableRow(AIPFile Info ){
		Tabrows.add(getTableRowOfInfo(Info));
		TableModel.fireTableRowsInserted(Tabrows.size()-1,Tabrows.size()-1);
	}
	public void DeleteSelectedRows(){
		int[]  Rows = table.getSelectedRows();
		for(int i=Rows.length-1; i >= 0;i--){
			int fileid =((Integer)TableModel.getValueAt(Rows[i],EHFILEIDCOL)).intValue();
			AIPFile aipFile =new AIPFile();
			aipFile.setId(fileid);
			int ret =AIPManager.getVWC().deleteAIPHistoryFile(AIPManager.getCurrSid(),aipFile);
			if(ret !=AIPManager.NOERROR)
			{    
				JOptionPane.showMessageDialog(this,AIPManager.getErrorDescription(ret)+"("+ret+")",AIPUtil.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);
				return ;              
			}               
			TableModel.RemoveRow(Rows[i]);
		}
	}
	public void ProcessSelectedRows(){
		VWLogger.audit("Pending documents started processing..... ");
		try{
			PendingPane.EHD.PManager =new PagesManager();
		}
		catch (java.lang.UnsatisfiedLinkError e){
			JOptionPane.showMessageDialog(this,AIPUtil.getString("Init.Msg.LostLibrary"),AIPUtil.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);
			return;
		}
		ReLoadEHForms();
		VWLogger.info("CurrentThreadName 0 :"+Thread.currentThread().getName()+" isInterrupted :"+Thread.currentThread().isInterrupted());
		int[]  Rows = table.getSelectedRows();
		refreashNodesRooms =new Vector();
		for(int i=Rows.length-1; i >= 0;i--){	
			/**CV10.2 - Added for restarting the Pending Process if it is hanging. Added by Vanitha S **/		
			AIPFile aipFile=(AIPFile)getAIPFileInfoOfRow(Rows[i]);
			VWLogger.info("aipFile.getPending()....."+aipFile.getPending());
			VWLogger.info("CurrentThreadName 1 :"+Thread.currentThread().getName()+" isInterrupted :"+Thread.interrupted());
			/***This condition is added bz already processed document should not process again after restarting the pending peocess  **/
			if (Thread.currentThread().isInterrupted() || isPendingProcessStopped) {
				VWLogger.info("Reprocessing of Pending Documents ended by force stop ..1");
				return;
			}
			ProcessAIPFile(aipFile);
			/*This condition is added bz sometimes JNI methods taking time to process the records.That time process monitoring thread will restart the process. 
			current thread process also will be running in background and new thread also will start the same process.
			either one of the thread will complete the process soon. That time  isPendingProcessStopped value will be true.
			Second thread process will be terminated after seeing the isPendingProcessStopped=true value. Bz no need to continue the same process. bz it is already done.
			After restarting the new thread,it will continue the process, that time old thread will be interrupted. Thread.currentThread().isInterrupted() for old thread will be true, no need to continue the same process.*/ 
			/*if (isPendingProcessStopped) {
				VWLogger.info("Reprocessing of Pending Documents ended by force stop ..0");
				return ;
			}*/
			if (Thread.currentThread().isInterrupted() || isPendingProcessStopped) {
				VWLogger.info("Reprocessing of Pending Documents ended by force stop ..9");
				return;
			}
			VWLogger.info("After ProcessAIPFile aipFile.getProcessDate() :"+aipFile.getProcessDate());
			VWLogger.info("After ProcessAIPFile aipFile.getPending() :"+aipFile.getPending());
			TableModel.setValueAt(aipFile.getProcessDate(),Rows[i],FPROCESSCOL );
			TableModel.setValueAt(new Integer(aipFile.getPending()),Rows[i],PENDINGCOL );
			TableModel.fireTableRowsUpdated(Rows[i],Rows[i]);
			VWLogger.info("After fireTableRowsUpdated");
		}		
		PendingPane.EHD.PManager =null;
		VWLogger.audit("Selected Pending documents processed..... ");
	}
	public void ProcessAIPFile(AIPFile aipFile){
		ProcessAIPFile(aipFile, 0);
	}
	public void ProcessAIPFile(AIPFile aipFile, int pendingStatus){
		VWLogger.info("Reprocessing file with ID: " + aipFile.getId());
		VWLogger.info("Reprocessing bif file : "+aipFile.getFilePath());
		int delay = 0;
		try{
			Vector pendingDocs =new Vector();  
			int ret =AIPManager.getVWC().getAIPDocuments(AIPManager.getCurrSid(),aipFile,pendingDocs, pendingStatus);  
			/*			
			 * We need to call filterLockedPendingDocuments 
			 
			if (pendingStatus == 13){
				Vector filterdVector = new Vector(); 
				filterdVector  = filterLockedPendingDocs(pendingDocs);
				pendingDocs.removeAllElements();
				pendingDocs.addAll(filterdVector); 
			}*/
			if(ret!=AIPManager.NOERROR)
			{    
				JOptionPane.showMessageDialog(this,AIPManager.getErrorDescription(ret)+"("+ret+")",AIPUtil.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);
				return ; 
			}    
			
			if(pendingDocs != null && pendingDocs.size() >0){
				for(int i=0;i<pendingDocs.size();i++)
				{
					ret = AIPManager.getVWC().getAIPDocFields(AIPManager.getCurrSid(),(AIPDocument)pendingDocs.get(i));
					/**
					 * This code has been added for giving a delay between document segments
					 * Vijaykumar.S.P - 03 March 2011
					 */
					if(AIPManager.getDocumentDelay()>0){
						try{
							delay = (1000*AIPManager.getDocumentDelay());
							VWLogger.info("Adding a delay between Pending Documents : "+AIPManager.getDocumentDelay()+" Seconds");
							Thread.currentThread().sleep(delay);			// To add delay between the document segments
						}catch(InterruptedException e){
							VWLogger.info("Exception while adding delay to the document segments "+e.getMessage());
						}
					}
				} 
			}
			int currSid =AIPManager.getCurrSid();
			VWLogger.info("Pending Processing File Name and Id " + aipFile.getFilePath() + " ::: " + aipFile.getId());
			VWLogger.info("Before Process Pending Documents - Imported Documents " + aipFile.getImported() + " ::: Pending Documents " + aipFile.getPending());
			/**CV10.2 - Added for restarting the Pending Process if it is hanging. Added by Vanitha S **/
			int docImport= ReprocessAIPFileDocuments(pendingDocs, aipFile.getFilePath());
			if (Thread.currentThread().isInterrupted() || isPendingProcessStopped) {
				VWLogger.info("Reprocessing of Pending Documents ended by force stop ..9");
				return;
			}
			if (docImport != -1) {
				VWLogger.info("After Processing Pending documents - Imported Documents " + aipFile.getImported() + " ::: Pending Documents " + aipFile.getPending());
				if (docImport >= 0) {
					if (pendingProcessDocDetails != null && pendingProcessMonitoringDetails != null) {
						pendingProcessDocDetails.setProcessStatus(1);
						pendingProcessMonitoringDetails.setProcessStatus(1);
					}
				}
				AIPManager.setCurrSid(currSid);
				aipFile.setImported(aipFile.getImported()+docImport);
				aipFile.setPending(aipFile.getPending()-docImport);
				aipFile.setProcessDate(AIPUtil.SetDateMaskedValue(AIPUtil.MASKDATE,new Date()));
				AIPManager.getVWC().setAIPHistoryFile(AIPManager.getCurrSid(),aipFile);  
	
				VWLogger.audit("File:<" + aipFile.getFilePath() + ">\t" + "Processed:<" + aipFile.getProcessDate() + ">\t" + "Docs Imported:<" + aipFile.getImported() + ">\t" + "Docs Pending:<" + aipFile.getPending() + ">");
			} /*else {
				Calendar date = Calendar.getInstance();
				String dateAndTime = date.getTime().toString();
				pendProcessMonitoringTimer.cancel();
				pendProcessMonitoringTimer = null;
				pendingProcessDocDetails.setDocumentName("99999");
				pendingProcessDocDetails.setProcessStatus(1);
				pendingProcessDocDetails.setProcessStartTime(dateAndTime);
				date = null;
			}*/
		}
		catch (Exception e){
			VWLogger.error(" in Processing history Saved File <"+aipFile.getFilePath()+"> :",e );
			return ;
		}

	}
	
	public Vector filterLockedPendingDocs(Vector pendingDocs){
		//VWLogger.info("filterLockedPendingDocs...");
		Vector filterDocs = new Vector();
		// Remove the other than pending status code 13 
		for (int i = 0; i < pendingDocs.size(); i++){
			AIPDocument aipDoc = (AIPDocument) pendingDocs.get(i);
			if (aipDoc.getPendCause() == 13){
				filterDocs.add(pendingDocs.get(i));
			}		
		}
		return filterDocs;
	}

	public synchronized int ReprocessAIPFileDocuments(Vector aipDocs, String bifFilePath) {
		int DocSaveNum = 0;
		int deletedDocs = 0;
		int isPrimitiveDocs = 0;
		int isConvertToDJVU = 0;
		/**CV10.2 - Added for restarting the Pending Process if it is hanging. Added by Vanitha S **/
		try {
			VWLogger.info("isPendingProcessStopped......."+isPendingProcessStopped);			
			for (int i = 0; i < aipDocs.size(); i++) {
				/**isPendingProcessStopped is true means main thread process has completed before child thread process starts**/
				/*if (isPendingProcessStopped) {
					VWLogger.info("Reprocessing of Pending Documents ended by force stop ..1");
					return -1;
				}*/
				/**Thread.interrupted() is true means new thread has initiated. New thread will handle the pending process. so no need to continue on previous thread process 
				   This case will come rarely. Bz sometimes JNI call will take time to process the images. That time monitoring thread will restart the service. New thread will create to process the pending documents 
				**/
				if (Thread.currentThread().isInterrupted() || isPendingProcessStopped) {
					VWLogger.info("Reprocessing of Pending Documents ended by force stop ..2");
					return -1;
				}
				AIPDocument aipDoc = (AIPDocument) aipDocs.get(i);
				String Name = aipDoc.getName();
				VWLogger.info("Processing document.... "+Name);
				setPendProcessDocDetails(Name);
				VWLogger.debug("Add Pending AIPDocument <" + aipDoc.getName() + "> To " + Constants.PRODUCT_NAME);
				/* added by amaleh on 2003-06-20 */
				Document vwDoc = null;
				AIPForm aipForm;
				int sid = AIPManager.getCurrSid();
				isPrimitiveDocs = AIPManager.isPrimitiveDocs();
				isConvertToDJVU = AIPManager.isConvertToDJVU();
				// now checks for whether aipDoc has field names or not. That
				// way, it works with or without templates.
				if ((aipDoc.getFieldNames() != null) && (!aipDoc.getFieldNames().trim().equals(""))) {
					try {
						Session s = null;
						if (DocumentsManager.calcDepth(aipDoc.getVWNodePath()) >= 4) {
			                s =DocumentsManager.getSessionOfNodePath(aipDoc.getVWNodePath());
							if (s == null) {
								VWLogger.error(" Room was not found in combo box!", null);
								aipDoc.setStatus(aipDoc.PENDING);
								aipDoc.setPendCause(12);
								String docImpMsg = aipDoc.getName() + " is pending because of error " + aipDoc.getPendCause() + ": " + AIPUtil.getString("Pending.Status." + aipDoc.getPendCause());
								VWLogger.audit(EHGeneralPanel.formatDocMsg(aipDoc, docImpMsg));
								AIPManager.getVWC().setAIPDocument(AIPManager.getCurrSid(), aipDoc);
								continue;
							}
						} else {
							s = AIPManager.getSession(AIPManager.getCurrSid());
						}
						sid = s.sessionId;
						if (sid <= 0) {
							sid = PendingPane.EHD.GenPane.internalConnect(s);
							if (sid <= 0) {
								VWLogger.error(" Cannot connect to the destination Room!", null);
								aipDoc.setStatus(aipDoc.PENDING);
								aipDoc.setPendCause(12);
								String docImpMsg = aipDoc.getName() + " is pending because of error " + aipDoc.getPendCause() + ": " + AIPUtil.getString("Pending.Status." + aipDoc.getPendCause());
								VWLogger.audit(EHGeneralPanel.formatDocMsg(aipDoc, docImpMsg));
								AIPManager.getVWC().setAIPDocument(AIPManager.getCurrSid(), aipDoc);
								continue;
							}
							refreashNodesRooms.add(Integer.toString(sid));
	
						} else {
							if (!refreashNodesRooms.contains(Integer.toString(sid))) {
								DocumentsManager.getNavigatorNode(sid, true);
								refreashNodesRooms.add(Integer.toString(sid));
	
							}
						}					
						vwDoc = EHManager.TransferAIPDocToVWDoc(sid, null, aipDoc);
						if (vwDoc == null) {
							// aipDoc.setStatus(aipDoc.PENDING);
							// aipDoc.setPendCause(9);
							String docImpMsg = aipDoc.getName() + " is pending because of error " + aipDoc.getPendCause() + ": " + AIPUtil.getString("Pending.Status." + aipDoc.getPendCause());
							VWLogger.audit(EHGeneralPanel.formatDocMsg(aipDoc, docImpMsg));
							AIPManager.getVWC().setAIPDocument(AIPManager.getCurrSid(), aipDoc);
							continue;
						}
					} catch (Exception e) {
						VWLogger.info("Exception while calling getSessionOfNodePath/TransferAIPDocToVWDoc method in AddNewAIPDocumentsToVW :"+e.getMessage());
					}

				} else // Eyes Hands Process
				{
					try {
						if (!refreashNodesRooms.contains(Integer.toString(sid))) {
							DocumentsManager.getNavigatorNode(sid, true);
							refreashNodesRooms.add(Integer.toString(sid));
						}
						aipForm = getAIPForm(aipDoc.getAIPForm().getName());
						if (aipForm == null || aipDoc.getAIPForm().getName().equals("")) {
							aipDoc.setPendCause(1);
							AIPManager.getVWC().setAIPDocument(AIPManager.getCurrSid(), aipDoc);
							String docImpMsg = aipDoc.getName() + " is pending because of error " + aipDoc.getPendCause() + ": " + AIPUtil.getString("Pending.Status." + aipDoc.getPendCause());
							VWLogger.audit(EHGeneralPanel.formatDocMsg(aipDoc, docImpMsg));
							continue;
						}
						String FormName = aipForm.getName();
						if (aipForm.getStatus() == aipForm.FIELDSNEEDMAP) {
							aipDoc.setPendCause(3);
							AIPManager.getVWC().setAIPDocument(AIPManager.getCurrSid(), aipDoc);
							String docImpMsg = aipDoc.getName() + " is pending because of error " + aipDoc.getPendCause() + ": " + AIPUtil.getString("Pending.Status." + aipDoc.getPendCause());
							VWLogger.audit(EHGeneralPanel.formatDocMsg(aipDoc, docImpMsg));
							continue;
						}
						if (aipForm.getMapDTId() <= 0) {
							aipDoc.setPendCause(2);
							AIPManager.getVWC().setAIPDocument(AIPManager.getCurrSid(), aipDoc);
							String docImpMsg = aipDoc.getName() + " is pending because of error " + aipDoc.getPendCause() + ": " + AIPUtil.getString("Pending.Status." + aipDoc.getPendCause());
							VWLogger.audit(EHGeneralPanel.formatDocMsg(aipDoc, docImpMsg));
							continue;
						}
	
						DocType dt = DoctypesManager.getDoctype(AIPManager.getCurrSid(), aipForm.getMapDTId());
						vwDoc = EHManager.TransferAIPDocToVWDoc(null, aipDoc, aipForm, dt);
						if (vwDoc == null) {
							String docImpMsg = aipDoc.getName() + " is pending because of error " + aipDoc.getPendCause() + ": " + AIPUtil.getString("Pending.Status." + aipDoc.getPendCause());
							VWLogger.audit(EHGeneralPanel.formatDocMsg(aipDoc, docImpMsg));
	
							AIPManager.getVWC().setAIPDocument(AIPManager.getCurrSid(), aipDoc);
							continue;
						}
					} catch (Exception e) {
						VWLogger.info("Exception while calling getDoctype/TransferAIPDocToVWDoc method in AddNewAIPDocumentsToVW :"+e.getMessage());
					}
				}
				/*if (isPendingProcessStopped) {
					VWLogger.info("Reprocessing of Pending Documents ended by force stop ..1");
					return -1;
				}*/
				if (Thread.currentThread().isInterrupted() || isPendingProcessStopped) {
					VWLogger.info("Reprocessing of Pending Documents ended by force stop ..3");
					return -1;
				}
				File[] imageArray = null;
				try {
					imageArray = PendingPane.EHD.PManager.parseImageFiles(aipDoc.getImagesPath());
					VWLogger.info("parseImageFiles return value .... "+imageArray);
					if (imageArray == null) {
						aipDoc.setPendCause(4);
						VWLogger.info("pendingErrorMessage :: " + PagesManager.pendingErrorMessage);
						if (PagesManager.pendingErrorMessage == 2) {
							aipDoc.setPendCause(22);
						} else if (PagesManager.pendingErrorMessage == 7) {
							aipDoc.setPendCause(27);					
						}
						PagesManager.pendingErrorMessage = 0;
						String docImpMsg = aipDoc.getName() + " is pending because of error " + aipDoc.getPendCause() + ": " + AIPUtil.getString("Pending.Status." + aipDoc.getPendCause());
						VWLogger.audit(EHGeneralPanel.formatDocMsg(aipDoc, docImpMsg));
						AIPManager.getVWC().setAIPDocument(AIPManager.getCurrSid(), aipDoc);
						continue;
					}
				} catch (Exception e) {
					VWLogger.info("Exception while calling parseImageFiles method in AddNewAIPDocumentsToVW :"+e.getMessage());
				}
				/*if (isPendingProcessStopped) {
					VWLogger.info("Reprocessing of Pending Documents ended by force stop ..1");
					return -1;
				}*/
				if (Thread.currentThread().isInterrupted() || isPendingProcessStopped) {
					VWLogger.info("Reprocessing of Pending Documents ended by force stop ..4");
					return -1;
				}
				/**
				 * Added for sending the document to pending if the size is more than 100MB(Default)
				 *  or based on the maxdocsize registry set value.
				 *  New pend cause added as 19 if the document size is more than 100MB or registry set value.
				*/
				try {
					VWLogger.audit("Before calling getAllowedPageSize.....");
					boolean retflag=PendingPane.EHD.PManager.getAllowedPageSize(aipDoc.getImagesPath(), imageArray);				
					VWLogger.audit("Allowed Page Size return value : <"+retflag+">");
					if(retflag==false){
						try {
							aipDoc.setStatus(aipDoc.PENDING);         
							if (PagesManager.pendingErrorMessage == 5) {// Page file size exceeded 250 MB.
								aipDoc.setPendCause(25);
							} else {
								aipDoc.setPendCause(19); // Image/Page file size is more
							}
							PagesManager.pendingErrorMessage = 0;			 						
							String docImpMsg = aipDoc.getName() + " is pending because of error " + aipDoc.getPendCause() + ": " + AIPUtil.getString("Pending.Status." + aipDoc.getPendCause());
							VWLogger.audit(EHGeneralPanel.formatDocMsg(aipDoc, docImpMsg));
							AIPManager.getVWC().setAIPDocument(AIPManager.getCurrSid(), aipDoc);
						} catch (Exception e) {
							VWLogger.info("Exception in setPendCause(19) :"+e.getMessage());
						}
						continue;
					}
				} catch (Exception e) {
					VWLogger.info("Exception while calling getAllowedPageSize method in AddNewAIPDocumentsToVW :"+e.getMessage());
				}
				/*if (isPendingProcessStopped) {
					VWLogger.info("Reprocessing of Pending Documents ended by force stop ..1");
					return -1;
				}*/
				if (Thread.currentThread().isInterrupted() || isPendingProcessStopped) {
					VWLogger.info("Reprocessing of Pending Documents ended by force stop ..5");
					return -1;
				}
				DocumentsManager DocsManager = new DocumentsManager();
				int VWDocId = 0;
				VWLogger.info("aipDoc.isAppend() :: " + aipDoc.isAppend());
				if (aipDoc.isAppend()) {
					VWDocId = DocsManager.getIDofDoc(sid, vwDoc);
					if (VWDocId > 0 && imageArray.length > 0) {
					try {
							VWLogger.debug("pull Pages from document <" + VWDocId + ">");
							boolean bGetPages = PendingPane.EHD.PManager.pullDocumentPages(sid, VWDocId);
							if (!bGetPages) {
								/***Added to fix issue - AIP creating document with 0 pages when the DSS is inactive.Added by vanitha on 17/01/2020 ***/
								VWLogger.info("pullDocumentPages method error message : "+PagesManager.pendingErrorMessage);
								if (PagesManager.pendingErrorMessage < 0) {
									aipDoc.setStatus(aipDoc.PENDING);
									if(PagesManager.pendingErrorMessage==ViewWiseErrors.dssCriticalError){
										aipDoc.setPendCause(15);
									} else if(PagesManager.pendingErrorMessage == ViewWiseErrors.dssInactive){
										aipDoc.setPendCause(11);
									} else {
										aipDoc.setPendCause(5);
									}
									PagesManager.pendingErrorMessage = 0;
									String docImpMsg = "ProcessPendingFileLayout : " + aipDoc.getName()+" is pending because of error " + aipDoc.getPendCause() + ": "+AIPUtil.getString("Pending.Status." + aipDoc.getPendCause());
									VWLogger.audit(EHGeneralPanel.formatDocMsg(aipDoc, docImpMsg));
									AIPManager.getVWC().setAIPDocument(sid, aipDoc);
									continue;
								}	
							} else {
								VWLogger.debug("add new Pages to document <" + VWDocId + ">");
								// if(PendingPane.EHD.PManager.addPagesToDocument(sid,VWDocId,imageArray,vwDoc))
								int ret = PendingPane.EHD.PManager.addPagesToDocument(sid, VWDocId, imageArray, vwDoc);
								VWLogger.info("ProcessPendingFileLayout : return value from addPagesToDocument() : " + ret + " : " + AIPManager.getErrorDescription(ret));
	
								if (ret == ViewWiseErrors.NoError)
									VWLogger.debug("Pages added successfully ");
								else {
									aipDoc.setStatus(aipDoc.PENDING);
									if (PagesManager.pendingErrorMessage > 0) {
										if (PagesManager.pendingErrorMessage == 4) {// Not enough memory
											aipDoc.setPendCause(23);
										} else {
											if (isConvertToDJVU != 3) {
												aipDoc.setPendCause(24); // Failed to create all.zip
											} else {
												aipDoc.setPendCause(26); // Failed to convert HC-PDF
											}
										}
										PagesManager.pendingErrorMessage = 0;
									} else {
										if (ret == ViewWiseErrors.documentAlreadyLocked) {
											aipDoc.setPendCause(13);
										} else if (ret == ViewWiseErrors.pageCountMismatch) {
											aipDoc.setPendCause(21);
										} else
											aipDoc.setPendCause(14);
									}
									AIPManager.getVWC().setAIPDocument(AIPManager.getCurrSid(), aipDoc);
									String docImpMsg = "ProcessPendingFileLayout :::  " + aipDoc.getName() + " is pending because of error " + aipDoc.getPendCause() + ": "	+ AIPUtil.getString("Pending.Status." + aipDoc.getPendCause());
									VWLogger.audit(EHGeneralPanel.formatDocMsg(aipDoc, docImpMsg));
									continue;
								}
								vwDoc.setId(VWDocId);
							}
						} catch (Exception e) {
							VWLogger.info("Exception while pulling and adding pages to documents :"+e.getMessage());
						}
					}
				}
				/*if (isPendingProcessStopped) {
					VWLogger.info("Reprocessing of Pending Documents ended by force stop ..1");
					return -1;
				}*/
				if (Thread.currentThread().isInterrupted() || isPendingProcessStopped) {
					VWLogger.info("Reprocessing of Pending Documents ended by force stop ..6");
					return -1;
				}
				if (VWDocId <= 0) {
					if (imageArray.length > 0) {
						try {
							String docFilePath = PagesManager.createPagesTempFolder(sid, VWDocId);
							/*
							 * Issue - 742 Developer - Nebu Alex Code Description -
							 * The following piece of code implements Generate
							 * primitive document functionalities
							 */
							boolean findPages = false;
							if (isPrimitiveDocs == 1 && isConvertToDJVU == 0) {
								findPages = PendingPane.EHD.PManager.addPagesToTempFolderPrimitiveGeneration(sid, VWDocId, imageArray);
							} else {
								findPages = PendingPane.EHD.PManager.addPagesToTempFolder(sid, VWDocId, imageArray);
							}
							// boolean
							// findPages=PendingPane.EHD.PManager.addPagesToTempFolder(sid,VWDocId,imageArray);
							// End of fix
							if (!findPages) {
								aipDoc.setStatus(aipDoc.PENDING);
								aipDoc.setPendCause(4);
								VWLogger.info("pendingErrorMessage :: " + PagesManager.pendingErrorMessage);
								if (PagesManager.pendingErrorMessage == 3) {
									aipDoc.setPendCause(21);					
								} else if (PagesManager.pendingErrorMessage == 4) {
									aipDoc.setPendCause(23);
								} else if (PagesManager.pendingErrorMessage == 5) {
									aipDoc.setPendCause(25);
								} else if (PagesManager.pendingErrorMessage == 6) {
									if (isConvertToDJVU != 3) {
										aipDoc.setPendCause(24);
									} else {
										aipDoc.setPendCause(26); // Failed to convert HC-PDF
									}
								}
								PagesManager.pendingErrorMessage = 0;
								AIPManager.getVWC().setAIPDocument(AIPManager.getCurrSid(), aipDoc);
								String docImpMsg = aipDoc.getName() + " is pending because of error " + aipDoc.getPendCause() + ": " + AIPUtil.getString("Pending.Status." + aipDoc.getPendCause());
								VWLogger.audit(EHGeneralPanel.formatDocMsg(aipDoc, docImpMsg));
								continue;
							} else
								vwDoc.setVersionDocFile(docFilePath);
						} catch (Exception e) {
							VWLogger.info("Exception while adding pages to temp folder .."+e.getMessage());
						}
					}
					/*if (isPendingProcessStopped) {
						VWLogger.info("Reprocessing of Pending Documents ended by force stop ..1");
						return -1;
					}*/
					VWLogger.info("CurrentThreadName 2 :"+Thread.currentThread().getName()+" isInterrupted :"+Thread.currentThread().isInterrupted());
					if (Thread.currentThread().isInterrupted() || isPendingProcessStopped) {
						VWLogger.info("Reprocessing of Pending Documents ended by force stop ..7");
						return -1;
					}
					try {
						int ret = AIPManager.getVWC().createDocument(sid, vwDoc);
						PendingPane.EHD.PManager.deletePagesTempFolder(sid, VWDocId);
						if (ret <= 0) {
							aipDoc.setStatus(aipDoc.PENDING);
							if (ret == ViewWiseErrors.dssCriticalError) {
								aipDoc.setPendCause(15);
							} // Added for Lock document type
							else if (ret == ViewWiseErrors.DocumentTypeNotPermitted) {
								aipDoc.setPendCause(16);
							} else if (ret == ViewWiseErrors.dssInactive) {
								aipDoc.setPendCause(11);
							} // CV10.1 Enhancement-Dynamic Sequence validation. Modified by rkant.
							else if (ret == ViewWiseErrors.CheckUniquenessIndexErr) {
								aipDoc.setPendCause(17);
							} else if (ret == ViewWiseErrors.InvalidDateMask) {
								aipDoc.setPendCause(20);
							} else if (ret == ViewWiseErrors.allDotZipDoesNotExist) {
								aipDoc.setPendCause(24);
							} else {
								aipDoc.setPendCause(5);
							}
							String docImpMsg = aipDoc.getName() + " is pending because of error " + aipDoc.getPendCause() + ": " + AIPUtil.getString("Pending.Status." + aipDoc.getPendCause());
							VWLogger.audit(EHGeneralPanel.formatDocMsg(aipDoc, docImpMsg));
							AIPManager.getVWC().setAIPDocument(AIPManager.getCurrSid(), aipDoc);
	
							continue;
						}
					} catch (Exception e) {
						VWLogger.info("Exception while calling createDocument in  AddNewAIPDocumentsToVW:"+e.getMessage());
					}
				}
				// if (ret > 0){
				try {
					VWLogger.debug("Security Info " + aipDoc.getSecurityInfo());
					StringTokenizer strSecurityTokens = new StringTokenizer(aipDoc.getSecurityInfo(), "|");
					while (strSecurityTokens.hasMoreTokens()) {
						StringTokenizer strSecurity = new StringTokenizer(strSecurityTokens.nextToken(), ",");
						String userGroup = strSecurity.nextToken();
						String permissions = strSecurity.nextToken();
						VWLogger.debug("Security Info  " + userGroup + " ::: " + permissions + " :::: " + vwDoc.getId());
						int selectedNodeId = 0;
						selectedNodeId = vwDoc.getId();
						int result = AIPManager.getVWC().copyNodeSecurity(sid, selectedNodeId, userGroup, permissions);
						switch (result) {
							case 0:
								VWLogger.info("Successfully Adding Security Entry for the : " + vwDoc.getName()	+ " - User/Group Name -> " + userGroup + " : Permissions -> " + permissions);
								break;
							case -1:
								VWLogger.info(vwDoc.getName() + " : User/Group Name -> " + userGroup + " not found!");
								break;
							case -2:
								VWLogger.info(vwDoc.getName() + " : User/Group Name -> " + userGroup + " : Principal not found!");
								break;
							case -3:
								VWLogger.info(vwDoc.getName() + " : User Group Name -> " + userGroup + " : Permissions -> "	+ permissions + " : Given Permission and effective Permission are same.");
								break;
							case -4:
								VWLogger.info(vwDoc.getName() + " : User Group Name -> " + userGroup + " : Permissions -> "	+ permissions + " : Given permission same as effective permission, remove the existing security permission!");
								break;
							default:
								VWLogger.info("Error While Adding Security Entry for the node " + vwDoc.getName() + " : User Group Name -> " + userGroup + " : Permissions -> " + permissions);
						}
					}
				} catch (Exception ex) {
					VWLogger.debug("Exception in reading the security " + ex.getMessage());
				}
				// }

				DocSaveNum++;
				if (aipDoc.getCommentFile() != null && !aipDoc.getCommentFile().trim().equals("")) {
					VWLogger.info("load comment from file <" + aipDoc.getCommentFile() + ">");
					String comment = DocsManager.loadComment(aipDoc.getCommentFile());
					if (comment != null) {
						int retc = DocsManager.setDocumentComment(sid, vwDoc, comment);
						if (retc <= 0)
							VWLogger.error("can not" + " add comment to document <" + vwDoc.getName() + ">", null);
					} else {
						VWLogger.info("Comment file path was invalid! Comment field will remain empty.");
					}

				}

				aipDoc.setStatus(aipDoc.IMPORTED);
				if (aipDoc.isDeleteImages()) {
					String bifFileName = null;
					if (bifFilePath != null) {
						bifFileName = bifFilePath.substring(bifFilePath.lastIndexOf(AIPUtil.pathSep)+1, bifFilePath.indexOf("."));
					} 
					VWLogger.info("bifFileName :" + bifFileName);
					String imageTempLocation = PendingPane.EHD.PManager.createImageTempLocation(null, bifFileName);
					PendingPane.EHD.PManager.moveImagesToTempLocation(imageArray, imageTempLocation, null);
					EyesHandsManager.deleteImageFiles(aipDoc, null);
				}
				EyesHandsManager.deleteCommentFile(aipDoc);
				VWLogger.info("AIP deleting document:  " + aipDoc.getId() + " : " + aipDoc.getFileId());
				AIPManager.getVWC().deleteAIPDocument(AIPManager.getCurrSid(), aipDoc);
				deletedDocs++;
				String docImpMsg = aipDoc.getName() + " is imported.";
				VWLogger.info(EHGeneralPanel.formatDocMsg(aipDoc, docImpMsg));
				// VWLogger.debug(" AIPDocument <"+aipDoc.getName()+"> is
				// imported");

			}
		} catch (Exception e) {
			// TODO: handle exception
			VWLogger.error("Error in Reprocess AIP file ", e);

		}
		VWLogger.info("Total imported and deleted documents : " + DocSaveNum + " : : : " + deletedDocs);
		return DocSaveNum;
	}
	public void ReLoadEHForms(){
		EHForms =new Vector();
		AIPManager.getVWC().getAIPForms(AIPManager.getCurrSid(),EHForms);      
	}
	public AIPForm getAIPForm(String FormName){
		if(FormName ==" ") return null;
		for(int i=0;i<EHForms.size();i++){
			AIPForm FInfo =(AIPForm)EHForms.get(i);
			if(FormName.equals(FInfo.getName())){
				return FInfo;
			}
		}
		return null;
	}

	public void setEditable(boolean enable){
		isTableEdit = enable;
	}
	public void setAllEnabled(boolean enable){
		table.setEnabled(enable);

	}


	public AIPFile getAIPFileInfoOfRow(int rownum){
		AIPFile aipFile =new AIPFile();
		String aipFilePath = TableModel.getValueAt(rownum,EHFILECOL).toString();
		if(!aipFilePath.equalsIgnoreCase("")){
			aipFile.setFilePath(aipFilePath);
			aipFile.setId(((Integer)TableModel.getValueAt(rownum,EHFILEIDCOL)).intValue());
			aipFile.setCreationDate(TableModel.getValueAt(rownum,FCREATECOL).toString().trim());
			aipFile.setProcessDate(TableModel.getValueAt(rownum,FPROCESSCOL).toString().trim());
			aipFile.setImported(0);
			aipFile.setPending(((Integer)TableModel.getValueAt(rownum,PENDINGCOL)).intValue());
		}
		return aipFile;
	}

	public Vector getTableRowOfInfo(AIPFile FInfo){
		Vector Trow  =new Vector();
		Trow.add(MARGINCOL,"");
		Trow.add(EHFILECOL,FInfo.getFilePath());
		Trow.add(FCREATECOL,FInfo.getCreationDate());
		Trow.add(FPROCESSCOL,FInfo.getProcessDate());
		//Trow.add(IMPORTCOL,new Integer(FInfo.getImported()));
		Trow.add(PENDINGCOL,new Integer(FInfo.getPending()));
		Trow.add(EHFILEIDCOL,new Integer(FInfo.getId()));
		Trow.add(STATE,new Integer(0));
		return Trow;
	}
	public void CheckEnableButton(){
		int[] Rows= table.getSelectedRows();
		if(Rows.length > 0 ){
			PendingPane.Delbtn.setEnabled(true);
			for(int i=0;i<Rows.length;i++){
				PendingPane.Procssbtn.setEnabled(false);
				if(true){//((Integer)TableModel.getValueAt(Rows[i],PENDINGCOL)).intValue() >0){
					PendingPane.Procssbtn.setEnabled(true);
					break;
				}
			}
		}else{
			PendingPane.Delbtn.setEnabled(false);
			PendingPane.Procssbtn.setEnabled(false);
		}

	}
	public void ShowPendingDocumentDialog(int Pointrow,int X,int Y){
		this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

		Frame f = JOptionPane.getFrameForComponent(table);
		int fileId =((Integer)TableModel.getValueAt(Pointrow,EHFILEIDCOL)).intValue();
		ReLoadEHForms();
		Vector pendingDocs =new Vector();
		AIPFile aipFile =new AIPFile();
		aipFile.setId(fileId);
		int ret =AIPManager.getVWC().getAIPDocuments(AIPManager.getCurrSid(),aipFile,pendingDocs);
		if(ret !=AIPManager.NOERROR)
		{    
			JOptionPane.showMessageDialog(this,AIPManager.getErrorDescription(ret)+"("+ret+")",AIPUtil.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);
			return ; 
		}    
		PendingDocumentDialog PDD =new PendingDocumentDialog(f,AIPUtil.getString("Pending.Lab.Title"),true,this,pendingDocs);
		PDD.setLocationRelativeTo(table);
		PDD.setLocation(PendingPane.getLocationOnScreen().x+155,PendingPane.getLocationOnScreen().y+30);
		PDD.setSize(525,500);
		PDD.setResizable(false);
		PDD.setVisible(true);
		this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		f.pack();

	}
	//--------------------table Inner class & Methode------------------------

	//Methode responce to table Selection change
	public void  tableSelectionChanged(ListSelectionEvent e){
		ListSelectionModel lsm = (ListSelectionModel)e.getSource();
		if (!lsm.isSelectionEmpty()){
			if(table.isEnabled()) {
				CheckEnableButton();
			}else {
				PendingPane.Procssbtn.setEnabled(false);
				PendingPane.Delbtn.setEnabled(false);
			}
		}else {
			PendingPane.Procssbtn.setEnabled(false);
			PendingPane.Delbtn.setEnabled(false);
		}
	}
	private class TableMouseListener extends MouseAdapter  {		
		public void mousePressed(MouseEvent e) {
			VWLogger.debug("TableMouseListener PendingPane.EHD.Getcursor :"+PendingPane.EHD.getCursor());
			/**CV10.2 - Added for restarting the Pending Process if it is hanging. Added by Vanitha S **/
			if (PendingPane.EHD.getCursor().toString().contains("Wait Cursor")) {
				//PendingPane.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
				return;
			}
			int Pointrow =table.rowAtPoint(e.getPoint());
			if (e.getClickCount() == 1){


				if(Pointrow > -1)
					table.setRowSelectionInterval(Pointrow,Pointrow);
				if (e.getModifiers() == e.BUTTON3_MASK) {

				}else if (e.getModifiers() == e.BUTTON1_MASK){

				}
			} else if(e.getClickCount() == 2 && table.isEnabled()){
				if(Pointrow > -1){
					if(true){//{((Integer)TableModel.getValueAt(Pointrow,PENDINGCOL)).intValue() > 0){
						ShowPendingDocumentDialog(Pointrow,e.getX(),e.getY());
					}
				}
			}

		}
		public void mouseReleased(MouseEvent e) {
		}
	}

	//--------------------  SpecialTableModel-----------------------
	public  class SpecialTableModel extends AbstractTableModel {
		private   boolean[] visibleColumns = new boolean[5];{ 
			visibleColumns[0] = true; 
			visibleColumns[1] = true; 
			visibleColumns[2] = true; 
			visibleColumns[3] = true; 
			visibleColumns[4] = true;       
		}

		String[] columnNames={"",AIPUtil.getString("History.TCol.FileName"),AIPUtil.getString("History.TCol.DateCreated"),AIPUtil.getString("History.TCol.DateProcessed"),AIPUtil.getString("History.TCol.Pending")};
		Vector ARow;
		int Srow ;
		int m_sortCol = 0;
		boolean m_sortAsc = true;

		public SpecialTableModel(){
		}
		public int getRowCount() {
			return Tabrows.size();
		}
		public String getColumnName(int col) {
			int j =getNumber(col);
			return columnNames[j]; 
		}
		public Object getValueAt(int aRow, int aColumn) {
			if(Tabrows.size() <= aRow) return ""; 
			Vector row = (Vector) Tabrows.elementAt(aRow);
			return row.elementAt(aColumn);
		}
		public Class getColumnClass(int c) {
			return getValueAt(0, c).getClass();
		}
		public boolean isCellEditable(int row, int col) {
			return false;
		}
		public void setValueAt(Object value, int row, int col) {
			Vector row1 = (Vector) Tabrows.elementAt(row);
			row1 = (Vector) Tabrows.elementAt(row);
			if(value ==null)return;
			if(value.equals(row1.elementAt(col))) return;
			row1.remove(col);
			row1.add(col,value);
		}

		public void RemoveRow(int Rnum){
			Tabrows.remove(Rnum);
			fireTableRowsDeleted(Rnum,Rnum);
		}
		public void RemoveAllRows(){
			int lastrow =Tabrows.size();
			if(lastrow >0){
				Tabrows.removeAllElements();
				this.fireTableRowsDeleted(0,lastrow-1);
			}
		}

		protected void setVisibleColumns(int col, boolean selection){  
			int count=0;
			for(int i=1;i<visibleColumns.length;i++){
				if(visibleColumns[i]==true){
					count++;
				} 			  
			}  	
			if(count==1 && selection==false){
			}else{ 			 
				visibleColumns[col] = selection; 
				fireTableStructureChanged();   
			}

		} 

		/** 
		 * This function converts a column number of the table into
		 * the right number of the data. 
		 */ 
		 protected int getNumber(int column) { 
			int n = column;    // right number

			int i = 0; 
			do { 
				if (!(visibleColumns[i])) n++; 
				i++; 
			} while (i < n); 
			// When we are on an invisible column, 
			// we must go on one step
			while (!(visibleColumns[n])) n++; 

			return n; 
		 } 
		 // *** METHODS OF THE TABLE MODEL *** 
		 public int getColumnCount() {    	  
			 int n = 0; 
			 for (int i = 0; i < visibleColumns.length; i++) 
				 if (visibleColumns[i]) n++;    		
			 return n; 
		 } 
		 class ColumnListener extends MouseAdapter
		 {
			 protected JTable m_table;
			 protected ProcessPendingFileLayout pfl;

			 public ColumnListener(ProcessPendingFileLayout PFL){
				 pfl = PFL;
				 m_table = PFL.table;
			 }

			 public void mouseClicked(MouseEvent e){
				 if(e.getModifiers()==e.BUTTON1_MASK){
					 // JOptionPane.showMessageDialog(null, "Header Clicked" ,"" ,JOptionPane.OK_OPTION);
					 //Issue 545 
					 //This Code sets the cursor to Hour glass when sort process is done
					 m_table.getTableHeader().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));	        	
					 sortCol(e);	         	
					 m_table.getTableHeader().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));	         	
					 // End of fix 545
				 }
				 else if(e.getModifiers()==e.BUTTON3_MASK){
					 javax.swing.JPopupMenu menu=new javax.swing.JPopupMenu("View");
					 for (int i=1; i < columnNames.length; i++){
						 final javax.swing.JCheckBoxMenuItem subMenu=new javax.swing.JCheckBoxMenuItem(
								 columnNames[i],visibleColumns[i]);
						 final int j =i;
						 subMenu.addActionListener(new ActionListener() { 
							 public void actionPerformed(ActionEvent evt) {
								 TableModel.setVisibleColumns(j, subMenu.getState()); 
								 for(int i=0;i<table.getColumnCount();i++){
									 TableColumn column = table.getColumnModel().getColumn(i);
									 if (i==MARGINCOL){
										 column.setPreferredWidth(22);
										 column.setMaxWidth(22);
										 column.setCellRenderer(new ButtonRenderer());
										 column.setCellEditor(new ButtonEditor());
									 }
								 }
							 } 
						 }); 
						 menu.add(subMenu);
					 }
					 menu.show(m_table,e.getX(),e.getY());             
				 }				
			 }

			 private void sortCol(MouseEvent e)
			 {
				 TableColumnModel colModel = m_table.getColumnModel();
				 int columnModelIndex = colModel.getColumnIndexAtX(e.getX());
				 int modelIndex = colModel.getColumn(columnModelIndex).getModelIndex();
				 if (modelIndex < 0)
					 return;
				 if ( m_sortCol==modelIndex)
					 m_sortAsc = !m_sortAsc;
				 else
					 m_sortCol = modelIndex;

				 for (int i=0; i < colModel.getColumnCount();i++) {
					 TableColumn column = colModel.getColumn(i);
					 column.setHeaderValue(m_table.getColumnName(column.getModelIndex()));    
				 }
				 m_table.getTableHeader().repaint();  

				 //JOptionPane.showMessageDialog(null, ""+Tabrows.size() ,"" ,JOptionPane.OK_OPTION);
				 m_table.getTableHeader().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));  
				 Collections.sort(Tabrows, new AIPFileComparator(modelIndex, m_sortAsc));
				 pfl.TableModel.fireTableChanged(new TableModelEvent(SpecialTableModel.this));
				 //pfl.TableModel.fireTableDataChanged();
				 //m_table.tableChanged(new TableModelEvent(SpecialTableModel.this)); 
				 //m_table.repaint();  

			 }
		 }
		 class AIPFileComparator implements Comparator
		 {
			 protected int     m_sortCol;
			 protected boolean m_sortAsc;

			 public AIPFileComparator(int sortCol, boolean sortAsc) {
				 m_sortCol = sortCol;
				 m_sortAsc = sortAsc;
			 }

			 public int compare(Object o1, Object o2) {
				 String sAip1 = o1.toString();
				 String sAip2 = o2.toString();
				 sAip1 = sAip1.substring(3,sAip1.length());
				 sAip2 = sAip2.substring(3,sAip2.length());
				 o1 = new AIPFile(sAip1,"",0);
				 o2 = new AIPFile(sAip2,"",0);
				 //JOptionPane.showMessageDialog(null, ""+sAip1+"\n"+sAip2,"" ,JOptionPane.OK_OPTION);
				 if(!(o1 instanceof AIPFile) || !(o2 instanceof AIPFile))
					 return 0;
				 AIPFile s1 = (AIPFile)o1;
				 AIPFile s2 = (AIPFile)o2;
				 //JOptionPane.showMessageDialog(null, ""+s1.getFilePath() ,"" ,JOptionPane.OK_OPTION);
				 int result = 0;
				 double d1, d2;
				 switch (m_sortCol) {
				 case 1:
					 result = s1.getFilePath().compareTo(s2.getFilePath());
					 break;
				 case 2:
					 try{
						 //yyyy/mm/dd
						 Date date_1 = AIPUtil.getDate(s1.getCreationDate());
						 Date date_2 = AIPUtil.getDate(s2.getCreationDate());
						 result = date_1.compareTo(date_2);
					 }catch(Exception e){

					 }
					 break;
				 case 3:
					 try{
						 //yyyymmdd
						 Date date_1 = AIPUtil.getDate(s1.getProcessDate());
						 Date date_2 = AIPUtil.getDate(s2.getProcessDate());
						 result = date_1.compareTo(date_2);
					 }catch(Exception e){

					 }
					 break;
				 case 4:
					 try{
						 Integer intPending1 = Integer.valueOf(s1.getPending());
						 Integer intPending2 = Integer.valueOf(s2.getPending());
						 result = intPending1.compareTo(intPending2);
					 }catch(Exception e){

					 }
					 break;
				 case 5:
					 result = String.valueOf(s1.getImported()).compareTo(String.valueOf(s2.getImported()));
					 break;
				 }
				 if (!m_sortAsc)
					 result = -result;
				 return result;
			 }

			 public boolean equals(Object obj) {
				 if (obj instanceof AIPFileComparator) {
					 AIPFileComparator compObj = (AIPFileComparator)obj;
					 return (compObj.m_sortCol==m_sortCol) && 
					 (compObj.m_sortAsc==m_sortAsc);
				 }
				 return false;
			 }
		 }

		 // Code added for EAS Enahance History Tab - end
		 private class SortableHeaderRenderer implements TableCellRenderer {
			 private TableCellRenderer tableCellRenderer;

			 public SortableHeaderRenderer(TableCellRenderer tableCellRenderer) {
				 this.tableCellRenderer = tableCellRenderer;
			 }

			 public Component getTableCellRendererComponent(JTable table, 
					 Object value,
					 boolean isSelected, 
					 boolean hasFocus,
					 int row, 
					 int column) {
				 Component c = tableCellRenderer.getTableCellRendererComponent(table, 
						 value, isSelected, hasFocus, row, column);
				 if (c instanceof JLabel) {
					 JLabel l = (JLabel) c;
					 l.setHorizontalTextPosition(JLabel.LEFT);
					 int modelColumn = table.convertColumnIndexToModel(column);
					 l.setIcon(getHeaderRendererIcon(modelColumn, l.getFont().getSize()));
				 }
				 return c;
			 }
		 }
		 protected Icon getHeaderRendererIcon(int column, int size) {
			 if(m_sortCol == 0 || column != m_sortCol)
				 return null;
			 return new Arrow(!m_sortAsc, size,column);
		 }



	}

	public boolean IsUnSaved(){

		return false;
	}
	public boolean checkUnSaved(){

		return true;
	}
	private static class Arrow implements Icon {
		private boolean descending;
		private int size;
		private int priority;

		public Arrow(boolean descending, int size, int priority) {
			this.descending = descending;
			this.size = 10;
			this.priority = 1;
		}

		public void paintIcon(Component c, Graphics g, int x, int y) {
			Color color = c == null ? Color.GRAY : c.getBackground();             
			// In a compound sort, make each succesive triangle 20% 
			// smaller than the previous one. 
			int dx = (int)(size/2*Math.pow(0.8, priority));
			int dy = descending ? dx : -dx;
			// Align icon (roughly) with font baseline. 
			y = y + 5*size/6 + (descending ? -dy : 0);
			int shift = descending ? 1 : -1;
			g.translate(x, y);

			// Right diagonal. 
			g.setColor(color.darker());
			g.drawLine(dx / 2, dy, 0, 0);
			g.drawLine(dx / 2, dy + shift, 0, shift);

			// Left diagonal. 
			g.setColor(color.brighter());
			g.drawLine(dx / 2, dy, dx, 0);
			g.drawLine(dx / 2, dy + shift, dx, shift);

			// Horizontal line. 
			if (descending) {
				g.setColor(color.darker().darker());
			} else {
				g.setColor(color.brighter().brighter());
			}
			g.drawLine(dx, 0, 0, 0);

			g.setColor(color);
			g.translate(-x, -y);
		}

		public int getIconWidth() {
			return 10;
		}

		public int getIconHeight() {
			return 10;
		}
	}

	private class ButtonRenderer extends JButton
	implements TableCellRenderer {
		public ButtonRenderer() {
			super();

		}
		public Component getTableCellRendererComponent(
				JTable table, Object color,
				boolean isSelected, boolean hasFocus,
				int row, int column) {
			return this;
		}
	}
	private class ButtonEditor extends DefaultCellEditor {
		public ButtonEditor() {
			super(new JCheckBox());
			JButton b =new JButton();
			editorComponent = b;
			setClickCountToStart(1);
			b.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					fireEditingStopped();
				}
			});
		}
		protected void fireEditingStopped() {
			super.fireEditingStopped();
		}

		public Object getCellEditorValue() {
			return null;
		}
		public Component getTableCellEditorComponent(JTable table,
				Object value,
				boolean isSelected,
				int row,
				int column) {
			return editorComponent;
		}
	}
	/*
	 * Issue / Enhancement 	: Enhancements of AIP  
	 * 						  Requirment sent by Rajesh on 09-June-2006 through eMail  	 
	 * Created By			: M.Premananth
	 * Date					: 14-June-2006
	 */
	 class ButtonListener implements ActionListener {  	
		public void actionPerformed(ActionEvent e) {
			/**CV10.2 - Added for restarting the Pending Process if it is hanging. Added by Vanitha S **/
			VWLogger.debug("Button Listener PendingPane.EHD.Getcursor :"+PendingPane.EHD.getCursor());
			if (PendingPane.EHD.getCursor().toString().contains("Wait Cursor")) {
				//PendingPane.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
				return;
			}
			Nextbtn.setEnabled(true);            	
			Lastbtn.setEnabled(true); 
			Firstbtn.setEnabled(true);
			Previousbtn.setEnabled(true);
			JButton Source = (JButton)e.getSource();			
			if(Source.equals(Firstbtn)){
				PendingPane.RemoveAllPFLRows();
				int iStart 	= 0;
				int iEnd 	= EHPendingPanel.iMaximumRows;	
				Vector hFiles =new Vector();
				PendingPane.EHD.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
				AIPManager.getVWC().getAIPHistoryFiles(AIPManager.getCurrSid(),hFiles,iStart,iEnd,0);
				PendingPane.PFL.LoadTableData(hFiles); 
				PendingPane.EHD.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				Firstbtn.setEnabled(false);
				Previousbtn.setEnabled(false);  
				EHPendingPanel.iStartIndex 	= iStart;
				EHPendingPanel.iEndIndex	= iEnd;            
			}
			else if(Source.equals(Previousbtn)){
				PendingPane.RemoveAllPFLRows();
				int iEnd 	= EHPendingPanel.iStartIndex;
				int iStart 	= iEnd - EHPendingPanel.iMaximumRows;
				if(iStart < 0)
				{
					iStart 	= 0;
					iEnd 	= EHPendingPanel.iMaximumRows;
				}
				Vector hFiles =new Vector();
				PendingPane.EHD.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
				AIPManager.getVWC().getAIPHistoryFiles(AIPManager.getCurrSid(),hFiles,iStart,iEnd,0);
				if(iStart== 0){
					Firstbtn.setEnabled(false);
					Previousbtn.setEnabled(false);
				}             
				PendingPane.PFL.LoadTableData(hFiles);
				PendingPane.EHD.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				EHPendingPanel.iStartIndex 	= iStart;
				EHPendingPanel.iEndIndex		= iEnd;


			}
			else if(Source.equals(Nextbtn)){
				PendingPane.RemoveAllPFLRows();
				int iStart 	= EHPendingPanel.iEndIndex;
				int iEnd 	= iStart + EHPendingPanel.iMaximumRows;
				// This code is added to implement dynamic update 
				Vector hPendingCount = new Vector();
				AIPManager.getVWC().getAIPHistoryCount(AIPManager.getCurrSid(),hPendingCount,0);
				EHPendingPanel.iTotalCount = Integer.parseInt(hPendingCount.get(0).toString());
				hPendingCount.clear();
				hPendingCount = null;
				// End of fix
				if(iEnd > EHPendingPanel.iTotalCount)
				{
					iStart 	= EHPendingPanel.iTotalCount - EHPendingPanel.iMaximumRows;
					iEnd	= EHPendingPanel.iTotalCount;
				}
				Vector hFiles =new Vector();
				PendingPane.EHD.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
				AIPManager.getVWC().getAIPHistoryFiles(AIPManager.getCurrSid(),hFiles,iStart,iEnd+1,0);
				int count =iEnd-iStart;
				if(hFiles.size()==count){
					Nextbtn.setEnabled(false);
					Lastbtn.setEnabled(false);
				}
				else if(hFiles.size()>count){
					hFiles.remove(count);
				}
				else if(hFiles.size()<count){
					Nextbtn.setEnabled(false);
					Lastbtn.setEnabled(false);            	
				}           
				PendingPane.PFL.LoadTableData(hFiles);
				PendingPane.EHD.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				EHPendingPanel.iStartIndex 	= iStart;
				EHPendingPanel.iEndIndex		= iEnd;       	  	
			}
			else if(Source.equals(Lastbtn)){           	
				PendingPane.RemoveAllPFLRows();
				// This code is added to implement dynamic update 
				Vector hPendingCount = new Vector();
				AIPManager.getVWC().getAIPHistoryCount(AIPManager.getCurrSid(),hPendingCount,0);
				EHPendingPanel.iTotalCount = Integer.parseInt(hPendingCount.get(0).toString());
				hPendingCount.clear();
				hPendingCount = null;
				// End of fix
				int iStart 	= EHPendingPanel.iTotalCount - EHPendingPanel.iMaximumRows;
				int iEnd 	= EHPendingPanel.iTotalCount;
				Vector hFiles =new Vector();
				PendingPane.EHD.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
				AIPManager.getVWC().getAIPHistoryFiles(AIPManager.getCurrSid(),hFiles,iStart,iEnd,0);
				PendingPane.PFL.LoadTableData(hFiles);
				PendingPane.EHD.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				Lastbtn.setEnabled(false);
				Nextbtn.setEnabled(false);
				EHPendingPanel.iStartIndex 	= iStart;
				EHPendingPanel.iEndIndex	= iEnd;            
			}

		}
	 }

	 class TextFieldListener implements FocusListener{
		 public void focusGained(FocusEvent e){

		 }
		 public void focusLost(FocusEvent e){
			 JTextField source = (JTextField)e.getSource();
			 String sMaxRow = source.getText(); 
			 if(sMaxRow.trim().length() > 0)
			 {
				 int maxRow =Integer.parseInt(sMaxRow);
				 if (maxRow==0){
					 JOptionPane.showMessageDialog(null,connectorManager.getString("ProcessPendingFileLayout.Rows"),connectorManager.getString("ProcessPendingFileLayout.AIP"),JOptionPane.OK_OPTION);
					 source.setText("15");	  			   
				 }
				 EHPendingPanel.iMaximumRows = Integer.parseInt(source.getText());
				 AIPManager.setMaximumRow(EHPendingPanel.iMaximumRows);
			 }
			 //JOptionPane.showMessageDialog(null, "Text value Chenge to"+source.getText() ,"" ,JOptionPane.OK_OPTION);
		 }
	 }
	 /**CV10.2 - Added for restarting the Pending Process if it is hanging. Added by Vanitha S **/
	 public void createPendingProcessMonitoringThread() {
		 pendProcessMonitoringThread = new PendProcessMonitoringThread();
		 pendProcessMonitoringThread.start();
	 }
	 public void stopProcessMonitoringThread() {
		 if (pendProcessMonitoringThread != null) {
			 try {
				 Thread t = pendProcessMonitoringThread;
				 pendProcessMonitoringThread = null;
				 t.interrupt();
				 t = null;
			 } catch (Exception e) {
				 VWLogger.info("Exception while stoping process monitoring thread....."+e.getMessage()+" for thread :"+pendProcessMonitoringThread);
			 }
		 }
	 }
	 private class PendProcessMonitoringThread extends Thread
		{    		
			public void run()
			{
				VWLogger.info("*** PendProcessMonitoringThread initiated ****");
				initiatePendProcessMonitoringScheduler();
			}
		}    
		
		public synchronized void initiatePendProcessMonitoringScheduler(){
			pendProcessMonitoringTimer = new java.util.Timer();		
			int frequency = 1;
			try{
				VWLogger.info("*** initiatePendProcessMonitoringScheduler  ***" + frequency);
				Calendar calendar = Calendar.getInstance();

				Date startTime = calendar.getTime();
				VWLogger.info("***initiatePendProcessMonitoringScheduler  startTime ***" + startTime.toString());
				VWLogger.info("***initiatePendProcessMonitoringScheduler  period ***" + 1000 * frequency * 60);
				pendProcessMonitoringTimer.schedule(new schedulePendProcessMonitoringTask(), startTime, 1000 * frequency * 60);
			}catch (Exception e) {
				VWLogger.error("Exception while scheduling process monitoring task ", e);
			}

		}
		
		private void setPendProcessDocDetails(String Name){
			if (pendingProcessDocDetails == null) {
				pendingProcessDocDetails = new DocumentProcessStatus();
			}
			if (pendingProcessMonitoringDetails == null) {
				pendingProcessMonitoringDetails = new DocumentProcessStatus();
			}
			
			//if (processingThread != null && processMonitoringThread != null) {
				Calendar date = Calendar.getInstance();
				pendingProcessDocDetails.setDocumentName(Name);
				pendingProcessMonitoringDetails.setDocumentName(Name);
				String dateAndTime = date.getTime().toString();
				VWLogger.info("dateAndTime...................."+dateAndTime);
				pendingProcessDocDetails.setProcessStartTime(dateAndTime);
				pendingProcessMonitoringDetails.setProcessStartTime(dateAndTime);
				pendingProcessDocDetails.setProcessStatus(0);
				pendingProcessMonitoringDetails.setProcessStatus(0);
				date = null;
			//}
		}
		
		class schedulePendProcessMonitoringTask extends TimerTask{
				
			public void run() {
				try{
					VWLogger.info("*** run in schedulePendProcessMonitoringTask ");
					startPendTaskScheduler();
				}catch (Exception e) {
					VWLogger.error("Exception in schedulePendProcessMonitoringTask run() method ", e);
				}
			}
			
			public void startPendTaskScheduler(){
				VWLogger.info("startPendTaskScheduler.................");
				VWLogger.info("pendingProcessDocDetails................."+pendingProcessDocDetails);
				VWLogger.info("pendingProcessMonitoringDetails................."+pendingProcessMonitoringDetails);
				
				try{				
					if (pendingProcessDocDetails != null && pendingProcessMonitoringDetails != null
							&& pendingProcessMonitoringDetails.getDocumentName() != null
							&& pendingProcessDocDetails.getDocumentName() != null) {
						Calendar date = Calendar.getInstance();
						String dateAndTime = date.getTime().toString();
						pendingProcessMonitoringDetails.setProcessStartTime(dateAndTime);
						VWLogger.info("Current date and time :"+dateAndTime);
						VWLogger.info("Last Processed Document :"+pendingProcessDocDetails.getDocumentName());
						VWLogger.info("Last Processed Document in monitoring thread :"+pendingProcessMonitoringDetails.getDocumentName());
						VWLogger.info("Last Processed Document status :"+pendingProcessDocDetails.getProcessStatus());
						VWLogger.info("Last Processed Document status in monitoring thread :"+pendingProcessMonitoringDetails.getProcessStatus());
						if (pendingProcessDocDetails.getDocumentName().equals(pendingProcessMonitoringDetails.getDocumentName())
								&& pendingProcessDocDetails.getProcessStatus() == pendingProcessMonitoringDetails.getProcessStatus() 
								&& pendingProcessDocDetails.getProcessStatus() == 0) {
							VWLogger.info("*** startScheduler to monitor hanging document Started ***");
							try {
								SimpleDateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss z yyyy");
								String processTime =  pendingProcessDocDetails.getProcessStartTime();
								String monitorTime = pendingProcessMonitoringDetails.getProcessStartTime();
								VWLogger.info("processTime :"+processTime);
								VWLogger.info("monitorTime :"+monitorTime);
								Date pDate=formatter.parse(processTime);
								Date mDate=formatter.parse(monitorTime);  
								long diff = mDate.getTime() - pDate.getTime();
								VWLogger.info("Time difference :"+diff);
								long diffMinutes = diff / (60 * 1000);
								VWLogger.info("Time difference in minutes :"+diffMinutes);
								if (diffMinutes >= 5) {
									//PendingPane.makePendingProcessToWaitState();
									VWLogger.info("Before restarting the pending process...");
									pendProcessMonitoringTimer.cancel();
									pendProcessMonitoringTimer = null;
									pendingProcessDocDetails.setDocumentName("99999");
									pendingProcessDocDetails.setProcessStatus(1);
									pendingProcessDocDetails.setProcessStartTime(dateAndTime);									
									VWLogger.info("CurrentThreadName 3 :"+Thread.currentThread().getName()+" isInterrupted :"+Thread.currentThread().isInterrupted());
									PendingPane.restartPendingProcess();
								}
								
							} catch (Exception e) {
								VWLogger.error("Exception while datecomparison check in startTaskScheduler method ", e);
							}
							date = null;
						}
					}
				}catch (Exception e) {
					if (pendProcessMonitoringTimer != null) {
						VWLogger.error("Exception in startTaskScheduler method : ", e);
					} else {
						VWLogger.info("Pending process monitoring timer has stopped.....");					}
				}
			}		
		}
		
		public void stopPendingProcess() {
			VWLogger.info("Pending process monitoring timer stopped ...");
			if(pendProcessMonitoringTimer != null) {
				pendProcessMonitoringTimer.cancel();
				pendProcessMonitoringTimer = null;
				pendingProcessDocDetails = null;
				pendingProcessMonitoringDetails = null;				
			}
		}
		/**End of CV10.2 code merges**/
}