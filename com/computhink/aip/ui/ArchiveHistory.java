/*
 * ArchiveHistory.java
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.computhink.aip.ui;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import com.computhink.aip.AIPManager;
import com.computhink.aip.client.AIPFile;
import com.computhink.aip.util.AIPUtil;
import com.computhink.aip.util.VWLogger;
import com.computhink.aip.util.XMLCastorSerializer;
import com.computhink.aip.util.XMLSerializationException;


/**
 * Archive histories and save as a XML file 
 * Follows Singleton Design Pattern.
 *
 * All exceptions are handled.
 * 
 */
public class ArchiveHistory extends Thread
{
    EHGeneralPanel EHGPanel;
    private boolean ArchiveStop = true;
    public ArchiveHistory(EHGeneralPanel EHG)
    {
        super("Archive "+AIPManager.getCurrSid());
        EHGPanel = EHG;
    }
    public boolean getStatus() {
        return this.ArchiveStop;
    }    
    public void setStatus(boolean status) {
        this.ArchiveStop = status;
    }    
    
    public void run()
    {
    	int iArchiveCount = AIPManager.getArchiveCount();
        while(!ArchiveStop)
        {
            Vector aFiles = new Vector();
            AIPManager.getVWC().getAIPHistoryFilesForArchive(AIPManager.getCurrSid(),aFiles,AIPManager.getAgeForArchive(),iArchiveCount);
            XMLCastorSerializer XMLSerializer = XMLCastorSerializer.getInstance();
            int fromIndex=0, toIndex = 1; 
            try{
                while(aFiles.size() >= (toIndex * iArchiveCount))
                {
                    XMLSerializer.saveToXML(new ArrayList(aFiles.subList(fromIndex,(toIndex * iArchiveCount))), new File(AIPManager.getArchivalFolder()+"\\AIPHistory_"+System.currentTimeMillis()+".xml"));
                    try{
                        sleep(200);
                    }catch(Exception ex){}
                    //fromIndex = (toIndex * iArchiveCount)-1;
                    fromIndex = (toIndex * iArchiveCount);
                    toIndex++;
                }
    			//VWLogger.info("after while toIndex "+toIndex);
                /*Issue No 714_01:The xml files created from the history tab have duplicate information.
                * Created By		: C.Shanmugavalli       * Date			: 18-Sep-2006
                */
                if(aFiles.size()>iArchiveCount && aFiles.size() > 0){
                	fromIndex = (toIndex-1)*iArchiveCount;
                	//VWLogger.info("yes coming in fromIndex "+fromIndex + "to Index :"+aFiles.size()+":"+AIPManager.getArchivalFolder()+"\\AIPHistory_"+System.currentTimeMillis()+".xml");
                	XMLSerializer.saveToXML(new ArrayList(aFiles.subList(fromIndex,aFiles.size())), new File(AIPManager.getArchivalFolder()+"\\AIPHistory_"+System.currentTimeMillis()+".xml"));
                }
            }catch(FileNotFoundException ex)
            {
    			VWLogger.error("Exception while updation", ex);
            }
            catch(XMLSerializationException ex)
            {
    			VWLogger.error("Exception while updation", ex);
            }
            
            try{
                sleep(1*60*1000);
            }catch(Exception ex){}
            
        }
    }
}
