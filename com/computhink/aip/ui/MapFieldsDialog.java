package com.computhink.aip.ui;

import com.computhink.aip.*;
import com.computhink.aip.util.AIPUtil;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.util.*;

import com.computhink.aip.client.AIPForm;
/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public class MapFieldsDialog extends JDialog {
   MapFieldsLayout MapFld ;
   MapDTypeLayout MDT;
   public  JButton Savebtn ;
   private  JButton Hlpbtn ;
   private  JButton Cancelbtn;

  public MapFieldsDialog(Frame frame, String title, boolean modal,MapDTypeLayout MDT1,AIPForm FormInfo) {
    super(frame, title, modal);
     //setBorder(new EmptyBorder(5,5,5,5));
     MDT =MDT1;
     MapFld = new MapFieldsLayout(this,FormInfo);
     JPanel Panel1 =new JPanel();
     getContentPane().add(Panel1,BorderLayout.CENTER);
     Panel1.setMinimumSize(new Dimension(200,100));
     Panel1.setPreferredSize(new Dimension(500,300));

     Panel1.setLayout(new BoxLayout(Panel1,BoxLayout.Y_AXIS));
     Panel1.setBorder(new CompoundBorder(new TitledBorder(null,null,
	TitledBorder.LEFT, TitledBorder.TOP),new EmptyBorder(5,8,5,5)));

     Panel1.add(MapFld);
     Panel1.add(Box.createVerticalStrut(15));
     Panel1.add(CreateButtonPanel());

  }

 /* public MapFieldsDialog() {
    this(null, "", false);
  }*/
  public void exitDialog(){
       setVisible(false);
  }
  public void setAllEnable(boolean enable){
      Savebtn.setEnabled(enable);
      //Cancelbtn.setEnabled(false);
      Hlpbtn.setEnabled(enable);
      MapFld.setAllEnable(enable);
  }
  public JPanel CreateButtonPanel(){
    JPanel BtnPanel =new JPanel();
    BtnPanel.setLayout(new BoxLayout(BtnPanel, BoxLayout.X_AXIS));
   // BtnPanel.setBorder(new CompoundBorder(new TitledBorder(null,null,
	//	 TitledBorder.LEFT, TitledBorder.TOP),new EmptyBorder(5,5,5,5) ));
    ButtonListener btnListener =new ButtonListener();
    Savebtn    =new JButton(AIPUtil.getString("FieldsMapping.Btn.Save"));
    Hlpbtn      =new JButton(AIPUtil.getString("FieldsMapping.Btn.Help"));
    Cancelbtn     =new JButton(AIPUtil.getString("FieldsMapping.Btn.Cancel"));

    Savebtn.setMnemonic(AIPUtil.getString("FieldsMapping.MBtn.Save").trim().charAt(0));
    Cancelbtn.setMnemonic(AIPUtil.getString("FieldsMapping.MBtn.Cancel").trim().charAt(0));
    Hlpbtn.setMnemonic(AIPUtil.getString("FieldsMapping.MBtn.Help").trim().charAt(0));

    Savebtn.addActionListener(btnListener);
    Cancelbtn.addActionListener(btnListener);
    Hlpbtn.addActionListener(btnListener);


    Savebtn.setPreferredSize(new Dimension(80,25));
    Savebtn.setMinimumSize(new Dimension(80,25));
    Cancelbtn.setPreferredSize(new Dimension(80,25));
    Cancelbtn.setMinimumSize(new Dimension(80,25));
    Hlpbtn.setPreferredSize(new Dimension(80,25));
    Hlpbtn.setMinimumSize(new Dimension(80,25));

    Savebtn.setEnabled(false);
    Cancelbtn.setEnabled(true);
    Hlpbtn.setEnabled(true);

    BtnPanel.add(Box.createHorizontalGlue());
    BtnPanel.add(Savebtn);
    BtnPanel.add(Cancelbtn);
    BtnPanel.add(Hlpbtn);

    BtnPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
    return BtnPanel;

  }
  //-------------------------------------- Listener actions-------------------------------
   class ButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
           JButton Source = (JButton)e.getSource();
           if( Source.equals(Savebtn)){ //.getText().equalsIgnoreCase("Save")){
                  if(MapFld.DTtable.isEditing()){
                    if(MapFld.DTtable.getEditorComponent() instanceof JTextField){
                        String Txt=((JTextField)MapFld.DTtable.getEditorComponent()).getText();
                        MapFld.DTtable.setValueAt(Txt,MapFld.DTtable.getEditingRow(),MapFld.DTtable.getEditingColumn());
                    }
                  }
                  if(!MapFld.CheckMapRequiredIndex()){
                      JOptionPane.showMessageDialog(MapFld,AIPUtil.getString("FieldsMapping.Msg.NoMapRequiredField"),AIPUtil.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);
                      return;
                  }
                  if(MapFld.isRequiredDefaultFieldsEmpty()){
                       JOptionPane.showMessageDialog(MapFld,AIPUtil.getString("FieldsMapping.Msg.NoDefaultRequiredField"),AIPUtil.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);
                        return ;
                  }

                  MapFld.ClearOldEHFiledsMap();
                  MapFld.SaveNewMap();
                  MDT.TableModel.setValueAt(new Integer(AIPForm.FIELDSUPDATE),MDT.SelectedRow,MDT.STATE);
                  MDT.TableModel.fireTableCellUpdated(MDT.SelectedRow,0);
                  MDT.isTableUnsave =true;
                  MDT.tempPane.Savebtn.setEnabled(true);
                  Source.setEnabled(false);
                  exitDialog();


           }else if( Source.equals(Cancelbtn)){
                exitDialog();
           }
      }
  }
}