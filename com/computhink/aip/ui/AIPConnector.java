package com.computhink.aip.ui;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.net.URL;
import java.util.prefs.Preferences;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.plaf.FontUIResource;

import com.computhink.aip.resource.ResourceManager;
import com.computhink.aip.resource.images.ImageManager;
import com.jgoodies.looks.plastic.Plastic3DLookAndFeel;
import com.computhink.aip.util.AIPUtil;
import com.computhink.aip.util.FoldersMonitoring;
import com.computhink.aip.util.VWLogger;
import com.computhink.aip.AIPManager;
import com.computhink.aip.AIPPreferences;
import com.computhink.aip.EyesHandsManager;
import com.computhink.common.Constants;
import com.computhink.vwc.VWCPreferences;
import com.computhink.vwc.VWClient;


//added by amaleh on 2003-07-23

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

/**
 * This class is the first class to be used in the program when run since it
 * contains the main method. 
 * It first displays a splash screen, then checks authentication and
 * authorization, and finally launches the program's main panel.
 */
public class AIPConnector 
{
  /** Label to contain the splash screen image */
  private JLabel BeginLabel =  null;
  /** Window to represent the splash screen */
  private JWindow BeginWindow = null;
  /** Frame to contain the program's contents */
  private static JFrame f = null;
  /** Main panel in the program's frame */
  private EHMainPanel ehd =null;
  public static String fontName="Arial";
  public static int fontSize=0;
  // added by amaleh on 2003-11-05
  /** String to store User Name */
  private String username = System.getProperty("user.name");
  /** ImageManager to be used for loading images */
  ImageManager IM =new ImageManager();
  private static AIPConnector AIPConnObject = null;
  
  /** Main constructor */
  public AIPConnector() {
	/* added on 2003-12-12
     * Bypass manual shut down if initiated.
     */
      bypassManualShutDown();
    /**/
    // fill in the contents of the splash screen
    CreateBeginLog();
    SwingUtilities.invokeLater(new Runnable() {
	    public void run() {
		ShowBeginLog();
	    }
    });
    //AIPManager.initLogger();
    // initialize rooms; initialize and fill in the main frame's contents.
    Initialize();
    //added by amaleh on 2003-07-23
    // log the program's status in the audit log
    VWLogger.info("* * * * * VW Input Processor Started * * * * *");
    // log the program's version in the audit log
    VWLogger.info("Version: " + AIPUtil.getString("General.Lab.Version"));
   
    SwingUtilities.invokeLater(new Runnable() {
           public void run() {
             try{
                // hide splash screen
                hideBeginLog();
                // give the program the desired size
                f.setSize(685,520);
                // calculate the screen dimensions
                Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
                /* shift the program's main window so it is in the middle of
                the screen */
                f.setLocation((screenSize.width/2 - f.getSize().width/2)-50,
				    (screenSize.height/2 - f.getSize().height/20)-314);
                /* make the main window visible */
                f.setVisible(true);                
                f.pack();
               
             }
             catch (Exception e){
               VWLogger.error("Error: Initialize E&H Connector ",e);
             }

           }
    });
  }
  /** fills in the contents of the splash screen */
  public void CreateBeginLog(){
	  BeginLabel = new JLabel(getSplashImage());
	  BeginWindow = new JWindow();
	  BeginWindow.getContentPane().add(BeginLabel);
	  BeginWindow.pack();
	  Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	  BeginWindow.setLocation(screenSize.width/2 - BeginWindow.getSize().width/2,
			  screenSize.height/2 - BeginWindow.getSize().height/2);

  }
  
  public ImageIcon getSplashImage(){
	  ImageIcon splashImage = null;
	  try{
		  Preferences imagePrefs = Preferences.systemRoot().node("/computhink/" + Constants.PRODUCT_NAME.toLowerCase() +"/imagepath");
		  String imagePath = imagePrefs.get("aipsplash", "");

		  if(imagePath != null && imagePath.trim().length() > 0){
			  try{
				  if(imagePath.startsWith("http:")){
					  if(imagePath.contains("http:\\")){
						  imagePath = imagePath.replace("http:\\", "http://");
					  }
					  URL url = new URL(imagePath);
					  Image image = ImageIO.read(url);
					  splashImage = new ImageIcon(image);
				  }else{
					  File file = new File(imagePath);
					  Image image = ImageIO.read(file);
					  splashImage = new ImageIcon(image);
				  }
			  }catch (Exception e) {
				  splashImage = new ImageIcon(IM.getClass().getResource("FCSplash.png"));
			  }
		  }else{
			  splashImage = new ImageIcon(IM.getClass().getResource("FCSplash.png"));
		  }
	  }catch (Exception e) {
		  splashImage = new ImageIcon(IM.getClass().getResource("FCSplash.png"));
	  }
	  return splashImage;
  }
  public void ShowBeginLog(){
	  /*
  	 * new JWindow().show(); method is replaced with new JWindow().setVisible(true); 
  	 * as show() method is deprecated  //Gurumurthy.T.S 19/12/2013
  	 */
    BeginWindow.setVisible(true);
  }
  public void hideBeginLog(){
        BeginLabel.setVisible(false);
        /*
      	 * new JWindow().hide(); method is replaced with new JWindow().setVisible(false); 
      	 * as hide() method is deprecated  //Gurumurthy.T.S 19/12/2013,CV8B5-001
      	 */
        BeginWindow.setVisible(false);
	BeginWindow = null;
	BeginLabel = null;
  }
   
  public void Initialize(){
     
      f =new JFrame();
      // create the main panel that will contain all other panels.
      ehd =new EHMainPanel(f);
      // set window icon image in the top left corner.
      f.setIconImage(f.getToolkit().getImage(IM.getClass().getResource("VWicon.gif")));
      // set window title.
      f.setTitle(AIPUtil.getString("Init.Msg.Title"));
      // add main panel to the frame.
      f.getContentPane().add(ehd);
      // make the frame (window) irresizable.
      f.setResizable(false);
      // add a listener to save the Template panel's info if the user browses away to another panel.
      f.addComponentListener(new ComponentAdapter(){
          public void componentHidden(ComponentEvent e){
              
              if(!EyesHandsManager.tempDisabled && ehd.TempPane.MDT.IsUnSaved()){
                f.setVisible(true);
                if(ehd.TempPane.MDT.CheckUnSaveBeforeChange(AIPUtil.getString("General.Msg.SaveTemplete")))
                ehd.ExitConnector();
              } else ehd.ExitConnector();
          }
          public void componentMoved(ComponentEvent e){

          }
          public void componentShown(ComponentEvent e){
          }
     });
  }
 
  /**
   * checks for the hidden file .AIPShutDown and shuts down manually
   * if it finds it.
   */
	public static void checkManualShutDown() {
		/*
		 * added on 2003-12-12 if shutdown file exists, shut down manually
		 */
		try {
			java.io.File f = new java.io.File(".AIPShutDown");
			if (f.exists()) {
				VWLogger.audit("Manual Shutdown Initiated...");
				java.util.Vector sids = AIPManager.getConnectSessionIds();
				for (int i = 0; i < sids.size(); i++) {
					AIPManager.logout(((Integer) sids.get(i)).intValue());
				}
				AIPManager.shutdown();

				f.deleteOnExit();
				System.exit(0);
			}
		} catch (Exception e) {
			VWLogger.info("Exception in checkManualShutDown :" + e.getMessage());
		}
		/**/
	}
  /**
   * returns true if an initiated shutdown was bypassed
   * returns false if no shutdown was initiated
   */
  public static boolean bypassManualShutDown()
  {
            /* added on 2003-12-12
             * if shutdown file exists, shut down manually
             */
            java.io.File f = new java.io.File(".AIPShutDown");
            if (f.exists()) 
            {
                f.delete();
                return true;
            }
            return false;
            /**/
  }
  /* Issue # 754: If VWS server is down, client should be notified
  *  Methods added of issue 754, C.Shanmugavalli, 10 Nov 2006
  */
  public EHMainPanel getEHMainPanel()
  {
  	return this.ehd;
  }
  public static AIPConnector getInstance()
  {
  	return AIPConnObject;
  }
  
  public static void stopService(int eventId)
  {
	  
      AIPManager.logoutAll();
      AIPManager.shutdown();
      AIPManager.setAIPServiceStarted(false);
  }
  // End of Issue # 754
  public static void main(String s[]) {
	  boolean createAipConnector = false;
	  final ResourceManager connectorManager = ResourceManager.getDefaultManager();
        try {
             /* added by amaleh on 2003-11-04 */
        	AIPManager.initLogger();
        	VWLogger.info("application parameter ::::"+s);
             if (s.length > 0)
             {
            	 VWLogger.info("application running status ::::"+s.length);
            	 //Condition added for 10.2
            	 VWLogger.info("application running status ::::"+AIPManager.isApplicationProcessRunning());
              	 if(!AIPManager.isApplicationProcessRunning()){
              		 VWLogger.info("Inside if condition of application running status"+AIPManager.isAIPApplicationStarted());
              		 if(AIPManager.isAIPApplicationStarted()){
              			 AIPManager.setAIPApplicationStarted(false);
              			 VWLogger.info("application running status inside second if::::"+AIPManager.isApplicationProcessRunning());

              		 }
              	 }//End of condition added in 10.2
            	 if(!AIPManager.isAIPApplicationStarted())
                 {
                     if (s[0].equalsIgnoreCase("autostart") | s[0].equalsIgnoreCase("-autostart") | s[0].equalsIgnoreCase("/autostart"))
                     {    
                         com.computhink.aip.AIPManager.setAutoStart(true);
                         AIPManager.setAIPServiceStarted(true);
                         createAipConnector = true;
                         // this option "manual start" will start the AIP application and started processing the "startup room"
                     }else if (s[0].equalsIgnoreCase("manual") | s[0].equalsIgnoreCase("manualstart") | s[0].equalsIgnoreCase("ms")){
                    	 AIPManager.setCommandPrompt(true);
                         AIPManager.setAutoStart(true);    
                         //AIPManager.setAIPApplicationStarted(true);
                         createAipConnector = true;
                     }
                 }else{
                     VWLogger.info("AIP Application is Running! Stopping AIP Service!");
                     FoldersMonitoring.stopAIPService();
                 }
             }else{
                 if(AIPManager.isAIPServiceStarted())
                 {
                     JOptionPane.showMessageDialog(null, connectorManager.getString("Aip.Stopping.Service1")+" AIP "+connectorManager.getString("Aip.Stopping.Service2"));
                     VWLogger.info("Launching the AIP Application! Stopping the AIP Service!");
                     AIPManager.setAutoStart(false);
                     FoldersMonitoring.stopAIPService();
                 }
                 AIPManager.setAIPApplicationStarted(true);
                 createAipConnector = true;
             }
             
             /**/
             String mac      = "com.sun.java.swing.plaf.mac.MacLookAndFeel";
             String metal    = "javax.swing.plaf.metal.MetalLookAndFeel";
             String motif    = "com.sun.java.swing.plaf.motif.MotifLookAndFeel";
             String windows  = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
             String plasticLookandFeel  = "com.jgoodies.looks.plastic.Plastic3DLookAndFeel";
             //UIManager.setLookAndFeel(plasticLookandFeel);
             UIManager.setLookAndFeel(windows);
             if (VWCPreferences.getFontName().trim().length()> 0)
         		fontName = VWCPreferences.getFontName();
         	fontSize = VWCPreferences.getFontSize();
         	setUIFont(new FontUIResource(fontName, Font.PLAIN, fontSize));
             //UIManager.getSystemLookAndFeelClassName() ;
         } catch (Exception e) {}
         if (createAipConnector){
        	 if(AIPConnObject == null )
        		 AIPConnObject = new AIPConnector();
         }
          //AIPConnector In =new AIPConnector();
  }
  /*
	 * Added for localization to set font
	 * Modified by Madhavan
	 */
public static void setUIFont(javax.swing.plaf.FontUIResource f) {
	java.util.Enumeration keys = UIManager.getDefaults().keys();
	while (keys.hasMoreElements()) {
	    Object key = keys.nextElement();
	    Object value = UIManager.get(key);
	    
	    if (value instanceof javax.swing.plaf.FontUIResource){		
		UIManager.put(key, f);
	    }
	}
    }

}