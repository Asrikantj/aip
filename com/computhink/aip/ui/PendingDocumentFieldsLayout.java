package com.computhink.aip.ui;

import com.computhink.aip.*;
import com.computhink.aip.resource.images.ImageManager;
import com.computhink.aip.util.*;
import com.computhink.aip.client.*;
import com.computhink.common.DocType;
import com.computhink.common.Index;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.util.Vector;
import javax.swing.border.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
/* added by amaleh on 2003-08-08 */
import java.util.StringTokenizer;



/**/

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class PendingDocumentFieldsLayout extends JPanel {

  private PendingDocumentDialog PDD;
  private Vector Tabrows;
  private String[] columnNames;
  public JTable DTtable;
  private boolean isTableEdit =true;
  public boolean isTableUpdate =false;
  SpecialTableModel TableModel;
  public AIPDocument currPendDoc;
  public AIPForm  currAIPForm;


  private final int MARGINCOL       =0 ;  private final int EHFIELDCOL     =1 ;
  private final int VALUECOL        =2 ;  private final int EHFIELDIDCOL   =3 ;
  private final int FSTATUSCOL      =4 ;  private final int CSTATE         =5 ;
  private final int MAXCOLNUM = 6;

  public PendingDocumentFieldsLayout(PendingDocumentDialog PDD1) {
    PDD =PDD1;
    setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
    setAlignmentX(Component.LEFT_ALIGNMENT);
    add(Box.createVerticalStrut(5));
    add(createDocFieldsPanel(),BorderLayout.CENTER);
  }
  public JScrollPane createDocFieldsPanel(){
        Tabrows =new Vector();
        TableModel  = new SpecialTableModel();
        DTtable = new JTable(TableModel);
        DTtable.setRowHeight(20);
        DTtable.getTableHeader().setReorderingAllowed(false);
        DTtable.getTableHeader().setMinimumSize(new Dimension(30,21));
        DTtable.getTableHeader().setPreferredSize(new Dimension(50,21));
        DTtable.getTableHeader().setAlignmentX(LEFT_ALIGNMENT) ;
        DTtable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        DTtable.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);

        TableMouseListener MListener =new TableMouseListener();
        DTtable.addMouseListener(MListener);

        DTtable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        ListSelectionModel SelectionModel= DTtable.getSelectionModel();
        JScrollPane tableScroller = new JScrollPane(DTtable);
        tableScroller.addMouseListener(MListener);
        for(int i=0;i<DTtable.getColumnCount();i++){
             TableColumn column = DTtable.getColumnModel().getColumn(i);
             if (i==MARGINCOL){
                 column.setPreferredWidth(18);
                 column.setMaxWidth(18);
                 column.setCellRenderer(new ButtonRenderer());
                 column.setCellEditor(new ButtonEditor());
             }else if (i == EHFIELDCOL){
                 column.setMaxWidth(365);
                 column.setPreferredWidth(100);
             }else if (i == VALUECOL){
                 column.setMaxWidth(380);
                 column.setPreferredWidth(150);
                 column.setCellRenderer(new RowRenderer());
             }
        }
        tableScroller.setAlignmentX(Component.LEFT_ALIGNMENT);
        return tableScroller;
  }


  public void setAllEnable(boolean enable){
      isTableEdit =enable;
  }
  public boolean IsUnSaved(){
      return isTableUpdate;
  }
//--------------------------Doctype & Indices methods---------------
  public void setMasterInfo (AIPDocument DInfo,AIPForm FInfo){
      currPendDoc = DInfo;
      currAIPForm =FInfo;
      TableModel.RemoveAllRows();
      LoadTableData(currPendDoc.getAIPForm().getFields(),currAIPForm.getFields());
  }
  
  /* added by amaleh on 2003-07-29 */
  /** This version works for "No Templates" mode */
  public void setMasterInfo (AIPDocument DInfo){
      
      currPendDoc = DInfo;
      TableModel.RemoveAllRows();
      LoadTableData(currPendDoc);
  }
  /**/

  public void LoadTableData(Vector PDFieldsData,Vector FormFieldsData){
        for(int i=0;i< PDFieldsData.size();i++){
           if(FormFieldsData.size() > i){
                AIPField docField = (AIPField)PDFieldsData.get(i);
                AIPField FormField =getAIPFormField(docField.getOrder(),FormFieldsData);
                String FieldName =" ";
                if(FormField != null) FieldName=FormField.getName();
                String FieldValue =  docField.getValue();
                //int FieldId = ((Integer)((Vector)PDFieldsData.get(i)).get(0)).intValue();
                Vector Trow =new Vector();
                Trow.add(MARGINCOL,"");
                Trow.add(EHFIELDCOL,FieldName);
                Trow.add(VALUECOL,FieldValue);
                Trow.add(EHFIELDIDCOL,new Integer(docField.getId()));
                Trow.add(FSTATUSCOL, new Integer(docField.getStatus()));
                Trow.add(CSTATE,new Integer(1));
                Tabrows.add(Trow);
                TableModel.fireTableRowsInserted(Tabrows.size()-1,Tabrows.size()-1);
            }
        }
  }
  /* added by amaleh on 2003-07-29 */
  /** This version works for "No Templates" mode */
  public void LoadTableData(AIPDocument aipDoc){
      Vector PDFieldsData = aipDoc.getAIPForm().getFields();
      String fieldNamesString = aipDoc.getFieldNames();
      StringTokenizer fieldNamesTokens = new StringTokenizer(fieldNamesString, "|");
        for(int i=0;i< PDFieldsData.size();i++){
           //if(FormFieldsData.size() > i){
             
                AIPField docField = (AIPField)PDFieldsData.get(i);
                //EHFormFieldInfo FormField =getAIPFormField(FieldInfo.getFieldOrder(),FormFieldsData);
                String FieldName ="";
                //if(FormField != null) FieldName=FormField.getEHFieldName();
                if (fieldNamesTokens.hasMoreTokens())
                    FieldName = fieldNamesTokens.nextToken();
                String FieldValue =  docField.getValue();
                //int FieldId = ((Integer)((Vector)PDFieldsData.get(i)).get(0)).intValue();
                Vector Trow =new Vector();
                Trow.add(MARGINCOL,"");
                Trow.add(EHFIELDCOL,FieldName);
                Trow.add(VALUECOL,FieldValue);
                Trow.add(EHFIELDIDCOL,new Integer(docField.getId()));
                Trow.add(FSTATUSCOL, new Integer(docField.getStatus()));
                Trow.add(CSTATE,new Integer(1));
                Tabrows.add(Trow);
                TableModel.fireTableRowsInserted(Tabrows.size()-1,Tabrows.size()-1);
            //}
                 
        }
  }
  /**/
  public AIPField getAIPFormField(int order,Vector aipFormFields){
        for(int i=0;i<aipFormFields.size();i++){
            AIPField formField =(AIPField)aipFormFields.get(i);
            if(formField.getOrder() == order) return formField;
        }
        return null;
  }
  //added Ayman
  public void setUpdateDocFields()
  { 
       StringBuffer fieldNamesBuffer = new StringBuffer();
      for(int i=0;i< TableModel.getRowCount();i++){
         if(((Integer)TableModel.getValueAt(i,CSTATE)).intValue() > 1){
             setAIPDocField(i,TableModel.getValueAt(i,VALUECOL).toString());
         }
         fieldNamesBuffer.append(TableModel.getValueAt(i,EHFIELDCOL).toString() + "|");         
      }
      String fieldNames = new String(fieldNamesBuffer);
      if ((currPendDoc.getFieldNames() != null) && (!currPendDoc.getFieldNames().trim().equals("")))
        currPendDoc.setFieldNames(fieldNames); 
        
  } 
  // ayman replaced with setUpdateDocFields()
 /* public boolean SaveUpdateDocFields(){
    
     for(int i=0;i< TableModel.getRowCount();i++){
         if(((Integer)TableModel.getValueAt(i,CSTATE)).intValue() > 1){
             UpdateEHDocFieldInfo(i,TableModel.getValueAt(i,VALUECOL).toString());
         }
         fieldNamesBuffer.append(TableModel.getValueAt(i,EHFIELDCOL).toString() + "|");
     }
     fieldNamesBuffer.deleteCharAt(fieldNamesBuffer.length() - 1);
     String fieldNames = new String(fieldNamesBuffer);
     UpdateEHDocument(fieldNames);
     return true;
  }*/
  /* added by amaleh on 2003-08-15 */
  //remark Ayman
  /*public void UpdateEHDocument(String fieldNames)
  {
     //added by amaleh on 2004-02-06 to work with both templates and non templates mode
     if ((currPendDoc.getFieldNames() != null) && (!currPendDoc.getFieldNames().trim().equals("")))
        currPendDoc.setFieldNames(fieldNames);
     PDD.PFL.EHManager.UpdateEHDocument(currPendDoc,AIPManager.getCurrSid());
  }*/
  /**/
  //update ayman
  public void setAIPDocField(int Order,String Value){
      for(int i=0;i<currPendDoc.getAIPForm().getFields().size();i++){
           if(i == Order) {
              AIPField docField =(AIPField)currPendDoc.getAIPForm().getFields().get(i);
                docField.setValue(Value);
                //PDD.PFL.EHManager.UpdateEHDocField(currPendDoc.getId(),docField,AIPManager.getCurrSid());
           }
      }
  }
//--------------------table Inner class------------------------

 private class TableMouseListener extends MouseAdapter  {
      public void mousePressed(MouseEvent e) {
           if (e.getClickCount() == 1){
              int Pointrow =DTtable.rowAtPoint(e.getPoint());
              if(Pointrow > -1)
                  DTtable.setRowSelectionInterval(Pointrow,Pointrow);
              if (e.getModifiers() == e.BUTTON3_MASK) {
              }else if (e.getModifiers() == e.BUTTON1_MASK){

              }
           }
     }
     public void mouseReleased(MouseEvent e) {
     }

  }
  public  class SpecialTableModel extends AbstractTableModel {
       String[] columnNames={"",AIPUtil.getString("Pending.TCol.FieldName"),AIPUtil.getString("Pending.TCol.FieldValue")};
       Vector ARow;
       int Srow;
       public SpecialTableModel(){
       }
       public int getColumnCount() {
           return columnNames.length;
       }
       public int getRowCount() {
          return Tabrows.size();
       }
       public String getColumnName(int col) {
         return columnNames[col];
       }
       public Object getValueAt(int aRow, int aColumn) {
          Vector row = (Vector) Tabrows.elementAt(aRow);
          return row.elementAt(aColumn);
       }
       public Class getColumnClass(int c) {
          return getValueAt(0, c).getClass();
       }
       public boolean isCellEditable(int row, int col) {
           if(!isTableEdit) return false;
           return isDocFieldsCellEditable(row,col);
       }
       public void setValueAt(Object value, int row, int col) {
              Vector row1 = (Vector) Tabrows.elementAt(row);
              row1 = (Vector) Tabrows.elementAt(row);
              if(value ==null) return;
              if(value.toString().equals(row1.elementAt(col).toString())) return;
              row1.remove(col);
              row1.add(col,value);
              row1.remove(CSTATE);
              row1.add(CSTATE,new Integer(2));//State = update
              if(!isTableUpdate){
               isTableUpdate = true;
               PDD.Savebtn.setEnabled(true);
              }

       }
       public boolean isAllColumnEmpty(int col){
              for(int i=0;i <Tabrows.size();i++){
                 if(!getValueAt(i,col).toString().equals("") ) return false;
              }
              return true;
       }
       public void RemoveRow(int Rnum){
        /*  int Order =((Integer)getValueAt(Rnum,ORDER)).intValue();
          EHFormFieldInfo Info= (EHFormFieldInfo)EHFormFields.get(Order);
          Info.setState(3);//Delete
          Tabrows.remove(Rnum);
          fireTableRowsDeleted(Rnum,Rnum);*/
       }
       public void RemoveAllRows(){
              int lastrow =Tabrows.size();
              if(lastrow >0){
                Tabrows.removeAllElements();
                this.fireTableRowsDeleted(0,lastrow-1);
              }
       }
       public boolean isDocFieldsCellEditable(int row, int col) {
             //ARow = (Vector) Tabrows.elementAt(row);
           if(col == MARGINCOL) return false;
           Srow =row;
           TableColumn column;
           JComboBox EditCombo =new JComboBox();
           //added Ayman 06-08-2004
           if(col == EHFIELDCOL){
                column = DTtable.getColumnModel().getColumn(EHFIELDCOL);
                Vector IndexName=new Vector();                
                //check if it is "templates" or "no templates" mode
                if ((currPendDoc.getFieldNames() != null) && (!currPendDoc.getFieldNames().trim().equals("")))
                {
                    String DTName = currPendDoc.getAIPForm().getName();
                    DocType vwDT = DoctypesManager.getDoctype(AIPManager.getCurrSid(), DTName);
                    if(vwDT !=null)
                    {    
                        Vector DTIndices = vwDT.getIndices();
                        for (int i = 0; i < DTIndices.size(); i++)
                        {
                            IndexName.add((i), ((Index)DTIndices.get(i)).getName());
                        }
                        EditCombo =new JComboBox(IndexName);
                        EditCombo.setEditable(true);
                        column.setCellEditor(new DefaultCellEditor(EditCombo));
                        return true;
                    }
                    else
                    {
                       column.setCellEditor(new DefaultCellEditor(new JTextField())); 
                    }    
                }
                else
                {
                    // "Templates" mode
                    return false;
                    /*
                    String FormName = CurrPDInfo.getEHFormName();
                    Vector EHForms = PDD.PFL.EHManager.getEHForms(InitParamsInfo.CurrRoomId, true);
                    EHFormInfo FormInfo = null;
                    for (int i = 0; i < EHForms.size(); i++)
                    {
                        FormInfo = (EHFormInfo)EHForms.get(i);
                        if (FormInfo.getEHFormName().equals(FormName))
                            break;
                        FormInfo = null;
                    }
                    if (FormInfo == null)
                        return false;
                    else
                    {
                        Vector FormFields = FormInfo.getFormFields();
                        for (int i = 0; i < FormFields.size(); i++)
                        {
                            IndexName.add((i), ((EHFormFieldInfo)FormFields.get(i)).getEHFieldName());
                        }
                    }
                     */
                }
                
            }
            if(col == VALUECOL){
                column = DTtable.getColumnModel().getColumn(VALUECOL);
                int  IndexType = 0;
                String IndexMask = new String();
                int IndexSvid = 0;
                int IndexID = 0;
                Index indexInfo =null;
                
                //check if it is "templates" or "no templates" mode
                if ((currPendDoc.getFieldNames() != null) && (!currPendDoc.getFieldNames().trim().equals("")))
                {
                    // "No Templates" mode
                    String FieldName = (String)getValueAt(Srow, EHFIELDCOL);
                    //pull Index and extract mask and type from it
                    String DTName = currPendDoc.getAIPForm().getName();
                    indexInfo =DoctypesManager.getIndex(AIPManager.getCurrSid(), DTName,FieldName);                                         
                    if (indexInfo == null)
                        return false;
                    IndexMask = indexInfo.getMask();
                    IndexType = indexInfo.getType();
                    //IndexSvid = indexInfo.getSvid();
                    IndexID = indexInfo.getId();
                    //todo have fieldnames combo boxes of field names if the doctype or form was valid
                    //EHDocFieldInfo FieldInfo = (EHDocFieldInfo)PDFieldsData.get(Srow);
                    //EHFormFieldInfo FormField =getEHFormField(FieldInfo.getFieldOrder(),FormFieldsData);
                    
                }
                else
                {
                    // "Templates" mode
                    String FieldName = (String)getValueAt(Srow, EHFIELDCOL);
                    String FormName = currPendDoc.getAIPForm().getName();                    
                    AIPForm FormInfo = PDD.PFL.getAIPForm(FormName);                     
                    if (FormInfo == null)
                        return false;
                    else
                    {
                        Vector FormFields = FormInfo.getFields();
                        AIPField FormFieldInfo = null;
                        for (int i = 0; i < FormFields.size(); i++)
                        {
                            FormFieldInfo = (AIPField)FormFields.get(i);
                            if (FormFieldInfo.getName().equals(FieldName))
                                break;
                            FormFieldInfo = null;
                        }
                        if (FormFieldInfo == null)
                            return false;
                        else
                        {
                            if(FormFieldInfo.getMapIndexId() <= 0)
                                return false;             
                            else
                            {
                                IndexID = FormFieldInfo.getMapIndexId();                                
                                indexInfo = DoctypesManager.getIndex(AIPManager.getCurrSid(),FormInfo.getMapDTId(), IndexID);
                                IndexMask = indexInfo.getMask();
                                IndexType = indexInfo.getType();
                                //IndexSvid = indexInfo.getSvid();
                                //IndexID = indexInfo.getIndexid();
                            }
                        }
                    }
                }
                
                if( IndexType == 0 || IndexType == 5){
                       if(IndexMask.equals("") || IndexMask.equals(" "))
                         column.setCellEditor(new DefaultCellEditor(new JTextField(" ")));
                       else{
                         JMaskedTextField MaskField =new JMaskedTextField();
                         MaskField.setDatatype(MaskField.texttype);
                         MaskField.setMask(IndexMask);
                         column.setCellEditor(new DefaultCellEditor(MaskField));
                       }
                }if( IndexType == 1){
                        NumericField MaskField = new NumericField(0,9);
                        column.setCellEditor(new DefaultCellEditor(MaskField));

                }else if( IndexType == 2){
                     // System.out.println(((DoctypeIndicesInfo)RDTIndices.get(Srow)).getSvid());
                      if(IndexSvid == 1){
                          return false;
                      }
                      VWDateComboBox EditDateCombo = new VWDateComboBox();
                      EditDateCombo.setDateFormat(IndexMask);
                      column.setCellEditor(new DefaultCellEditor(EditDateCombo));
                } else if(IndexType == 3){
                      String[] EComboData ={"Yes","No"};
                      EditCombo =new JComboBox(EComboData);
                      column.setCellEditor(new DefaultCellEditor(EditCombo));
                } else if(IndexType == 4){
                      Vector SDATA=indexInfo.getDef();
                     
                      EditCombo =new JComboBox(SDATA);
                      column.setCellEditor(new DefaultCellEditor(EditCombo));
                }

                return true;
            }            
            return true;
       } 
       
   }
   private class ButtonRenderer extends JButton
                        implements TableCellRenderer {
        ImageManager IM =new ImageManager();
        public ButtonRenderer() {
            super();
            //this.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
        }
        public Component getTableCellRendererComponent(
                                JTable table, Object color,
                                boolean isSelected, boolean hasFocus,
                               int row, int column) {
          int Status =((Integer)TableModel.getValueAt(row,FSTATUSCOL)).intValue();
          if(Status == 1){
            setIcon(IM.getImageIcon("deleterec.gif"));
           // setToolTipText("Eyes & Hands Form Deleted");
          }else setIcon(null);
          return this;
        }
   }
   class RowRenderer extends DefaultTableCellRenderer {
       public RowRenderer(){
          super();
       }
       public Component getTableCellRendererComponent(
            JTable table, Object value,
            boolean isSelected, boolean hasFocus,
            int row, int column) {

          //  setForeground(Color.red);
            //setForeground(Color.black);
            return   super.getTableCellRendererComponent(table,value
                        ,isSelected,hasFocus,row,column);

       }
  }
   private class ButtonEditor extends DefaultCellEditor {
       public ButtonEditor() {
                super(new JCheckBox());
            JButton b =new JButton();
            editorComponent = b;
            setClickCountToStart(1);
            b.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    fireEditingStopped();
                }
            });
       }
       protected void fireEditingStopped() {
            super.fireEditingStopped();
       }

       public Object getCellEditorValue() {
            return null;
       }
       public Component getTableCellEditorComponent(JTable table,
                                                     Object value,
                                                     boolean isSelected,
                                                     int row,
                                                    int column) {
          return editorComponent;
       }
   }
}