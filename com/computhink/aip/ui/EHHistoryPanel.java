package com.computhink.aip.ui;

import com.computhink.aip.*;
import com.computhink.aip.resource.ResourceManager;
import com.computhink.aip.util.AIPUtil;
import com.computhink.aip.util.VWLogger;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


import java.io.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public class EHHistoryPanel extends JPanel {
  ProcessFileLayout PFL;
  JButton Procssbtn;
  JButton Delbtn;
  private JButton hlpbtn;
  JButton DelArchivebtn;
  EHMainPanel EHD;
  /*
   * Enhancement 	: Enhancements of AIP  
   * 				  Requirment sent by Rajesh on 09-June-2006 through eMail  	 
   * Created By		: M.Premananth
   * Date			: 14-June-2006
   */
  
  public static int iStartIndex;
  public static int iEndIndex;
  public static int iTotalCount;
  public static int iMaximumRows;
  
  
  JTextField archivalFolderPath;
  JButton    browseButton;
  JFileChooser fileChooser;
  JSpinner      sHistoryLife;
  JCheckBox enableArchive;
  JLabel lHistoryLife;
  JLabel lHistoryLifeSuff;  
  
  private final ResourceManager connectorManager = ResourceManager.getDefaultManager();
	private  String languageLocale = ResourceManager.getDefaultManager().getLocale().toString(); 
  
  public EHHistoryPanel(EHMainPanel EHD1) {
     EHD= EHD1;
     PFL =new ProcessFileLayout(this);
     setBorder(new EmptyBorder(5,5,5,5));
     JPanel Panel1 =new JPanel();
     this.add(Panel1,BorderLayout.CENTER);
     Panel1.setMinimumSize(new Dimension(200,100));
     //Panel1.setPreferredSize(new Dimension(550,350));
     Panel1.setPreferredSize(new Dimension(685,520));
     Panel1.setLayout(new BoxLayout(Panel1,BoxLayout.Y_AXIS));
     Panel1.setBorder(new CompoundBorder(new TitledBorder(null,null,
	TitledBorder.LEFT, TitledBorder.TOP),new EmptyBorder(5,8,5,5)));
     Panel1.add(PFL);
     Panel1.add(Box.createVerticalStrut(10));

     /*
      * Enhancement 	: Enhancements of AIP  
      * 				  Requirment sent by Rajesh on 09-June-2006 through eMail  	 
      * Created By		: M.Premananth
      * Date			: 14-June-2006
      */
     JPanel historyPanel =new JPanel();
     historyPanel.setLayout(new BoxLayout(historyPanel, BoxLayout.X_AXIS));
     historyPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
     JLabel aJLabel = new JLabel(connectorManager.getString("History.Lab.ArchivalFolder"));
     historyPanel.add(aJLabel);
     historyPanel.add(Box.createHorizontalStrut(3));
     
     archivalFolderPath = new JTextField(AIPManager.getArchivalFolder());
     archivalFolderPath.setMaximumSize(new java.awt.Dimension(350, 20));
     archivalFolderPath.setPreferredSize(new java.awt.Dimension(350, 20));
     String path = AIPManager.getArchivalFolder();
     if("".equals(path))
     {
    	 path = AIPUtil.findArchivalPath();
    	 AIPManager.setArchivalFolder(path);
    	 AIPManager.setAgeForArchive(24);
    	 AIPManager.setArchiveCount(10000);
     }
     
     archivalFolderPath.setText(path);
     archivalFolderPath.setEnabled(false);
     archivalFolderPath.setToolTipText(connectorManager.getString("EHHistoryPanel.Folder")+AIPUtil.findHome()+")");
     historyPanel.add(archivalFolderPath);
     historyPanel.add(Box.createHorizontalStrut(5));
     Cursor handCursor = new Cursor(Cursor.HAND_CURSOR);
     browseButton = new JButton(); 
     browseButton.setIcon(new javax.swing.ImageIcon(getClass()
    		 .getResource(
    		 "/com/computhink/aip/resource/images/OpenFolderIcon.png")));
     browseButton.setMaximumSize(new java.awt.Dimension(35, 20));
     browseButton.setPreferredSize(new java.awt.Dimension(35, 20));
     browseButton.setCursor(handCursor);
     historyPanel.add(browseButton);
     DelArchivebtn     =new JButton(AIPUtil.getString("History.Btn.DeleteArchived"));
     if(languageLocale.equals("nl_NL")){
    	  DelArchivebtn.setPreferredSize(new Dimension(112+45,25));
     }else
     DelArchivebtn.setPreferredSize(new Dimension(112,25));
     DelArchivebtn.setMinimumSize(new Dimension(112,25));
     DelArchivebtn.setMnemonic(AIPUtil.getString("History.MBtn.DeleteArchived").charAt(0));
     historyPanel.add(Box.createHorizontalStrut(60+30));
     historyPanel.add(DelArchivebtn);
     final JPanel finalInformationPanel = historyPanel;
     //final String sPath = path;
     browseButton.addActionListener(new ActionListener(){
    	 public void actionPerformed(ActionEvent PassedActionEvent)
    	 {
    		 String sPath = archivalFolderPath.getText().trim();
    		 File tempfolder = new File(sPath);
    		 fileChooser     = new JFileChooser(tempfolder);
    		 fileChooser.setFileSelectionMode(fileChooser.DIRECTORIES_ONLY);
    		 fileChooser.setBounds(5, 5, 500,500);
    		 int returnVal = fileChooser.showOpenDialog(finalInformationPanel);
    		 if (returnVal == JFileChooser.APPROVE_OPTION) 
    		 {
    			 File selectedFile = fileChooser.getSelectedFile();
    			 String pathName = selectedFile.getPath();    			 
    			 pathName = checkArchivalPath(pathName);    			 
    			 archivalFolderPath.setText(pathName);
    			 AIPManager.setArchivalFolder(pathName);
    		 }
    	 }
     });  
     Panel1.add(historyPanel);
     Panel1.add(Box.createVerticalStrut(5));
     Panel1.add(CreateButtonPanel());

  }
  
  /**
   * Desc   :This method takes the argument as selected path. It checks weather the selected path is homepath or not. If it is 
   * 		 homepath returns AIPUtil.findArchivalPath(), selectedPath(argument passed) otherwise.
   * Author :Nishad Nambiar
   * Date   :15-May-2008
   * 
   * @return archivalPath
   */
  public String checkArchivalPath(String selectedPath){
		String homePath = AIPUtil.findHome();
		String archivalPath = "";
		archivalPath = (selectedPath.equalsIgnoreCase(homePath.trim())) ? AIPUtil.findArchivalPath() : selectedPath;
		return archivalPath;
  }
  
  public JPanel CreateButtonPanel(){
    JPanel BtnPanel =new JPanel();
    BtnPanel.setLayout(new BoxLayout(BtnPanel, BoxLayout.X_AXIS));
    BtnPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
    ButtonListener btnListener =new ButtonListener();
    Procssbtn  =new JButton(AIPUtil.getString("History.Btn.Process"));
    Delbtn     =new JButton(AIPUtil.getString("History.Btn.Delete"));
    hlpbtn     =new JButton(AIPUtil.getString("History.Btn.Help"));

    hlpbtn.setMnemonic(AIPUtil.getString("History.MBtn.Help").charAt(0));
    Delbtn.setMnemonic(AIPUtil.getString("History.MBtn.Delete").charAt(0));
    Procssbtn.setMnemonic(AIPUtil.getString("History.MBtn.Process").charAt(0));
    
    hlpbtn.addActionListener(btnListener);
    Delbtn.addActionListener(btnListener);
    Procssbtn.addActionListener(btnListener);
    DelArchivebtn.addActionListener(btnListener);
    if(languageLocale.equals("nl_NL")){
    	hlpbtn.setPreferredSize(new Dimension(120,25));
    }else
    	hlpbtn.setPreferredSize(new Dimension(80,25));
    hlpbtn.setMinimumSize(new Dimension(80,25));
    if(languageLocale.equals("nl_NL")){
    	Delbtn.setPreferredSize(new Dimension(120,25));
    }else
    	Delbtn.setPreferredSize(new Dimension(80,25));	
    Delbtn.setMinimumSize(new Dimension(80,25));
    Procssbtn.setPreferredSize(new Dimension(85,25));
    Procssbtn.setMinimumSize(new Dimension(85,25));

    hlpbtn.setEnabled(true);
    Delbtn.setEnabled(false);
    Procssbtn.setEnabled(false);
    DelArchivebtn.setEnabled(true);

    /*
     * Enhancement 	: Enhancements of AIP  
     * 				  Requirment sent by Rajesh on 09-June-2006 through eMail  	 
     * Created By		: M.Premananth
     * Date			: 14-June-2006
     */
    lHistoryLife =new JLabel(connectorManager.getString("History.Lab.ArchiveHistories")+"  ");
    SpinnerNumberModel sModel = new SpinnerNumberModel(1,1,420, 1);         
    sHistoryLife =new JSpinner(sModel);      
    JSpinner.DefaultEditor editor = (JSpinner.DefaultEditor)sHistoryLife.getEditor();       
    JTextField spfield  = (JTextField)editor.getTextField();
    
    spfield.setBorder(BorderFactory.createEmptyBorder());  
    sHistoryLife.setMaximumSize(new Dimension(45,21));
    sHistoryLife.setPreferredSize(new Dimension(45,21));
    sHistoryLife.setValue(new Integer(AIPManager.getAgeForArchive()));
    sHistoryLife.addChangeListener(new ChangeListener(){
        public void stateChanged(ChangeEvent e)
        {
            String newVal = ((JSpinner)e.getSource()).getValue().toString();
            int iVal =AIPUtil.to_Number(newVal);
            if(iVal >= 1 && iVal <= 240)
            {
                AIPManager.setAgeForArchive(iVal);
            }    
        } 
    }); 
    lHistoryLifeSuff =new JLabel("  "+connectorManager.getString("History.Lab.HoursAfterProcessingTime"));
    JPanel historyLifePanel =new JPanel();
    historyLifePanel.setLayout(new BoxLayout(historyLifePanel,BoxLayout.X_AXIS));
    historyLifePanel.setAlignmentX(Component.LEFT_ALIGNMENT);
    Dimension min = new Dimension(1,1);
    Dimension max = new Dimension(1,1);
    Dimension pref = new Dimension(1,1);
    historyLifePanel.add(new Box.Filler(min,pref,max)); 
    
    
 	enableArchive = new JCheckBox("");
	enableArchive.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent actionEvent){
			if(enableArchive.isSelected()){
				setAllHistoryEnabled(true);
				ArchiveHistory archivehistory = new ArchiveHistory(EHD.GenPane);
				archivehistory.setStatus(false);
				AIPManager.addArchiveThread(AIPManager.getCurrSid(),archivehistory);
				AIPManager.setArchiveHistoryEnabled(AIPManager.getCurrSid(),true);
				archivehistory.start();
			}
			else{
				AIPManager.setArchiveHistoryEnabled(AIPManager.getCurrSid(),false);
				ArchiveHistory archiveHistory = AIPManager.removeArchiveThread(AIPManager.getCurrSid());
				if(archiveHistory != null)
					archiveHistory.setStatus(true);
				setAllHistoryEnabled(false);
			}
		}
	});
	
	historyLifePanel.add(enableArchive);
    historyLifePanel.add(lHistoryLife);
    historyLifePanel.add(sHistoryLife);
    historyLifePanel.add(lHistoryLifeSuff);
    //historyLifePanel.add(Box.createHorizontalStrut(40));
    //historyLifePanel.add(DelArchivebtn);
    historyLifePanel.add(Box.createHorizontalGlue());
	//BtnPanel.add(Box.createHorizontalStrut(17));
	//BtnPanel.add(enableArchive);
	
    BtnPanel.add(historyLifePanel);
    BtnPanel.add(Delbtn);
    BtnPanel.add(Box.createHorizontalStrut(3));
    BtnPanel.add(hlpbtn);
    BtnPanel.setAlignmentX(Component.LEFT_ALIGNMENT);

    return BtnPanel;

  }
  /*
   * Enhancement 	: Enhancements of AIP  
   * 				  Requirment sent by Rajesh on 09-June-2006 through eMail  	 
   * Created By		: M.Premananth
   * Date			: 14-June-2006
   */
  public void setAllEnabled(boolean enable){
      if(!enable){
      Procssbtn.setEnabled(enable);
      Delbtn.setEnabled(enable);
      PFL.table.clearSelection();
      }
      else PFL.CheckEnableButton();
      hlpbtn.setEnabled(enable);
      PFL.setAllEnabled(enable);
      DelArchivebtn.setEnabled(enable);
  }
  public void RemoveAllPFLRows(){
      PFL.TableModel.RemoveAllRows();
      Delbtn.setEnabled(false);
      Procssbtn.setEnabled(false);
  }
  public void setAllHistoryEnabled(boolean enable){
  	enableArchive.setSelected(enable);
		sHistoryLife.setEnabled(enable);
		lHistoryLifeSuff.setEnabled(enable);
}

//----------------------------------Actions & Listeners----------------------
  class ButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
           JButton Source = (JButton)e.getSource();
           if( Source.equals(Procssbtn)){
              if(!EHD.TempPane.MDT.CheckUnSaveBeforeChange(AIPUtil.getString("General.Msg.SaveTemplete2")))  return;
              EHD.setCursor(new Cursor(Cursor.WAIT_CURSOR));
              hlpbtn.setEnabled(false);
              Delbtn.setEnabled(false);
              Procssbtn.setEnabled(false);              
              //EyesHandsManager.getInitParams(AIPManager.getCurrSid()).setDefaultRoomNodeId(EHD.GenPane.VWNodes.getSelectedNodeid());
              PFL.ProcessSelectedRows();
              /*EHD.TempPane.MDT.setMapDoctypes(PFL.VWDoctypes);
              EHD.TempPane.RemoveAllMDTRows();
              EHD.TempPane.MDT.LoadTableData(PFL.EHForms);*/
              EHD.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
              hlpbtn.setEnabled(true);
              PFL.CheckEnableButton();
			  DelArchivebtn.setEnabled(true);
           }
           else if( Source.equals(Delbtn)){
              int result= JOptionPane.showConfirmDialog(PFL,AIPUtil.getString("History.Msg.DelEHFile"),AIPUtil.getString("Init.Msg.Title"),JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
              if(result == JOptionPane.OK_OPTION){
                 PFL.DeleteSelectedRows();
                 Delbtn.setEnabled(false);
				  DelArchivebtn.setEnabled(false);
                 PFL.CheckEnableButton();
              }
           }
           else  if( Source.equals(hlpbtn) ){
               try
               {
                   String helpFilePath = System.getProperty("user.dir") + File.separator + ".." + File.separator + "Help" + File.separator + "VWAIP.chm";
                   File helpFile = new File(helpFilePath);
                   if (helpFile.canRead())
                   {
                       //Runtime.getRuntime().exec("winhlp32.exe " + helpFilePath);
                       java.lang.Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + helpFilePath);
                       //send the window to the back so help files appear on top of it.
                       //EHD.frame.toBack();
                   }
                   else
                    JOptionPane.showMessageDialog(null, AIPUtil.getString("General.Msg.NoHelpFile"), AIPUtil.getString("Init.Msg.Title"), JOptionPane.ERROR_MESSAGE);    
               }
               catch (Exception he)
               {
                   JOptionPane.showMessageDialog(null, AIPUtil.getString("General.Msg.NoHelpFile"), AIPUtil.getString("Init.Msg.Title"), JOptionPane.ERROR_MESSAGE);
                  // if(Log.LOG_ERROR)Log.error("Error in launching help file: ", he);
               }
           }
		  else  if( Source.equals(DelArchivebtn) ){
			  try
			  {
				  VWLogger.info("Deleting the Archived files from the History files!");
				  AIPManager.getVWC().deleteAIPHistoryArchivedFiles(AIPManager.getCurrSid());
           
        }
			  catch (Exception he)
			  {
				  VWLogger.error("Error while removing archived files", he);
			  }
		  }
	  }
  }

}