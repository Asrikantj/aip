package com.computhink.aip.ui;

import com.computhink.aip.*;
import com.computhink.aip.resource.ResourceManager;
import com.computhink.aip.util.AIPUtil;
import com.computhink.aip.util.VWLogger;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;


import java.io.*;
/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public class EHPendingPanel extends JPanel {
  ProcessPendingFileLayout PFL;
  JButton Procssbtn;  
  JButton Delbtn;
  private JButton hlpbtn;
  EHMainPanel EHD;
  
  // Code added for EAS Enhance History Tab - Start
  public static int iStartIndex;
  public static int iEndIndex;
  public static int iTotalCount;
  public static int iMaximumRows;
  // Code added for EAS Enhance History Tab - End  
  private  String languageLocale = ResourceManager.getDefaultManager().getLocale().toString(); 
  ButtonListener btnListener =new ButtonListener();
  ReProcessThread reProcess = null; 
      
  public EHPendingPanel(EHMainPanel EHD1) {
     EHD= EHD1;
     PFL =new ProcessPendingFileLayout(this);
     setBorder(new EmptyBorder(5,5,5,5));
     JPanel Panel1 =new JPanel();
     this.add(Panel1,BorderLayout.CENTER);
     Panel1.setMinimumSize(new Dimension(200,100));
     // Panel1.setPreferredSize(new Dimension(550,350));
     //Panel1.setPreferredSize(new Dimension(655,500));
     Panel1.setPreferredSize(new Dimension(685,520));
     Panel1.setLayout(new BoxLayout(Panel1,BoxLayout.Y_AXIS));
     Panel1.setBorder(new CompoundBorder(new TitledBorder(null,null,
	 TitledBorder.LEFT, TitledBorder.TOP),new EmptyBorder(5,8,5,5)));
     Panel1.add(PFL);
     Panel1.add(Box.createVerticalStrut(20));
     Panel1.add(CreateButtonPanel());

  }
  public JPanel CreateButtonPanel(){
    JPanel BtnPanel =new JPanel();
    BtnPanel.setLayout(new BoxLayout(BtnPanel, BoxLayout.X_AXIS));
    
    Procssbtn  =new JButton(AIPUtil.getString("History.Btn.Process"));
    Delbtn     =new JButton(AIPUtil.getString("History.Btn.Delete"));
    hlpbtn     =new JButton(AIPUtil.getString("History.Btn.Help"));

    hlpbtn.setMnemonic(AIPUtil.getString("History.MBtn.Help").charAt(0));
    Delbtn.setMnemonic(AIPUtil.getString("History.MBtn.Delete").charAt(0));
    Procssbtn.setMnemonic(AIPUtil.getString("History.MBtn.Process").charAt(0));

    hlpbtn.addActionListener(btnListener);
    Delbtn.addActionListener(btnListener);
    Procssbtn.addActionListener(btnListener);
    
    if(languageLocale.equals("nl_NL")){
    	 hlpbtn.setPreferredSize(new Dimension(110,25)); 	
    }else
    hlpbtn.setPreferredSize(new Dimension(85,25));
    hlpbtn.setMinimumSize(new Dimension(85,25));
    if(languageLocale.equals("nl_NL")){
    	Delbtn.setPreferredSize(new Dimension(115,25));
    }else
    Delbtn.setPreferredSize(new Dimension(85,25));
    Delbtn.setMinimumSize(new Dimension(85,25));
    if(languageLocale.equals("nl_NL")){
    	Procssbtn.setPreferredSize(new Dimension(115,25));
    }else
    Procssbtn.setPreferredSize(new Dimension(85,25));
    Procssbtn.setMinimumSize(new Dimension(85,25));

    hlpbtn.setEnabled(true);
    Delbtn.setEnabled(false);
    Procssbtn.setEnabled(false);

    BtnPanel.add(Box.createHorizontalGlue());
    BtnPanel.add(Procssbtn);
    BtnPanel.add(Delbtn);
    BtnPanel.add(hlpbtn);

    BtnPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
    PagesManager.nConvertToDJVU = AIPManager.isConvertToDJVU();
    return BtnPanel;

  }
  public void setAllEnabled(boolean enable){
      if(!enable){
      Procssbtn.setEnabled(enable);
      Delbtn.setEnabled(enable);
      PFL.table.clearSelection();
      }
      else PFL.CheckEnableButton();
      hlpbtn.setEnabled(enable);
      PFL.setAllEnabled(enable);
  }
  public void RemoveAllPFLRows(){
      PFL.TableModel.RemoveAllRows();
      Delbtn.setEnabled(false);
      Procssbtn.setEnabled(false);
  }
  
  /**CV10.2 - Added for restarting the pending process if it is hanging. Added by Vanitha S**/
  public void restartPendingProcess() {
	  VWLogger.info("Pending process restarted........");	  
	  Procssbtn.setEnabled(true);	  
	  Procssbtn.doClick();
  }
  
//----------------------------------Actions & Listeners----------------------
  class ButtonListener implements ActionListener {
        public synchronized void actionPerformed(ActionEvent e) {
        JButton Source = (JButton)e.getSource();
           if(Source.equals(Procssbtn)){   
        	  ProcessPendingFileLayout.isPendingProcessStopped = false;
        	  VWLogger.info("inside process Button onclick ....");	  
        	  if(!EHD.TempPane.MDT.CheckUnSaveBeforeChange(AIPUtil.getString("General.Msg.SaveTemplete2")))  return;
              EHD.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
              hlpbtn.setEnabled(false);
              Delbtn.setEnabled(false);
              Procssbtn.setEnabled(false);             
              //EyesHandsManager.getInitParams(AIPManager.getCurrSid()).setDefaultRoomNodeId(EHD.GenPane.VWNodes.getSelectedNodeid());
              
        	  VWLogger.info("reProcess........"+reProcess);	  
        	  if (reProcess != null) {
            	  reProcess.stopRunning();
              }
        	  /**CV10.2 - ProcessMonitor Thread has initiated to monitor the document process. Added by Vanitha S**/
        	  PFL.createPendingProcessMonitoringThread();
        	  VWLogger.info("Before creating new thread........");	
        	  reProcess = new ReProcessThread(false);
              reProcess.start();        
           }
           else if( Source.equals(Delbtn)){
              int result= JOptionPane.showConfirmDialog(PFL,AIPUtil.getString("Pending.Msg.DelEHFile"),AIPUtil.getString("Init.Msg.Title"),JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
              if(result == JOptionPane.OK_OPTION){
                 PFL.DeleteSelectedRows();
                 Delbtn.setEnabled(false);
                 PFL.CheckEnableButton();
              }
           }
           else  if( Source.equals(hlpbtn) ){
               try
               {
                   String helpFilePath = System.getProperty("user.dir") + File.separator + ".." + File.separator + "Help" + File.separator + "VWAIP.chm";
                   File helpFile = new File(helpFilePath);
                   if (helpFile.canRead())
                   {
                       //Runtime.getRuntime().exec("winhlp32.exe " + helpFilePath);
                       java.lang.Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + helpFilePath);
                       //send the window to the back so help files appear on top of it.
                       //EHD.frame.toBack();
                   }
                   else
                    JOptionPane.showMessageDialog(null, AIPUtil.getString("General.Msg.NoHelpFile"), AIPUtil.getString("Init.Msg.Title"), JOptionPane.ERROR_MESSAGE);    
               }
               catch (Exception he)
               {
                   JOptionPane.showMessageDialog(null, AIPUtil.getString("General.Msg.NoHelpFile"), AIPUtil.getString("Init.Msg.Title"), JOptionPane.ERROR_MESSAGE);
                  // if(Log.LOG_ERROR)Log.error("Error in launching help file: ", he);
               }
           }
           
        }
  }

  public void stopPendingProcess() {
	  PFL.stopPendingProcess();
  }
  
  /**
   * CV10.2 - Added to check whether current thread is interrupted. If it is interrupted then new process will initiate and it will re-process the pending documents.
   * Added by Vanitha S
  **/
  public class ReProcessThread extends Thread {
	private boolean isHanging = false;

	protected ReProcessThread(boolean flag) {
		isHanging = flag;
	}

	public void stopRunning() {
		VWLogger.info("Inside RePorcess thread StopRunning..........");
		try {
			isHanging = true;
			if (reProcess != null) {
				VWLogger.info("Before calling thread interrupt ...");
				reProcess.interrupt();
				VWLogger.info("check the thread "+reProcess.getName()+" isAlive :"+reProcess.isAlive()+" isinterrupted :"+reProcess.isInterrupted());
			}
		} catch (Exception e) {
			VWLogger.info("Exception while stopping the current thread......."+e.getMessage());	
        }			
	}

	public void run() {
		VWLogger.info("** run inside Pending Process Thread....");
		VWLogger.info("isHanging........" + isHanging);
		//while (!isHanging) {
			try {
				processSelectedDocuments();								
			} catch (Exception e) {
				VWLogger.info("Exception occur :"+reProcess.getName());
			}
		//}	
		VWLogger.info("Pending Process Thread run method ended.......");
	}
	
	public synchronized void processSelectedDocuments() {
		VWLogger.info("Pending process started.......");
		try {
			EHD.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));	
			EHD.TabPane.setEnabledAt(0, false);
			EHD.TabPane.setEnabledAt(2, false);
			EHD.TabPane.setEnabledAt(3, false);
			
			Procssbtn.setEnabled(false);
			Delbtn.setEnabled(false);            	
			hlpbtn.setEnabled(false); 
			
			PFL.ProcessSelectedRows();
			VWLogger.info("After ProcessSelectedRows call.....");
			VWLogger.info("Thread.currentThread().isInterrupted()......"+Thread.currentThread().isInterrupted());
			if (!Thread.currentThread().isInterrupted()) {
				ProcessPendingFileLayout.isPendingProcessStopped = true;
				EHD.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				EHD.TabPane.setEnabledAt(0, true);
				EHD.TabPane.setEnabledAt(2, true);
				EHD.TabPane.setEnabledAt(3, true);
				
				
				Procssbtn.setEnabled(true);
				Delbtn.setEnabled(true);            	
				hlpbtn.setEnabled(true);
				  
				// Issue 544   
				hlpbtn.setEnabled(true);
				PFL.CheckEnableButton();			
				PFL.stopPendingProcess();
				PFL.stopProcessMonitoringThread();
				stopRunning();	
				VWLogger.info("Pending process ended.......");	
			}	
		} catch (Exception e) {
			VWLogger.info("Exception in processSelectedDocuments method :"+e.getMessage());
		}
	}
}
}