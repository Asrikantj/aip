/*
 * DestVWNodeDialog.java
 *
 * Created on August 4, 2004, 4:05 PM
 */

package com.computhink.aip.ui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.tree.*;

import com.computhink.aip.resource.ResourceManager;
import com.computhink.aip.resource.images.ImageManager;
import com.computhink.common.Constants;
/**
 *
 * @author  Ayman
 */
public class DestVWNodeDialog extends javax.swing.JDialog { 
	private final ResourceManager connectorManager = ResourceManager.getDefaultManager();
    private JTree tree ;
    private DefaultMutableTreeNode MainNode;
    private DefaultTreeModel treeModel ;
    private TreeSelectionModel TreeSModel ;
    public DestVWNodeDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        init();
        initComponents();
        
    }
   
    private void initComponents() {//GEN-BEGIN:initComponents
        
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });
        
        pack();
    }//GEN-END:initComponents
    private void init()
    {
        this.setSize(300,420);
        setResizable(false);
        
        JPanel pane1 =new JPanel();
        pane1.setLayout(new BoxLayout(pane1,BoxLayout.Y_AXIS));
        //pane1.setBorder(new CompoundBorder(new TitledBorder(null,null,
	//TitledBorder.LEFT, TitledBorder.TOP),new EmptyBorder(1,1,1,1)));
        //pane1.setMaximumSize(new Dimension(290,350));
        //pane1.setPreferredSize(new Dimension(290,350));
        pane1.add(createTreePanel());
        pane1.add(Box.createVerticalStrut(10));
        pane1.add(createNodePathPanel());
        
        
        JPanel mainPane =new JPanel();
        mainPane.setBorder(new EmptyBorder(5,5,5,5));
        
        mainPane.setLayout(new BoxLayout(mainPane,BoxLayout.Y_AXIS)); 
        mainPane.add(pane1);
        mainPane.add(Box.createVerticalStrut(10));
        mainPane.add(createBtnPane());
        mainPane.add(Box.createVerticalStrut(5));
        
        this.getContentPane().add(mainPane);
    }    
    private JPanel createTreePanel()    
    {
        JPanel treePane =new JPanel();
        treePane.setBorder(new CompoundBorder(new TitledBorder(null,Constants.PRODUCT_NAME +" "+connectorManager.getString("DestVWNodeDialog.TitleNodes"),
	TitledBorder.LEFT, TitledBorder.TOP),new EmptyBorder(1,1,3,1)));
        treePane.setLayout(new BorderLayout());
        //treePane.setMaximumSize(new Dimension(290,300));
        //treePane.setPreferredSize(new Dimension(290,300));
            treeModel = new DefaultTreeModel(MainNode);
            tree = new JTree(treeModel) {
	          public Insets getInsets() {
		    return new Insets(5,5,5,5);
	           }
	    };
            tree.getSelectionModel().setSelectionMode
                (TreeSelectionModel.SINGLE_TREE_SELECTION);
            TreeSModel = tree.getSelectionModel();
           /* tree.addTreeSelectionListener(new TreeSelectionListener() {
                public void valueChanged(TreeSelectionEvent e) {
                    DefaultMutableTreeNode node = (DefaultMutableTreeNode)
                             tree.getLastSelectedPathComponent();
                    int  NodeId =0;
                    if(node != null && node != MainNode){
                      SNodeLevel=node.getLevel();
                      if( node.getUserObject() instanceof VWNodeInfo){
                          NodeId = ((VWNodeInfo)node.getUserObject()).getNodeId();

                      }
                    }
                    if(SNodeId != NodeId ){
                       SNodeId =NodeId;
                    }

                }
            });*/
            tree.setScrollsOnExpand(true);
            tree.setCellRenderer(new IconRenderer());
            tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
            tree.setEditable(false);
            //tree.putClientProperty("JTree.lineStyle", "Angled");
            tree.setRowHeight(16);

            JScrollPane SPane =new JScrollPane(tree);
            treePane.add(SPane);
            //SPane.setAlignmentX(Component.LEFT_ALIGNMENT);
	    return treePane;
        
    } 
    private JPanel createNodePathPanel()
    {
        JPanel pathPane =new JPanel();
        pathPane.setBorder(new CompoundBorder(new TitledBorder(null,Constants.PRODUCT_NAME +" "+connectorManager.getString("DestVWNodeDialog.NodePath"),
	TitledBorder.LEFT, TitledBorder.TOP),new EmptyBorder(1,3,5,3)));
        pathPane.setLayout(new BoxLayout(pathPane,BoxLayout.X_AXIS));
        //pathPane.setMaximumSize(new Dimension(295,50));
        //pathPane.setPreferredSize(new Dimension(295,50));
        
        //JLabel nodePathLab =new JLabel("Node Path");
        //nodePathLab.setMaximumSize(new Dimension(65,23));
        //nodePathLab.setPreferredSize(new Dimension(65,23));
        JTextField nodePathField =new JTextField();
        nodePathField.setMaximumSize(new Dimension(270,23));
        nodePathField.setPreferredSize(new Dimension(270,23));
        JButton custmBtn =new JButton(connectorManager.getString("DestVWNodeDialog.Customize"));
        custmBtn.setMaximumSize(new Dimension(85,23));
        custmBtn.setPreferredSize(new Dimension(85,23));
        
       // pathPane.add(nodePathLab);
        //pathPane.add(Box.createHorizontalStrut(5));
        pathPane.add(nodePathField);
        pathPane.add(Box.createHorizontalStrut(2));
        pathPane.add(custmBtn);
        return pathPane;
    }
    private JPanel createBtnPane()
    {
        JPanel btnPane =new JPanel();
        btnPane.setLayout(new BoxLayout(btnPane,BoxLayout.X_AXIS));
        JButton saveBtn =new JButton(connectorManager.getString("DestVWNodeDialog.Save"));
        saveBtn.setMaximumSize(new Dimension(75,25));
        saveBtn.setPreferredSize(new Dimension(75,25));
        JButton closeBtn =new JButton(connectorManager.getString("DestVWNodeDialog.Close"));
        closeBtn.setMaximumSize(new Dimension(75,25));
        closeBtn.setPreferredSize(new Dimension(75,25));
        btnPane.add(Box.createHorizontalGlue());
        btnPane.add(saveBtn);
        btnPane.add(closeBtn);
        return btnPane;
    }    
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        setVisible(false);
        dispose();
    }//GEN-LAST:event_closeDialog
    class IconRenderer extends DefaultTreeCellRenderer {

        ImageManager IM =new ImageManager();
        public IconRenderer() {

        }
        public Component getTreeCellRendererComponent(
                            JTree tree,Object value,boolean sel,boolean expanded,boolean leaf,
                            int row, boolean hasFocus) {

            super.getTreeCellRendererComponent(
                        tree, value, sel,expanded, leaf, row,hasFocus);
            int level =((DefaultMutableTreeNode)value).getLevel();
          //  if(sel) SNodeLevel = level;
            if(level == 0) setIcon(IM.getImageIcon("roomicon.png"));
            else if(level == 1){
              if(expanded) setIcon(IM.getImageIcon("ecabinet.png"));
              else setIcon(IM.getImageIcon("cabinet.png"));
            }else if(level == 2){
              if(expanded) setIcon(IM.getImageIcon("edrawer.png"));
              else setIcon(IM.getImageIcon("drawer.png"));
            }else if(level > 2) setIcon(IM.getImageIcon("folder.png"));


           return this;
       }

  }
    
    public static void main(String args[]) {
        try{
            String windows  = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
             UIManager.setLookAndFeel(windows);
             //UIManager.getSystemLookAndFeelClassName() ;
        } catch (Exception e) { }
        /*
      	 * new JDialog().show(); method is replaced with new JDialog().setVisible(true); 
      	 * as show() method is deprecated  //Gurumurthy.T.S 19/12/2013,CV8B5-001
      	 */
        new DestVWNodeDialog(new javax.swing.JFrame(), true).setVisible(true);
    }
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
    
}
