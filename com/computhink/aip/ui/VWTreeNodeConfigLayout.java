/*
 * VWTreeNodeConfigPane.java
 *
 * Created on January 6, 2004, 3:04 PM
 */

package com.computhink.aip.ui;

import com.computhink.aip.resource.ResourceManager;
import com.computhink.aip.resource.images.ImageManager;
import com.computhink.aip.util.*;
import com.computhink.common.Constants;

import javax.swing.*;
import java.awt.*;
import java.util.*;

/**
 * This panel lets the user specify a ViewWise Tree Node dynamically using
 * explicit values or values taken from existing indices, doctypes, day or time.
 * @author  amaleh
 */
public class VWTreeNodeConfigLayout extends javax.swing.JPanel {
	private final ResourceManager connectorManager = ResourceManager.getDefaultManager();
    /** a ChangeObservable that notifies observers of changes */
    private Observable observable = new ChangeObservable();
    /** an array that holds the values of indices */
    private String[] roomValues;
    private String[] cabinetValues;
    private String[] possibleValues;
    private String[] currentPathValues;
    private ImageManager IM =new ImageManager();
    
    /** Creates new form VWTreeNodeConfigPane.
     * Possible values constitute possible values for every field
     * Current values constitute the current value to be placed in each field.
     */
    public VWTreeNodeConfigLayout(String[] roomValues, String[] cabinetValues, String[] possibleValues, String[] currentPathValues) {
        this.roomValues = roomValues;
        this.cabinetValues = cabinetValues;
        this.possibleValues = possibleValues;
        this.currentPathValues = currentPathValues;
        initComponents();
        for (int i = 0; i < currentPathValues.length; i++)
        {
            switch(i)
            {
                case 0:
                    roomDD.setSelectedIndex(-1);
                    roomDD.setSelectedIndex(0);
                    break;
                case 1:
                    int cabinetIndex = 0;
                    for (int j = 0; j < cabinetValues.length; j++)
                    {
                        if (cabinetValues[j].equals(currentPathValues[i]))
                            cabinetIndex = j;
                    }
                    cabinetDD.setSelectedIndex(cabinetIndex);
                    break;
                case 2:
                    drawerDD.setSelectedItem(currentPathValues[i]);
                    break;
                case 3:
                    folderDD.setSelectedItem(currentPathValues[i]);
                    break;
                default:
                    addSubFolder(currentPathValues[i]);
            }
        }
    }

    /** returns the observable object that enables registering observers */
    public Observable getObservable()
    {
        return observable;
    }
    
    /** Returns current Pseudo-path */
    public String getPath()
    {
        String VWTreePath = "";
        String room = (String)(roomDD.getSelectedItem());
        VWTreePath += room;
        String cabinet = (String)(cabinetDD.getSelectedItem());
        VWTreePath += "/";
        VWTreePath += cabinet;
        String drawer = (String)(drawerDD.getSelectedItem());
        VWTreePath += "/";
        VWTreePath += drawer;
        String folder = (String)(folderDD.getSelectedItem());
        VWTreePath += "/";
        VWTreePath += folder;
        String[] subfolders = new String[subfoldersList.getComponentCount()];
        for (int i = 0; i < subfolders.length; i++)
        {
            JPanel panel = (JPanel)subfoldersList.getComponent(i);
            JComboBox box = (JComboBox)panel.getComponent(0);
            subfolders[i] = (String)(box.getSelectedItem());
            VWTreePath += "/";
            VWTreePath += subfolders[i];
        }
        return VWTreePath;
        
    }
    
    /*
    private String contourText(String text)
    {
        return "(" + text + ")";
    }
     */
    
    /** parses the destination VW tree node using the index values supplied */
    /*
    public String parseVWTreeNode(String doctypeValue, String[] indexValues)
    {
        String VWTreeNode = "";
        String room = parseTargetIndexValue((String)(roomDD.getSelectedItem()), indices, indexValues);
        VWTreeNode += room;
        String cabinet = parseTargetIndexValue((String)(cabinetDD.getSelectedItem()), indices, indexValues);
        VWTreeNode += "/";
        VWTreeNode += cabinet;
        String drawer = parseTargetIndexValue((String)(drawerDD.getSelectedItem()), indices, indexValues);
        VWTreeNode += "/";
        VWTreeNode += drawer;
        String folder = parseTargetIndexValue((String)(folderDD.getSelectedItem()), indices, indexValues);
        VWTreeNode += "/";
        VWTreeNode += folder;
        String[] subfolders = new String[subfoldersList.getComponentCount()];
        for (int i = 0; i < subfolders.length; i++)
        {
            JPanel panel = (JPanel)subfoldersList.getComponent(i);
            JComboBox box = (JComboBox)panel.getComponent(0);
            subfolders[i] = parseTargetIndexValue((String)(box.getSelectedItem()), indices, indexValues);
            VWTreeNode += "/";
            VWTreeNode += subfolders[i];
        }
        return VWTreeNode;
    }
     */
    
    /** parses targetted index value */
    /*
    protected String parseTargetIndexValue(String value, String[] indexNames, String[] indexValues)
    {
        /*
        if (value.charAt(0) == '(' && value.charAt(value.length() - 1) == ')')
            value = value.substring(1, value.length() - 1);
         */
    /*
        for (int i = 0; i < indexNames.length; i++)
        {
            if (value.equals(indexNames[i]))
                return indexValues[i];
        }
        return value;
    }
    */
    
    /** adds a subfolder to the subfolder list */
    public void addSubFolder(String subFolderName)
    {
        JComboBox comboBox = ToolTipComboBoxMorphingFactory.getInstance().morph(new ObservableComboBox(observable,possibleValues));
        comboBox.setEditable(true);
        Dimension size = new Dimension(71+15, 18);
        comboBox.setPreferredSize(size);
        comboBox.setMaximumSize(new Dimension(Integer.MAX_VALUE, 18));
        comboBox.setMinimumSize(size);
        comboBox.setFont(new java.awt.Font("Dialog", 0, 10));
        comboBox.setSelectedItem(subFolderName);
        
        JLabel slashLabel = new JLabel();
        slashLabel.setFont(new java.awt.Font("Dialog", 1, 18));
        slashLabel.setText(" / ");
        slashLabel.setMaximumSize(new java.awt.Dimension(15, 18));
        slashLabel.setMinimumSize(new java.awt.Dimension(15, 18));
        slashLabel.setPreferredSize(new java.awt.Dimension(15, 18));
        
        JPanel subfolderPanel = new JPanel();
        subfolderPanel.setPreferredSize(size);
        subfolderPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, 18));
        subfolderPanel.setMinimumSize(size);
        subfolderPanel.setLayout(new BorderLayout());
        subfolderPanel.add(comboBox, BorderLayout.CENTER);
        subfolderPanel.add(slashLabel, BorderLayout.EAST);
        
        subfoldersList.add(subfolderPanel);
        subfoldersList.doLayout();
        subfoldersList.revalidate();
        subfoldersList.repaint();        
    }
    
    /** main method for testing purposes only */
    /*
    public static void main(String[] args)
    {
        JFrame frame = new JFrame("ViewWise Destination Location");
        JPanel panel = new VWTreeNodeConfigLayout(new String[] {"index1", "index2", "index3", "index4", "index5"}, new String[0]);
        frame.getContentPane().add(panel);
        frame.setMaximizedBounds(new Rectangle(0, 0, 800, 600));
        frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
        frame.setSize((int)panel.getPreferredSize().getWidth() + 16,(int)panel.getPreferredSize().getHeight() + 16);
        frame.show();
    }
     */
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    private void initComponents() {//*GEN-BEGIN:initComponents
        java.awt.GridBagConstraints gridBagConstraints;

        subfoldersList = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        roomLabel = new javax.swing.JLabel();
        ToolTipComboBoxMorphingFactory factory = ToolTipComboBoxMorphingFactory.getInstance();
        roomDD = factory.morph(new ObservableComboBox(observable,roomValues));
        slashLabel = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        cabinetLabel = new javax.swing.JLabel();
        cabinetDD = factory.morph(new ObservableComboBox(observable,cabinetValues));
        jLabel3 = new javax.swing.JLabel();
        jPanel12 = new javax.swing.JPanel();
        drawerLabel = new javax.swing.JLabel();
        drawerDD = factory.morph(new ObservableComboBox(observable,possibleValues));
        jLabel9 = new javax.swing.JLabel();
        jPanel14 = new javax.swing.JPanel();
        folderLabel = new javax.swing.JLabel();
        folderDD = factory.morph(new ObservableComboBox(observable,possibleValues));
        jLabel11 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        subfoldersLabel = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jScrollPane1 = new JScrollPane(subfoldersList);

        subfoldersList.setLayout(new javax.swing.BoxLayout(subfoldersList, javax.swing.BoxLayout.Y_AXIS));

        jPanel1.setLayout(new java.awt.BorderLayout());

        jPanel1.setMaximumSize(new java.awt.Dimension(32767, 16));
        jPanel1.setMinimumSize(new java.awt.Dimension(490, 16));
        jPanel1.setPreferredSize(new java.awt.Dimension(490, 16));
        jLabel1.setText(Constants.PRODUCT_NAME +" "+ connectorManager.getString("VWTreeNodeConfigLayout.TreeNode"));
        jLabel1.setBorder(new javax.swing.border.EmptyBorder(new java.awt.Insets(0, 2, 0, 0)));
        jLabel1.setMinimumSize(new java.awt.Dimension(640, 16));
        jLabel1.setPreferredSize(new java.awt.Dimension(640, 16));
        jPanel1.add(jLabel1, java.awt.BorderLayout.NORTH);

        setLayout(new java.awt.BorderLayout());

        setMaximumSize(new java.awt.Dimension(505, 128));
        setMinimumSize(new java.awt.Dimension(505, 128));
        //setPreferredSize(new java.awt.Dimension(560, 192));
        setPreferredSize(new java.awt.Dimension(640, 192));
        jPanel3.setLayout(new java.awt.GridBagLayout());

        jPanel3.setBorder(new javax.swing.border.EmptyBorder(new java.awt.Insets(1, 6, 1, 6)));
        jPanel3.setMaximumSize(new java.awt.Dimension(32767, 128));
        jPanel3.setMinimumSize(new java.awt.Dimension(490, 128));
        jPanel3.setPreferredSize(new java.awt.Dimension(490, 64));
        jPanel7.setLayout(new java.awt.BorderLayout());

        jPanel7.setMinimumSize(new java.awt.Dimension(85, 34));
        jPanel7.setPreferredSize(new java.awt.Dimension(85, 34));
        roomLabel.setIcon(IM.getImageIcon("roomicon.png"));
        roomLabel.setText("Room");
        roomLabel.setPreferredSize(new java.awt.Dimension(70, 16));
        jPanel7.add(roomLabel, java.awt.BorderLayout.NORTH);

        roomDD.setFont(new java.awt.Font("Dialog", 0, 10));
        jPanel7.add(roomDD, java.awt.BorderLayout.CENTER);

        slashLabel.setFont(new java.awt.Font("Dialog", 1, 18));
        slashLabel.setText(" / ");
        slashLabel.setMaximumSize(new java.awt.Dimension(15, 18));
        slashLabel.setMinimumSize(new java.awt.Dimension(15, 18));
        slashLabel.setPreferredSize(new java.awt.Dimension(15, 18));
        jPanel7.add(slashLabel, java.awt.BorderLayout.EAST);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.weightx = 1.0;
        jPanel3.add(jPanel7, gridBagConstraints);

        jPanel9.setLayout(new java.awt.BorderLayout());

        jPanel9.setMinimumSize(new java.awt.Dimension(85, 34));
        jPanel9.setPreferredSize(new java.awt.Dimension(85, 34));
        cabinetLabel.setIcon(IM.getImageIcon("cabinet.png"));
        cabinetLabel.setText(connectorManager.getString("VWTreeNodeConfigLayout.Cabinet"));
        cabinetLabel.setPreferredSize(new java.awt.Dimension(70, 16));
        jPanel9.add(cabinetLabel, java.awt.BorderLayout.NORTH);

        cabinetDD.setFont(new java.awt.Font("Dialog", 0, 10));
        jPanel9.add(cabinetDD, java.awt.BorderLayout.CENTER);

        jLabel3.setFont(new java.awt.Font("Dialog", 1, 18));
        jLabel3.setText(" / ");
        jLabel3.setMaximumSize(new java.awt.Dimension(15, 18));
        jLabel3.setMinimumSize(new java.awt.Dimension(15, 18));
        jLabel3.setPreferredSize(new java.awt.Dimension(15, 18));
        jPanel9.add(jLabel3, java.awt.BorderLayout.EAST);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(16, 0, 0, 0);
        jPanel3.add(jPanel9, gridBagConstraints);

        jPanel12.setLayout(new java.awt.BorderLayout());

        jPanel12.setMinimumSize(new java.awt.Dimension(85, 34));
        jPanel12.setPreferredSize(new java.awt.Dimension(85, 34));
        drawerLabel.setIcon(IM.getImageIcon("drawer.png"));
        drawerLabel.setText(connectorManager.getString("VWTreeNodeConfigLayout.Drawer"));
        drawerLabel.setPreferredSize(new java.awt.Dimension(70, 16));
        jPanel12.add(drawerLabel, java.awt.BorderLayout.NORTH);

        drawerDD.setEditable(true);
        drawerDD.setFont(new java.awt.Font("Dialog", 0, 10));
        drawerDD.setSelectedIndex(-1);
        jPanel12.add(drawerDD, java.awt.BorderLayout.CENTER);

        jLabel9.setFont(new java.awt.Font("Dialog", 1, 18));
        jLabel9.setText(" / ");
        jLabel9.setMaximumSize(new java.awt.Dimension(15, 18));
        jLabel9.setMinimumSize(new java.awt.Dimension(15, 18));
        jLabel9.setPreferredSize(new java.awt.Dimension(15, 18));
        jPanel12.add(jLabel9, java.awt.BorderLayout.EAST);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(32, 0, 0, 0);
        jPanel3.add(jPanel12, gridBagConstraints);

        jPanel14.setLayout(new java.awt.BorderLayout());

        jPanel14.setMinimumSize(new java.awt.Dimension(85, 34));
        jPanel14.setPreferredSize(new java.awt.Dimension(85, 34));
        folderLabel.setIcon(IM.getImageIcon("folder.png"));
        folderLabel.setText(connectorManager.getString("VWTreeNodeConfigLayout.Folder"));
        folderLabel.setPreferredSize(new java.awt.Dimension(70, 16));
        jPanel14.add(folderLabel, java.awt.BorderLayout.NORTH);

        folderDD.setEditable(true);
        folderDD.setFont(new java.awt.Font("Dialog", 0, 10));
        folderDD.setSelectedIndex(-1);
        jPanel14.add(folderDD, java.awt.BorderLayout.CENTER);

        jLabel11.setFont(new java.awt.Font("Dialog", 1, 18));
        jLabel11.setText(" / ");
        jLabel11.setMaximumSize(new java.awt.Dimension(15, 18));
        jLabel11.setMinimumSize(new java.awt.Dimension(15, 18));
        jLabel11.setPreferredSize(new java.awt.Dimension(15, 18));
        jPanel14.add(jLabel11, java.awt.BorderLayout.EAST);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(48, 0, 0, 0);
        jPanel3.add(jPanel14, gridBagConstraints);

        jPanel4.setLayout(new java.awt.BorderLayout());

        jPanel4.setPreferredSize(new java.awt.Dimension(123, 90));
        subfoldersLabel.setIcon(IM.getImageIcon("folder.png"));
        subfoldersLabel.setText(connectorManager.getString("VWTreeNodeConfigLayout.Subfolder(s)"));
        jPanel4.add(subfoldersLabel, java.awt.BorderLayout.NORTH);

        jPanel5.setPreferredSize(new java.awt.Dimension(20, 26));
        jButton1.setFont(new java.awt.Font("Dialog", 1, 17));
        jButton1.setText("+");
        jButton1.setAlignmentY(0.0F);
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton1.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButton1.setPreferredSize(new java.awt.Dimension(16, 16));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jPanel5.add(jButton1);

        jButton2.setFont(new java.awt.Font("Dialog", 0, 24));
        jButton2.setText("-");
        jButton2.setAlignmentY(0.0F);
        jButton2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton2.setMargin(new java.awt.Insets(0, 0, 5, 0));
        jButton2.setPreferredSize(new java.awt.Dimension(16, 16));
        jButton2.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jPanel5.add(jButton2);

        jButton3.setText("...");
        jButton3.setPreferredSize(new java.awt.Dimension(16, 16));
        jButton3.setVisible(false);
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jPanel5.add(jButton3);

        jPanel4.add(jPanel5, java.awt.BorderLayout.EAST);

        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane1.setVerticalScrollBarPolicy(javax.swing.JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        jPanel4.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(64, 0, 0, 0);
        jPanel3.add(jPanel4, gridBagConstraints);

        add(jPanel3, java.awt.BorderLayout.CENTER);

    }//*GEN-END:initComponents

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//*GEN-FIRST:event_jButton3ActionPerformed
        // Add your handling code here:
        JOptionPane.showMessageDialog(null, getPath());
    }//*GEN-LAST:event_jButton3ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//*GEN-FIRST:event_jButton2ActionPerformed
        // Add your handling code here:
        int compCount = subfoldersList.getComponentCount();
        if (compCount > 0)
        {
            subfoldersList.remove(compCount - 1);
            observable.notifyObservers();
        }
        subfoldersList.doLayout();
        subfoldersList.revalidate();
        subfoldersList.repaint();
    }//*GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//*GEN-FIRST:event_jButton1ActionPerformed
        // Add your handling code here:
        addSubFolder("");
        observable.notifyObservers();
    }//*GEN-LAST:event_jButton1ActionPerformed
    
    
    // Variables declaration - do not modify//*GEN-BEGIN:variables
    private javax.swing.JComboBox cabinetDD;
    private javax.swing.JLabel cabinetLabel;
    private javax.swing.JComboBox drawerDD;
    private javax.swing.JLabel drawerLabel;
    private javax.swing.JComboBox folderDD;
    private javax.swing.JLabel folderLabel;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JComboBox roomDD;
    private javax.swing.JLabel roomLabel;
    private javax.swing.JLabel slashLabel;
    private javax.swing.JLabel subfoldersLabel;
    private javax.swing.JPanel subfoldersList;
    // End of variables declaration//*GEN-END:variables

}
