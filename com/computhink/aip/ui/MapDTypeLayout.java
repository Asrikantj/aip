package com.computhink.aip.ui;

import com.computhink.aip.*;
import com.computhink.aip.resource.ResourceManager;
import com.computhink.aip.resource.images.ImageManager;
import com.computhink.aip.util.AIPUtil;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.event.*;
import java.util.Vector;
import java.util.TreeMap;
import java.util.Iterator;
import javax.swing.border.*;

import com.computhink.common.DocType;
import com.computhink.aip.client.*;
import com.computhink.aip.util.TreeCombo;
/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public class MapDTypeLayout extends JPanel {
  EHTemplatePanel tempPane;
  private Vector Tabrows;
  private String[] columnNames;
  public JTable DTtable;
  public boolean isTableUnsave =false;
  SpecialTableModel TableModel;
 // DoctypesManager DTManager =new DoctypesManager() ;
  EyesHandsManager EHManager =new EyesHandsManager();
  Vector MapDoctypes;
  Vector NodesNavigator;
  int SelectedRow;
  private boolean isAllCellEditable =true;

  private final ResourceManager connectorManager = ResourceManager.getDefaultManager();
  private final int BTNCOL       =0 ;  private final int EHFORMCOL     =1 ;
  private final int RDOCTYPCOL   =2 ;  private final int DNODECOL      =3 ;
  private final int EHFORMIDCOL  =4 ;  private final int RDTIDCOL      =5 ;
  private final int RNODEIDCOL   =6 ;  private final int FSTATUS       =7 ;
  private final int  FEXSITS     =8 ;  public final int STATE          =9 ;
  private final int EHFFIELDSCOL =10 ; private final int RNODEPATHCOL   =11 ;
  private final int MAPDTFORMFIELDSCOL = 12; private final int COLNUMS      =13 ;
  
  public MapDTypeLayout(EHTemplatePanel tempPane1) {
    tempPane =tempPane1;
    setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
    setAlignmentX(Component.LEFT_ALIGNMENT);
    JLabel MapLab= new JLabel(AIPUtil.getString("Template.Lab.MappedForms"));
    MapLab.setFont(new Font(MapLab.getFont().getName(),Font.BOLD,MapLab.getFont().getSize()));
    MapLab.setAlignmentX(Component.LEFT_ALIGNMENT);
    add(MapLab);
    add(Box.createVerticalStrut(5));
    add(createDTMapTablePanel());
  }
  public JScrollPane createDTMapTablePanel(){
        Tabrows =new Vector();
        TableModel  = new SpecialTableModel();
        DTtable = new JTable(TableModel);
        DTtable.setRowHeight(20);
        DTtable.getTableHeader().setReorderingAllowed(false);
        DTtable.getTableHeader().setResizingAllowed(true);
        DTtable.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
        //TableColumn Rcolumn =
       // DTtable.getTableHeader().setResizingColumn();
        DTtable.getTableHeader().setMinimumSize(new Dimension(30,21));
        DTtable.getTableHeader().setPreferredSize(new Dimension(100,21));
        DTtable.getTableHeader().setAlignmentX(LEFT_ALIGNMENT) ;
        DTtable.setAlignmentX(LEFT_ALIGNMENT) ;
        JLabel lblHeader=(JLabel)DTtable.getTableHeader().getDefaultRenderer();
		lblHeader.setHorizontalAlignment(SwingConstants.CENTER);
        TableMouseListener MListener =new TableMouseListener();
        this.addMouseListener(MListener);
        DTtable.addMouseListener(MListener);
        DTtable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        ListSelectionModel SelectionModel= DTtable.getSelectionModel();
        SelectionModel.addListSelectionListener(new ListSelectionListener() {
              public void valueChanged(ListSelectionEvent e) {
                    DTTableSelectionChanged(e);
              }
        });
        JScrollPane tableScroller = new JScrollPane(DTtable);
        RowRenderer Render =new RowRenderer();
          for(int i=0;i<DTtable.getColumnCount();i++){
           TableColumn column = DTtable.getColumnModel().getColumn(i);

           if (i==BTNCOL){
               column.setPreferredWidth(18);
               column.setMaxWidth(18);
               column.setCellRenderer(new ButtonRenderer());
               column.setCellEditor(new ButtonEditor());
           }else if (i == EHFORMCOL){
               column.setPreferredWidth(125);
               column.setMaxWidth(475);
               column.setCellRenderer(Render);
           }else if (i == RDOCTYPCOL){
               column.setPreferredWidth(125);
               column.setMaxWidth(475);
               column.setCellRenderer(Render);
           }else if (i == DNODECOL){
               column.setPreferredWidth(125);
               column.setMaxWidth(500);
               column.setCellRenderer(Render);
           }
        }
        tableScroller.setAlignmentX(Component.LEFT_ALIGNMENT);
        return tableScroller;
  }

  public void setMapDoctypes(Vector MapDTs){
      MapDoctypes =MapDTs;
  }
  public Vector getMapDoctypes(){
        return  MapDoctypes;
  }
  public void setNodesNavigator(Vector Nodes){
      NodesNavigator = Nodes;
  }
  public Vector getDTIndices(int Dtid){
      for(int i=0;i<MapDoctypes.size();i++){
          DocType DT=(DocType)MapDoctypes.get(i);
          if(Dtid ==DT.getId()){
                return DT.getIndices();
          }
      }
      return new Vector();
  }
  public DocType getDocType(int dtId)
  {
     for(int i=0;i<MapDoctypes.size();i++){
          DocType DT=(DocType)MapDoctypes.get(i);
          if(dtId ==DT.getId()){
                return DT;
          }
      } 
      return null;
  }    
  public String getNodeName(int NodeId){       
       if(NodeId <= 0) return AIPUtil.getString("Template.Val.CustomVWNode");
       if(NodesNavigator.size() > 0 ){
           DefaultMutableTreeNode node =DocumentsManager.getNode(NodeId,NodesNavigator);
           if(node !=null)
               return node.toString();       
       }
       return AIPUtil.getString("Template.Val.MissingVWNode");
  }
  public void LoadTableData(Vector TData){
    if(TData !=null){
       if(TData.size() >0){
         for(int i=0;i<TData.size();i++){
             Tabrows.add(getTableRowOfInfo((AIPForm)TData.get(i)));
             TableModel.fireTableRowsInserted(Tabrows.size()-1,Tabrows.size()-1);
         }
      }
    }
 }
 public boolean CheckUnSaveBeforeChange(String Message){
     if(IsUnSaved()){
           int result = JOptionPane.showConfirmDialog(tempPane.EHD,Message,AIPUtil.getString("Init.Msg.Title"),JOptionPane.YES_NO_CANCEL_OPTION);
           if(result == JOptionPane.CANCEL_OPTION) return false;
           if(result == JOptionPane.YES_OPTION) {
                 int tab=tempPane.EHD.CurrentTab ;
                 if(tab != 1) tempPane.EHD.TabPane.setSelectedIndex(1);
                 if(saveTableForms()){
                  if(tab != 1) tempPane.EHD.TabPane.setSelectedIndex(tab);
                    return true;
                 }else{
                    return false;
                 }
            }
            if(result == JOptionPane.NO_OPTION) {
                  return true;
            }
      }
      return true;
 }
 public boolean saveTableForms(){
      boolean CompleteSave=true;
      for(int i=0;i<TableModel.getRowCount();i++){
           int State = ((Integer)TableModel.getValueAt(i,STATE)).intValue();
           if(State == AIPForm.NORMAL) continue;
           else if(State == AIPForm.FIELDSNEEDMAP){
                if(((Integer)TableModel.getValueAt(i,RDTIDCOL)).intValue() == 0){
                   // if(((Integer)TableModel.getValueAt(i,FSTATUS)).intValue() !=AIPForm.DELETED)
                    TableModel.setValueAt(new Integer(AIPForm.NORMAL),i,FSTATUS);
                    AIPForm FInfo= getAIPForm(i);                    
                    int FormId=AIPManager.getVWC().setAIPForm(AIPManager.getCurrSid(),FInfo);
                    if(FormId <= 0)
                    {   
                        javax.swing.JOptionPane.showMessageDialog(this,AIPManager.getErrorDescription(FormId)+"<"+FormId+">",AIPUtil.getString("Template.Lab.MappedForms"),javax.swing.JOptionPane.ERROR_MESSAGE);
                        return false;
                    } 
                    int ret =AIPManager.getVWC().deleteAIPFormMap(AIPManager.getCurrSid(),FInfo);
                    if(ret !=AIPManager.NOERROR)
                    {    
                        javax.swing.JOptionPane.showMessageDialog(this,AIPManager.getErrorDescription(ret)+"<"+ret+">",AIPUtil.getString("Template.Lab.MappedForms"),javax.swing.JOptionPane.ERROR_MESSAGE);
                        return false;
                    }    
                    ResetAllMapFields(FInfo);
                    //EHManager.SaveEHFormFileds(FInfo,true,AIPManager.getCurrSid());
                    TableModel.setValueAt(new Integer(AIPForm.NORMAL),i,STATE);
                    TableModel.fireTableCellUpdated(i,BTNCOL);
                }else {
                   CompleteSave =false;
                   DTtable.setRowSelectionInterval(i,i);
                   JOptionPane.showMessageDialog(tempPane.EHD,AIPUtil.getString("Template.Msg.FieldsNotMapped_pref")+" <"+TableModel.getValueAt(i,EHFORMCOL).toString()+"> "+AIPUtil.getString("Template.Msg.FieldsNotMapped_suff"),AIPUtil.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);
                }
           }else if (State == AIPForm.UPDATE  ){
                int ret =AIPManager.getVWC().setAIPForm(AIPManager.getCurrSid(),getAIPForm(i));
                if(ret <=0)
                {
                    javax.swing.JOptionPane.showMessageDialog(this,AIPManager.getErrorDescription(ret)+"<"+ret+">",AIPUtil.getString("Template.Lab.MappedForms"),javax.swing.JOptionPane.ERROR_MESSAGE);
                    return false;
                }    
                TableModel.setValueAt(new Integer(AIPForm.NORMAL),i,STATE);
                TableModel.fireTableCellUpdated(i,BTNCOL);
           }else if (State == AIPForm.FIELDSUPDATE ){
                 
                TableModel.setValueAt(new Integer(AIPForm.NORMAL),i,FSTATUS);
                AIPForm FInfo= getAIPForm(i);
                int fId =AIPManager.getVWC().setAIPForm(AIPManager.getCurrSid(),FInfo);
                if(fId <=0)
                {    
                        javax.swing.JOptionPane.showMessageDialog(this,AIPManager.getErrorDescription(fId)+"<"+fId+">",AIPUtil.getString("Template.Lab.MappedForms"),javax.swing.JOptionPane.ERROR_MESSAGE);
                        return false;
                }   
                int ret =AIPManager.getVWC().deleteAIPFormMap(AIPManager.getCurrSid(),FInfo);
                if(ret !=AIPManager.NOERROR)
                {    
                        javax.swing.JOptionPane.showMessageDialog(this,AIPManager.getErrorDescription(ret)+"<"+ret+">",AIPUtil.getString("Template.Lab.MappedForms"),javax.swing.JOptionPane.ERROR_MESSAGE);
                        return false;
                }   
               
                ret =AIPManager.getVWC().setAIPFormMap(AIPManager.getCurrSid(),FInfo);
                if(ret !=AIPManager.NOERROR)
                {    
                        javax.swing.JOptionPane.showMessageDialog(this,AIPManager.getErrorDescription(ret)+"<"+ret+">",AIPUtil.getString("Template.Lab.MappedForms"),javax.swing.JOptionPane.ERROR_MESSAGE);
                        return false;
                }  
               
                TableModel.setValueAt(new Integer(AIPForm.NORMAL),i,STATE);
                TableModel.fireTableRowsUpdated(i,i);
           }
      }
      if(CompleteSave){
        isTableUnsave =false;
        tempPane.Savebtn.setEnabled(false);
      }
      return CompleteSave;
  }

  public AIPForm getAIPForm(int rownum){
      AIPForm EHInfo =new AIPForm();
      String EHFormName= TableModel.getValueAt(rownum,EHFORMCOL).toString();
      String DocTypeName=TableModel.getValueAt(rownum,RDOCTYPCOL).toString();
      if(!EHFormName.equalsIgnoreCase("")){
          EHInfo.setName(EHFormName);
          EHInfo.setId(((Integer)TableModel.getValueAt(rownum,EHFORMIDCOL)).intValue());
          EHInfo.setMapDTId(((Integer)TableModel.getValueAt(rownum,RDTIDCOL)).intValue());
          //EHInfo.setRMapDTName(DocTypeName);
          //modified by amaleh on 2004-01-26
          //made DNodeid a variable that can be accessed later.
          int DNodeid = ((Integer)TableModel.getValueAt(rownum,RNODEIDCOL)).intValue();
          EHInfo.setDesNodeId(DNodeid);
          //added by amaleh on 2004-01-29
          //get stored tree path in table
          EHInfo.setDesNodePath((String)TableModel.getValueAt(rownum,RNODEPATHCOL));
          /*
          //if node id has the default value, then get stored tree path in table
          if (DNodeid == 0)
          {
              EHInfo.setDNodePath((String)TableModel.getValueAt(rownum,RNODEPATHCOL));
          }
          //otherwise, calculate it from node id
          else
          {
          //
          //added by amaleh on 2004-01-26
          //sets DNodePath, so the tree path string is available in AIPForm
              //calculates tree node path from id number
            DefaultMutableTreeNode node = null;
            //skip root, begin from room
            for (int i = 0; i < NodesNavigator.size(); i++)
            {
                Vector current = (Vector)NodesNavigator.get(i);
                int currentNodeID = ((Integer)(current.get(0))).intValue();
                if (currentNodeID == DNodeid)
                {
                    node = (DefaultMutableTreeNode)(current.get(1));
                    break;
                }
            }
            if (node != null)
            {
                Object[] nodePath = node.getUserObjectPath();
                StringBuffer pathBuffer = new StringBuffer();
                for (int i = 1; i < nodePath.length; i++)
                {
                    VWNodeInfo nodeInfo = (VWNodeInfo)nodePath[i];
                    if (i != nodePath.length - 1)
                        pathBuffer.append(nodeInfo.getNodeName() + "/");
                    else
                        pathBuffer.append(nodeInfo.getNodeName());
                }


              String DNodePath = pathBuffer.toString();
              EHInfo.setDNodePath(DNodePath);
           }
          //
          }
           */
          EHInfo.setStatus(((Integer)TableModel.getValueAt(rownum,FSTATUS)).intValue());
          int iExsits =((Integer)TableModel.getValueAt(rownum,FEXSITS)).intValue();
          EHInfo.setExsits(iExsits >0 ? true:false);
          EHInfo.setFields((Vector)TableModel.getValueAt(rownum,EHFFIELDSCOL));
          //EHInfo.setMapDTFormFields((Vector)TableModel.getValueAt(rownum,MAPDTFORMFIELDSCOL));
      }
      return EHInfo;
  }

  public Vector getTableRowOfInfo(AIPForm FInfo){
     Vector Trow  =new Vector();
     Trow.add(BTNCOL,"");
     Trow.add(EHFORMCOL,FInfo.getName());     
     Trow.add(RDOCTYPCOL,getDoctypeName(FInfo.getMapDTId()));
     Trow.add(DNODECOL,getNodeName(FInfo.getDesNodeId()));
     Trow.add(EHFORMIDCOL,new Integer(FInfo.getId()));
     Trow.add(RDTIDCOL,new Integer(FInfo.getMapDTId()));
     Trow.add(RNODEIDCOL,new Integer(FInfo.getDesNodeId()));
     Trow.add(FSTATUS,new Integer(FInfo.getStatus()));
     Trow.add(FEXSITS,new Integer(FInfo.isExsits() ? 1:0));
     Trow.add(STATE,new Integer(6));
     Trow.add(EHFFIELDSCOL,FInfo.getFields());     
     //added by amaleh on 2004-01-29
     //column containing node path
     if(FInfo.getDesNodePath() !=null)
     Trow.add(RNODEPATHCOL, FInfo.getDesNodePath());
     else Trow.add(RNODEPATHCOL, "");
    
     return Trow;
  }
  public int getFormRow(String FormName){
       for(int i=0;i<TableModel.getRowCount();i++){
            if(FormName.equalsIgnoreCase(TableModel.getValueAt(i,EHFORMCOL).toString().trim()))
            return i;
       }
       return -1;
  }
  public String getDoctypeName (int DTid){
      if(DTid == 0) return "";
      DocType dt =DoctypesManager.getDoctype(AIPManager.getCurrSid(),DTid);
      if(dt != null )
         return dt.getName();
      
      return "";
  }
  public void DeleteEHFormRow(int Row){

        int Formid =((Integer)TableModel.getValueAt(Row,EHFORMIDCOL)).intValue();
        if(Formid >0){
            AIPForm form =new AIPForm();
            form.setId(Formid);
           int ret =AIPManager.getVWC().deleteAIPForm(AIPManager.getCurrSid(),form);            
           if(ret != AIPManager.NOERROR)
               javax.swing.JOptionPane.showMessageDialog(this,AIPManager.getErrorDescription(ret)+"<"+ret+">",AIPUtil.getString("Template.Msg.NoSelectFolder"),javax.swing.JOptionPane.ERROR_MESSAGE);
           else
           TableModel.RemoveRow(Row);
        }
  }
  public void setAllEnabled(boolean enable){
     isAllCellEditable = enable;
     if(enable){
       if(DTtable.getSelectedRows().length == 0 ) tempPane.Delbtn.setEnabled(false);
       else tempPane.Delbtn.setEnabled(true);
       if(!isTableUnsave ) tempPane.Savebtn.setEnabled(false);
       else tempPane.Savebtn.setEnabled(true);
     }
  }
  public void ResetAllMapFields(AIPForm form){
       
      for(int i=0;i<form.getFields().size();i++)
     {    
           AIPField f =(AIPField)form.getFields().get(i);
           f.setMapIndexId(0);
           f.setFDefault("");
     }    
      
  }
 
  //--------------------table Inner class & Methode------------------------
  public void ShowMapFieldsDialog(int Pointrow,int X,int Y){
          Frame f = JOptionPane.getFrameForComponent(DTtable);
          MapFieldsDialog MFs =new MapFieldsDialog(f,AIPUtil.getString("FieldsMapping.Lab.Title"),true,this,getAIPForm(Pointrow));
          
          if(! isAllCellEditable)MFs.setAllEnable(false);
          MFs.setLocationRelativeTo(DTtable);
          MFs.setLocation(tempPane.getLocationOnScreen().x+211,tempPane.getLocationOnScreen().y+126);
          MFs.setSize(475,270);
          MFs.setVisible(true);
          f.pack();
  }
  //Methode responce to table Selection change
  public void  DTTableSelectionChanged(ListSelectionEvent e){
        ListSelectionModel lsm = (ListSelectionModel)e.getSource();
        if (!lsm.isSelectionEmpty()) {
            SelectedRow =DTtable.getSelectedRow();
            if (!EyesHandsManager.isProcessing) {
               tempPane.Delbtn.setEnabled(true);

            }else {

                tempPane.Delbtn.setEnabled(false);
            }
        }else {

                tempPane.Delbtn.setEnabled(false);
        }
  }
  private class TableMouseListener extends MouseAdapter  {
      public void mousePressed(MouseEvent e) {
           int Pointrow =DTtable.rowAtPoint(e.getPoint());
           if (e.getClickCount() == 1){


              if(Pointrow > -1)
                  DTtable.setRowSelectionInterval(Pointrow,Pointrow);
              if (e.getModifiers() == e.BUTTON3_MASK) {
                //  maybeShowPopup(e,Pointrow);
              }else if (e.getModifiers() == e.BUTTON1_MASK){

              }
           }else if(e.getClickCount() == 2){
                if(Pointrow > -1){
                  if(!TableModel.getValueAt(Pointrow,RDOCTYPCOL).toString().equals("")){
                    ShowMapFieldsDialog(Pointrow,e.getX(),e.getY());
                  }
                }
           }
     }
     public void mouseReleased(MouseEvent e) {
     }

  }

 //--------------------  SpecialTableModel-----------------------
  public  class SpecialTableModel extends AbstractTableModel {
       String[] columnNames={"",AIPUtil.getString("Template.TCol.EHForm"),AIPUtil.getString("Template.TCol.VWDocType"),AIPUtil.getString("Template.TCol.DestinationVWNode")};
       Vector ARow;
       int Srow ;

       public SpecialTableModel(){
       }
       public int getColumnCount() {
           return columnNames.length;
       }
       public int getRowCount() {
          return Tabrows.size();
       }
       public String getColumnName(int col) {
         return columnNames[col];
       }
       public Object getValueAt(int aRow, int aColumn) {
          Vector row = (Vector) Tabrows.elementAt(aRow);
          return row.elementAt(aColumn);
       }
       public Class getColumnClass(int c) {
          return getValueAt(0, c).getClass();
       }
       public boolean isCellEditable(int row, int col) {
          
           if(isAllCellEditable) return isDTCellEditable(row,col);
           
           else return false;
       }
       public void addTableRow(Vector RowData){
          Tabrows.add(RowData);
           fireTableRowsInserted(Tabrows.size()-1,Tabrows.size()-1);
       }
       public void setValueAt(Object value, int row, int col) {

              Vector row1 = (Vector) Tabrows.elementAt(row);
              if(value ==null)return;
              if(value.toString().equals(row1.elementAt(col).toString())) return;
              
              if(col==DNODECOL) {
                  isTableUnsave=true;
                  tempPane.Savebtn.setEnabled(true);
              }
              if (col == RDOCTYPCOL){
                 isTableUnsave=true;
                 tempPane.Savebtn.setEnabled(true);
                 row1.remove(STATE);
                 row1.add(STATE,new Integer(AIPForm.FIELDSNEEDMAP));
                 ResetAllMapFields(getAIPForm(row));
                // ResetAllMapFields((Vector)row1.get(MAPDTFORMFIELDSCOL));
              }
              int rowState= ((Integer)row1.get(STATE)).intValue();
              if (col == RNODEIDCOL && ! row1.elementAt(col).equals(value)){
                 if(rowState != AIPForm.UPDATE && rowState != AIPForm.FIELDSUPDATE &&
                     rowState != AIPForm.NEW && rowState != AIPForm.FIELDSNEEDMAP ){
                    row1.remove(STATE);
                    row1.add(STATE,new Integer(AIPForm.UPDATE));
                 }
              }
              //added by amaleh on 2004-01-29
              //sets row status to UPDATE if node path is updated.
              if (col == RNODEPATHCOL && ! row1.elementAt(col).equals(value)){
                 if(rowState != AIPForm.UPDATE && rowState != AIPForm.FIELDSUPDATE &&
                     rowState != AIPForm.NEW && rowState != AIPForm.FIELDSNEEDMAP ){
                    isTableUnsave=true;
                    tempPane.Savebtn.setEnabled(true);
                    row1.remove(STATE);
                    row1.add(STATE,new Integer(AIPForm.UPDATE));
                 }
              }
              //
              row1.remove(col);
              row1.add(col,value);
       }
       public void RemoveRow(int Rnum){
          Tabrows.remove(Rnum);
          fireTableRowsDeleted(Rnum,Rnum);
       }
       public void RemoveAllRows(){
              int lastrow =Tabrows.size();
              if(lastrow >0){
                Tabrows.removeAllElements();
                fireTableRowsDeleted(0,lastrow-1);
              }
       }

       public boolean isDTCellEditable(int row, int col) {
             Srow =row;
             ARow = (Vector) Tabrows.elementAt(row);
            TableColumn column;
            JComboBox EditCombo =new JComboBox();
            if(col ==BTNCOL) return false;
            if(col == EHFORMCOL) return false;
            if(col == RDOCTYPCOL){
                column = DTtable.getColumnModel().getColumn(RDOCTYPCOL);
                String[] DTName =new String[MapDoctypes.size()+1] ;
                DTName[0]="";
                for(int i=0 ;i<MapDoctypes.size();i++){
                    DocType DT =(DocType)MapDoctypes.get(i);
                    DTName[i+1]=DT.getName();
                }
                EditCombo = new JComboBox(DTName);
                EditCombo.addActionListener(new ActionListener(){
                    public void actionPerformed(ActionEvent e) {
                        
                        int index=((JComboBox)e.getSource()).getSelectedIndex();
                        if(index > 0){
                            DocType DT =(DocType)MapDoctypes.get(index-1);                            
                            ARow.remove(RDTIDCOL);
                            ARow.add(RDTIDCOL,new Integer(DT.getId()));
                        } else {
                            ARow.remove(RDTIDCOL);
                            ARow.add(RDTIDCOL,new Integer(0));
                        }
                    }
                });
                column.setCellEditor(new DefaultCellEditor(EditCombo));
                return true;
            }
            if(col == DNODECOL ){
                column = DTtable.getColumnModel().getColumn(DNODECOL);
                VWNodeInfo DefaultNode =new VWNodeInfo();
                DefaultNode.setNodeId(0);
                DefaultNode.setNodeName(AIPUtil.getString("Template.Val.CustomVWNode"));
                //modified by amaleh on 2003-06-03 -- (-1) was changed to (-2)
                DefaultNode.setParentId(-1);
                DefaultMutableTreeNode MainNode1=new DefaultMutableTreeNode(DefaultNode);

                DefaultMutableTreeNode MainNode =(DefaultMutableTreeNode)((Vector)NodesNavigator.get(0)).get(1);
                /* added by amaleh on 2003-06-03 */
                /*
                VWNodeInfo TextFileNode = new VWNodeInfo();
                TextFileNode.setNodeId(-1);
                TextFileNode.setNodeName(AIPUtil.getString("Template.Val.TextFileVWNode"));
                TextFileNode.setParentId(-2);
                DefaultMutableTreeNode MainNode2=new DefaultMutableTreeNode(TextFileNode);
                MainNode1.add(MainNode2);
                 */
                /**/
                MainNode1.add(MainNode);
                DefaultTreeModel treeModel = new DefaultTreeModel(MainNode1);
                TreeCombo NodeNavTreeCombo = new TreeCombo(treeModel);                
                
                NodeNavTreeCombo.addActionListener(new ActionListener(){                    
                    public void actionPerformed(ActionEvent e) {
                       
                        int NodeId = ((TreeCombo)e.getSource()).VWNodeId;
                        int SLevel = ((TreeCombo)e.getSource()).VWNodeLevel;
                        // modified by amaleh on 2004-01-28
                        // bring up DestinationVWNodeDialog if custom was chosen
                        if (NodeId == 0 && SLevel == 0)
                        {
                            Object o= getValueAt(Srow, RDOCTYPCOL);
                            //JComboBox box = (JComboBox)getValueAt(frow, fcol);
                            //int index=box.getSelectedIndex();
                            String dtName = o.toString();
                            int roomID = AIPManager.getCurrSid();
                            Frame f = JOptionPane.getFrameForComponent(DTtable);
                            String nodePath = (String)getValueAt(Srow, RNODEPATHCOL);
                            
                            DestinationVWNodeDialog dialog = new DestinationVWNodeDialog(f, AIPUtil.getString("DestinationVWNode.Lab.Title"), true, dtName, AIPManager.getCurrSid(), nodePath);
                            dialog.show();
                            //set value at tree path column as dialog.getPath()
                            if (dialog.getValue() == dialog.OK_OPTION)
                               setValueAt(dialog.getPath(), Srow, RNODEPATHCOL);
                        }
                        //
                        
                        // added by amaleh on 2004-02-05
                        // add node path to already chosen node.
                        if (NodeId > -1 && SLevel > 3)
                        {
                            String DNodePath = "";
                            int DNodeid = NodeId;
                            //calculates tree node path from id number
                            DefaultMutableTreeNode node = null;
                            //skip root, begin from room
                            for (int i = 0; i < NodesNavigator.size(); i++)
                            {
                                Vector current = (Vector)NodesNavigator.get(i);
                                int currentNodeID = ((Integer)(current.get(0))).intValue();
                                if (currentNodeID == DNodeid)
                                {
                                    node = (DefaultMutableTreeNode)(current.get(1));
                                    break;
                                }
                            }
                            if (node != null)
                            {
                                Object[] nodePath = node.getUserObjectPath();
                                StringBuffer pathBuffer = new StringBuffer();
                                for (int i = 1; i < nodePath.length; i++)
                                {
                                    VWNodeInfo nodeInfo = (VWNodeInfo)nodePath[i];
                                    if (i != nodePath.length - 1)
                                        pathBuffer.append(nodeInfo.getNodeName() + "/");
                                    else
                                        pathBuffer.append(nodeInfo.getNodeName());
                                }


                              DNodePath = pathBuffer.toString();
                            }
                            setValueAt(DNodePath, Srow, RNODEPATHCOL);
                        }
                        //

                        // modified by amaleh on 2003-06-03 -- (Node ID == 0 &&... changed to (Node ID <= 0)
                        if((NodeId > -1 && SLevel > 3)||(NodeId == 0 && SLevel == 0)/*||(NodeId < 0 && SLevel == 1)*/){
                            setValueAt(new Integer(NodeId),Srow,RNODEIDCOL);

                        } else {
                             if(SLevel != 0)
                             JOptionPane.showMessageDialog(DTtable,AIPUtil.getString("Template.Msg.NoSelectFolder"),AIPUtil.getString("Init.Msg.Title"),JOptionPane.INFORMATION_MESSAGE);
                             //modified by amaleh on 2004-01-28 from DefaultVWNode to CustomVWNode
                             setValueAt(AIPUtil.getString("Template.Val.CustomVWNode"),Srow,DNODECOL);
                             setValueAt(new Integer(0),Srow,RNODEIDCOL);
                        }
                    }
                });
                
                column.setCellEditor(new DefaultCellEditor(NodeNavTreeCombo));
                return true;
            }
            return true;
       }

   }

   public boolean IsUnSaved(){
      return isTableUnsave;
   }
   public boolean checkUnSavedRows(){
      for(int i=0;i<TableModel.getRowCount();i++){
           int State = ((Integer)TableModel.getValueAt(i,STATE)).intValue();
           if(State == AIPForm.FIELDSNEEDMAP ||State == AIPForm.UPDATE ||State == AIPForm.FIELDSUPDATE  ){
                isTableUnsave =true;
                return true;
           }
      }
      isTableUnsave =false;
      return false;
   }
   class RowRenderer extends DefaultTableCellRenderer {
      public RowRenderer(){
        super();
        //setHorizontalAlignment(JLabel.CENTER);
       // setBackground(Color.white);
      }

      public Component getTableCellRendererComponent(
            JTable table, Object value,
            boolean isSelected, boolean hasFocus,
            int row, int column) {
           int Status =((Integer)TableModel.getValueAt(row,FSTATUS)).intValue();
           if(Status == AIPForm.FIELDSNEEDMAP){
              setBackground(new Color(166,166,166));
           }
           else{
              setBackground(Color.white);
           }
           if(column == DNODECOL && TableModel.getValueAt(row,DNODECOL).toString().equalsIgnoreCase(AIPUtil.getString("Template.Val.MissingVWNode"))){
              setForeground(Color.red);
           }else setForeground(Color.black);
          return  super.getTableCellRendererComponent(table,value
                        ,isSelected,hasFocus,row,column);
      }


  }

   private class ButtonRenderer extends JButton
                        implements TableCellRenderer {
        ImageManager IM =new ImageManager();
        public ButtonRenderer() {
            super();
        }
        public Component getTableCellRendererComponent(
                                JTable table, Object color,
                                boolean isSelected, boolean hasFocus,
                               int row, int column) {

          int EXsits =((Integer)TableModel.getValueAt(row,FEXSITS)).intValue();
          //int state =((Integer)TableModel.getValueAt(row,STATE)).intValue();
          //setToolTipText("State:"+TableModel.getValueAt(row,STATE).toString());
          
          if(EXsits ==0 ){
            setIcon(IM.getImageIcon("deleterec.gif"));
            setToolTipText(connectorManager.getString("MapDTypeLayout.Eye.Mis"));
          }else{
             int Status =((Integer)TableModel.getValueAt(row,FSTATUS)).intValue();
             if(Status == AIPForm.FIELDSNEEDMAP){
                setIcon(IM.getImageIcon("updaterec.gif"));
                setToolTipText(connectorManager.getString("MapDTypeLayout.Eye.Updated.NewMap"));
             }else if(Status == AIPForm.UPDATE){
               setIcon(IM.getImageIcon("updaterec.gif"));
                setToolTipText(connectorManager.getString("MapDTypeLayout.Eye.Updated")+" ");
             }else{
                setIcon(null);
                setToolTipText(null);
             }
          }
          return this;
        }
   }
   private class ButtonEditor extends DefaultCellEditor {
       public ButtonEditor() {
                super(new JCheckBox());
            JButton b =new JButton();
            editorComponent = b;
            setClickCountToStart(1);
            b.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    fireEditingStopped();
                }
            });
       }
       protected void fireEditingStopped() {
            super.fireEditingStopped();
       }

       public Object getCellEditorValue() {
            return null;
       }
       public Component getTableCellEditorComponent(JTable table,
                                                     Object value,
                                                     boolean isSelected,
                                                     int row,
                                                    int column) {
          return editorComponent;
       }
   }
   class T_HeaderRenderer extends JLabel
                        implements TableCellRenderer {

        public T_HeaderRenderer() {
            super();
            setBorder(new CompoundBorder(BorderFactory.createEtchedBorder(),new EmptyBorder(0,2,0,0)));
            setFont(new Font("Dialog",0,12));
            setHorizontalAlignment(SwingConstants.LEFT);
            setHorizontalTextPosition(SwingConstants.LEFT);
        }
        public Component getTableCellRendererComponent(
                                JTable table, Object value,
                                boolean isSelected, boolean hasFocus,
                               int row, int column) {
            setText(value.toString());
            return this;
        }
   }
}