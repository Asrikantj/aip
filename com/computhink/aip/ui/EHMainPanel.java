package com.computhink.aip.ui;

import com.computhink.aip.*;
import com.computhink.aip.resource.ResourceManager;
import com.computhink.aip.util.AIPUtil;
import com.computhink.aip.util.VWLogger;

import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;



/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public class EHMainPanel extends JPanel{
  JTabbedPane TabPane;
  EHTemplatePanel TempPane;
  EHHistoryPanel HistoryPane;
  /*
   * Enhancement 	: Enhancements of AIP  
   * 				  Requirment sent by Rajesh on 09-June-2006 through eMail  	 
   * Created By		: M.Premananth
   * Date			: 14-June-2006
   */
  EHPendingPanel PendingPane;
  EHGeneralPanel GenPane;
  int CurrentTab =0;
  PagesManager  PManager;
  EyesFileManager EFManager;
  // added by amaleh
  CustomFileManager CFManager;
  public JFrame frame;
  private JLabel lblContentverse;
  

  public EHMainPanel(JFrame frame1){
    // super(frame,title,modal);
     frame =frame1;
     EFManager =new EyesFileManager();
      EFManager =new EyesFileManager();
     EFManager.setFormsFileName(AIPUtil.getString("Init.FormsFile.Name"));
     EFManager.setDocsFileExt(AIPUtil.getString("Init.FormsDocsFile.Ext"));
     /* added by amaleh */
     CFManager =new CustomFileManager();
     CFManager.setFormsFileName(AIPUtil.getString("Init.FormsFile.Name"));
     CFManager.setFormsDocsFileExt(AIPUtil.getString("Init.FormsDocsFile.Ext"));
     CFManager.setDocsFileExt(AIPUtil.getString("Init.DocsFile.Ext"));
     CFManager.setTempDocsFileExt(AIPUtil.getString("Init.TempDocsFile.Ext"));
     /**/
     TempPane =new EHTemplatePanel(this);
     HistoryPane =new EHHistoryPanel(this);
     /*
      * Enhancement 	: Enhancements of AIP  
      * 				  Requirment sent by Rajesh on 09-June-2006 through eMail  	 
      * Created By		: M.Premananth
      * Date			: 14-June-2006
      */
     PendingPane =new EHPendingPanel(this);
     GenPane =new EHGeneralPanel(this);
     try{
     PManager =new PagesManager();
     }
     catch (java.lang.UnsatisfiedLinkError e){
             JOptionPane.showMessageDialog(this,AIPUtil.getString("Init.Msg.LostLibrary"),AIPUtil.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);
            // if(Log.LOG_ERROR) Log.error("Native library VWPage.dll not found. Cannot proceed.",e);
             ExitConnector();
     }

    JPanel Panel1 =new JPanel();
    Panel1.setBorder(new EmptyBorder(5,5,10,8));
    Panel1.setLayout(new BorderLayout());
    //getContentPane().add(Panel1);
    this.add(Panel1);
    TabPane=new JTabbedPane();
    Panel1.setMinimumSize(new Dimension(600,450));
    Panel1.setMaximumSize(new Dimension(600,450));
    TabPane.setBorder(new EmptyBorder(5,5,5,5));
    TabPane.addTab(AIPUtil.getString("Init.Lab.General"),GenPane);
    
    TabPane.addTab(AIPUtil.getString("Init.Lab.Template"),TempPane);
    
    TabPane.addTab(AIPUtil.getString("Init.Lab.History"),HistoryPane);
    /*
     * Enhancement 	: Enhancements of AIP  
     * 				  Requirment sent by Rajesh on 09-June-2006 through eMail  	 
     * Created By		: M.Premananth
     * Date			: 14-June-2006
     */
    TabPane.addTab(AIPUtil.getString("Init.Lab.Pending"),PendingPane);
    TabPane.setFont(new Font(TabPane.getFont().getName(),Font.BOLD,TabPane.getFont().getSize()));
    TabPane.addChangeListener(new ChangeListener(){
        public void stateChanged(ChangeEvent e){
            if(CurrentTab == 1) TempPane.MDT.DTtable.removeEditor();
            CurrentTab = ((JTabbedPane)e.getSource()).getSelectedIndex();
        }
    });
    Panel1.add(TabPane, BorderLayout.CENTER);
    setAllEnabled(false,1);
    TabPane.setEnabledAt(1, false);
    TabPane.setEnabledAt(2, false);
    TabPane.setEnabledAt(3, false);
    
    lblContentverse = new JLabel(AIPUtil.getString("powered_by_contentverse")+ "  ", SwingConstants.RIGHT);
	lblContentverse.setForeground(Color.GRAY);
	Font font = lblContentverse.getFont();
	lblContentverse.setFont(new Font(font.getName(), Font.PLAIN, 10));
	Panel1.add(lblContentverse, BorderLayout.SOUTH);
	
	if(AIPManager.isAutoStart())
    {    
        GenPane.autoStart();        
    }    
  }
  public void setAllEnabled(boolean enable,int status){
     if(status == 2) // process type
     {    
         if(enable) frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
         else frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
          GenPane.setAllEnabled(enable,2);
          TempPane.setAllEnabled(enable);
          HistoryPane.setAllEnabled(enable);
          PendingPane.setAllEnabled(enable);
     }
     else if(status ==1) // init type
     {
          GenPane.setAllEnabled(enable,1);
          TempPane.setAllEnabled(enable);
          HistoryPane.setAllEnabled(enable);
          PendingPane.setAllEnabled(enable);
         
     }    
  }
  /* Issue # 754: If VWS server is down, client should be notified
  *  Methods added of issue 754, C.Shanmugavalli, 10 Nov 2006
  */
  public void stopProcessAndExit()
  {
 
  	{
  		try {
  		GenPane.Startbtn.setText(ResourceManager.getDefaultManager().getString("General.Btn.Stop"));
  		GenPane.clickStartStop();
  		GenPane.clickConnectDisconnect();
  	}catch(Exception ex){ VWLogger.error("Exception :::"+ex.getMessage(),ex);
  		GenPane.clickConnectDisconnect();
  	}

  	}  
	
  }
  
  public void ExitConnector(){
       java.util.Vector sids =AIPManager.getConnectSessionIds();
       for(int i=0;i<sids.size();i++)
       {
           GenPane.disconnect(((Integer)sids.get(i)).intValue());           
       }
      //when close the application, reset the application  start flag;   
       AIPManager.setAIPApplicationStarted(false);
	   /**CV10.2 - Added to stop AIP process after clicking on close button**/
       GenPane.stopAIPProcess(false);
       PendingPane.stopPendingProcess();
       /**End of CV10.2 code merges********/
       AIPManager.shutdown();
       System.exit(0); 
  }
  
}