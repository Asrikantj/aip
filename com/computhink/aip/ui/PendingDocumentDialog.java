package com.computhink.aip.ui;

import com.computhink.aip.*;
import com.computhink.aip.resource.ResourceManager;
import com.computhink.aip.resource.images.ImageManager;
import com.computhink.aip.util.AIPUtil;
import com.computhink.aip.util.VWLogger;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;
import javax.swing.border.*;
import java.util.*;
import java.io.*;
import com.computhink.aip.client.*;
import com.computhink.common.DocType;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class PendingDocumentDialog extends JDialog {
  ProcessPendingFileLayout PFL;
  PendingDocumentFieldsLayout PDFL;
  JButton Savebtn;
  private JButton Hlpbtn;
  private JButton Closebtn;
  private JButton Delbtn;
  private JButton Prevbtn;
  private JButton nextbtn;
  private ButtonListener btnListener =new ButtonListener();
  private final ResourceManager connectorManager = ResourceManager.getDefaultManager();
  //JTextField FormFld;
  JComboBox FormBox;
  
  JCheckBox FormAllChk = new JCheckBox(connectorManager.getString("Pending.Lab.All"));
  
  JLabel DocumentLab;
  JTextField PathFld;
  
  JCheckBox PathAllChk = new JCheckBox(connectorManager.getString("Pending.Lab.All"));
  
  JTextField VWNodeFld;
  JTextField VWSecurityFld;
 
  JCheckBox VWNodeAllChk = new JCheckBox(connectorManager.getString("Pending.Lab.All"));
  
  JComboBox imageDropDown = new javax.swing.JComboBox();
  JButton viewButton ;
  private Vector images = new Vector();
  /**/
  private JTextField statusField  ;
  Vector  PendingDoc ;
  int DocNum =0;
  int CurrDocId;
  boolean CurrDocUpdate =false;
  

  public PendingDocumentDialog(Frame frame, String title, boolean modal,ProcessPendingFileLayout PFL1,Vector  PendingDoc1) {
    super(frame, title, modal);
    PFL =PFL1;
    PDFL =new PendingDocumentFieldsLayout(this);
    JPanel Panel1 =new JPanel();
    getContentPane().add(Panel1,BorderLayout.CENTER);
    Panel1.setLayout(new BoxLayout(Panel1,BoxLayout.Y_AXIS));
    Panel1.setBorder(new CompoundBorder(new TitledBorder(null,null,
	TitledBorder.LEFT, TitledBorder.TOP),new EmptyBorder(2,8,5,5)));
    statusField = new JTextField("");
    statusField.setBackground(Panel1.getBackground());
    statusField.setPreferredSize(new Dimension(200,30));
    statusField.setBorder(new CompoundBorder(new EmptyBorder(1,0,0,0),new TitledBorder(null,null,
	TitledBorder.LEFT, TitledBorder.TOP)));
    statusField.setForeground(Color.red);
    //statusField.setBorder(new EmptyBorder(0,3,0,5));
    statusField.setEditable(false);

    Panel1.add(CreateDocPanel());
    Panel1.add(Box.createVerticalStrut(10));
    Panel1.add(PDFL);
    Panel1.add(Box.createVerticalStrut(10));
    Panel1.add(CreateButtonPanel());
    getContentPane().add(statusField,BorderLayout.SOUTH);
    PendingDoc =PendingDoc1;
    loadDocumentData(DocNum);
    //this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    addComponentListener(new ComponentAdapter(){
          public void componentHidden(ComponentEvent e){
              if(CurrDocUpdate || PDFL.isTableUpdate ){
                setModal(false);
                setVisible(true);
                if(CheckUnSaveBeforeChange()) setVisible(false);
                setModal(true);
              }
          }
          public void componentMoved(ComponentEvent e){
          }
          public void componentShown(ComponentEvent e){
          }
     });

  }
  public void exitDialog(){
       setVisible(false);
  }
  public void setAllEnable(boolean enable){
      Savebtn.setEnabled(enable);
      //Closebtn.setEnabled(false);
      Hlpbtn.setEnabled(enable);
  }
  public JPanel CreateDocPanel(){
      JPanel Pane =new JPanel();
      Pane.setLayout(new BoxLayout(Pane, BoxLayout.X_AXIS));
      JPanel Doc =new JPanel();
      Doc.setLayout(new BoxLayout(Doc, BoxLayout.Y_AXIS));
      JPanel DocP1 =new JPanel();
      DocP1.setLayout(new BoxLayout(DocP1, BoxLayout.X_AXIS));
      JPanel DocP2 =new JPanel();
      DocP2.setLayout(new BoxLayout(DocP2, BoxLayout.X_AXIS));
      JPanel DocP3 =new JPanel();
      DocP3.setLayout(new BoxLayout(DocP3, BoxLayout.X_AXIS));
      /* added by amaleh on 2003-10-27 */
      JPanel DocP3A =new JPanel();
      DocP3A.setLayout(new BoxLayout(DocP3A, BoxLayout.X_AXIS));
      JPanel DocP3B =new JPanel();
      DocP3B.setLayout(new BoxLayout(DocP3B, BoxLayout.X_AXIS));
      /**/
      /* added by amaleh on 2003-07-17 */
      JPanel DocP4 =new JPanel();
      DocP4.setLayout(new BoxLayout(DocP4, BoxLayout.X_AXIS));
      /**/
      /* added by amaleh on 2003-10-27 */
      JPanel dropDownPanel = new JPanel();
      
      viewButton = new javax.swing.JButton();
      viewButton.setText(AIPUtil.getString("Pending.Btn.View"));
      viewButton.setMnemonic(AIPUtil.getString("Pending.MBtn.View").trim().charAt(0));
      viewButton.addActionListener(btnListener);
      dropDownPanel.add(imageDropDown);
      dropDownPanel.add(viewButton); 
      ((FlowLayout)(dropDownPanel.getLayout())).setAlignment(FlowLayout.LEFT);
      /**/
      DocumentLab =new JLabel(AIPUtil.getString("Pending.Lab.Document"));
      DocumentLab.setFont(new Font(DocumentLab.getFont().getName(),Font.BOLD,DocumentLab.getFont().getSize()));
      JLabel DNumber =new JLabel();
      ImageManager IM =new ImageManager();
      Prevbtn =new JButton(IM.getImageIcon("prev.gif"));
      nextbtn =new JButton(IM.getImageIcon("next.gif"));
      Prevbtn.setPreferredSize(new Dimension(35,28));
      Prevbtn.setMinimumSize(new Dimension(35,28));
      nextbtn.setPreferredSize(new Dimension(35,28));
      nextbtn.setMinimumSize(new Dimension(35,28));

      DocP1.add(DocumentLab);
      DocP1.add(DNumber);
      DocP1.add(Box.createHorizontalGlue());
      DocP1.add(Prevbtn);
      DocP1.add(nextbtn);
      DocP1.setAlignmentX(Component.LEFT_ALIGNMENT);

      JLabel FormLab = new JLabel(AIPUtil.getString("Pending.Lab.FormName"));
     
      FormBox = new JComboBox();
      FormBox.setEditable(true);
            
      FormBox.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                CurrDocUpdate =true;
                Savebtn.setEnabled(true);
            }
      });
      FormLab.setPreferredSize(new Dimension(75,22));
      FormLab.setMinimumSize(new Dimension(75,22));
            //FormFld.setEnabled(false);
      JLabel ImageLab = new JLabel(AIPUtil.getString("Pending.Lab.ImagesPath"));
      ImageLab.setPreferredSize(new Dimension(75,22));
      ImageLab.setMinimumSize(new Dimension(75,22));
      PathFld =new JTextField();
      PathFld.getDocument().addDocumentListener(new MyDocumentListener());
      PathFld.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                CurrDocUpdate =true;
                Savebtn.setEnabled(true);
            }

      });
      JLabel securityLab =new JLabel(AIPUtil.getString("Pending.Lab.Security"));
      securityLab.setPreferredSize(new Dimension(75,22));
      securityLab.setMinimumSize(new Dimension(75,22));
      VWSecurityFld =new JTextField();
      VWSecurityFld.getDocument().addDocumentListener(new MyDocumentListener());
      /* added by amaleh on 2003-07-17 */
      JLabel VWNodeLab = new JLabel(AIPUtil.getString("Pending.Lab.VWNode"));
      VWNodeLab.setPreferredSize(new Dimension(75,22));
      VWNodeLab.setMinimumSize(new Dimension(75,22));
      VWNodeFld =new JTextField();
      VWNodeFld.getDocument().addDocumentListener(new MyDocumentListener());
      VWNodeFld.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                CurrDocUpdate =true;
                Savebtn.setEnabled(true);
            }

      });     
     
      ItemListener chkListener = new ItemListener()
      {
          public void itemStateChanged(ItemEvent e)
          {
              JCheckBox chk = (JCheckBox)(e.getSource());
              if (e.getStateChange() == e.SELECTED){
                JOptionPane.showMessageDialog(PendingDocumentDialog.this, AIPUtil.getString("Pending.Msg.AllDocNotification"), AIPUtil.getString("Init.Msg.Title"), JOptionPane.WARNING_MESSAGE);              
                CurrDocUpdate =true;
                Savebtn.setEnabled(true);
              }  
          }
      };
      FormAllChk.addItemListener(chkListener);
      PathAllChk.addItemListener(chkListener);
      VWNodeAllChk.addItemListener(chkListener);
      /**/
      DocP2.add(FormLab);
      DocP2.add(Box.createHorizontalStrut(10));
      DocP2.add(FormBox);
      //added by amaleh on 2003-09-25
      DocP2.add(Box.createHorizontalStrut(5));
      DocP2.add(FormAllChk);
      DocP2.setAlignmentX(Component.LEFT_ALIGNMENT);

      DocP3.add(ImageLab);
      DocP3.add(Box.createHorizontalStrut(10));
      DocP3.add(PathFld);
      //added by amaleh on 2003-09-25
      DocP3.add(Box.createHorizontalStrut(5));
      DocP3.add(PathAllChk);
      DocP3.setAlignmentX(Component.LEFT_ALIGNMENT);
      
      DocP3A.add(Box.createHorizontalStrut(80));
      DocP3A.add(dropDownPanel);
      DocP3A.setAlignmentX(Component.LEFT_ALIGNMENT);
      
      DocP3B.add(securityLab);
      DocP3B.add(Box.createHorizontalStrut(10));
      DocP3B.add(VWSecurityFld);
      DocP3B.setAlignmentX(Component.LEFT_ALIGNMENT);
      
      DocP4.add(VWNodeLab);
      DocP4.add(Box.createHorizontalStrut(10));
      DocP4.add(VWNodeFld);
      //added by amaleh on 2003-09-25
      DocP4.add(Box.createHorizontalStrut(5));
      DocP4.add(VWNodeAllChk);
      DocP4.setAlignmentX(Component.LEFT_ALIGNMENT);

      Doc.add(DocP1);
      Doc.add(Box.createVerticalStrut(10));
      Doc.add(DocP2);
      Doc.add(Box.createVerticalStrut(10));
      Doc.add(DocP3);
      Doc.add(Box.createVerticalStrut(10));
      /* added by amaleh on 2003-10-27 */
      /* creates a panel that contains a dropdown list of image indices, which the
       * user can choose one of and then hit the View button to open the chosen
       * image.
       */
      Doc.add(DocP3A);
      Doc.add(Box.createVerticalStrut(10));
      Doc.add(DocP3B);
      Doc.add(Box.createVerticalStrut(10));

      /**/
      Doc.add(DocP4);
      Doc.setAlignmentX(Component.LEFT_ALIGNMENT);
      return Doc;

  }
  public JPanel CreateButtonPanel(){
    JPanel BtnPanel =new JPanel();
    BtnPanel.setLayout(new BoxLayout(BtnPanel, BoxLayout.X_AXIS));
    
    Savebtn    =new JButton(AIPUtil.getString("Pending.Btn.Save"));
    Delbtn     =new JButton(AIPUtil.getString("Pending.Btn.Delete"));
    Hlpbtn      =new JButton(AIPUtil.getString("Pending.Btn.Help"));
    Closebtn     =new JButton(AIPUtil.getString("Pending.Btn.Close"));

    Savebtn.setMnemonic(AIPUtil.getString("Pending.MBtn.Save").trim().charAt(0));
    Delbtn.setMnemonic(AIPUtil.getString("Pending.MBtn.Delete").trim().charAt(0));
    Closebtn.setMnemonic(AIPUtil.getString("Pending.MBtn.Close").trim().charAt(0));
    Hlpbtn.setMnemonic(AIPUtil.getString("Pending.MBtn.Help").trim().charAt(0));

    Savebtn.addActionListener(btnListener);
    Delbtn.addActionListener(btnListener);
    Closebtn.addActionListener(btnListener);
    Hlpbtn.addActionListener(btnListener);
    Prevbtn.addActionListener(btnListener);
    nextbtn.addActionListener(btnListener);

    Savebtn.setPreferredSize(new Dimension(80,25));
    Savebtn.setMinimumSize(new Dimension(80,25));
    Delbtn.setPreferredSize(new Dimension(80,25));
    Delbtn.setMinimumSize(new Dimension(80,25));
    Closebtn.setPreferredSize(new Dimension(80,25));
    Closebtn.setMinimumSize(new Dimension(80,25));
    Hlpbtn.setPreferredSize(new Dimension(80,25));
    Hlpbtn.setMinimumSize(new Dimension(80,25));

    Savebtn.setEnabled(false);
    Delbtn.setEnabled(true);
    Closebtn.setEnabled(true);
    Hlpbtn.setEnabled(true);

    BtnPanel.add(Box.createHorizontalGlue());
    BtnPanel.add(Savebtn);
    BtnPanel.add(Delbtn);
    BtnPanel.add(Closebtn);
    BtnPanel.add(Hlpbtn);
    
    BtnPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
    return BtnPanel;

  }  


  //-------------------------------------- ----------------------------------------------
   public void loadDocumentData(int Docnum){
        int sid =0;
        if(Docnum < PendingDoc.size()){
            if(Docnum == 0 ) Prevbtn.setEnabled(false);
            else Prevbtn.setEnabled(true);
            if(Docnum == PendingDoc.size()-1 ) nextbtn.setEnabled(false);
            else nextbtn.setEnabled(true);
            AIPDocument PDInfo=(AIPDocument)PendingDoc.get(Docnum);
            int ret = AIPManager.getVWC().getAIPDocFields(AIPManager.getCurrSid(),PDInfo);
            if(ret !=AIPManager.NOERROR)
            {    
              JOptionPane.showMessageDialog(this,AIPManager.getErrorDescription(ret)+"("+ret+")",AIPUtil.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);
              //return ; 
            }              
            DocumentLab.setText(AIPUtil.getString("Pending.Lab.Document")+" "+(Docnum+1)+" of "+PendingDoc.size());
                          
            if ((PDInfo.getFieldNames() != null) && (!PDInfo.getFieldNames().trim().equals("")))
            {
                        FormBox.removeAllItems();      
                
                        Vector Doctypes =new Vector();
                         DoctypesManager.getDoctypes(AIPManager.getCurrSid(),Doctypes, false);
                        for (int i = 0; i < Doctypes.size(); i++)
                        {
                            DocType currentDoctype = (DocType)Doctypes.get(i);
                            FormBox.addItem(currentDoctype.getName());
                        }
                        FormBox.setEnabled(true);                         
            }
            else
            {                        
                        FormBox.setEnabled(false);
                       
            }
            FormBox.setSelectedItem(PDInfo.getAIPForm().getName());
            
            String imagePaths = PDInfo.getImagesPath();
            VWLogger.info("imagePaths >>>>>>"+imagePaths);
            if(imagePaths!=null)
            {    
                PathFld.setText(imagePaths);               
                File[] files = PagesManager.parseImageFiles(imagePaths);
                DefaultComboBoxModel imageModel = new DefaultComboBoxModel();
                int i = 1;
                images = new Vector();                
                if (files != null)
                for (int j = 0; j < files.length; j++)
                {
                     images.addElement(files[j].getPath());
                    imageModel.addElement(new Integer(i++));
                }
                imageDropDown.setModel(imageModel);
            }
            VWLogger.info("PDInfo.getPendCause() >>>>>>>"+PDInfo.getPendCause());
            statusField.setText(getPendingReasonMessage(PDInfo.getPendCause())); 
            CurrDocId = PDInfo.getId();
            AIPForm FormInfo= null;
            
            if ((PDInfo.getFieldNames() != null) && (!PDInfo.getFieldNames().trim().equals("")))
               PDFL.setMasterInfo(PDInfo);
            else
            {    
                 FormInfo= PFL.getAIPForm(PDInfo.getAIPForm().getName());
                 if(FormInfo != null)
                 {
                    PDFL.setMasterInfo(PDInfo,FormInfo);
                 }
                 else{ 
                    PDFL.setMasterInfo(PDInfo);
                    statusField.setText(getPendingReasonMessage(1)); //No Form Found
                 }   
            }
             
            if(PDInfo.getVWNodePath() != null &&!PDInfo.getVWNodePath().trim().equals(""))
              VWNodeFld.setText(PDInfo.getVWNodePath());
            else 
                if(FormInfo != null && FormInfo.getDesNodePath() != null)
                    VWNodeFld.setText(FormInfo.getDesNodePath());          

            if(PDInfo.getSecurityInfo() != null &&!PDInfo.getSecurityInfo().trim().equals(""))
                VWSecurityFld.setText(PDInfo.getSecurityInfo());
            else
            	VWSecurityFld.setText("");
            
        }
        PDFL.isTableUpdate =false;
        CurrDocUpdate =false;
        Savebtn.setEnabled(false);
   }
   public String getPendingReasonMessage(int i){
          //modified by amaleh on 2003-06-02
          // 7 was changed to 9
          //modified by amaleh on 2003-08-18
          // 9 was changed to 10
          //modified by amaleh on 2003-10-29
          //limit was removed
          if(i>0){
        	String status = AIPUtil.getString("Pending.Status."+Integer.toString(i));
        	if (i == 27) {
        		String filePath = PathFld.getText();
        		if (filePath.indexOf(".") > 0)
        			return "";
        		
        		VWLogger.info("filePath >>>>>>>>"+filePath);
        		String fileName = null;
        		if (filePath.lastIndexOf("/") > 0) {
        			fileName = filePath.substring(filePath.lastIndexOf("/")+1, filePath.length()-1);
        		} else {
        			fileName = filePath.substring(filePath.lastIndexOf("\\")+1, filePath.length()-1);
        		}
        		VWLogger.info("fileName >>>>>>>>"+fileName);
        		status = status.replaceAll("Filename", fileName);
        	}
        	VWLogger.info("status >>>>>>>>"+status);
            return status;
          }
          else return "";
   }
   /* modified by amaleh on 2003-09-25 */
   public void SaveCurrentDocument(){
       if(CurrDocUpdate || PDFL.isTableUpdate ){
           boolean currentOnly = false;
           for (int i = 0; i < PendingDoc.size(); i++)
           {
               // if none of the checkboxes are selected, then work on the current doc only.
               if (!(FormAllChk.isSelected() || PathAllChk.isSelected() || VWNodeAllChk.isSelected()))
               {
                    i = DocNum;
                    currentOnly = true;
               }
               AIPDocument PDInfo=(AIPDocument)PendingDoc.get(i);
               /* added by amaleh on 2003-09-25 */
               if (FormAllChk.isSelected() || i == DocNum)
               {
                   String FormName = AIPUtil.cleanTabInString((String)FormBox.getSelectedItem());
                   PDInfo.getAIPForm().setName(FormName);
               }
               /* added by amaleh on 2003-09-25 */
               if (PathAllChk.isSelected() || i == DocNum)
               {
                   /* added by amaleh on 2003-06-20 */
                   String ImagePath =AIPUtil.cleanTabInString(PathFld.getText());
                   PDInfo.setImagesPath(ImagePath);
               }
               /**/
               /* added by amaleh on 2003-09-25 */
               if (VWNodeAllChk.isSelected() || i == DocNum)
               {
                   /* added by amaleh on 2003-07-17 */
                   String VWNode = AIPUtil.cleanTabInString(VWNodeFld.getText());
                   PDInfo.setVWNodePath(VWNode);
               }
               if (i == DocNum)
               {                
                   String VWSecurityInfo = AIPUtil.cleanTabInString(VWSecurityFld.getText());
                   PDInfo.setSecurityInfo(VWSecurityInfo);
               }               
               // added ayman 
               if(PDFL.isTableUpdate && i== DocNum){
                  PDFL.setUpdateDocFields();
                    PDFL.isTableUpdate =false;
               }
               /**/
               int ret =AIPManager.getVWC().setAIPDocument(AIPManager.getCurrSid(),PDInfo);
               if(ret <=AIPManager.NOERROR)
               {
                  if(ret !=AIPManager.NOERROR)
                  {
                        JOptionPane.showMessageDialog(PendingDocumentDialog.this, AIPManager.getErrorDescription(ret)+"("+ret+")", AIPUtil.getString("Init.Msg.Title"), JOptionPane.ERROR_MESSAGE);    
                  }
               }    
               
               if (currentOnly)
                   break;
           }
           CurrDocUpdate =false;
       }
       
       /* added by amaleh on 2003-09-25 */
       FormAllChk.setSelected(false);
       PathAllChk.setSelected(false);
       VWNodeAllChk.setSelected(false);
       /**/
       Savebtn.setEnabled(false);
   }
   public boolean CheckUnSaveBeforeChange(){       
     if(CurrDocUpdate || PDFL.isTableUpdate ){
           int result = JOptionPane.showConfirmDialog(this,AIPUtil.getString("Pending.Msg.SaveChange"),AIPUtil.getString("Init.Msg.Title"),JOptionPane.YES_NO_CANCEL_OPTION);
           if(result == JOptionPane.CANCEL_OPTION || result==-1 ) return false;
           if(result == JOptionPane.YES_OPTION) {
                SaveCurrentDocument();
                return true;
            }
            if(result == JOptionPane.NO_OPTION) {
                  /* added by amaleh on 2003-09-25 */
                  FormAllChk.setSelected(false);
                  PathAllChk.setSelected(false);
                  VWNodeAllChk.setSelected(false);
                  /**/
                  CurrDocUpdate =false;
                  PDFL.isTableUpdate =false;
                  Savebtn.setEnabled(false);
                  return true;
            }
      }
      return true;
   }
  //-------------------------------------- Listener actions-------------------------------

  class ButtonListener implements ActionListener {
      public void actionPerformed(ActionEvent e) {
           JButton Source = (JButton)e.getSource();
           
           if(Source.equals(Prevbtn)){
                
                if(CheckUnSaveBeforeChange()) loadDocumentData(--DocNum);
                
           }else if(Source.equals(nextbtn)){
               
                if(CheckUnSaveBeforeChange()) loadDocumentData(++DocNum);
                
           }else if(Source.equals(Savebtn)){
                    SaveCurrentDocument();
                    
           }else if(Source.equals(Closebtn)){
                // if(CheckUnSaveBeforeChange())
                  exitDialog();
           }else if(Source.equals(Delbtn)){
              int result = JOptionPane.showConfirmDialog(PDFL,AIPUtil.getString("Pending.Msg.DelEHDoc"),AIPUtil.getString("Init.Msg.Title"),JOptionPane.YES_NO_OPTION);
              if(result == 0){
                  AIPDocument aipDoc =new AIPDocument();
                  aipDoc.setId(CurrDocId);
                  int ret =AIPManager.getVWC().deleteAIPDocument(AIPManager.getCurrSid(),aipDoc);
                  if(ret !=AIPManager.NOERROR)
                  {
                        JOptionPane.showMessageDialog(PendingDocumentDialog.this, AIPManager.getErrorDescription(ret)+"("+ret+")", AIPUtil.getString("Init.Msg.Title"), JOptionPane.ERROR_MESSAGE);    
                  }
                 
                  PendingDoc.remove(DocNum);
                  int FileRow=PFL.table.getSelectedRow();
                  PFL.TableModel.setValueAt(new Integer(PendingDoc.size()),FileRow,PFL.PENDINGCOL);
                  PFL.TableModel.fireTableRowsUpdated(FileRow,FileRow);
                  AIPFile aipFile=(AIPFile)PFL.getAIPFileInfoOfRow(FileRow);
                  AIPManager.getVWC().setAIPHistoryFile(AIPManager.getCurrSid(),aipFile);
                  
                  if(PendingDoc.size() == 0) exitDialog();
                  else{
                   DocNum =0;
                   loadDocumentData(DocNum);
                  // setAllComponentEnable(false);

                  }
              }
              
           }
           else  if( Source.equals(Hlpbtn) ){
               try
               {
                   String helpFilePath = System.getProperty("user.dir") + File.separator + ".." + File.separator + "Help" + File.separator + "VWAIP.chm";
                   File helpFile = new File(helpFilePath);
                   if (helpFile.canRead())
                   {
                       //Runtime.getRuntime().exec("winhlp32.exe " + helpFilePath);
                       java.lang.Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + helpFilePath);
                       //send the window to the back so help files appear on top of it.
                       //PFL.frame.toBack();
                   }
                   else
                    JOptionPane.showMessageDialog(null, AIPUtil.getString("General.Msg.NoHelpFile"), AIPUtil.getString("Init.Msg.Title"), JOptionPane.ERROR_MESSAGE);    
               }
               catch (Exception he)
               {
                   JOptionPane.showMessageDialog(null, AIPUtil.getString("General.Msg.NoHelpFile"), AIPUtil.getString("Init.Msg.Title"), JOptionPane.ERROR_MESSAGE);
                   //if(Log.LOG_ERROR)Log.error("Error in launching help file: ", he);
               }              
           }
           else if(Source.equals(viewButton))
           {
                        viewButton.setEnabled(false); 
                        String imagePath ="";
                        try
                        {
                            //if at least one image exists, launch it in VW
                            if (images.size() != 0)
                            {
                                int imageIndex = imageDropDown.getSelectedIndex();
                                imagePath = (String)(images.get(imageIndex));
                                VWLogger.info("Open image :"+imagePath);
                                Runtime.getRuntime().exec("explorer " + imagePath);
                            }
                        }
                        catch(java.io.IOException ioe)
                        {
                            VWLogger.error("unable to open image <"+imagePath+">!",ioe);
                            JOptionPane.showMessageDialog(null, connectorManager.getString("PendingDocumentDialog.Open.Error"),AIPUtil.getString("Init.Msg.Title"), JOptionPane.ERROR_MESSAGE);
                        }
                        viewButton.setEnabled(true); 
           }    
      }
  }
  class MyDocumentListener implements DocumentListener {
          public void insertUpdate(DocumentEvent e) {
             // AIPDocument PDInfo=(AIPDocument)PendingDoc.get(DocNum);
              //if(!PDInfo.getLocationPath().equals(PathFld.getText())){
                    CurrDocUpdate =true;
                    Savebtn.setEnabled(true);
              //}
          }
          public void removeUpdate(DocumentEvent e) {
                  CurrDocUpdate =true;
                Savebtn.setEnabled(true);
          }
          public void changedUpdate(DocumentEvent e) {
                    CurrDocUpdate =true;
                Savebtn.setEnabled(true);
          }


  }
}