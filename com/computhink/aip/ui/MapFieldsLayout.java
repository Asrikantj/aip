package com.computhink.aip.ui;

import com.computhink.aip.*;
import com.computhink.aip.resource.images.ImageManager;
import com.computhink.aip.util.*;



import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.util.Vector;
import javax.swing.border.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import com.computhink.common.DocType;
import com.computhink.common.Index;
import com.computhink.aip.client.AIPField;
import com.computhink.aip.client.AIPForm;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public class MapFieldsLayout extends JPanel {
  private MapFieldsDialog MFsD;
  private Vector Tabrows;
  private String[] columnNames;
  JTable DTtable;
  private boolean isTableEdit =true;
  public boolean isTableUnsave =false;
  SpecialTableModel TableModel;
  AIPForm   aipForm ;
  String FormName="Form";
  String DTName="DocType";
  DoctypesManager DTManager;
  int CurRDTid =0;
  int CurRIndxKeyRow  =-1;
  Vector RDTIndices;
  Vector FormFields;


  private final int MARGINCOL       =0 ;  private final int VWINDEXCOL     =1 ;
  private final int EHFIELDCOL      =2 ;  private final int DEFAULTCOL     =3 ;
  private final int MASKCOL         =4 ;  private final int EHFIELDIDCOL   =5 ;
  private final int VWINDEXIDCOL    =6 ;  private final int ORDER          =7 ;
  private final int KEYCOL          =8 ;  private final int REQUIRED       =9 ;
  private final int STATE           =10 ; private final int SVID           =11;

  private final int MAXCOLNUM =12;

  public MapFieldsLayout(MapFieldsDialog MFsD1,AIPForm FormInfo) {
    MFsD =MFsD1;
    if(FormInfo.getName() != null && !FormInfo.getName().equals(" ")) FormName = FormInfo.getName();
    if(FormInfo.getMapDTId() >0)
    {    
        DocType dt =DoctypesManager.getDoctype(AIPManager.getCurrSid(),FormInfo.getMapDTId());
        if(dt.getName() != null && !dt.getName().equals(" ")) DTName = dt.getName();
    }    
    
    
    setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
    setAlignmentX(Component.LEFT_ALIGNMENT);
    add(Box.createVerticalStrut(5));
    add(createDTFieldMapPanel(),BorderLayout.CENTER);
    DTManager =new DoctypesManager();
    setMasterEHForm(FormInfo);

  }
  public JScrollPane createDTFieldMapPanel(){
        Tabrows =new Vector();
        TableModel  = new SpecialTableModel();
        DTtable = new JTable(TableModel);
        DTtable.setRowHeight(20);
        DTtable.getTableHeader().setReorderingAllowed(false);
        DTtable.getTableHeader().setMinimumSize(new Dimension(30,21));
        DTtable.getTableHeader().setPreferredSize(new Dimension(50,21));
        DTtable.getTableHeader().setAlignmentX(LEFT_ALIGNMENT) ;
        DTtable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        DTtable.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);

        TableMouseListener MListener =new TableMouseListener();
        DTtable.addMouseListener(MListener);

        DTtable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        ListSelectionModel SelectionModel= DTtable.getSelectionModel();
        JScrollPane tableScroller = new JScrollPane(DTtable);
        tableScroller.addMouseListener(MListener);
        for(int i=0;i<DTtable.getColumnCount();i++){
             TableColumn column = DTtable.getColumnModel().getColumn(i);
             if (i==MARGINCOL){
                 column.setPreferredWidth(18);
                 column.setMaxWidth(18);
                 column.setCellRenderer(new ButtonRenderer());
                 column.setCellEditor(new ButtonEditor());
             }else if (i == EHFIELDCOL){
                 column.setPreferredWidth(130);
                 column.setMaxWidth(400);
             }else if (i == VWINDEXCOL){
                 column.setPreferredWidth(140);
                 column.setMaxWidth(400);
                 column.setCellRenderer(new RowRenderer());
             }else if (i == DEFAULTCOL){
                 column.setPreferredWidth(170);
                 column.setMaxWidth(400);
                 column.setCellRenderer(new RowRenderer());
             }
        }
        tableScroller.setAlignmentX(Component.LEFT_ALIGNMENT);
        return tableScroller;
  }
  public void setAllEnable(boolean enable){
      isTableEdit =enable;
  }
  public Vector getTableRowOfInfo(Index indexInfo){//update
       Vector Trow  =new Vector();
     Trow.add(MARGINCOL,"");
     Trow.add(VWINDEXCOL,indexInfo.getName());
     int Fieldid =0;
     String FFieldName =" ";
     int FOrder =0;
     String Default =(String)indexInfo.getDef().get(0);
     int isKey = indexInfo.isKey() ? 1:0;
     int isRequired =indexInfo.isRequired() ? 1:0;
     AIPField MapField=getMappedFormField (indexInfo.getId());
     if(MapField != null){
        Fieldid = MapField.getId();
        Default = MapField.getFDefault();
        FFieldName = MapField.getName();
        FOrder = MapField.getOrder();
     }
    
     //remark ayman to be see 
     //if(IndexInfo.getSvid() == 1) Default =AIPUtil.getString("FieldsMapping.Val.CurrentDate");
     Trow.add(EHFIELDCOL,FFieldName);
     Trow.add(DEFAULTCOL,Default);
     Trow.add(MASKCOL,indexInfo.getMask());
     Trow.add(EHFIELDIDCOL,new Integer(Fieldid));
     Trow.add(VWINDEXIDCOL,new Integer(indexInfo.getId()));
     Trow.add(ORDER,new Integer(FOrder));
     Trow.add(KEYCOL,new Integer(isKey));
     Trow.add(REQUIRED,new Integer(isRequired));
     Trow.add(STATE,new Integer(0));
     Trow.add(SVID,new Integer(0));       //to be see ayman IndexInfo.getSvid()));
     return Trow;
  }
  public AIPField getMappedFormField(int RIndexid){//update
       if(aipForm.getFields().size() > 0){
          for(int i=0;i<FormFields.size();i++){
             AIPField MapInfo=(AIPField)FormFields.get(i);
              if(MapInfo.getMapIndexId() == RIndexid)
                  return MapInfo;
          }
       }
       return null;
  }
  public AIPField getFormFieldInfo(int FFid){//update
       if(FFid == 0) return null ;
       for(int i=0;i<FormFields.size();i++){
         AIPField FFInfo=(AIPField)FormFields.get(i);
         if(FFid == FFInfo.getId())
          return FFInfo;
       }
       return null;
  }

  public boolean IsUnSaved(){
      return isTableUnsave;
  }
  public boolean isRequiredDefaultFieldsEmpty(){
      for(int i=0;i< TableModel.getRowCount();i++){
        if(TableModel.getValueAt(i,DEFAULTCOL).equals("") && ((Integer)TableModel.getValueAt(i,REQUIRED)).intValue() >0){
            return true;
        }
      }
      return false;
  }
//--------------------------Doctype & Indices methods---------------
  public void setMasterEHForm(AIPForm EInfo){
      aipForm =EInfo;
      FormFields = aipForm.getFields();      
      CurRDTid =aipForm.getMapDTId();  
      
      RDTIndices =MFsD.MDT.getDTIndices(CurRDTid);
      LoadTableData(RDTIndices);
  }
  public void LoadTableData(Vector TData){
    if(TData !=null){
     if(TData.size() > 0){
       for(int i=0;i<TData.size();i++){
           Tabrows.add(getTableRowOfInfo((Index)TData.get(i)));
           TableModel.fireTableRowsInserted(Tabrows.size()-1,Tabrows.size()-1);
       }
     }
    }
  }
  public boolean CheckMapRequiredIndex(){
       for(int i=0;i<TableModel.getRowCount();i++){
           if(((Integer)TableModel.getValueAt(i,REQUIRED)).intValue() > 0){
              String EHfieldvalue =  TableModel.getValueAt(i,EHFIELDCOL).toString();
               if(EHfieldvalue.equals(" ") || EHfieldvalue.equals(""))
               return false;
           }
       }
       return true;
  }
  public void ClearOldEHFiledsMap(){//update
     
     for(int i=0;i<FormFields.size();i++)
     {    
           AIPField f =(AIPField)FormFields.get(i);
           f.setMapIndexId(0);
           f.setFDefault("");
     }    
  }
  public boolean SaveNewMap(){
       ClearOldEHFiledsMap();
       for(int i=0;i<TableModel.getRowCount();i++){          
          int fieldId =((Integer)TableModel.getValueAt(i,EHFIELDIDCOL)).intValue();
          AIPField fld =getFormFieldInfo(fieldId);
          if(fld != null)
          {
              fld.setMapIndexId(((Integer)TableModel.getValueAt(i,VWINDEXIDCOL)).intValue());  
              if(((Integer)TableModel.getValueAt(i,SVID)).intValue() == 1)
                fld.setFDefault("");
              else
                fld.setFDefault(TableModel.getValueAt(i,DEFAULTCOL).toString());
          }    
       }      
       isTableUnsave =false;
       return true;
  }

//--------------------table Inner class------------------------

 private class TableMouseListener extends MouseAdapter  {
      public void mousePressed(MouseEvent e) {
           if (e.getClickCount() == 1){
              int Pointrow =DTtable.rowAtPoint(e.getPoint());
              if(Pointrow > -1)
                  DTtable.setRowSelectionInterval(Pointrow,Pointrow);
              if (e.getModifiers() == e.BUTTON3_MASK) {
              }else if (e.getModifiers() == e.BUTTON1_MASK){

              }
           }
     }
     public void mouseReleased(MouseEvent e) {
     }

  }
  public  class SpecialTableModel extends AbstractTableModel {
       String[] columnNames={"",DTName+" "+AIPUtil.getString("FieldsMapping.TCol.DTField"),FormName+" "+ AIPUtil.getString("FieldsMapping.TCol.FormField"),AIPUtil.getString("FieldsMapping.TCol.DefaultValue")};
       Vector ARow;
       int Srow;
       public SpecialTableModel(){
       }
       public int getColumnCount() {
           return columnNames.length;
       }
       public int getRowCount() {
          return Tabrows.size();
       }
       public String getColumnName(int col) {
         return columnNames[col];
       }
       public Object getValueAt(int aRow, int aColumn) {
          Vector row = (Vector) Tabrows.elementAt(aRow);
          return row.elementAt(aColumn);
       }
       public Class getColumnClass(int c) {
          return getValueAt(0, c).getClass();
       }
       public boolean isCellEditable(int row, int col) {
           if(!isTableEdit) return false;
           return isDTIndicesCellEditable(row,col);
       }
       public void setValueAt(Object value, int row, int col) {
              Vector row1 = (Vector) Tabrows.elementAt(row);
              row1 = (Vector) Tabrows.elementAt(row);
              if(value ==null) return;
              if(value.toString().equals(row1.elementAt(col).toString())) return;
              row1.remove(col);
              row1.add(col,value);
              row1.remove(STATE);
              row1.add(STATE,new Integer(2));//State = update
              row1.remove(col);
              row1.add(col,value);
              if(col == DEFAULTCOL){
                if(!isTableUnsave && !isAllColumnEmpty(EHFIELDCOL) ){
                   isTableUnsave =true;
                   MFsD.Savebtn.setEnabled(true);
                }

              }else
              if(col == EHFIELDCOL ){
                if(!isTableUnsave){
                   isTableUnsave =true;
                   MFsD.Savebtn.setEnabled(true);
                }
                if(value.toString().equals("")){
                    if(isAllColumnEmpty(col)){
                     MFsD.Savebtn.setEnabled(false);
                    }
                }

              }
       }
       public boolean isAllColumnEmpty(int col){
              for(int i=0;i <Tabrows.size();i++){
                 if(!getValueAt(i,col).toString().equals("") ) return false;
              }
              return true;
       }
       public void RemoveRow(int Rnum){
          int Order =((Integer)getValueAt(Rnum,ORDER)).intValue();
          AIPField Info= (AIPField)FormFields.get(Order);
          Info.setStatus(3);//Delete
          Tabrows.remove(Rnum);
          fireTableRowsDeleted(Rnum,Rnum);
       }
       public void RemoveAllRows(){
              int lastrow =Tabrows.size();
              if(lastrow >0){
                Tabrows.removeAllElements();
                this.fireTableRowsDeleted(0,lastrow-1);
              }
       }
       public boolean isDTIndicesCellEditable(int row, int col) {
             ARow = (Vector) Tabrows.elementAt(row);
             Srow =row;
            TableColumn column;
            JComboBox EditCombo =new JComboBox();
            if(col == MARGINCOL) return false;
            if(col == VWINDEXCOL) return false;
            if(col == EHFIELDCOL){
                column = DTtable.getColumnModel().getColumn(EHFIELDCOL);
                Vector IndexName=new Vector();
                IndexName.add(0,"");
                if(FormFields != null){
                    for(int i=0;i<FormFields.size();i++){
                          IndexName.add(i+1,((AIPField)FormFields.get(i)).toString());
                    }
                }
                EditCombo =new JComboBox(IndexName);
                EditCombo.addActionListener(new ActionListener(){
                    public void actionPerformed(ActionEvent e) {
                         int Sindx = ((JComboBox)e.getSource()).getSelectedIndex() ;
                         setValueAt(new Integer(Sindx -1), Srow,ORDER);
                          if(Sindx >0){
                               int  EHFieldType= ((AIPField)FormFields.get(Sindx-1)).getType();
                               int  IndexType= ((Index)RDTIndices.get(Srow)).getType();
                               int  EHFieldid =((AIPField)FormFields.get(Sindx-1)).getId();

                              if(IndexType ==1 && IndexType != EHFieldType){
                                  JOptionPane.showMessageDialog(DTtable,AIPUtil.getString("FieldsMapping.Msg.TypeMismatch"),AIPUtil.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);
                                  setValueAt("", Srow,EHFIELDCOL);
                                  setValueAt(new Integer(-1), Srow,ORDER);
                                  setValueAt(new Integer(0), Srow,EHFIELDIDCOL);
                                  return;
                              }
                              /*else if(IndexType ==2 && EHFieldType == 1){
                                  JOptionPane.showMessageDialog(DTtable,AIPUtil.getString("FieldsMapping.Msg.TypeMismatch"),AIPUtil.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);
                                  setValueAt("", Srow,EHFIELDCOL);
                                  setValueAt(new Integer(-1), Srow,ORDER);
                                  setValueAt(new Integer(0), Srow,EHFIELDIDCOL);
                                  return;
                              }*/
                              setValueAt(new Integer(EHFieldid), Srow,EHFIELDIDCOL);

                          }else {
                              setValueAt(new Integer(-1), Srow,ORDER);
                              setValueAt(new Integer(0), Srow,EHFIELDIDCOL);
                          }
                    }
                });
                column.setCellEditor(new DefaultCellEditor(EditCombo));
                return true;
            }
            if(col == DEFAULTCOL){
                column = DTtable.getColumnModel().getColumn(DEFAULTCOL);
                Index rdtIndex =(Index)RDTIndices.get(Srow);
                int  IndexType= rdtIndex.getType();
                if( IndexType == 0 || IndexType == 5){
                       if(getValueAt(Srow,MASKCOL).toString() == "" || getValueAt(Srow,MASKCOL).toString() == " ")
                         column.setCellEditor(new DefaultCellEditor(new JTextField(" ")));
                       else{
                         JMaskedTextField MaskField =new JMaskedTextField();
                         MaskField.setDatatype(MaskField.texttype);
                         MaskField.setMask(getValueAt(Srow,MASKCOL).toString());
                         column.setCellEditor(new DefaultCellEditor(MaskField));
                       }
                }if( IndexType == 1){
                        NumericField MaskField = new NumericField(0,9);
                        column.setCellEditor(new DefaultCellEditor(MaskField));

                }else if( IndexType == 2){
                     // System.out.println(((DoctypeIndicesInfo)RDTIndices.get(Srow)).getSvid());
                     // if(((DoctypeIndicesInfo)RDTIndices.get(Srow)).getSvid() == 1){
                       //   return false;
                      //}
                      VWDateComboBox EditDateCombo = new VWDateComboBox();
                      EditDateCombo.setDateFormat(getValueAt(row,MASKCOL).toString());
                      column.setCellEditor(new DefaultCellEditor(EditDateCombo));
                } else if(IndexType == 3){
                      String[] EComboData ={"Yes","No"};
                      EditCombo =new JComboBox(EComboData);
                      column.setCellEditor(new DefaultCellEditor(EditCombo));
                } else if(IndexType == 4){
                    Vector  selectData =rdtIndex.getDef();
                     EditCombo =new JComboBox(selectData);
                     column.setCellEditor(new DefaultCellEditor(EditCombo));
                      
                }

                return true;
            }
            return true;
       }

   }
   private class ButtonRenderer extends JButton
                        implements TableCellRenderer {
        ImageManager IM =new ImageManager();
        public ButtonRenderer() {
            super();
            //this.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
        }
        public Component getTableCellRendererComponent(
                                JTable table, Object color,
                                boolean isSelected, boolean hasFocus,
                               int row, int column) {
           if(((Integer)TableModel.getValueAt(row,KEYCOL)).intValue() >0 ){

                setIcon(IM.getImageIcon("keyicon.gif"));
                setToolTipText("Key Field");
           }else{
                setIcon(null);
                setToolTipText("");
           }
           if(column==VWINDEXCOL &&((Integer)TableModel.getValueAt(row,REQUIRED)).intValue() >0 ){
           }
          return this;
        }
   }
   class RowRenderer extends DefaultTableCellRenderer {
       public RowRenderer(){
          super();
       }
       public Component getTableCellRendererComponent(
            JTable table, Object value,
            boolean isSelected, boolean hasFocus,
            int row, int column) {
            if(column==VWINDEXCOL &&((Integer)TableModel.getValueAt(row,REQUIRED)).intValue() >0 ){
               setForeground(Color.red);
            }else  setForeground(Color.black);
            if(column == DEFAULTCOL){
               String mask = TableModel.getValueAt(row,MASKCOL).toString();
               if(!mask.equals("") || !mask.equals(" "))
                   setToolTipText(mask);
               else setToolTipText(null);
            }
            return   super.getTableCellRendererComponent(table,value
                        ,isSelected,hasFocus,row,column);

       }
  }
   private class ButtonEditor extends DefaultCellEditor {
       public ButtonEditor() {
                super(new JCheckBox());
            JButton b =new JButton();
            editorComponent = b;
            setClickCountToStart(1);
            b.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    fireEditingStopped();
                }
            });
       }
       protected void fireEditingStopped() {
            super.fireEditingStopped();
       }

       public Object getCellEditorValue() {
            return null;
       }
       public Component getTableCellEditorComponent(JTable table,
                                                     Object value,
                                                     boolean isSelected,
                                                     int row,
                                                    int column) {
          return editorComponent;
       }
   }
}