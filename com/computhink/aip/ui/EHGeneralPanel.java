package com.computhink.aip.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileFilter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.TimerTask;
import java.util.TreeMap;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.computhink.aip.AIPCurrentUserPreferences;
import com.computhink.aip.AIPManager;
import com.computhink.aip.AIPPreferences;
import com.computhink.aip.CustomFileManager;
import com.computhink.aip.DoctypesManager;
import com.computhink.aip.DocumentsManager;
import com.computhink.aip.EyesFileManager;
import com.computhink.aip.EyesHandsManager;
import com.computhink.aip.PagesManager;
import com.computhink.aip.Session;
import com.computhink.aip.client.AIPDocument;
import com.computhink.aip.client.AIPField;
import com.computhink.aip.client.AIPFile;
import com.computhink.aip.client.AIPForm;
import com.computhink.aip.client.AIPInit;
import com.computhink.aip.resource.ResourceManager;
import com.computhink.aip.resource.images.ImageManager;
import com.computhink.aip.util.AIPUtil;
import com.computhink.aip.util.FoldersMonitoring;
import com.computhink.aip.util.LocationStatus;
import com.computhink.aip.util.MonitoringListener;
import com.computhink.aip.util.MutableList;
import com.computhink.aip.util.MutableLocationThread;
import com.computhink.aip.util.SystemExplorer;
import com.computhink.aip.util.UserPassPane;
import com.computhink.aip.util.VWLogger;
import com.computhink.common.Constants;
import com.computhink.common.DocType;
import com.computhink.common.Document;
import com.computhink.common.VWEvent;
import com.computhink.common.ViewWiseErrors;


/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public class EHGeneralPanel extends JPanel {

	java.util.Timer firstTimer =null;
	/**CV10.2 - Process monitoring thread has added to monitor the AIP document process. Added by Vanitha S**/
	java.util.Timer processPendingDocTimer =null;
	java.util.Timer processMonitoringTimer =null;
	java.util.Timer imageBackupMonitoringTimer =null;
	java.util.Timer idleTimer =null;
	boolean isProcessRestarted = false;

	//resource manager used to get static strings used in interface or other places
	private final ResourceManager connectorManager = ResourceManager.getDefaultManager();  
	private  String languageLocale = ResourceManager.getDefaultManager().getLocale().toString();  
	public JPanel TreesPane;
	//public  VWNodesNavigator VWNodes;
	public SystemExplorer SysExp;
	private EyesHandsManager EHManager =new EyesHandsManager();
	private DocumentsManager DocsManager =new DocumentsManager(); 
	private DoctypesManager DTManager = new DoctypesManager();

	private EHMainPanel EHD ;
	private JProgressBar progressBar;
	private JLabel CurrState, locPol, lmin, lPollingInfo, lLogLevel, lGrayLine, lWhiteLine;
	private Vector VWRooms ;
	private Vector EHForms;
	private Vector VWDoctypes;
	private SwingWorker worker;
	private boolean ProcessStop =true;
	private JComboBox RoomCombo;
	private boolean isUpdatingCombo =false;
	private Vector workingFolders;
	// private JButton VWNodeSettings;
	public JButton Startbtn;
	//private JButton Startbtn;
	private JButton Hidebtn;
	private JButton Refbtn;
	private JButton Hlpbtn;
	private JButton Clsbtn ;
	public JButton conbtn;
	private JRadioButton rbAuto,rbPoll, rbScheduler,retainFormat,rconvertJPEG,rconvertDJVU,rconvertHCPDF;
		
	public static int WIDTH = 80;
	public static int HEIGHT = 20;
	public static int LEFT = 10;
	public static int TOP = 0;

	public static long pollTime = 0;
	
	// Polling Modes : 0 - Automatic, 1 - Every, 2 - Daily At
	public static final int AUTO = 0;
	public static final int EVERY = 1;
	public static final int DAILY_AT = 2;
	
	public static final int RetainFormat = 0;
	public static final int RConvertJPEG = 2;
	public static final int RConvertDJVU = 1;
	public static final int RConvertHCPDF = 3;
	//Flag added to check the network failure 
	public static boolean serverfailure=false;
	//private JButton conbtn;

	private final int progressBarMax = 100000;
	private int progressBarValue;
	private ButtonListener btnListener = new ButtonListener();
	private JCheckBox templateBox;
	private ImageManager IM = new ImageManager();
	private Vector refreashSidNodes ;

	private InputFolderController inputFolderController;
	MutableList inputFolderList;
	private JSpinner sLogLevel,sPollTime, sScheduler,sCompression;
	//private JCheckBox cbConvDJVU;
	// Issue - 742
	private JCheckBox cUseJHPages ;  
	// Issue 742
	public static Hashtable listTable = null;
	//Code added by Srikanth on 01 Dec 2016
	public static int StartFlag = 0; 
	private JCheckBox processSubDirsBox;
	/**CV10.2 - Process monitoring thread has added to monitor the AIP document process. Added by Vanitha S**/
	ProcessMonitoringThread processMonitoringThread = null;
	private DocumentProcessStatus processingDocDetails = null;
	private DocumentProcessStatus processMonitoringDetails = null;
	//boolean enableConverToDJVU = false;
	private JCheckBox singlePageHcpdf = null; 
 	
	EHGeneralPanel(){

	}
	public EHGeneralPanel(EHMainPanel EHPanel) {
		EHD =EHPanel;
		setBorder(new EmptyBorder(5,5,5,5));
		JPanel Panel1 =new JPanel();
		this.add(Panel1,BorderLayout.CENTER);
		Panel1.setMinimumSize(new Dimension(200,100));
		//Panel1.setPreferredSize(new Dimension(550,350));
		//Panel1.setPreferredSize(new Dimension(655,500));
		Panel1.setPreferredSize(new Dimension(685,520));
		Panel1.setLayout(new BoxLayout(Panel1,BoxLayout.Y_AXIS));
		//Panel1.setBorder(new CompoundBorder(new TitledBorder(null,null,
				//TitledBorder.LEFT, TitledBorder.TOP),new EmptyBorder(5,8,5,5)));

		JPanel roomPane = CreateRoomPane();
		Panel1.add(roomPane);

		TreesPane =new JPanel();
		TreesPane.setLayout(new BoxLayout(TreesPane,BoxLayout.X_AXIS));
		VWLogger.debug("Get System Folder Tree");
		SysExp =new SystemExplorer();
		SysExp.setTitle(connectorManager.getString("General.Lab.SysExplorerTitle"));
		/* modified by amaleh on 2003-09-29 from x=250 to x=245 */
		SysExp.setMaximumSize(new Dimension(295+16,190));
		SysExp.setPreferredSize(new Dimension(295+16,190));
		SysExp.setMinimumSize(new Dimension(295+16,185));
		/**/

		TreesPane.add(SysExp,0);

		TreesPane.add(Box.createHorizontalStrut(5),1);
		/* modified by amaleh on 2003-09-26 */
		inputFolderList = new MutableList();
		inputFolderList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		inputFolderController = new InputFolderController(SysExp,inputFolderList);
		
		inputFolderController.setMaximumSize(new Dimension(28+33,185));
		inputFolderController.setPreferredSize(new Dimension(28+33,185));
		inputFolderController.setMinimumSize(new Dimension(28+33,170));
		TreesPane.add(inputFolderController,2);
		TreesPane.add(Box.createHorizontalStrut(1),3);
	
		JPanel inputFolderListPanel = new JPanel();
		JLabel inputFolderListLabel = new JLabel(connectorManager.getString("General.Lab.InputFolderListTitle"));
		inputFolderListPanel.setLayout(new BorderLayout(0,5));
		inputFolderListPanel.add(inputFolderListLabel,BorderLayout.NORTH);
		JScrollPane inputFolderListScrollPane = new JScrollPane(inputFolderList, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		inputFolderListPanel.add(inputFolderListScrollPane,BorderLayout.CENTER);
		/* modified by amaleh on 2003-09-29 from x=250 to x=245 */
		//inputFolderListPanel.setBackground(Color.red);
		//293+30
		inputFolderListPanel.setMaximumSize(new Dimension(292+15,190));
		inputFolderListPanel.setPreferredSize(new Dimension(292+15,190));
		inputFolderListPanel.setMinimumSize(new Dimension(292+15,185));
		listTable = new Hashtable();
		//update Ayman 24-07
		inputFolderList.addListSelectionListener(new ListSelectionListener(){
			public void valueChanged(ListSelectionEvent e)
			{

				if (!e.getValueIsAdjusting()) return;
				String loc = (String)inputFolderList.getSelectedValue();				
				LocationStatus locStatus =AIPManager.getLocationStatus(AIPManager.getCurrSid(),loc); 
				int locationPolling = locStatus.getPolling();
				if(!Startbtn.getText().equals(connectorManager.getString("General.Btn.Start")))
				{  
					if(locStatus !=null)
					{                               
						updateProgressBar(locStatus);
						updateStatusText(locStatus);                     
					}					
				}
				else   
				{					
					if(locStatus !=null && locStatus.isEnabledProcess())
					{
						if(locationPolling != 0)
							sPollTime.setValue(locationPolling);
						else
							sPollTime.setValue(new Integer(1));
					}					 
				} 
				int locNo =inputFolderList.getSelectedIndex()+1;
				if(locNo > 0)          
					locPol.setText(connectorManager.getString("General.Lab.Polling"));  
				else  locPol.setText(connectorManager.getString("General.Lab.Polling"));  				



			}    
		});  
		inputFolderList.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if((e.getModifiers()& InputEvent.BUTTON1_MASK)
						== InputEvent.BUTTON1_MASK )
				{  
					if(ProcessStop)
					{ 
						
						if(!Startbtn.getText().equals(connectorManager.getString("General.Btn.Start"))) return;
						String loc = (String)inputFolderList.getSelectedValue();
						int indx =inputFolderList.getSelectedIndex();
						LocationStatus locStatus =AIPManager.getLocationStatus(AIPManager.getCurrSid(),loc);
						if(locStatus !=null)
						{   
							locStatus.setEnabledProcess(!locStatus.isEnabledProcess());
							if(EyesHandsManager.processSubdirs &&  !locStatus.isEnabledProcess())
							{
								locStatus.setStatus(locStatus.STOP);
								locStatus.setProgressVal(0);
								locStatus.setProgressStr("");            
								locStatus.setStatusText(connectorManager.getString("General.Lab.ProcessStopped"));
							}
							if(AIPManager.getPollinMode() == EVERY){
								//For new processing locations
								int poll =((Integer)sPollTime.getValue()).intValue();
								locStatus.setPolling(poll);
							}
							if(indx > -1) inputFolderList.repaint(indx);
							updateEnabledFolders(AIPManager.getCurrSid());
						}
					}    
				}
			}          
		});
		/**/
		TreesPane.add(inputFolderListPanel,4);
		//commented by amaleh on 2004-02-12
		//TreesPane.add(VWNodes,2);
		/**/
		TreesPane.setAlignmentX(Component.LEFT_ALIGNMENT);

		JPanel PollPane =new JPanel();
		PollPane.setLayout(new BoxLayout(PollPane,BoxLayout.X_AXIS));
		PollPane.setAlignmentX(Component.LEFT_ALIGNMENT);

		JPanel PollingPane =new JPanel();
		PollingPane.setLayout(new BoxLayout(PollingPane,BoxLayout.X_AXIS));
		PollingPane.setAlignmentX(Component.LEFT_ALIGNMENT);

		JPanel LogPane =new JPanel();     
		LogPane.setLayout(new BoxLayout(LogPane,BoxLayout.X_AXIS));
		LogPane.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		JPanel DjvuPane=new JPanel();
		DjvuPane.setLayout(new BoxLayout(DjvuPane,BoxLayout.X_AXIS));
		DjvuPane.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		

		templateBox = new JCheckBox(connectorManager.getString("General.Lab.TempBox"),true);

		//when checkbox is checked, template support is disabled.
		templateBox.addItemListener(new ItemListener()
		{
			public void itemStateChanged(ItemEvent e)
			{
				if (e.getStateChange() == ItemEvent.SELECTED)
				{
					//disable templates panel
					EHD.TabPane.setEnabledAt(1, false);

					EyesHandsManager.tempDisabled = true;
				}
				else
				{
					//enable templates panel
					EHD.TabPane.setEnabledAt(1, true);                 
					EyesHandsManager.tempDisabled = false;
				}

			}
		});

		/*  Issue - 742
		 *   Developer - Nebu Alex 
		 *   Code Description - The following piece of code implements
		 *                      Generate primitive document functionalities 
		 */
		cUseJHPages =new JCheckBox(connectorManager.getString("General.Lab.GeneratePrimitiveDoc")); 		
		cUseJHPages.setAlignmentX(Component.LEFT_ALIGNMENT);  
		int usejhp =AIPManager.isPrimitiveDocs();
		boolean selection = (usejhp == 0)? false :true;
		cUseJHPages.setSelected(selection);
		cUseJHPages.addItemListener(new ItemListener()
		{
			public void itemStateChanged(ItemEvent e)
			{
				VWLogger.info("inside itemStateChanged listener.......");
				if (e.getStateChange() == ItemEvent.SELECTED)
				{
					// Comment the validation; if primitive option and DJVU option selected it will call the Dll call
					// if primitive option only selected it will call the java call for encript and compressed the page.

					/*if(AIPManager.isConvertToDJVU()==1){
			            	 cUseJHPages.setSelected(false);
			            	 return;
			                 }*/					
					AIPManager.setPrimitiveDocs(1);
				}
				else
				{	
					AIPManager.setPrimitiveDocs(0);
				}

			}
		});

		//End of Issue - 742     
		//cbConvDJVU = new JCheckBox();
		
		PagesManager.nConvertToDJVU =AIPManager.isConvertToDJVU();
		// set Retain INF Files checkbox according to how it is saved in the database.
		boolean bConvDJVU = (PagesManager.nConvertToDJVU == 0)? false :true;    
		//cmbConvJPEGDJVU.setSelected(bConvDJVU);

		// add listener for checking the box
		/*cbConvDJVU.addItemListener(new ItemListener()
		{
			public void itemStateChanged(ItemEvent e)
			{
				if (e.getStateChange() == ItemEvent.SELECTED)
				{
					// Comment the validation; if primitive option and DJVU option selected it will call the Dll call
					// if primitive option only selected it will call the java call for encript and compressed the page.
					            	 if(AIPManager.isPrimitiveDocs()==1){
            		 cbConvDJVU.setSelected(false);
	            	 return;
	             }
					//PagesManager.nConvertToDJVU = 1;
					//AIPManager.setConvertToDJVU(1);
					//sCompression.setEnabled(true);
					//AIPManager.setEnableConversion(true);
				}
				else
				{
					//PagesManager.nConvertToDJVU = 0;
					//AIPManager.setConvertToDJVU(0);
					//sCompression.setEnabled(false);
					//AIPManager.setEnableConversion(false);
				}

			}
		});*/

		JCheckBox cLog =new JCheckBox(" " + connectorManager.getString("General.Lab.EnableLog"));
		cLog.setSelected(AIPManager.isEnableLog()); 
		cLog.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				if(((JCheckBox)e.getSource()).isSelected())
				{                   
					AIPManager.setEnableLog(true);
					AIPManager.initLogger();                
				}    
				else
				{                   
					AIPManager.setEnableLog(false);
					AIPManager.stopLogger();
				}    
			}
		});

		JLabel lLogLevel =new JLabel(connectorManager.getString("General.Lab.LogLevel") + " ");

		SpinnerNumberModel sModel = new SpinnerNumberModel(1,1, 10, 1);         
		sLogLevel =new JSpinner(sModel); 
	
		JSpinner.DefaultEditor editor = (JSpinner.DefaultEditor)sLogLevel.getEditor();       
		JTextField spfield  = (JTextField)editor.getTextField();
		
		
		JLabel lCompression =new JLabel(connectorManager.getString("General.Lab.Compression") + " ");
		SpinnerNumberModel spcModel = new SpinnerNumberModel(2,2, 255, 1); 
		sCompression=new JSpinner(spcModel);
		JSpinner.DefaultEditor editor1 = (JSpinner.DefaultEditor)sCompression.getEditor();
		JTextField spfield1 = (JTextField)editor1.getTextField();
		spfield1.setBorder(BorderFactory.createEmptyBorder());  
		sCompression.setMaximumSize(new Dimension(35,22));
		sCompression.setPreferredSize(new Dimension(35,22));
		VWLogger.info("Compression.ToolTipText >>>> "+connectorManager.getString("Compression.ToolTipText"));
		sCompression.setToolTipText(connectorManager.getString("Compression.ToolTipText"));
		spfield.setBorder(BorderFactory.createEmptyBorder());  
		sLogLevel.setMaximumSize(new Dimension(35,22));
		sLogLevel.setPreferredSize(new Dimension(35,22));

		sLogLevel.setValue(new Integer(AIPManager.getLogLevel()));
		sLogLevel.addChangeListener(new ChangeListener(){
			public void stateChanged(ChangeEvent e)
			{
				String newVal = ((JSpinner)e.getSource()).getValue().toString();
				int iVal =AIPUtil.to_Number(newVal);
				if(iVal >= 1 && iVal <= 10)
				{
					AIPManager.setLogLevel(iVal);
				}    
			} 
		});    
		sCompression.setValue(new Integer(AIPManager.getCompression()));
		sCompression.addChangeListener(new ChangeListener(){
			public void stateChanged(ChangeEvent e)
			{
				String newVal = ((JSpinner)e.getSource()).getValue().toString();
				int iVal =AIPUtil.to_Number(newVal);
				if(iVal >= 2 && iVal <= 255)
				{
					AIPManager.setCompression(iVal);
				}    
			} 
		});  
		
		
		
		
		//////// Add the sub directory option
		processSubDirsBox = new JCheckBox(connectorManager.getString("General.Lab.ProcessSubDir"));
		EyesHandsManager.processSubdirs =AIPManager.isProcessSubdirs();
		// set Retain INF Files checkbox according to how it is saved in the database.
		processSubDirsBox.setSelected(EyesHandsManager.processSubdirs);

		// add listener for checking the box
		processSubDirsBox.addItemListener(new ItemListener()
		{
			public void itemStateChanged(ItemEvent e)
			{
				if (e.getStateChange() == ItemEvent.SELECTED)
				{
					int size =inputFolderList.getContents().size();
					boolean[] delLocs =new boolean[size];                 
					boolean findloc =false;
					for(int i=0;i< size;i++)
					{                     
						String loc =(String)inputFolderList.getContents().get(i); 
						LocationStatus locStatus = 
							AIPManager.getLocationStatus(AIPManager.getCurrSid(),loc);
						if(!locStatus.isEnabledProcess())continue;

						for(int j=i+1;j<size;j++)
						{
							String comploc =(String)inputFolderList.getContents().get(j); 
							LocationStatus complocStatus = 
								AIPManager.getLocationStatus(AIPManager.getCurrSid(),comploc);
							if(!complocStatus.isEnabledProcess())continue;
							if(comploc.length() > loc.length() && comploc.indexOf(loc) > -1 && comploc.charAt(loc.length())== '\\')
							{
								delLocs[j] =true; 
								findloc =true;                                                                
							}   
							else
								if(comploc.length() < loc.length() && loc.indexOf(comploc) > -1 && loc.charAt(comploc.length())== '\\')
								{  
									delLocs[i] =true; 
									findloc =true;
									break;
								}    
						}
					}        
					if(findloc)
					{
						int op =JOptionPane.showConfirmDialog(EHGeneralPanel.this,
								connectorManager.getString("EHGeneralPanel.Location"),
								connectorManager.getString("Init.Msg.Title"),
								JOptionPane.OK_CANCEL_OPTION);
						if(op == JOptionPane.OK_OPTION)
						{    
							for(int k=size-1;k>=0;k--)
							{
								if(delLocs[k])
								{
									String loc =(String)inputFolderList.getContents().get(k);
									LocationStatus locStatus =AIPManager.getLocationStatus(AIPManager.getCurrSid(),loc);
									if(locStatus !=null && locStatus.isEnabledProcess())
									{   
										locStatus.setEnabledProcess(!locStatus.isEnabledProcess());                                   
										locStatus.setStatus(locStatus.STOP);
										locStatus.setProgressVal(0);
										locStatus.setProgressStr("");            
										locStatus.setStatusText(connectorManager.getString("General.Lab.ProcessStopped"));
										inputFolderList.repaint(k);
									}     
								}    
							}    

						}
						else
						{
							((JCheckBox)e.getSource()).setSelected(false);
							return ;
						}    
					}    
					EyesHandsManager.processSubdirs = true;
					AIPManager.setProcessSubdirs(true);
				}
				else
				{
					EyesHandsManager.processSubdirs = false;
					AIPManager.setProcessSubdirs(false);
				}

			}
		});			
		//////// End 

		lGrayLine = new JLabel("");
		lGrayLine.setBorder(BorderFactory.createLineBorder(Color.GRAY,1));
		lGrayLine.setMinimumSize(new Dimension(620+30+32, 1));
		lGrayLine.setPreferredSize(new Dimension(620+30+32, 1));
		lGrayLine.setMaximumSize(new Dimension(620+30+32, 1));
		//lGrayLine.setBackground(Color.CYAN);
		
		lWhiteLine = new JLabel("");
		lWhiteLine.setBorder(BorderFactory.createLineBorder(Color.WHITE));
		lWhiteLine.setMinimumSize(new Dimension(620+30+32, 1));
		lWhiteLine.setPreferredSize(new Dimension(620+30+32, 1));
		lWhiteLine.setMaximumSize(new Dimension(620+30+32, 1));
		

		JPanel pollingPanel = CreatePollingPanel();
		PollingPane.add(pollingPanel);
		PollPane.add(processSubDirsBox);
		//PollPane.add(templateBox);
		//templateBox
		// Issue - 742
		PollPane.add(Box.createHorizontalStrut(90+35));
		//17/
		PollPane.add(retainFormat);
		//PollPane.add(cUseJHPages);     
		// Issue - 742
		if(languageLocale.equals("nl_NL")){
			PollPane.add(Box.createHorizontalStrut(50));
		}else
			PollPane.add(Box.createHorizontalStrut(100));
		//PollPane.add(cbConvDJVU);
		//PollPane.add(cmbConvJPEGDJVU);
		//PollPane.add(sCompression);
		PollPane.add(cLog);
	
		//PollPane.setBackground(Color.blue);
		PollPane.add(Box.createHorizontalStrut(62));
		PollPane.add(lLogLevel);
		PollPane.add(sLogLevel);
		//  shift to next line
		//  LogPane.add(Box.createHorizontalStrut(2));
		//17/11/2017
		//LogPane.add(cLog);
		LogPane.add(cUseJHPages);
		//LogPane.setBackground(Color.red);
		LogPane.add(Box.createHorizontalStrut(90+11));
		
		LogPane.add(rconvertJPEG);
		LogPane.add(Box.createHorizontalStrut(90));
		
		LogPane.add(lCompression);
		LogPane.add(Box.createHorizontalStrut(4));
		LogPane.add(sCompression);
		
		//LogPane.add(Box.createHorizontalStrut(70));
		//madhavan
		//LogPane.add(lLogLevel);
		//LogPane.add(sLogLevel);
		if(languageLocale.equals("nl_NL")){
			LogPane.add(Box.createHorizontalStrut(152));
		}else
		LogPane.add(Box.createHorizontalStrut(160));
		
		DjvuPane.add(templateBox);
		DjvuPane.add(Box.createHorizontalStrut(90+40+3));
		
		DjvuPane.add(rconvertDJVU);		
		DjvuPane.add(rconvertHCPDF);
		//if (enableConverToDJVU) {
			rconvertDJVU.setVisible(true);
			rconvertHCPDF.setVisible(false);
		/*} else {
			rconvertDJVU.setVisible(false);
			rconvertHCPDF.setVisible(true);
			DjvuPane.add(Box.createHorizontalStrut(75));			
			//DjvuPane.add(singlePageHcpdf);
			singlePageHcpdf.setSelected(false); 
			if (AIPManager.isSinglePageHcpdf() == 1)
				singlePageHcpdf.setSelected(true); 
			singlePageHcpdf.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					if (((JCheckBox) e.getSource()).isSelected()) {
						AIPManager.setSinglePageHcpdf(1);
					} else {
						AIPManager.setSinglePageHcpdf(0); 
					}
				}
			});
		}*/
		
		//17/11/2017
		//LogPane.add(processSubDirsBox);
		// Shif to next line
		Panel1.add(Box.createVerticalStrut(2));
		Panel1.add(TreesPane);
		Panel1.add(Box.createVerticalStrut(10));
		Panel1.add(PollingPane);
		Panel1.add(Box.createVerticalStrut(5));
		//Panel1.setBorder(BorderFactory.createLineBorder(Color.gray));
		Panel1.add(lGrayLine);
		Panel1.add(lWhiteLine);
		Panel1.add(Box.createVerticalStrut(10));
		Panel1.add(PollPane);
		Panel1.add(Box.createVerticalStrut(2));
		Panel1.add(LogPane);
		Panel1.add(Box.createVerticalStrut(2));
		Panel1.add(DjvuPane);
	
		Panel1.add(Box.createVerticalStrut(10));
		Panel1.add(CreateStatusPanel());
		Panel1.add(Box.createVerticalStrut(10));
		Panel1.add(CreateButtonPanel());
		//LoadGeneralData();
	}


	/**
	 * Desc   :Function isDJVUExeExists() checks weather VW2DjVu.exe exists or not.
	 *         Returns true if file exists, false otherwise
	 * Author :Nishad Nambiar
	 * Date   :15-Mar-2007
	 * @return
	 */
	private boolean isDJVUExeExists(){
		String home = AIPUtil.findHome();
		String exePath = home+AIPUtil.pathSep+AIPUtil.getString("General.DJVU.EXE.Name") ;
		File djvuFile = new File(exePath);
		VWLogger.info("isDJVUExeExists ::"+djvuFile.exists());
		return djvuFile.exists()? true:false; 
	}


	public JPanel CreateRoomPane(){
		JPanel RoomPane =new JPanel();
		RoomPane.setLayout(new BoxLayout(RoomPane,BoxLayout.X_AXIS));
		JLabel RoomName = new JLabel(connectorManager.getString("General.Lab.VWRoom"));
		if(languageLocale.equals("nl_NL")){
			RoomName.setMaximumSize(new Dimension(120,25));
			RoomName.setPreferredSize(new Dimension(120,25));
		}else{
			RoomName.setMaximumSize(new Dimension(96,25));
			RoomName.setPreferredSize(new Dimension(96,25));
		}
		
		RoomName.setMinimumSize(new Dimension(96,25));
		
		
		RoomCombo =new JComboBox(LoadVWRooms());

		//Desc   :Load the startup room.
		//Author :Nishad Nambiar
		//Date   :12-Dec-2007
		if(AIPManager.getDefaultStartupRoom() != null && !("").equals(AIPManager.getDefaultStartupRoom().trim()))
		{
			RoomCombo.setSelectedItem(AIPManager.getDefaultStartupRoom());
		}       
		RoomCombo.setMinimumSize(new Dimension(205,25));
		RoomCombo.setPreferredSize(new Dimension(205,25));
		RoomCombo.setMaximumSize(new Dimension(205,25));

		RoomCombo.setEditable(false);
		RoomCombo.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				RoomCombo_actionPerformed(e);
			}
		});
		//conbtn =new JButton(connectorManager.getString("General.Btn.Connect"));
		conbtn =new JButton();
		conbtn.setIcon(IM.getImageIcon("connect.gif"));
		conbtn.setToolTipText(connectorManager.getString("General.Btn.Connect"));
		conbtn.setMinimumSize(new Dimension(25,25));
		conbtn.setMaximumSize(new Dimension(25,25));
		conbtn.setPreferredSize(new Dimension(25,25));
		conbtn.addActionListener(btnListener);
		
		lPollingInfo=new JLabel("");
		
		//lPollingInfo.setForeground(Color.blue);
		lPollingInfo.setMinimumSize(new Dimension(150,25));
		lPollingInfo.setPreferredSize(new Dimension(150,25));
		lPollingInfo.setMaximumSize(new Dimension(150,25));
		
		RoomPane.add(RoomName);
		RoomPane.add(Box.createHorizontalStrut(10));
		RoomPane.add(RoomCombo);

		RoomPane.add(Box.createHorizontalStrut(20));
		RoomPane.add(conbtn); 
		
		RoomPane.add(Box.createHorizontalStrut(32+15));	
		RoomPane.add(lPollingInfo);

		//RoomPane.add(Box.createHorizontalStrut(8));
		// RoomPane.add(VWNodeSettings);   
		RoomPane.setAlignmentX(Component.LEFT_ALIGNMENT);
		return RoomPane;
	}
	 
	public JPanel CreateStatusPanel(){
		JPanel StatePane =new JPanel();
		StatePane.setLayout(new BoxLayout(StatePane,BoxLayout.Y_AXIS));
		StatePane.setAlignmentX(Component.LEFT_ALIGNMENT);
		JPanel Pan =new JPanel();
		Pan.setLayout(new BoxLayout(Pan,BoxLayout.X_AXIS));
		JLabel State  =new JLabel(connectorManager.getString("General.Lab.Status"));

		State.setFont(new Font(State.getFont().getName(),Font.BOLD,State.getFont().getSize()));
		CurrState =new JLabel(connectorManager.getString("General.Lab.ProcessStopped"));

		Pan.add(State);
		Pan.add(CurrState);
		Pan.setAlignmentX(Component.LEFT_ALIGNMENT);
		progressBar =new JProgressBar(0,progressBarMax) ;
		progressBar.setBorderPainted(true);
		progressBar.setPreferredSize(new Dimension(300,25));
		progressBar.setAlignmentX(Component.LEFT_ALIGNMENT);
		/* added by amaleh on 2004-01-22 */
		//progress bar now displays a string referring to the document currently being processed
		progressBar.setStringPainted(true);
		progressBar.setString("");
		/**/
		StatePane.add(Pan);
		StatePane.add(Box.createVerticalStrut(5));
		StatePane.add(progressBar);

		return StatePane;
	}
	class scheduleProcessPendingDocs extends TimerTask{
		
		public void run() {
			try{
				VWLogger.info("*** run in scheduleProcessPendingDocs ");
				startScheduler();
			}catch (Exception e) {
				VWLogger.error("Exception in scheduleProcessPendingDocs run() method ", e);
			}
		}
		
		public void startScheduler(){
			try{
				VWLogger.info("*** startScheduler to process locked Pending Document Started ***");
				//VWLogger.info("*** Set the lastProcess False in Scheduler");
				ProcessStop =false;
				progressBar.setValue(0);

				boolean ret = processLockedPendingDocs();
				VWLogger.info("Return value from processLockedPendingDocs() :: "+ret);
				//resumeAllProcessLocation(true);
				
				String locName = (String) inputFolderList.getSelectedValue();
				LocationStatus locStatus = AIPManager.getLocationStatus(AIPManager.getCurrSid(), locName);
				if(locStatus != null){
					updateStatusText(locStatus);
				}else{
					CurrState.setText("");
				}
			}catch (Exception e) {
				VWLogger.error("Exception in scheduleProcessPendingDocs startScheduler() method ", e);
			}
		}
		
		// New Method to process locked Pending Documents
		public synchronized  boolean processLockedPendingDocs(){
			try{
				VWLogger.info("Executing processLockedPendingDocs...");
				Vector hFiles = new Vector();
				int startIndex = 0;
				int endIndex = 100;
				int count = 0;

				VWLogger.info("Gettting locked pending documents...");

				AIPManager.getVWC().getAIPHistoryFiles(AIPManager.getCurrSid(),hFiles,startIndex,endIndex,0);
				if(hFiles != null && hFiles.size() > 0)
					count = hFiles.size();
				/**
				 * CV10.1 :- Issue fix to set the current sid for removing orpan session.
				 */
				VWLogger.info("Setting aip connection status during pending process");
				AIPManager.setAIPConnectionStatus(AIPManager.getCurrSid());

				CurrState.setText(connectorManager.getString("EHGeneralPanel.Processinglocked"));
				VWLogger.info("Locked pending documents count : "+count);
				if(count>=0){
					//VWLogger.info("Inside count greater than zero");
					Vector locs =AIPManager.getAllLocationsStatus(AIPManager.getCurrSid());
					VWLogger.info("Location status count : "+locs);
					//VWLogger.info("locs vector ::::"+locs);
					if(locs != null && locs.size() > 0){
						for(int j=0;j<locs.size();j++)
						{
							//VWLogger.info("Inside for loop");
							LocationStatus locStatus =(LocationStatus)locs.get(j);
							VWLogger.info("locStatus.getStatus() : "+locStatus.getStatus());
							if(locStatus.getStatus() == LocationStatus.PROCESS){
								//VWLogger.info("Inside process status true :");
								updateStatusText(locStatus);								
								return false;
							}    
							VWLogger.info("locStatus.get name before startshd :::"+locStatus.getName());
							startSHD(locStatus);							
						}
					}
				}
				
				if(count>=0){
					VWLogger.info("Inside count greater than zero");
					Vector locs =AIPManager.getAllLocationsStatus(AIPManager.getCurrSid());
					VWLogger.info("locs vector ::::"+locs);
					if(locs != null && locs.size() > 0){
						for(int j=0;j<locs.size();j++)
						{
							VWLogger.info("Inside for loop");
							LocationStatus locStatus =(LocationStatus)locs.get(j);
							
							if(locStatus.getStatus() == LocationStatus.PROCESS){
								//processLockedPendingDocs = false;
								updateStatusText(locStatus);

								return false;
							}    
							VWLogger.info("locStatus.get name before startshd :::"+locStatus.getName());
							startSHD(locStatus);
						}
					}
				}
				

				while(count >= 1){
					for (int i = 0; i < hFiles.size(); i++){						
//						boolean processLockedPendingDocs = true;
						Vector locs =AIPManager.getAllLocationsStatus(AIPManager.getCurrSid());
						if(locs != null && locs.size() > 0){
							for(int j=0;j<locs.size();j++)
							{
								LocationStatus locStatus =(LocationStatus)locs.get(j);
								VWLogger.info("locStatus.getStatus() ::"+locStatus.getStatus());
								if(locStatus.getStatus() == LocationStatus.PROCESS){
									VWLogger.info("Inside process status");
									//processLockedPendingDocs = false;
									updateStatusText(locStatus);
									return false;
								}    
							}
						}
						
						CurrState.setText(connectorManager.getString("EHGeneralPanel.Processinglocked"));
						progressBar.setValue(0);
						progressBar.setString("");
						//Vector refreshNodesRooms =new Vector();
						AIPFile aipFile=(AIPFile)hFiles.get(i);

						if(ProcessStop){							
							return true;
						}
						/**
						 * Reprocessing the locked pending documents
						 */
						EHD.PendingPane.PFL.ProcessAIPFile(aipFile, 13);	
						//VWLogger.info("Before calling process aip 5");
						/**
						 * CV10 Plus add the following pend cause 5 & 7
						 * if any network error occurs and to reprocss the pending documents
						 */
						EHD.PendingPane.PFL.ProcessAIPFile(aipFile, 5);
						VWLogger.info("Before calling process aip 7");
						EHD.PendingPane.PFL.ProcessAIPFile(aipFile, 7);
					}

					hFiles.clear();
					startIndex = endIndex;
					endIndex += 100;
					AIPManager.getVWC().getAIPHistoryFiles(AIPManager.getCurrSid(),hFiles,startIndex,endIndex,0);

					if(hFiles != null && hFiles.size() > 0)
						count = hFiles.size();
					else
						count = 0;

					VWLogger.info("Locked pending documents count2 : "+count);
				}
				VWLogger.info("Completed processLockedPendingDocs...");
			}catch (Exception e) {
				VWLogger.error("Exception in processLockedPendingDocs() ", e);
			}
			return false;
		}
		public boolean checkAllProcessLocation()
		{		
			boolean allLocationWait = false;
			try{
				ArrayList locationStatusList = AIPManager.getAllLocationStatus();		    
				for (int index = 0; index < locationStatusList.size(); index++){
					LocationStatus locStatus = (LocationStatus) locationStatusList.get(index); 								             
					if(locStatus != null && locStatus.getStatus() ==locStatus.PROCESS) return false;
					if(locStatus != null && locStatus.getStatus() ==locStatus.WAIT) allLocationWait = true;    
					//LocationThread locThread = (LocationThread)locStatus.getWorkThread();
/*					if( locThread != null){
						locThread.waitUntilAsk();
					}*/					
				}
			}catch(Exception ex){
				//VWLogger.info("processLocation Failed " + ex.getMessage());
				return false;
			}
			return allLocationWait;				
		}
		public boolean resumeAllProcessLocation(boolean init)
		{		
			ArrayList locationStatusList = AIPManager.getAllLocationStatus();		    
			for (int index = 0; index < locationStatusList.size(); index++){
				try{
					LocationStatus locStatus = (LocationStatus) locationStatusList.get(index); 								             

					LocationThread locThread = (LocationThread)locStatus.getWorkThread();
					if( locThread != null){
						locThread.resumeProcess();
					}
				}catch(Exception ex){
					//VWLogger.info("processLocation Failed " + ex.getMessage());
				}				
			}			
			return true;				
		}
		
	}
		
	class scheduleProcess extends TimerTask implements MonitoringListener{
		public void startScheduler(){
			VWLogger.info("*** startScheduler Import Process started ***");
			//VWLogger.info("*** Set the lastProcess False in Scheduler");
			AIPManager.setLastAIPProcessSuccess(false);
			refreshVWNodesTree();
			if(!EyesHandsManager.usejHandlePages)
			{    
				try{
					EHD.PManager =new PagesManager();
				}
				catch (java.lang.UnsatisfiedLinkError e1){

					JOptionPane.showMessageDialog(EHD,connectorManager.getString("Init.Msg.LostLibrary"),connectorManager.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);
					return;

				}
			}

			ProcessStop =false;
			try
			{        
				workingFolders =new Vector();
				Vector autoPollFolders =new Vector();
				Vector invalidFolders =new Vector();
				ArrayList sessionList = getConnectedSessionId();
				//VWLogger.info("sessionList  " + sessionList);
				int connectedSession = sessionList.size();
				for (int index = 0; index < connectedSession; index++){
					int currentSessionId = Integer.parseInt(sessionList.get(index).toString());
					Vector enabledFolders = AIPManager.getEnabledFolders(currentSessionId);
					//VWLogger.info("enabledFolders  " + enabledFolders);
					int size = enabledFolders.size();			    
					//int size =inputFolderList.getContents().size();
					for(int i=0;i< size;i++)
					{
						//String loc =(String)inputFolderList.getContents().get(i);
						String loc = "";
						String eFPath =(String)enabledFolders.get(i);
						//	VWLogger.info("efPath - " + eFPath);
						if (eFPath == null) continue;
						int tabLoc = eFPath.indexOf("\t");
						if( tabLoc>0)
						{
							loc = eFPath.substring(0,tabLoc).trim();
						}else
							continue;

						LocationStatus locStatus = AIPManager.getLocationStatus(loc);						
						if(locStatus != null) 
						{
							locStatus.setStatus(LocationStatus.PROCESS);
							if( locStatus.isEnabledProcess()){
								File locFile =new File(locStatus.getName());	
								try{
									if (locFile.exists() && locFile.isDirectory())
									{    
										File test =new File(loc+File.separator+"test.inv");
										test.createNewFile(); 
										test.delete();                                                
									}        
									else 
									{
										locStatus.setStatusText(connectorManager.getString("EHGeneralPanel.Stopped")+connectorManager.getString("EHGeneralPanel.Stopped")+" ");
										VWLogger.audit("Location("+(i+1)+"):"+"Stopped "+"<Invalid location>");
										invalidFolders.add(loc);
										continue;
									}
								}
								catch(Exception io)
								{ 
									String expMsg ="<"+io.getMessage()+">";
									if(expMsg ==null || expMsg.trim().equals(""))                                           
										expMsg = connectorManager.getString("EHGeneralPanel.Stopped")+" ";
									locStatus.setStatusText(connectorManager.getString("EHGeneralPanel.Stopped")+" "+expMsg);
									VWLogger.audit("Location("+(i+1)+"):"+"Stopped "+expMsg);
									//io.printStackTrace();
									invalidFolders.add(loc);
									continue;
								} 
								workingFolders.add(loc); 
								if(locStatus.getPolling() == 0)
									autoPollFolders.add(loc);
							}
						}				
					}			
				}				
				if( workingFolders.size() == 0) 
				{ 
					String msg =connectorManager.getString("General.Msg.NoSelectProcessLocation");
					if(invalidFolders.size() > 0)
						msg = connectorManager.getString("EHGeneralPanel.processinglocations");
					JOptionPane.showMessageDialog(EHGeneralPanel.this,msg,connectorManager.getString("Init.Msg.Title"),JOptionPane.WARNING_MESSAGE);              
					VWLogger.audit(msg);
					VWLogger.info("*** Import Process Ended ***");
					ProcessStop =true;
					return  ;                     
				}
				if(autoPollFolders.size() > 0)
				{    
					FoldersMonitoring.addMonitoringListener(this);                  
					boolean st = FoldersMonitoring.startMonitorFolders(autoPollFolders);

					if(st)
						VWLogger.debug("start location  monitors");
					else{
						JOptionPane.showMessageDialog(EHD,"Cannot start location monitors ",connectorManager.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);
						ProcessStop =true;
						return;
					}    
				}     
			}
			catch (java.lang.Error er)
			{
				JOptionPane.showMessageDialog(EHD,connectorManager.getString("EHGeneralPanel.CannotStart.LocationMonitor")+" ",connectorManager.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);
				ProcessStop =true;
				return;  
			}
			catch (Exception ex)
			{
				JOptionPane.showMessageDialog(EHD,connectorManager.getString("EHGeneralPanel.CannotStart.LocationMonitor")+" ",connectorManager.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);
				ProcessStop =true;
				return;  
			}

			Startbtn.setText(connectorManager.getString("General.Btn.Stop"));
			EHD.setAllEnabled(false,2); 
			progressBar.setValue(0);
			progressBar.setString(""); 
			worker =new SwingWorker(){
				public Object construct() {
					VWLogger.info("Before calling processLocation......from scheduleProcess");
					processLocation(true);
					return "";
				}
			};	

		}
		public boolean processLocation(boolean init)
		{		
			VWLogger.info("calling from processLocation 1......");
			try{
				ArrayList locationStatusList = AIPManager.getAllLocationStatus();		    
				for (int index = 0; index < locationStatusList.size(); index++){
					LocationStatus locStatus = (LocationStatus) locationStatusList.get(index); 			
					if(locStatus == null || !locStatus.isEnabledProcess()) return true;             
					if(!init && locStatus.getStatus() ==locStatus.PROCESS) return true;
					if(init) locStatus.setStatus(locStatus.WAIT);            
					else if(locStatus.getPolling() ==0) {			    
						try{
							Thread.currentThread().sleep(300);
						}
						catch(Exception e)
						{
							VWLogger.error("Error while processor wait thread for loc :"+locStatus.getName(),e);
							return false;
						}         
					}    
					LocationThread locThread = (LocationThread)locStatus.getWorkThread();
					if( locThread != null){
						locThread.resumeProcess();
					}
					else
					{
						locThread =new LocationThread(locStatus);			    
						locThread.start();
					}    
					AIPManager.setLastAIPProcessSuccess(true);
				}
			}catch(Exception ex){
				//VWLogger.info("processLocation Failed " + ex.getMessage());
			}
			return true;				
		}

		public boolean processLocation(String location,boolean init)
		{	
			VWLogger.info("calling from processLocation 3......");
			LocationStatus locStatus = AIPManager.getLocationStatus(location); 

			if(locStatus == null || !locStatus.isEnabledProcess()) return true;             
			if(!init && locStatus.getStatus() ==locStatus.PROCESS) return true;
			if(init) locStatus.setStatus(locStatus.WAIT);            
			else
				if(locStatus.getPolling() ==0)    
				{                
					try{
						Thread.currentThread().sleep(300);
					}
					catch(Exception e)
					{
						VWLogger.error("Error while processor wait thread for loc :"+locStatus.getName(),e);
						return false;
					}         
				}    
			//VWLogger.debug("Scheduler Start thread for processing location <"+locStatus.getName()+"> Init <"+init+">");
			//VWLogger.debug("Scheduler Start locStatus <"+locStatus.getStatus()+"> <"+locStatus.getStatusText()+">");
			//VWLogger.debug("Scheduler Start locStatus getPolling <"+locStatus.getPolling()+"> ");
			LocationThread locThread = (LocationThread)locStatus.getWorkThread();
			if( locThread != null){        	
				locThread.resumeProcess();
			}
			else
			{
				locThread =new LocationThread(locStatus);
				//VWLogger.debug("locThread Started " + locStatus);
				locThread.start();
			}    
			return true;
		}
		public void run(){
			startScheduler();
			//VWLogger.debug("Complete the startScheduler");
			FoldersMonitoring.stopMonitoring();
		}
	}
	
	public JPanel CreateButtonPanel(){
		JPanel BtnPanel =new JPanel();
		BtnPanel.setLayout(new BoxLayout(BtnPanel, BoxLayout.X_AXIS));


		Startbtn    =new JButton(connectorManager.getString("General.Btn.Start"));
		Refbtn      =new JButton(connectorManager.getString("General.Btn.Refresh"));
		Hlpbtn      =new JButton(connectorManager.getString("General.Btn.Help"));
		Clsbtn      =new JButton(connectorManager.getString("General.Btn.Close"));

		Startbtn.setMnemonic(connectorManager.getString("General.MBtn.Start").trim().charAt(0));
		Refbtn .setMnemonic(connectorManager.getString("General.MBtn.Refresh").trim().charAt(0));
		Hlpbtn .setMnemonic(connectorManager.getString("General.MBtn.Help").trim().charAt(0));
		Clsbtn.setMnemonic(connectorManager.getString("General.MBtn.Close").trim().charAt(0));

		Startbtn.addActionListener(btnListener);
		Refbtn .addActionListener(btnListener);
		Hlpbtn .addActionListener(btnListener);
		Clsbtn.addActionListener(btnListener);
		if(languageLocale.equals("nl_NL")){
			Startbtn.setPreferredSize(new Dimension(100,25));	
		}else
		Startbtn.setPreferredSize(new Dimension(80,25));
		Startbtn.setMinimumSize(new Dimension(80,25));
		if(languageLocale.equals("nl_NL")){
		Refbtn.setPreferredSize(new Dimension(100,25));
		}else
			Refbtn.setPreferredSize(new Dimension(80,25));
		Refbtn.setMinimumSize(new Dimension(80,25));
		if(languageLocale.equals("nl_NL")){
		Hlpbtn.setPreferredSize(new Dimension(100,25));
		}else
			Hlpbtn.setPreferredSize(new Dimension(80,25));
		Hlpbtn.setMinimumSize(new Dimension(80,25));
		if(languageLocale.equals("nl_NL")){
		Clsbtn.setMinimumSize(new Dimension(100,25));
		}else
			Clsbtn.setMinimumSize(new Dimension(80,25));	
		
		Clsbtn.setPreferredSize(new Dimension(80,25));

		Startbtn.setEnabled(true);
		Refbtn.setEnabled(true);
		Hlpbtn.setEnabled(true);
		Clsbtn.setEnabled(true);

		BtnPanel.add(Box.createHorizontalGlue());
		BtnPanel.add(Startbtn);
		BtnPanel.add(Refbtn);
		BtnPanel.add(Hlpbtn);
		BtnPanel.add(Clsbtn);

		BtnPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		return BtnPanel;

	}  

	private JPanel CreatePollingPanel()
	{
		JPanel pollingPanel = new JPanel();
		JPanel groupPanel = new JPanel();
		groupPanel.setLayout(new BoxLayout(groupPanel, BoxLayout.Y_AXIS));
		groupPanel.setPreferredSize(new Dimension(200,20+20));
		
		BoxLayout bLayout = new BoxLayout(pollingPanel,BoxLayout.X_AXIS);

		pollingPanel.setLayout(bLayout);
		//pollingPanel.setSize(425, 25);
		//pollingPanel.setBackground(java.awt.Color.green);
		//pollingPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		//pollingPanel.setBorder(new LineBorder(Color.RED));
		//pollingPanel.setBackground(Color.magenta);
		pollingPanel.setMaximumSize(new Dimension(650+37,20+20));
		
		JPanel dispPollingPanel = new JPanel();
		dispPollingPanel.setLayout(new BoxLayout(dispPollingPanel,BoxLayout.X_AXIS));


		locPol =new JLabel(connectorManager.getString("General.Lab.Polling"));
		rbAuto =new JRadioButton(connectorManager.getString("General.Lab.PollingAutomatic"));
		rbScheduler =new JRadioButton(connectorManager.getString("General.Lab.PollingDailyAt") + " ");
		rbPoll =new JRadioButton(connectorManager.getString("General.Lab.PollingEvery") + "     ");
		retainFormat=new JRadioButton(connectorManager.getString("General.Convertion.RetainFormat"));
		rconvertJPEG=new JRadioButton(connectorManager.getString("General.Convertion.JPEG"));
		rconvertDJVU=new JRadioButton(connectorManager.getString("General.Convertion.DJVU"));
		rconvertHCPDF = new JRadioButton(connectorManager.getString("General.Convertion.HCPDF"));
		singlePageHcpdf = new JCheckBox(connectorManager.getString("General.Lab.hcpdfSinglePage"),true);
				
		ButtonGroup group = new ButtonGroup();
		group.add(rbAuto);
		group.add(rbPoll);
		group.add(rbScheduler);
		ButtonGroup group1 = new ButtonGroup();
		group1.add(retainFormat);
		group1.add(rconvertJPEG);
		group1.add(rconvertDJVU);
		group1.add(rconvertHCPDF);

		RadioBtnListener rblistener =new RadioBtnListener();
		rbAuto.addActionListener(rblistener);
		rbPoll.addActionListener(rblistener);
		rbScheduler.addActionListener(rblistener);
		RadioConvertBtnListener rbconvlistener=new RadioConvertBtnListener();
		retainFormat.addActionListener(rbconvlistener);
		rconvertJPEG.addActionListener(rbconvlistener);
		rconvertDJVU.addActionListener(rbconvlistener);
		rconvertHCPDF.addActionListener(rbconvlistener);
		

		JPanel subPane =new JPanel();
		subPane.setLayout(null);

		SpinnerNumberModel sPModel = new SpinnerNumberModel(1,1,999, 1);         
		sPollTime =new JSpinner(sPModel);      
		JSpinner.DefaultEditor editor = (JSpinner.DefaultEditor)sPollTime.getEditor();       
		JTextField spfield  = (JTextField)editor.getTextField();      
		spfield.setBorder(BorderFactory.createEmptyBorder());  

		sPollTime.setMaximumSize(new Dimension(40,20));
		sPollTime.setPreferredSize(new Dimension(40,21));

		sPollTime.addChangeListener(new ChangeListener(){
			public void stateChanged(ChangeEvent e)
			{
				String newVal = ((JSpinner)e.getSource()).getValue().toString();
				int iVal =AIPUtil.to_Number(newVal);
				if(iVal >= 1 && iVal <= 999)
				{
					if(rbPoll.isSelected())
					{    
						int listSize = inputFolderList.getContents().getSize();
						for(int i=0; i<listSize; i++){
							String loc = (String) inputFolderList.getContents().get(i);
							LocationStatus locStatus = AIPManager.getLocationStatus(AIPManager.getCurrSid(), loc);
							if(locStatus != null){
								locStatus.setPolling(iVal);
							}
						}
					}    
				}    
			} 
		});    

		lmin =new JLabel("  "+connectorManager.getString("General.Lab.PollingMinutes"));

		rbPoll.setBounds(LEFT-10, TOP, WIDTH+20, HEIGHT+80);
		subPane.add(rbPoll);
		subPane.add(Box.createHorizontalStrut(150));
		subPane.add(sPollTime);
		subPane.add(lmin);


		locPol.setBounds(LEFT-10, TOP, WIDTH+20, HEIGHT);

		rbAuto.setBounds(LEFT-10, TOP+30, WIDTH+20, HEIGHT);

		JSpinner minuteSpinner;
		SpinnerDateModel dateModel = new SpinnerDateModel();
		String dailyFreq = null;
		sScheduler = new JSpinner(dateModel);
		sScheduler.setFont(sPollTime.getFont());
		JSpinner.DateEditor editor1 = new JSpinner.DateEditor(sScheduler, "HH:mm:ss");	   
		sScheduler.setEditor(editor1);
		sScheduler.setMaximumSize(new Dimension(73,20));

		dateModel.setCalendarField(Calendar.HOUR_OF_DAY);	
		try{
			dailyFreq = AIPManager.getScheduleTime();
			SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
			Date date = timeFormat.parse(dailyFreq);
			dateModel.setValue(date);
		}catch(Exception ex){
			dateModel.setCalendarField(Calendar.HOUR_OF_DAY);	
		}

		sScheduler.addChangeListener(new ChangeListener(){
			public void stateChanged(ChangeEvent cEvent){
				Date dailyFrequency = (Date)(((JSpinner)cEvent.getSource()).getModel().getValue());
				String dailyFreq = new SimpleDateFormat("HH:mm:ss").format(dailyFrequency);
				AIPManager.setScheduleTime(dailyFreq);
				lPollingInfo.setText(connectorManager.getString("EHGeneralPanel.NextPolling")+" "+ dailyFreq);

			}
		});		



		//pollingPanel.add(dispPollingPanel);
		groupPanel.add(dispPollingPanel);

		locPol.setBounds(LEFT, TOP, WIDTH+70, HEIGHT);
		pollingPanel.add(locPol);

		rbAuto.setBounds(LEFT-5, TOP+22, WIDTH+70, HEIGHT);
		pollingPanel.add(rbAuto);

		pollingPanel.add(Box.createHorizontalStrut(150));

		rbPoll.setBounds(LEFT-5, TOP+44, WIDTH-10, HEIGHT);
		pollingPanel.add(rbPoll);

		sPollTime.setBounds(LEFT+80, TOP+44, WIDTH-40, HEIGHT-1);
		pollingPanel.add(sPollTime);

		lmin.setBounds(LEFT+130, TOP+44, WIDTH+20, HEIGHT);
		pollingPanel.add(lmin);

		pollingPanel.add(Box.createHorizontalStrut(123));

		rbScheduler.setBounds(LEFT-5+20, TOP+66, WIDTH+11, HEIGHT);
		pollingPanel.add(rbScheduler);

		sScheduler.setBounds(LEFT+80+30, TOP+66, WIDTH+130, HEIGHT-1);
		pollingPanel.add(sScheduler);

		/*JLabel test = new JLabel("Test");
		test.setBounds(LEFT+170, TOP+86, WIDTH+130, HEIGHT);
		pollingPanel.add(test);*/
		/*		lLogLevel.setBounds(LEFT+80, TOP+88, WIDTH+70, HEIGHT);
		pollingPanel.add(lLogLevel);
		sLogLevel.setBounds(LEFT+150, TOP+88, WIDTH-40, HEIGHT);
		pollingPanel.add(sLogLevel);

		 */		//cFullTextIndex.setBounds(LEFT, TOP+95, WIDTH+120, HEIGHT);
		//	pollingPanel.add(cFullTextIndex);

		groupPanel.add(pollingPanel);

		return groupPanel;
	}
	
	private class RadioConvertBtnListener implements ActionListener
	{

		public void actionPerformed(ActionEvent e)
		{
			JRadioButton source =(JRadioButton)e.getSource();
			singlePageHcpdf.setSelected(false);
			AIPManager.setSinglePageHcpdf(0);
			if(source.equals(retainFormat)){
				PagesManager.nConvertToDJVU = 0;
				AIPManager.setConvertToDJVU(0);
				sCompression.setEnabled(false); 
				//AIPManager.setConversionMode(RetainFormat);
			}
			else if(source.equals(rconvertJPEG)){
				PagesManager.nConvertToDJVU = 2;
				AIPManager.setConvertToDJVU(2);
				if(rconvertJPEG.isEnabled()==false){
					VWLogger.info("Inside if condition ......");
					sCompression.setEnabled(false);
				}else{				
				sCompression.setEnabled(true);
				}
				//AIPManager.setConversionMode(RConvertJPEG);
		
			}
			else if(source.equals(rconvertDJVU)){
				PagesManager.nConvertToDJVU = 1;
				AIPManager.setConvertToDJVU(1);
				sCompression.setEnabled(false);
				//AIPManager.setConversionMode(RConvertDJVU);
			} else if(source.equals(rconvertHCPDF)){
				PagesManager.nConvertToDJVU = 3;
				AIPManager.setConvertToDJVU(3);
				sCompression.setEnabled(true);
				//AIPManager.setConversionMode(RConvertDJVU);
			}   
		}
	
	}
	
	

	private class RadioBtnListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			JRadioButton source =(JRadioButton)e.getSource();
			if(source.equals(rbAuto)){
				lPollingInfo.setText("");
				AIPManager.setPollingMode(0);
				
				sScheduler.setEnabled(false);
				sPollTime.setEnabled(false);
				sPollTime.setValue(new Integer(1));

				int listSize = inputFolderList.getContents().getSize();
				for(int i=0; i<listSize; i++){
					String loc = (String)inputFolderList.getContents().get(i);
					LocationStatus locStatus =AIPManager.getLocationStatus(AIPManager.getCurrSid(),loc);
					if(locStatus !=null){   
						locStatus.setPolling(0);
					}
				}
			}
			else if(source.equals(rbPoll)){
				SimpleDateFormat formatter= new SimpleDateFormat ("HH:mm:ss");
				long lCurTime = System.currentTimeMillis();
				Date date = new Date(lCurTime);
				String dateString = formatter.format(date);

				lPollingInfo.setText(connectorManager.getString("EHGeneralPanel.NextPolling")+" " + dateString);
				AIPManager.setPollingMode(1);
				
				sPollTime.setEnabled(true);
				sScheduler.setEnabled(false);

				int listSize = inputFolderList.getContents().getSize();
				for(int i=0; i<listSize; i++){
					String loc = (String)inputFolderList.getContents().get(i);
					LocationStatus locStatus =AIPManager.getLocationStatus(AIPManager.getCurrSid(),loc);
					if(locStatus !=null){
						int poll =((Integer)sPollTime.getValue()).intValue();
						locStatus.setPolling(poll); 
					}
				}
			}
			else if(source.equals(rbScheduler)){
				SimpleDateFormat formatter= new SimpleDateFormat ("HH:mm:ss");
				String dateString = formatter.format(sScheduler.getValue());
				lPollingInfo.setText(connectorManager.getString("EHGeneralPanel.NextPolling")+" " + dateString);						
				AIPManager.setPollingMode(2);
				
				sScheduler.setEnabled(true);

				sPollTime.setEnabled(false);
				sPollTime.setValue(new Integer(1));

				int listSize = inputFolderList.getContents().getSize();
				for(int i=0; i<listSize; i++){
					String loc = (String)inputFolderList.getContents().get(i);
					LocationStatus locStatus =AIPManager.getLocationStatus(AIPManager.getCurrSid(),loc);
					if(locStatus !=null){  
						locStatus.setPolling(0);
					}
				}
			}    
		}
	}    
	public void setAllEnabled(boolean enable,int status){
		VWLogger.info("enable 000000"+enable);
		VWLogger.info("status 000000"+status);
		if(status == 2) // Connector stop/work 
		{    
			VWLogger.info("enable 22222"+enable);
			VWLogger.info("status 22222"+status);
			Refbtn.setEnabled(enable);
			//Hlpbtn.setEnabled(enable);
			Clsbtn.setEnabled(enable);
			//Polltime.setEnabled(enable);
			RoomCombo.setEnabled(enable);
			locPol.setEnabled(enable);
			rbScheduler.setEnabled(enable);
			rbAuto.setEnabled(enable);
			rbPoll.setEnabled(enable);
			retainFormat.setEnabled(enable);
			VWLogger.info("retainFormat.isEanble 111:::"+retainFormat.isEnabled());
			rconvertJPEG.setEnabled(enable);
			VWLogger.info("rconvertJPEG.isEanble 111:::"+rconvertJPEG.isEnabled());
			rconvertDJVU.setEnabled(enable);
			VWLogger.info("rconvertDJVU.isEanble 111:::"+rconvertDJVU.isEnabled());
			rconvertHCPDF.setEnabled(enable);
			VWLogger.info("rconvertHCPDF.isEanble 111:::"+rconvertHCPDF.isEnabled());
			singlePageHcpdf.setEnabled(enable);
			lmin.setEnabled(enable);
			SysExp.setAllEnabled(enable);        
			inputFolderController.setAllEnabled(enable);
			//inputFolderList.setEnabled(enable);
			//cbConvDJVU.setEnabled(enable);
			lGrayLine.setEnabled(enable);
			lWhiteLine.setEnabled(enable);

			//Disable checkbox if isDJVUExeExists() function returns false.
			/**
			 * Condition commented to change the djvu to jpeg
			 * Commented by madhavan
			 * Date :-13 Nov 2017
			 */
			if(!isDJVUExeExists()){
				rconvertDJVU.setEnabled(false);	
			}

			templateBox.setEnabled(enable);
			conbtn.setEnabled(enable);
			cUseJHPages.setEnabled(enable);
			
			switch(AIPManager.getPollinMode()){
				case AUTO: rbAuto.setSelected(true);
						   rbPoll.setSelected(false);
						   rbScheduler.setSelected(false);
						   sPollTime.setEnabled(false);
						   sScheduler.setEnabled(false);
						   break;
				
				case EVERY: rbAuto.setSelected(false);
							rbPoll.setSelected(true);
							rbScheduler.setSelected(false);
							sPollTime.setEnabled(true);
							sScheduler.setEnabled(false);
							break;
				
				case DAILY_AT:	rbAuto.setSelected(false);
								rbPoll.setSelected(false); 
								rbScheduler.setSelected(true);
								sPollTime.setEnabled(false);
								sScheduler.setEnabled(true);
								break;
			}
			//VWLogger.info("AIPManager.getConversionMode()::::"+AIPManager.getConversionMode());
			switch(AIPManager.isConvertToDJVU()){
			case RetainFormat: retainFormat.setSelected(true);
							   rconvertJPEG.setSelected(false);
							   rconvertDJVU.setSelected(false);
							   sCompression.setEnabled(false);
							   rconvertHCPDF.setSelected(false);
							   singlePageHcpdf.setSelected(false);
							break;
			
			case RConvertJPEG: retainFormat.setSelected(false);
							   rconvertJPEG.setSelected(true);
							   rconvertDJVU.setSelected(false);
							   if((rconvertJPEG.isSelected()==true)&&(rconvertJPEG.isEnabled()==false)){
								   sCompression.setEnabled(false); 
							   }else{
							   sCompression.setEnabled(true);
							   }
							   rconvertHCPDF.setSelected(false);
							   singlePageHcpdf.setSelected(false);
						break;
			
			case RConvertDJVU:	retainFormat.setSelected(false);
								rconvertJPEG.setSelected(false);
								rconvertDJVU.setSelected(true);
								sCompression.setEnabled(false);
								rconvertHCPDF.setSelected(false);
								singlePageHcpdf.setSelected(false);
							break;
			case RConvertHCPDF:								
								retainFormat.setSelected(false);
								rconvertJPEG.setSelected(false);
								rconvertDJVU.setSelected(false);
								sCompression.setEnabled(true);
								rconvertHCPDF.setSelected(true);
							break;
		}

		}
		else if(status ==1) // Connector connect/disconnect
		{
			VWLogger.info("enable 11111"+enable);
			Refbtn.setEnabled(enable);           
			Startbtn.setEnabled(enable);   
			locPol.setEnabled(enable);
			rbAuto.setEnabled(enable);
			rbPoll.setEnabled(enable);
			rbScheduler.setEnabled(enable);
			retainFormat.setEnabled(enable);
			
			VWLogger.info("retainFormat.isEanble 2222:::"+retainFormat.isEnabled());
			rconvertJPEG.setEnabled(enable);
			VWLogger.info("retainFormat.isEanble 222:::"+rconvertJPEG.isEnabled());
			VWLogger.info("retainFormat.isselected 222:::"+rconvertJPEG.isSelected());
			if((rconvertJPEG.isSelected()==true)&&(rconvertJPEG.isEnabled()==false)){
				VWLogger.info("Inside if condition ......");
				sCompression.setEnabled(false);
			}
			
			rconvertDJVU.setEnabled(enable);
			VWLogger.info("retainFormat.isEanble 222:::"+rconvertDJVU.isEnabled());
			rconvertHCPDF.setEnabled(enable);
			VWLogger.info("rconvertHCPDF.isEanble 111:::"+rconvertHCPDF.isEnabled());
			singlePageHcpdf.setEnabled(enable);
			lmin.setEnabled(enable);
			SysExp.setAllEnabled(enable);        
			inputFolderController.setAllEnabled(enable);
			inputFolderList.setEnabled(enable); 
			//cbConvDJVU.setEnabled(enable);
			lGrayLine.setEnabled(enable);
			lWhiteLine.setEnabled(enable);

			//Disable checkbox if isDJVUExeExists() function returns false.
			/**
			 * Condition commented to change the djvu to jpeg
			 * Commented by madhavan
			 * Date :-13 Nov 2017
			 */
			/*if(!isDJVUExeExists()){
				cbConvDJVU.setEnabled(false);	
			}*/
			if(!isDJVUExeExists()){
				rconvertDJVU.setEnabled(false);	
			}


			templateBox.setEnabled(enable);        
			cUseJHPages.setEnabled(enable);
			switch(AIPManager.getPollinMode()){
				case AUTO: rbAuto.setSelected(true);
						   rbPoll.setSelected(false);
						   rbScheduler.setSelected(false);
						   sPollTime.setEnabled(false);
						   sScheduler.setEnabled(false);
						   break;
	
				case EVERY: rbAuto.setSelected(false);
							rbPoll.setSelected(true);
							rbScheduler.setSelected(false);
							sPollTime.setEnabled(true);
							sScheduler.setEnabled(false);
							break;
	
				case DAILY_AT: rbAuto.setSelected(false);
							   rbPoll.setSelected(false); 
							   rbScheduler.setSelected(true);
							   sPollTime.setEnabled(false);
							   sScheduler.setEnabled(true);
							   break;
			}
			
			//VWLogger.info("AIPManager.getConversionMode()::::"+AIPManager.getConversionMode());
			switch (AIPManager.isConvertToDJVU()) {
			case RetainFormat:
				retainFormat.setSelected(true);
				rconvertJPEG.setSelected(false);
				rconvertDJVU.setSelected(false);
				sCompression.setEnabled(false);
				rconvertHCPDF.setSelected(false);
				singlePageHcpdf.setSelected(false);
				break;

			case RConvertJPEG:
				retainFormat.setSelected(false);
				rconvertJPEG.setSelected(true);
				rconvertDJVU.setSelected(false);
				if ((rconvertJPEG.isSelected() == true) && (rconvertJPEG.isEnabled() == false)) {
					sCompression.setEnabled(false);
				} else {
					sCompression.setEnabled(true);
				}
				rconvertHCPDF.setSelected(false);
				singlePageHcpdf.setSelected(false);
				break;

			case RConvertDJVU:
				retainFormat.setSelected(false);
				rconvertJPEG.setSelected(false);
				rconvertDJVU.setSelected(true);
				sCompression.setEnabled(false);
				rconvertHCPDF.setSelected(false);
				singlePageHcpdf.setSelected(false);
				break;
			case RConvertHCPDF:								
				retainFormat.setSelected(false);
				rconvertJPEG.setSelected(false);
				rconvertDJVU.setSelected(false);
				sCompression.setEnabled(true);
				rconvertHCPDF.setSelected(true);
			break;
			}
		}    

	}


	//-----------------------Method Process --------------------------------
	public Vector LoadVWRooms(){
		Vector  serversRooms =new Vector();
		int status = AIPManager.getServersRooms(serversRooms);
		if(status !=AIPManager.NOERROR)
		{
			JOptionPane.showMessageDialog(this,connectorManager.getString("Init.Msg.NoRooms"),connectorManager.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);
			EHD.ExitConnector();

		}    
		if(serversRooms.size()== 0){
			JOptionPane.showMessageDialog(this,connectorManager.getString("Init.Msg.NoRooms"),connectorManager.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);
			EHD.ExitConnector();
		}

		return serversRooms;
	}
	// update Ayman
	public boolean setInitializeParams(int sid){
		if(sid !=0)
		{    
			AIPInit iInfo =   EyesHandsManager.getInitParams(sid);    
			if(iInfo == null)        
			{  
				String sRoom ="";
				Session s =AIPManager.getSession(sid); 
				if(s!=null)sRoom = s.server+"."+s.room;
				JOptionPane.showMessageDialog(this,connectorManager.getString("Init.Msg.RoomDown_pref")+" "+sRoom+" "+"",connectorManager.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);				
				//if(Log.LOG_DEBUG) Log.debug("Error :ViewWise Database components not found in Room <"+RName+"> .Cannot connect. ");
				return false;
			}      
			// Polltime.setText(iInfo.getPollingTime());

			inputFolderList.getContents().removeAllElements();
			AIPManager.removeAllLocationsStatus(sid);
			String[] inputFolders =iInfo.getInputFolders();
			Vector enabledFolders =AIPManager.getEnabledFolders(sid);
			for (int i = 0; i < inputFolders.length; i++)
			{     
				if (!inputFolders[i].trim().equals(""))
				{
					inputFolderList.getContents().addElement(inputFolders[i]);
					LocationStatus loc =new LocationStatus(inputFolders[i]);					
					AIPManager.addLocationStatus(sid,loc);
					for(int j=0;j<enabledFolders.size();j++)
					{   
						String eFPath =(String)enabledFolders.get(j);      
						int tabLoc = eFPath.indexOf("\t");
						if( tabLoc>0)
						{
							if(eFPath.substring(0,tabLoc).trim().equalsIgnoreCase(inputFolders[i]))
							{
								int poll =AIPUtil.to_Number(eFPath.substring(tabLoc+1,eFPath.length()));
								loc.setEnabledProcess(true);
								/**
								 * Issue Fixed : On Disconnect of a room, poll value got updated to 0 in registry for that room.
								 */
								if(AIPManager.getPollinMode() == EVERY && poll == 0){
									poll = Integer.parseInt(String.valueOf(sPollTime.getValue()));
									loc.setPolling(poll);
								}
								else
									loc.setPolling(poll);
								break;
							}  
						} 
						else 
						{
							if(eFPath.trim().equalsIgnoreCase(inputFolders[i]))
							{
								loc.setEnabledProcess(true);
								loc.setPolling(0);
								break;   
							}    
						}                   
					}
					if(inputFolders[i].trim().equals(iInfo.getDefaultLinkedFolder()))
					{
						locPol.setText(connectorManager.getString("General.Lab.Polling"));
						if(loc.isEnabledProcess())
						{
							if(loc.getPolling() <=0)
							{
								rbAuto.setSelected(true);
								sPollTime.setValue(new Integer(1));
								sPollTime.setEnabled(false);
							}    
							else
							{
								rbPoll.setSelected(true);
								sPollTime.setValue(new Integer(loc.getPolling()));
							}    
						}
						else
						{
							rbAuto.setSelected(true);
							sPollTime.setValue(new Integer(1));
						}  
						inputFolderList.setSelectedValue(iInfo.getDefaultLinkedFolder(),true);
					}    
				}

			}
			//inputFolderList.setSelectedValue(iInfo.getDefaultLinkedFolder(),true);
			java.io.File FFile =EHD.CFManager.getEHFormsFile(iInfo.getDefaultLinkedFolder());
			if(!(FFile !=null && EHD.CFManager.isEHFile(FFile,"forms")))
			{
				EyesHandsManager.tempDisabled = true;				
			}			

		}    
		else
		{
			// Polltime.setText("");
			//AIPManager.removeAllLocationsStatus();
			inputFolderList.getContents().removeAllElements();

			locPol.setText(connectorManager.getString("General.Lab.Polling"));
			rbAuto.setSelected(false);
			rbPoll.setSelected(false);
			retainFormat.setSelected(false);
			rconvertJPEG.setSelected(false);
			rconvertDJVU.setSelected(false);
			sPollTime.setValue(new Integer(1));
			rconvertHCPDF.setSelected(false);
			singlePageHcpdf.setSelected(false);
		}    
		return true;
	}
	private Session checkNotConnectedServer(Session s)
	{    
		Session retS =null; 
		if(s.room.equalsIgnoreCase(AIPUtil.getString("Init.Lab.NoServer")))  
		{      String server =s.server;
		Vector rooms =new Vector();
		AIPManager.getVWC().getRooms(s.server,rooms);
		if(rooms.size() ==0)
		{
			JOptionPane.showMessageDialog(this,connectorManager.getString("EHGeneralPanel.Server")+" "+server,
					connectorManager.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);
			return retS;
		}
		else
		{    
			int indx =RoomCombo.getSelectedIndex();
			isUpdatingCombo =true;
			RoomCombo.removeItemAt(indx);
			Vector sessions =AIPManager.getSessions();
			sessions.remove(indx);
			for(int j=0;j<rooms.size();j++)
			{                         
				RoomCombo.insertItemAt(server+"."+(String)rooms.get(j),indx);
				Session s1 = new Session(0,server,(String)rooms.get(j),AIPManager.getWorkingFolder(),"");   
				if(j ==0) 
				{    
					RoomCombo.setSelectedIndex(indx);
					retS =s1;
				}
				sessions.add(indx++,s1); 


			} 
			isUpdatingCombo =false;
			return retS;
		}     

		}
		retS =s;
		return retS;
	}
	public int internalConnect(Session s)
	{
		/**CV10.2 - Try catch block added. Added by Vanitha S**/
		int sessionId =0;
		try {
			String username ="";
			String userPass ="";
			boolean rememberAuth =false;
	
			String auth =AIPManager.getLastAuthentication();
			if(auth == null) return sessionId;
	
			StringTokenizer sToken = new StringTokenizer(auth, "/");      
			if (sToken.hasMoreTokens()) username = sToken.nextToken();
			if (sToken.hasMoreTokens()) userPass = sToken.nextToken();
			sessionId = AIPManager.login(s,username,userPass);
			if(sessionId > 0)     
			{           
				AIPInit iInfo =   EyesHandsManager.getInitParams(sessionId);    
				if(iInfo == null)        
				{              
					internalDisconnect(sessionId);
					return 0;
				}
				else 
				{      
					/*enableConverToDJVU = AIPManager.checkConvertToDJVUFlag(sessionId);
					VWLogger.info("enableConverToDJVU :::::::"+enableConverToDJVU);
					setAIPProcessOptions(enableConverToDJVU);*/
					Vector dts =new Vector();
					int status = DTManager.getDoctypes(sessionId,dts,true);
					if(status != AIPManager.NOERROR)
					{    
	
					}
					EHD.TempPane.MDT.setMapDoctypes(VWDoctypes);      
					Vector vNodes =DocumentsManager.getNavigatorNode(sessionId,true);                 
				}     
			}
		} catch (Exception e) {
			VWLogger.info("Exception in internalConnect :"+e.getMessage());
		}
		return sessionId;     
	}    
	private int internalDisconnect(int sid)
	{
		int status = AIPManager.logout(sid);
		//if(status != AIPManager.NOERROR)       
		EyesHandsManager.releaseSession(sid);
		DocumentsManager.releaseSession(sid);
		DoctypesManager.releaseSession(sid); 
		return status;  
	}    

	public int connect(Session s)
	{ 
		VWLogger.info("inside connect method......");
		/**CV10.2 - Process monitor thread has to be re-initiated after reconnect. Added by Vanitha S**/
		stopAllProcessScheduling();
		int sessionId =0;  

		s = checkNotConnectedServer(s);
		if(s ==null) return sessionId;

		String username ="";
		String userPass ="";
		boolean rememberAuth =false;

		String auth =AIPManager.getLastAuthentication();


		if(auth !=null)
		{            
			StringTokenizer sToken = new StringTokenizer(auth, "/");      
			if (sToken.hasMoreTokens()) username = sToken.nextToken();
			if (sToken.hasMoreTokens()) userPass = sToken.nextToken();
			rememberAuth =true;

		}    
		else
		{    
			username = s.user;
			if(username.equals(""))
				username =System.getProperty("user.name");
		}   

		UserPassPane userPassPanel = new UserPassPane();    
		userPassPanel.setUsername(username);
		userPassPanel.setPassword(userPass);
		userPassPanel.setRememberAuth(rememberAuth);

		// Code added for AIP Service - start
		if(AIPManager.getDefaultStartupRoom() != null && !("").equals(AIPManager.getDefaultStartupRoom().trim()))
		{
			if(RoomCombo.getSelectedItem().toString().trim().equalsIgnoreCase(AIPManager.getDefaultStartupRoom().toString().trim()))
				userPassPanel.setDefaultStartupRoom(true);
			else
				userPassPanel.setDefaultStartupRoom(false);

		}
		else
			userPassPanel.setDefaultStartupRoom(false);
		// Code added for AIP Service - End

		JOptionPane optionPane = new JOptionPane(userPassPanel,JOptionPane.PLAIN_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
		optionPane.setPreferredSize(new java.awt.Dimension(280,210)); 
		JDialog d = optionPane.createDialog(this,connectorManager.getString("UserPane.Lab.Title"));
		/*
    	 * new JDialog().show(); method is replaced with new JDialog().setVisible(true); 
    	 * as show() method is deprecated  //Gurumurthy.T.S 19/12/2013,CV8B5-001
    	 */
		d.setVisible(true);

		int val =((Integer)optionPane.getValue()).intValue();    

		if(val ==JOptionPane.OK_OPTION)
		{  
			username = userPassPanel.getUsername();        
			userPass = userPassPanel.getPassword();
			if(username == null ||username.trim().equalsIgnoreCase("")){
				JOptionPane.showMessageDialog(this, connectorManager.getString("Aip.Blank.Username"),null, JOptionPane.ERROR_MESSAGE);
				return 0;	
			}
			if(userPass == null ||userPass.trim().equalsIgnoreCase("")){
				JOptionPane.showMessageDialog(this, connectorManager.getString("Aip.Blank.Password"),null, JOptionPane.ERROR_MESSAGE);
				return 0;	
			}
			if(username.trim().equals(""))
			{
				AIPManager.setLastAuthentication("");
				return sessionId;
			}    
			if(userPassPanel.isRememberAuth())
			{    
				AIPManager.setLastAuthentication(username+"/"+userPass);    
			}        
			else   AIPManager.setLastAuthentication(""); 
			// Code added for EAS Service - start
			if(userPassPanel.isDefaultStartupRoom())
				AIPManager.setDefaultStartupRoom(RoomCombo.getSelectedItem().toString());    
			else   AIPManager.setDefaultStartupRoom("");      
			// Code added for EAS Service - end    

			sessionId = AIPManager.login(s,username,userPass);
			switch(sessionId){
			case ViewWiseErrors.NoMoreLicenseSeats:
				JOptionPane.showMessageDialog(this,connectorManager.getString("Init.Msg.NoMoreLicenceSeats"),connectorManager.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);
				break;
			case ViewWiseErrors.accessDenied:
				JOptionPane.showMessageDialog(this,connectorManager.getString("Init.Msg.InvalidAdmin"),connectorManager.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);
				break;
			case ViewWiseErrors.checkUserError:
				JOptionPane.showMessageDialog(this,connectorManager.getString("Init.Msg.InvalidAdmin"),connectorManager.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);
				break;
			case ViewWiseErrors.LoginCountExceeded:
				JOptionPane.showMessageDialog(this,connectorManager.getString("Init.Msg.LoginCountExceeded"),connectorManager.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);
				break;
			case ViewWiseErrors.UserDoesNotExistINDB:
				JOptionPane.showMessageDialog(this,connectorManager.getString("Init.Msg.InvalidAdmin"),connectorManager.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);
				break;
			default:
				d.setVisible(false); 
			AIPManager.setCurrSid(sessionId);
			/**
			 * Added for restarting aip automatically due to networkfailure
			 */
			VWLogger.info("After setting curSid in  autostart :::"+sessionId); 
			if(listTable.isEmpty()){
				VWLogger.info("Inside list is empty");
				listTable.put(sessionId, new AIPClient(s));
				VWLogger.info("after adding"+listTable);
			}
			
			if(!setInitializeParams(sessionId))
			{    
				disconnect(sessionId);
				return 0;
			}
			else 
			{     
				/**CV2020 - Image backup enhancement **/
				initiateImageBackupMonitoringScheduler();
				/***************************************************************/
				EHD.setAllEnabled(true,1);  
				EHD.TabPane.setEnabledAt(2, true);//history pane
				EHD.TabPane.setEnabledAt(3, true);//Pending pane
				if(!EyesHandsManager.tempDisabled)
					EHD.TabPane.setEnabledAt(1, true);
				//conbtn.setText(connectorManager.getString("General.Btn.Disconnect"));
				conbtn.setToolTipText(connectorManager.getString("General.Btn.Disconnect"));
				conbtn.setIcon(IM.getImageIcon("disconnect.gif"));
				/*
				 * Enhancement 	: Enhancements of AIP  
				 * 				  Requirment sent by Rajesh on 09-June-2006 through eMail  	 
				 * Created By		: M.Premananth
				 * Date			: 14-June-2006
				 */
				if(AIPManager.isArchiveHistoryEnabled(AIPManager.getCurrSid()))
				{
					EHD.HistoryPane.setAllHistoryEnabled(true);
					ArchiveHistory archivehistory = new ArchiveHistory(EHD.GenPane);
					archivehistory.setStatus(false);
					AIPManager.addArchiveThread(AIPManager.getCurrSid(),archivehistory);
					archivehistory.start();
				}
				else
				{
					AIPManager.setArchiveHistoryEnabled(AIPManager.getCurrSid(),false);
					EHD.HistoryPane.setAllHistoryEnabled(false);
				}
				LoadGeneralData();                 
			}    
			listTable.put(s.sessionId, new AIPClient(s));
			//new AIPClient(s);
			}
		}    
		else
			d.setVisible(false);
		if(sessionId > 0){
			/*enableConverToDJVU = AIPManager.checkConvertToDJVUFlag(sessionId);
			VWLogger.info("enableConverToDJVU :::::::"+enableConverToDJVU);
			setAIPProcessOptions(enableConverToDJVU);*/
			if(AIPManager.getPollinMode() == AUTO){
				sPollTime.setEnabled(false);
				sScheduler.setEnabled(false);				
			}else if(AIPManager.getPollinMode() == EVERY){
				sPollTime.setEnabled(true);
				sScheduler.setEnabled(false);
				long lCurTime = System.currentTimeMillis();
				Date date = new Date(lCurTime);
				String nextPollTime = date.getHours()+":"+date.getMinutes()+":"+date.getSeconds();
				SimpleDateFormat formatter= new SimpleDateFormat ("HH:mm:ss");
				String dateString = formatter.format(date);
				lPollingInfo.setText(connectorManager.getString("EHGeneralPanel.NextPolling")+" "+ dateString);

			}else if(AIPManager.getPollinMode() == DAILY_AT){
				sPollTime.setEnabled(false);
				sScheduler.setEnabled(true);
				lPollingInfo.setText(connectorManager.getString("EHGeneralPanel.NextPolling")+" "+ AIPManager.getScheduleTime());
			}
		}
		return sessionId;
	}
	/* Issue #754: Method added to alert client when server is down
	 *  C.Shanmugavalli 13 Nov 2006
	 */
	protected class IdleTimerAction implements java.awt.event.ActionListener
	{
		public void actionPerformed(java.awt.event.ActionEvent e)
		{
			VWLogger.info("Inside idle timer action");
			//JOptionPane.showMessageDialog(null, " coming in IdleTimerAction 1", "Info", JOptionPane.OK_OPTION);
			/**
			 * CV10.1 Issue fix Added for removing aip orphan session
			 */
			VWLogger.info("listTable size::::::::::"+listTable.size());
			int sessionStatus=AIPManager.setAIPConnectionStatus(AIPManager.getCurrSid());
			VWLogger.info("sessionStatus from server :::::::"+sessionStatus);
			if(sessionStatus==-49||sessionStatus==-499){
				VWLogger.info("Inside if condition session not exist in server hashtable");
				serverfailure=true;
			}
			
			if(listTable != null && listTable.size()>0){
				//JOptionPane.showMessageDialog(null, " coming in IdleTimerAction 2", "Info", JOptionPane.OK_OPTION);
				Enumeration enumc = listTable.elements();
				int sessionId = 0;
				String roomName = "";
				String server = "";
				VWLogger.info("enum has more elements ::::::::::"+enumc.hasMoreElements());
				while (enumc.hasMoreElements())
				{
					//Session s = (Session)enumc.nextElement();
					AIPClient aipClient = (AIPClient)enumc.nextElement();
					Session s = aipClient.session;
					roomName = s.room;
					sessionId = s.sessionId;
					server = s.server;
					int status = AIPManager.getServersStatus(server);
					/**
					 * Condition added for autostart fix due to network failure 
					 */
					VWLogger.info("status from getserver :::::::::::::"+status);
					VWLogger.info("AIPManager.isAIPServiceStarted() :::::::"+AIPManager.isAIPServiceStarted());
					VWLogger.info("server failure flag value:::"+serverfailure);
					if(serverfailure==true && status==0 && AIPManager.isAIPServiceStarted()==true){
						VWLogger.info("Inside if condition of automatic start::::::::");
						listTable.clear();
						VWLogger.info("session id before disconnect:::"+AIPManager.getCurrSid());
						//Fix added to stop the process immediately when network error occurs
						if( Startbtn.getText().equalsIgnoreCase(connectorManager.getString("General.Btn.Stop"))){
							stopAIPProcess(false);
						}
						disconnect(AIPManager.getCurrSid());
						VWLogger.info("After disconnect previous session:::::::");
						VWLogger.info("session id after disconnect call :::"+sessionId);
						VWLogger.info("Calling autostart automatically");
						VWLogger.info("***********************************************************************");
						autoStart();
						VWLogger.info("Calling clickStartStop button after Calling autostart automatically");
						if(Startbtn.getText().equals(connectorManager.getString("General.Btn.Start"))) {
							VWLogger.info("Before click start stop action to start process in idletimer action...");
							clickStartStop();
						}
						VWLogger.info("After Calling autostart automatically");
						try {
							Thread.sleep(10000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						serverfailure=false;
					}
					//Code added by Srikanth on 01 Dec 2016
					if(serverfailure==false && status==0 && AIPManager.isAIPServiceStarted()==true && StartFlag == 0){
						VWLogger.info("Inside serverfailure false and startflag is 0 and aip server is running::::::::");
						VWLogger.info("Calling clickStartStop button after Calling autostart automatically");
						if(Startbtn.getText().equals(connectorManager.getString("General.Btn.Start"))) {
							VWLogger.info("2222 Before click start stop action to start process inside idletimeraction...");
							clickStartStop();
						}
						VWLogger.info("After Calling clickstartstop inside idletimeraction...");
						try {
							Thread.sleep(10000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					
					}
					//Code added by Srikanth on 01 Dec 2016
					
					
					if(status == AIPManager.ERROR&&AIPManager.isAIPApplicationStarted()==true){
						//JOptionPane.showMessageDialog(null, " coming in IdleTimerAction 4", "Info", JOptionPane.OK_OPTION);
						//listTable.remove(s.sessionId);
						VWEvent event = new VWEvent(server,roomName, VWEvent.ADMIN_DISCONNECT, sessionId);
						AIPManager.getVWC().eventHandler(event);  
						aipClient.idleTimer.stop();
					}
				}
				if(AIPManager.isAIPApplicationStarted()==true)
					listTable.clear();
			}

			/*if(AIPConnector.listTable != null && AIPConnector.listTable.size()>0){
	          Enumeration enumc = AIPConnector.listTable.elements();
	          JOptionPane.showMessageDialog(null, " coming in 2", "Info", JOptionPane.OK_OPTION);
	          int sessionId = 0;
	          String roomName = "";
	          int isIdle = -1;
	          String server = "";
	          if(AIPConnector.listTable.size()>0)
	        	  JOptionPane.showMessageDialog(null, " table size "+AIPConnector.listTable.size(), "Info", JOptionPane.OK_OPTION);
	          else
	        	  JOptionPane.showMessageDialog(null, " table size 0 ", "Info", JOptionPane.OK_OPTION);
	          while (enumc.hasMoreElements())
	          {
	          	try{
	          		JOptionPane.showMessageDialog(null, " coming in 3", "Info", JOptionPane.OK_OPTION);
	          		AIPClient aipClient = (AIPClient)enumc.nextElement();
	          		Session s = aipClient.session;
	          		roomName = s.room;
	          		sessionId = s.sessionId;
	          		server = s.server;
	          		isIdle = vwc.isRoomIdle(sessionId, roomName);
	          		JOptionPane.showMessageDialog(null, "isIdle "+isIdle, "Info", JOptionPane.OK_OPTION);
	          	if(isIdle == VWEvent.IDLE_DISCONNECT){
	          		VWEvent event = new VWEvent(server,roomName, VWEvent.IDLE_DISCONNECT,sessionId);
	          		vwc.eventHandler(event);  
	          		aipClient.idleTimer.stop();
	          	}else if(isIdle == ViewWiseErrors.invalidSessionId){
	          		VWEvent event = new VWEvent(server,roomName, ViewWiseErrors.invalidSessionId,sessionId);
	          		vwc.eventHandler(event);
	          		aipClient.idleTimer.stop();
	          	}else if(isIdle == ViewWiseErrors.destinationRoomServerInactive){
	          		VWEvent event = new VWEvent(server,roomName, ViewWiseErrors.destinationRoomServerInactive,sessionId);
	          		vwc.eventHandler(event);
	          		aipClient.idleTimer.stop();
	          	}
	          	//VWMessage.showMessage(null,"Idle Information " + roomId + " - " + roomName + " - " + isIdle ,VWMessage.DIALOG_TYPE);
	          	//VWMessage.showMessage(null,"Current Time in Millis - " + System.currentTimeMillis() ,VWMessage.DIALOG_TYPE);
	          }catch(Exception ex){          }
	      }
    	}else{ JOptionPane.showMessageDialog(null, " table size 0", "Info", JOptionPane.OK_OPTION);}
			 */
		}
	}

	public class AIPClient{
		public javax.swing.Timer idleTimer;
		public Session session = null;
		public AIPClient(Session session ){
			this.session = session;
			setIdleTimeout(1);
		}
		// 15 seconds once it will check server is down or not
		public void setIdleTimeout(int timeout)
		{
			VWLogger.info("setIdleTimeout ::::timeout:::"+timeout);
			if (timeout > 0)
			{
				VWLogger.info("Inside IdleTimeout if contion");
				if (idleTimer != null){
					VWLogger.info("Inside idle timer is null");
					idleTimer.stop();
				}
				idleTimer = new javax.swing.Timer(timeout*250*60, new IdleTimerAction());
				idleTimer.setRepeats(true);
				idleTimer.start();
			}
		}
		public void run(){
			VWLogger.info("Inside idle timer run method");
			setIdleTimeout(1);
		}
	}
//	End of Issue # 754
	public int disconnect(int sid)
	{
		if(AIPManager.getCurrSid() == sid)
		{     
			VWLogger.info("inside disconnect method");
			AIPInit iInfo =EyesHandsManager.getInitParams(sid);
			if(iInfo !=null)
			{
				iInfo.setPollingTime("");
				//iInfo.setDefaultRoomNodeId(VWNodes.getSelectedNodeid());
				if (inputFolderList.getSelectedIndex() == -1)
					iInfo.setDefaultLinkedFolder("");
				else iInfo.setDefaultLinkedFolder((String)(inputFolderList.getSelectedValue()));      
				int size = inputFolderList.getContents().size();
				String[] inputFolders = new String[size];
				for (int i = 0; i < size; i++)
					inputFolders[i] = (String)(inputFolderList.getContents().getElementAt(i));
				Vector locs =AIPManager.getAllLocationsStatus(sid);
				Vector enabledFolders =new Vector();            
				for(int j=0;j<locs.size();j++)
				{    
					LocationStatus loc =(LocationStatus)locs.get(j);
					if(loc.isEnabledProcess())
						enabledFolders.add(loc.getName()+"\t"+loc.getPolling());    
				}  
				AIPManager.putEnabledFolders(sid,enabledFolders);
				iInfo.setInputFolders(inputFolders);
			}
		} 
		if (imageBackupMonitoringTimer != null) {
			imageBackupMonitoringTimer.cancel();
			imageBackupMonitoringTimer = null;
		}
		InetAddress inet;
		try {
			inet = InetAddress.getLocalHost();
			EyesHandsManager.saveInitParams(sid,inet.getHostAddress());
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		VWLogger.info("before logout disconnect method");
		int status = AIPManager.logout(sid);
	
		VWLogger.info("After logout disconnect method"+status);
		//if(status != AIPManager.NOERROR)
		//  JOptionPane.showMessageDialog(this,AIPManager.getErrorDescription(status)+"("+status+")",
		//                 connectorManager.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE); 
		//else 
		//{  
		EyesHandsManager.releaseSession(sid);
		DocumentsManager.releaseSession(sid);
		DoctypesManager.releaseSession(sid); 
		if(AIPManager.getCurrSid() == sid)
		{    
			//conbtn.setText(connectorManager.getString("General.Btn.Connect")); 
			conbtn.setToolTipText(connectorManager.getString("General.Btn.Connect"));
			conbtn.setIcon(IM.getImageIcon("connect.gif"));
			setInitializeParams(0);
			removeGeneralData();
			ArchiveHistory archiveHistory = AIPManager.removeArchiveThread(AIPManager.getCurrSid());
			if(archiveHistory != null)
				archiveHistory.setStatus(true);
			AIPManager.setCurrSid(0);
			EHD.setAllEnabled(false,1);
			EHD.TabPane.setEnabledAt(1, false);
			EHD.TabPane.setEnabledAt(2, false);            
			EHD.TabPane.setEnabledAt(3, false);            
		}   
		// }   
		lPollingInfo.setText("");
		return status;
	}


	//update ayman
	public void LoadGeneralData(){
		// if(Log.LOG_DEBUG)Log.debug("Load General Data");
		VWDoctypes =new Vector();
		int status = DTManager.getDoctypes(AIPManager.getCurrSid(),VWDoctypes,false);
		if(status != AIPManager.NOERROR)
		{    
			JOptionPane.showMessageDialog(this,AIPManager.getErrorDescription(status)+"("+status+")",
					connectorManager.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE); 
			return;
		}
		EHD.TempPane.MDT.setMapDoctypes(VWDoctypes);      
		// AIP Hang Issue
		// Desc : Check for 'template' disabled option, if not checked
		// allow vector to be loaded with nodes else do not load vector with nodes
		// Author : Nebu Alex
		if (!EyesHandsManager.tempDisabled)
		{  
			/*
			 * Issue No	 	:  
			 * 				   	 
			 * Created By		: Nebu Alex
			 * Date			: 19-August-2006
			 */
			Vector vNodes =DocumentsManager.getNavigatorNode(AIPManager.getCurrSid(),true);     
			EHD.TempPane.MDT.setNodesNavigator(vNodes);
			EHD.TempPane.RemoveAllMDTRows();
			// End of code change
			Vector forms =new Vector();
			AIPManager.getVWC().getAIPForms(AIPManager.getCurrSid(),forms);
			EHD.TempPane.MDT.LoadTableData(forms);
		}    
		/*
		 * Enhancement 	: Enhancements of AIP  
		 * 				  Requirment sent by Rajesh on 09-June-2006 through eMail  	 
		 * Created By		: M.Premananth
		 * Date			: 14-June-2006
		 */
		EHD.HistoryPane.RemoveAllPFLRows();
		Vector hCount = new Vector();
		AIPManager.getVWC().getAIPHistoryCount(AIPManager.getCurrSid(),hCount,1);
		EHHistoryPanel.iTotalCount = Integer.parseInt(hCount.get(0).toString());
		Vector hFiles =new Vector();
		AIPManager.getVWC().getAIPHistoryFiles(AIPManager.getCurrSid(),hFiles,0,EHHistoryPanel.iMaximumRows,1);
		EHD.HistoryPane.PFL.LoadTableData(hFiles);
		EHHistoryPanel.iStartIndex 	= 0;
		EHHistoryPanel.iEndIndex		= EHHistoryPanel.iMaximumRows;

		EHD.PendingPane.RemoveAllPFLRows();
		Vector hPendingCount = new Vector();
		AIPManager.getVWC().getAIPHistoryCount(AIPManager.getCurrSid(),hPendingCount,0);
		EHPendingPanel.iTotalCount = Integer.parseInt(hPendingCount.get(0).toString());
		Vector hPendingFiles =new Vector();
		AIPManager.getVWC().getAIPHistoryFiles(AIPManager.getCurrSid(),hPendingFiles,0,EHPendingPanel.iMaximumRows,0);
		EHD.PendingPane.PFL.LoadTableData(hPendingFiles);
		EHPendingPanel.iStartIndex    = 0;
		EHPendingPanel.iEndIndex      = EHPendingPanel.iMaximumRows;




	}
	public void removeGeneralData(){
		EHD.TempPane.MDT.setMapDoctypes(null);
		EHD.TempPane.MDT.setNodesNavigator(null);
		EHD.TempPane.RemoveAllMDTRows();      
		EHD.HistoryPane.RemoveAllPFLRows();
		EHD.HistoryPane.PFL.LoadTableData(null); 
		EHD.PendingPane.RemoveAllPFLRows();
		EHD.PendingPane.PFL.LoadTableData(null); 
	}     
//	---------------------------------CHeck Forms Actions---------------------------------------------------------
	public TreeMap getEHFileForms(String location){

		java.io.File FFile =EHD.EFManager.getEHFormsFile(location);
		if(FFile !=null && EHD.EFManager.isEHFile(FFile,"forms"))
		{         
			java.util.Date LastModifiedDate =new java.util.Date(FFile.lastModified());
			Date formModifiedDate = EyesHandsManager.getLastModifiedFormLocation(location); 
			if(formModifiedDate != null)
			{    
				if(LastModifiedDate.after(formModifiedDate))
				{
					//VWLogger.debug("In the location <"+location+"> EHForms File modified in "+LastModifiedDate.toString());
					EyesHandsManager.setLastModifiedFormLocation(location,LastModifiedDate) ;
					return EHD.EFManager.getForms();
				}else{
					//VWLogger.debug("EHForms File not modified");
					return new TreeMap();
				}
			}else
			{
				LastModifiedDate =new java.util.Date(FFile.lastModified());
				EyesHandsManager.setLastModifiedFormLocation(location,LastModifiedDate) ;
				return EHD.EFManager.getForms();
			}
		}else{
			// JOptionPane.showMessageDialog(this,connectorManager.getString("General.Msg.NoEHFormFile"),connectorManager.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);
			return null;
		}
	}
//	---------------------------------CHeck Forms Actions---------------------------------------------------------
	public TreeMap getEHFileForms(boolean CheckModified){
		//EyesFileManager EFM =new EyesFileManager();
		java.io.File FFile =EHD.EFManager.getEHFormsFile(EyesHandsManager.getInitParams(AIPManager.getCurrSid()).getDefaultLinkedFolder());
		if(FFile !=null && EHD.EFManager.isEHFile(FFile,"forms")){
			if(CheckModified){
				//if(Log.LOG_DEBUG) Log.debug("Check EHForms File ");
				java.util.Date LastModifiedDate =new java.util.Date(FFile.lastModified());
				if(LastModifiedDate.after(EyesHandsManager.FormsFileLastModified)){
					//if(Log.LOG_DEBUG) Log.debug("EHForms File modified in "+LastModifiedDate.toString());
					EyesHandsManager.FormsFileLastModified=LastModifiedDate ;
					return EHD.EFManager.getForms();
				}else{
					//if(Log.LOG_DEBUG) Log.debug("EHForms File not modified");
					return new TreeMap();
				}
			}else{
				java.util.Date LastModifiedDate =new java.util.Date(FFile.lastModified());
				EyesHandsManager.FormsFileLastModified=LastModifiedDate ;
				return EHD.EFManager.getForms();
			}
		}else{
			JOptionPane.showMessageDialog(this,connectorManager.getString("General.Msg.NoEHFormFile"),connectorManager.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);
			return null;
		}
	}

	public boolean CheckEHFormsFileUpdate(TreeMap FileForms){
		//if(Log.LOG_DEBUG) Log.debug("Check EHForms File Update ");
		int lastFCount=EHForms.size();
		VWDoctypes = EHD.TempPane.MDT.getMapDoctypes();
		boolean Update= EHManager.RefreshDBFormsWithFileForms(EHForms,FileForms,AIPManager.getCurrSid()) ;
		EHD.TempPane.MDT.TableModel.RemoveAllRows();
		EHD.TempPane.MDT.LoadTableData(EHForms);
		EHD.TempPane.MDT.isTableUnsave =false;
		EHD.TempPane.Savebtn.setEnabled(false);
		if(lastFCount == 0) Update = false;
		//if(Log.LOG_DEBUG) Log.debug("EHForms File Update :"+Update);
		return Update;
	}

//	---------------------------------Process Actions-------------------------------------------
	public boolean checkTemplate(){   

		if (!EyesHandsManager.tempDisabled)
		{      
			if(!EHD.TempPane.MDT.CheckUnSaveBeforeChange(connectorManager.getString("General.Msg.SaveTemplete2")))
				return false;
			Vector forms =new Vector();
			int ret =AIPManager.getVWC().getAIPForms(AIPManager.getCurrSid(),forms);
			if(ret == AIPManager.NOERROR)
			{    
				EHForms=forms;   
				if(!checkEHFormDestinationNode()) return false;
			}   


		}      

		VWLogger.debug("Check template : Ok");
		return true;
	}
	public boolean checkEHFormDestinationNode(){
		for(int i=0;i<EHForms.size();i++){
			int Nodeid =((AIPForm)EHForms.get(i)).getDesNodeId();
			int Status = ((AIPForm)EHForms.get(i)).getStatus() ;
			boolean Exsits =((AIPForm)EHForms.get(i)).isExsits() ;
			if (Nodeid > 0 && (Exsits && Status != AIPForm.FIELDSNEEDMAP ) ){
				if( !DocumentsManager.isNodeidExists(AIPManager.getCurrSid(),Nodeid)){
					JOptionPane.showMessageDialog(this,connectorManager.getString("General.Msg.NoFormDefaultNode"),connectorManager.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);
					VWLogger.error("In check Template Forms Destination Node : Missing Nodeid "+Nodeid,null );
					return false;
				}
			}
		}
		VWLogger.debug("Check Template Forms Destination Node : OK");
		return true;
	}
	public boolean refreashTemplateFromLocation(String location){
		TreeMap FileForms =getEHFileForms(location);
		if(FileForms == null) return false;
		if(CheckEHFormsFileUpdate(FileForms)) {
			VWLogger.debug(" Template discrepancies has been detected");
			int result =  JOptionPane.showConfirmDialog(this,connectorManager.getString("General.Msg.TemplateChange"),connectorManager.getString("Init.Msg.Title"),JOptionPane.YES_NO_OPTION,JOptionPane.INFORMATION_MESSAGE);
			if(result == JOptionPane.NO_OPTION){
				return false;
			}
		}
		return true;
	}


	//add ayman 24-07
	public synchronized void updateProgressBar(LocationStatus locStatus)
	{
		if(!locStatus.getName().equalsIgnoreCase((String)inputFolderList.getSelectedValue())) 
			return;
		progressBar.setValue(locStatus.getProgressVal()); 
		progressBar.setString(locStatus.getProgressStr()); 
	}    
	//add ayman 24-07
	public synchronized void updateStatusText(LocationStatus locStatus)
	{
		if(!locStatus.getName().equalsIgnoreCase((String)inputFolderList.getSelectedValue())) 
			return; 
		int locNo =inputFolderList.getSelectedIndex()+1;
		if(locNo > 0)

			CurrState.setText(connectorManager.getString("EHGeneralPanel.Location")+locNo+"): "+locStatus.getStatusText());
		else
			CurrState.setText(locStatus.getStatusText());  
	}

	public static boolean checkAIPServiceAndApplication()
	{
		if(AIPManager.isAIPApplicationStarted() && AIPManager.isAIPServiceStarted())
		{
			if(AIPManager.isAutoStart())
			{
				AIPManager.setAIPServiceStarted(false);
				FoldersMonitoring.stopAIPService();
			}
		}
		return true;
	}

	/// Add by raj

	public class DirectoryFilter implements FileFilter{              
		public boolean accept(File f){
			if(f.isDirectory()) return true;         
			return false;
		}

	}
	public void getFiles(File folder,Vector vGFiles ,boolean encludeSubdirs)
	{
		if(!folder.isDirectory()) return;
		File[] aGFiles = folder.listFiles(new DirectoryFilter()); 
		int fileCount = aGFiles.length;
		vGFiles.add(folder);
		if(encludeSubdirs)
		{
			File[] aDirs =folder.listFiles(new DirectoryFilter());
			if(aDirs !=null && aDirs.length >0)
			{    
				for(int j=0;j<aDirs.length;j++)
				{
					getFiles(aDirs[j],vGFiles,encludeSubdirs);
				}    
			}
		} 
	}  

	//update ayman 24-07
	public boolean startProcess(LocationStatus locStatus)
	{
		VWLogger.info("startProcess..... ");
		int locOrder =AIPManager.getLocationStatusOrder(AIPManager.getCurrSid(),locStatus.getName());
		AIPInit iInfo =  EyesHandsManager.getInitParams(AIPManager.getCurrSid());
		EyesFileManager EFManager = new EyesFileManager();;
		CustomFileManager CFManager =new CustomFileManager();
		if (!EyesHandsManager.tempDisabled)
		{            
			EFManager.setFormsFileName(AIPUtil.getString("Init.FormsFile.Name"));
			EFManager.setDocsFileExt(AIPUtil.getString("Init.FormsDocsFile.Ext"));
		}    
		else
		{               
			CFManager.setFormsFileName(AIPUtil.getString("Init.FormsFile.Name"));
			CFManager.setFormsDocsFileExt(AIPUtil.getString("Init.FormsDocsFile.Ext"));
			CFManager.setDocsFileExt(AIPUtil.getString("Init.DocsFile.Ext"));
			CFManager.setTempDocsFileExt(AIPUtil.getString("Init.TempDocsFile.Ext"));   
		}     

		//Code added by Srikanth on 01 Dec 2016
		StartFlag = 1;
		
		String processLoc = locStatus.getName();
		VWLogger.info("Process Location  :  "+processLoc);
		File folder =new File(processLoc);
		VWLogger.info("Selected folder is : " + folder );
		if(!folder.isDirectory())
		{
			VWLogger.audit("Location("+(locOrder+1)+"):"+"Import process ended for location :"+locStatus.getName());
			return false ;
		}              
		if(inputFolderList.getSelectedIndex() < 0)
		{
			inputFolderList.setSelectedValue(processLoc,true);
			iInfo.setDefaultLinkedFolder(processLoc);
		}
		else
		{
			String sloc =(String)inputFolderList.getSelectedValue();
			LocationStatus sLocStatus =AIPManager.getLocationStatus(AIPManager.getCurrSid(),sloc);
			if(sLocStatus ==null || sLocStatus.getStatus() !=sLocStatus.PROCESS) 
			{
				inputFolderList.setSelectedValue(processLoc,true);
				iInfo.setDefaultLinkedFolder(processLoc);
			}    
		}    
		locStatus.setProgressVal(0);
		locStatus.setProgressStr("");
		updateProgressBar(locStatus);             
		locStatus.setStatusText(connectorManager.getString("General.Lab.InputFolderListTitle")+": "+processLoc);             
		updateStatusText(locStatus);             
		Vector vGFiles =new Vector();
		if (EyesHandsManager.processSubdirs)
			getFiles(folder,vGFiles,EyesHandsManager.processSubdirs);
		else
			vGFiles.add(folder);
		//VWLogger.info("	vGFiles.add(folder.getName()) " + folder.getName());
		//File[] gFiles =converter.getFiles(folder); 
		//if(gFiles.length == 0)  return true;
		if(vGFiles.size() ==0)  return true;
		VWLogger.audit("Location("+(locOrder+1)+"):"+" Import process started for location :"+locStatus.getName());
		VWLogger.info(" fileList size " +  	vGFiles.size());
		for (int fileList = 0; fileList < vGFiles.size(); fileList++)
		{   

			File[] pFiles;
			File tempFile = null;
			File gFile = null;//(File)vGFiles.get(j);                
			String gFileName = "";//gFile.getName();

			// Start Copying Remote archive into local cache folder
			try{
				tempFile = (File)vGFiles.get(fileList);
				VWLogger.info("Remote file path :"+(tempFile).getCanonicalPath());
				{
					gFile = (File)vGFiles.get(fileList);
					//VWLogger.info("gFile  ******************** "  +gFile );
					gFileName = gFile.toString();
					//VWLogger.info("gFileName  ******************** "  +gFileName );
					//if(EyesHandsManager.processSubdirs)
					//	gFileName =gFile.getPath().substring(processLoc.length()+1,gFile.getPath().length());                     
				}
			}catch(Exception ex){
				VWLogger.error("Exception while copying remote file !",ex);
			}
			//VWLogger.info("EyesHandsManager.tempDisable " + EyesHandsManager.tempDisabled);
			if (!EyesHandsManager.tempDisabled)
			{
				TreeMap FileForms =getEHFileForms(gFileName);
				if(FileForms == null) return true;
				if(FileForms.size() >0)
				{    
					if( CheckEHFormsFileUpdate(FileForms))
					{
						VWLogger.debug("Location("+(locOrder+1)+"):"+" Template discrepancies has been detected");
						int result =  JOptionPane.showConfirmDialog(this,connectorManager.getString("General.Msg.TemplateChange"),connectorManager.getString("Init.Msg.Title"),JOptionPane.YES_NO_OPTION,JOptionPane.INFORMATION_MESSAGE);
						if(result == JOptionPane.NO_OPTION){
							return false;
						}
					}                      
				}                
				pFiles = EHD.EFManager.getEHDocumentsFiles(gFileName);             
				if(pFiles == null || pFiles.length == 0) 
				{ 
					VWLogger.info("Location("+(locOrder+1)+"): No BIF/SHD File Found");
					return true;
				}    
			}
			else
			{
				VWLogger.info("gFileName " + gFileName);        		 
				File[] DocFiles =CFManager.getEHDocumentsFiles(gFileName);        
				File[] ShadowFiles = CFManager.getEHDocumentsShadowFiles(gFileName);
				if (DocFiles == null || ShadowFiles == null)
				{
					VWLogger.info("Location("+(locOrder+1)+"): No BIF/SHD File Found");
					return true;

				}
				//concatenate both arrays into Files
				pFiles = new File[DocFiles.length + ShadowFiles.length];                   
				for (int j = 0; j < ShadowFiles.length; j++)
				{
					pFiles[j] = ShadowFiles[j];
				}
				for (int j = 0; j < DocFiles.length; j++)
				{
					pFiles[j + ShadowFiles.length] = DocFiles[j];
				} 
			} 
			//If in root folder file length is zero it should return here. It should continue the loop
			//if(pFiles.length == 0) return true;
			VWLogger.info("Location("+(locOrder+1)+"):"+"Import process started for location :"+locStatus.getName());  
			if(pFiles.length == 0) {
				VWLogger.info("Location("+(locOrder+1)+"): No BIF/SHD File Found");
				continue;
			}
			
			VWLogger.info("Location("+(locOrder+1)+"): <"+pFiles.length +"> Document File(s) Found");             

			for (int j = 0; j < pFiles.length; j++)
			{   
				if(!checkAIPServiceAndApplication()) return false;
				AIPConnector.checkManualShutDown();
				if(ProcessStop || locStatus.getStatus() == locStatus.STOP){
					return false;
				}
				locStatus.setStatusText("Processing File"+" "+pFiles[j].getName());
				updateStatusText(locStatus);       
				VWLogger.info("Location("+(locOrder+1)+"):"+"Processing File"+" "+pFiles[j].getName());                   
				File bifFile =pFiles[j];
				if (bifFile != null){
					if (bifFile.length() <= 0) continue;
				}
				try{
					VWLogger.info("Before calling ProcessAIPFile.....");
					if (ProcessAIPFile(bifFile,locStatus,EFManager,CFManager))
					{                      
						File f;
						if (!EyesHandsManager.tempDisabled)
							f = EFManager.GetUsedFile();
						else
							f = CFManager.GetUsedFile();
					
						/**
						 * Condition added for autostart fix due to network failure 
						 */
						VWLogger.info("serverfailure flag before delete :::"+serverfailure);
						// This condition has added because SHD file should not get deleted on click of stop button
						VWLogger.info("ProcessStop :"+ProcessStop+" locStatus.getStatus() :"+locStatus.getStatus());
						if(serverfailure==false){
							VWLogger.info("Before deleting the file:::");
							if(f.delete()){
								VWLogger.debug("Location("+(locOrder+1)+"):"+"AIPDocument File <"+pFiles[j].getName()+"> Deleted ");   
							}
						}	
												
					} /*else {
						if(ProcessStop || locStatus.getStatus() == locStatus.STOP){
							return false;
						}
					}*/
				} catch(Exception e) {
					if(e.getMessage().equalsIgnoreCase("Process Stopped by the User!"))
					{
						VWLogger.audit("Location("+(locOrder+1)+"):"+"Process Stopped by the User!");
						return false;
					}    
					//e.printStackTrace();
					VWLogger.error("Location("+(locOrder+1)+"):"+" In Processing File <"+pFiles[j].getName()+"> :",e );
					locStatus.setProgressVal(0);
					locStatus.setProgressStr("");
					updateProgressBar(locStatus);  
					return true;   
				}

			}//inner for loop end 
		}//outer for loop end
		locStatus.setProgressVal(0);
		locStatus.setProgressStr("");
		updateProgressBar(locStatus);  


		if (AIPManager.getPollinMode() == EVERY){
			long lCurTime = System.currentTimeMillis();
			pollTime = Long.parseLong(String.valueOf(sPollTime.getValue()));
			pollTime = lCurTime + (pollTime * 60 * 1000);
			Date date = new Date(pollTime);
			String nextPollTime = date.getHours()+":"+date.getMinutes()+":"+date.getSeconds();
			SimpleDateFormat formatter= new SimpleDateFormat ("HH:mm:ss");
			String dateString = formatter.format(date);

			lPollingInfo.setText(connectorManager.getString("EHGeneralPanel.NextPolling")+" "+ dateString);

		}else if(AIPManager.getPollinMode() == DAILY_AT){
			String nextPollTime = AIPManager.getScheduleTime();
			lPollingInfo.setText(connectorManager.getString("EHGeneralPanel.NextPolling")+" "+ nextPollTime);				
			
		}else
			lPollingInfo.setText("");

		//VWLogger.info("*** Import Process Ended ***");
		return true;          
	}     

	//now, can either get forms from file or from templates
	public AIPForm getAIPForm(String FormName, boolean fromTemplates){
		/**CV10.2 - Try catch block added. Added by Vanitha S**/
		try {
			Vector aipForms = this.EHForms;
			// refresh forms from templates if option is true
			if (fromTemplates) {
				Vector forms = new Vector();
				int ret = AIPManager.getVWC().getAIPForms(AIPManager.getCurrSid(), forms);
				if (ret == AIPManager.NOERROR)
					EHForms = forms;
			}
			for (int i = 0; i < EHForms.size(); i++) {
				if (FormName.equalsIgnoreCase(EHForms.get(i).toString()))
					return (AIPForm) EHForms.get(i);
			}
		} catch (Exception e) {
			VWLogger.info("Exception in getAIPForm :"+e.getMessage());	
		}
		return null;
	}

	/* returns true if it processes the file successfully (regardless of whether
	 * the documents processed were pending or not) and false otherwise.
	 */
	public boolean ProcessAIPFile(File file,LocationStatus locStatus,
		EyesFileManager EFManager,CustomFileManager CFManager){
		int locOrder =AIPManager.getLocationStatusOrder(AIPManager.getCurrSid(),locStatus.getName());
		if((EyesHandsManager.tempDisabled && CFManager.isEHFile(file,"docs")) 
				|| (!EyesHandsManager.tempDisabled && EFManager.isEHFile(file,"docs"))){
			/**
			 * Condition added for autostart fix due to network failure 
			 * before processing the documents
			 */
			VWLogger.info("First line of processAIPFile");
			Session s =AIPManager.getSession(AIPManager.getCurrSid());
			VWLogger.info("s.server :::::"+s.server);
			int status = AIPManager.getServersStatus(s.server);
			VWLogger.info("server status in startprocess ::::"+status);
			if(status==-1){
				VWLogger.info("Server failure in ProcessAIPFile first");
				serverfailure=true;
				VWLogger.info("Calling clickstartstop automatically first::::::::");
				try {
					VWLogger.info("***********************************************************************");
					//If condition added by madhavan 01 Dec 2016
					if(!Startbtn.getText().equals(connectorManager.getString("General.Btn.Start"))) {
						VWLogger.info("Inside button stop condition during server error...");
						clickStartStop();
					}

					int aipautostartDelay=AIPManager.getAipAutoStartDelayTime();
					VWLogger.info("aipautostartDelay is :"+aipautostartDelay);
					Thread.sleep(aipautostartDelay);
					return false;
				} catch (InterruptedException e1) {
					VWLogger.info("Exception while stopping aip automatically first:"+e1.getMessage());
				}
			}

			VWLogger.info("Location("+(locOrder+1)+"):"+"Extracting documents from file <"+file.getName()+">");
			locStatus.setStatusText(AIPUtil.getString("General.Lab.ParsingFile")+" "+file.getName());
			updateStatusText(locStatus);

			locStatus.setProgressVal(0);
			updateProgressBar(locStatus);          

			//VWLogger.debug("Location("+(locOrder+1)+"):"+"Begin Timing CFManager.getDocuments: ");			
			TreeMap FDocs;
			String imageTempLocation = null;
			File tempBifFile = null;
			if (!EyesHandsManager.tempDisabled) {
				VWLogger.info("Bif File Path X:"+EFManager.GetUsedFile());
				tempBifFile = EHD.PManager.moveBifFileToImageLocation(EFManager.GetUsedFile());
				//imageTempLocation = EHD.PManager.createImageTempLocation(EFManager.GetUsedFile(), null);
				VWLogger.info("Before calling getDocuments....");
				FDocs = EFManager.getDocuments();
			} else {
				VWLogger.info("Bif File Path Y:"+CFManager.GetUsedFile());				
				tempBifFile = EHD.PManager.moveBifFileToImageLocation(CFManager.GetUsedFile());
				//imageTempLocation = EHD.PManager.createImageTempLocation(CFManager.GetUsedFile(), null);	
				VWLogger.info("Before calling getDocuments....");
				FDocs = CFManager.getDocuments();
			}
			VWLogger.info("Temp bif file location :"+tempBifFile); 
			VWLogger.info("getDocuments return value :"+FDocs);
			if (FDocs == null) {
				VWLogger.info("Location(" + (locOrder + 1) + "):" + "Fail to extract documents from file <" + file.getName() + ">.");
				return false;
			}   
			VWLogger.info("Location("+(locOrder+1)+"):"+"<"+FDocs.size()+"> Documents have been extracted.");
			//VWLogger.debug("Location("+(locOrder+1)+"):"+"End Timing CFManager.getDocuments() " );
			locStatus.setStatusText(AIPUtil.getString("General.Lab.ProcessingFile")+" "+file.getName());
			updateStatusText(locStatus);

			locStatus.setProgressVal((int)(.1 * progressBarMax));
			updateProgressBar(locStatus); 


			// added  to return to the current sid
			int currSid =AIPManager.getCurrSid();
			VWLogger.info("Before calling AddNewAIPDocumentsToVW.......");
			int docImport= AddNewAIPDocumentsToVW(FDocs,locStatus,CFManager,tempBifFile);
			VWLogger.info("ProcessStop :"+ProcessStop+" locStatus.getStatus() :"+locStatus.getStatus());
			/**CV10.2 - if condition had added to check whether stop button clicked when document is in process . Added by Vanitha S**/
			if(ProcessStop || locStatus.getStatus() == locStatus.STOP){
				CFManager.close();
				VWLogger.info("Before process stopped by user");
				throw new RuntimeException("Process Stopped by the User!");		
			}
			VWLogger.info("After calling AddNewAIPDocumentsToVW......."+docImport);
			if (docImport >= 0) {
				if (processingDocDetails != null && processMonitoringDetails != null) {
					processingDocDetails.setProcessStatus(1);
					processMonitoringDetails.setProcessStatus(1);
				}
			}
			AIPManager.setCurrSid(currSid);
			tempBifFile.delete();
			

			locStatus.setStatusText(AIPUtil.getString("General.Lab.SavingFile")+" "+file.getName()+" "+AIPUtil.getString("General.Lab.InHistory"));
			updateStatusText(locStatus);

			locStatus.setProgressVal((int)(.8 * progressBarMax));
			updateProgressBar(locStatus);           

			VWLogger.info("Location("+(locOrder+1)+"):"+"Saving processing file to history"); 
			int Fileid=SaveProcessAIPHistoryFile(file.getPath(), (EyesHandsManager.tempDisabled) ? CFManager.GetUsedFile().lastModified() : EFManager.GetUsedFile().lastModified() ,docImport,FDocs.size()-docImport);
			VWLogger.info("Save the document to History File " + Fileid);
			if(Fileid > 0)
			{  
				if (EyesHandsManager.tempDisabled)
				{
					TreeMap map = new TreeMap();
					String FileidString = Integer.toString(Fileid);
					FileidString = padNumber(FileidString, 10);
					map.put("file_id", FileidString);
					map.put("doc_number", "0");
					CFManager.updateNext(map);
				}
				VWLogger.info("Save the Pending documents to database " + Fileid);
				SaveAIPDocumentsToHistoryFile(Fileid,FDocs,locStatus);
				if (EyesHandsManager.tempDisabled)
					CFManager.close();
				Date ModifiedDate =new Date(file.lastModified());
				String Modified =AIPUtil.SetDateMaskedValue(AIPUtil.MASKDATE,ModifiedDate);
				String Processed =AIPUtil.SetDateMaskedValue(AIPUtil.MASKDATE,new Date());         
				VWLogger.audit("Location("+(locOrder+1)+"):"+"File:<" + file.getName() + ">\t" + "Created:<" + Modified + ">\t" + "Processed:<" + Processed + ">\t" + "Docs Imported:<" + docImport + ">\t" + "Docs Pending:<" + (FDocs.size() - docImport) + ">");
			}
			else
			{   
				if (EyesHandsManager.tempDisabled)
					CFManager.close(); 
				VWLogger.error("Location("+(locOrder+1)+"):"+" will saving processing file ->"
						+AIPManager.getErrorDescription(Fileid)+"("+Fileid+")",null);    
				if(Fileid == ViewWiseErrors.ServerNotFound || Fileid==ViewWiseErrors.ServerNotAvailable
						||Fileid == ViewWiseErrors.invalidSessionId )
				{  
					/**
					 * Condition added for autostart fix due to network failure 
					 */
					if(AIPManager.isAIPServiceStarted()==true){
						VWLogger.info("Before stoping contenverse automatically 1");
						VWLogger.audit(Constants.PRODUCT_NAME +" Server connection lost! Shutting down...");
						//System.exit(0);
						serverfailure=true;
						VWLogger.info("Before calling clickstartstop automatically::::::::");

						try {
							if(!Startbtn.getText().equals(connectorManager.getString("General.Btn.Start"))) {
								VWLogger.info("Stoting during server not found and invalid session..");
								clickStartStop();
							}

							int aipautostartDelay=AIPManager.getAipAutoStartDelayTime();
							VWLogger.info("aipautostartDelay is :"+aipautostartDelay);
							VWLogger.info("***********************************************************************");
							Thread.sleep(aipautostartDelay);
							VWLogger.info("Before returning false");
							return false;
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							VWLogger.info("Exception while stopping aip automatically :"+e1.getMessage());
						}
						VWLogger.info("after calling clickstartstop automatically::::::::");
					}
					if(AIPManager.isAIPApplicationStarted()==true){
						AIPManager.logoutAll();
						AIPManager.shutdown();
						VWLogger.audit(Constants.PRODUCT_NAME +" Server connection lost! Shutting down...");
						AIPManager.setAIPApplicationStarted(false);
						System.exit(0);
					}
				}  
			}  

		}else {
			VWLogger.error("Location("+(locOrder+1)+"):"+" Can not verify the file <"+file.getName()+">",null);
		}
		
		return true;
	}

	/** added by amaleh on 2003-12-11
	 * pads numbers within strings with zeros
	 */
	public static String padNumber(String s, int n)
	{
		if (s.length() < n)
		{
			StringBuffer sb = new StringBuffer(s);
			int ns = n - s.length();
			for (int i = 0; i < ns; i++)
				sb.insert(0, '0');
			return sb.toString();
		}
		else
			return s;
	}
	/* modified on 2003-12-18
	 * Now takes file path and modified date instead of file.
	 */
	public int SaveProcessAIPHistoryFile(String filePath,long lastModified, int imported,int Pending){
		AIPFile aipFile =new AIPFile();
		aipFile.setId(0);
		aipFile.setFilePath(filePath);
		Date ModifiedDate =new Date(lastModified);
		String Modified =AIPUtil.SetDateMaskedValue(AIPUtil.MASKDATE,ModifiedDate);
		String Processed =AIPUtil.SetDateMaskedValue(AIPUtil.MASKDATE,new Date());
		aipFile.setCreationDate(Modified);
		aipFile.setProcessDate(Processed);
		aipFile.setImported(imported);
		aipFile.setPending(Pending);
		int Fileid =AIPManager.getVWC().setAIPHistoryFile(AIPManager.getCurrSid(),aipFile);     
		/*
		 * Enhancement 	: Enhancements of AIP  
		 * 				  Requirment sent by Rajesh on 09-June-2006 through eMail  	 
		 * Created By		: M.Premananth
		 * Date			: 14-June-2006
		 */
		if(Pending == 0)
			EHD.HistoryPane.PFL.addTableRow(aipFile);
		else
			EHD.PendingPane.PFL.addTableRow(aipFile);
		return Fileid;
	}
	/**/
	public void SaveAIPDocumentsToHistoryFile(int FileId,TreeMap FDocs,LocationStatus locStatus){
		/**CV10.2 - Try catch block added . Added by Vanitha S**/
		try {
			int stepBar =((int)(.2 * progressBarMax));
			if(FDocs.size() >0) stepBar = ((int)(.2 * progressBarMax/ FDocs.size()));
			Collection col= FDocs.values();
			int docNum = 1;
			for (Iterator val=FDocs.values().iterator(); val.hasNext(); ) {
				AIPDocument aipDoc =(AIPDocument)val.next();
				aipDoc.setFileId(FileId);
				if(aipDoc.getStatus() == aipDoc.PENDING)
				{                  
					int ret = AIPManager.getVWC().setAIPDocument(AIPManager.getCurrSid(),aipDoc);
					VWLogger.info(aipDoc.getId() + " ::: Saving Pending documents  to Database " + ret );								
	
					if(ret <=AIPManager.NOERROR)
						VWLogger.error("error will saving pending aipDocument "+AIPManager.getErrorDescription(ret)+"("+ret+")",null);
				} 
				locStatus.setProgressVal(locStatus.getProgressVal()+stepBar);
				locStatus.setProgressStr(Integer.toString(docNum++) + "/" + Integer.toString(FDocs.size()));
				updateProgressBar(locStatus);
			}
		} catch (Exception e) {
			VWLogger.info("Exception in SaveAIPDocumentsToHistoryFile method " + e.getMessage());
		}
	}
	public int AddNewAIPDocumentsToVW(TreeMap FileDocsMap,LocationStatus locStatus,
			CustomFileManager CFManager, File tempBifFile){
		int DocSaveNum = 0; 
		int isPrimitiveDocs = 0;
		int isConvertToDJVU = 0;
		String processLocation = locStatus.getName();
		Iterator iterator = FileDocsMap.keySet().iterator();

		int stepBar =((int)(.7 * progressBarMax));
		if(FileDocsMap.size() >0) {
			VWLogger.info("Size of the document :"+FileDocsMap.size());
			stepBar =((int)(.7 * progressBarMax / FileDocsMap.size()));
		}
		
		int docNum = 0;
		int iterations = 0;
		int delay = 0;
		//keeps track of the number of special docs encountered
		int nextSpecialDocsNameNumber = FileDocsMap.size() + 1;

		locStatus.setProgressStr(Integer.toString(docNum++) + "/" + Integer.toString(FileDocsMap.size()));
		updateProgressBar(locStatus);
		VWLogger.info("locStatus.getName() before calling getLocationStatusOrder :: " + locStatus.getName());
		int locOrder =AIPManager.getLocationStatusOrder(AIPManager.getCurrSid(),locStatus.getName());
		VWLogger.info("After calling getLocationStatusOrder :" + locOrder);
		isPrimitiveDocs = AIPManager.isPrimitiveDocs();
		isConvertToDJVU = AIPManager.isConvertToDJVU();
		String imageTempLocation = null;
		while (iterator.hasNext()){

			AIPConnector.checkManualShutDown();

			if(ProcessStop || locStatus.getStatus() == locStatus.STOP){
				CFManager.close();
				VWLogger.info("Before process stopped by user");
				throw new RuntimeException("Process Stopped by the User!");		
			}    

			//VWLogger.ver_debug("Location("+(locOrder+1)+"):"+"Begin Total Document Timing");                       
			String Name = iterator.next().toString();
			VWLogger.info("Processing document.... "+Name);
			AIPDocument aipDoc = (AIPDocument)FileDocsMap.get(Name);
			
			setProcessDocDetails(Name);
			
			VWLogger.info("Location("+(locOrder+1)+"):"+"Adding AIPDocument <"+aipDoc.getName()+"> To "+ Constants.PRODUCT_NAME);
			int sid =AIPManager.getCurrSid();

			String FormName=aipDoc.getAIPForm().getName();
			VWLogger.audit("Document :<" + Name + ">\t started processing....");

			Document vwDoc = null;
			AIPForm aipForm;
			String docImpMsg = null;
			if (EyesHandsManager.tempDisabled)                
			{
				try {
					if (aipDoc.getStatus() != 2)
					{
						if (aipDoc.getStatus() == 1)
						{   
							VWLogger.audit("Location("+(locOrder+1)+"):"+"AIPDocument <"+aipDoc.getName()+"> is already imported");
							DocSaveNum++;  
	
						}else
						{
							VWLogger.audit("Location("+(locOrder+1)+"):"+"AIPDocument <"+aipDoc.getName()+"> is already pending");
						}    
	
						locStatus.setProgressVal(locStatus.getProgressVal()+stepBar);
						locStatus.setProgressStr(Integer.toString(docNum++) + "/" + Integer.toString(FileDocsMap.size()));
						updateProgressBar(locStatus);
						continue;
					}
					Session s = null;
					if (DocumentsManager.calcDepth(aipDoc.getVWNodePath()) >= 4) {
						s = DocumentsManager.getSessionOfNodePath(aipDoc.getVWNodePath());
						VWLogger.info("Node Path session  :::: "+s);
						if(s ==null){
							VWLogger.error("Location("+(locOrder+1)+"):"+" Room was not found in combo box!",null);
							aipDoc.setStatus(aipDoc.PENDING);
							aipDoc.setPendCause(12);
							//update shadow
							TreeMap map = new TreeMap();
							String docNumber = Name.substring(8);
							map.put("doc_number", docNumber);
							map.put("status", Integer.toString(AIPDocument.PENDING));
							map.put("pend_cause", padNumber(Integer.toString(aipDoc.getPendCause()), 2));
							CFManager.updateNext(map);
							/**/     
							docImpMsg = aipDoc.getName()+" is pending because of error " + aipDoc.getPendCause() 
							+ ": "+connectorManager.getString("Pending.Status." + aipDoc.getPendCause());
							VWLogger.audit("Location("+(locOrder+1)+"):"+formatDocMsg(aipDoc, docImpMsg));                    
							locStatus.setProgressVal(locStatus.getProgressVal()+stepBar);
							locStatus.setProgressStr(Integer.toString(docNum++) + "/" + Integer.toString(FileDocsMap.size()));
							updateProgressBar(locStatus);
							continue;                    
						}
					} else {
						s = AIPManager.getSession(AIPManager.getCurrSid());
					}
					sid =s.sessionId;
					if(sid <=0 )
					{    
						sid= internalConnect(s);
						if(sid <=0)
						{
							VWLogger.error(" Room was not found in combo box!",null);
							aipDoc.setStatus(aipDoc.PENDING);
							aipDoc.setPendCause(12);
							//update shadow
							TreeMap map = new TreeMap();
							String docNumber = Name.substring(8);
							map.put("doc_number", docNumber);
							map.put("status", Integer.toString(AIPDocument.PENDING));
							map.put("pend_cause", padNumber(Integer.toString(aipDoc.getPendCause()), 2));
							CFManager.updateNext(map);
							/**/     
							docImpMsg = aipDoc.getName()+" is pending because of error " + aipDoc.getPendCause() 
							+ ": "+connectorManager.getString("Pending.Status." + aipDoc.getPendCause());
							VWLogger.audit("Location("+(locOrder+1)+"):"+formatDocMsg(aipDoc, docImpMsg));                    
							locStatus.setProgressVal(locStatus.getProgressVal()+stepBar);
							locStatus.setProgressStr(Integer.toString(docNum++) + "/" + Integer.toString(FileDocsMap.size()));
							updateProgressBar(locStatus);
							continue;
						}
					//      DocumentsManager.getNavigatorNode(sid,true);
					//      refreashSidNodes.add(Integer.toString(sid));
					}        
					else
					{
						/*  if(!refreashSidNodes.contains(Integer.toString(sid)))
	                    {
	                      DocumentsManager.getNavigatorNode(sid,true);
	                       refreashSidNodes.add(Integer.toString(sid));
	
	                    }    */
					}    
					// VWLogger.ver_debug("Location("+(locOrder+1)+"):"+"Beging Timing TransferAIPDocToVWDoc");
	
					vwDoc = EHManager.TransferAIPDocToVWDoc(sid,locStatus,aipDoc);
					VWLogger.info("TransferAIPDocToVWDoc vwDoc  :::: "+vwDoc);
					// VWLogger.ver_debug("Location("+(locOrder+1)+"):"+"End Timing TransferAIPDocToVWDoc ");
					if(vwDoc == null){                    
						//update shadow
						TreeMap map = new TreeMap();
						String docNumber = Name.substring(8);
						map.put("doc_number", docNumber);
						map.put("status", Integer.toString(AIPDocument.PENDING));
						map.put("pend_cause", padNumber(Integer.toString(aipDoc.getPendCause()), 2));
						CFManager.updateNext(map);
						/**/     
						docImpMsg = aipDoc.getName()+" is pending because of error " + aipDoc.getPendCause() 
						+ ": "+connectorManager.getString("Pending.Status." + aipDoc.getPendCause());
						VWLogger.audit("Location("+(locOrder+1)+"):"+formatDocMsg(aipDoc, docImpMsg));                    
						locStatus.setProgressVal(locStatus.getProgressVal()+stepBar);
						locStatus.setProgressStr(Integer.toString(docNum++) + "/" + Integer.toString(FileDocsMap.size()));
						updateProgressBar(locStatus);
						continue;
					}
					int j;
					for(j=0;j <aipDoc.getAIPForm().getFields().size();j++){
						AIPField DFInfo= (AIPField)aipDoc.getAIPForm().getFields().get(j);
						if(DFInfo.getStatus() ==DFInfo.REJECTED)
						{
							aipDoc.setStatus(aipDoc.PENDING);
							aipDoc.setPendCause(10);
	
							//update shadow
							TreeMap map = new TreeMap();
							String docNumber = Name.substring(8);
							map.put("doc_number", docNumber);
							map.put("status", Integer.toString(AIPDocument.PENDING));
							map.put("pend_cause", padNumber(Integer.toString(aipDoc.getPendCause()), 2));
							Vector fields = aipDoc.getAIPForm().getFields();
							for (int i = 0; i < fields.size(); i++)
							{
								AIPField formfield = (AIPField)(fields.get(i));
								map.put("field" + Integer.toString(i+1) + "_status", Integer.toString(formfield.getStatus()));
							}
							CFManager.updateNext(map);
							/**/   
							docImpMsg = aipDoc.getName()+" is pending because of error " + aipDoc.getPendCause() 
							+ ": "+connectorManager.getString("Pending.Status." + aipDoc.getPendCause());
							VWLogger.audit("Location("+(locOrder+1)+"):"+formatDocMsg(aipDoc, docImpMsg));
	
							break;
						}
					}
					if (j < aipDoc.getAIPForm().getFields().size())
						continue;
				} catch (Exception e) {
					VWLogger.info("Exception while calling getSessionOfNodePath/TransferAIPDocToVWDoc method in AddNewAIPDocumentsToVW :"+e.getMessage());
				}

			}

			else // Eyes Hands Process
			{
				try {
					aipForm=getAIPForm(FormName, true);
					if(aipForm == null) {
	
						aipDoc.setStatus(aipDoc.PENDING);
						aipDoc.setPendCause(1);
						docImpMsg = aipDoc.getName()+" is pending because of error " + aipDoc.getPendCause() 
						+ ": "+connectorManager.getString("Pending.Status." + aipDoc.getPendCause());
						VWLogger.audit("Location("+(locOrder+1)+"):"+formatDocMsg(aipDoc, docImpMsg));
	
						locStatus.setProgressVal(locStatus.getProgressVal()+stepBar);
						locStatus.setProgressStr(Integer.toString(docNum++) + "/" + Integer.toString(FileDocsMap.size()));
						updateProgressBar(locStatus);
						continue;
					}
	
					if( aipForm.getStatus()== aipForm.FIELDSNEEDMAP ){                    
						aipDoc.setStatus(aipDoc.PENDING);
						aipDoc.setPendCause(3);
						docImpMsg = aipDoc.getName()+" is pending because of error " + aipDoc.getPendCause() 
						+ ": "+connectorManager.getString("Pending.Status." + aipDoc.getPendCause());
						VWLogger.audit("Location("+(locOrder+1)+"):"+formatDocMsg(aipDoc, docImpMsg)); 
						locStatus.setProgressVal(locStatus.getProgressVal()+stepBar);
						locStatus.setProgressStr(Integer.toString(docNum++) + "/" + Integer.toString(FileDocsMap.size()));
						updateProgressBar(locStatus);
						continue;
					}
					if (aipForm.getMapDTId() <=0){
	
						aipDoc.setStatus(aipDoc.PENDING);
						aipDoc.setPendCause(2);
						docImpMsg = aipDoc.getName()+" is pending because of error " + aipDoc.getPendCause() 
						+ ": "+connectorManager.getString("Pending.Status." + aipDoc.getPendCause());
						VWLogger.audit("Location("+(locOrder+1)+"):"+formatDocMsg(aipDoc, docImpMsg));
						locStatus.setProgressVal(locStatus.getProgressVal()+stepBar);
						locStatus.setProgressStr(Integer.toString(docNum++) + "/" + Integer.toString(FileDocsMap.size()));
						updateProgressBar(locStatus);
						continue;
					}
	
					DocType dt =DoctypesManager.getDoctype(sid,aipForm.getMapDTId());
					VWLogger.info("get document type dt  :::: "+dt);
					if(dt == null)
					{    
						aipDoc.setStatus(aipDoc.PENDING);
						aipDoc.setPendCause(7);
						docImpMsg = aipDoc.getName()+" is pending because of error " + aipDoc.getPendCause() 
						+ ": "+connectorManager.getString("Pending.Status." + aipDoc.getPendCause());
						VWLogger.audit("Location("+(locOrder+1)+"):"+formatDocMsg(aipDoc, docImpMsg)); 
	
						locStatus.setProgressVal(locStatus.getProgressVal()+stepBar);
						locStatus.setProgressStr(Integer.toString(docNum++) + "/" + Integer.toString(FileDocsMap.size()));
						updateProgressBar(locStatus);
						continue;  
					}
					vwDoc=EHManager.TransferAIPDocToVWDoc(locStatus,aipDoc,aipForm,dt);   
					if(vwDoc == null)
					{                   
						docImpMsg = aipDoc.getName()+" is pending because of error " + aipDoc.getPendCause() + ": "+connectorManager.getString("Pending.Status." + aipDoc.getPendCause());
						VWLogger.audit("Location("+(locOrder+1)+"):"+formatDocMsg(aipDoc, docImpMsg));   
						locStatus.setProgressVal(locStatus.getProgressVal()+stepBar);
						locStatus.setProgressStr(Integer.toString(docNum++) + "/" + Integer.toString(FileDocsMap.size()));
						updateProgressBar(locStatus);
						continue;
					}
				} catch (Exception e) {
					VWLogger.info("Exception while calling getDoctype/TransferAIPDocToVWDoc method in AddNewAIPDocumentsToVW :"+e.getMessage());
				}
			} // End Eyes Hands Process

			/* File[] imageArray =new File[2];
            imageArray[0] =new File("C:\\Output\\BofA\\20031001315000717303.tiff");
            imageArray[1] =new File("C:\\Output\\BofA\\20031001315000048303.tiff");
			 */
			File[] imageArray = null;
			try {
				imageArray = EHD.PManager.parseImageFiles(aipDoc.getImagesPath());
				VWLogger.info("parseImageFiles return value : "+imageArray);
				if(imageArray == null){   							
					aipDoc.setStatus(aipDoc.PENDING);
					aipDoc.setPendCause(4);
					VWLogger.info("pendingErrorMessage :: "+PagesManager.pendingErrorMessage);
					if (PagesManager.pendingErrorMessage == 2) {
						aipDoc.setPendCause(22);					
					} else if (PagesManager.pendingErrorMessage == 7) {
						aipDoc.setPendCause(27);					
					} else if (PagesManager.pendingErrorMessage == 8) {
						aipDoc.setPendCause(30);					
					}
					PagesManager.pendingErrorMessage = 0;
					VWLogger.info("Image Error message : "+aipDoc.getName()+" is pending because of error " + aipDoc.getPendCause() + ": "+connectorManager.getString("Pending.Status." + aipDoc.getPendCause()));
					docImpMsg = aipDoc.getName()+" is pending because of error " + aipDoc.getPendCause() + ": "+connectorManager.getString("Pending.Status." + aipDoc.getPendCause());
					VWLogger.audit("Location("+(locOrder+1)+"):"+formatDocMsg(aipDoc, docImpMsg));
	
					if (EyesHandsManager.tempDisabled)
					{
						TreeMap map = new TreeMap();
						String docNumber = Name.substring(8);
						map.put("doc_number", docNumber);
						map.put("status", Integer.toString(aipDoc.PENDING));
						map.put("pend_cause", padNumber(Integer.toString(4), 2));
						CFManager.updateNext(map);
					}
					locStatus.setProgressVal(locStatus.getProgressVal()+stepBar);
					locStatus.setProgressStr(Integer.toString(docNum++) + "/" + Integer.toString(FileDocsMap.size()));
					updateProgressBar(locStatus);							
					continue;
				} 
				
				
			} catch (Exception e) {
				VWLogger.info("Exception while calling parseImageFiles method in AddNewAIPDocumentsToVW :"+e.getMessage());
			}
			/**
			 * Added for sending the document to pending if the size is more than 100MB(Default)
			 *  or based on the maxdocsize registry set value.
			 *  New pend cause added as 19 if the document size is more than 100MB or registry set value.
			 */
			try {
				boolean retflag=EHD.PManager.getAllowedPageSize(aipDoc.getImagesPath(), imageArray);
				VWLogger.debug("Allowed Page Size return value : "+retflag);
				if(retflag==false){							
					aipDoc.setStatus(aipDoc.PENDING);     
					if (PagesManager.pendingErrorMessage == 5) {// Page file size exceeded 250 MB.
						aipDoc.setPendCause(25);
					} else {
						aipDoc.setPendCause(19); // Image/Page file size is more
					}
					PagesManager.pendingErrorMessage = 0;							 
					if (EyesHandsManager.tempDisabled)
					{
						TreeMap map = new TreeMap();
						String docNumber = Name.substring(8);
						map.put("doc_number", docNumber);
						map.put("status", Integer.toString(aipDoc.getStatus()));
						map.put("pend_cause", padNumber(Integer.toString(aipDoc.getPendCause()), 2));
						CFManager.updateNext(map);
					}
					locStatus.setProgressVal(locStatus.getProgressVal()+stepBar);
					locStatus.setProgressStr(Integer.toString(docNum++) + "/" + Integer.toString(FileDocsMap.size()));
					updateProgressBar(locStatus);
					docImpMsg = aipDoc.getName()+" is pending because of error " + aipDoc.getPendCause() + ": "+connectorManager.getString("Pending.Status." + aipDoc.getPendCause());
					VWLogger.audit(EHGeneralPanel.formatDocMsg(aipDoc, docImpMsg));
					
					continue;
				}
			} catch (Exception e) {
				VWLogger.info("Exception while calling getAllowedPageSize method in AddNewAIPDocumentsToVW :"+e.getMessage());
			}
			int VWDocId =0;
			VWLogger.info("image isAppend :::: "+aipDoc.isAppend());
			if (aipDoc.isAppend())
			{				
				VWLogger.info("Before calling getIDofDoc method .......");
				VWDocId = DocsManager.getIDofDoc(sid, vwDoc); 
				VWLogger.info("Document Id after calling getIDofDoc method :"+VWDocId);
				if(VWDocId > 0 && imageArray.length >0)
				{   
					try {
						VWLogger.debug("Location("+(locOrder+1)+"):"+"pull Pages from document <"+VWDocId+">");                         
						boolean bGetPages =EHD.PManager.pullDocumentPages(sid,VWDocId);
						VWLogger.debug("Location("+(locOrder+1)+"):"+"Downloading document <"+VWDocId+"> status is " + bGetPages );
						// Commented to fixed the issue 1052
						/**Uncommented if condition to fix the issue - AIP creating document with 0 pages when the DSS is inactive**/
						/**Uncommented by Vanitha On 17/01/2020**/
						if(bGetPages)
						{ 
							VWLogger.debug("Location("+(locOrder+1)+"):"+"add new Pages to document <"+VWDocId+">");
							VWLogger.info("Before calling addPagesToDocument isPrimitiveDocs....."+isPrimitiveDocs);
							VWLogger.info("Before calling addPagesToDocument isConvertToDJVU....."+isConvertToDJVU);
							int ret = EHD.PManager.addPagesToDocument(sid,VWDocId,imageArray, vwDoc);
							VWLogger.info("Location" + (locOrder+1) + "):" + "EHGeneralPanel : return value from addPagesToDocument() : "+ret+" : "+AIPManager.getErrorDescription(ret));
							if (ret == ViewWiseErrors.NoError) {
								VWLogger.debug("Location("+(locOrder+1)+"):"+"Pages added successfully ");
							} else {
								aipDoc.setStatus(aipDoc.PENDING);
								if (PagesManager.pendingErrorMessage > 0) {
									if (PagesManager.pendingErrorMessage == 4) {// Not enough memory
										aipDoc.setPendCause(23);
									} else {
										if (isConvertToDJVU != 3) {
											aipDoc.setPendCause(24); // Failed to create all.zip
										} else {
											aipDoc.setPendCause(26); // Failed to convert HC-PDF
										}
									}
									PagesManager.pendingErrorMessage = 0;
								} else {										
									if (ret == ViewWiseErrors.documentAlreadyLocked) {
										aipDoc.setPendCause(13);
									} else if (ret == ViewWiseErrors.pageCountMismatch) {

										aipDoc.setPendCause(21);
									} else {
										aipDoc.setPendCause(14);
									}
								}
								docImpMsg = "Location" + (locOrder+1) + "):" + aipDoc.getName()+" is pending because of error " + aipDoc.getPendCause() + ": "+AIPUtil.getString("Pending.Status." + aipDoc.getPendCause());
								VWLogger.audit(EHGeneralPanel.formatDocMsg(aipDoc, docImpMsg));
								/*if (PagesManager.pendingErrorMessage == 4) {
									VWLogger.info("Not enough memory......");
									PagesManager.pendingErrorMessage = 0;
									stopAIPProcess(true);
									break;
								} else*/
									continue;
							}
						} else {
							/***Added to fix issue - AIP creating document with 0 pages when the DSS is inactive.Added by vanitha on 17/01/2020 ***/
							VWLogger.info("pullDocumentPages method error message : "+PagesManager.pendingErrorMessage);
							if (PagesManager.pendingErrorMessage < 0) {
								aipDoc.setStatus(aipDoc.PENDING);
								if(PagesManager.pendingErrorMessage==ViewWiseErrors.dssCriticalError){
									aipDoc.setPendCause(15);
			
									JOptionPane.showMessageDialog(this,connectorManager.getString("EHGeneral.DSSCritical"));
									if(Startbtn.getText().equalsIgnoreCase("Stop")){
										
										Startbtn.doClick();
										
									}
								} else if(PagesManager.pendingErrorMessage == ViewWiseErrors.dssInactive){
									aipDoc.setPendCause(11);
								} else {
									aipDoc.setPendCause(5);
								}
								PagesManager.pendingErrorMessage = 0;
								docImpMsg = "Location" + (locOrder+1) + "):" + aipDoc.getName()+" is pending because of error " + aipDoc.getPendCause() + ": "+AIPUtil.getString("Pending.Status." + aipDoc.getPendCause());
								VWLogger.audit(EHGeneralPanel.formatDocMsg(aipDoc, docImpMsg));
								continue;
							}							
						}
						vwDoc.setId(VWDocId); 
					} catch (Exception e) {
						VWLogger.info("Exception while pulling and adding pages to documents :"+e.getMessage());
					}

				} 
			}
			int ret = 0;
			if(VWDocId <= 0)
			{    
				// VWLogger.info("***********Create ViewWise document ***********");
				if(imageArray.length >0)
				{   
					try {

						String docFilePath =PagesManager.createPagesTempFolder(sid,locOrder);    
						/*  Issue - 742
						 *   Developer - Nebu Alex 
						 *   Code Description - The following piece of code implements
						 *                      Generate primitive document functionalities 
						 */
	
						// Issue - 742 - Implementing Generate Primitive documnents
						VWLogger.info("Before calling addPagesToTempFolder :....");
						boolean findPages = false;
						VWLogger.info("isPrimitiveDocs....."+isPrimitiveDocs);
						VWLogger.info("isConvertToDJVU....."+isConvertToDJVU);
						if (isPrimitiveDocs == 1 && isConvertToDJVU == 0) {
							findPages=EHD.PManager.addPagesToTempFolderPrimitiveGeneration(sid,locOrder,imageArray);
						}else{
							findPages=EHD.PManager.addPagesToTempFolder(sid,locOrder,imageArray);
						}
						// End of issue
						VWLogger.info("After adding pagges to temp folder :: "+findPages);
						if(!findPages)
						{   
							aipDoc.setStatus(aipDoc.PENDING);         						
							aipDoc.setPendCause(4); 
							VWLogger.info("pendingErrorMessage :: "+PagesManager.pendingErrorMessage);
							if (PagesManager.pendingErrorMessage == 3) {
								aipDoc.setPendCause(21);					
							} else if (PagesManager.pendingErrorMessage == 4) {
								aipDoc.setPendCause(23);
							} else if (PagesManager.pendingErrorMessage == 5) {
								aipDoc.setPendCause(25);
							} else if (PagesManager.pendingErrorMessage == 6) {
								if (isConvertToDJVU != 3) {
									aipDoc.setPendCause(24);
								} else {
									aipDoc.setPendCause(26); // Failed to convert HC-PDF
								}
							}
							if (EyesHandsManager.tempDisabled)
							{
								TreeMap map = new TreeMap();
								String docNumber = Name.substring(8);
								map.put("doc_number", docNumber);
								map.put("status", Integer.toString(aipDoc.getStatus()));
								map.put("pend_cause", padNumber(Integer.toString(aipDoc.getPendCause()), 2));
								CFManager.updateNext(map);
							}   
	
							docImpMsg = aipDoc.getName()+" is pending because of error " + aipDoc.getPendCause() + ": "+AIPUtil.getString("Pending.Status." + aipDoc.getPendCause());
							VWLogger.audit("Location("+(locOrder+1)+"):"+formatDocMsg(aipDoc, docImpMsg));
							/*if (PagesManager.pendingErrorMessage == 4) {
								VWLogger.info("Not enough memory......");
								PagesManager.pendingErrorMessage = 0;
								stopAIPProcess(true);
								break;
							} else*/
								continue;
						} 
						else vwDoc.setVersionDocFile(docFilePath);
					} catch (Exception e) {
						VWLogger.info("Exception while adding pages to temp folder .."+e.getMessage());
					}
				}
				  /*
                 * Enhancemnt:- Unique to room 
                 * Code added to avoid to avoid duplication of Sequence value for unique to room set.
                 * Pendcause 17 Unique sequence violation.
                 */
				try {
					Vector returnVect = new Vector();
					AIPManager.getVWC().checkValidateUniqueSeq(sid, vwDoc.getDocType().getId(), returnVect);
					if (returnVect != null && returnVect.size() > 0) {
						// VWLogger.info("checkValidateUniqueSeq ret
						// val2222"+returnVect);
						if (!returnVect.get(0).toString().equalsIgnoreCase("0")) {
							aipDoc.setStatus(aipDoc.PENDING);
							aipDoc.setPendCause(17);
							docImpMsg = aipDoc.getName()+" is pending because of error " + aipDoc.getPendCause() + ": "+AIPUtil.getString("Pending.Status." + aipDoc.getPendCause());
							VWLogger.audit("Location("+(locOrder+1)+"):"+formatDocMsg(aipDoc, docImpMsg)); 
							break;
						}
					}
				} catch (Exception e) {
					VWLogger.info("Exception in checkValidateUniqueSeq" + e.getMessage());
					break;
				}
				VWLogger.info("session id before calling createdocument ::::"+sid);
				ret =AIPManager.getVWC().createDocument(sid,vwDoc);
				VWLogger.info("return value from createDocument ::: "+ret);
				AIPManager.setAIPConnectionStatus(AIPManager.getCurrSid());
				
				EHD.PManager.deletePagesTempFolder(sid,locOrder);
				if(ret <=0)
				{
					try {
						aipDoc.setStatus(aipDoc.PENDING);
						if(ret==ViewWiseErrors.dssCriticalError){
							aipDoc.setPendCause(15);
	
							JOptionPane.showMessageDialog(this,connectorManager.getString("EHGeneral.DSSCritical"));
							if(Startbtn.getText().equalsIgnoreCase("Stop")){
								
								Startbtn.doClick();
								
							}
						}//Added for Lock document type
						else if(ret==ViewWiseErrors.DocumentTypeNotPermitted){
							aipDoc.setPendCause(16);
						}
						else if(ret == ViewWiseErrors.dssInactive){
							aipDoc.setPendCause(11);
						}//CV10.1 Enhancement-Dynamic Sequence validation. Modified by rkant.
						else if(ret==ViewWiseErrors.CheckUniquenessIndexErr){
							aipDoc.setPendCause(17); 
						} else if(ret==ViewWiseErrors.InvalidDateMask){
							aipDoc.setPendCause(20); 
						} else if (ret==ViewWiseErrors.allDotZipDoesNotExist) { 
							aipDoc.setPendCause(24); 
						} else {
							aipDoc.setPendCause(5);
						}
						if (EyesHandsManager.tempDisabled)
						{
							TreeMap map = new TreeMap();
							String docNumber = Name.substring(8);
							map.put("doc_number", docNumber);
							map.put("status", Integer.toString(aipDoc.getStatus()));
							map.put("pend_cause", padNumber(Integer.toString(aipDoc.getPendCause()), 2));
							CFManager.updateNext(map);
						} 
						VWLogger.error("Location("+(locOrder+1)+"):"+"Fail to create VW document -> "+AIPManager.getErrorDescription(ret)+"<"+ret+">",null);
						docImpMsg = aipDoc.getName()+" is pending because of error " + aipDoc.getPendCause() + ": "+AIPUtil.getString("Pending.Status." + aipDoc.getPendCause());
						VWLogger.audit("Location("+(locOrder+1)+"):"+formatDocMsg(aipDoc, docImpMsg));                    
						if(ret == ViewWiseErrors.ServerNotFound || ret==ViewWiseErrors.ServerNotAvailable
								|| ret == ViewWiseErrors.invalidSessionId)
						{   
							if(AIPManager.isAIPServiceStarted()==true){
								VWLogger.info("Before stoping contenverse automatically 2");
								VWLogger.audit(Constants.PRODUCT_NAME +" Server connection lost! Shutting down...");
								//System.exit(0);
								serverfailure=true;
								VWLogger.info("Before calling clickstartstop 2::::::::");
								//If condition added by madhavan 1 Dec 32016
								if(!Startbtn.getText().equals(connectorManager.getString("General.Btn.Start"))) {
									VWLogger.info("Inside calling clickstartstop 2::::::::");
									clickStartStop();
								}
	
								VWLogger.info("After calling clickstartstop 2::::::::");
								try {
									int aipautostartDelay=AIPManager.getAipAutoStartDelayTime();
									VWLogger.info("aipautostartDelay is :"+aipautostartDelay);
									Thread.sleep(aipautostartDelay);
								} catch (InterruptedException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
								VWLogger.info("after calling clickstartstop ::::::::");
							}
							if(AIPManager.isAIPApplicationStarted()==true){
								AIPManager.logoutAll();
								AIPManager.shutdown();
								VWLogger.audit(Constants.PRODUCT_NAME +" Server connection lost! Shutting down...");
								AIPManager.setAIPApplicationStarted(false);
								System.exit(0);	
							}
						
						}   
						locStatus.setProgressVal(locStatus.getProgressVal()+stepBar);
						locStatus.setProgressStr(Integer.toString(docNum++) + "/" + Integer.toString(FileDocsMap.size()));
						updateProgressBar(locStatus);
					} catch (Exception e) {
						VWLogger.info("Exception while checking AIP services :"+e.getMessage());
					}
					continue;
				}
				VWLogger.debug("Location("+(locOrder+1)+"):"+"Document "+vwDoc.getName()+" have been added to VW with id <"+ret+">"); 
			}

			///Addding the Security
			if (ret > 0 || VWDocId > 0){
				try{
					VWLogger.debug("Location("+(locOrder+1)+"):" + "Document is created and apply the security Permission " + vwDoc.getId());
					VWLogger.info("aipDoc.getSecurityInfo() : "+aipDoc.getSecurityInfo());
					StringTokenizer strSecurityTokens = new StringTokenizer(aipDoc.getSecurityInfo(), "|");
					while (strSecurityTokens.hasMoreTokens()){
						StringTokenizer strSecurity = new StringTokenizer(strSecurityTokens.nextToken(), ",");
						String userGroup =strSecurity.nextToken();
						VWLogger.info("userGroup : "+userGroup);						
						String permissions = strSecurity.nextToken();
						VWLogger.info("permissions : "+permissions);
						int selectedNodeId = 0;
						if ((AIPManager.currentNode != null &&  AIPManager.currentNode.getId() > 0 )&& !AIPPreferences.isSecurityForDoc()) {
							selectedNodeId =AIPManager.currentNode.getId();	            		   
						}
						if (selectedNodeId == 0) selectedNodeId = vwDoc.getId();
						VWLogger.info("Location("+(locOrder+1)+"):" +  " Apply the Security for the : " + selectedNodeId + " : " + userGroup + " : " + permissions);
						int result = AIPManager.getVWC().copyNodeSecurity(sid, selectedNodeId,userGroup , permissions);
						switch(result){
						case 0:
							VWLogger.info("Location("+(locOrder+1)+"):" + "Successfully Adding Security Entry for the : "+ (AIPManager.currentNode != null && selectedNodeId == AIPManager.currentNode.getId()?AIPManager.currentNode.getName():vwDoc.getName())+ " - User/Group Name -> " + userGroup + " : Permissions -> " + permissions);
							break;
						case -1:
							VWLogger.info("Location("+(locOrder+1)+"):" + vwDoc.getName() + " : User/Group Name -> " + userGroup + " not found!");
							break;
						case -2:
							VWLogger.info("Location("+(locOrder+1)+"):" + vwDoc.getName() + " : User/Group Name -> " + userGroup + " : Principal not found!");	            			
							break;
						case -3: 
							VWLogger.info("Location("+(locOrder+1)+"):" + vwDoc.getName() + " : User Group Name -> " + userGroup + " : Permissions -> " + permissions + " : Given Permission and effective Permission are same.");
							break;
						case -4:
							VWLogger.info("Location("+(locOrder+1)+"):" + vwDoc.getName() + " : User Group Name -> " + userGroup + " : Permissions -> " + permissions + " : Given permission same as effective permission, remove the existing security permission!");
							break;
						default:
							VWLogger.info("Location("+(locOrder+1)+"):" + " Error While Adding Security Entry for the node " + vwDoc.getName() + " : User Group Name -> " + userGroup + " : Permissions -> " + permissions);	            			
						}

					}            	   
				}catch(Exception ex){
					VWLogger.debug("Exception in reading the security " + ex.getMessage());            				   
				}
				AIPManager.currentNode  = null;
			}

			DocSaveNum++;           

			if(aipDoc.getCommentFile() !=null && !aipDoc.getCommentFile().trim().equals(""))
			{    
				VWLogger.info("Location("+(locOrder+1)+"):"+"load comment from file <"+aipDoc.getCommentFile()+">");                
				String comment = DocsManager.loadComment(aipDoc.getCommentFile());
				if(comment!=null)
				{    
					int retc =DocsManager.setDocumentComment(sid,vwDoc,comment);                
					if(retc <= 0)
						VWLogger.error("Location("+(locOrder+1)+"):"+"can not"+
								" add comment to document <"+vwDoc.getName()+">"
								,null); 
				}
				else
				{
					VWLogger.info("Location("+(locOrder+1)+"):"+"Comment file path was invalid! Comment field will remain empty.");
				}    

			}     
			aipDoc.setStatus(aipDoc.IMPORTED); 
			if (EyesHandsManager.tempDisabled)
			{
				TreeMap map = new TreeMap();
				String docNumber = Name.substring(8);
				map.put("doc_number", docNumber);
				map.put("status", Integer.toString(aipDoc.IMPORTED));
				CFManager.updateNext(map);
			}
			
			if (aipDoc.isDeleteImages() && ret > 0) {
				if (imageTempLocation == null) {
					imageTempLocation = EHD.PManager.createImageTempLocation(tempBifFile, null);
				}
				EHD.PManager.moveImagesToTempLocation(imageArray, imageTempLocation, aipDoc.getName());
				EyesHandsManager.deleteImageFiles(aipDoc, processLocation);
				VWLogger.info("Image files are deleted.....");
			}
			EyesHandsManager.deleteCommentFile(aipDoc);

			docImpMsg = aipDoc.getName()+" is imported.";
			VWLogger.info("Location("+(locOrder+1)+"):"+formatDocMsg(aipDoc, docImpMsg)); 
			locStatus.setProgressVal(locStatus.getProgressVal()+stepBar);
			locStatus.setProgressStr(Integer.toString(docNum++) + "/" + Integer.toString(FileDocsMap.size()));
			updateProgressBar(locStatus);  

			/**
			 * This code has been added for giving a delay between document segments
			 * Vijaykumar.S.P - 03 March 2011
			 */
			if(AIPManager.getDocumentDelay()>0){
				try {
					delay = (1000*AIPManager.getDocumentDelay());
					VWLogger.info("Adding a delay between Document Segments : "+AIPManager.getDocumentDelay()+" Seconds");
					locStatus.getWorkThread().sleep(delay);//Thread.currentThread().sleep(delay);
				} catch (Exception e) {
					VWLogger.info("Exception while adding delay to the document segments "+e.getMessage());
					//e.printStackTrace();
				}
			}

		}//end while loop
		locStatus.setProgressVal((int)(.8 * progressBarMax));        
		updateProgressBar(locStatus);	
		return DocSaveNum;
	}
	public void RefreshSystemAndNodesTree(){
		VWLogger.debug("Refresh System Folder Tree And "+ Constants.PRODUCT_NAME +" Navigators Tree");
		AIPInit iInfo =EyesHandsManager.getInitParams(AIPManager.getCurrSid());
		if (inputFolderList.getSelectedIndex() != -1)
			iInfo.setDefaultLinkedFolder((String)inputFolderList.getSelectedValue());
		else    iInfo.setDefaultLinkedFolder("");
		/**/
		//iInfo.setDefaultRoomNodeId(VWNodes.getSelectedNodeid());
		SysExp =new SystemExplorer();
		/* modified by amaleh on 2003-09-29 */
		SysExp.setMaximumSize(new Dimension(295,190));
		SysExp.setPreferredSize(new Dimension(295,190));
		SysExp.setMinimumSize(new Dimension(295,185));
		/**/
		SysExp.setTitle(connectorManager.getString("General.Lab.SysExplorerTitle"));
		Vector vNodes =DocumentsManager.getNavigatorNode(AIPManager.getCurrSid(),true);

		/* added by amaleh on 2003-10-13 */
		inputFolderController = new InputFolderController(SysExp,inputFolderList);
		inputFolderController.setMaximumSize(new Dimension(28+33,185));
		inputFolderController.setPreferredSize(new Dimension(28+33,185));
		inputFolderController.setMinimumSize(new Dimension(28+33,170));


		TreesPane.remove(0);
		TreesPane.add(SysExp,0);
		/* added by amaleh on 2003-10-13 */
		TreesPane.remove(2);
		TreesPane.add(inputFolderController,2);
		/**/
		TreesPane.revalidate();
		TreesPane.repaint();

		inputFolderList.setSelectedValue(iInfo.getDefaultLinkedFolder(),true);
		EHD.TempPane.MDT.setNodesNavigator(vNodes);
	}

	public void refreshVWNodesTree()
	{
		Vector vNodes =DocumentsManager.getNavigatorNode(AIPManager.getCurrSid(),true);
		EHD.TempPane.MDT.setNodesNavigator(vNodes);

	}
	/* added by amaleh on 2003-06-27 */
	/**
	 * returns a string representing an audit trail entry for a document
	 */
	static String formatDocMsg(AIPDocument aipDoc, String msg)
	{
		String msgLineSep = "\r\n                         "; 
		msg += msgLineSep;
		msg += "Document: " + aipDoc.getName();
		msg += msgLineSep;
		msg += "Doctype: " + aipDoc.getAIPForm().getName();
		msg += msgLineSep;
		msg += "Image Path: " + aipDoc.getImagesPath();
		msg += msgLineSep;
		msg += "Destination VW Node: " + aipDoc.getVWNodePath();
		return msg;
	}
	/**/
	/* added by amaleh on 2003-06-30 */
	public JComboBox getRoomCombo()
	{
		return RoomCombo;
	}
	/**/ 

	public void clickStartStop()
	{
		Startbtn.doClick();
	}
	protected void autoStart()
	{
		VWLogger.info("Inside autostart ...........");
		String username ="";
		String userPass ="";
		int sessionId=0;//added in CV10.1
		String auth =AIPManager.getLastAuthentication();
		if(auth ==null) return ;
		StringTokenizer sToken = new StringTokenizer(auth, "/");      
		if (sToken.hasMoreTokens()) username = sToken.nextToken();
		if (sToken.hasMoreTokens()) userPass = sToken.nextToken(); 
		Session s=null;
		// Code added for EAS Service - start
//		String autRoom =AIPManager.getAutoStartRoom();
		String autRoom =AIPManager.getDefaultStartupRoom();
		autRoom = autRoom.substring(autRoom.indexOf(".")+1,autRoom.length());
		//VWLogger.info("<"+autRoom+"> to auto start ");
		// Code added for EAS Service - end
		boolean isRoomAvailable = false;
		int count = 0;
		while(!isRoomAvailable){
			VWLogger.info("Inside While loop of !isRoomAvailable...");
			if(autRoom !=null && !autRoom.equals(""))
			{    
				s =AIPManager.getRoomSession(autRoom);
				if(s ==null )
				{
					VWLogger.error("Can not find room <"+autRoom+"> to auto start ",null);
				}    
			}    
			else 
			{    
				Vector sessions =AIPManager.getSessions();     
				if(sessions !=null && sessions.size()>0)      
				{    
					s =(Session)sessions.get(0);
				}        
			}
			if(s ==null) {
				try{
					count++;
					VWLogger.info("Try to connect the Room After 1 Minute...");
					Thread.currentThread().sleep(1000 * 60);
					Vector sessionRooms = new Vector();
					AIPManager.getServersRooms(sessionRooms);
				}catch (Exception e) {
					VWLogger.info("Exception while get room in autostart :"+e.getMessage());
					// TODO: handle exception
				}
			}else{
				isRoomAvailable = true;
			}
		}

		VWLogger.info("AIP Service tried to connect the "+ Constants.PRODUCT_NAME +" Servers <"+count+"> No. of times");
		boolean retryStatus=AIPManager.isLoginReTry();
		VWLogger.info("retryStatus......"+retryStatus);
		//int sessionId=0;
		if(retryStatus==false){
		if(s ==null) return ;
		sessionId= AIPManager.login(s,username,userPass);
		}else if(retryStatus==true){
			/**
			 * Code added to try login for 5 times if login failure
			 */
			if(s ==null) return ;
			if (sessionId <= 0) {
				VWLogger.info("Inside sessionId less than zero");
				try {
					boolean tryout = false;
					Session s1 =AIPManager.getSession(AIPManager.getCurrSid());
					VWLogger.info("s.server :::::"+s.server);
					int status = AIPManager.getServersStatus(s1.server);
					VWLogger.info("Server status before try connecting:::::"+status);
					if(status!=0){
						VWLogger.info("Status inside if not equal to zero");
						tryout=true;
					}else if(status==0){
						VWLogger.info("Status inside else");
						tryout=false;
					}
					int trycounter = 0;
					int maxLoginCount=AIPManager.getMaxLoginCount();
					VWLogger.info("maxLoginCount ::::"+maxLoginCount);
					if(maxLoginCount==0){
						maxLoginCount=9999999;
					}
					while(tryout==false){
						VWLogger.info("Inside second while loop");
						if(trycounter < maxLoginCount){
							VWLogger.info("Retry since sessionId was: " + sessionId);
						}
						/*VWLogger.info("username:::"+username);
					VWLogger.info("password:::"+userPass);*/
						if(s!=null){
							VWLogger.info("Before login ");
							sessionId   = AIPManager.login(s,username,userPass);
							VWLogger.info("after lgoin call");
						}
						VWLogger.info("Session ID: " + sessionId + " Retry Counter: " + trycounter);
						if(sessionId > 0){
							tryout=true;
						}
						if(trycounter>maxLoginCount){
							tryout=true;
						}
						trycounter++;
						Thread.currentThread().sleep(1000 * 60);
						VWLogger.info("trycounter after increment :::::"+trycounter);
					}
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		VWLogger.info("SessionId inside autostart :::"+sessionId);
		if(sessionId <=0){
			JOptionPane.showMessageDialog(this,AIPManager.getErrorDescription(sessionId)+"("+sessionId+")",
					connectorManager.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE); 
			if(sessionId==-19){
				VWLogger.info("Inside invalid session condition");
				return;
			}
		}
		else 
		{            
			AIPManager.setCurrSid(sessionId);
			/**
			 * Condition added for autostart fix due to network failure 
			 */
			if(listTable.isEmpty()){
				VWLogger.info("List is empty");
				//Calling AIPClient to start the idle timer thread for autostart due to network failure 
				listTable.put(sessionId, new AIPClient(s));
				VWLogger.info("After adding sessionId to list table:::"+listTable);
			}
			if(!setInitializeParams(sessionId))
			{    
				disconnect(sessionId);                
			}
			else
			{
				/*enableConverToDJVU = AIPManager.checkConvertToDJVUFlag(sessionId);
				VWLogger.info("enableConverToDJVU :::::::"+enableConverToDJVU);
				setAIPProcessOptions(enableConverToDJVU);*/
				EHD.setAllEnabled(true,1);  
				EHD.TabPane.setEnabledAt(2, true);//history Pane AIP
				EHD.TabPane.setEnabledAt(3, true);//Pending Pane AIP 
				//conbtn.setText("Disconnect");
				conbtn.setToolTipText(connectorManager.getString("General.Btn.Disconnect"));
				conbtn.setIcon(IM.getImageIcon("disconnect.gif"));
				LoadGeneralData();
				VWLogger.info("11111Before calling clickstartstop inside autologin");
				//added if criteria check by srikanth on 30 Nov 2016
				if(setInitializeParams(sessionId)) {
					VWLogger.info("17171clickstartstop is now calling Stop action. End of Autostart Method.");
					clickStartStop();
				}
			}               

		}    

	} 
	/** Returns ProcessStop status, indicating whether stopping has been initiated */
	public synchronized boolean isProcessStop()
	{
		return ProcessStop;
	}

	public synchronized void setProcessStop(boolean stop)

	{
		ProcessStop = stop;
	}
	public void clickConnectDisconnect()
	{
		// Once stop the EAS service, The connection will be changed to "Disconnect" mode. for Issue No 501.
		//conbtn.doClick();
		conbtn.setToolTipText(connectorManager.getString("General.Btn.Connect"));
		conbtn.setIcon(IM.getImageIcon("connect.gif"));
		EHD.setAllEnabled(false,1);
		EHD.TabPane.setEnabledAt(1, false);
		EHD.TabPane.setEnabledAt(2, false);
		//if(EyesHandsManager.uniqueMsgId)
		EHD.TabPane.setEnabledAt(3, false);
		setInitializeParams(0);
		removeGeneralData();
		AIPManager.setCurrSid(0);       

	}
	private void checkAllProcessLocationsStop()
	{
		try {
			if(!ProcessStop)return;
			Vector locs =AIPManager.getAllLocationsStatus(AIPManager.getCurrSid());
			if(locs!=null&&locs.size()>0){
				for(int i=0;i<locs.size();i++)
				{
					LocationStatus locStatus =(LocationStatus)locs.get(i);
					VWLogger.info("inside stop method locStatus.getStatus()........"+locStatus.getStatus());
					if(locStatus.getStatus() != locStatus.STOP )
					{    
						return;  
					}    
				}
			}
			VWLogger.info("*** Import Process Stopped ***");	
			/**CV10.2 - if process stopped by the user then have to change the lable from stop to start . Added by Vanitha S**/
			changeStartButtonLable();
			EHD.PManager =null;
			EHD.setAllEnabled(true,2);
		} catch (Exception e) {
			VWLogger.info("Exception in checkAllProcessLocationsStop....."+e);
			VWLogger.info("Exception in checkAllProcessLocationsStop....."+e.getMessage());
		}
	} 
	
	/**
	 * CV10.2 - Method added to change the stop button lable change 
	 */
	private void changeStartButtonLable() {
		CurrState.setText(connectorManager.getString("General.Lab.ProcessStopped"));
		Startbtn.setText(connectorManager.getString("General.Btn.Start"));
		Startbtn.setEnabled(true);
		progressBar.setValue(0);
		progressBar.setString("");
	}
//	----------------------------------Actions & Listeners----------------------
	class ButtonListener implements ActionListener,MonitoringListener  {
		public void actionPerformed(ActionEvent e) {
			JButton Source = (JButton)e.getSource();
			if(Source.equals(conbtn))
			{
				String conTip = conbtn.getToolTipText();
				if( conTip.equalsIgnoreCase(connectorManager.getString("General.Btn.Connect")))
				{

					int idx = RoomCombo.getSelectedIndex();               
					Session s = (Session)AIPManager.getSessions().get(idx);
					if(s !=null)
					{ 
						connect(s);           

					}   
				}
				else
					if( conTip.equalsIgnoreCase(connectorManager.getString("General.Btn.Disconnect")))
					{
						int idx = RoomCombo.getSelectedIndex();               
						Session s = (Session)AIPManager.getSessions().get(idx);
						if(s !=null)
						{ 
							disconnect(s.sessionId); 
						}   
					} 

			}
			else    
				if( Source.getText().equalsIgnoreCase(connectorManager.getString("General.Btn.Start")))
				{              
					VWLogger.info("*** Import Process started ***");
					if(!EyesHandsManager.tempDisabled)
					{    
						Vector vNodes =DocumentsManager.getNavigatorNode(AIPManager.getCurrSid(),true);
						EHD.TempPane.MDT.setNodesNavigator(vNodes);
					}
					else
					{
						refreashSidNodes =new Vector();  
					}    

					if (!EyesHandsManager.tempDisabled)
					{
						if(! checkTemplate()){
							return ;
						}
					}      
					ProcessStop =false;
					try{
						EHD.PManager = new PagesManager();
					}
					catch (java.lang.UnsatisfiedLinkError es){
						JOptionPane.showMessageDialog(EHD,connectorManager.getString("Init.Msg.LostLibrary"),connectorManager.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);
						EHD.ExitConnector();
					}
					try
					{        
						// initiating scheduler for processing locked pending documents
						
						initiateProcessPendingDocScheduler();
						VWLogger.info("After processing SHD file ProcessStop :"+ProcessStop);
						// initiating scheduler for processing hanging documents
						
						/**CV10.2 - Process monitoring thread has initiated to monitor the document process**/
						processMonitoringThread = new ProcessMonitoringThread();
						processMonitoringThread.start();
						 
						workingFolders = new Vector();
						Vector autoPollFolders =new Vector();

						int size =inputFolderList.getContents().size();
						for(int i=0;i< size;i++)
						{
							String loc =(String)inputFolderList.getContents().get(i); 
							LocationStatus locStatus = AIPManager.getLocationStatus(AIPManager.getCurrSid(),loc);                                          
							if(locStatus != null && locStatus.isEnabledProcess()) 
							{                                   
								File locFile =new File(loc);
								try{
									if (locFile.exists() && locFile.isDirectory())
									{    
										File test =new File(loc+File.separator+"test.inv");
										test.createNewFile(); 
										test.delete();                                                
									}        
									else 
									{
										locStatus.setStatusText(connectorManager.getString("EHGeneralPanel.Stopped")+" "+connectorManager.getString("EHGeneralPanel.Invalidlocation"));
										
										VWLogger.audit("Location("+(i+1)+"):"+"Stopped "+"<Invalid location>");
										continue;
									}									
								}
								catch(Exception io)
								{    
									String expMsg ="<"+io.getMessage()+">";
									if(expMsg ==null || expMsg.trim().equals(""))                                           
										expMsg = connectorManager.getString("EHGeneralPanel.Invalidlocation");
									locStatus.setStatusText(connectorManager.getString("EHGeneralPanel.Stopped")+" "+expMsg);
									VWLogger.audit("Location("+(i+1)+"):"+"Stopped "+expMsg);
									//io.printStackTrace();
									continue;
								}                                
								workingFolders.add(loc);
								if(locStatus.getPolling() == 0)
									autoPollFolders.add(loc);
							}         
						}
						if( workingFolders.size() == 0) 
						{ 
							JOptionPane.showMessageDialog(EHGeneralPanel.this,connectorManager.getString("General.Msg.NoSelectProcessLocation"),connectorManager.getString("Init.Msg.Title"),JOptionPane.WARNING_MESSAGE);              
							VWLogger.audit(connectorManager.getString("General.Msg.NoSelectProcessLocation"));
							VWLogger.info("*** Import Process Ended ***");
							ProcessStop =true;
							return  ;                     
						}  
						try{	
							if (AIPManager.getPollinMode() == DAILY_AT) {
								VWLogger.info("*** Started Timer *** ");
								Source.setText(connectorManager.getString("General.Btn.Stop"));

								EHD.setAllEnabled(false,2); 
								progressBar.setValue(0);
								progressBar.setString(""); 
								if (!AIPManager.isLastAIPProcessSuccess()) new scheduleProcess().startScheduler();
								initiateScheduler();
								return;
							}else {
								//VWLogger.info("*** Cancelling Timer *** ");
								if (firstTimer != null) firstTimer.cancel();
							}
						}catch(Exception ex){
							//VWLogger.info("*** Exception in Start Button Import Process started *** " + ex.getMessage());	
						}


						if(autoPollFolders.size() > 0)
						{

							FoldersMonitoring.addMonitoringListener(this);
							boolean st = FoldersMonitoring.startMonitorFolders(autoPollFolders);
							if(st)
								VWLogger.debug("start location  monitors");
							else{
								ProcessStop =true;
								JOptionPane.showMessageDialog(EHD,connectorManager.getString("EHGeneralPanel.CannotStart.LocationMonitor")+" ",connectorManager.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);
								return;
							} 
						}
					}
					catch (java.lang.Error er)
					{       
						ProcessStop =true;                             
						JOptionPane.showMessageDialog(EHD,connectorManager.getString("EHGeneralPanel.CannotStart.LocationMonitor")+" ",connectorManager.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);
						return;  
					}
					catch (Exception ex)
					{   
						ProcessStop =true;                          
						JOptionPane.showMessageDialog(EHD,connectorManager.getString("EHGeneralPanel.CannotStart.LocationMonitor")+" ",connectorManager.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);
						return;  
					}              

					Source.setText(connectorManager.getString("General.Btn.Stop"));
					//CurrState.setText(connectorManager.getString("General.Lab.ProcessWait"));
					progressBar.setValue(0);
					progressBar.setString(""); 
					EHD.setAllEnabled(false,2);
					worker =new SwingWorker(){
						public Object construct()
						{
							for(int i=0;i< workingFolders.size();i++)
							{    
								String loc =(String)workingFolders.get(i);
								VWLogger.info("Before calling processLocation......from ButtonListener");
								processLocation(loc,true);

							}  
							return "";
						}    
					}; 


				}
				else if( Source.getText().equalsIgnoreCase(connectorManager.getString("General.Btn.Stop"))){
					stopAIPProcess(false);
				}
				else if( Source.getText().equalsIgnoreCase(connectorManager.getString("General.Btn.Close"))){
					if(EHD.TempPane.MDT.CheckUnSaveBeforeChange(connectorManager.getString("General.Msg.SaveTemplete")))
						EHD.ExitConnector();
				}
				else  if( Source.getText().equalsIgnoreCase(connectorManager.getString("General.Btn.Refresh"))){
					RefreshSystemAndNodesTree();
					Vector dts =new Vector();
					int status = DTManager.getDoctypes(AIPManager.getCurrSid(),dts,true);
					if(status != AIPManager.NOERROR)
					{    
						JOptionPane.showMessageDialog(EHGeneralPanel.this,AIPManager.getErrorDescription(status)+"("+status+")",
								connectorManager.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE); 
						return;
					}
					VWDoctypes =dts;
					EHD.TempPane.MDT.setMapDoctypes(VWDoctypes); 
				}
				else  if( Source.getText().equalsIgnoreCase(connectorManager.getString("General.Btn.Help"))){
					try
					{
						String helpFilePath = System.getProperty("user.dir") + File.separator + ".." + File.separator + "Help" + File.separator + "VWAIP.chm";
						File helpFile = new File(helpFilePath);
						if (helpFile.canRead())
						{
							//Runtime.getRuntime().exec("winhlp32.exe " + helpFilePath);
							java.lang.Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + helpFilePath);
							//send the window to the back so help files appear on top of it.
							//EHD.frame.toBack();
						}
						else
							JOptionPane.showMessageDialog(null, connectorManager.getString("General.Msg.NoHelpFile"), connectorManager.getString("Init.Msg.Title"), JOptionPane.ERROR_MESSAGE);    
					}
					catch (Exception he)
					{
						JOptionPane.showMessageDialog(null, connectorManager.getString("General.Msg.NoHelpFile"), connectorManager.getString("Init.Msg.Title"), JOptionPane.ERROR_MESSAGE);
						//if(Log.LOG_ERROR)Log.error("Error in launching help file: ", he);
					}
				}
		}

		//add ayman 24-07
		public boolean processLocation(String location,boolean init)
		{        
			VWLogger.info("calling from processLocation 2......");
			LocationStatus locStatus = AIPManager.getLocationStatus(AIPManager.getCurrSid(),location);        
			if(locStatus == null || !locStatus.isEnabledProcess()) return true; 
			if(!init && locStatus.getStatus() ==locStatus.PROCESS) return true;
			if(init) locStatus.setStatus(locStatus.PROCESS);
			else
			{
				if(locStatus.getPolling() ==0)    
				{                
					try{
						Thread.currentThread().sleep(300);
					}
					catch(Exception e)
					{
						VWLogger.error("Error while processor wait thread for loc :"+locStatus.getName(),e);
						return false;
					}         
				}         
			}    
			VWLogger.debug("Start thread for processing location <"+locStatus.getName()+"> Init <"+init+">");
			LocationThread locThread = (LocationThread)locStatus.getWorkThread();
			if( locThread != null)            
				locThread.resumeProcess();
			else
			{
				locThread =new LocationThread(locStatus);
				locThread.start();
			} 
			AIPManager.setLastAIPProcessSuccess(true);
			return true;
		}
	}

	private ArrayList getConnectedSessionId(){
		ArrayList sessionIdList = new ArrayList();
		try{
			for ( int index = 0; index < RoomCombo.getModel().getSize(); index++){
				Session sessionObject = (Session)AIPManager.getSessions().get(index);
				if (sessionObject.sessionId > 0){
					sessionIdList.add(sessionObject.sessionId);
				}
			}
		}catch(Exception ex){		
		}
		return sessionIdList;
	}
	public void stopAIPProcess(boolean isAutoStartRequired) {
		try {
			VWLogger.info("Stopping the AIP...");
			ProcessStop =true;
			Startbtn.setEnabled(false);
			FoldersMonitoring.stopMonitoring();
			VWLogger.info("Stopping Monitoring...");
			VWLogger.info("Current Sid before location status"+AIPManager.getCurrSid());
			Vector locs =AIPManager.getAllLocationsStatus(AIPManager.getCurrSid());
			//Thread.currentThread().notifyAll();
			VWLogger.info("locs vector value ::"+locs);
			if(locs!=null && locs.size()>0){
				for(int i=0;i<locs.size();i++)
				{
					LocationStatus locStatus =(LocationStatus)locs.get(i);
					if(locStatus.getWorkThread() != null)
					{    
						((LocationThread)locStatus.getWorkThread()).resumeProcess();
					}    
				}
			}
			VWLogger.info("Checkig the Process Location Stop...");
			checkAllProcessLocationsStop();
			VWLogger.info("Completed stop operation...");
			VWLogger.info("isAutoStartRequired....."+isAutoStartRequired);	
			/**CV10.2 - when the AIP document process is hanging then based on isAutoStartRequired flag value, have to restart the process again**/
			/**ProcessMonitoring thread will monitor the document process. If the process is hanging then inside the thread isAutoStartRequired value setting it as true.**/
			if (isAutoStartRequired) {
				VWLogger.audit("AIP process restarted........");
				doForceStop();
				if(!Startbtn.getText().equals(connectorManager.getString("General.Btn.Start"))) {
					Startbtn.setText(connectorManager.getString("General.Btn.Start"));
				}
				Startbtn.doClick();				
			} else {
				stopAllProcessScheduling();
			}
		} catch (Exception e) {
			VWLogger.error("Exception while stopping AIP process", e);			
		}
	}
 
	public void doForceStop() {
		Vector locs =AIPManager.getAllLocationsStatus(AIPManager.getCurrSid());
		VWLogger.info("Location status count : "+locs);
		if(locs != null && locs.size() > 0){
			for(int j=0;j<locs.size();j++)
			{
				//VWLogger.info("Inside for loop");
				LocationStatus locStatus =(LocationStatus)locs.get(j);
				locStatus.setStatus(locStatus.STOP);
				locStatus.setProgressVal(0);
				locStatus.setProgressStr("");
				updateProgressBar(locStatus);
				locStatus.setStatusText(connectorManager.getString("General.Lab.ProcessStopped"));
				updateStatusText(locStatus);  
				checkAllProcessLocationsStop();
				locStatus.setWorkThread(null);
			}
		}
	}
	
	/**
	 * CV10.2 - This method used to stop the document process when the current thread is interrupted
	 * @param locStatus
	 */
	private void stopCurrentSHDProcess(LocationStatus locStatus) {
		locStatus.setStatus(locStatus.STOP);
		locStatus.setProgressVal(0);
		locStatus.setProgressStr("");
		updateProgressBar(locStatus);
		locStatus.setStatusText(connectorManager.getString("General.Lab.ProcessStopped"));
		updateStatusText(locStatus);  
		checkAllProcessLocationsStop();
		locStatus.setWorkThread(null);
	}
	
	//update ayman 
	public void RoomCombo_actionPerformed(ActionEvent e) 
	{
		if(isUpdatingCombo) return;
		JComboBox SComb =(JComboBox)e.getSource();

		int idx = RoomCombo.getSelectedIndex();               
		Session s = (Session)AIPManager.getSessions().get(idx);
		if(s ==null) return;
		int sid =s.sessionId;          
		if(AIPManager.getCurrSid() !=  sid)
		{

			if(sid >0)
			{          
				conbtn.setToolTipText(connectorManager.getString("General.Btn.Disconnect"));
				conbtn.setIcon(IM.getImageIcon("disconnect.gif"));
				if(setInitializeParams(sid))
				{
					AIPManager.setCurrSid(sid) ;                 
					EHD.setAllEnabled(true,1);
					if(!EyesHandsManager.tempDisabled)
						EHD.TabPane.setEnabledAt(1, true);
					EHD.TabPane.setEnabledAt(2, true);
					LoadGeneralData();
				}
				else {
					disconnect(sid);
				}    
			}
			else
			{
				//conbtn.setText(connectorManager.getString("General.Btn.Connect"));
				conbtn.setToolTipText(connectorManager.getString("General.Btn.Connect"));
				conbtn.setIcon(IM.getImageIcon("connect.gif"));
				AIPManager.setCurrSid(0) ;
				setInitializeParams(0);
				removeGeneralData();
				EHD.setAllEnabled(false,1);

				EHD.TabPane.setEnabledAt(1, false);
				EHD.TabPane.setEnabledAt(2, false);
			}    

		}
	}
	//add ayman 24-07
	//update ayman 02-08
	private class LocationThread extends MutableLocationThread
	{
		
		public LocationThread(LocationStatus locStatus)
		{
			super(locStatus);            
		}    
		public void run()
		{
			try {
				VWLogger.info("Inside LocationThread .....");
				int locOrder =AIPManager.getLocationStatusOrder(locStatus.getName());
				if(locStatus ==null) return;
				locStatus.setWorkThread(this);
				VWLogger.info("locStatus.getWorkThread()......"+locStatus.getWorkThread());
				VWLogger.info("locStatus.getStatus()......"+locStatus.getStatus());
				while(!ProcessStop && locStatus.isEnabledProcess() 
						&& locStatus.getStatus() !=locStatus.STOP)
				{
					FoldersMonitoring.resetFolderStatus(locStatus.getName());
					locStatus.setStatus(locStatus.PROCESS);
					if(!startProcess(locStatus)) 
					{                        
						break;
					}
					if(locStatus.getPolling() == 0 && FoldersMonitoring.isFolderUpdated(locStatus.getName()))
						continue;
					AIPManager.setLastAIPProcessSuccess(true);	
					locStatus.setStatus(locStatus.WAIT);
					locStatus.setProgressVal(0);
					locStatus.setProgressStr("");
					updateProgressBar(locStatus);
					locStatus.setStatusText(connectorManager.getString("General.Lab.ProcessWait"));
					
					
	/*				boolean isPendingDocsStopped = processLockedPendingDocs();
					
					if(isPendingDocsStopped){
						break;
					}*/
					updateStatusText(locStatus);
					if(AIPManager.getPollinMode() == AUTO)
					{
						VWLogger.info("Location("+(locOrder+1)+"):"+"The thread of location <"+locStatus.getName()+"> waiting and monitoring update");  
						waitUntilAsk();
					}  
					else if(AIPManager.getPollinMode() == EVERY)
					{
						VWLogger.info("Location("+(locOrder+1)+"):"+"The thread of location <"+locStatus.getName()+"> waiting for "+locStatus.getPolling()+" minute");    
						waitForMin(locStatus.getPolling());
						if(!checkAIPServiceAndApplication()) return;
					}else if(AIPManager.getPollinMode() == DAILY_AT){
						VWLogger.info("Location("+(locOrder+1)+"):"+"The thread of location <"+locStatus.getName()+"> waiting and next processing time at " + AIPManager.getScheduleTime());
						waitUntilAsk();
					}
					
				}
				VWLogger.info("After processing from LocationThread ...."+locStatus.getStatus());
				locStatus.setStatus(locStatus.STOP);
				locStatus.setProgressVal(0);
				locStatus.setProgressStr("");
				updateProgressBar(locStatus);
				locStatus.setStatusText(connectorManager.getString("General.Lab.ProcessStopped"));
				updateStatusText(locStatus);  
				checkAllProcessLocationsStop();
				locStatus.setWorkThread(null);
			} catch (Exception e) {
				VWLogger.info("Exception in LocationThread run method :"+e.getMessage());
			}
		}
	}    

	public void initiateScheduler(){
		VWLogger.info("*** Initiated Scheduler ***" );
		firstTimer = new java.util.Timer();		
		String dailyFrequency  = AIPManager.getScheduleTime();		        
		StringTokenizer stDailyFreq = null;
		stDailyFreq = new StringTokenizer(dailyFrequency, ":");
		//VWLogger.info("*** dailyFrequency ***" + dailyFrequency);
		Calendar calendar = Calendar.getInstance();
		try{	
			if(stDailyFreq != null && stDailyFreq.countTokens() == 3){
				calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(stDailyFreq.nextToken()));
				calendar.set(Calendar.MINUTE, Integer.parseInt(stDailyFreq.nextToken()));
				calendar.set(Calendar.SECOND, Integer.parseInt(stDailyFreq.nextToken()));
			}else{
				calendar.set(Calendar.HOUR_OF_DAY, 0);
				calendar.set(Calendar.MINUTE, 0);
				calendar.set(Calendar.SECOND, 0);
			}
		}catch(Exception ex){
			//VWLogger.info("*** Time Exception  ***" + ex.getMessage());	
		}
		Date startTime = calendar.getTime();
		//VWLogger.info("*** startTime ***" + startTime.toString());
		firstTimer.schedule(new scheduleProcess(), startTime, 1000 * 24 * 60 * 60);

	}
	
	public void initiateProcessPendingDocScheduler(){
		VWLogger.info("*** Initiated Scheduler to Process Pending document of locked documents***" );
		processPendingDocTimer = new java.util.Timer();		
		String dailyFrequency  = AIPManager.getScheduleTimeForProcessingPendingDocs();//"00:02:00";//AIPManager.getScheduleTime();	// Reading Schedule Time we need to add pendingDocSchedule frequency Time	        
		VWLogger.info("dailyFrequency from reg::"+dailyFrequency);
		StringTokenizer stDailyFreq = null;
				
		int frequency = 45;
		try{
			frequency = Integer.parseInt(dailyFrequency);
			VWLogger.info("frequency after converting to int:"+frequency);
		}catch (Exception e) {
			// TODO: handle exception
			frequency = 45;
			VWLogger.info("Inside catch block of pending frequency"+e);
		}

		try{
			VWLogger.info("*** initiateProcessPendingDocScheduler  ***" + frequency);
			Calendar calendar = Calendar.getInstance();

			Date startTime = calendar.getTime();
			VWLogger.info("***initiateProcessPendingDocScheduler  startTime ***" + startTime.toString());
			VWLogger.info("***initiateProcessPendingDocScheduler  period ***" + 1000 * frequency * 60);
			processPendingDocTimer.schedule(new scheduleProcessPendingDocs(), startTime, 1000 * frequency * 60);
		}catch (Exception e) {
			VWLogger.error("Exception while scheduling task ", e);
		}

	}
	
	public void updateEnabledFolders(int sid){
		Vector locs =AIPManager.getAllLocationsStatus(sid);
		Vector enabledFolders =new Vector();            
		for(int j=0;j<locs.size();j++)
		{    
			LocationStatus loc =(LocationStatus)locs.get(j);
			if(loc.isEnabledProcess())
				enabledFolders.add(loc.getName()+"\t"+loc.getPolling());    
		}  
		AIPManager.putEnabledFolders(sid,enabledFolders);	    
	}
	/**
	 * CV10.1 ISSue Fix :- Method created to reprocess the pending shd file
	 * @param locStatus
	 * @return
	 */
	public boolean startSHD(LocationStatus locStatus)
	{
		VWLogger.info("Inside pending shd processing");
		StartFlag = 1;
		VWLogger.info("locStatus.getName ::::"+locStatus.getName());
		int locOrder =AIPManager.getLocationStatusOrder(AIPManager.getCurrSid(),locStatus.getName());
		AIPInit iInfo =  EyesHandsManager.getInitParams(AIPManager.getCurrSid());
		EyesFileManager EFManager = new EyesFileManager();;
		CustomFileManager CFManager =new CustomFileManager();
		if (!EyesHandsManager.tempDisabled)
		{            
			EFManager.setFormsFileName(AIPUtil.getString("Init.FormsFile.Name"));
			EFManager.setDocsFileExt(AIPUtil.getString("Init.FormsDocsFile.Ext"));
		}    
		else
		{               
			CFManager.setFormsFileName(AIPUtil.getString("Init.FormsFile.Name"));
			CFManager.setFormsDocsFileExt(AIPUtil.getString("Init.FormsDocsFile.Ext"));
			CFManager.setDocsFileExt(AIPUtil.getString("Init.DocsFile.Ext"));
			CFManager.setTempDocsFileExt(AIPUtil.getString("Init.TempDocsFile.Ext"));   
		}     

		String processLoc = locStatus.getName();

		File folder =new File(processLoc);
		VWLogger.info("Selected folder in pending shd process " + folder );
		if(!folder.isDirectory())
		{
			//VWLogger.audit("Location("+(locOrder+1)+"):"+"Import process ended for location :"+locStatus.getName());
			return false ;
		}              
		if(inputFolderList.getSelectedIndex() < 0)
		{
			inputFolderList.setSelectedValue(processLoc,true);
			iInfo.setDefaultLinkedFolder(processLoc);
		}
		else
		{
			String sloc =(String)inputFolderList.getSelectedValue();
			LocationStatus sLocStatus =AIPManager.getLocationStatus(AIPManager.getCurrSid(),sloc);
			if(sLocStatus ==null || sLocStatus.getStatus() !=sLocStatus.PROCESS) 
			{
				inputFolderList.setSelectedValue(processLoc,true);
				iInfo.setDefaultLinkedFolder(processLoc);
			}    
		}    
		locStatus.setProgressVal(0);
		locStatus.setProgressStr("");
		updateProgressBar(locStatus);             
		locStatus.setStatusText(connectorManager.getString("General.Lab.InputFolderListTitle")+": "+processLoc);             
		updateStatusText(locStatus);             
		Vector vGFiles =new Vector();
		if (EyesHandsManager.processSubdirs)
			getFiles(folder,vGFiles,EyesHandsManager.processSubdirs);
		else
			vGFiles.add(folder);
		if(vGFiles.size() ==0)  return true;
		VWLogger.audit("Location("+(locOrder+1)+"):"+" Import process started for location in pending shd process :"+locStatus.getName());
		VWLogger.info(" fileList size " +  	vGFiles.size());
		for (int fileList = 0; fileList < vGFiles.size(); fileList++)
		{   

			File[] pFiles;
			File tempFile = null;
			File gFile = null;//(File)vGFiles.get(j);                
			String gFileName = "";//gFile.getName();

			// Start Copying Remote archive into local cache folder
			try{
				tempFile = (File)vGFiles.get(fileList);
				VWLogger.info("Remote file path in pending shd process :"+(tempFile).getCanonicalPath());
				{
					gFile = (File)vGFiles.get(fileList);
					gFileName = gFile.toString();
				}
			}catch(Exception ex){
				VWLogger.error("Exception while copying remote file while pending shd process !",ex);
			}
			
			
				VWLogger.info("gFileName in pending shd process " + gFileName);        		 
				File[] ShadowFiles = CFManager.getEHDocumentsShadowFiles(gFileName);
				VWLogger.info("ShadowFiles list in AIP : " + ShadowFiles);
				if ( ShadowFiles == null)
				{
					return true;
				}
				VWLogger.info("ShadowFiles list before checking the list size ");
				//concatenate both arrays into Files
				pFiles = new File[ShadowFiles.length];   
				VWLogger.info("After checking the shadowfile list size :"+pFiles);
				for (int j = 0; j < ShadowFiles.length; j++)
				{
					pFiles[j] = ShadowFiles[j];
				}
				VWLogger.info("pFiles.length :"+pFiles.length);
			if(pFiles.length == 0) 
				continue;
			VWLogger.info("Location("+(locOrder+1)+"):"+"Import process started for location in pending shd process:"+locStatus.getName());  
			VWLogger.info("Location("+(locOrder+1)+"):"+pFiles.length +" Document File(s) Found in pending shd process");	             

			for (int j = 0; j < pFiles.length; j++)
			{   
				if(!checkAIPServiceAndApplication()) return false;
				AIPConnector.checkManualShutDown();
				VWLogger.info("Before processing SHD file ProcessStop :"+ProcessStop+" locStatus.getStatus() :"+locStatus.getStatus());
				if(ProcessStop || locStatus.getStatus() == locStatus.STOP){
					return false;
				}
				locStatus.setStatusText("Processing pending shadow File in pending shd process"+" "+pFiles[j].getName());
				updateStatusText(locStatus);       
				VWLogger.info("Location("+(locOrder+1)+"):"+"Processing File in pending shd process"+" "+pFiles[j].getName());                   
				File bifFile =pFiles[j];
				if (bifFile != null){
					if (bifFile.length() <= 0) continue;
				}
				try{
					if (ProcessAIPFile(bifFile,locStatus,EFManager,CFManager))
					{                      
						File f;
						if (!EyesHandsManager.tempDisabled)
							f = EFManager.GetUsedFile();
						else
							f = CFManager.GetUsedFile();
					
						/**
						 * Condition added for autostart fix due to network failure 
						 */
						VWLogger.info("serverfailure flag before delete in pending shd process :::"+serverfailure);
						if(serverfailure==false){
							VWLogger.info("Before deleting the file:::");
							if(f.delete()){
								VWLogger.debug("Location("+(locOrder+1)+"):"+"AIPDocument File <"+pFiles[j].getName()+"> Deleted ");   
							}
						}
					}
				}
				catch(Exception e)
				{
					if(e.getMessage().equalsIgnoreCase("Process Stopped by the User while shd reprocess!"))
					{
						VWLogger.audit("Location("+(locOrder+1)+"):"+"Process Stopped by the User during pending shd process!");
						return false;
					}    
					//e.printStackTrace();
					VWLogger.error("Location("+(locOrder+1)+"):"+" In Processing File during pending shd process <"+pFiles[j].getName()+"> :",e );
					locStatus.setProgressVal(0);
					locStatus.setProgressStr("");
					updateProgressBar(locStatus);  
					return true;   
				}

			}//inner for loop end 
		}//outer for loop end
		locStatus.setProgressVal(0);
		locStatus.setProgressStr("");
		updateProgressBar(locStatus);  
		VWLogger.info("*** End of pending shd process***");
		return true;          
	}  
	
	/***
	 * CV10.2 Enhancement - Process monitoring thread has added to check the document process
	 * @author vanitha.s
	 *
	 */
	private class ProcessMonitoringThread extends Thread
	{    		
		public void run()
		{
			try {
				VWLogger.info("*** ProcessMonitoringThread initiated ****");				
				initiateProcessMonitoringScheduler();
				String locName = (String) inputFolderList.getSelectedValue();
				try {
					int pollingMode = AIPManager.getPollinMode();
					int pollingTimeInMinutes = 0;
					if (pollingMode == 1) {
						LocationStatus locStatus = AIPManager.getLocationStatus(AIPManager.getCurrSid(), locName);
						VWLogger.info("Before initiate initiateIdleTimer .... "+locStatus.getPolling());
						pollingTimeInMinutes = locStatus.getPolling();		
						if (pollingTimeInMinutes > 30)
							initiateIdleTimer();
					} else {
						/*String dailyFreq = AIPManager.getScheduleTime();
						SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
						Date date = timeFormat.parse(dailyFreq);*/
						initiateIdleTimer();
					}
					
				} catch (Exception e) {
					VWLogger.info("Exception while calling initiateIdleTimer in ProcessMonitoringThread...."+e.getMessage());
				}
			} catch (Exception e) {
				VWLogger.info("Exception in ProcessMonitoringThread...."+e.getMessage());
			}
		}
	}    
	
	public synchronized void initiateProcessMonitoringScheduler(){
		processMonitoringTimer = new java.util.Timer();		
		int frequency = 5;
		try{
			VWLogger.info("*** initiateProcessMonitoringScheduler  ***" + frequency);
			Calendar calendar = Calendar.getInstance();

			Date startTime = calendar.getTime();
			VWLogger.info("***initiateProcessMonitoringScheduler  startTime ***" + startTime.toString());
			VWLogger.info("***initiateProcessMonitoringScheduler  period ***" + 1000 * frequency * 60);
			processMonitoringTimer.schedule(new ScheduleProcessMonitoringTask(), startTime, 1000 * frequency * 60);
		}catch (Exception e) {
			VWLogger.error("Exception while scheduling process monitoring task ", e);
		}

	}
	/**
	 * CV10.2 - Method to set the processing document details in global variable
	 * @param Name
	 */
	private void setProcessDocDetails(String Name){		
		if (processingDocDetails == null) {
			processingDocDetails = new DocumentProcessStatus();
		}
		if (processMonitoringDetails == null) {
			processMonitoringDetails = new DocumentProcessStatus();
		}
		Calendar date = Calendar.getInstance(); 
		processingDocDetails.setDocumentName(Name);
		processMonitoringDetails.setDocumentName(Name);
		String dateAndTime = date.getTime().toString();
		VWLogger.info("dateAndTime...................."+dateAndTime);
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		Date date1 = new Date();
		String dateAndTimeReg = dateFormat.format(date1);
		VWLogger.info("dateAndTimeReg...................."+dateAndTimeReg);
		
		processingDocDetails.setProcessStartTime(dateAndTime);
		processMonitoringDetails.setProcessStartTime(dateAndTime);
		processingDocDetails.setProcessStatus(0);
		processMonitoringDetails.setProcessStatus(0);
		
		AIPCurrentUserPreferences.setDocumentName(Name);
		AIPCurrentUserPreferences.setProcessDate(dateAndTimeReg);
	}
	
	/**
	 * CV10.2 - This method is used to check whether document is in hanging state. If is hanging then this will restart the AIP document process again by calling stopAIPProcess(true) method 
	 * @author vanitha.s
	 *
	 */
	class ScheduleProcessMonitoringTask extends TimerTask{
			
		public void run() {
			try{
				VWLogger.info("*** run in scheduleProcessMonitoringTask ");
				startTaskScheduler();
			}catch (Exception e) {
				VWLogger.error("Exception in scheduleProcessMonitoringTask run() method ", e);
			}
		}
		
		public void startTaskScheduler(){
			VWLogger.info("startTaskScheduler.................");
			try{				
				if (processingDocDetails != null && processMonitoringDetails != null
						&& processMonitoringDetails.getDocumentName() != null
						&& processingDocDetails.getDocumentName() != null) {					
					VWLogger.info("Last Processed Document :"+processingDocDetails.getDocumentName());
					VWLogger.info("Last Processed Document in monitoring thread :"+processMonitoringDetails.getDocumentName());
					VWLogger.info("Last Processed Document status :"+processingDocDetails.getProcessStatus());
					VWLogger.info("Last Processed Document status in monitoring thread :"+processMonitoringDetails.getProcessStatus());
					if (processingDocDetails.getDocumentName().equals(processMonitoringDetails.getDocumentName())
							&& processingDocDetails.getProcessStatus() == processMonitoringDetails.getProcessStatus() 
							&& processingDocDetails.getProcessStatus() == 0) {						
						if (ProcessStop == false && Startbtn.getText().equals(connectorManager.getString("General.Btn.Stop"))){
							//VWLogger.info("Hanging monitor thread has initiated for the document :<"+processingDocDetails.getDocumentName()+">");
							Calendar date = Calendar.getInstance();
							String dateAndTime = date.getTime().toString();
							processMonitoringDetails.setProcessStartTime(dateAndTime);
							//VWLogger.info("Current date and time :"+dateAndTime);
							VWLogger.info("*** Scheduler to monitor hanging document Started ***");
							try {
								SimpleDateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss z yyyy");
								String processTime =  processingDocDetails.getProcessStartTime();
								String monitorTime = processMonitoringDetails.getProcessStartTime();
								VWLogger.info("Process Initiated Time :"+processTime);
								VWLogger.info("Monitoring Time :"+monitorTime);
								Date pDate=formatter.parse(processTime);
								Date mDate=formatter.parse(monitorTime);  
								long diff = mDate.getTime() - pDate.getTime();
								//VWLogger.info("Time difference :"+diff);
								long diffMinutes = diff / (60 * 1000);
								VWLogger.info("Time difference in minutes :"+diffMinutes);
								if (diffMinutes >= 30) {
									VWLogger.info("Hanging monitor thread has identified that the document :<"+processingDocDetails.getDocumentName()+"> is in hanging state");
									VWLogger.info("Document: <"+processingDocDetails.getDocumentName()+">     Process initiated time: <"+processTime+">");
									VWLogger.info("AIP Process has restarted....");
									processMonitoringTimer.cancel();
									processMonitoringTimer = null;
									if (idleTimer != null) {
										idleTimer.cancel();
										idleTimer = null;
									}
									processingDocDetails.setDocumentName("99999");
									processingDocDetails.setProcessStatus(1);
									processingDocDetails.setProcessStartTime(dateAndTime);
									stopAIPProcess(true);
									//VWLogger.info("After calling stopAIPProcess...");
								}
								
							} catch (Exception e) {
								VWLogger.error("Exception while datecomparison check in startTaskScheduler method ", e);
							}
						}
					}
				}
			}catch (Exception e) {
				VWLogger.error("Exception in startTaskScheduler method : ", e);
			}
		}		
	}
	public void stopAllProcessScheduling(){
		VWLogger.info("Stopping process monitoring Listener ...");
		if(processPendingDocTimer != null) {
			processPendingDocTimer.cancel();
			processPendingDocTimer = null;
		}
		if(processMonitoringTimer != null) {
			processMonitoringTimer.cancel();
			processMonitoringTimer = null;
			processingDocDetails = null;
			processMonitoringDetails = null;
		}		
		if (idleTimer != null) {
			idleTimer.cancel();
			idleTimer = null;
		}
	}
	
	/**
	 * CV10.2 - This method used to initiate the idle timer after particular time interval 
	 */
	public synchronized void initiateIdleTimer(){
		idleTimer = new java.util.Timer();		
		int frequency = 5;
		try{
			VWLogger.info("*** initiateIdleTimer  ***" + frequency);
			Calendar calendar = Calendar.getInstance();

			Date startTime = calendar.getTime();
			VWLogger.info("***initiateIdleTimer  startTime ***" + startTime.toString());
			VWLogger.info("***initiateIdleTimer  period ***" + 1000 * frequency * 60);
			idleTimer.schedule(new scheduleIdleTimerTask(), startTime, 1000 * frequency * 60);
		}catch (Exception e) {
			VWLogger.error("Exception while scheduling idletimer task ", e);
		}

	}
	
	/**
	 * CV10.2 - This method is used to check whether AIP process is in idle state, if it is idle more than 1 hour then this method will restart the AIP document process again by calling stopAIPProcess(true) method 
	 * @author vanitha.s
	 *
	 */
	class scheduleIdleTimerTask extends TimerTask {
		
		public void run() {
			try{
				VWLogger.info("*** run in scheduleIdleTimerTask ");
				startIdleTimerTaskScheduler();
			}catch (Exception e) {
				VWLogger.error("Exception in scheduleIdleTimerTask run() method ", e);
			}
		}
		
		public void startIdleTimerTaskScheduler(){
			VWLogger.info("IdleTimerTaskScheduler started.................");
			try{				
				if (ProcessStop == false && Startbtn.getText().equals(connectorManager.getString("General.Btn.Stop"))){
					//VWLogger.info("processing document details in ildetimer thread................."+processingDocDetails);
					//VWLogger.info("process monitoring details in idletimer thread ................."+processMonitoringDetails);
					if (processingDocDetails != null && processMonitoringDetails != null) {
						String docName = processingDocDetails.getDocumentName();
						int status  = processingDocDetails.getProcessStatus();
						VWLogger.info("Last Processed Document :"+processingDocDetails.getDocumentName());
						VWLogger.info("Last Processed Document in monitoring thread :"+processMonitoringDetails.getDocumentName());
						VWLogger.info("Last Processed Document status :"+processingDocDetails.getProcessStatus());
						VWLogger.info("Last Processed Document status in monitoring thread :"+processMonitoringDetails.getProcessStatus());						
						if (docName != "99999" && status == 1) {
							VWLogger.info("Idle-timeout thread has initiated.....");
							try {
								String processTime =  processingDocDetails.getProcessStartTime();
								Calendar date = Calendar.getInstance();
								String dateAndTime = date.getTime().toString();
								SimpleDateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss z yyyy");
								Date pDate=formatter.parse(processTime);								
								Date cDate=formatter.parse(dateAndTime);  
								VWLogger.info("Document processed time :"+pDate);
								VWLogger.info("Current Time :"+cDate);
								long diff = cDate.getTime() - pDate.getTime();
								//VWLogger.info("Time difference inside idletimer :"+diff);
								long diffMinutes = diff / (60 * 1000);
								VWLogger.info("Time difference in minutes inside idletimer :"+diffMinutes);
								if (diffMinutes >= 30) {
									VWLogger.info("Idle-timeout thread has identified that the AIP service is in idle state for more than 30 minutes.....");
									VWLogger.info("AIP process has restarted ....");
									idleTimer.cancel();
									idleTimer = null;
									if (processMonitoringTimer != null) {
										processMonitoringTimer.cancel();
										processMonitoringTimer = null;
									}
									processingDocDetails.setDocumentName("99999");
									processingDocDetails.setProcessStatus(1);
									processingDocDetails.setProcessStartTime(dateAndTime);
									stopAIPProcess(true);
									//VWLogger.info("After calling stopAIPProcess from idle timer...");
								}
							} catch (Exception e) {
								VWLogger.info("Exception in idle timer scheduler..."+e.getMessage());
							}
						}
					}
				}
			}catch (Exception e) {
				VWLogger.error("Exception in startIdleTimerTaskScheduler method : ", e);
			}
		}		
	}
	
	private void setAIPProcessOptions(boolean djvu) {
		if (djvu) {
			rconvertDJVU.setVisible(true);
			rconvertHCPDF.setVisible(false);
			singlePageHcpdf.setVisible(false);
		} else {
			rconvertDJVU.setVisible(false);
			rconvertHCPDF.setVisible(true);
			singlePageHcpdf.setVisible(true);
		}
	}
	
	class ScheduleImageBackupMonitoringTask extends TimerTask{
		public void run() {
			try{
				VWLogger.info("*** run in ScheduleImageBackupMonitoringTask ");
				startImageBackupMonitoringTask();
			}catch (Exception e) {
				VWLogger.error("Exception in ScheduleImageBackupMonitoringTask run() method ", e);
			}
		}	
		private void startImageBackupMonitoringTask() {
			try {
				String currentDate = null, folderDate = null;
				String home = AIPUtil.findHome();
				String imageTempFolder = home + AIPUtil.pathSep + "AIPImageTemp";
				File imageBackupFolder = new File(imageTempFolder);
				if (imageBackupFolder.exists()) {
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");			
					Date date1 = new Date();
					currentDate = dateFormat.format(date1);
					Date cDate = dateFormat.parse(currentDate);
					Date fDate = null;
					File[] files = imageBackupFolder.listFiles();
					for (int i = 0; i < files.length; i++) {
						if (files[i].isDirectory()) {
							folderDate = files[i].getName();
							fDate = dateFormat.parse(folderDate);
							VWLogger.info("Image backup folder date  :"+fDate);
							VWLogger.info("Current date :"+cDate);
							long diff = cDate.getTime() - fDate.getTime();
							long daysDiff = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
							VWLogger.info("Days difference : " + daysDiff);
							int imageBackupDays = AIPManager.getImageBackupDays();
							if (daysDiff > imageBackupDays) {
								AIPUtil.deleteDirectory(files[i].getPath());
								VWLogger.info("Image backup has deleted from " + files[i].getPath());
							}
						}
					}
				}
			} catch (Exception e) {
				VWLogger.info("Exception while monitoring image backup folder for deletion :"+e);
			}
		}
	}
	/**
	 * This method is added for to monitor the image backup folder.If the folder creation date exceeds more than 5 days then those folder will be getting deleted
	 */
	public void initiateImageBackupMonitoringScheduler() {
		imageBackupMonitoringTimer = new java.util.Timer();		
		int frequency = 12;
		try{
			VWLogger.info("*** image backup monitoring timer has initiated  ***" + frequency);
			Calendar calendar = Calendar.getInstance();

			Date startTime = calendar.getTime();
			VWLogger.info("***backup monitoring timer ** startTime ***" + startTime.toString());
			VWLogger.info("***backup monitoring timer  ** next scheduling will be after " + 12 +" hour(s)");
			imageBackupMonitoringTimer.schedule(new ScheduleImageBackupMonitoringTask(), startTime, 1000 * frequency * 60 * 60);
		}catch (Exception e) {
			VWLogger.error("Exception while scheduling image backup monitoring task ", e);
		}

	}
}