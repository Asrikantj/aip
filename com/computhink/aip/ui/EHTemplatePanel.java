package com.computhink.aip.ui;


import com.computhink.aip.*;
import com.computhink.aip.util.AIPUtil;

import com.computhink.aip.client.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import java.io.File;
import java.util.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */
/** changes by amaleh
 * every EFManager was turned to CFManager *
 */

public class EHTemplatePanel extends JPanel {
  MapDTypeLayout MDT;
  private JRadioButton AutDefRadio;
  private JRadioButton SendPendRadio;
  JButton Savebtn    ;
  JButton Delbtn    ;
  private  JButton Refbtn    ;
  private  JButton Hlpbtn    ;
  EHMainPanel EHD;

  public EHTemplatePanel(EHMainPanel EHD1) {
     EHD =EHD1;
     MDT =new MapDTypeLayout(this);
     setBorder(new EmptyBorder(5,5,5,5));
     JPanel Panel1 =new JPanel();
     add(Panel1,BorderLayout.CENTER);
     Panel1.setLayout(new BoxLayout(Panel1,BoxLayout.Y_AXIS));
     Panel1.setBorder(new CompoundBorder(new TitledBorder(null,null,
	TitledBorder.LEFT, TitledBorder.TOP),new EmptyBorder(5,8,5,5)));
     Panel1.setMinimumSize(new Dimension(200,100));
     Panel1.setPreferredSize(new Dimension(655,500));
     MDT.setAlignmentX(Component.LEFT_ALIGNMENT);
     Panel1.add(MDT);
     //Panel1.add(Box.createVerticalStrut(20));
     //Panel1.add(createOptionsPane());
     Panel1.add(Box.createVerticalStrut(20));
     Panel1.add(CreateButtonPanel());
  }
 /* public JPanel createOptionsPane(){
      JPanel op =new JPanel();
      op.setLayout(new BoxLayout(op,BoxLayout.Y_AXIS));
      JLabel lab =new JLabel("When an unmapped Form definition is encountered :");
        lab.setFont(new Font(lab.getFont().getName(),Font.BOLD,lab.getFont().getSize()));

        AutDefRadio = new JRadioButton("Create new ViewWise Document type.");
        AutDefRadio.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                  if(AutDefRadio.isSelected())
                       InitParamsInfo.NewFormProcess =2 ;
            }
      });

      SendPendRadio = new JRadioButton("Send to Pending Forms.");
      SendPendRadio.setSelected(true);
      SendPendRadio.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                  if(SendPendRadio.isSelected())
                       InitParamsInfo.NewFormProcess =1 ;
            }
      });
      AutDefRadio.setSelected(true);
      InitParamsInfo.NewFormProcess =1;
      ButtonGroup group = new ButtonGroup();
      group.add(SendPendRadio);
      group.add(AutDefRadio);

      JPanel Pan =new JPanel();
       Pan.setLayout(new BoxLayout(Pan,BoxLayout.X_AXIS));

       JPanel Pan1 =new JPanel();
       Pan1.setLayout(new BoxLayout(Pan1,BoxLayout.Y_AXIS));
       Pan1.add(SendPendRadio);
       Pan1.add(AutDefRadio);
       Pan1.setAlignmentX(Component.LEFT_ALIGNMENT);
       Pan.add(Box.createHorizontalStrut(15));
       Pan.setAlignmentX(Component.LEFT_ALIGNMENT);
       Pan.add(Pan1);
       op.add(lab);
       op.add(Pan);
       //op.setBorder(BorderFactory.createEtchedBorder());
       op.setAlignmentX(Component.LEFT_ALIGNMENT);
       return op;
  }*/

  public JPanel CreateButtonPanel(){
    JPanel BtnPanel =new JPanel();
    BtnPanel.setLayout(new BoxLayout(BtnPanel, BoxLayout.X_AXIS));
    ButtonListener btnListener =new ButtonListener();
    Savebtn    =new JButton(AIPUtil.getString("Template.Btn.Save"));
    Delbtn     =new JButton(AIPUtil.getString("Template.Btn.Delete"));
    Refbtn      =new JButton(AIPUtil.getString("Template.Btn.Refresh"));
    Hlpbtn      =new JButton(AIPUtil.getString("Template.Btn.Help"));

    Savebtn.setMnemonic(AIPUtil.getString("Template.MBtn.Save").charAt(0));
    Delbtn.setMnemonic(AIPUtil.getString("Template.MBtn.Delete").charAt(0));
    Refbtn .setMnemonic(AIPUtil.getString("Template.MBtn.Refresh").charAt(0));
    Hlpbtn .setMnemonic(AIPUtil.getString("Template.MBtn.Help").charAt(0));

    Savebtn.addActionListener(btnListener);
    Delbtn.addActionListener(btnListener);
    Refbtn .addActionListener(btnListener);
    Hlpbtn .addActionListener(btnListener);

    Savebtn.setPreferredSize(new Dimension(80,25));
    Savebtn.setMinimumSize(new Dimension(80,25));
    Delbtn.setPreferredSize(new Dimension(80,25));
    Delbtn.setMinimumSize(new Dimension(80,25));
    Refbtn.setPreferredSize(new Dimension(80,25));
    Refbtn.setMinimumSize(new Dimension(80,25));
    Hlpbtn.setPreferredSize(new Dimension(80,25));
    Hlpbtn.setMinimumSize(new Dimension(80,25));
    //
    Savebtn.setEnabled(false);
    Delbtn.setEnabled(false);
    Refbtn.setEnabled(true);
    Hlpbtn.setEnabled(true);

    BtnPanel.add(Box.createHorizontalGlue());
    BtnPanel.add(Savebtn);
    BtnPanel.add(Delbtn);
    BtnPanel.add(Refbtn);
    BtnPanel.add(Hlpbtn);

    BtnPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
    return BtnPanel;

  }

  public void setAllEnabled(boolean enable){

     if(!enable){
      Savebtn.setEnabled(enable) ;
      Delbtn.setEnabled(enable);
      MDT.DTtable.clearSelection();
     }
     Refbtn.setEnabled(enable);
     Hlpbtn.setEnabled(enable);
     //AutDefRadio.setEnabled(enable);
     //SendPendRadio.setEnabled(enable);
     MDT.setAllEnabled(enable);
  }
 //------------------------------------Templete Methods ------------------------
  public TreeMap getEHFileForms(){
      //modified by amaleh on 2004-01-26, will be using processing locations now
      AIPInit initInfo =EyesHandsManager.getInitParams(AIPManager.getCurrSid());
      initInfo.setDefaultLinkedFolder((String)(EHD.GenPane.inputFolderList.getSelectedValue()));
      //
      /* modified by amaleh
      java.io.File FFile =EHD.EFManager.getEHFormsFile(InitParamsInfo.LinkedFolderPath);
      if(FFile !=null && EHD.EFManager.isEHFile(FFile,"forms")){
            return EHD.EFManager.getForms();
     }else{
        JOptionPane.showMessageDialog(this,AIPUtil.getString("Template.Msg.NoEHFormFile"),AIPUtil.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);
     }
      */
      /* added by amaleh */
      java.io.File FFile =EHD.EFManager.getEHFormsFile(initInfo.getDefaultLinkedFolder());
      if(FFile !=null && EHD.EFManager.isEHFile(FFile,"forms")){
            return EHD.EFManager.getForms();
     }else{
        JOptionPane.showMessageDialog(this,AIPUtil.getString("Template.Msg.NoEHFormFile"),AIPUtil.getString("Init.Msg.Title"),JOptionPane.ERROR_MESSAGE);
     }
      /**/
      
     return null;
  }
  public void RefreashFormsTable(){
      //if(Log.LOG_DEBUG) Log.debug();
      TreeMap FileForms =getEHFileForms();
      if(FileForms !=null){
         AIPInit initInfo =EyesHandsManager.getInitParams(AIPManager.getCurrSid()); 
         //initInfo.setDefaultRoomNodeId(EHD.GenPane.VWNodes.getSelectedNodeid());     
         initInfo.setDefaultLinkedFolder((String)(EHD.GenPane.inputFolderList.getSelectedValue()));
      //
         Vector dbForms =new Vector();
         AIPManager.getVWC().getAIPForms(AIPManager.getCurrSid(),dbForms);
      
          MDT.EHManager.RefreshDBFormsWithFileForms(dbForms,FileForms,AIPManager.getCurrSid()) ;
          RemoveAllMDTRows();
          MDT.LoadTableData(dbForms);
      }

  }
  public void RemoveAllMDTRows(){
      MDT.DTtable.removeEditor();
      MDT.TableModel.RemoveAllRows();
      Savebtn.setEnabled(false);
      Delbtn.setEnabled(false);
  }
//----------------------------------Actions & Listeners----------------------
  class ButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
           JButton Source = (JButton)e.getSource();
           if( Source.getText().equalsIgnoreCase(AIPUtil.getString("Template.Btn.Refresh"))){
               Vector vwdts =new Vector();
               int status =DoctypesManager.getDoctypes(AIPManager.getCurrSid(),vwdts,true);
               if(status != AIPManager.NOERROR)
               {
                   JOptionPane.showMessageDialog(EHTemplatePanel.this,AIPManager.getErrorDescription(status)+"("+status+")",
                                                                        AIPUtil.getString("Template.Msg.Title"),JOptionPane.ERROR_MESSAGE); 
                   return;
               }    
               MDT.setMapDoctypes(vwdts);
               AIPInit initInfo =EyesHandsManager.getInitParams(AIPManager.getCurrSid()); 
             
              Vector nodes =DocumentsManager.getNavigatorNode(AIPManager.getCurrSid(),true); 
              MDT.setNodesNavigator(nodes);
              if(!MDT.CheckUnSaveBeforeChange(AIPUtil.getString("Template.Msg.SaveTemplete")))  return;
              RefreashFormsTable();
              Delbtn.setEnabled(false);
              Savebtn.setEnabled(false);
           }else  if( Source.getText().equalsIgnoreCase(AIPUtil.getString("Template.Btn.Save"))){
                MDT.saveTableForms();
           }else  if( Source.getText().equalsIgnoreCase(AIPUtil.getString("Template.Btn.Delete"))){
                  int[] Rows =MDT.DTtable.getSelectedRows();
                  if(Rows.length >0 ){
                    int result = JOptionPane.showConfirmDialog(MDT,AIPUtil.getString("Template.Msg.DeleteForm"),AIPUtil.getString("Init.Msg.Title"),JOptionPane.YES_NO_OPTION);
                    if(result == 0){
                       for(int i=Rows.length-1; i >= 0;i--){
                          MDT.DeleteEHFormRow(Rows[i]);
                       }
                       Delbtn.setEnabled(false);
                       if(MDT.checkUnSavedRows()) Savebtn.setEnabled(true);
                       else  Savebtn.setEnabled(false);
                    }
                  }

            }
           else  if( Source.getText().equalsIgnoreCase(AIPUtil.getString("Template.Btn.Help") )){
            try
            {
                String helpFilePath = System.getProperty("user.dir") + File.separator + ".." + File.separator + "Help" + File.separator + "VWAIP.chm";
                File helpFile = new File(helpFilePath);
                if (helpFile.canRead())
                {
                    //Runtime.getRuntime().exec("winhlp32.exe " + helpFilePath);
                    java.lang.Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + helpFilePath);
                    //send the window to the back so help files appear on top of it.
                    //EHD.frame.toBack();
                }
                else
                 JOptionPane.showMessageDialog(null, AIPUtil.getString("General.Msg.NoHelpFile"), AIPUtil.getString("Init.Msg.Title"), JOptionPane.ERROR_MESSAGE);    
            }
            catch (Exception he)
            {
                JOptionPane.showMessageDialog(null, AIPUtil.getString("General.Msg.NoHelpFile"), AIPUtil.getString("Init.Msg.Title"), JOptionPane.ERROR_MESSAGE);
               // if(Log.LOG_ERROR)Log.error("Error in launching help file: ", he);
            }
        }
           
        }
  }
}