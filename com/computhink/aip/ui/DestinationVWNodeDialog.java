/*
 * DestinationVWNode.java
 *
 * Created on January 27, 2004, 11:56 AM
 */

package com.computhink.aip.ui;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import javax.swing.tree.DefaultMutableTreeNode;
import com.computhink.aip.*;

import com.computhink.aip.util.AIPUtil;
import com.computhink.common.DocType;
import com.computhink.common.Index;

/**
 *
 * @author  amaleh
 */
public class DestinationVWNodeDialog extends javax.swing.JDialog 
{
    public static final int UNINITIALIZED_VALUE = 0;
    public static final int CLOSED_OPTION = 1;
    public static final int CANCEL_OPTION = 2;
    public static final int OK_OPTION = 3;
    private int value = UNINITIALIZED_VALUE;
    VWTreeNodeConfigLayout TNC;
    private String doctypeName;
    private String roomName;
    private Session session;
    private String path;
    private ResourceBundle bundle;
    /** Creates new form DestinationVWNode */
    /*
    public DestinationVWNodeDialog(Frame parent, boolean modal)
    {
        super(parent, modal);
    }
     */
    //update ayman
    public DestinationVWNodeDialog(Frame parent, String title, boolean modal, String doctypeName, int sessionId, String currentNodePath) 
    {
        super(parent, title, modal);
        this.TNC = TNC;
        this.doctypeName = doctypeName;
        this.session = AIPManager.getSession(sessionId);
        if(session == null) return;
        this.roomName =session.server+"."+session.room ;

        //parse node path elements and place them in an array
        Vector nodePathElements = new Vector();
        StringTokenizer nodePathTokens = new StringTokenizer(currentNodePath, "/");
        while(nodePathTokens.hasMoreTokens())
            nodePathElements.add(nodePathTokens.nextToken());
        String[] nodePathArray = new String[nodePathElements.size()];
        for (int i = 0; i < nodePathArray.length; i++)
        {
            //commented: make sure to save room name if working with multiple rooms simultaneously
            /*
            if (i == 0)
                roomName = nodePathArray[i] = (String)nodePathElements.get(i);
            else
             */
                nodePathArray[i] = (String)nodePathElements.get(i);
        }
        Vector nodesData =DocumentsManager.getNavigatorNode(sessionId,false);
        DefaultMutableTreeNode roomNode =DocumentsManager.getNode(0,nodesData);
        
        int cabCount =roomNode.getChildCount();
        String[] cabinetNodes =new String[cabCount];
        
        for(int i=0;i<cabCount;i++)
        {
             DefaultMutableTreeNode childNode = (DefaultMutableTreeNode)roomNode.getChildAt(i);
             cabinetNodes[i] = childNode.toString();
        }    
        /*VWNodesNavigator navigator = new VWNodesNavigator();
        
        VWNodeInfo[] cabinetNodes = navigator.getVWCabinets(roomName);
        String[] cabinetNodeNames = new String[cabinetNodes.length];
        for (int i = 0; i < cabinetNodes.length; i++)
            cabinetNodeNames[i] = cabinetNodes[i].getNodeName();
        
        int roomID = navigator.getIDofVWRoom(roomName);
        */
        // update Ayman
        DocType dt = DoctypesManager.getDoctype(sessionId,doctypeName);
        //int doctypeID = dtManager.getIDofDoctype(roomID, doctypeName);
        if(dt == null || dt.getId() <=0)                
        {
            doctypeName = "";
            return;
        }
        Vector dtIndices = dt.getIndices();
        Vector dtIndicesNames = new Vector();
        for (int i = 0; i < dtIndices.size(); i++)
        {
            Index dtIndex = (Index)dtIndices.get(i);
            dtIndicesNames.add(contourText(dtIndex.getName()));
        }
        String[] names = new String[dtIndicesNames.size()];
        dtIndicesNames.toArray(names);

        TNC = new VWTreeNodeConfigLayout(new String[] {roomName}, cabinetNodes, names, nodePathArray);

        initComponents();
        
        // calculate the screen dimensions
        //Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        Dimension parentSize = parent.getSize();
        Point location = parent.getLocation();
        /* shift the program's main window so it is in the middle of
        the screen */
        setLocation((int)location.getX() + parentSize.width/2 - getSize().width/2,
                            (int)location.getY() + parentSize.height/2 - getSize().height/2);
    }
    public String getPath()
    {
        return path;
    }
    /**
     * Returns selected value.
     * OK_OPTION means the user clicked OK.
     * CANCEL_OPTION means the user clicked Cancel.
     * CLOSED_OPTION means the user closed the dialog.
     * UNINTIALIZED_VALUE means the user did not click any value button yet.
     */
    public int getValue()
    {
        return value;
    }
    public String contourText(String text)
    {
        return "<".concat(text).concat(">");
    }
    
        
    /* added by amaleh on 2003-07-21 */
    /**
    * returns the depth of a path in the VW tree
    */
    public int calcDepth(String VWNodePath)
    {
      StringTokenizer sTokens = new StringTokenizer(VWNodePath, "/");
      int i = 0;
      while (sTokens.hasMoreTokens())
      {
          i++;
          sTokens.nextToken();
      }
      return i;
    }
    
    
    /**
     * Overwrides show so it wouldn't show if it did not have a doctype value
     */
    public void show()
    {
        if (doctypeName == null || doctypeName.trim().equals(""))
            return;
        else
        	/*
          	 * new JDialog().show(); method is replaced with new JDialog().setVisible(true); 
          	 * as show() method is deprecated  //Gurumurthy.T.S 19/12/2013,CV8B5-001
          	 */
            super.setVisible(true);
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    private void initComponents() {//GEN-BEGIN:initComponents
        jPanel3 = new javax.swing.JPanel();
        jPanel1 = TNC;
        jPanel2 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel3.setLayout(new java.awt.BorderLayout());

        jPanel3.setBorder(new javax.swing.border.EtchedBorder());
        jPanel1.setBorder(new javax.swing.border.CompoundBorder(new javax.swing.border.EmptyBorder(new java.awt.Insets(5, 8, 5, 5)), new javax.swing.border.TitledBorder(null, "", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.TOP)));
        jPanel3.add(jPanel1, java.awt.BorderLayout.CENTER);

        jPanel2.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

        jPanel2.setPreferredSize(new java.awt.Dimension(36, 36));
        jButton1.setText("OK");
        jButton1.setPreferredSize(new java.awt.Dimension(80, 25));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jPanel2.add(jButton1);

        jButton2.setText("Cancel");
        jButton2.setPreferredSize(new java.awt.Dimension(80, 25));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jPanel2.add(jButton2);

        jPanel3.add(jPanel2, java.awt.BorderLayout.SOUTH);

        getContentPane().add(jPanel3, java.awt.BorderLayout.CENTER);

        pack();
    }//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // Add your handling code here:
        path = TNC.getPath();
        int depth = calcDepth(path);
        if (depth < 4 || path.indexOf("//") != -1 || path.charAt(path.length()-1) == '/')
            JOptionPane.showMessageDialog(null,AIPUtil.getString("DestinationVWNode.Msg.NotAllFieldsFilled"), this.getTitle(), JOptionPane.INFORMATION_MESSAGE);
        else
        {
        setVisible(false);
        dispose();
        value = OK_OPTION;
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // Add your handling code here:
        setVisible(false);
        dispose();
        value = CANCEL_OPTION;
    }//GEN-LAST:event_jButton2ActionPerformed
    
    /** Closes the dialog */
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        setVisible(false);
        dispose();
        value = CLOSED_OPTION;
    }//GEN-LAST:event_closeDialog
    
    /**
     * @param args the command line arguments
     */
    /*
    public static void main(String args[]) {
        new DestinationVWNodeDialog(new javax.swing.JFrame(), true).show();
    }
     */
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton1;
    // End of variables declaration//GEN-END:variables
    
}
