package com.computhink.aip;


import java.util.*;

import com.computhink.aip.util.VWLogger;
import com.computhink.common.Index;
import com.computhink.common.DocType;
/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public class DoctypesManager {
      
   private static Hashtable vwDoctypes =new Hashtable();
   public DoctypesManager() {

   }
   //add ayman
   public static void releaseSession(int sid)
   {
     vwDoctypes.remove(new Integer(sid));  
   }  
   //update ayman 1/4/2004 
   public static int getDoctypes(int sid,Vector dts,boolean refresh) {
    //if(Log.LOG_VERBOSE_DEBUG) Log.debug("Get Doctypes :Server Id <"+Roomid+"> ");
        if(vwDoctypes.containsKey(new Integer(sid)) && !refresh)
        {       
                Vector vwdts = (Vector)vwDoctypes.get(new Integer(sid));
                dts.addAll(vwdts);
               return  AIPManager.NOERROR;
        }              
        int status = AIPManager.getVWC().getDocTypes(sid,dts);
        if(status ==AIPManager.NOERROR)
        {
            
            for(int i =0;i< dts.size();i++)
            {
                 DocType dt =(DocType)dts.get(i);
                 AIPManager.getVWC().getDocTypeIndices(sid,dt); 
            }    
            vwDoctypes.put(new Integer(sid),dts);
        }            
        return status;         

  }
  
   //update ayman 1/4/2004
   
  public static int getIDofDoctype(int sid,String doctypeName)
  {  
      Vector dts =new Vector();
         getDoctypes(sid,dts,false);
      for(int i=0;i< dts.size();i++)
      {
            DocType dt = (DocType)dts.get(i);
            if(doctypeName.equalsIgnoreCase(dt.getName()))
                return dt.getId();
      }   
      return 0;   
  }
  
  // new ayman
  public static DocType getDoctype(int sid,String doctypeName)
  {  
	  try{
      Vector dts =new Vector();
         getDoctypes(sid,dts,false);
      for(int i=0;i< dts.size();i++)
      {
            DocType dt = (DocType)dts.get(i);
            if(doctypeName.equalsIgnoreCase(dt.getName()))
                return dt;
      }   
	  }catch(Exception e){
		  VWLogger.info("Exception while getdocumenttype :"+e.getMessage());
	  }
      return null;   
  }
  
  public static DocType getDoctype(int sid,int dtId)
  {
		try {
			Vector dts = new Vector();
			getDoctypes(sid, dts, false);
			for (int i = 0; i < dts.size(); i++) {
				DocType dt = (DocType) dts.get(i);
				if (dtId == dt.getId())
					return dt;
			}
		} catch (Exception e) {
			VWLogger.info("Exception while getDoctype :" + e.getMessage());
		}
		return null;
	}
  
  public int getIDofIndex(int sid, String indexName)
 {
		Vector doctypes = new Vector();
		try {
			int ret = getDoctypes(sid, doctypes, false);
			if (ret == AIPManager.NOERROR) {
				for (int i = 0; i < doctypes.size(); i++) {
					DocType dt = (DocType) doctypes.get(i);
					Vector indices = dt.getIndices();
					for (int j = 0; j < indices.size(); j++) {
						Index idx = (Index) indices.get(j);
						if (indexName.equalsIgnoreCase(idx.getName()))
							return idx.getId();
					}
				}
			}
		} catch (Exception e) {
			VWLogger.info("Exception in getIDofIndex :" + e.getMessage());
		}
		return 0;
	}
  public static Index getIndex(int sid,int dtId,int indxId)
  {  
      Vector dts =new Vector();
         getDoctypes(sid,dts,false);
      for(int i=0;i< dts.size();i++)
      {
            DocType dt = (DocType)dts.get(i);
            if(dtId ==dt.getId())
            { 
                Vector indices =dt.getIndices();
                for(int j=0;j < indices.size();j++)
                {
                    Index idx =(Index)indices.get(j);
                    if(indxId ==idx.getId())
                        return idx;                    
                } 
                return null;
            }    
      }   
      return null;   
  }
  //Ayman add 06-08-2004
  public static Index getIndex(int sid,String DTName,String indexName)
  {
      Vector dts =new Vector();
         getDoctypes(sid,dts,false);
      for(int i=0;i< dts.size();i++)
      {
            DocType dt = (DocType)dts.get(i);
            if(DTName.equalsIgnoreCase(dt.getName()))
            { 
                Vector indices =dt.getIndices();
                for(int j=0;j < indices.size();j++)
                {
                    Index idx =(Index)indices.get(j);
                    if(indexName.equalsIgnoreCase(idx.getName()))
                        return idx;                    
                } 
                return null;
            }    
      }   
      return null;   
      
  }    
  
}